// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    baseUrl: 'http://ec2-15-237-10-209.eu-west-3.compute.amazonaws.com/api/v0/',
    walletUrl: 'https://ropsten.infura.io/v3/d4f62bb5b7f84db2a3cd96578f9340e2',
    bosocoinAddress: '0xace264a941a18dbc5cec45b821dcf5f3789b1566',
    eureka: 'http://15.237.10.209:8761/',
    wallboard: 'http://15.237.10.209:8763/wallboard',
    hytrix: 'http://15.237.10.209:8764/hystrix/monitor?stream=http%3A%2F%2Flocalhost%3A8080%2Fturbine.stream%3Fcluster%3Ddefault',
    zipkin: 'http://15.237.10.209:9411/zipkin/dependency',
    rabbitMq: 'http://15.237.10.209:15672/#/',
    kibana: 'http://15.237.10.209:5601/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
