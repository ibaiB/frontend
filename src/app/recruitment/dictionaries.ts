export const dictionaries = {
  storageLocations: ['GOOGLE_DRIVE', 'SHARE_POINT', 'AMAZON'],
  englishLevels: ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'],
  technologyGroups: [],
  tests: [
    'Technical test',
    'Reasoning test',
    'Performance test',
    'Competency test',
    'Candidate test',
  ],
  months: [
    { value: 9, name: 'SEP' },
    { value: 10, name: 'OCT' },
    { value: 11, name: 'NOV' },
    { value: 12, name: 'DEC' },
    { value: 1, name: 'JAN' },
    { value: 2, name: 'FEB' },
    { value: 3, name: 'MAR' },
    { value: 4, name: 'APR' },
    { value: 5, name: 'MAY' },
    { value: 6, name: 'JUN' },
    { value: 7, name: 'JUL' },
    { value: 8, name: 'AUG' },
  ],
  displayedColumns: [
    'select',
    'center',
    'name',
    'studies',
    'city',
    'type_practice',
    'knowledge',
    'status',
    'start_date',
    'end_date',
  ],
  displayedColumnsTimeTable: [
    'edit',
    'city',
    'name',
    'month_sep',
    'month_oct',
    'month_nov',
    'month_dec',
    'month_jan',
    'month_feb',
    'month_mar',
    'month_apr',
    'month_may',
    'month_jun',
    'month_jul',
    'month_aug',
  ],
  RECRUITMEN_DATA: [
    {
      city: 'LOGROÑO',
      name: 'FP DUAL 1º',
      periodo: {
        inicio: '01/03/2020',
        fin: '31/05/2020',
        porcentaje: 50,
        color: '#13DAB5',
      },
    },
    {
      city: 'LOGROÑO',
      name: 'FP DUAL 2',
      periodo: {
        inicio: '01/03/2020',
        fin: '31/05/2020',
        porcentaje: 50,
        color: '#13DAB5',
      },
    },
    {
      city: 'LOGROÑO',
      name: 'FP NO DUAL',
      periodo: {
        inicio: '01/01/2020',
        fin: '31/05/2020',
        porcentaje: 50,
        color: '#DE7373',
      },
    },
    {
      city: 'LOGROÑO',
      name: 'GRADO ING.',
      periodo: {
        inicio: '01/09/2019',
        fin: '31/12/2019',
        porcentaje: 100,
        color: '#393939',
      },
    },
    {
      city: 'LOGROÑO',
      name: 'MÁSTER FINTECH',
      periodo: {
        inicio: '01/09/2019',
        fin: '31/12/2019',
        porcentaje: 100,
        color: '#393939',
      },
    },
    {
      city: 'LOGROÑO',
      name: 'MÁSTER FINTECH',
      periodo: {
        inicio: '01/09/2019',
        fin: '31/12/2019',
        porcentaje: 100,
        color: '#393939',
      },
    },
    {
      city: 'LOGROÑO',
      name: 'MÁSTER FINTECH',
      periodo: {
        inicio: '01/09/2019',
        fin: '31/12/2019',
        porcentaje: 100,
        color: '#393939',
      },
    },
  ],
  displayedColumnsCandidate: [
    'select',
    'comment',
    'id',
    'name',
    'city',
    'status',
    'competence-score',
    'cognitive-score',
    'test-status',
    'tech-interviewer',
  ],
  status: [
    { name: 'Open', value: 'OPEN' },
    { name: 'Search for candidates', value: 'SEARCH FOR CANDIDATES' },
    { name: 'Evaluation of candidates', value: 'EVALUATION OF CANDIDATES' },
    { name: 'Assigned candidates', value: 'ASSIGNED CANDIDATES' },
    { name: 'Closed', value: 'CLOSED' },
  ],
  cities: [
    'LOGROÑO',
    'ZARAGOZA',
    'BILBAO',
    'MADRID',
    'JAÉN',
    'PAMPLONA',
  ],
  citiesFull: [
    { name: 'Logroño', value: 'LOGROÑO' },
    { name: 'Madrid', value: 'MADRID' },
    { name: 'Bilbao', value: 'BILBAO' },
    { name: 'Jaén', value: 'JAÉN' },
    { name: 'Zaragoza', value: 'ZARAGOZA' },
    { name: 'Pamplona', value: 'PAMPLONA' },
  ],
  profilesJob: [
    'Desarrollador Back',
    'Desarrollador Front',
    'Desarrollador Full Stack',
    'Devops',
    'Maquetador',
    'Data Scientist',
    'Consultor BI',
    'Big Data',
    'Data Management',
    'IOT Engineer',
    'Marching Learning',
    'IA',
    'ERP',
    'Team Leader',
    'Technical Support',
  ],
  languages: ['English', 'French', 'German'],
  languagesLevel: ['NO', 'B1', 'B2', 'C1', 'C2'],
  typesStudent: ['A', 'B', 'C'],
  typesPractice: [
    { name: 'FP Dual 1º', value: 'FPDUAL1' },
    { name: 'FP Dual 2', value: 'FPDUAL2' },
    { name: 'FP No Dual', value: 'FPNODUAL' },
    { name: 'Prácticas curriculares', value: 'PRACTICASCURRICULARES' },
    { name: 'Prácticas no curriculares', value: 'PRACTICASNOCURRICULARES' },
    { name: 'Prácticas completas', value: 'PRACTICASCOMPLETAS' },
  ],
  internshipTypes: [
    'FP_DUAL_1',
    'FP_DUAL_2',
    'FP_NO_DUAL',
    'PRACTICAS_CURRICULARES',
    'PRACTICAS_NO_CURRICULARES',
    'PRACTICAS_COMPLETAS',
  ],
  types: [
    { id: 'TRIAL_PERIOD', name: 'Trial period' },
    { id: 'END_OF_PRACTICE', name: 'End of practice' },
    { id: 'DISMISSAL', name: 'Dismissal' },
    { id: 'VOLUNTARY_DEPARTURE', name: 'Voluntary Departure' },
  ],
  contracts: ['INDEFINITE', 'PRACTICE', 'OTHER'],
  sources: [
    'Market',
    'Nfq recommendation',
    'Direct Search Campaign',
    'Recommended',
  ],
  statuses: ['Launched', 'In process', 'Confirmed'],
  places: [true, false],
  rankings: [0, 1, 2, 3, 4, 5],
  geos: ['YES', 'NO'],
  employeeStates: [
    {
      id: 'sinLanzar',
      name: 'Sin lanzar',
      order: 1
    },
    {
      id: 'lanzado',
      name: 'Lanzado',
      order: 2
    },
    {
      id: 'enProceso',
      name: 'En Proceso',
      order: 3
    },
    {
      id: 'finalizado',
      name: 'Finalizado',
      order: 4
    },
    {
      id: 'incidenciaSinResolver',
      name: 'Incidencia sin resolver',
      order: 5
    },
    {
      id: 'incidenciaResuelta',
      name: 'Incidencia resuelta',
      order: 6
    },
  ],
  columnsTestTable: [
    'select',
    'comment',
    'id',
    'name',
    'completed',
    'numQuestions'
  ],
  candidates: [
    'Candidate 1',
    'Candidate 2',
    'Candidate 3',
    'Candidate 4',
    'Candidate 5',
    'Candidate 6',
    'Candidate 7',
    'Candidate 8',
    'Candidate 9',
  ],
  companies: [
    'BOSONIT',
    'NTER',
    'NFQ ADVISORY',
    'NFORCE',
    'SOLUTIONS'
  ],
  testTypes: [
    'Front-end test',
    'Back-end test',
    'Angular test',
    'Spring test',
    '.NET test',
    'Laravel test',
    'Cakephp test',
    'Big data test',
    'IOT test'
  ],
  inputTypes: ['RADIO', 'CHECKBOX', 'TEXT'],
  fileTypes: [
    'CVI',
    'Dni',
    'EnglishCV',
    'JustificationOfEmployment',
    'Offer',
    'SS',
    'SocialSecurityCertificate',
    'SpanishCV',
  ],
  query: {
    permission: 'writer',
    sharedDomains: 'reader:nfq.es;reader:bosonit.com',
    sharedUsers: '',
    storageLocation: 'GOOGLE_DRIVE',
    uploadName: '',
  },
profileJobPosition:[
    "Desarrollador Back"
    , "Desarrollador Front"
    , "Desarrollador Full Stack"
    , "Devops"
    , "Maquetador"
    , "Data Scientist"
    , "Consultor BI"
    , "Big Data"
    , "Data Management"
    , "IOT Engineer"
    , "Marching Learning"
    , "IA"
    , "ERP"
    , "Team Leader"
    , "Technical Support"],
motivationJob:[
    "ECONOMIC"
    , "PROJECT"
    , "MANAGEMENT"
    , "GEOGRAPHICAL_MOBILITY"
    , "OTHERS"],
languageLevel: ["NO" , "B1" , "B2" , "C1" , "C2"]
};
