import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CampaignTableComponent} from './campaign/infrastructure/ng-components/campaign-table/campaign-table.component';
import {EditCampaignComponent} from './campaign/infrastructure/ng-components/edit-campaign/edit-campaign.component';
import {CandidateTableComponent} from './candidate/infrastructure/ng-components/candidate-table/candidate-table.component';
import {CampaignDetailResolverService} from './campaign/infrastructure/resolvers/campaign-detail-resolver.service';
import {CampaignTableResolverService} from './campaign/infrastructure/resolvers/campaign-table-resolver.service';
import {CandidateDetailResolverService} from './candidate/infrastructure/resolvers/candidate-detail-resolver.service';
import {CandidateTableResolverService} from './candidate/infrastructure/resolvers/candidate-table-resolver.service';
import {StudyTableComponent} from './study/infrastructure/ng-components/study-table/study-table.component';
import {StudyTableResolverService} from './study/infrastructure/resolvers/study-table-resolver.service';
import {CollegeTableComponent} from './college/infrastructure/ng-components/college-table/college-table.component';
import {CollegeTableResolverService} from './college/infrastructure/resolvers/college-table-resolver.service';
import {EditCandidateComponent} from './candidate/infrastructure/ng-components/edit-candidate/edit-candidate.component';

const routes: Routes = [
    {
        path: 'campaigns',
        data: {breadcrumb: 'Campaigns'},
        children: [
            {
                path: '',
                data: {breadcrumb: ''},
                resolve: {response: CampaignTableResolverService},
                component: CampaignTableComponent
            },
            {
                path: 'edit/:id',
                component: EditCampaignComponent,
                data: { breadcrumb: 'edit/:id' },
                resolve: { response: CampaignDetailResolverService }
            },
        ]
    },
    {
        path: 'candidates',
        data: { breadcrumb: 'Candidates' },
        children: [
            {
                path: '',
                data: { breadcrumb: '' },
                resolve: { response: CandidateTableResolverService },
                component: CandidateTableComponent
            },
            {
                path: 'new',
                data: { breadcrumb: 'New Candidate' },
                component: EditCandidateComponent,
                resolve: { response: CandidateDetailResolverService }
            },
            {
                path: 'candidate-detail/:id',
                data: { breadcrumb: 'detail/:id' },
                component: EditCandidateComponent,
                resolve: { response: CandidateDetailResolverService }
            }
        ]
    },
    {
        path: 'studies',
        data: { breadcrumb: 'Studies'},
        children: [
            {
                path: '',
                data: { breadcrumb: ''},
                resolve: { response: StudyTableResolverService },
                component: StudyTableComponent
            }
        ]
    },
    {
        path: 'colleges',
        data: { breadcrumb: 'Colleges'},
        children: [
            {
                path: '',
                data: { breadcrumb: ''},
                resolve: { response: CollegeTableResolverService },
                component: CollegeTableComponent
            }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
