import {Injectable} from '@angular/core';
import {IStudy} from '../domain/IStudy';
import {IWebStudy} from '../domain/IWebStudy';

@Injectable({
    providedIn: 'any'
})
export class StudyMapper {
    mapTo(params: any): IStudy {
        const {
            id,
            name,
            description
        } = params;

        return {
            id,
            name,
            description
        };
    }

    mapFrom(params: any): IWebStudy{
        const {
            id,
            name,
            description
        } = params;

        return {
            id: id,
            name: name,
            description: description
        };
    }
}
