import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {StudyService} from '../services/study.service';
import {getStudies} from '../../application/getStudies';
import {StudyMapper} from '../StudyMapper';
import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {IStudyTableData} from '../../domain/IStudyTableData';
import {ResolvedData} from 'src/app/shared/generics/IResolvedData';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';

@Injectable({
    providedIn: 'any'
})
export class StudyTableResolverService implements Resolve<ResolvedData<IStudyTableData>> {

    constructor(private _studyService: StudyService,
                private _studyMapper: StudyMapper,
                private _cookieService: CookieFacadeService) {

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<IStudyTableData>> {
        const filtersCache = this._cookieService.get('studyFilterCache');
        const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('study-table-config'));
        const studyParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
        const orderParams = {...tableFiltersConfig?.order} ?? {};
        
        const studies = getStudies({...JSON.parse(filtersCache),...studyParams, ...orderParams}, this._studyService, this._studyMapper);

        return forkJoin(studies)
      .pipe(
        map(response => ({
          data: {
            studies: response[0],
            filtersValue:filtersCache,
            tableFiltersConfig: tableFiltersConfig
          }
        })),
        catchError(error => {
          return of({
            data: null,
            error: error,
            message: 'Error on study table resolver, data couldn\'t be fetched'
          });
        })
      );
    }
}
