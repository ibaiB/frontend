import {Observable} from 'rxjs';
import {IWebStudy} from '../domain/IWebStudy';
import {IWebStudyList} from '../domain/IWebStudyList';

export abstract class StudyAbstractService {

  abstract create(study: IWebStudy): Observable<IWebStudy>;

  abstract search(query: any): Observable<IWebStudyList>;

  abstract findById(idStudy: string): Observable<IWebStudy>;

}
