import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment';
import {IWebStudy} from '../../domain/IWebStudy';
import {IWebStudyList} from '../../domain/IWebStudyList';
import {StudyAbstractService} from '../StudyAbstractService';

@Injectable({
    providedIn: 'any'
})
export class StudyService extends StudyAbstractService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT = 'studies';

    constructor(private _http: HttpClient) {
        super();
    }

    create(study: IWebStudy): Observable<IWebStudy> {
        return this._http
            .post<IWebStudy>(`${this.BASE_URL}${this.ENDPOINT}`, study);
    }

    search(query: any): Observable<IWebStudyList> {
        let httpParams = new HttpParams();
        let paramsKeys = Object.keys(query);
        paramsKeys.forEach(key => {if(query[key]!== undefined) httpParams = httpParams.append(key, query[key].toString())});
        return this._http
            .get<IWebStudyList>(`${this.BASE_URL}${this.ENDPOINT}/search`, { params: httpParams });
}

    findById(idStudy: string): Observable<IWebStudy> {
        return this._http
            .get<IWebStudy>(`${this.BASE_URL}${this.ENDPOINT}/${idStudy}`);
    }

}
