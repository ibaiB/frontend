import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
  multiSelect: false,
  columns: [
    {
      name: "Id",
      prop: "id",
      shown: true,
      route: "",
      cellClass: "id-table-column",
      routeId:""
    },
    {
      name: "Name",
      prop: "name",
      shown: true,
    },
    {
      name: "Description",
      prop: "description",
      shown: true,
    },
  ],
};