import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {StudyService} from 'src/app/recruitment/study/infrastructure/services/study.service';
import {StudyMapper} from '../../StudyMapper';
import {getStudies} from '../../../application/getStudies';
import {IDynamicColumn} from 'src/app/shared/custom-mat-elements/dynamic-table/IDynamicColumn';
import Swal from 'sweetalert2';
import {CreateStudyComponent} from '../create-study/create-study.component';
import {IStudy} from 'src/app/recruitment/study/domain/IStudy';
import {PageEvent} from '@angular/material/paginator';
import {IStudyTableData} from 'src/app/recruitment/study/domain/IStudyTableData';
import {ResolvedData} from 'src/app/shared/generics/IResolvedData';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from '../../../../../shared/custom-mat-elements/dynamic';
import {table} from './config';
import {DynamicTableMarkIiComponent} from 'src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {IList} from 'src/app/shared/generics/IList';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';

@Component({
  selector: 'app-study-table',
  templateUrl: './study-table.component.html',
  styleUrls: ['./study-table.component.scss']
})
export class StudyTableComponent implements OnInit {
  private _resolved: ResolvedData<IStudyTableData>;
  public filters: IAuto[] = [];
  public columns: IDynamicColumn[] = [];
  public loading = true;
  public tableData: IList<any>;
  tableConfig: ITableConfig = table;
  configVisibility: boolean = false;
  queryPagination = { page: 0, size: 10 };
  queryOrder = {orderField: '', order: ''};
  queryFilters = {};

  @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;
  
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _studyService: StudyService,
    private _studyMapper: StudyMapper,
    public dialog: MatDialog,
    private _cookieFacade: CookieFacadeService
  ) { }

  ngOnInit() {
    this._resolved = this._route.snapshot.data['response'];
    if (this._resolved.error) {
      Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
          confirmButtonColor: '#db5e5e'
      });
      this._router.navigate(['error']);
    }

    if(this._resolved.data){
      this._buildFilters();
      parseTableFiltersConfig(this._resolved.data.tableFiltersConfig, this.tableConfig, this.filters);
      this.queryFilters=this._resolved.data.filtersValue ? this._clean((JSON.parse(this._resolved.data.filtersValue))) :{};
      this.tableConfig.pagination=this._resolved.data.tableFiltersConfig?.pagination ?? {page:0, size:10};
      this.queryPagination=this.tableConfig.pagination;
      this.tableConfig.order = this._resolved.data.tableFiltersConfig?.order ?? {orderField:'', order:''};
      this.queryOrder={...this.tableConfig.order}; 
      this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                                          Object.entries(this._studyMapper.mapTo({[this.tableConfig.order.orderField]:''})).find(entry => entry[1] === '')[0] : '';
      this.tableData = this._resolved.data.studies;
      this.loading = false;
    }
    this.configFilterValue();
  }

  configFilterValue(){
      let queryFilterAux=this._resolved.data.filtersValue ? this._clean(this._studyMapper.mapTo(JSON.parse(this._resolved.data.filtersValue))) : {};

      this.filters.forEach(filter => {filter.defaultValue = queryFilterAux[this.getFilterProp(filter)]
                                      filter.value = queryFilterAux[this.getFilterProp(filter)]});        
  }

  getFilterProp(filter:any){
      return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
  }

  private _clean(object): any { 
      const cleaned = {}; 
      const keys = Object.keys(object); 
      keys.forEach(key => { 
          if (object[key]) { cleaned[key] = object[key]; } 
      }); 
      return cleaned; 
  }

  public backSearch() {
    getStudies({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._studyService, this._studyMapper)
    .subscribe(response => {
      this.tableData = response;
    }, error => {
      console.error('Error on back search', error);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
        confirmButtonColor: '#db5e5e'
      });
    });
  }

  public openCreateStudy(){
    const dialogRef = this.dialog.open(CreateStudyComponent);
    dialogRef
      .afterClosed()
      .subscribe(newStudy => {
        if(newStudy){
          this.tableData.content = [...this.tableData.content, newStudy];
        }
      })
  }

  public doAction(event: {action: string, item: IStudy}){}

  /*public navigate(event: {id: string, property: string}){
    if(event.property === 'id'){
      this._router.navigate([`edit/${event.id}`], {relativeTo: this._route});
    }
  }*/

  public setSize(event: PageEvent){}

  configChange(event: any){
    this.tableConfig = {...event[0]};
    this.filters = event[1];
    this._cookieFacade.save('study-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters,this.queryPagination,this.queryOrder)));
  }

  setConfigVisibility(visible: boolean) {
    this.configVisibility = visible;
  }

  paginationChanged(event: any) {
    this.queryPagination.page = event.page;
    this.queryPagination.size = event.size;
    this._cookieFacade.save('study-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters,this.queryPagination,this.queryOrder)));
    this.backSearch();
  }

  orderChanged(event: any) {
    if (event.orderField ==='userEmail') event.orderField='userId';
    let aux = this._studyMapper.mapFrom({[event.orderField]: ''});

    this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
    this.queryOrder.order = event.order;

    this.queryPagination.page = event.page;
    this.queryPagination.size = event.size;
    this._cookieFacade.save('study-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters,this.queryPagination,this.queryOrder)));
    this.backSearch();
  }

  filtersChanged(params?: any) {
    if (params && Object.keys(params).length !== 0) {
        const mapped = this._studyMapper.mapFrom(params);
        this.queryFilters = mapped;
    } else {
        this.queryFilters = {};
    }
    this._cookieFacade.save('studyFilterCache',JSON.stringify(this.queryFilters));
    this.queryPagination = { page: 0, size: this.queryPagination.size };
    this.dynamicTable.resetPageIndex();
    this._cookieFacade.save('study-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters,this.queryPagination,this.queryOrder)));
    this.backSearch();
  }

  private _buildFilters(){
    this.filters = [
      {
        options: [],
        prop: 'id',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Id',
        placeholder: 'Id',
        shown: true,
      },
      {
        options: [],
        prop: 'name',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Name',
        placeholder: 'Name',
        shown: true,
      },
      {
        options: [],
        prop: 'description',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Description',
        placeholder: 'Description',
        shown: true,
      },
    ];
  }
}
