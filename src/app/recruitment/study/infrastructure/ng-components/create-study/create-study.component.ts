import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {createStudy} from 'src/app/recruitment/study/application/createStudy';
import {IStudy} from 'src/app/recruitment/study/domain/IStudy';
import {StudyMapper} from 'src/app/recruitment/study/infrastructure/StudyMapper';
import {StudyService} from 'src/app/recruitment/study/infrastructure/services/study.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-study',
  templateUrl: './create-study.component.html',
  styleUrls: ['./create-study.component.scss']
})
export class CreateStudyComponent implements OnInit {

  public form: FormGroup;

  constructor(private _fb: FormBuilder,
              public dialogRef: MatDialogRef<CreateStudyComponent>,
              private studyService: StudyService,
              private studyMapper: StudyMapper
              ) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(){
    this.form = this._fb.group({
      name: [''],
      description: ['']
    })
  }

  public autocompleteSelected(event: {formField: string, value: string}){
    this.form.controls[event.formField].setValue(event.value);
  }

  public onCancelClick(){
    this.dialogRef.close();
  }

  public onSaveClick(){
    if(this.form.valid){
      const nameF = this.form.get('name').value;
      const descriptionF = this.form.get('description').value;

      const study: IStudy = {
        name: nameF,
        description: descriptionF
      };

      createStudy(study, this.studyService, this.studyMapper)
        .subscribe(response => {
          this.dialogRef.close(response);
          Swal.fire({
            icon: 'success',
            title: 'Success',
            text: `Study with Id ${response.id} created`,
            confirmButtonColor: '#db5e5e'
          });
        }, error => {
          console.error('Error at create study', error);
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            confirmButtonColor: '#db5e5e'
          });
        });
    }
  }
}
