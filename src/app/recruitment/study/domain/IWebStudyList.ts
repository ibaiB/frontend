import {IWebStudy} from './IWebStudy';

export interface IWebStudyList {
    content: IWebStudy[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
