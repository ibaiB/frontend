import {IStudy} from './IStudy';

export interface IStudyList {
    content: IStudy[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
