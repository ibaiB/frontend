export interface IStudy {
    id?: string;
    name: string;
    description: string;
}