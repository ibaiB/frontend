export interface IWebStudy {
    id?: string;
    name: string;
    description: string;
}
