import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {IList} from 'src/app/shared/generics/IList';
import {IStudy} from './IStudy';

export interface IStudyTableData{
    studies: IList<IStudy>;
    filtersValue: any;
    tableFiltersConfig: ITableFiltersConfig
}
