import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';
import {IStudy} from '../domain/IStudy';
import {StudyAbstractService} from '../infrastructure/StudyAbstractService';
import {StudyMapper} from '../infrastructure/StudyMapper';

export function getStudies(query: any, service: StudyAbstractService, mapper: StudyMapper): Observable<IList<IStudy>> {
    return service.search(query);
}
