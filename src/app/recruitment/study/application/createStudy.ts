import {map} from 'rxjs/operators';
import {IStudy} from '../domain/IStudy';
import {StudyAbstractService} from '../infrastructure/StudyAbstractService';
import {StudyMapper} from '../infrastructure/StudyMapper';

export function createStudy(study: IStudy, service: StudyAbstractService, mapper: StudyMapper){
    return service.create(mapper.mapFrom(study)).pipe(map(mapper.mapTo))
}
