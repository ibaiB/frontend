import {CollegeAbstractService} from '../infrastructure/CollegeAbstractService';

export function searchColleges(query: any, service: CollegeAbstractService) {
    return service.search(query);
}
