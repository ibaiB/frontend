import {map} from 'rxjs/operators';
import {ICollege} from '../domain/ICollege';
import {CollegeAbstractService} from '../infrastructure/CollegeAbstractService';
import {CollegeMapper} from '../infrastructure/CollegeMapper';

export function updateCollege(college: ICollege, service: CollegeAbstractService, mapper: CollegeMapper) {

    return service.update(mapper.mapFrom(college)).pipe(map(mapper.mapTo));
}
