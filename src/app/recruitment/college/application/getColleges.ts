import {CollegeAbstractService} from '../infrastructure/CollegeAbstractService';
import {CollegeMapper} from '../infrastructure/CollegeMapper';

export function getColleges(query: any, service: CollegeAbstractService, mapper: CollegeMapper) {
    return service.search(query);
}
