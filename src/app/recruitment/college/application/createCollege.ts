import {map} from 'rxjs/operators';
import {ICollege} from '../domain/ICollege';
import {CollegeAbstractService} from '../infrastructure/CollegeAbstractService';
import {CollegeMapper} from '../infrastructure/CollegeMapper';

export function createCollege(college: ICollege, service: CollegeAbstractService, mapper: CollegeMapper) {
    return service.create(mapper.mapFrom(college)).pipe(map(mapper.mapTo));
}
