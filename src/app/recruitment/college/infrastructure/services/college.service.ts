import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IList} from 'src/app/shared/generics/IList';

import {environment} from '../../../../../environments/environment.prod';
import {ICollege} from '../../domain/ICollege';
import {IWebCollege} from '../../domain/IWebCollege';
import {CollegeAbstractService} from '../CollegeAbstractService';
import {CollegeMapper} from '../CollegeMapper';

@Injectable({
  providedIn: 'any'
})
export class CollegeService extends CollegeAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'colleges';

  constructor(
    private _http: HttpClient,
    private _collegeMapper: CollegeMapper
  ) {
    super();
  }

  create(college: IWebCollege): Observable<IWebCollege> {
    return this._http.post<IWebCollege>(`${this.BASE_URL}${this.ENDPOINT}`, college);
  }

  addStudy(idCollege: string, idStudy: string): Observable<IWebCollege> {
    return this._http.post<IWebCollege>(`${this.BASE_URL}${this.ENDPOINT}/${idCollege}/studies`, idStudy);
  }

  removeStudy(idCollege: string, idStudy: string): Observable<any> {
    return this._http.delete<IWebCollege>(`${this.BASE_URL}${this.ENDPOINT}/${idCollege}/studies?idStudy=${idStudy}`);
  }

  search(query: any): Observable<IList<IWebCollege>> {
    return this._http.get<IList<any>>(`${this.BASE_URL}${this.ENDPOINT}/search`,  {params: new HttpParams({fromObject: this._clean(query)})});
  }

  update(college: ICollege): Observable<ICollege> {
    return this._http.put<ICollege>(`${this.BASE_URL}${this.ENDPOINT}/${college.id}`, this._clean(college)).pipe(map(this._collegeMapper.mapTo));
  }

  private _clean(object) {
    const cleaned = {};
    const keys = Object.keys(object);
    keys.forEach(key => {
      if (object[key]) {
        cleaned[key] = object[key];
      }
    });
    return cleaned;
  }

}
