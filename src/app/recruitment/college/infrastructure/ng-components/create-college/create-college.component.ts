import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {createCollege} from 'src/app/recruitment/college/application/createCollege';
import {ICollege} from 'src/app/recruitment/college/domain/ICollege';
import {CollegeMapper} from 'src/app/recruitment/college/infrastructure/CollegeMapper';
import {StudyService} from 'src/app/recruitment/study/infrastructure/services/study.service';
import {StudyMapper} from 'src/app/recruitment/study/infrastructure/StudyMapper';
import {getStudies} from 'src/app/recruitment/study/application/getStudies';
import {CollegeService} from 'src/app/recruitment/college/infrastructure/services/college.service';
import {IStudy} from 'src/app/recruitment/study/domain/IStudy';
import {updateCollege} from 'src/app/recruitment/college/application/updateCollege';
import {successAlert} from '../../../../../shared/error/custom-alerts';

@Component({
    selector: 'app-create-college',
    templateUrl: './create-college.component.html',
    styleUrls: ['./create-college.component.scss']
})
export class CreateCollegeComponent implements OnInit {

    public form: FormGroup;
    studiesOptions: string[] = [];
    collegeStudies: IStudy[];
    searchStudies: IStudy[] = [];
    width = '100%';
    loading: boolean = true;

    constructor(private _fb: FormBuilder,
                @Inject(MAT_DIALOG_DATA) public data: {
                    college: ICollege
                },
                public dialogRef: MatDialogRef<CreateCollegeComponent>,
                private _studyService: StudyService,
                private collegeService: CollegeService,
                private collegeMapper: CollegeMapper,
                private _studyMapper: StudyMapper) {
    }

    ngOnInit(): void {
        this.initForm(this.data?.college);
        this.fillStudies();
        this.collegeStudies = this.data?.college ? this.data?.college.studies : [];
    }

    private initForm(college?: ICollege) {
        this.form = this._fb.group({
            name: [college ? college.name : ''],
            contactMethod: [college ? college.contactMethod : ''],
            contactPerson: [college ? college.contactPerson : '']
        });
    }

    public autocompleteSelected(event: { formField: string, value: string }) {
        const selectedStudy = this.searchStudies.find(s => s.name === event.value);
        if (selectedStudy) {
            this.collegeStudies.push(selectedStudy);
        }
    }

    public removeStudy(study: IStudy) {
        const index = this.collegeStudies.indexOf(study);
        if (index > -1) {
            this.collegeStudies.splice(index, 1);
        }
        console.log('Out', this.collegeStudies);
    }

    public onCancelClick() {
        this.dialogRef.close();
    }

    public onSaveClick() {
        if (this.form.valid) {
            this.data?.college ? this.update() : this.create();
        }
    }

    public create() {
        const college: ICollege = {
            ...this.form.value, studies: this.collegeStudies.map(s => s.id)
        };
        createCollege(college, this.collegeService, this.collegeMapper)
            .subscribe(response => {
                this.dialogRef.close(response);
                successAlert('The college was successfully saved');
            });
    }

    public update() {
        const college: ICollege = {
            ...this.form.value, studies: this.collegeStudies.map(s => s.id), id: this.data.college.id
        };
        updateCollege(college, this.collegeService, this.collegeMapper)
            .subscribe(response => {
                this.dialogRef.close(response);
                successAlert('The college was successfully saved');
            });
    }

    public fillStudies() {
        let params = {page: 0, size: 100};
        getStudies(params, this._studyService, this._studyMapper)
            .subscribe(res => {
                this.studiesOptions = res.content.map(std => std.name);
                this.searchStudies = res.content;
                this.loading = false;
            }, error => {
                console.error('Error on back search', error);
            });
    }
}
