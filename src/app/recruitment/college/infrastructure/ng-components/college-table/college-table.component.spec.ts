import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CollegeTableComponent} from './college-table.component';

describe('CollegeTableComponent', () => {
  let component: CollegeTableComponent;
  let fixture: ComponentFixture<CollegeTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollegeTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollegeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
