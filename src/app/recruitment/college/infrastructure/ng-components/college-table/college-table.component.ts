import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {PageEvent} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {getColleges} from 'src/app/recruitment/college/application/getColleges';
import {ICollege} from 'src/app/recruitment/college/domain/ICollege';
import {ICollegeTableData} from 'src/app/recruitment/college/domain/ICollegeTableData';
import {CollegeMapper} from 'src/app/recruitment/college/infrastructure/CollegeMapper';
import {CollegeService} from 'src/app/recruitment/college/infrastructure/services/college.service';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {DynamicTableMarkIiComponent} from 'src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {IDynamicColumn} from 'src/app/shared/custom-mat-elements/dynamic-table/IDynamicColumn';
import {ResolvedData} from 'src/app/shared/generics/IResolvedData';
import Swal from 'sweetalert2';
import {CreateCollegeComponent} from '../create-college/create-college.component';
import {table} from './config';

@Component({
    selector: 'app-college-table',
    templateUrl: './college-table.component.html',
    styleUrls: ['./college-table.component.scss']
})
export class CollegeTableComponent implements OnInit {

    private _resolved: ResolvedData<ICollegeTableData>;
    public filters: IAuto[] = [];
    public columns: IDynamicColumn[] = [];
    public loading = true;
    public tableData;
    tableConfig: ITableConfig = table;
    queryPagination = { page: 0, size: 10 };
    queryOrder = {orderField: '', order: ''};
    queryFilters = {};
    configVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;
    
    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _collegeService: CollegeService,
        private _collegeMapper: CollegeMapper,
        public dialog: MatDialog,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit(): void {
        this._resolved = this._route.snapshot.data['response'];

        if (this._resolved.error) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
                confirmButtonColor: '#db5e5e'
            });
            this._router.navigate(['error']);
        }

        if (this._resolved.data) {
            this._buildFilters();
            this._buildColumns();
            parseTableFiltersConfig(this._resolved.data.tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters=this._resolved.data.filtersValue ? this._clean((JSON.parse(this._resolved.data.filtersValue))) :{};
            this.tableConfig.pagination=this._resolved.data.tableFiltersConfig?.pagination ?? {page:0, size:10};
            this.queryPagination=this.tableConfig.pagination;
            this.tableConfig.order = this._resolved.data.tableFiltersConfig?.order ?? {orderField:'', order:''};
            this.queryOrder={...this.tableConfig.order}; 
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                                                Object.entries(this._collegeMapper.mapTo({[this.tableConfig.order.orderField]:''})).find(entry => entry[1] === '')[0] : '';
            this.tableData = this._resolved.data.colleges;
            this.loading = false;
        }
        this.configFilterValue();
    }

    configFilterValue(){
        let queryFilterAux=this._resolved.data.filtersValue ? this._clean(this._collegeMapper.mapTo(JSON.parse(this._resolved.data.filtersValue))) : {};

        this.filters.forEach(filter => {filter.defaultValue = queryFilterAux[this.getFilterProp(filter)]
                                        filter.value = queryFilterAux[this.getFilterProp(filter)]});        
    }

    getFilterProp(filter:any){
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any { 
        const cleaned = {}; 
        const keys = Object.keys(object); 
        keys.forEach(key => { 
            if (object[key]) { cleaned[key] = object[key]; } 
        }); 
        return cleaned; 
    }

    public backSearch() {
        getColleges({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._collegeService, this._collegeMapper)
            .subscribe(response => {
                this.tableData = {
                    content: response.content,
                    totalElements: response.totalElements,
                    numberOfElements: response.numberOfElements,
                    totalPages: response.totalPages
                };
            }, error => {
                console.error('Error on back search', error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    confirmButtonColor: '#db5e5e'
                });
            });
    }

    configChange(event:any){
        this.tableConfig={...event[0]};
        this.filters=event[1];
        this._cookieFacade.save('college-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination,this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    public openCreateCollege() {
        const dialogRef = this.dialog.open(CreateCollegeComponent, {
            data: null
        });
        dialogRef
            .afterClosed()
            .subscribe(newCollege => {
                if (newCollege) {
                    this.tableData.content = [...this.tableData.content, newCollege];
                }
            });
    }

    public doAction(event) {
        if (event) {
            this.openCollegeDetail(event.item);
        }
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('college-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination,this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        if (event.orderField ==='userEmail') event.orderField='userId';
        let aux = this._collegeMapper.mapFrom({[event.orderField]: ''});

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('college-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination,this.queryOrder)));
        this.backSearch();
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const mapped = this._collegeMapper.mapFrom(params);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('collegeFilterCache',JSON.stringify(this.queryFilters));
        this.queryPagination = { page: 0, size: this.queryPagination.size };
        this._cookieFacade.save('college-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination,this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    public openCollegeDetail(college: ICollege) {
        const dialogRef = this.dialog.open(CreateCollegeComponent, {
            data: {
                college: college
            }
        });

        dialogRef
            .afterClosed()
            .subscribe(newCollege => {
                if (newCollege) {
                    const college = this.tableData.content.find(c => c.id === newCollege.id);
                    const index = this.tableData.content.indexOf(college);
                    if (index > -1) {
                        this.tableData.content.splice(index, 1, newCollege);
                        const aux = this.tableData.content;
                        this.tableData.content = [];
                        this.tableData.content = [...aux];
                    }
                }
            });
    }

    public setSize(event: PageEvent) {
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Id',
                placeholder: 'Id',
                shown: true,
            },
            {
                options: [],
                prop: 'name',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Name',
                placeholder: 'Name',
                shown: true,
            },
            {
                options: [],
                prop: 'contactMethod',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Contact method',
                placeholder: 'ContactMethod',
                shown: true,
            },
            {
                options: [],
                prop: 'contactPerson',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Contact person',
                placeholder: 'ContactPerson',
                shown: true,
            },

        ];
    }

    private _buildColumns() {
        this.columns = [
            {name: 'Id', property: 'id', shown: true, type: 'id', class: ''},
            {name: 'Name', property: 'name', shown: true, type: 'string', class: ''},
            {name: 'Contact method', property: 'contactMethod', shown: true, type: 'string', class: ''},
            {name: 'Contact person', property: 'contactPerson', shown: true, type: 'string', class: ''}
        ];
    }
}
