import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
  multiSelect: false,
  columns: [
    {
      name: 'Id',
      prop: 'id',
      shown: true,
      route: 'a',
      routeId: '',
      cellClass: 'id-table-column'
    },
      {
        name: 'Name',
        prop: 'name',
        shown: true
      },
      {
        name: 'Contact method',
        prop: 'contactMethod',
        shown: true
      },
      {
        name: 'Contact person',
        prop: 'contactPerson',
        shown: true
      },
    ]
  };
