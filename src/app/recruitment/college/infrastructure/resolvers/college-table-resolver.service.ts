import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ResolvedData} from 'src/app/shared/generics/IResolvedData';
import {getColleges} from '../../application/getColleges';
import {ICollegeTableData} from '../../domain/ICollegeTableData';
import {CollegeMapper} from '../CollegeMapper';
import {CollegeService} from '../services/college.service';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';

@Injectable({
    providedIn: 'any'
})
export class CollegeTableResolverService implements Resolve<ResolvedData<ICollegeTableData>> {

    constructor(private _collegeService: CollegeService,
                private _collegeMapper: CollegeMapper,
                private _cookieService: CookieFacadeService) {

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<ICollegeTableData>> {
        const filtersCache = this._cookieService.get('collegeFilterCache');
        const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('college-table-config'));
        const collegeParams =  tableFiltersConfig?.pagination ?? {page: 0, size: 10};
        const orderParams = {...tableFiltersConfig?.order} ?? {};
        
        const colleges = getColleges({...JSON.parse(filtersCache), ...collegeParams, ...orderParams}, this._collegeService, this._collegeMapper);
        
        return forkJoin(colleges)
    .pipe(
        map(response => ({
        data: {
            colleges: {
                content: response[0].content.map(this._collegeMapper.mapTo),
                numberOfElements: response[0].numberOfElements,
                totalElements: response[0].totalElements,
                totalPages: response[0].totalPages,
              },
              filtersValue: filtersCache,
              tableFiltersConfig: tableFiltersConfig
        }
        })),
        catchError(error => {
        return of({
            data: null,
            error: error,
            message: 'Error on college table resolver, data couldn\'t be fetched'
        });
        })
    );
    }
}
