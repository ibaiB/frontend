import {Observable} from 'rxjs';
import {ICollege} from '../domain/ICollege';
import {IWebCollege} from '../domain/IWebCollege';
import {IWebCollegeList} from '../domain/IWebCollegeList';

export abstract class CollegeAbstractService {

  abstract create(college: IWebCollege): Observable<IWebCollege>;

  abstract addStudy(idCollege: string, idStudy: string): Observable<IWebCollege>;

  abstract removeStudy(idCollege: string, idStudy: string): Observable<any>;

  abstract search(query: any): Observable<IWebCollegeList>;

  abstract update(college: ICollege): Observable<ICollege>;

}
