import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class CollegeMapper {
    mapTo(params: any) {
        const {
            id,
            name,
            contactMethod,
            contactPerson,
            studies,
        } = params;

        return {
            id: id,
            name: name,
            contactMethod: contactMethod,
            contactPerson: contactPerson,
            studies: studies,
        };
    }

    mapFrom(params: any) {
        const {
            id,
            name,
            contactMethod,
            contactPerson,
            studies,
        } = params;

        return {
            id: id,
            name: name,
            contactMethod: contactMethod,
            contactPerson: contactPerson,
            idStudies: studies,
        };
    }
}
