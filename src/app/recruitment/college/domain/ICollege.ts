export interface ICollege {
    id?: string;
    name: string;
    contactMethod: string;
    contactPerson: string;
    studies?: any[];
    idStudies?: string[]
}
