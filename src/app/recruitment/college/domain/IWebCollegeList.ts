import {IWebCollege} from './IWebCollege';

export interface IWebCollegeList {
    content: IWebCollege[];
    totalElements: number;
    numberOfElements: number;
    totalPages: number;
}
