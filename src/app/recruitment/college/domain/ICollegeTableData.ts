import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {IList} from 'src/app/shared/generics/IList';
import {ICollege} from './ICollege';

export interface ICollegeTableData{
    colleges: IList<ICollege>;
    filtersValue:any;
    tableFiltersConfig: ITableFiltersConfig
}
