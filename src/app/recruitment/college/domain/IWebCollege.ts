import {IStudy} from '../../study/domain/IStudy';

export interface IWebCollege {
    id?: string;
    name: string;
    contactMethod: string;
    contactPerson: string;
    studies?: IStudy[];
    idStudies?  : string[];
}
