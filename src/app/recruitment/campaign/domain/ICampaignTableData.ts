import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {IWebCollegeList} from '../../college/domain/IWebCollegeList';
import {IStudyList} from '../../study/domain/IStudyList';
import {IWebCampaignList} from './IWebCampaignList';

export interface ICampaignTableData {
    campaigns: IWebCampaignList;
    colleges: IWebCollegeList;
    studies: IStudyList;
    filtersValue:any;
    tableFiltersConfig: ITableFiltersConfig
}
