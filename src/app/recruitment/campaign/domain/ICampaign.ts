import {ICandidate} from '../../candidate/domain/ICandidate';

export interface ICampaign {
    id?: string;
    city: string;
    contactMethod: string;
    endDate: string;
    college?: any;
    study?: any;
    idCollege?: string;
    idStudy?: string;
    name: string;
    nhours: number;
    placeGuaranteed: number;
    placeNotGuaranteed: number;
    startDate: string;
    status: string;
    typeOfInternship: string;
    weeklySchedule: string;
    candidates: ICandidate[];
}
