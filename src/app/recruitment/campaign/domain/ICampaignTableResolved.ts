import {ICampaignTableData} from './ICampaignTableData';

export interface ICampaignTableResolved {
    tableData: ICampaignTableData;
    error?: any;
}
