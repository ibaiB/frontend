import {IList} from 'src/app/shared/generics/IList';
import {ICandidate} from '../../candidate/domain/ICandidate';
import {ICollege} from '../../college/domain/ICollege';
import {IStudy} from '../../study/domain/IStudy';
import {ICampaign} from './ICampaign';

export interface ICampaignDetail {
    campaign: ICampaign;
    colleges: IList<ICollege>;
    studies: IList<IStudy>;
    candidates: IList<ICandidate>;
    allCandidates: IList<ICandidate>;
}
