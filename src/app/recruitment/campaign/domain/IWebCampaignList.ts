import {IWebCampaign} from './IWebCampaign';

export interface IWebCampaignList {
    content: IWebCampaign[];
    totalElements: number;
    numberOfElements: number;
    totalPages: number;
}
