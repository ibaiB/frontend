import {ICampaign} from '../domain/ICampaign';
import {CampaignAbstractService} from '../infrastructure/CampaignAbstractService';
import {CampaignMapper} from '../infrastructure/CampaignMapper';
import {map} from 'rxjs/operators';

export function createCampaign(campaign: ICampaign, service: CampaignAbstractService, mapper: CampaignMapper) {
    return service.create(mapper.mapFrom(campaign)).pipe(map(mapper.mapTo));
}
