import {map} from 'rxjs/operators';

import {IWebCampaign} from '../domain/IWebCampaign';
import {CampaignAbstractService} from '../infrastructure/CampaignAbstractService';
import {CampaignMapper} from '../infrastructure/CampaignMapper';

export function updateCampaign(campaign: IWebCampaign, service: CampaignAbstractService, mapper: CampaignMapper) {
    return service.update(mapper.mapFrom(campaign)).pipe(map(mapper.mapTo));
}
