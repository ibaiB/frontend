import {CampaignAbstractService} from '../infrastructure/CampaignAbstractService';

export function getCampaigns(query: any, service: CampaignAbstractService) {
    return service.search(query);
}
