import {CampaignAbstractService} from '../infrastructure/CampaignAbstractService';

export function removeCandidateFromCampaign(idCampaign: string, idCandidate: string, service: CampaignAbstractService) {
    return service.removeCandidate(idCampaign, idCandidate);
}
