import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ICampaign} from '../domain/ICampaign';
import {CampaignAbstractService} from '../infrastructure/CampaignAbstractService';
import {CampaignMapper} from '../infrastructure/CampaignMapper';

export function getCampaignById(idCampaign: string, service: CampaignAbstractService, mapper: CampaignMapper): Observable<ICampaign> {
    return service.findById(idCampaign).pipe(map(mapper.mapTo));
}
