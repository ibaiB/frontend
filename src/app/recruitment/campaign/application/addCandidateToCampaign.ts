import {CampaignAbstractService} from '../infrastructure/CampaignAbstractService';

export function addCandidateToCampaign(idCampaign: string, idCandidate: string, service: CampaignAbstractService) {
    return service.addCandidate(idCampaign, idCandidate);
}
