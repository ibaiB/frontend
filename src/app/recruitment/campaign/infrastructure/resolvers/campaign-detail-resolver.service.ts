import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';

import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {getCampaignById} from '../../application/getCampaignById';
import {ICampaignDetail} from '../../domain/ICampaignDetail';
import {CampaignMapper} from '../CampaignMapper';
import {searchCandidates} from '../../../candidate/application/searchCandidates';
import {CandidateMapper} from '../../../candidate/infrastructure/CandidateMapper';
import {getColleges} from '../../../college/application/getColleges';
import {CollegeMapper} from '../../../college/infrastructure/CollegeMapper';
import {getStudies} from '../../../study/application/getStudies';
import {StudyMapper} from '../../../study/infrastructure/StudyMapper';
import {CampaignService} from '../services/campaign.service';
import {CandidateService} from '../../../candidate/infrastructure/services/candidate.service';
import {CollegeService} from '../../../college/infrastructure/services/college.service';
import {StudyService} from '../../../study/infrastructure/services/study.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignDetailResolverService implements Resolve<ResolvedData<ICampaignDetail>> {

  constructor(
      private _campaignService: CampaignService,
      private _collegeService: CollegeService,
      private _studyService: StudyService,
      private _candidateService: CandidateService,
      private _studyMapper: StudyMapper,
      private _candidateMapper: CandidateMapper,
      private _collegeMapper: CollegeMapper,
      private _campaignMapper: CampaignMapper,
      private _cookieService : CookieFacadeService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<ICampaignDetail>> {
    const params = {page: 0, size: 100};
    const filtersCache = this._cookieService.get('editCampaignFilterCache');
    const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('edit-campaign-table-config'));
    const candidateParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
    const orderParams = {...tableFiltersConfig?.order} ?? {};

    const campaign = getCampaignById(route.params['id'], this._campaignService, this._campaignMapper);
    const colleges = getColleges(params, this._collegeService, this._collegeMapper);
    const studies = getStudies(params, this._studyService, this._studyMapper);
    const candidates = searchCandidates({ ...JSON.parse(filtersCache),...candidateParams,...orderParams, idCampaign: route.params['id'] }, this._candidateService, this._candidateMapper);
    //@TODO esto es una burrada
    const allCandidates = searchCandidates({page: 0, size: 500}, this._candidateService, this._candidateMapper);

    return forkJoin(campaign, colleges, studies, candidates, allCandidates)
      .pipe(
        map(response => ({
          data: {
            campaign: response[0],
            colleges: {
              content: response[1].content.map(this._collegeMapper.mapTo),
              numberOfElements: response[1].numberOfElements,
              totalElements: response[1].totalElements,
              totalPages: response[1].totalPages
            },
            studies: response[2],
            candidates: response[3],
            allCandidates: response[4],
            filtersValue:filtersCache,
            tableFiltersConfig: tableFiltersConfig
          }
        })),
        catchError(error => {
          return of({
            data: null,
            message: 'Error on campaign detail resolver, data coudn\'t be fetched',
            error: error
          })
        })
      );

  }
}
