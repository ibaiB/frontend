import {Injectable} from '@angular/core';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {getCampaigns} from '../../application/getCampaigns';
import {ICampaignTableResolved} from '../../domain/ICampaignTableResolved';
import {CampaignService} from '../services/campaign.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {getStudies} from '../../../study/application/getStudies';
import {StudyService} from '../../../study/infrastructure/services/study.service';
import {getColleges} from '../../../college/application/getColleges';
import {CollegeService} from '../../../college/infrastructure/services/college.service';
import {CollegeMapper} from '../../../college/infrastructure/CollegeMapper';
import {StudyMapper} from '../../../study/infrastructure/StudyMapper';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';

@Injectable({
  providedIn: 'any'
})
export class CampaignTableResolverService implements Resolve<ICampaignTableResolved> {

  constructor(private _campaignService: CampaignService,
              private _studyService: StudyService,
              private _collegeService: CollegeService,
              private _collegeMapper: CollegeMapper,
    private _studyMapper: StudyMapper,
    private _cookieService : CookieFacadeService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<ICampaignTableResolved> | Promise<ICampaignTableResolved> | ICampaignTableResolved {
    let params = { page: 0, size: 1000 };
    const filtersCache = this._cookieService.get('campaignFilterCache');
    const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('campaign-table-config'));
    const campaingParams=tableFiltersConfig?.pagination ?? {page: 0, size: 10};
    const orderParams = {...tableFiltersConfig?.order} ?? {};

    const campaigns = getCampaigns({...JSON.parse(filtersCache), ...campaingParams,...orderParams}, this._campaignService);
    const colleges = getColleges(params, this._collegeService, this._collegeMapper);
    const studies = getStudies(params, this._studyService, this._studyMapper);

    return forkJoin(campaigns, colleges, studies)
      .pipe(
        map(response => ({
          tableData: {
            campaigns: response[0],
            colleges: response[1],
            studies: response[2],
            filtersValue : filtersCache,
            tableFiltersConfig: tableFiltersConfig
          }
        })),
        catchError(error => {
          return of({
            tableData: null,
            error: { message: 'Error on campaign table resolver, data couldn\'t be fetched', error: error }
          });
        })
      );
  }
}
