import {TestBed} from '@angular/core/testing';

import {CampaignDetailResolverService} from './campaign-detail-resolver.service';

describe('CampaignDetailResolverService', () => {
  let service: CampaignDetailResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignDetailResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
