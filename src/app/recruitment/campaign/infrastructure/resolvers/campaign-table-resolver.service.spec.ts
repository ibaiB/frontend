import {TestBed} from '@angular/core/testing';

import {CampaignTableResolverService} from './campaign-table-resolver.service';

describe('CampaingTableResolverService', () => {
  let service: CampaignTableResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampaignTableResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
