import {Observable} from 'rxjs';
import {IWebCampaign} from '../domain/IWebCampaign';
import {ICampaign} from '../domain/ICampaign';
import {IList} from '../../../shared/generics/IList';

export abstract class CampaignAbstractService {

  abstract create(campaign: IWebCampaign): Observable<IWebCampaign>;

  abstract update(campaign: IWebCampaign): Observable<IWebCampaign>;

  abstract delete(id: string): Observable<any>;

  abstract findById(id: string): Observable<IWebCampaign>;

  abstract search(query: any): Observable<IList<ICampaign>>;

  abstract addCandidate(idCampaign: string, idCandidate: string): Observable<IWebCampaign>;

  abstract removeCandidate(idCampaign: string, idCandidate: string): Observable<IWebCampaign>;

}
