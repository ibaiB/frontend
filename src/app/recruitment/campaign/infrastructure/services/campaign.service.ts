import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment.prod';
import {CampaignAbstractService} from '../CampaignAbstractService';
import {IWebCampaign} from '../../domain/IWebCampaign';
import {ICampaign} from '../../domain/ICampaign';
import {IList} from 'src/app/shared/generics/IList';

@Injectable({
    providedIn: 'any'
})
export class CampaignService extends CampaignAbstractService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'campaigns';

    constructor(private _http: HttpClient) {
        super();
    }

    create(campaign: IWebCampaign): Observable<IWebCampaign> {
        return this._http.post<IWebCampaign>(`${this.BASE_URL}${this.ENDPOINT}`, campaign);
    }

    findById(id: string): Observable<IWebCampaign> {
        return this._http.get<IWebCampaign>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    update(campaign: IWebCampaign): Observable<IWebCampaign> {
        return this._http.put<IWebCampaign>(`${this.BASE_URL}${this.ENDPOINT}/${campaign.id}`, campaign);
    }

    delete(id: string): Observable<any> {
        return this._http.delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    search(query: any): Observable<IList<ICampaign>> {
        return this._http.get<IList<ICampaign>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: this._clean(query)})});
    }

    addCandidate(idCampaign: string, idCandidate: string): Observable<IWebCampaign> {
        return this._http.post<IWebCampaign>(`${this.BASE_URL}${this.ENDPOINT}/${idCampaign}/candidates/${idCandidate}`, {});
    }

    removeCandidate(idCampaign: string, idCandidate: string): Observable<IWebCampaign> {
        return this._http.delete<IWebCampaign>(`${this.BASE_URL}${this.ENDPOINT}/${idCampaign}/candidates/${idCandidate}`);
    }

    private _clean(object): any { 
        const cleaned = {}; 
        const keys = Object.keys(object); 
        keys.forEach(key => { 
            if (object[key]) { cleaned[key] = object[key]; } 
        }); 
        return cleaned; 
    }
}

