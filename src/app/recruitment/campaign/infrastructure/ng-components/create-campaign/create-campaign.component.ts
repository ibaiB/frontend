import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

import {createCampaign} from '../../../application/createCampaign';
import {ICampaign} from '../../../domain/ICampaign';
import {CampaignMapper} from '../../CampaignMapper';
import {IWebCollege} from '../../../../college/domain/IWebCollege';
import {IWebStudy} from '../../../../study/domain/IWebStudy';
import {CampaignService} from '../../services/campaign.service';
import {successAlert} from '../../../../../shared/error/custom-alerts';

@Component({
    selector: 'app-create-campaign',
    templateUrl: './create-campaign.component.html',
    styleUrls: ['./create-campaign.component.scss']
})
export class CreateCampaignComponent implements OnInit {

    public form: FormGroup;

    public collegeOptions: string[] = [];
    public studyOptions: string[] = [];
    public statusOptions: string[] = [];
    status: string[] = ['No comenzada', 'Comenzada', 'Finalizada'];

    constructor(
        private _fb: FormBuilder,
        public dialogRef: MatDialogRef<CreateCampaignComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { colleges: IWebCollege[], studies: IWebStudy[] },
        private campaignService: CampaignService,
        private campaignMapper: CampaignMapper
    ) {
    }

    ngOnInit() {
        this.collegeOptions = this.data.colleges.map(c => c.name);
        this.studyOptions = this.data.studies.map(s => s.name);
        // TODO: waiting for entity back and front
        // this.statusOptions = this.data.status.map(s => s.name);

        this.initForm();
    }

    private initForm() {
        this.form = this._fb.group({
            name: [''],
            nhours: ['', Validators.pattern('([0-9])*')],
            city: [''],
            placeGuaranteed: ['', Validators.pattern('([0-9])*')],
            placeNotGuaranteed: ['', Validators.pattern('([0-9])*')],
            contactMethod: [''],
            startDate: [''],
            endDate: [''],
            typeOfInternship: [''],
            weeklySchedule: [''],
            status: [this.status[0]],
            // autocomplete
            idCollege: [''],
            idStudy: [''],
            idStatus: [''],
        });
    }

    public autocompleteSelected(event: { formField: string, value: string }) {
        this.form.controls[event.formField].setValue(event.value);
    }

    public onCancelClick() {
        this.dialogRef.close();
    }

    public onSaveClick() {
        if (this.form.valid) {
            const nameValue = this.form.get('name').value;
            const nhoursValue = this.form.get('nhours').value;
            const cityValue = this.form.get('city').value;
            const placeGuaranteedValue = this.form.get('placeGuaranteed').value;
            const placeNotGuaranteedValue = this.form.get('placeNotGuaranteed').value;
            const contactMethodValue = this.form.get('contactMethod').value;
            const startDateValue = this.form.get('startDate').value;
            const endDateValue = this.form.get('endDate').value;
            const typeOfInternshipValue = this.form.get('typeOfInternship').value;
            const weeklyScheduleValue = this.form.get('weeklySchedule').value;
            // autocomplete?
            const collegeValue = this.form.get('idCollege').value;
            const studyValue = this.form.get('idStudy').value;
            const statusValue = this.form.get('status').value;

            const college = this.data.colleges.find(c => c.name === collegeValue);
            const study = this.data.studies.find(s => s.name === studyValue);
            // const status = this.data.status.find(s => s.name === statusValue);

            const campaign: ICampaign = {
                name: nameValue,
                nhours: nhoursValue,
                city: cityValue,
                placeGuaranteed: placeGuaranteedValue,
                placeNotGuaranteed: placeNotGuaranteedValue,
                contactMethod: contactMethodValue,
                startDate: startDateValue,
                endDate: endDateValue,
                idCollege: college ? college.id : null,
                idStudy: study ? study.id : null,
                status: statusValue ? statusValue : null,
                // idStatus: idStatus,
                typeOfInternship: typeOfInternshipValue,
                weeklySchedule: weeklyScheduleValue,
                candidates: null,
            };

            createCampaign(campaign, this.campaignService, this.campaignMapper)
                .subscribe(() => {
                    successAlert('').then(() => this.dialogRef.close());
                });
        }
    }

}
