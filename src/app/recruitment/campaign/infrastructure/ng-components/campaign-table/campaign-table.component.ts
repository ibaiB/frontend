import {StudyMapper} from '../../../../study/infrastructure/StudyMapper';
import {StudyService} from '../../../../study/infrastructure/services/study.service';
import {IStudy} from '../../../../study/domain/IStudy';
import {CollegeMapper} from '../../../../college/infrastructure/CollegeMapper';
import {CollegeService} from '../../../../college/infrastructure/services/college.service';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';

import {IActivity} from '../../../../../crm/entities/activity/domain/IActivity';
import {IDynamicColumn} from '../../../../../shared/custom-mat-elements/dynamic-table/IDynamicColumn';
import {CampaignService} from '../../services/campaign.service';
import {getCampaigns} from '../../../application/getCampaigns';
import {CreateCampaignComponent} from '../create-campaign/create-campaign.component';
import {IWebCollege} from '../../../../college/domain/IWebCollege';
import {IWebStudy} from '../../../../study/domain/IWebStudy';
import {ICampaignTableResolved} from '../../../domain/ICampaignTableResolved';
import {table} from './config';
import {IList} from '../../../../../shared/generics/IList';
import {DynamicTableMarkIiComponent} from '../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {CampaignMapper} from '../../CampaignMapper';
import {ICampaign} from 'src/app/recruitment/campaign/domain/ICampaign';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';

@Component({
    selector: 'app-campaign-table',
    templateUrl: './campaign-table.component.html',
    styleUrls: ['./campaign-table.component.scss']
})
export class CampaignTableComponent implements OnInit {

    private _resolved: ICampaignTableResolved;
    tableConfig: ITableConfig = table;
    public filters: IAuto[] = [];
    public columns: IDynamicColumn[] = [];
    public loading = true;
    public data: any[] = [];
    public centerOptions: IWebCollege[] = [];
    public studiesOptions: IStudy[] = [];
    tableData: IList<any>;
    queryPagination;
    queryOrder;
    queryFilters;

    private colleges: IWebCollege[];
    private studies: IWebStudy[];
    // TODO: back has to create the entity
    private campaignStatus: any[];

    configVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _campaignService: CampaignService,
        public dialog: MatDialog,
        private _collegeService: CollegeService,
        private _collegeMapper: CollegeMapper,
        private _studyService: StudyService,
        private _studyMapper: StudyMapper,
        private _campaingMapper: CampaignMapper,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit() {
        this._resolved = this._route.snapshot.data['response'];

        if (this._resolved.tableData) {
            this.centerOptions = this._resolved.tableData.colleges.content;
            this.studiesOptions = this._resolved.tableData.studies.content;
            this._buildFilters();
            parseTableFiltersConfig(this._resolved.tableData.tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = this._resolved.tableData.filtersValue ? this._clean((JSON.parse(this._resolved.tableData.filtersValue))) : {};
            this.tableConfig.pagination = this._resolved.tableData.tableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = this._resolved.tableData.tableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries(this._campaingMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
            this.colleges = this._resolved.tableData.colleges.content;
            this.studies = this._resolved.tableData.studies.content;
            this.tableData = this._resolved.tableData.campaigns;
            this.tableData.content = this.tableData.content.map(CampaignTableComponent._extend);
            this.campaignStatus = null;

            this.loading = false;
        }
        this.configFilterValue();
    }

    configFilterValue() {
        let queryFilterAux = this._resolved.tableData.filtersValue ? this._clean(this._campaingMapper.mapTo(JSON.parse(this._resolved.tableData.filtersValue))) : {};

        queryFilterAux['idCollege'] = this._resolved.tableData.colleges.content.find(college => college.id === queryFilterAux['idCollege'])?.name ?? '';
        queryFilterAux['idStudy'] = this._resolved.tableData.studies.content.find(study => study.id === queryFilterAux['idStudy'])?.name ?? '',
            this.filters.forEach(filter => {
                filter.defaultValue = queryFilterAux[this.getFilterProp(filter)];
                filter.value = queryFilterAux[this.getFilterProp(filter)];
            });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autocomplete' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }


    public backSearch() {

        getCampaigns({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._campaignService)
            .subscribe(response => {
                this.tableData = {
                    content: response.content,
                    totalElements: response.totalElements,
                    numberOfElements: response.numberOfElements,
                    totalPages: response.totalPages
                };
                this.tableData.content = this.tableData.content.map(CampaignTableComponent._extend);
            });
    }

    private static _extend(a: ICampaign): any {
        return {
            ...a,
            collageName: a.college ? a.college.name : a.college,
            studyName: a.study ? a.study.name : a.study,
        };
    }

    public openCreateCampaign() {
        const dialogRef = this.dialog.open(CreateCampaignComponent, {
            data: {
                colleges: this.colleges,
                studies: this.studies,
                status: this.campaignStatus,
            }
        });
    }

    public doAction(event: { action: string, item: IActivity }) {

    }

    public navigate(event: { id: string, property: string }) {
        if (event.property === 'id') {
            this._router.navigate([`./app/edit/${event.id}`], {relativeTo: this._route});
        }
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const switched = this._querySwitcher(params, this._resolved);
            const mapped = this._campaingMapper.mapFrom(switched);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('campaignFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('campaign-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('campaign-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        if (event.orderField === 'studyName') {
            event.orderField = 'idStudy';
        }
        if (event.orderField === 'collageName') {
            event.orderField = 'idCollege';
        }
        let aux = {[event.orderField]: ''};
        aux = this._campaingMapper.mapFrom(aux);

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('campaign-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

        this.backSearch();
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('campaign-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Id',
                placeholder: 'Id',
                shown: true,
            },
            {
                options: this.centerOptions.map(center => center.name),
                prop: 'name',
                retProp: 'id',
                searchValue: 'idCollege',
                type: 'autocomplete',
                appearance: 'outline',
                class: 'autocomplete',
                label: 'Center',
                placeholder: 'Center',
                shown: true,
            },
            {
                options: [],
                prop: 'name',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Name',
                placeholder: 'Name',
                shown: true,
            },
            {
                options: this.studiesOptions.map(study => study.name),
                prop: 'name',
                retProp: 'id',
                searchValue: 'idStudy',
                type: 'autocomplete',
                appearance: 'outline',
                class: 'autocomplete',
                label: 'Study',
                placeholder: 'Study',
                shown: true,
            },
            {
                options: [],
                prop: 'city',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'City',
                placeholder: 'City',
                shown: false,
            },
            {
                options: [],
                prop: 'status',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Status',
                placeholder: 'Status',
                shown: false,
            },
            {
                options: [],
                prop: 'startDate',
                retProp: 'startDate',
                type: 'date',
                appearance: 'outline',
                class: 'date',
                label: 'Start Date',
                placeholder: 'Start Date',
                shown: false,
            },
            {
                options: [],
                prop: 'endDate',
                retProp: 'endDate',
                type: 'date',
                appearance: 'outline',
                class: 'date',
                label: 'End Date',
                placeholder: 'End Date',
                shown: false,
            },
        ];
    }

    _querySwitcher(query: any, _resolved) {

        const college = _resolved.tableData.colleges.content.filter(college => college.name === query.idCollege)[0];
        if (college) {
            query.idCollege = college.id;
        }

        const study = _resolved.tableData.studies.content.filter(study => study.name === query.idStudy)[0];
        if (study) {
            query.idStudy = study.id;
        }

        if (query.startDate) {
            let date = new Date(query.startDate);
            date = new Date(date.setDate(date.getDate() - 1)); // -1 to include current day
            query.initialStartDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
            date = new Date(date.setDate(date.getDate() + 2)); // Date adding one day
            query.finalStartDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
        }

        if (query.endDate) {
            let date = new Date(query.endDate);
            date = new Date(date.setDate(date.getDate() - 1));
            query.initialEndDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
            date = new Date(date.setDate(date.getDate() + 1)); // Date adding one day
            query.finalEndDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
        }

        return query;
    }
}
