import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
  multiSelect: false,
  columns: [
    {
      name: 'Id',
      prop: 'id',
      shown: true,
      route: './app/recruitment/campaigns/edit/',
      cellClass: 'id-table-column',
      routeId: 'id'
    },
      {
        name: "Center",
        prop: "collageName",
        shown: true,
      },
      {
        name: "Name",
        prop: "name",
        shown: true,
      },
      {
        name: "Study",
        prop: "studyName",
        shown: true,
      },
      {
        name: "City",
        prop: "city",
        shown: true,
      },
      {
        name: "Status",
        prop: "status",
        shown: true,
      },
      {
        name: "Start Date",
        prop: "startDate",
        shown: true,
        type:"date"
      },
      {
        name: "End Date",
        prop: "endDate",
        shown: true,
        type:"date"
      },
    ],
  };
