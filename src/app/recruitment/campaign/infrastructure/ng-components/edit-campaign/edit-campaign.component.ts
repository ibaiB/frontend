import {HttpClient} from '@angular/common/http';
import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
import {forkJoin} from 'rxjs';
import {ICandidate} from 'src/app/recruitment/candidate/domain/ICandidate';
import {CandidateMapper} from 'src/app/recruitment/candidate/infrastructure/CandidateMapper';
import {CandidateService} from 'src/app/recruitment/candidate/infrastructure/services/candidate.service';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {IList} from 'src/app/shared/generics/IList';
import {DynamicTableMarkIiComponent} from '../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {removeCandidateFromCampaign} from '../../../application/removeCandidateFromCampaign';
import {updateCampaign} from '../../../application/updateCampaign';
import {ICampaign} from '../../../domain/ICampaign';
import {IWebCampaign} from '../../../domain/IWebCampaign';
import {CampaignMapper} from '../../CampaignMapper';
import {IWebCandidate} from '../../../../candidate/domain/IWebCandidate';
import {IWebCollege} from '../../../../college/domain/IWebCollege';
import {IWebStudy} from '../../../../study/domain/IWebStudy';
import {CampaignService} from '../../services/campaign.service';
import {AddCandidateComponent} from '../add-candidate/add-candidate.component';
import {table} from './config';
import {errorAlert, successAlert} from '../../../../../shared/error/custom-alerts';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';

@Component({
    selector: 'app-edit-campaign',
    templateUrl: './edit-campaign.component.html',
    styleUrls: ['./edit-campaign.component.scss']
})
export class EditCampaignComponent implements OnInit {

    tableConfig: ITableConfig = table;
    public form: FormGroup;

    queryPagination;
    queryOrder;
    queryFilters;

    campaign: ICampaign;
    filtersValue: any;
    private colleges: IWebCollege[];
    private studies: IWebStudy[];
    private allCandidates: ICandidate[];
    canDelete = false;
    toDelete: any[] = [];
    baseTableOptions;
    tableOptions;
    public collegeOptions: string[] = [];
    public studyOptions: string[] = [];
    public statusOptions: string[] = ['No comenzada', 'Comenzada', 'Finalizada'];
    filters: IAuto[] = [];

    college: String;
    study: String;

    public loading = true;
    tableData: IList<ICandidate>;
    configVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private _campaignService: CampaignService,
        private campaignMapper: CampaignMapper,
        public _candidateService: CandidateService,
        public _candidateMapper: CandidateMapper,
        private _dialog: MatDialog,
        private _httpClient: HttpClient, //For dynamic table trial
        private _cookieService: CookieFacadeService
    ) {
    }


    ngOnInit(): void {
        // RESOLVER BRING JOINED COLLEGE STUDY AND STATUS
        const {error, data} = this.route.snapshot.data.response;

        if (data) {
            const {campaign, candidates, filtersValue, tableFiltersConfig} = data;

            this.campaign = campaign;
            this.filtersValue = filtersValue;

            this.baseTableOptions = {idCampaign: this.campaign.id};
            this.tableOptions = this.baseTableOptions;

            this._initLists(data);
            this._initOptions(data);
            this._formInit();
            this._buildFilters();
            parseTableFiltersConfig(tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = this.filtersValue ? this._clean((JSON.parse(this.filtersValue))) : {};
            this.tableConfig.pagination = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = tableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries(this._candidateMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
            this.tableData = candidates;

            this.loading = false;
        }
        this.configFilterValue();
    }

    configFilterValue() {
        let queryFilterAux = this.filtersValue ? this._clean(this._candidateMapper.mapTo(JSON.parse(this.filtersValue))) : {};

        this.filters.forEach(filter => {
            filter.defaultValue = queryFilterAux[this.getFilterProp(filter)];
            filter.value = queryFilterAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    private _initLists(data) {
        this.colleges = data.colleges.content;
        this.studies = data.studies.content;
        this.allCandidates = data.allCandidates.content;
    }

    private _initOptions(data) {
        this.collegeOptions = data.colleges.content.map(c => c.name);
        this.studyOptions = data.studies.content.map(s => s.name);
    }

    private _formInit() {
        // Id swap to name
        const college = this.colleges.find(c => c.id === this.campaign?.college?.id);
        const study = this.studies.find(c => c.id === this.campaign?.study?.id);

        this.college = college ? college.name : '';
        this.study = study ? study.name : '';

        this.form = this.fb.group({
            city: [this.campaign.city],
            contactMethod: [this.campaign.contactMethod],
            endDate: [this.campaign.endDate],
            name: [this.campaign.name, Validators.required],
            nhours: [this.campaign.nhours, [Validators.required, Validators.pattern('([0-9])*')]],
            placeGuaranteed: [this.campaign.placeGuaranteed],
            placeNotGuaranteed: [this.campaign.placeNotGuaranteed],
            startDate: [this.campaign.startDate],
            typeOfInternship: [this.campaign.typeOfInternship],
            weeklySchedule: [this.campaign.weeklySchedule],
            candidates: [this.campaign.candidates],
            idCollege: [this.college],
            idStudy: [this.study],
            status: [this.campaign.status],
        });
    }

    public autocompleteSelected(event: { formField: string, value: string }) {
        this.form.controls[event.formField].setValue(event.value);
    }

    private _switch() {
        this.form.value['idCollege'] = this.colleges.find(c => c.name === this.form.value['idCollege'])?.id ?? this.campaign.college?.id;
        this.form.value['idStudy'] = this.studies.find(e => e.name === this.form.value['idStudy'])?.id ?? this.campaign.study?.id;
    }

    public onSaveClick() {
        if (this.form.valid) {
            this._switch();
            const campaign: IWebCampaign = {id: this.campaign.id, ...this.form.value};
            updateCampaign(campaign, this._campaignService, this.campaignMapper)
                .subscribe(response => {
                    successAlert('');
                });
        }
    }

    backSearch() {
        this._candidateService.search({...this.queryFilters, ...this.baseTableOptions, ...this.queryPagination, ...this.queryOrder})
            .subscribe(result => {
                    this.tableData = {
                        content: result.content.map(this._candidateMapper.mapTo),
                        totalElements: result.totalElements,
                        numberOfElements: result.numberOfElements,
                        totalPages: result.totalPages
                    };
                },
                (error) => {
                    console.error('Error on back search', error);
                    const {error: {message, id}} = error;
                    errorAlert(`Something went wrong!`, message, id);
                });
    }

    selected(event: IWebCandidate[]) {
        this.canDelete = event.length > 0;
        this.toDelete = event;
    }

    delete(event) {
        event.stopPropagation();
        let deleting = [];
        this.toDelete.forEach(candidate => {
            const deleteCandidate = removeCandidateFromCampaign(this.campaign.id, candidate.id, this._campaignService);
            deleting.push(deleteCandidate);
        });

        forkJoin(deleting).subscribe(
            () => {
                successAlert('Candidates were removed from campaign')
                    .then(() => {
                        this.backSearch();
                        this.canDelete = false;
                    });
            }
        );
    }

    addCandidateDialog(event) {
        event.stopPropagation();
        const dialogRef = this._dialog.open(AddCandidateComponent, {
            data: {campaign: this.campaign, allCandidates: this.allCandidates}
        });

        dialogRef.afterClosed().subscribe(() => {
            this.backSearch();
        });
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const mapped = this._candidateMapper.mapFrom(params);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this._cookieService.save('editCampaignFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieService.save('edit-campaign-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieService.save('edit-campaign-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        let aux = {[event.orderField]: ''};
        aux = this._candidateMapper.mapFrom(aux);

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieService.save('edit-campaign-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieService.save('edit-campaign-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Id',
                placeholder: 'Id',
                shown: true
            },
            {
                options: [],
                prop: 'name',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Name',
                placeholder: 'Name',
                shown: true
            },
            {
                options: [],
                prop: 'surname',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Surname',
                placeholder: 'Surname',
                shown: true
            },
            {
                options: [],
                prop: 'email',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Email',
                placeholder: 'Email',
                shown: true
            },
            {
                options: this.studyOptions,
                prop: '',
                retProp: '',
                searchValue: 'studies',
                type: 'autocomplete',
                appearance: 'outline',
                class: 'autocomplete',
                label: 'Studies',
                placeholder: '',
                shown: false
            }
        ];
    }

}
