import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
  multiSelect: true,
  columns: [
    {
      name: 'Id',
      prop: 'id',
      shown: true,
      route: './app/recruitment/candidates/candidate-detail',
      cellClass: 'id-table-column',
      routeId: 'id'
    },
    {
      name: "Name",
      prop: "name",
      shown: true,
    },
    {
      name: "Surname",
      prop: "surname",
      shown: true,
    },
    {
      name: "Email",
      prop: "email",
      shown: true,
    },
    {
      name: "Studies",
      prop: "studies",
      shown: true,
    },
  ],
};
