import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CampaignTimetableComponent} from './campaign-timetable.component';

describe('CampaignTimetableComponent', () => {
  let component: CampaignTimetableComponent;
  let fixture: ComponentFixture<CampaignTimetableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CampaignTimetableComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignTimetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
