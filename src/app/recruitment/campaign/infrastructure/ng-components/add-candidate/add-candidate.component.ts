import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {forkJoin} from 'rxjs';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {addCandidateToCampaign} from '../../../application/addCandidateToCampaign';
import {IWebCampaign} from '../../../domain/IWebCampaign';
import {IWebCandidate} from '../../../../candidate/domain/IWebCandidate';
import {CampaignService} from '../../services/campaign.service';

@Component({
    selector: 'app-add-candidate',
    templateUrl: './add-candidate.component.html',
    styleUrls: ['./add-candidate.component.scss']
})
export class AddCandidateComponent implements OnInit {

    public form: FormGroup;
    public readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    selected: IWebCandidate[] = [];
    candidates: string[] = [];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: { campaign: IWebCampaign, allCandidates: IWebCandidate[] },
        public dialogRef: MatDialogRef<AddCandidateComponent>,
        private _fb: FormBuilder,
        private _campaignService: CampaignService,
    ) {
    }

    ngOnInit(): void {
        this.data.allCandidates.forEach((candidate) => {
            this.candidates.push(`${candidate.name} ${candidate.surname}, ${candidate.id}`);
        });

        this.formInit();
    }

    private formInit(): void {
        this.form = this._fb.group({
            email: ['', Validators.email],
            candidate: [''],
            campaign: [this.data.campaign.name],
        });
    }

    public autocompleteSelected(selectedRow: string) {
        if (selectedRow) {
            const splitedRow = selectedRow.split(',');
            const id = splitedRow[1].trim();
            const candidate = this.data.allCandidates.find((c) => c.id === id);
            if (candidate) {
                this.selected.push(candidate);
            }
        }
    }

    public remove(candidate: any): void {
        const index = this.selected.indexOf(candidate);
        if (index > -1) {
            this.selected.splice(index, 1);
        }
    }

    public onNoClick(): void {
        this.dialogRef.close();
    }

    public onCancelClick(): void {
        this.dialogRef.close();
    }

    public onSaveClick(): void {
        const addedCandidates = [];
        this.selected.forEach(candidate => {
            const addEmail = addCandidateToCampaign(this.data.campaign.id, candidate.id, this._campaignService);
            addedCandidates.push(addEmail);
        });

        forkJoin(addedCandidates).subscribe(
            () => {
                successAlert('All candidates where correctly added')
                    .then(() => this.dialogRef.close());
            }
        );
    }
}
