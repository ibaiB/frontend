import {Injectable} from '@angular/core';
import {ICampaign} from '../domain/ICampaign';

@Injectable({
    providedIn: 'any',
})
export class CampaignMapper {
  mapTo(params: any): ICampaign {
    const {
      id,
      city,
      contactMethod,
      endDate,
      idCollege,
      idStudy,
      college,
      study,
      name,
      nhours,
      placeGuaranteed,
      placeNotGuaranteed,
      startDate,
      status,
      typeOfInternship,
      weeklySchedule,
      candidates,
    } = params;

    return {
      id: id,
      city: city,
      contactMethod: contactMethod,
      endDate: endDate,
      idCollege: idCollege,
      idStudy: idStudy,
      college: college,
      study: study,
      name: name,
      nhours: nhours,
      placeGuaranteed: placeGuaranteed,
      placeNotGuaranteed: placeNotGuaranteed,
      startDate: startDate,
      status: status,
      typeOfInternship: typeOfInternship,
      weeklySchedule: weeklySchedule,
      candidates: candidates,
    };
  }

  mapFrom(params: any) {
    const {
      id,
      city,
      contactMethod,
      endDate,
      idCollege,
      idStudy,
      name,
      nhours,
      placeGuaranteed,
      placeNotGuaranteed,
      startDate,
      status,
      typeOfInternship,
      weeklySchedule,
      candidates,
    } = params;

    return {
      id: id,
      city: city,
      contactMethod: contactMethod,
      endDate: endDate,
      idCollege: idCollege,
      idStudy: idStudy,
      name: name,
      nhours: nhours,
      placeGuaranteed: placeGuaranteed,
      placeNotGuaranteed: placeNotGuaranteed,
      startDate: startDate,
      status: status,
      typeOfInternship: typeOfInternship,
      weeklySchedule: weeklySchedule,
      candidates: candidates,
    };
  }
}
