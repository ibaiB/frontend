import {IWebCandidate} from './IWebCandidate';

export interface IWebCandidateList {
    content: IWebCandidate[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
