import {ICandidateTableData} from './ICandidateTableData';

export interface ICandidateTableResolved {
    tableData: ICandidateTableData;
    error?: any;
}
