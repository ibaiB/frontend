import {IWebCategory} from 'src/app/internal/category/domain/IWebCategory';

export interface IWebCandidate {
    id?: string;
    adaptability: number;
    autonomy: number;
    city: string;
    commitment: number;
    emails: string[];
    english: string;
    geographicAvailability: boolean;
    idCampaign: string;
    idCollege: string;
    idTechnologies: string[];
    interviewDate: string;
    knowledge: string;
    motivationJob: string;
    name: string;
    objectiveOriented: number;
    others: string;
    salaryEstimate: string;
    studies: string;
    studiesInCurse: string;
    surname: string;
    teamwork: number;
    yearsOfExperience: number;
    candidateTechnologies: any;
    candidateEmails: ICandidateEmail[];
    employeeTechnicalInterviewer: any;
    idEmployeeTechnicalInterviewer: string;
    categoryProfileJobPosition: IWebCategory;
    idCategoryProfileJobPosition: string;
    otherLanguages: string;
    recruitmentParticipation: boolean;
}

export interface ICandidateEmail {
    id: string;
    email: string;
}
