import {ICandidate} from './ICandidate';
import {IList} from '../../../shared/generics/IList';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';

export interface ICandidateTableData {
    candidates: IList<ICandidate>;
    filtersValue: any;
    tableFiltersConfig: ITableFiltersConfig
}
