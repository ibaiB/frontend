import {INewCandidateData} from './INewCandidateData';

export interface INewCandidateResolved {
    data: INewCandidateData;
    error?: any;
}
