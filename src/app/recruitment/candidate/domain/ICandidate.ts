import {ICategory} from 'src/app/internal/category/domain/ICategory';
import {IPublicEmployee} from '../../../internal/employee/domain/IEmployee';

export interface ICandidate {
    id?: string;
    idChat?: string;
    idFileManager?: string;

    adaptability: number;
    autonomy: number;
    city: string;
    commitment: number;
    email: string[];
    english: string;
    geographicAvailability: boolean;
    idCampaign: string;
    idCollege: string;
    idTechnologies: string[];
    interviewDate: string;
    knowledge: string;
    motivationJob: string;
    name: string;
    objectiveOriented: number;
    others: string;
    salaryEstimate: string;
    studies: string;
    studiesInCurse: string;
    surname: string;
    teamwork: number;
    yearsOfExperience: number;
    candidateTechnologies: any;
    candidateEmails: ICandidateEmail[];
    employeeTechnicalInterviewer: IPublicEmployee;
    idEmployeeTechnicalInterviewer: string;
    categoryProfileJobPosition: ICategory;
    idCategoryProfileJobPosition: string;
    otherLanguages: string;
    recruitmentParticipation: boolean;
}

export interface ICandidateEmail {
    id: string;
    email: string;
}
