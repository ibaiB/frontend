import {IWebCollegeList} from '../../college/domain/IWebCollegeList';
import {IDictionaryValue} from 'src/app/shared/entities/dictionary/domain/IDictionaryValue';
import {IWebTechnologyList} from '../../../internal/technology/domain/IWebTechnologyList';
import {IWebStudyList} from '../../study/domain/IWebStudyList';
import {IList} from 'src/app/shared/generics/IList';
import {IEmployee} from '../../../internal/employee/domain/IEmployee';

export interface INewCandidateData {
    colleges: IWebCollegeList;
    cars: IDictionaryValue[];
    employees: IList<IEmployee>;
    technologies: IWebTechnologyList;
    studies: IWebStudyList;
}
