import {IList} from 'src/app/shared/generics/IList';
import {IWebCarList} from '../../../internal/car/domain/IWebCarList';
import {IWebCollegeList} from '../../college/domain/IWebCollegeList';
import {IWebStudyList} from '../../study/domain/IWebStudyList';
import {IWebTechnologyList} from '../../../internal/technology/domain/IWebTechnologyList';
import {IWebCandidate} from './IWebCandidate';
import {IPublicEmployee} from '../../../internal/employee/domain/IEmployee';

export interface ICandidateDetail {
    candidate: IWebCandidate;
    studies: IWebStudyList;
    colleges: IWebCollegeList;
    technologies: IWebTechnologyList;
    employees: IList<IPublicEmployee>;
    cars: IWebCarList;

}
