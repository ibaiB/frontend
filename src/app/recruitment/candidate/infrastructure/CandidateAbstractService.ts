import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';
import {ICandidate} from '../domain/ICandidate';

import {ICandidateEmail, IWebCandidate} from '../domain/IWebCandidate';

export abstract class CandidateAbstractService {
  abstract create(candidate: IWebCandidate): Observable<IWebCandidate>;

  abstract update(candidate: IWebCandidate): Observable<IWebCandidate>;

  abstract delete(id: string): Observable<any>;

  abstract get(id: string): Observable<IWebCandidate>;

  abstract search(query: any): Observable<IList<ICandidate>>;

  abstract getCandidateEmail(idCandidate: string): Observable<ICandidateEmail>;
}
