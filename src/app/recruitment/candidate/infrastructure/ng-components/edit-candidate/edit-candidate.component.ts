import {COMMA, ENTER, SPACE} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidatorFn} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2';
import {ResolvedData} from '../../../../../shared/generics/IResolvedData';
import {ICandidate} from '../../../domain/ICandidate';
import {ICandidateDetail} from '../../../domain/ICandidateDetail';
import {CandidateService} from '../../services/candidate.service';
import {CandidateMapper} from '../../CandidateMapper';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {IAuto} from 'src/app/shared/custom-mat-elements/dynamic';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {getCategories} from 'src/app/internal/category/application/getCategories';
import {CategoryService} from 'src/app/internal/category/infrastructure/services/category.service';
import {ICategory} from 'src/app/internal/category/domain/ICategory';
import {CategoryMapper} from 'src/app/internal/category/infrastructure/CategoryMapper';
import {createCandidate} from 'src/app/recruitment/candidate/application/createCandidate';
import {updateCandidate} from 'src/app/recruitment/candidate/application/updateCandidate';
import {EmployeeService} from '../../../../../internal/employee/infrastructure/services/employee.service';
import {EmployeeMapper} from '../../../../../internal/employee/infrastructure/EmployeeMapper';
import {searchEmployee} from '../../../../../internal/employee/application/searchEmployee';
import {IPublicEmployee} from '../../../../../internal/employee/domain/IEmployee';

@Component({
    selector: 'app-edit-candidate',
    templateUrl: './edit-candidate.component.html',
    styleUrls: ['./edit-candidate.component.scss'],
})
export class EditCandidateComponent implements OnInit {

    idChat: string;
    idStorage: string;
    chatHidden = true;
    storageHidden = true;

    private _resolved: ResolvedData<ICandidateDetail>;

    candidate: ICandidate = null;
    formCandidate: FormGroup;

    collegeOptions: string[];
    studyOptions: string[];
    collegeName: string;
    sectorKnowledge: string;
    motivationJob: string = '';
    emails: string[] = [];
    techInterviewer: IPublicEmployee;
    techInterviewerControl: FormControl = new FormControl('');
    profileJobPosition: ICategory;
    profileJobPositionControl: FormControl = new FormControl('');
    techProfile1: string;
    techProfile2: string;
    techProfile3: string;

    adaptability: number = 1;
    teamwork: number = 1;
    autonomy: number = 1;
    objectiveOriented: number = 1;
    commitment: number = 1;
    overallRating: number = 1;

    width = '100%';
    allCarOptions: string[] = [];
    technologyOptions: string[] = [];

    cities: string[] = [
        'LOGROÑO',
        'ZARAGOZA',
        'BILBAO',
        'MADRID',
        'JAÉN',
        'PAMPLONA',
    ];

    // Material chips config
    selectable = true;
    removable = true;
    showError = false;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];

    // Other languages config
    allLanguages = [
        'English',
        'French',
        'German'
    ];
    languagesLevel = ['NO', 'B1', 'B2', 'C1', 'C2'];
    languages = [];
    filteredLanguages: Observable<string[]>;
    languageCtrl = new FormControl();
    @ViewChild('languageInput') languageInput: ElementRef<HTMLInputElement>;
    @ViewChild('auto') matAutocomplete: MatAutocomplete;

    // Autoselect config
    techInterviewerConfig: IAuto = {
        options: [],
        prop: 'virtualName',
        retProp: 'id',
        searchValue: 'name',
        class: 'autoselect',
        appearance: 'outline',
        label: 'Technical interviewer',
        placeholder: 'Technical interviewer',
        type: 'autoselect',
        form: this.techInterviewerControl
    };
    profileJobPositionConfig: IAuto = {
        options: [],
        prop: 'name',
        retProp: 'id',
        searchValue: 'name',
        class: 'autoselect',
        appearance: 'outline',
        label: 'Profile job position',
        placeholder: 'Profile job position',
        type: 'autoselect',
        form: this.profileJobPositionControl
    };

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _fb: FormBuilder,
        private _candidateService: CandidateService,
        private _candidateMapper: CandidateMapper,
        private _employeeService: EmployeeService,
        private _employeeMapper: EmployeeMapper,
        private _categoryService: CategoryService,
        private _categoryMapper: CategoryMapper
    ) {
        this.filteredLanguages = this.languageCtrl.valueChanges.pipe(
            startWith(null),
            map((language: string | null) => language ? this._filterLanguages(language) : this.allLanguages.slice()));
    }

    ngOnInit(): void {
        const {data, error, message} = this._route.snapshot.data['response'];
        this._resolved = this._route.snapshot.data['response'];

        if (!data) {
            return;
        }

        this.collegeOptions = data.colleges.content.map(college => college.name);
        this.studyOptions = data.studies.content.map(study => study.name);
        this.technologyOptions = data.technologies.content.map(tech => tech.name);
        this.allCarOptions = data.cars.content.map(car => car.name);

        if (data.candidate) {
            this.candidate = this._candidateMapper.mapTo(data.candidate);
            this.collegeName = this.candidate.idCollege ? data.colleges.content.find(college => college.id = this.candidate.idCollege)?.name : '';
            this.motivationJob = this.candidate.motivationJob;
            this.profileJobPosition = this.candidate.categoryProfileJobPosition;

            this.techProfile1 = this.candidate.candidateTechnologies[0]?.technology.name ?? null;
            this.techProfile2 = this.candidate.candidateTechnologies[1]?.technology.name ?? null;
            this.techProfile3 = this.candidate.candidateTechnologies[2]?.technology.name ?? null;

            this.adaptability = this.candidate.adaptability;
            this.teamwork = this.candidate.teamwork;
            this.autonomy = this.candidate.autonomy;
            this.objectiveOriented = this.candidate.objectiveOriented;
            this.commitment = this.candidate.commitment;

            this.sectorKnowledge = this.candidate.knowledge
                ? data.cars.content.find(car => car.id === this.candidate.knowledge)?.name ?? ''
                : '';

            this.idChat = this.candidate.idChat;
            this.idStorage = this.candidate.idFileManager;
        }

        this.actualizarMedia();
        this.colors();
        this._initForm(this.candidate);
        this._initAutoselects();
    }

    private _initForm(candidate?: ICandidate) {
        this.emails = candidate?.candidateEmails.map(ce => ce.email) ?? [];
        this.languages = candidate?.otherLanguages ? candidate.otherLanguages.split(',') : [];

        this.formCandidate = this._fb.group({
            name: this._createControlConfig(candidate, false, candidate?.name),
            surname: this._createControlConfig(candidate, false, candidate?.surname),
            emails: this._createControlConfig(candidate, false, this.emails),
            studiesInCurse: this._createControlConfig(candidate, false, candidate?.studiesInCurse),
            idCollege: this._createControlConfig(candidate, false, candidate?.idCollege),
            city: this._createControlConfig(candidate, false, candidate?.city),
            interviewDate: this._createControlConfig(candidate, false, candidate?.interviewDate),
            technicalInterviewer: this._createControlConfig(candidate, false, candidate?.employeeTechnicalInterviewer),
            salaryEstimate: this._createControlConfig(candidate, false, candidate?.salaryEstimate),
            yearsOfExperience: this._createControlConfig(candidate, false, candidate?.yearsOfExperience),
            profileJobPosition: this._createControlConfig(candidate, false, candidate?.categoryProfileJobPosition),
            geographicAvailability: this._createControlConfig(candidate, false, candidate?.geographicAvailability),
            studies: this._createControlConfig(candidate, false, candidate?.studies),
            motivationJob: this._createControlConfig(candidate, false, candidate?.motivationJob),
            english: this._createControlConfig(candidate, false, candidate?.english),
            otherLanguages: this._createControlConfig(candidate, false, this.languages),
            technologicalProfile1: this._createControlConfig(candidate, false, this.techProfile1),
            technologicalProfile2: this._createControlConfig(candidate, false, this.techProfile2),
            technologicalProfile3: this._createControlConfig(candidate, false, this.techProfile3),
            others: this._createControlConfig(candidate, false, candidate?.others),
            car: [],
            adaptability: this._createControlConfig(candidate, false, candidate?.adaptability),
            teamwork: this._createControlConfig(candidate, false, candidate?.teamwork),
            autonomy: this._createControlConfig(candidate, false, candidate?.autonomy),
            objectiveOriented: this._createControlConfig(candidate, false, candidate?.objectiveOriented),
            commitment: this._createControlConfig(candidate, false, candidate?.commitment),
            overallRating: [],
            knowledge: this._createControlConfig(candidate, false, candidate?.knowledge),
            recruitmentParticipation: this._createControlConfig(candidate, false, candidate?.recruitmentParticipation),
        });
    }

    private _createControlConfig(entity: any, disabled: boolean, value: any, validator?: ValidatorFn) {
        const config = [!entity ? '' : (disabled ? {value: value ?? '', disabled: true} : (value ?? ''))];
        if (validator) {
            config.push(validator);
        }
        return config;
    }

    private _initAutoselects() {
        if (this.candidate?.categoryProfileJobPosition) {
            this.profileJobPosition = this._categoryMapper.mapTo(this.candidate.categoryProfileJobPosition);
            this.profileJobPositionControl.setValue(this.profileJobPosition.name);
        }
        if (this.candidate?.employeeTechnicalInterviewer) {
            this.techInterviewer = this.candidate.employeeTechnicalInterviewer;
            this.techInterviewerControl.setValue(this.techInterviewer.name + ' ' + this.techInterviewer.surname);
        }
    }

    private _candidateSwitcher(): ICandidate {
        const collegeValue = this.formCandidate.get('idCollege').value;
        const knowledgeValue = this.formCandidate.get('knowledge').value;
        const collegeFiltered = this._resolved.data.colleges.content.find(college => college.name === collegeValue);
        const knowledgeFiltered = this._resolved.data.cars.content.find(car => car.name === knowledgeValue);
        const otherLanguages = this.languages;
        this.formCandidate.get('emails').setValue(this.emails);

        // Technological profile
        const technologicalProfile1Value = this.formCandidate.get('technologicalProfile1').value;
        const technologicalProfile2Value = this.formCandidate.get('technologicalProfile2').value;
        const technologicalProfile3Value = this.formCandidate.get('technologicalProfile3').value;
        const technologicalProfile1Filtered = this._resolved.data.technologies.content.find(tech =>
            tech.name === technologicalProfile1Value);
        const technologicalProfile2Filtered = this._resolved.data.technologies.content.find(tech =>
            tech.name === technologicalProfile2Value);
        const technologicalProfile3Filtered = this._resolved.data.technologies.content.find(tech =>
            tech.name === technologicalProfile3Value);
        const technologicalProfile = [];
        for (const techProfile of [technologicalProfile1Filtered, technologicalProfile2Filtered, technologicalProfile3Filtered]) {
            if (techProfile) {
                technologicalProfile.push(techProfile.id);
            }
        }

        // Non-technical aspects
        this.formCandidate.get('adaptability').setValue(this.adaptability);
        this.formCandidate.get('teamwork').setValue(this.teamwork);
        this.formCandidate.get('autonomy').setValue(this.autonomy);
        this.formCandidate.get('objectiveOriented').setValue(this.objectiveOriented);
        this.formCandidate.get('commitment').setValue(this.commitment);

        return {
            ...this.formCandidate.value,
            knowledge: knowledgeFiltered ? knowledgeFiltered.id : null,
            idTechnologies: technologicalProfile,
            idCollege: collegeFiltered ? collegeFiltered.id : null,
            idEmployeeTechnicalInterviewer: this.techInterviewer?.id ?? null,
            idCategoryProfileJobPosition: this.profileJobPosition?.id ?? null,
            otherLanguages: otherLanguages.join(',') ?? null
        };
    }

    save() {
        if (!this.formCandidate.valid || this.emails.length === 0) {
            Swal.fire({
                icon: 'error',
                title: 'Ooops...!',
                text: 'Some fields are not filled',
                confirmButtonColor: '#db5e5e'
            });
            return;
        }

        const full: ICandidate = this._candidateSwitcher();

        const $obs: Observable<any> = this.candidate
            ? updateCandidate({...full, id: this.candidate.id}, this._candidateService, this._candidateMapper)
            : createCandidate(full, this._candidateService, this._candidateMapper);

        $obs.subscribe(
            (result) => {
                successAlert('Candidate saved.').then(() => {
                    if (!this.candidate) {
                        this._router.navigate(['./app/recruitment/candidates']);
                    }
                });
            }
        );
    }

    /* AUTOSELECT & AUTOCOMPLETE LOGIC */

    autocompleteSelected(event: { formField: string, value: string }) {
        if (event.formField === 'technicalInterviewer') {
            if (event.value) {
                this.techInterviewer = this._employeeMapper.mapPublic(event.value);
            } else {
                this.techInterviewer = null;
            }
        }
        if (event.formField === 'profileJobPosition') {
            if (event.value) {
                this.profileJobPosition = this._categoryMapper.mapTo(event.value);
            } else {
                this.profileJobPosition = null;
            }
        }
        this.formCandidate.controls[event.formField].setValue(event.value);
    }

    changeAutoselectOptions(event: { searchValue: string, value: any }) {
        if (event.searchValue === 'idEmployeeTechnicalInterviewer') {
            searchEmployee({nameSurname: event.value, status: true}, this._employeeService).subscribe(employees => {
                const emps: any[] = [];
                employees.content.forEach(employee => {
                    employee['virtualName'] = employee.name + ' ' + employee.surname;
                    emps.push(employee);
                });
                this.techInterviewerConfig = {...this.techInterviewerConfig, options: emps};
            });
        }

        if (event.searchValue === 'profileJobPosition') {
            getCategories({name: event.value}, this._categoryService).subscribe(categories => {
                this.profileJobPositionConfig = {...this.profileJobPositionConfig, options: categories.content};
            });
        }
    }

    /* SPECIAL FIELDS LOGIC */

    motivationChanged(motivation) {
        this.motivationJob = motivation;
        this.formCandidate.get('motivationJob').setValue(motivation);
    }

    addEmail(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        this.showError = true;
        if ((value || '').trim()) {
            if (!this.emails.includes(value.trim())) {
                this.emails.push(value.trim());
            }
        }
        if (input) {
            input.value = '';
        }
    }

    removeEmail(email: string): void {
        const index = this.emails.indexOf(email);
        if (index >= 0) {
            this.emails.splice(index, 1);
        }
    }

    addLanguage(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        if ((value || '').trim()) {
            if (this.allLanguages.includes(value.trim())
                && !this.languages.includes(value.trim())) {
                this.languages.push(value.trim());
            }
        }
        if (input) {
            input.value = '';
        }
        this.languageCtrl.setValue(null);
    }

    selectedLanguage(event: MatAutocompleteSelectedEvent): void {
        this.addLanguage({
            input: this.languageInput.nativeElement,
            value: event.option.viewValue
        });
    }

    removeLanguage(language: string): void {
        const index = this.languages.indexOf(language);
        if (index >= 0) {
            this.languages.splice(index, 1);
        }
    }

    private _filterLanguages(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.allLanguages.filter(lang => lang.toLowerCase().indexOf(filterValue) === 0);
    }

    ratingChanged(type: string, event: number) {
        switch (type) {
            case 'adaptability':
                this.adaptability = event;
                break;
            case 'teamwork':
                this.teamwork = event;
                break;
            case 'autonomy':
                this.autonomy = event;
                break;
            case 'objectiveOriented':
                this.objectiveOriented = event;
                break;
            case 'commitment':
                this.commitment = event;
                break;
        }
        this.actualizarMedia();
    }

    actualizarMedia() {
        const sum =
            +this.adaptability +
            +this.teamwork +
            +this.autonomy +
            +this.objectiveOriented +
            +this.commitment;

        const promedio: number = Math.round(sum / 5);
        this.overallRating = promedio !== 0 ? promedio : 1;
        const checkeable = document.getElementById(this.overallRating.toString());
        const radios = document.getElementsByName('medias');
        radios.forEach((radio) => {
            radio.removeAttribute('checked');
        });
        if (checkeable) checkeable.setAttribute('checked', '');
    }

    colors() {
        this.darkblue();
        this.purple();
        this.blue();
        this.darkblue();
        this.green();
    }

    darkblue() {
        return this.motivationJob ? this.motivationJob.includes('GEOGRAPHICAL_MOBILITY') : false;
    }

    purple() {
        return this.motivationJob ? this.motivationJob.includes('OTHERS') : false;
    }

    blue() {
        return this.motivationJob ? this.motivationJob.includes('MANAGEMENT') : false;
    }

    darkgreen() {
        return this.motivationJob ? this.motivationJob.includes('ECONOMIC') : false;
    }

    green() {
        return this.motivationJob ? this.motivationJob.includes('PROJECT') : false;
    }

    setChatVisibility(visibility: boolean) {
        this.chatHidden = !visibility;
    }

    setStorageVisibility(visibility: boolean) {
        this.storageHidden = !visibility;
    }
}
