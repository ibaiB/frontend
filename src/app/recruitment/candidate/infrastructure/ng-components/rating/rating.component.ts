import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: "app-rating",
  templateUrl: "./rating.component.html",
  styleUrls: ["./rating.component.scss"],
})
export class RatingComponent implements OnInit {
  // @Input() public color: string = null;
  // @Input() public rate: string = null;
  //@Input() candidate: Candidate;
  @Input() valor: number;
  @Input() public rate: string = null;
  @Output()
  ratings = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  selectedRating(ev) {
    var target = ev.target;

    this.ratings.emit(target.value);
  }
}
