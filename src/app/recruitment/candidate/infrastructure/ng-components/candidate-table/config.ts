import {IAuto, ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
    multiSelect: false,
    actions: [
        {name: 'New Employee Request', icon: 'send',}
    ],
    columns: [
        {
            name: 'Id',
            prop: 'id',
            shown: true,
            route: './app/recruitment/candidates/candidate-detail/',
            cellClass: 'id-table-column',
            routeId: 'id'
        },
      {
        name: "Name",
        prop: "name",
        shown: true,
      },
      {
        name: "Surname",
        prop: "surname",
        shown: true,
      },
      {
        name: "Email",
        prop: "email",
        shown: true,
      },
      {
        name: "Studies",
        prop: "studies",
        shown: true,
      },
    ]
  };

  export const filters: IAuto[] = [
    {
        options: [],
        prop: 'id',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Id',
        placeholder: 'Id',
        shown: true,
    },
    {
        options: [],
        prop: 'name',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Name',
        placeholder: 'Name',
        shown: true,
    },
    {
        options: [],
        prop: 'surname',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Surname',
        placeholder: 'Surname',
        shown: true,
    },
    {
        options: [],
        prop: 'email',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Email',
        placeholder: 'Email',
    },
    {
        options: [],
        prop: 'studies',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Studies',
        placeholder: 'Studies',
        shown: true,
    },
];
