import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from '../../../../../shared/custom-mat-elements/dynamic';
import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CandidateMapper} from 'src/app/recruitment/candidate/infrastructure/CandidateMapper';
import {searchCandidates} from '../../../application/searchCandidates';
import {CandidateService} from '../../services/candidate.service';
import {filters, table} from './config';
import {IList} from '../../../../../shared/generics/IList';
import {DynamicTableMarkIiComponent} from '../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {ICandidateTableResolved} from 'src/app/recruitment/candidate/domain/ICandidateTableResolved';
import {ICandidate} from 'src/app/recruitment/candidate/domain/ICandidate';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';

@Component({
    selector: 'app-candidate-table',
    templateUrl: './candidate-table.component.html',
    styleUrls: ['./candidate-table.component.scss']
})
export class CandidateTableComponent implements OnInit {

    private _resolved: ICandidateTableResolved;

    tableConfig: ITableConfig = table;
    filters: IAuto[] = filters;
    tableData: IList<any>;
    queryPagination = {page: 0, size: 10};
    queryOrder = {orderField: '', order: ''};
    queryFilters = {};
    configVisibility: boolean = false;

    loading: boolean = true;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        private _candidateService: CandidateService,
        private _candidateMapper: CandidateMapper,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit() {
        this._resolved = this._route.snapshot.data['response'];

        if (this._resolved.tableData) {
            parseTableFiltersConfig(this._resolved.tableData.tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = this._resolved.tableData.filtersValue ? this._clean((JSON.parse(this._resolved.tableData.filtersValue))) : {};
            this.tableConfig.pagination = this._resolved.tableData.tableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = this._resolved.tableData.tableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries(this._candidateMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
            this.tableData = this._resolved.tableData.candidates;
            this.loading = false;
        }
        this.configFilterValue();
    }

    configFilterValue() {
        let queryFilterAux = this._resolved.tableData.filtersValue ? this._clean(this._candidateMapper.mapTo(JSON.parse(this._resolved.tableData.filtersValue))) : {};

        this.filters.forEach(filter => {
            filter.defaultValue = queryFilterAux[this.getFilterProp(filter)];
            filter.value = queryFilterAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    backSearch() {
        searchCandidates({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._candidateService, this._candidateMapper)
            .subscribe(response => {
                this.tableData = {
                    content: response.content,
                    totalElements: response.totalElements,
                    numberOfElements: response.numberOfElements,
                    totalPages: response.totalPages
                };
            });
    }

    navigate(event: { id: string, property: string }) {
        if (event.property === 'id') {
            this._router.navigate([`./app/candidate-detail/${event.id}`], {relativeTo: this._route});
        }
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const mapped = this._candidateMapper.mapFrom(params);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('candidateFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this.dynamicTable.resetPageIndex();
        this._cookieFacade.save('candidate-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('candidate-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        let aux = {[event.orderField]: ''};
        aux = this._candidateMapper.mapFrom(aux);

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('candidate-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    doAction(event: { action: string, item: ICandidate }) {
        if (event.action === 'New Employee Request') {
            this._router.navigate(['./app/internal/employee/new-requests/fromCandidate/' + event.item.id], {state: {data: event.item}});
        }
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('candidate-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }
}
