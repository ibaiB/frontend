import {Injectable} from '@angular/core';
import {IList} from '../../../shared/generics/IList';
import {ICandidate} from '../domain/ICandidate';


//TODO hay que revisar esto porque las ñapas que estoy metiendo no son ni medio normales
@Injectable({
    providedIn: 'any',
})
export class CandidateMapper {
    mapTo(params: any) {
        const {
            id,
            adaptability,
            autonomy,
            city,
            commitment,
            emails,
            email,
            english,
            geographicAvailability,
            idCampaign,
            idCollege,
            idTechnologies,
            interviewDate,
            knowledge,
            motivationJob,
            name,
            objectiveOriented,
            others,
            salaryEstimate,
            studies,
            studiesInCurse,
            surname,
            teamwork,
            yearsOfExperience,
            candidateTechnologies,
            candidateEmails,
            employeeTechnicalInterviewer,
            categoryProfileJobPosition,
            idEmployeeTechnicalInterviewer,
            idCategoryProfileJobPosition,
            profileJobPosition,
            otherLanguages,
            recruitmentParticipation,
            idChat,
            idFileManager
        } = params;

        return {
            id: id,
            adaptability: adaptability,
            autonomy: autonomy,
            city: city,
            commitment: commitment,
            email: candidateEmails ? [CandidateMapper._candidateEmailsToString(candidateEmails)] : email, // TODO: Delete
            english: english,
            geographicAvailability: geographicAvailability,
            idCampaign: idCampaign,
            idCollege: idCollege,
            idTechnologies: idTechnologies,
            interviewDate: interviewDate,
            knowledge: knowledge,
            motivationJob: motivationJob,
            name: name,
            objectiveOriented: objectiveOriented,
            others: others,
            salaryEstimate: salaryEstimate,
            studies: studies,
            studiesInCurse: studiesInCurse,
            surname: surname,
            teamwork: teamwork,
            yearsOfExperience: yearsOfExperience,
            candidateTechnologies: candidateTechnologies,
            candidateEmails: candidateEmails,
            employeeTechnicalInterviewer: employeeTechnicalInterviewer,
            categoryProfileJobPosition: categoryProfileJobPosition,
            idEmployeeTechnicalInterviewer: idEmployeeTechnicalInterviewer,
            idCategoryProfileJobPosition: idCategoryProfileJobPosition,
            profileJobPosition: profileJobPosition,
            otherLanguages: otherLanguages,
            recruitmentParticipation,
            idChat,
            idFileManager
        };
    }

    private static _candidateEmailsToString(cs: any[]) {
        let emails = cs.map(c => c.email);
        return emails.join(', ');
    }

    mapFrom(params: any) {
        const {
            id,
            adaptability,
            autonomy,
            city,
            commitment,
            emails,
            english,
            geographicAvailability,
            idCampaign,
            idCollege,
            idTechnologies,
            interviewDate,
            knowledge,
            motivationJob,
            name,
            objectiveOriented,
            others,
            otherLanguages,
            salaryEstimate,
            studies,
            studiesInCurse,
            surname,
            teamwork,
            yearsOfExperience,
            candidateTechnologies,
            candidateEmails,
            employeeTechnicalInterviewer,
            idEmployeeTechnicalInterviewer,
            categoryProfileJobPosition,
            idCategoryProfileJobPosition,
            email,
            recruitmentParticipation
        } = params;

        return {
            id: id,
            adaptability: adaptability,
            autonomy: autonomy,
            city: city,
            commitment: commitment,
            emails: emails,
            english: english,
            geographicAvailability: geographicAvailability,
            idCampaign: idCampaign,
            idCollege: idCollege,
            idTechnologies: idTechnologies,
            interviewDate: interviewDate,
            knowledge: knowledge,
            motivationJob: motivationJob,
            name: name,
            objectiveOriented: objectiveOriented,
            others: others,
            otherLanguages: otherLanguages,
            salaryEstimate: salaryEstimate,
            studies: studies,
            studiesInCurse: studiesInCurse,
            surname: surname,
            teamwork: teamwork,
            yearsOfExperience: yearsOfExperience,
            candidateTechnologies: candidateTechnologies,
            candidateEmails: candidateEmails,
            employeeTechnicalInterviewer: employeeTechnicalInterviewer,
            idEmployeeTechnicalInterviewer: idEmployeeTechnicalInterviewer,
            categoryProfileJobPosition: categoryProfileJobPosition,
            idCategoryProfileJobPosition: idCategoryProfileJobPosition,
            email: email,
            recruitmentParticipation
        };
    }

    mapList(list: any): IList<ICandidate> {
        const mapper = new CandidateMapper();

        return {
            content: list.content.map(c => mapper.mapTo(c)),
            numberOfElements: list.numberOfElements,
            totalElements: list.totalElements,
            totalPages: list.totalPages
        };
    }

}
