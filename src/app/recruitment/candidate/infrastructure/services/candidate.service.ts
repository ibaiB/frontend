import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';

import {environment} from '../../../../../environments/environment.prod';
import {ICandidate} from '../../domain/ICandidate';
import {ICandidateEmail, IWebCandidate} from '../../domain/IWebCandidate';
import {CandidateAbstractService} from '../CandidateAbstractService';

@Injectable({
  providedIn: 'any'
})
export class CandidateService extends CandidateAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'candidates';

  constructor(private _http: HttpClient) {
    super();
  }

  create(candidate: IWebCandidate): Observable<IWebCandidate> {
    return this._http.post<IWebCandidate>(`${this.BASE_URL}${this.ENDPOINT}`, candidate);
  }

  delete(id: string): Observable<any> {
    return this._http.delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  get(id: string): Observable<IWebCandidate> {
    return this._http.get<IWebCandidate>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  search(query: any): Observable<IList<ICandidate>> {
    return this._http.get<IList<any>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: this._clean(query)})});
  }

  update(candidate: IWebCandidate): Observable<IWebCandidate> {
    return this._http.put<IWebCandidate>(`${this.BASE_URL}${this.ENDPOINT}/${candidate.id}`, candidate);
  }

  getCandidateEmail(idCandidate: string): Observable<ICandidateEmail> {
    return this._http
      .get<ICandidateEmail>(`${this.BASE_URL}${this.ENDPOINT}/${idCandidate}`);
  }

  private _clean(object): any { 
    const cleaned = {}; 
    const keys = Object.keys(object); 
    keys.forEach(key => { 
        if (object[key]) { cleaned[key] = object[key]; } 
    }); 
    return cleaned; 
  }
}
