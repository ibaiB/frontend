import {TestBed} from '@angular/core/testing';

import {CandidateTableResolverService} from './candidate-table-resolver.service';

describe('CandidateTableResolverService', () => {
  let service: CandidateTableResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CandidateTableResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
