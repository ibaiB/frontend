import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {getCars} from '../../../../internal/car/application/getCars';
import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {CarService} from '../../../../internal/car/infrastructure/services/car.service';
import {getSingleCandidate} from '../../application/getSingleCandidate';
import {ICandidateDetail} from '../../domain/ICandidateDetail';
import {getColleges} from '../../../college/application/getColleges';
import {CollegeMapper} from '../../../college/infrastructure/CollegeMapper';
import {searchEmployee} from '../../../../internal/employee/application/searchEmployee';
import {getStudies} from '../../../study/application/getStudies';
import {StudyMapper} from '../../../study/infrastructure/StudyMapper';
import {searchTechnology} from '../../../../internal/technology/application/searchTechnology';
import {CandidateService} from '../services/candidate.service';
import {CollegeService} from '../../../college/infrastructure/services/college.service';
import {StudyService} from '../../../study/infrastructure/services/study.service';
import {TechnologyService} from '../../../../internal/technology/infrastructure/services/technology.service';
import {EmployeeService} from '../../../../internal/employee/infrastructure/services/employee.service';

@Injectable({
    providedIn: 'any'
})
export class CandidateDetailResolverService implements Resolve<ResolvedData<ICandidateDetail>> {

    constructor(
        private _candidateService: CandidateService,
        private _studyService: StudyService,
        private _collegeService: CollegeService,
        private _technologyService: TechnologyService,
        private _employeeService: EmployeeService,
        private _carService: CarService,
        private _studyMapper: StudyMapper,
        private _collegeMapper: CollegeMapper
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<ICandidateDetail>> {
        let params = { page: 0, size: 100 };
        const candidateId = route.params['id'] ?? null;
        const $candidate = candidateId ? getSingleCandidate(candidateId, this._candidateService) : null;
        const $studies = getStudies(params, this._studyService, this._studyMapper);
        const $colleges = getColleges(params, this._collegeService, this._collegeMapper);
        const $technologies = searchTechnology(params, this._technologyService);
        const $employees = searchEmployee(params, this._employeeService);
        const $cars = getCars(params, this._carService);

        const sources: any[] = [$studies, $colleges, $technologies, $employees, $cars];
        if ($candidate) sources.push($candidate);

        return forkJoin(...sources).pipe(
            map(response => ({
                data: {
                    candidate: response[5] ?? null,
                    studies: response[0],
                    colleges: response[1],
                    technologies: response[2],
                    employees: response[3],
                    cars: response[4]
                }
            })),
            catchError(error => {
                return of({
                    data: null,
                    error,
                    message: 'Error while fetching candidate detail data'
                });
            }));
    }
}
