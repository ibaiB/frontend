import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {searchCandidates} from '../../application/searchCandidates';
import {ICandidateTableResolved} from '../../domain/ICandidateTableResolved';
import {CandidateService} from '../services/candidate.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {CandidateMapper} from '../CandidateMapper';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';

@Injectable({
    providedIn: 'any'
})
export class CandidateTableResolverService implements Resolve<ICandidateTableResolved> {

    constructor(private _candidateService: CandidateService, private _candidateMapper: CandidateMapper,
                private _cookieService: CookieFacadeService) {
    }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICandidateTableResolved> | Promise<ICandidateTableResolved> | ICandidateTableResolved {
    const filtersCache = this._cookieService.get('candidateFilterCache');
    const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('candidate-table-config'));
    const candidateParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
    const orderParams = {...tableFiltersConfig?.order} ?? {};

    return searchCandidates({...JSON.parse(filtersCache),...candidateParams,...orderParams}, this._candidateService, this._candidateMapper).pipe(map(response => ({
          tableData: {
            candidates: response,
            tableFiltersConfig: tableFiltersConfig,
            filtersValue:filtersCache
          }
        })),
        catchError(error => {
          return of({
            tableData: null,
          error: { message: 'Error on campaign table resolver, data couldn\'t be fetched', error }
        })
      }));
  }
}
