import {Injectable} from '@angular/core';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {INewCandidateResolved} from '../../domain/INewCandidateResolved';
import {CollegeService} from '../../../college/infrastructure/services/college.service';
import {searchColleges} from '../../../college/application/searchColleges';
import {getDictionaries} from 'src/app/shared/entities/dictionary/application/getDictionaries';
import {DictionaryService} from '../../../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {getDictionaryById} from 'src/app/shared/entities/dictionary/application/getDictionaryById';
import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {INewCandidateData} from '../../domain/INewCandidateData';
import {searchEmployee} from '../../../../internal/employee/application/searchEmployee';
import {searchTechnology} from '../../../../internal/technology/application/searchTechnology';
import {TechnologyService} from '../../../../internal/technology/infrastructure/services/technology.service';
import {StudyService} from '../../../study/infrastructure/services/study.service';
import {getStudies} from '../../../study/application/getStudies';
import {StudyMapper} from '../../../study/infrastructure/StudyMapper';
import {EmployeeService} from '../../../../internal/employee/infrastructure/services/employee.service';

@Injectable({
    providedIn: 'any'
})
export class NewCandidateResolverService implements Resolve<INewCandidateResolved> {

    constructor(
        private _collegeService: CollegeService,
        private _dictionaryService: DictionaryService,
        private _employeeService: EmployeeService,
        private _technologyService: TechnologyService,
        private _studyService: StudyService,
        private _studyMapper: StudyMapper
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<INewCandidateData>> {
        const params = {page: 0, size: 1000};
        const colleges = searchColleges(params, this._collegeService);
        const dictionaries = getDictionaries(params, this._dictionaryService);
        const employees = searchEmployee(params, this._employeeService);
        const technologies = searchTechnology(params, this._technologyService);
        const studies = getStudies(params, this._studyService, this._studyMapper);

        return forkJoin(colleges, dictionaries, employees, technologies, studies)
            .pipe(
                map(response => ({
                    data: {
                        colleges: response[0],
                        cars: getDictionaryById(response[1], 'CAR').values,
                        employees: response[2],
                        technologies: response[3],
                        studies: response[4]
                    }
                })),
                catchError(error => {
                    return of({
                        data: null,
                        message: 'Error on account table resolver, data couldn\'t be fetched',
                        error: error
                    });
                })
            );
    }
}
