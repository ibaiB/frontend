import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';
import {ICandidate} from '../domain/ICandidate';
import {CandidateAbstractService} from '../infrastructure/CandidateAbstractService';
import {CandidateMapper} from '../infrastructure/CandidateMapper';
import {map} from 'rxjs/operators';

export function searchCandidates(query: any, service: CandidateAbstractService, mapper: CandidateMapper): Observable<IList<ICandidate>> {
    return service.search(query).pipe(map(mapper.mapList));
}
