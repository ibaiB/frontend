import {CandidateAbstractService} from '../infrastructure/CandidateAbstractService';

export function getSingleCandidate(idCandidate: string, service: CandidateAbstractService) {
    return service.get(idCandidate);
}
