import {CandidateAbstractService} from '../infrastructure/CandidateAbstractService';
import {ICandidate} from '../domain/ICandidate';
import {CandidateMapper} from '../infrastructure/CandidateMapper';
import {map} from 'rxjs/operators';

export function updateCandidate(candidate: ICandidate, service: CandidateAbstractService, mapper: CandidateMapper) {
    return service.update(mapper.mapFrom(candidate)).pipe(map(mapper.mapTo));
}
