import {CandidateAbstractService} from '../infrastructure/CandidateAbstractService';

export function getCandidateEmail(idCandidate: string, service: CandidateAbstractService) {
    return service.getCandidateEmail(idCandidate);
}
