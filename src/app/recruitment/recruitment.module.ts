import {DragDropModule} from '@angular/cdk/drag-drop';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {AngularMaterialModule} from '../shared/material/angular-material.module';
import {SharedModule} from '../shared/shared.module';
import {ExamMapper} from '../evaluations/exam/infrastructure/ExamMapper';
import {SentExamMapper} from '../evaluations/sent-exam/infraestructure/SentExamMapper';
import {AddCandidateComponent} from './campaign/infrastructure/ng-components/add-candidate/add-candidate.component';
import {CampaignTableComponent} from './campaign/infrastructure/ng-components/campaign-table/campaign-table.component';
import {CampaignTimetableComponent} from './campaign/infrastructure/ng-components/campaign-timetable/campaign-timetable.component';
import {CreateCampaignComponent} from './campaign/infrastructure/ng-components/create-campaign/create-campaign.component';
import {EditCampaignComponent} from './campaign/infrastructure/ng-components/edit-campaign/edit-campaign.component';
import {CandidateTableComponent} from './candidate/infrastructure/ng-components/candidate-table/candidate-table.component';
import {routing} from './recruitment-routing.module';
import {StudyTableComponent} from './study/infrastructure/ng-components/study-table/study-table.component';
import {CreateStudyComponent} from './study/infrastructure/ng-components/create-study/create-study.component';
import {CreateCollegeComponent} from './college/infrastructure/ng-components/create-college/create-college.component';
import {CollegeTableComponent} from './college/infrastructure/ng-components/college-table/college-table.component';
import {EditCandidateComponent} from './candidate/infrastructure/ng-components/edit-candidate/edit-candidate.component';
import {RatingComponent} from './candidate/infrastructure/ng-components/rating/rating.component';


@NgModule({
    declarations: [
        CandidateTableComponent,
        CampaignTableComponent,
        CampaignTimetableComponent,
        CreateCampaignComponent,
        EditCampaignComponent,
        AddCandidateComponent,
        StudyTableComponent,
        CreateStudyComponent,
        CreateCollegeComponent,
        CollegeTableComponent,
        EditCandidateComponent,
        RatingComponent,
    ],
    exports: [],
    imports: [
        CommonModule,
        routing,
        AngularMaterialModule,
        SharedModule,
    ],
    providers: []
})
export class RecruitmentModule {
}
