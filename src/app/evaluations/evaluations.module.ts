import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {routing} from './evaluations-routing.module';
import {NewTestComponent} from './test/infrastructure/ng-components/new-test/new-test.component';
import {NewExamComponent} from './exam/infrastructure/ng-components/new-exam/new-exam.component';
import {ExamTableComponent} from './exam/infrastructure/ng-components/exam-table/exam-table.component';
import {TestTableComponent} from './test/infrastructure/ng-components/test-management/test-table/test-table.component';
import {TestAnalyticsComponent} from './test/infrastructure/ng-components/test-analytics/test-analytics.component';
import {ExamsManagmentComponent} from './exam/infrastructure/ng-components/exams-managment/exams-managment.component';
import {SentExamsTableComponent} from './exam/infrastructure/ng-components/sent-exams-table/sent-exams-table.component';
import {SentExamDetailComponent} from './exam/infrastructure/ng-components/sent-exam-detail/sent-exam-detail.component';
import {TestManagementComponent} from './test/infrastructure/ng-components/test-management/test-management.component';
import {CreateCorrectionComponent} from './test/infrastructure/ng-components/create-correction/create-correction.component';
import {TestDetailComponent} from './test/infrastructure/ng-components/test-detail/test-detail.component';
import {SendExamComponent} from './exam/infrastructure/ng-components/send-exam/send-exam.component';
import {EditExamComponent} from './exam/infrastructure/ng-components/edit-exam/edit-exam.component';
import {EditTestComponent} from './test/infrastructure/ng-components/edit-test/edit-test.component';
import {EditCorrectionComponent} from './test/infrastructure/ng-components/edit-correction/edit-correction.component';
import {SharedModule} from '../shared/shared.module';
import {AngularMaterialModule} from '../shared/material/angular-material.module';


@NgModule({
    declarations: [
        NewTestComponent,
        NewExamComponent,
        ExamTableComponent,
        TestTableComponent,
        TestAnalyticsComponent,
        ExamsManagmentComponent,
        SentExamsTableComponent,
        SentExamDetailComponent,
        TestManagementComponent,
        CreateCorrectionComponent,
        TestDetailComponent,
        SendExamComponent,
        EditExamComponent,
        EditTestComponent,
        EditCorrectionComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        AngularMaterialModule,
        routing
    ]
})
export class EvaluationsModule {
}
