import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment.prod';
import {IWebCounterList} from '../../domain/IWebCounterList';
import {CounterAbstractService} from '../CounterAbstractService';

@Injectable({
  providedIn: 'any'
})
export class CounterService extends CounterAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'counters';

  constructor(
    private http: HttpClient,
  ) {
    super();
  }

  getCounters(examSentId: string): Observable<IWebCounterList> {
    return this.http.get<IWebCounterList>(`${this.BASE_URL}${this.ENDPOINT}?exam_sent_id=${examSentId}`);
  }
}
