import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class CounterMapper {
  mapTo(params: any) {
    const {
      id,
      name,
      description,
    } = params;

    return {
      id: id,
      name: name,
      description: description,
    };
  }

  mapFrom(params: any) {
    const {
      id,
      name,
      description,
    } = params;

    return {
      id: id,
      name: name,
      description: description,
    };
  }
}
