import {Observable} from 'rxjs';

import {IWebCounterList} from '../domain/IWebCounterList';

export abstract class CounterAbstractService {

  abstract getCounters(examSentId: string): Observable<IWebCounterList>;

}
