export interface ICounter {
    id?: string;
    name: string;
    description: string;
}
