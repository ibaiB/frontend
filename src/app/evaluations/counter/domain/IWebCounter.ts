export interface IWebCounter {
    id?: string;
    name: string;
    description: string;
}
