import {IWebCounter} from './IWebCounter';

export interface IWebCounterList {
    results: IWebCounter[];
    nonComputedResponses: [
        {
            questionDescription: string;
            questionId: string;
            questionName: string;
            responseId: string;
            responseText: string;
        }
    ];
}
