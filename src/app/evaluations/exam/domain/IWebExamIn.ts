import {ICorrection} from '../../correction/domain/ICorrection';
import {ITest} from '../../test/domain/ITest';

export interface IWebExamIn {
    id?: string;
    name: string;
    description: string;
    test: ITest;
    correction: ICorrection;
}
