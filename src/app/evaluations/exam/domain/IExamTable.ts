import {IWebCorrectionList} from '../../correction/domain/IWebCorrectionList';
import {IWebSentExamList} from '../../sent-exam/domain/IWebSentExamList';
import {IWebTestList} from '../../test/domain/IWebTestList';
import {IWebExamList} from './IWebExamList';
import {ICandidate} from '../../../recruitment/candidate/domain/ICandidate';
import {IList} from '../../../shared/generics/IList';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';

export interface IExamTable {
    exams: IWebExamList;
    sentExams: IWebSentExamList;
    candidates: IList<ICandidate>;
    tests: IWebTestList;
    corrections: IWebCorrectionList;
    filtersValuesExam: any;
    filtersValuesSentExam: any;
    examTableFiltersConfig: ITableFiltersConfig;
    sentExamTableFiltersConfig: ITableFiltersConfig;
}
