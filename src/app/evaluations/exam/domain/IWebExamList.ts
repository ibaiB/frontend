import {IWebExamIn} from './IWebExamIn';

export interface IWebExamList {
    content: IWebExamIn[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
