export interface IWebExamOut {
    id?: string;
    name: string;
    description: string;
    idTest: string;
    idCorrection: string;
}
