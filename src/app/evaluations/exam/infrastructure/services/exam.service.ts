import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment.prod';
import {IExam} from '../../domain/IExam';
import {IWebExamIn} from '../../domain/IWebExamIn';
import {IWebExamOut} from '../../domain/IWebExamOut';
import {AbstractExamService} from '../abstract-exam-service';
import {IWebExamList} from '../../domain/IWebExamList';
import {ISentExamList} from '../../../sent-exam/domain/ISentExamList';

@Injectable({
    providedIn: 'any'
})
export class ExamService extends AbstractExamService {
    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'exams';
    private ENDPOINT2: string = 'exams-sent';

    constructor(private _http: HttpClient) {
        super();
    }

    createExam(exam: IWebExamOut): Observable<IWebExamIn> {
        return this._http.post<IWebExamIn>(`${this.BASE_URL}${this.ENDPOINT}`, exam);
    }

    get(id: string): Observable<IExam> {
        throw new Error('Method not implemented.');
    }

    update(idExam: string, exam: IWebExamOut): Observable<IWebExamIn> {
        return this._http
            .put<IWebExamIn>(`${this.BASE_URL}${this.ENDPOINT}/${idExam}`, exam);
    }

    updateCorrection(idExam: string, idCorrection: string): Observable<IWebExamIn> {
        return this._http
            .put<IWebExamIn>(`${this.BASE_URL}${this.ENDPOINT}/${idExam}/correction/${idCorrection}`, {});
    }

    search(query: any): Observable<IWebExamList> {
        return this._http.get<IWebExamList>(`${this.BASE_URL}${this.ENDPOINT}/search`, { params: new HttpParams({ fromObject: query }) });
    }

    getSentExams(query: any): Observable<ISentExamList> {
        return this._http.get<ISentExamList>(`${this.BASE_URL}${this.ENDPOINT2}/search`, {params: new HttpParams({fromObject: query})});
    }

    sendExam(idCandidateEmail: string, idExam: string): Observable<any> {
        return this._http
            .post<any>(`${this.BASE_URL}${this.ENDPOINT}/${idExam}/send?idCandidateEmail=${idCandidateEmail}`, idCandidateEmail);
    }

    resend(idExamSent: string): Observable<any> {
        return this._http.post<any>(`${this.BASE_URL}${this.ENDPOINT}/${idExamSent}/send/existing`, idExamSent);
    }

    restart(idExamSent: string): Observable<any> {
        return this._http.put<any>(`${this.BASE_URL}${this.ENDPOINT2}/${idExamSent}/restart`, idExamSent);
    }
}
