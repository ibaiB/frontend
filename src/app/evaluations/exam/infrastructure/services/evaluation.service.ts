import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EvaluationAbstractService} from '../../../evaluation/infrastructure/evaluation-abstract-service';
import {IWebEvaluationIn, IWebEvaluationOut} from '../../../evaluation/domain/IWebEvaluation';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class EvaluationService extends EvaluationAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'evaluations';

  constructor(private _http: HttpClient) {
    super();
  }

  end(response: IWebEvaluationOut, token: string): Observable<any> {
    return this._http.post<any>(`${this.BASE_URL}${this.ENDPOINT}/end?id_token=${token}`, response);
  }

  start(token: string): Observable<IWebEvaluationIn> {
    return this._http.post<any>(`${this.BASE_URL}${this.ENDPOINT}/start?id_token=${token}`, token);
  }
}
