import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class ExamMapper {
    mapTo(params: any) {
        const {
            id,
            name,
            description,
            test,
            correction
        } = params;

        return {
            id: id,
            name: name,
            description: description,
            test: test,
            correction: correction
        };
    }

    mapFrom(params: any) {
        const {
            id,
            name,
            description,
            test,
            correction
        } = params;

        return {
            id: id,
            name: name,
            description: description,
            test: test,
            correction: correction
        };
    }
}
