import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {searchCorrections} from 'src/app/evaluations/correction/application/searchCorrections';
import {ICorrection} from 'src/app/evaluations/correction/domain/ICorrection';
import {CorrectionMapper} from 'src/app/evaluations/correction/infrastructure/CorrectionMapper';
import {updateExamCorrection} from 'src/app/evaluations/exam/application/updateExamCorrection';
import {CorrectionService} from 'src/app/evaluations/correction/infrastructure/services/correction.service';
import {errorAlert, successAlert} from '../../../../../shared/error/custom-alerts';
import {updateExam} from '../../../application/updateExam';
import {IWebExamOut} from '../../../domain/IWebExamOut';
import {ExamService} from '../../services/exam.service';

@Component({
  selector: 'app-edit-exam',
  templateUrl: './edit-exam.component.html',
  styleUrls: ['./edit-exam.component.scss']
})
export class EditExamComponent implements OnInit {

  public form: FormGroup;
  public corrections: ICorrection[] = [];
  selectFood = 'Que paso rey';
  width = '100%';
  carValue: string = '';
  carOptions = [];
  constructor(
    private _fb: FormBuilder,
    public dialogRef: MatDialogRef<EditExamComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _examService: ExamService,
    private _correctionService: CorrectionService,
    private _correctionMapper: CorrectionMapper
  ) { }

  ngOnInit(): void {
    this.carValue = this.data.correctionName;
    searchCorrections({}, this._correctionService, this._correctionMapper).subscribe(corrections =>{
      this.corrections = corrections.content;
      this.carOptions = this.corrections.map(corr => corr.name);

    });
    this.formInit();
  }

  autocompleteSelected(event){
    this.carValue = event.value;
  }

  private formInit() {
    this.form = this._fb.group({
      name: [this.data.exam.name],
      test: [this.data.exam.test.name],
      description: [this.data.exam.description],
      corr: [''],
    });
  }

  public onNoClick() {
    this.dialogRef.close();
  }

  public onCancelClick() {
    this.dialogRef.close();
  }

  public onSaveClick() {
    const correction = this.corrections.find(c => c.name === this.carValue);
    console.log(correction);
    if (this.form.valid) {
      const editExam: IWebExamOut = {
        id: this.data.exam.id,
        name: this.form.get('name').value,
        description: this.form.get('description').value,
        idTest: this.data.exam.test.id,
        idCorrection: correction.id,
      };

      updateExam(editExam, this._examService)
        .subscribe(response => {
          successAlert('The exam was updated successfully');
        }, error => {
          console.error('Error at edit exam', error);
          const { error: { message, id } } = error;
          errorAlert(`Couldn\'t update exam`, message, id);
        });

      updateExamCorrection(this.data.exam.id, correction.id, this._examService)
      .subscribe(data => console.log(data));

      this.dialogRef.close();
    }
  }

}
