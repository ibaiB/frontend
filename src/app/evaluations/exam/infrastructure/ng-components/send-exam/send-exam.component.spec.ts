import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SendExamComponent} from './send-exam.component';

describe('SendExamComponent', () => {
  let component: SendExamComponent;
  let fixture: ComponentFixture<SendExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SendExamComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
