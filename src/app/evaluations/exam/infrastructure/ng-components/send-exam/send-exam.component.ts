import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {forkJoin} from 'rxjs';
import {searchCandidates} from '../../../../../recruitment/candidate/application/searchCandidates';
import {CandidateMapper} from '../../../../../recruitment/candidate/infrastructure/CandidateMapper';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {sendExam} from '../../../application/sendExam';
import {CandidateService} from '../../../../../recruitment/candidate/infrastructure/services/candidate.service';
import {ExamService} from '../../services/exam.service';
import {ICandidate} from '../../../../../recruitment/candidate/domain/ICandidate';

@Component({
    selector: 'app-send-exam',
    templateUrl: './send-exam.component.html',
    styleUrls: ['./send-exam.component.scss'],
})
export class SendExamComponent implements OnInit {
    public form: FormGroup;
    public readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    selected: {
        idCandidate: string;
        idEmail: string;
        email: string;
        name: string;
        surname: string;
    }[] = [];

    buffer: {
        idCandidate: string;
        email: string;
        name: string;
        surname: string;
    }[] = [];

    emails: string[] = [];

    candidateEmails: {
        id: string;
        email: string;
    }[] = [];

    constructor(
        private _fb: FormBuilder,
        public dialogRef: MatDialogRef<SendExamComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _examService: ExamService,
        private _candidateService: CandidateService,
        private _candidateMapper: CandidateMapper
    ) {
    }

    ngOnInit(): void {
        this.data.candidates.forEach((candidate) => {
            candidate.candidateEmails.forEach((email) => {
                this.emails.push(
                    `${email.email} (${candidate.name} ${candidate.surname})`
                );
                this.candidateEmails.push(email);
                this.buffer.push({
                    idCandidate: candidate.id,
                    email: email.email,
                    name: candidate.name,
                    surname: candidate.surname,
                });
            });
        });

        this.formInit();
    }

    private formInit(): void {
        this.form = this._fb.group({
            email: ['', Validators.email],
            candidate: [''],
            exam: [this.data.exam.exam.name],
        });
    }

    public autocompleteSelected(selectedRow: string) {
        if (selectedRow) {
            const splitedRow = selectedRow.split(' ');
            const candidateEmail = this.candidateEmails.find(
                (e) => e.email === splitedRow[0]
            );
            const candidate = this.buffer.find((c) => c.email === splitedRow[0]);
            const selected = {
                idCandidate: candidate.idCandidate,
                idEmail: candidateEmail.id,
                email: candidateEmail.email,
                name: candidate.name,
                surname: candidate.surname,
            };

            const contained = this.selected.find(c => c.idCandidate === candidate.idCandidate);
            if (contained) {
                const index = this.selected.indexOf(contained);
                this.selected[index] = selected;
            } else {
                this.selected.push(selected);
            }
        }
    }

    public remove(candidate: any): void {
        const index = this.selected.indexOf(candidate);
        if (index > -1) {
            this.selected.splice(index, 1);
        }
    }

    public onNoClick(): void {
        this.dialogRef.close();
    }

    public onCancelClick(): void {
        this.dialogRef.close();
    }

    public onSaveClick(): void {
        const idExam: string = this.data.exam.exam.id;
        const sentExams = [];
        for (const candidate of this.selected) {
            const sentEmail = sendExam(candidate.idEmail, idExam, this._examService);
            sentExams.push(sentEmail);
        }

        forkJoin(sentExams).subscribe(
            () => {
                successAlert('All users received the email correctly').then(() => this.dialogRef.close());
            }
        );
    }

    backSearch(event: string) {
        searchCandidates({email: event}, this._candidateService, this._candidateMapper)
            .subscribe(candidates => {
                const candidate: ICandidate[] = candidates.content;
                this.emails = [];
                this.candidateEmails = [];
                this.buffer = [];

                candidate.forEach((candidate) => {
                    candidate.candidateEmails.forEach((email) => {
                        this.emails.push(
                            `${email.email} (${candidate.name} ${candidate.surname})`
                        );
                        this.candidateEmails.push(email);
                        this.buffer.push({
                            idCandidate: candidate.id,
                            email: email.email,
                            name: candidate.name,
                            surname: candidate.surname,
                        });
                    });
                });
            });
    }
}
