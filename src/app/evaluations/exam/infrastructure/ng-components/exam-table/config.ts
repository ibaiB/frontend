import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
  multiSelect: false,
  actions: [
    { name: 'Send Exam', icon: 'email' },
    { name: 'Edit', icon: 'create' }
  ],
  columns: [
    {
      name: 'Id',
      prop: 'id',
      shown: true,
      route: 'a',
      routeId: '',
      cellClass: 'id-table-column'
    },
    {
      name: 'Name',
      prop: 'name',
      shown: true
    },
    {
      name: 'Description',
      prop: 'description',
      shown: true
    },
    {
      name: 'Test',
      prop: 'testName',
      shown: true
    },
    {
      name: 'Correction',
      prop: 'correctionName',
      shown: true
    },
  ]
};
