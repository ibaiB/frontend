import {
  createTableFiltersConfig,
  IAuto,
  ITableConfig,
  ITableFiltersConfig,
  parseTableFiltersConfig
} from 'src/app/shared/custom-mat-elements/dynamic';
import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {IDynamicColumn} from '../../../../../shared/custom-mat-elements/dynamic-table/IDynamicColumn';
import {errorAlert} from '../../../../../shared/error/custom-alerts';
import {IWebCandidateList} from '../../../../../recruitment/candidate/domain/IWebCandidateList';
import {IWebCorrectionList} from '../../../../correction/domain/IWebCorrectionList';
import {searchExams} from '../../../application/searchExams';
import {IExam} from '../../../domain/IExam';
import {IWebExamList} from '../../../domain/IWebExamList';
import {IWebTestList} from '../../../../test/domain/IWebTestList';
import {ExamService} from '../../services/exam.service';
import {EditExamComponent} from '../edit-exam/edit-exam.component';
import {SendExamComponent} from '../send-exam/send-exam.component';
import {IList} from 'src/app/shared/generics/IList';
import {table} from './config';
import {DynamicTableMarkIiComponent} from 'src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ExamMapper} from 'src/app/evaluations/exam/infrastructure/ExamMapper';


@Component({
  selector: 'app-exam-table',
  templateUrl: './exam-table.component.html',
  styleUrls: ['./exam-table.component.scss']
})
export class ExamTableComponent implements OnInit {

  @Input() exams: IWebExamList;
  @Input() filtersValues:any;
  @Input() candidates: IWebCandidateList;
  @Input() tests: IWebTestList;
  @Input() corrections: IWebCorrectionList;
  @Input() tableFiltersConfig: ITableFiltersConfig

  @Output() globalBackSearch = new EventEmitter<any>();

  public filters: IAuto[] = [];
  public columns: IDynamicColumn[] = [];

  tableConfig: ITableConfig = table;
  tableData: IList<any>;
  queryPagination;
  queryOrder;
  queryFilters;
  configVisibility: boolean = false;

  @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

  public loading = true;

  private scopeEmployeeInsert = false;    // Auxiliary variable to check scope
  private scopeEvaluationsInsert = false; // Auxiliary variable to check scope
  testOptions = [];
  correctionOptions = []
  constructor(
    private _examService: ExamService,
    private _dialog: MatDialog,
    private _examMapper : ExamMapper,
    private _cookieFacade: CookieFacadeService
  ) {
  }

  ngOnInit(): void {
    this.testOptions = this.tests.content
    this.correctionOptions = this.corrections.content.map(correction => correction.name);
    this._buildFilters();

    const data = [];
    this.exams.content.forEach(exam => data.push({
      exam,
      id: exam.id,
      name: exam.name,
      description: exam.description,
      // TODO: Fake properties
      correctionName: exam.correction.name,
      testName: exam.test.name
    }));
    this.exams.content = data;
    this.tableData = this.exams;
    parseTableFiltersConfig(this.tableFiltersConfig, this.tableConfig, this.filters);
    this.queryFilters=this.filtersValues ? this._clean((JSON.parse(this.filtersValues))) :{};
    this.tableConfig.pagination=this.tableFiltersConfig?.pagination ?? {page:0, size:10};
    this.queryPagination=this.tableConfig.pagination;
    this.tableConfig.order = this.tableFiltersConfig?.order ?? {orderField:'', order:''};
    this.queryOrder={...this.tableConfig.order}; 
    this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                                        Object.entries(this._examMapper.mapTo({[this.tableConfig.order.orderField]:''})).find(entry => entry[1] === '')[0] : '';
    this.loading = false;
    this.configFilterValue();
  }

  configFilterValue(){
      let filtersAux=this.filtersValues ? this._clean(this._examMapper.mapTo(JSON.parse(this.filtersValues))) : {};
      this.filters.forEach(filter => {filter.defaultValue = filtersAux[this.getFilterProp(filter)]
                                      filter.value = filtersAux[this.getFilterProp(filter)]});        
  }

  getFilterProp(filter:any){
      return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
  }

  private _clean(object): any { 
      const cleaned = {}; 
      const keys = Object.keys(object); 
      keys.forEach(key => { 
          if (object[key]) { cleaned[key] = object[key]; } 
      }); 
      return cleaned;
    }

  private sendExam(exam: IExam) {
    const dialogRef = this._dialog.open(SendExamComponent, {
      data: {
        exam,
        candidates: this.candidates.content
      }
    });
    dialogRef.afterClosed().subscribe(() => this.globalBackSearch.emit());
  }

  private editExam(exam: IExam) {
    const dialogRef = this._dialog.open(EditExamComponent, {
      data: exam
    });
    dialogRef.afterClosed().subscribe(() => this.globalBackSearch.emit());
  }

  public doAction(event: { action: string, item: IExam }) {
    if (event.action === 'Send Exam') {
      this.sendExam(event.item);
    }

    if (event.action === 'Edit') {
      this.editExam(event.item);
    }
  }

  configChange(event:any){
    this.tableConfig={...event[0]};
    this.filters=event[1];
    this._cookieFacade.save('exam-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters,this.queryPagination,this.queryOrder)));
  }

  setConfigVisibility(visible: boolean) {
    this.configVisibility = visible;
  }

  public backSearch() {
    searchExams({ ...this.queryPagination, ...this.queryOrder, ...this.queryFilters }, this._examService)
      .subscribe(result => {
        const data = [];
        result.content.forEach(exam => data.push({
          exam,
          id: exam.id,
          name: exam.name,
          description: exam.description,
          // TODO: Fake properties
          correctionName: exam.correction.name,
          testName: exam.test.name
        }));
        result.content = data;
        this.tableData = result;

      }, error => {
        console.error('error on back search', error);
        const { error: { message, id } } = error;
        errorAlert(`Couldn\'t retrieve data`, message, id);
      });
  }

  filtersChanged(params?: any) {
    if (params && Object.keys(params).length !== 0) {
        this.queryFilters = params;
    } else {
        this.queryFilters = {};
    }
    this._cookieFacade.save('examFilterCache',JSON.stringify(this.queryFilters));
    this.queryPagination = { page: 0, size: this.queryPagination.size };
    this._cookieFacade.save('exam-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters,this.queryPagination,this.queryOrder)));
    this.dynamicTable.resetPageIndex();
    this.backSearch();
  }

  paginationChanged(event: any) {
      this.queryPagination.page = event.page;
      this.queryPagination.size = event.size;
      this._cookieFacade.save('exam-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters,this.queryPagination,this.queryOrder)));
      this.backSearch();
  }

  orderChanged(event: any) {
      this.queryOrder.orderField = event.orderField;
      this.queryOrder.order = event.order;
      this.queryPagination.page = event.page;
      this.queryPagination.size = event.size;
      this._cookieFacade.save('exam-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters,this.queryPagination,this.queryOrder)));
      this.backSearch();
  }

  private _buildFilters() {
    this.filters = [
      {
        options: [],
        prop: 'id',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Id',
        placeholder: 'Id',
        shown: true,
      },
      {
        options: [],
        prop: 'name',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Name',
        placeholder: 'Name',
        shown: true,
      },
      {
        options: [],
        prop: 'description',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Description',
        placeholder: 'Description',
        shown: true,
      }
    ];
  }
}
