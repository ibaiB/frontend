import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {IList} from '../../../../../shared/generics/IList';
import {ResolvedData} from '../../../../../shared/generics/IResolvedData';
import {ICandidate} from '../../../../../recruitment/candidate/domain/ICandidate';
import {IWebCorrectionList} from '../../../../correction/domain/IWebCorrectionList';
import {IExamTable} from '../../../domain/IExamTable';
import {IWebExamList} from '../../../domain/IWebExamList';
import {IWebSentExamList} from '../../../../sent-exam/domain/IWebSentExamList';
import {IWebTestList} from '../../../../test/domain/IWebTestList';
import {NewExamComponent} from '../new-exam/new-exam.component';
import {SentExamsTableComponent} from '../sent-exams-table/sent-exams-table.component';

@Component({
    selector: 'app-exams-managment',
    templateUrl: './exams-managment.component.html',
    styleUrls: ['./exams-managment.component.scss']
})
export class ExamsManagmentComponent implements OnInit {

    private _resolved: ResolvedData<IExamTable>;
    public examsTable = true;

    exams: IWebExamList;
    sentExams: IWebSentExamList;
    candidates: IList<ICandidate>;
    tests: IWebTestList;
    corrections: IWebCorrectionList;
    loading: boolean = true;
    filtersValueExam: any;
    filtersValueSentExam: any;
    sentExamTableFiltersConfig: ITableFiltersConfig;
    examTableFiltersConfig: ITableFiltersConfig;

    @ViewChild('examTable') examTable: SentExamsTableComponent;
    @ViewChild('sentExamTable') sentExamTable: SentExamsTableComponent;

    constructor(
        private _router: Router,
        private dialog: MatDialog,
        private _route: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
        this._resolved = this._route.snapshot.data['response'];

        if (this._resolved.data) {
            this.exams = this._resolved.data.exams;
            this.sentExams = this._resolved.data.sentExams;
            this.candidates = this._resolved.data.candidates;
            this.tests = this._resolved.data.tests;
            this.corrections = this._resolved.data.corrections;
            this.filtersValueExam = this._resolved.data.filtersValuesExam;
            this.filtersValueSentExam = this._resolved.data.filtersValuesSentExam;

            this.loading = false;
            this.examTableFiltersConfig = this._resolved.data.examTableFiltersConfig;
            this.sentExamTableFiltersConfig = this._resolved.data.sentExamTableFiltersConfig;
        }

    }

    createNewExamDialog() {
        const dialogRef = this.dialog.open(NewExamComponent, {});

        dialogRef
            .afterClosed()
            .subscribe(newExam => {
                this.backSearch();
            });
    }

    backSearch() {
        this.examTable.backSearch();
        this.sentExamTable.backSearch();
    }
}
