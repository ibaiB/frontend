import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
  multiSelect: false,
  actions: [
    {
      name: 'Show',
      icon: 'search'
    },
    {
      name: 'Resend',
      icon: 'repeat'
    },
    {
      name: 'Restart',
      icon: 'replay'
    },
  ],
  columns: [
    {
      name: 'Id',
      prop: 'id',
      shown: true,
      route: '',
      routeId: '',
      cellClass: 'id-table-column'
    },
    {
      name: 'Exam',
      prop: 'examName',
      shown: true
    },
    {
      name: 'Test',
      prop: 'testName',
      shown: true
    },
    {
      name: 'Email',
      prop: 'email',
      shown: true
    },
    {
      name: 'Exam sent at',
      prop: 'examSentAt',
      type: 'date',
      shown: true
    },
    {
      name: 'Exam started at',
      prop: 'examStartedAt',
      type: 'date',
      shown: true
    },
    {
      name: 'Exam end at',
      prop: 'examEndAt',
      type: 'date',
      shown: true
    }
  ],
};
