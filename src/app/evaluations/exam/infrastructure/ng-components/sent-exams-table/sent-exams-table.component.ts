import {
    createTableFiltersConfig,
    IAuto,
    ITableConfig,
    ITableFiltersConfig,
    parseTableFiltersConfig
} from 'src/app/shared/custom-mat-elements/dynamic';
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {IUserInfo} from '../../../../../auth/session/domain/IUserInfo';
import {IDynamicColumn} from '../../../../../shared/custom-mat-elements/dynamic-table/IDynamicColumn';
import {IExam} from '../../../domain/IExam';
import {ISentExam} from '../../../../sent-exam/domain/ISentExam';
import {IWebSentExamList} from '../../../../sent-exam/domain/IWebSentExamList';
import {ExamService} from '../../services/exam.service';
import {SentExamDetailComponent} from '../sent-exam-detail/sent-exam-detail.component';
import {IList} from 'src/app/shared/generics/IList';
import {table} from './config';
import {DynamicTableMarkIiComponent} from 'src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import Swal from 'sweetalert2';
import {resend} from '../../../application/resend';
import {restart} from '../../../application/restart';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {SentExamMapper} from 'src/app/evaluations/sent-exam/infraestructure/SentExamMapper';

@Component({
    selector: 'app-sent-exams-table',
    templateUrl: './sent-exams-table.component.html',
    styleUrls: ['./sent-exams-table.component.scss']
})
export class SentExamsTableComponent implements OnInit {

    @Input() sentExams: IWebSentExamList;
    @Input() filtersValues: any;
    @Input() tableFiltersConfig: ITableFiltersConfig;

    public examsTable = true;
    public exams: IExam[] = [];
    public idExam = null;
    public user: IUserInfo = undefined;
    showFilters = false;
    loading = true;
    scopeEmployeeInsert = false;    // Auxiliary variable to check scope
    scopeEvaluationsInsert = false; // Auxiliary variable to check scope

    tableConfig: ITableConfig = table;
    tableData: IList<any>;
    queryPagination;
    queryOrder;
    queryFilters;

    filters: IAuto[] = [];
    columns: IDynamicColumn[] = [];
    configVisibility: boolean = false;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private examService: ExamService,
        private dialog: MatDialog,
        private _sentExamMapper: SentExamMapper,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit(): void {
        this._buildFilters();
        this.tableData = this.sentExams;
        parseTableFiltersConfig(this.tableFiltersConfig, this.tableConfig, this.filters);
        this.queryFilters = this.filtersValues ? this._clean((JSON.parse(this.filtersValues))) : {};
        this.tableConfig.pagination = this.tableFiltersConfig?.pagination ?? {page: 0, size: 10};
        this.queryPagination = this.tableConfig.pagination;
        this.tableConfig.order = this.tableFiltersConfig?.order ?? {orderField: '', order: ''};
        this.queryOrder = {...this.tableConfig.order};
        this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
            Object.entries(this._sentExamMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
        this.loading = false;
        this.configFilterValue();
    }

    configFilterValue() {
        let queryFilterAux = this.filtersValues ? this._clean(this._sentExamMapper.mapTo(JSON.parse(this.filtersValues))) : {};
        this.filters.forEach(filter => {
            filter.defaultValue = queryFilterAux[this.getFilterProp(filter)];
            filter.value = queryFilterAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('sentExam-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    backSearch() {
        if (this.queryFilters.examStartedAt) {
            let date = new Date(this.queryFilters.examStartedAt);
            date = new Date(date.setDate(date.getDate() - 1));
            this.queryFilters.examStartedAtInitial = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
            date = new Date(date.setDate(date.getDate() + 2)); // Date adding one day
            this.queryFilters.examStartedAtFinal = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
        }

        this.examService.getSentExams({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}).subscribe(exams => {

            this.tableData = exams;
        });
    }

    doAction(event: { action: string, item: ISentExam }) {
        if (event.action === 'Show') {
            this.openDetailSentExamDialog(event.item);
        }
        if (event.action === 'Resend') {
            this.openResendExamDialog(event.item);
        }
        if (event.action === 'Restart') {
            this.openRestartExamDialog(event.item);
        }
    }

    openDetailSentExamDialog(sentExam: ISentExam) {
        this.dialog.open(SentExamDetailComponent, {data: sentExam});
    }

    openResendExamDialog(sentExam: ISentExam) {
        Swal.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: 'You are about to resend this exam',
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080',
        }).then(result => {
            if (result.isConfirmed) {
                resend(sentExam.id, this.examService)
                    .subscribe(() => {
                        successAlert('The exam was successfully resent');
                    });
            }
        });
    }

    openRestartExamDialog(sentExam: ISentExam) {
        Swal.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: 'You are about to restart this exam\'s time',
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080',
        }).then(result => {
            if (result.isConfirmed) {
                restart(sentExam.id, this.examService)
                    .subscribe(() => {
                        successAlert('The exam was successfully resent');
                    });
            }
        });
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            this.queryFilters = params;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('sentExamFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('sentExam-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('sentExam-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        this.queryOrder.orderField = event.orderField;
        this.queryOrder.order = event.order;
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('sentExam-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Id',
                placeholder: 'Id',
                shown: true,
            },
            {
                options: [],
                prop: 'email',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Email',
                placeholder: 'Email',
                shown: true,
            },
            {
                options: [],
                prop: 'examStartedAt',
                retProp: 'examStartedAt',
                type: 'date',
                appearance: 'outline',
                class: 'date',
                label: 'Exam started at',
                placeholder: 'Exam started at',
                shown: true,
            }
        ];
    }
}
