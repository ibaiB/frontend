import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {forkJoin} from 'rxjs';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {ICorrection} from '../../../../correction/domain/ICorrection';
import {IExam} from '../../../domain/IExam';
import {ITest} from '../../../../test/domain/ITest';
import {CorrectionService} from '../../../../correction/infrastructure/services/correction.service';
import {ExamService} from '../../services/exam.service';
import {TestService} from '../../../../test/infrastructure/services/test.service';


@Component({
  selector: 'app-new-exam',
  templateUrl: './new-exam.component.html',
  styleUrls: ['./new-exam.component.scss'],
})
export class NewExamComponent implements OnInit {

  public form: FormGroup;

  public tests: ITest[] = [];
  public corrections: ICorrection[] = [];
  public allCorrections: ICorrection[] = [];

  public exam: IExam;

  constructor(
      private _testService: TestService,
      private _correctionService: CorrectionService,
      private _examService: ExamService,
      private _fb: FormBuilder,
      public dialogRef: MatDialogRef<NewExamComponent>
  ) { }

  ngOnInit(): void {
    const base = { page: 0, size: 1000 };
    const tests = this._testService.findAll({ ...base });
    const corrections = this._correctionService.findAll({ ...base });


    forkJoin(tests, corrections)
      .subscribe(data => {
        this.tests = data[0].content.filter(test => test.name);
        this.corrections = data[1].content;
        this.allCorrections = data[1].content;
      });

    this.formInit();
  }

  private formInit() {
    this.form = this._fb.group({
      name: [''],
      test: [''],
      description: [''],
      correction: [''],
    });
  }

  selectedTest(event) {
    this.corrections = this.allCorrections.filter(c => c.idTest === event.value.id);
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onSaveClick() {
    if (this.form.valid) {
      const name: string = this.form.get('name').value;
      const description: string = this.form.get('description').value;
      const test = this.form.get('test').value;
      const correction = this.form.get('correction').value;

      const result = {
        name: name,
        description: description,
        idTest: test.id,
        idCorrection: correction.id,
      };

      this._examService.createExam(result)
        .subscribe(result => {
          successAlert('Exam created successfully').then(() => this.dialogRef.close());
        });
    }
  }

}
