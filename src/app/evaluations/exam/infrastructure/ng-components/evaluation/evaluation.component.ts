import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {ActivatedRoute} from '@angular/router';
import {interval} from 'rxjs';
import Swal from 'sweetalert2';
import {endEvaluation} from '../../../../evaluation/application/endEvaluation';
import {startEvaluation} from '../../../../evaluation/application/startEvaluation';
import {IAnswer, IEvaluationIn, IEvaluationOut, IQuestion, IResponse, IStep} from '../../../../evaluation/domain/IEvaluation';
import {EvaluationMapper} from '../../../../evaluation/infrastructure/evaluation-mapper';
import {errorAlert, questionAlert} from '../../../../../shared/error/custom-alerts';
import {EvaluationService} from '../../services/evaluation.service';

@Component({
    selector: 'app-evaluation',
    templateUrl: './evaluation.component.html',
    styleUrls: ['./evaluation.component.scss'],
})
export class EvaluationComponent implements OnInit {
    forms: FormGroup[] = [];
    evaluation: IEvaluationIn;
    checkboxes: IResponse[] = [];
    loading: boolean = true;
    token: string;

    // Timer trial
    progressbarValue = 100;
    curSec: number = 0;
    progressColor = 'primary';

    constructor(
        private _route: ActivatedRoute,
        private _evaluationService: EvaluationService,
        private _evaluationMapper: EvaluationMapper,
        private _fb: FormBuilder
    ) {
    }

    private static _handleSuccess() {
        Swal.fire({
            icon: 'success',
            title: 'Todo perfecto',
            text: 'Seguro que lo has hecho genial!',
            confirmButtonColor: '#db5e5e',
        }).then(() => window.location.href = 'https://bosonit.com/');
    }

    private static _handleError(error) {
        if (error.message.toString().includes('Questions not answered')) {
            const {
                error: {message, id},
            } = error;
            errorAlert(
                `Parece que ha habido un error, por favor, no cierres la pestaña y ponte en contacto con
                            el departamento de RRHH`,
                message,
                id
            );
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Te has dejado preguntas sin contestar, por favor, revisa tu exámen y contestalas',
                confirmButtonColor: '#db5e5e',
            });
        }
    }

    ngOnInit() {
        this._route.queryParams.subscribe((params) => {
            if (!params['id_token']) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    confirmButtonColor: '#db5e5e',
                });
            }

            startEvaluation(params['id_token'], this._evaluationService, this._evaluationMapper).subscribe(
                (response) => {
                    this.token = params['id_token'];
                    Swal.fire({
                        icon: 'info',
                        title: 'Instrucciones',
                        text:
                            'Tienes una hora para realizar el exámen, una vez terminado el exámen, debes pulsar el botón de enviar! ' +
                            'En caso de que recibas un error, no cierres la ventana y ponte en contacto con el departamento de RRHH',
                        confirmButtonColor: '#db5e5e',
                    });
                    this.evaluation = response;
                    this._initForms();
                    this.loading = false;
                    this.startTimer(3600);
                },
                (error) => {
                    console.error('error', error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Parece que tu exámen ha caducado por favor ponte en contacto con el departamento de RRHH',
                        confirmButtonColor: '#db5e5e',
                    });
                }
            );
        });
    }

    startTimer(seconds: number) {
        const time = seconds;
        const timer$ = interval(1000);

        const sub = timer$.subscribe((sec) => {
            this.progressbarValue = 100 - (sec * 100) / seconds;
            this.curSec = sec;
            if (this.progressbarValue < 50) {
                if (this.progressbarValue < 25) {
                    this.progressColor = 'warn';
                }
            }
            if (this.curSec === seconds) {
                sub.unsubscribe();
            }
        });
    }

    private _initForms() {
        const {steps} = this.evaluation;
        steps.forEach((step) => {
            this._initForm(step);
        });
    }

    private _initForm(step: IStep) {
        let config = {};

        step.questions.forEach((question) => {
            if (question.questionType != 'CHECKBOX') {
                config[question.id] = [''];
            } else {
                this._initCheckbox(question);
            }
        });

        this.forms.push(this._fb.group(config));
    }

    private _sendTest() {
        const evaluation = this._setEvaluation();
        endEvaluation(evaluation, this.token, this._evaluationService, this._evaluationMapper).subscribe(
            () => {
                EvaluationComponent._handleSuccess();
            },
            (error) => {
                console.error('error', error);
                this._handleError(error);
            }
        );
    }

    private _initCheckbox(question: IQuestion) {
        question.answers.forEach((answer) => {
            this.checkboxes.push({
                question_id: question.id,
                answer_id: answer.id,
                answer_text: 'false',
            });
        });
    }

    setFormCheckBoxes(answer: IAnswer, change: MatCheckboxChange) {
        this.checkboxes.find((response) => response.answer_id === answer.id).answer_text = change.checked + '';
    }

    private _handleError(error) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Te has dejado preguntas sin contestar, por favor, revisa tu exámen y contestalas',
            confirmButtonColor: '#db5e5e',
        });
    }

    reset() {
        questionAlert('Se borrarán todas tus respuestas').then((result) => {
            if (result.isConfirmed) {
                this.forms = [];
                this._initForms();
            }
        });
    }

    finishTest() {
        questionAlert('Una vez enviado el examen no podrás modificar tus respuestas').then((result) => {
            if (result.isConfirmed) {
                this._sendTest();
            }
        });
    }

    private _setEvaluation(): IEvaluationOut {
        let responses: IResponse[] = [];

        this.forms.forEach((form) => {
            const questionIds = Object.keys(form.value);
            questionIds.forEach((id) =>
                responses.push({
                    answer_id: form.value[id],
                    question_id: id,
                    answer_text: form.value[id],
                })
            );
        });

        return {
            responses: responses.concat(this.checkboxes.filter((c) => c.answer_text === 'true')),
        };
    }
}
