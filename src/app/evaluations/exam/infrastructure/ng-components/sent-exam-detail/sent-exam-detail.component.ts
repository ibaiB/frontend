import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Component, Inject, OnInit} from '@angular/core';
import {CounterService} from '../../../../counter/infrastructure/services/counter.service';

@Component({
    selector: 'app-sent-exam-detail',
    templateUrl: './sent-exam-detail.component.html',
    styleUrls: ['./sent-exam-detail.component.scss']
})
export class SentExamDetailComponent implements OnInit {

    counterResults;
    nonComputedResponses;

  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      private _service: CounterService) {
  }

  ngOnInit(): void {
    this._service.getCounters(this.data.id)
        .subscribe(res => {
          this.counterResults = res.results;
          this.nonComputedResponses = res.nonComputedResponses;

        })
  }

}
