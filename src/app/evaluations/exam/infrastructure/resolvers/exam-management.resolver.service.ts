
import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {searchCandidates} from '../../../../recruitment/candidate/application/searchCandidates';
import {CandidateMapper} from '../../../../recruitment/candidate/infrastructure/CandidateMapper';
import {searchCorrections} from '../../../correction/application/searchCorrections';
import {CorrectionMapper} from '../../../correction/infrastructure/CorrectionMapper';
import {getExams} from '../../application/getExams';
import {IExamTable} from '../../domain/IExamTable';
import {getSentExams} from '../../../sent-exam/application/getSentExams';
import {searchTests} from '../../../test/application/searchTests';
import {CandidateService} from '../../../../recruitment/candidate/infrastructure/services/candidate.service';
import {CorrectionService} from '../../../correction/infrastructure/services/correction.service';
import {ExamService} from '../services/exam.service';
import {TestService} from '../../../test/infrastructure/services/test.service';

@Injectable({
    providedIn: 'any'
})
export class ExamManagementResolverService implements Resolve<ResolvedData<IExamTable>> {

    constructor(
        private _examService: ExamService,
        private _candidateService: CandidateService,
        private _testService: TestService,
        private _correctionService: CorrectionService,
        private _correctionMapper: CorrectionMapper,
        private _candidateMapper: CandidateMapper,
        private _cookieService : CookieFacadeService) {
    }


    resolve(): Observable<ResolvedData<IExamTable>> {
        const params={page:0,size:100};
        const filtersCacheExam = this._cookieService.get('examFilterCache');
        const filtersCacheSentExam = this._cookieService.get('sentExamFilterCache');
        const examTableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('exam-table-config'));
        const sentExamTableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('sentExam-table-config'));
        const examParms = examTableFiltersConfig?.pagination ?? {page: 0, size: 10};
        const sentExamParams = sentExamTableFiltersConfig?.pagination ?? {page: 0, size: 10};

        const exams = getExams({...JSON.parse(filtersCacheExam),...examParms}, this._examService);
        const sentExams = getSentExams({...JSON.parse(filtersCacheSentExam),...sentExamParams}, this._examService);
        const candidates = searchCandidates(params, this._candidateService, this._candidateMapper);
        const tests = searchTests(params, this._testService);
        const corrections = searchCorrections(params, this._correctionService, this._correctionMapper);

        return forkJoin(exams, sentExams, candidates, tests, corrections)
            .pipe(map(response => ({
                data: {
                    exams: response[0],
                    sentExams: response[1],
                    candidates: response[2],
                    tests: response[3],
                    corrections: response[4],
                    filtersValuesExam : filtersCacheExam,
                    filtersValuesSentExam:filtersCacheSentExam,
                    examTableFiltersConfig: examTableFiltersConfig,
                    sentExamTableFiltersConfig: sentExamTableFiltersConfig
                }
            })),
                catchError(error => {
                    return of({
                        data: null,
                        message: 'Error on exam management resolver, data couldn\' be fetched',
                        error
                    });
                }));

    }
}
