import {Observable} from 'rxjs';
import {IExam} from '../domain/IExam';
import {IWebExamIn} from '../domain/IWebExamIn';
import {IWebExamOut} from '../domain/IWebExamOut';
import {IWebExamList} from '../domain/IWebExamList';
import {ISentExamList} from '../../sent-exam/domain/ISentExamList';

export abstract class AbstractExamService {
    abstract get(id: string): Observable<IExam>;

    abstract update(idExam: string, exam: IWebExamOut): Observable<IWebExamIn>;

    abstract updateCorrection(idExam: string, idCorrection: string): Observable<IWebExamIn>;

    abstract createExam(exam: IWebExamOut): Observable<IWebExamIn>;

    abstract search(query: any): Observable<IWebExamList>;

    abstract getSentExams(query: any): Observable<ISentExamList>;

    abstract sendExam(idCandidateEmail: string, idExam: string): Observable<any>;

    abstract resend(idExamSent: string): Observable<any>;

    abstract restart(idExamSent: string): Observable<any>;
}
