import {AbstractExamService} from '../infrastructure/abstract-exam-service';
import {IWebExamOut} from '../domain/IWebExamOut';

export function updateExam(Exam: IWebExamOut, service: AbstractExamService) {
    return service.update(Exam.id, Exam);
}
