import {AbstractExamService} from '../infrastructure/abstract-exam-service';


export function getExams(params: any, service: AbstractExamService) {
    return service.search(params);
}
