import {AbstractExamService} from '../infrastructure/abstract-exam-service';

export function updateExamCorrection(examId: string, correctionId: string, service: AbstractExamService) {
    return service.updateCorrection(examId, correctionId);
}
