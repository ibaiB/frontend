import {AbstractExamService} from '../infrastructure/abstract-exam-service';

export function searchExams(params: any, service: AbstractExamService){
    return service.search(params);
}
