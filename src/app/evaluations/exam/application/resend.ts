import {AbstractExamService} from '../infrastructure/abstract-exam-service';

export function resend(id: string, service: AbstractExamService) {
    return service.resend(id);
}
