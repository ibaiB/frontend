import {AbstractExamService} from '../infrastructure/abstract-exam-service';

export function restart(id: string, service: AbstractExamService) {
    return service.restart(id);
}
