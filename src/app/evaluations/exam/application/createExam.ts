import {IWebExamOut} from '../domain/IWebExamOut';
import {AbstractExamService} from '../infrastructure/abstract-exam-service';

export function createExam(exam: IWebExamOut, service: AbstractExamService) {
    return service.createExam(exam);
}
