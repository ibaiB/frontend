import {AbstractExamService} from '../infrastructure/abstract-exam-service';

// export function sendExam(idCandidate: string, email: string, idExam: string, service: AbstractExamService) {
//     return service.sendExam(idCandidate, email, idExam);
// }
export function sendExam(idCandidateEmail: string, idExam: string, service: AbstractExamService) {
    return service.sendExam(idCandidateEmail, idExam);
}