import {AbstractExamService} from '../../exam/infrastructure/abstract-exam-service';

export function getSentExams(params: any, service: AbstractExamService) {
    return service.getSentExams(params);
}
