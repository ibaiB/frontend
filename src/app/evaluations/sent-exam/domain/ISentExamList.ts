import {ISentExam} from './ISentExam';

export interface ISentExamList {
    content: ISentExam[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
