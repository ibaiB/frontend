import {IWebSentExam} from './IWebSentExam';

export interface IWebSentExamList {
    content: IWebSentExam[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
