export interface IWebSentExam {
    id: string;
    email: string;
    examSentAt: string;
    examStartedAt: string;
    examEndAt: string;
    examId: string;
    examName: string;
    correctionId: string;
    correctionName: string;
    testId: string;
    testName: string;
}