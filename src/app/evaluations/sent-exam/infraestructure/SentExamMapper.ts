import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any'
})
export class SentExamMapper {
    mapTo(params: any) {
        const {
            id,
            email,
            examSentAt,
            examStartedAt,
            examEndAt,
            exam_id,
            examName,
            correction_id,
            correction_name,
            test_id,
            test_name,
        } = params;

        return {
            id: id,
            email: email,
            examSentAt: examSentAt,
            examStartedAt: examStartedAt,
            examEndAt: examEndAt,
            examId: exam_id,
            examName: examName,
            correctionId: correction_id,
            correctionName: correction_name,
            testId: test_id,
            testName: test_name
        };
    }
}
