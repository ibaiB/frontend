import {Injectable} from '@angular/core';
import {IList} from 'src/app/shared/generics/IList';
import {ICorrection} from '../domain/ICorrection';
import {IWebCorrection} from '../domain/IWebCorrection';

@Injectable({
    providedIn: 'any',
})
export class CorrectionMapper {
    constructor() {
    }

    mapTo(params: any): ICorrection {
        const {
            id,
            name,
            description,
            counters,
            idTest,
        } = params;

        return {
            id: id,
            name: name,
            description: description,
            counters: counters,
            idTest: idTest,
        };
    }

    mapFrom(params: any): IWebCorrection {
        const {
            id,
            name,
            description,
            counters,
            idTest,
        } = params;

        return {
            id: id,
            name: name,
            description: description,
            counters: counters,
            idTest: idTest,
        };
    }

    mapList(list: any): IList<ICorrection>{
        return {
            content: list.content,
            // TODO content: list.content.map(c => this.mapTo(c)),
            numberOfElements: list.numberOfElements,
            totalElements: list.totalElements,
            totalPages: list.totalPages
        }
    }
}
