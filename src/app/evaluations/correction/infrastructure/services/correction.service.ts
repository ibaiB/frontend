import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {IWebCorrection} from '../../domain/IWebCorrection';
import {IWebCorrectionList} from '../../domain/IWebCorrectionList';
import {CorrectionAbstractService} from '../CorrectionAbstractService';


@Injectable({
  providedIn: 'any'
})
export class CorrectionService extends CorrectionAbstractService {
  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'corrections';

  constructor(private _http: HttpClient) {
    super();
  }

  find(id: string): Observable<IWebCorrection> {
    return this._http.get<IWebCorrection>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  findAll(params: any): Observable<IWebCorrectionList> {
    return this._http.get<IWebCorrectionList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: params})});
  }

  create(correction: IWebCorrection): Observable<IWebCorrection> {
    return this._http.post<IWebCorrection>(`${this.BASE_URL}${this.ENDPOINT}`, correction);
  }

  delete(id: string): Observable<IWebCorrection> {
    return this._http.delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  update(correction: IWebCorrection): Observable<IWebCorrection> {
    return this._http.put<IWebCorrection>(`${this.BASE_URL}${this.ENDPOINT}/${correction.id}`, correction);
  }
}
