import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ICreateCorrectionResolved} from '../../domain/ICreateCorrectionResolved';
import {Observable, of} from 'rxjs';
import {TestService} from '../../../test/infrastructure/services/test.service';
import {TestMapper} from '../../../test/infrastructure/TestMapper';
import {getTest} from '../../../test/application/getTests';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'any'
})
export class CreateCorrectionResolverService implements Resolve<ICreateCorrectionResolved> {

  constructor(private _testService: TestService, private _testMapper: TestMapper) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICreateCorrectionResolved> | Promise<ICreateCorrectionResolved> | ICreateCorrectionResolved {

    return getTest(route.params['id'], this._testService).pipe(
      map(response => ({
        data: {
          test: this._testMapper.mapTo(response)
        }
      })),
      catchError(error => {
        return of({
          data: null,
          error: { message: 'Error on create correction resolver, data couldn\'t be fetched', error: error }
        })
      })
    );
  }
}
