import {TestBed} from '@angular/core/testing';

import {CreateCorrectionResolverService} from './create-correction-resolver.service';

describe('CreateCorrectionResolverService', () => {
  let service: CreateCorrectionResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateCorrectionResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
