import {Observable} from 'rxjs';
import {IWebCorrection} from '../domain/IWebCorrection';
import {IWebCorrectionList} from '../domain/IWebCorrectionList';

export abstract class CorrectionAbstractService {

  abstract find(params: any): Observable<IWebCorrection>;

  abstract findAll(params: any): Observable<IWebCorrectionList>;

  abstract create(correction: IWebCorrection): Observable<IWebCorrection>;

  abstract delete(id: string): Observable<IWebCorrection>;

  abstract update(correction: IWebCorrection): Observable<IWebCorrection>;

}
