import {CorrectionAbstractService} from '../infrastructure/CorrectionAbstractService';

export function deleteCorrection(correctionId: string, service: CorrectionAbstractService) {
    return service.delete(correctionId);
}
