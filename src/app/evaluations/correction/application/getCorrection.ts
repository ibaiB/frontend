import {CorrectionAbstractService} from '../infrastructure/CorrectionAbstractService';

export function getCorrection(correctionId: string, service: CorrectionAbstractService) {
    return service.find(correctionId);
}
