import {map} from 'rxjs/operators';
import {ICorrection} from '../domain/ICorrection';
import {CorrectionAbstractService} from '../infrastructure/CorrectionAbstractService';
import {CorrectionMapper} from '../infrastructure/CorrectionMapper';

export function updateCorrection(correction: ICorrection, service: CorrectionAbstractService, mapper: CorrectionMapper) {
    return service.update(mapper.mapFrom(correction)).pipe(map(mapper.mapTo));
}
