import {ICorrection} from '../domain/ICorrection';
import {CorrectionAbstractService} from '../infrastructure/CorrectionAbstractService';
import {CorrectionMapper} from '../infrastructure/CorrectionMapper';
import {map} from 'rxjs/operators';

export function createCorrection(correction: ICorrection, service: CorrectionAbstractService, mapper: CorrectionMapper) {
    return service.create(mapper.mapFrom(correction)).pipe(map(mapper.mapTo));
}
