import {map} from 'rxjs/operators';
import {CorrectionAbstractService} from '../infrastructure/CorrectionAbstractService';
import {CorrectionMapper} from '../infrastructure/CorrectionMapper';

export function searchCorrections(query: any, service: CorrectionAbstractService, mapper: CorrectionMapper) {
    return service.findAll(query).pipe(map(mapper.mapList));
}
