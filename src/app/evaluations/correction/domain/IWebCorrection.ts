import {ICounter} from '../../counter/domain/ICounter';

export interface IWebCorrection {
    id?: string;
    name: string;
    description: string;
    counters: ICounter[];
    idTest: string;
}
