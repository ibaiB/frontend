import {ICounter} from '../../counter/domain/ICounter';

export interface ICorrection {
    id?: string;
    name: string;
    description: string;
    counters: ICounter[];
    idTest: string;
}
