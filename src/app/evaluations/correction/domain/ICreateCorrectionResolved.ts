import {ICreateCorrectionData} from './ICreateCorrectionData';

export interface ICreateCorrectionResolved {
    data: ICreateCorrectionData;
    error?: any;
}
