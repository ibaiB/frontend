import {IWebCorrection} from './IWebCorrection';

export interface IWebCorrectionList {
    content: IWebCorrection[];
    totalElements: number;
    numberOfElements: number;
    totalPages: number;
}
