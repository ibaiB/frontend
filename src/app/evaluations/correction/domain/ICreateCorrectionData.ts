import {ITest} from '../../test/domain/ITest';

export interface ICreateCorrectionData {
    test: ITest;
}
