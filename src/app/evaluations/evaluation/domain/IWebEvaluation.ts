export interface IWebEvaluationIn {
    description: string,
    name: string,
    steps: [
        {
            name: string,
            questions: [
                {
                    answers: [
                        {
                            id: string,
                            message: string,
                            value: string
                        }
                    ],
                    description: string,
                    id: string,
                    name: string,
                    questionType: string,
                    step: string
                }
            ]
        }
    ]
}

export interface IWebEvaluationOut {
    responses: [
        {
            answer_id: string,
            answer_text: string,
            question_id: string
        }
    ]
}