export interface IEvaluationIn {
    description: string,
    name: string,
    steps: IStep[]
}

export interface IStep{
    name: string,
    questions: IQuestion[]
}

export interface IQuestion{
    description: string,
    id: string,
    name: string,
    questionType: string,
    step: string
    answers: IAnswer[]
}

export interface IAnswer{
    id: string,
    message: string,
    value: string
}

export interface IEvaluationOut {
    responses: IResponse[]
}

export interface IResponse {
    answer_id: string,
    answer_text: string,
    question_id: string
}
