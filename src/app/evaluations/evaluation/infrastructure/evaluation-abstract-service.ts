import {IWebEvaluationIn, IWebEvaluationOut} from '../domain/IWebEvaluation';
import {Observable} from 'rxjs';

export abstract class EvaluationAbstractService {
    abstract start(token: string): Observable<IWebEvaluationIn>;

    abstract end(response: IWebEvaluationOut, token: string): Observable<any>;
}

