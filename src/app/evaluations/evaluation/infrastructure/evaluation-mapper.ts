import {IEvaluationIn} from '../domain/IEvaluation';
import {IWebEvaluationOut} from '../domain/IWebEvaluation';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class EvaluationMapper {
    //TODO: only maps first level
    mapToIn(object: any): IEvaluationIn {
        const {description, name, steps} = object;

        return {
            description: description,
            name: name,
            steps: steps
        }
    }

    mapFromOut(object: any): IWebEvaluationOut {
        const {responses} = object;
        return {
            responses: responses
        }
    }
}
