import {EvaluationAbstractService} from '../infrastructure/evaluation-abstract-service';
import {EvaluationMapper} from '../infrastructure/evaluation-mapper';
import {IEvaluationOut} from '../domain/IEvaluation';

export function endEvaluation(evaluation: IEvaluationOut, token: string, service: EvaluationAbstractService, mapper: EvaluationMapper) {
    return service.end(mapper.mapFromOut(evaluation), token);
}
