import {EvaluationAbstractService} from '../infrastructure/evaluation-abstract-service';
import {EvaluationMapper} from '../infrastructure/evaluation-mapper';
import {map} from 'rxjs/operators';

export function startEvaluation(id: string, service: EvaluationAbstractService, mapper: EvaluationMapper) {
    return service.start(id).pipe(map(mapper.mapToIn));
}
