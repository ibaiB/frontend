import {TestAbstractService} from '../../test/infrastructure/TestAbstractService';

export function removeQuestion(questionId: string, testId: string, service: TestAbstractService) {
    return service.removeQuestion(questionId, testId);
}
