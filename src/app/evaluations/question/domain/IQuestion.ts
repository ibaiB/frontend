import {IAnswer} from '../../answer/domain/IAnswer';

export interface IQuestion {
    id?: string;
    name: string;
    description: string;
    step: string;
    questionType: string;
    answers: IAnswer[];
}
