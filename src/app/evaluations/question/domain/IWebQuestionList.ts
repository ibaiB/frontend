import {IWebQuestion} from './IWebQuestion';

export interface IWebQuestionList {
    content: IWebQuestion[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
