import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class QuestionMapper {
    mapTo(params: any) {
        const {
            id,
            name,
            description,
            step,
            questionType,
            answers,
        } = params;

        return {
            id: id,
            name: name,
            description: description,
            step: step,
            questionType: questionType,
            answers: answers,
        };
    }

    mapFrom(params: any) {
        const {
            id,
            name,
            description,
            step,
            questionType,
            answers,
        } = params;

        return {
            id: id,
            name: name,
            description: description,
            step: step,
            questionType: questionType,
            answers: answers,
        };
    }
}
