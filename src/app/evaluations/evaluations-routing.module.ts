import {RouterModule, Routes} from '@angular/router';
import {NewTestComponent} from './test/infrastructure/ng-components/new-test/new-test.component';
import {TestManagementComponent} from './test/infrastructure/ng-components/test-management/test-management.component';
import {TestManagementResolverService} from './test/infrastructure/resolvers/test-management-resolver.service';
import {TestDetailComponent} from './test/infrastructure/ng-components/test-detail/test-detail.component';
import {TestDetailResolverService} from './test/infrastructure/resolvers/test-detail-resolver.service';
import {EditTestComponent} from './test/infrastructure/ng-components/edit-test/edit-test.component';
import {EditTestResolverService} from './test/infrastructure/resolvers/edit-test-resolver.service';
import {CreateCorrectionComponent} from './test/infrastructure/ng-components/create-correction/create-correction.component';
import {CreateCorrectionResolverService} from './correction/infrastructure/resolvers/create-correction-resolver.service';
import {EditCorrectionComponent} from './test/infrastructure/ng-components/edit-correction/edit-correction.component';
import {ExamsManagmentComponent} from './exam/infrastructure/ng-components/exams-managment/exams-managment.component';
import {ExamManagementResolverService} from './exam/infrastructure/resolvers/exam-management.resolver.service';
import {ModuleWithProviders} from '@angular/core';

const routes: Routes = [
    {
        path: 'tests',
        data: {breadcrumb: 'Tests'},
        children: [
            {
                path: 'new',
                data: {breadcrumb: 'New Test'},
                component: NewTestComponent,
            },
            {
                path: '',
                data: {breadcrumb: 'Management'},
                component: TestManagementComponent,
                resolve: {response: TestManagementResolverService}
            },
            {
                path: 'test-detail/:id',
                component: TestDetailComponent,
                data: {breadcrumb: 'detail/:id'},
                resolve: {response: TestDetailResolverService}
            },
            {
                path: 'edit/:id',
                component: EditTestComponent,
                data: {breadcrumb: 'edit/:id'},
                resolve: {response: EditTestResolverService}
            },
            {
                path: 'test-correction/:id',
                component: CreateCorrectionComponent,
                data: {breadcrumb: 'Test Correction/:id'},
                resolve: {response: CreateCorrectionResolverService}
            },
            {
                path: ':idTest/test-correction/edit/:id',
                component: EditCorrectionComponent,
                data: {breadcrumb: ':idTest/Test Correction/:id'}
            }
        ]
    },
    {
        path: 'exams',
        data: {breadcrumb: 'Exams'},
        component: ExamsManagmentComponent,
        resolve: {response: ExamManagementResolverService}
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
