import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';
import {IWebQuestion} from '../../question/domain/IWebQuestion';
import {IWebTest} from '../domain/IWebTest';

export abstract class TestAbstractService {
    abstract findAll(params: any): Observable<IList<IWebTest>>;

    abstract create(test: IWebTest): Observable<IWebTest>;

    abstract delete(idTest: string): Observable<IWebTest>;

    abstract update(test: IWebTest): Observable<IWebTest>;

    abstract addQuestion(question: IWebQuestion, idTest: string): Observable<IWebTest>;

    abstract removeQuestion(idQuestion: string, idTest: string): Observable<IWebTest>;

    abstract finbById(id: string): Observable<IWebTest>;
}
