import {Injectable} from '@angular/core';
import {ITest} from '../domain/ITest';

@Injectable({
    providedIn: 'any'
})
export class TestMapper {
    mapTo(params: any): ITest {
        const {
            description,
            id,
            name,
            questions,
        } = params;

        return {
            description: description,
            id: id,
            name: name,
            questions: questions,
        };
    }

    mapFrom(params: any) {
        const {
            description,
            id,
            name,
            questions,
        } = params;

        return {
            description: description,
            id: id,
            name: name,
            questions: questions,
        };
    }
}
