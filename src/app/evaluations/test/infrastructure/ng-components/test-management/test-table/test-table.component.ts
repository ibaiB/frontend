import {IAuto} from 'src/app/shared/custom-mat-elements/dynamic';
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {SessionService} from '../../../../../../auth/session/infrastructure/services/session.service';
import {searchTests} from '../../../../application/searchTests';
import {ITest} from '../../../../domain/ITest';
import {IWebTestList} from '../../../../domain/IWebTestList';
import {TestService} from '../../../services/test.service';
import {table} from './config';
import {DynamicTableMarkIiComponent} from '../../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {TestMapper} from 'src/app/evaluations/test/infrastructure/TestMapper';

@Component({
    selector: 'app-test-table',
    templateUrl: './test-table.component.html',
    styleUrls: ['./test-table.component.scss'],
})
export class TestTableComponent implements OnInit {


    @Input() tests: IWebTestList;

    tableData: any;
    filters: IAuto[] = [];
    tableConfig = table;
    queryPagination = {page: 0, size: 10};
    queryOrder = {orderField: '', order: ''};
    queryFilters = {};

    loading: boolean = true;
    query: any = {};
    configVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private _testService: TestService,
        private _router: Router,
        private _sessionService: SessionService,
        private _route: ActivatedRoute,
        private _testMapper: TestMapper
    ) {
    }

    ngOnInit(): void {
        this._buildFilters();
        this.tableData = this.tests;
        this.loading = false;
    }

    doAction(event: { action: string, item: ITest }) {
        if (event.action === 'Add Correction') {
            this._router.navigate([`test-correction/${event.item.id}`], {relativeTo: this._route});
        }
    }

    navigate(event: { id: string, property: string }) {
        if (event.property === 'id') {
            this._router.navigate([`test-detail/${event.id}`], {relativeTo: this._route});
        }
    }

    setSize(event: PageEvent) {
        this.backSearch();
    }

    backSearch() {
        searchTests({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._testService)
            .subscribe(result => {
                this.tableData = {
                    content: result.content,
                    totalElements: result.totalElements,
                    numberOfElements: result.numberOfElements,
                    totalPages: result.totalPages
                };
            });
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const mapped = this._testMapper.mapFrom(params);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this.dynamicTable.resetPageIndex();

        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this.backSearch();
    }

    orderChanged(event: any) {
        let aux = {[event.orderField]: ''};
        aux = this._testMapper.mapFrom(aux);

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;

        this.backSearch();
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Id',
                placeholder: 'Id',
                shown: true,
            },
            {
                options: [],
                prop: 'name',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Name',
                placeholder: 'Name',
                shown: true,
            },
            {
                options: [],
                prop: 'description',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Description',
                placeholder: 'Description',
                shown: true,
            }
        ];
    }
}
