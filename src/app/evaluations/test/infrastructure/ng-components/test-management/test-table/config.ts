import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
    multiSelect: false,
    columns: [
        {
            name: 'Id',
            prop: 'id',
            shown: true,
            route: './app/evaluations/tests/test-detail/',
            cellClass: 'id-table-column',
            routeId: 'id'
        },
        {
            name: 'Name',
            prop: 'name',
            shown: true,
        },
        {
            name: 'Description',
            prop: 'description',
            shown: true,
        },
    ],
    actions: [
        {
            name: 'Add Correction',
            icon: 'control_point'
        }
    ]
};
