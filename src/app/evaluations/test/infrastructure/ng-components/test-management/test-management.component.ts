import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ResolvedData} from '../../../../../shared/generics/IResolvedData';
import {ITestTable} from '../../../domain/ITestTable';
import {IWebTestList} from '../../../domain/IWebTestList';
import {ITest} from '../../../domain/ITest';
import {PageEvent} from '@angular/material/paginator';
import {searchTests} from '../../../application/searchTests';
import {createTableFiltersConfig, IAuto, parseTableFiltersConfig} from '../../../../../shared/custom-mat-elements/dynamic';
import {table} from './test-table/config';
import {TestService} from '../../services/test.service';
import {SessionService} from '../../../../../auth/session/infrastructure/services/session.service';
import {TestMapper} from '../../TestMapper';
import {DynamicTableMarkIiComponent} from '../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';


@Component({
    selector: 'app-test-management',
    templateUrl: './test-management.component.html',
    styleUrls: ['./test-management.component.scss']
})
export class TestManagementComponent implements OnInit {

    private _resolved: ResolvedData<ITestTable>;
    tests: IWebTestList;

    resolvedData: ITestTable;
    tableData: any;
    filters: IAuto[] = [];
    tableConfig = table;
    queryPagination;
    queryOrder;
    queryFilters;
    configVisibility: boolean = false;

    loading: boolean = true;
    query: any = {};

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private _testService: TestService,
        private _router: Router,
        private _sessionService: SessionService,
        private _route: ActivatedRoute,
        private _testMapper: TestMapper,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit(): void {
        this._resolved = this._route.snapshot.data.response;

        if (this._resolved.data) {
            this._buildFilters();
            parseTableFiltersConfig(this._resolved.data.tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = this._resolved.data.filtersValue ? this._clean((JSON.parse(this._resolved.data.filtersValue))) : {};
            this.tableConfig.pagination = this._resolved.data.tableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = this._resolved.data.tableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries(this._testMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
            this.tableData = this._resolved.data.tests;
            this.loading = false;
        }
        this.configFilterValue();
    }

    configFilterValue() {
        let queryFiltersAux = this._resolved.data.filtersValue ? this._clean(this._testMapper.mapTo(JSON.parse(this._resolved.data.filtersValue))) : {};

        this.filters.forEach(filter => {
            filter.defaultValue = queryFiltersAux[this.getFilterProp(filter)];
            filter.value = queryFiltersAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    doAction(event: { action: string, item: ITest }) {
        if (event.action === 'Add Correction') {
            this._router.navigate([`test-correction/${event.item.id}`], {relativeTo: this._route});
        }
    }

    navigate(event: { id: string, property: string }) {
        if (event.property === 'id') {
            this._router.navigate([`test-detail/${event.id}`], {relativeTo: this._route});
        }
    }

    setSize(event: PageEvent) {
        this.backSearch();
    }

    backSearch() {
        searchTests({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._testService)
            .subscribe(result => {
                this.tableData = {
                    content: result.content,
                    totalElements: result.totalElements,
                    numberOfElements: result.numberOfElements,
                    totalPages: result.totalPages
                };
            });
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const mapped = this._testMapper.mapFrom(params);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('testFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this.dynamicTable.resetPageIndex();
        this._cookieFacade.save('test-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('test-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        let aux = {[event.orderField]: ''};
        aux = this._testMapper.mapFrom(aux);

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('test-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('test-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Id',
                placeholder: 'Id',
                shown: true,
            },
            {
                options: [],
                prop: 'name',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Name',
                placeholder: 'Name',
                shown: true,
            },
            {
                options: [],
                prop: 'description',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Description',
                placeholder: 'Description',
                shown: true,
            }
        ];
    }

}
