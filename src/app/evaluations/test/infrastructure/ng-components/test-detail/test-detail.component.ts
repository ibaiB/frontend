import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {deleteCorrection} from 'src/app/evaluations/correction/application/deleteCorrection';
import {ICorrection} from 'src/app/evaluations/correction/domain/ICorrection';
import {IWebCorrection} from 'src/app/evaluations/correction/domain/IWebCorrection';
import {ITest} from 'src/app/evaluations/test/domain/ITest';
import {CorrectionService} from 'src/app/evaluations/correction/infrastructure/services/correction.service';
import {errorAlert, questionAlert, successAlert} from 'src/app/shared/error/custom-alerts';
import {IList} from 'src/app/shared/generics/IList';

@Component({
  selector: 'app-test-detail',
  templateUrl: './test-detail.component.html',
  styleUrls: ['./test-detail.component.scss'],
})
export class TestDetailComponent implements OnInit {

  test: ITest;
  //TODO this shouldn´t be a web entity
  corrections: IList<IWebCorrection>;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _correctionService: CorrectionService) {

  }

  ngOnInit(): void {
    this.test = this._route.snapshot.data["response"].data.test;
    this.corrections = this._route.snapshot.data["response"].data.corrections;
  }

  editTest() {
    this._router.navigate(['../../edit/' + this.test.id], { relativeTo: this._route });
  }

  editCorrection(correction: ICorrection) {
    this._router.navigate([`../../${this.test.id}/test-correction/edit/`, correction.id],
    { relativeTo: this._route, state: { test: this.test, correction } });
  }

  addCorrection() {
    this._router.navigate(['../../test-correction/' + this.test.id], { relativeTo: this._route })
  }

  deleteCorrection(event, correction: ICorrection){
    event.stopPropagation();
    questionAlert('This question will be deleted').then(result => {
      if (result.isConfirmed){
        deleteCorrection(correction.id, this._correctionService)
        .subscribe(data => {
          successAlert('Correction deleted');
          this.corrections.content = this.corrections.content.filter(c => c.id !== correction.id);
        }, error => {
          console.error('Error on delete correction', error);
          const { error: { message, id } } = error;
          errorAlert(`Couldn\'t delete correction`, message, id);
        });
      }
    });
  }
}
