import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditCorrectionComponent} from './edit-correction.component';

describe('EditCorrectionComponent', () => {
  let component: EditCorrectionComponent;
  let fixture: ComponentFixture<EditCorrectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCorrectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCorrectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
