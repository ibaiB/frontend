import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {getCorrection} from 'src/app/evaluations/correction/application/getCorrection';
import {updateCorrection} from 'src/app/evaluations/correction/application/updateCorrection';
import {ICorrection} from 'src/app/evaluations/correction/domain/ICorrection';
import {CorrectionMapper} from 'src/app/evaluations/correction/infrastructure/CorrectionMapper';
import {getTest} from 'src/app/evaluations/test/application/getTests';
import {ITest} from 'src/app/evaluations/test/domain/ITest';
import {CorrectionService} from 'src/app/evaluations/correction/infrastructure/services/correction.service';
import {TestService} from 'src/app/evaluations/test/infrastructure/services/test.service';
import {questionAlert, successAlert} from '../../../../../shared/error/custom-alerts';


const defaultTest: ITest = {
    name: '',
    description: '',
    questions: []
};

const defaultCorrection: ICorrection = {
    name: '',
    description: '',
    counters: [],
    idTest: ''
};

//TODO decouple this interfaces
interface AnswerCounter {
    id?: string;
    calcValue: number;
    idAnswer: string;
}

interface Counter {
    id?: string;
    answerCounters: AnswerCounter[];
    description: string;
    name: string;
    isClicked?: boolean;
}

@Component({
    selector: 'app-edit-correction',
    templateUrl: './edit-correction.component.html',
    styleUrls: ['./edit-correction.component.scss']
})
export class EditCorrectionComponent implements OnInit {

    test: ITest = defaultTest;
    correction: ICorrection;
    counterForm: FormGroup;
    counters: Counter[] = [];
    counterIndex: number = -1;
    loading = true;
    disabledInput = true;

    constructor(
        private _fb: FormBuilder,
        private _route: ActivatedRoute,
        private _router: Router,
        private _service: CorrectionService,
        private _mapper: CorrectionMapper,
        private _correctionService: CorrectionService,
        private _testService: TestService
    ) {
        const nav = this._router.getCurrentNavigation();
        if (nav) {
            const state = nav.extras.state;
            this.test = state ? state.test : defaultTest;
            this.correction = state ? state.correction : defaultCorrection;
        } else {
            this.test = defaultTest;
            this.correction = defaultCorrection;
        }
    }

    ngOnInit(): void {
        this._route.params.subscribe(p => {
            getTest(p.idTest, this._testService).subscribe(test => {
                this.test = test;
                getCorrection(p.id, this._correctionService).subscribe(correction => {
                    this.correction = correction;
                    console.log('correction', this.correction);
                    this.loading = false;
                    this._initCounterForm();
                });
            });
        });


    }


    private _initCounterForm(counter?) {
        const config = {
            name: [counter ? counter.name : '', Validators.required],
            description: [counter ? counter.description : ''],
        };
        this.test.questions.forEach(question => {
            question.answers.forEach(answer => {
                config[answer.id] = [{value: '0', disabled: this.disabledInput}];
            });
        });

        this.counterForm = this._fb.group(config);

        if (counter) {
            counter.answerCounters.forEach(ac => {
                this.counterForm.get(ac.idAnswer).setValue(ac.calcValue);
            });
        }
    }

    addCounter() {
        if (this.counterForm.valid) {
            const counter = this._formatCounter();
            this.correction.counters.push(counter);
            this._initCounterForm();
        }
    }

    private _formatCounter(): Counter {
        const {name, description} = this.counterForm.value;
        const keys = Object.keys(this.counterForm.value);
        const answersCounters: AnswerCounter[] = [];

        keys.forEach((key) => {
            if (key !== 'name' && key !== 'description') {
                answersCounters.push({
                    calcValue: Number.parseInt(this.counterForm.value[key]),
                    idAnswer: key,
                });
            }
        });

        return {
            name: name,
            description: description,
            answerCounters: answersCounters,
        };
    }

    editCounter(counter: Counter, index: number) {
        this.disabledInput = false;
        this.counterIndex = index;
        this._initCounterForm(counter);
    }

    confirmEditCounter(event, counter?: Counter) {
        event.stopPropagation();
        if (this.counterForm.valid) {
            const updatedCounter = this._formatCounter();
            if (counter.id) {
                updatedCounter.answerCounters.forEach(ac => {
                    const c = counter.answerCounters.find(cac => cac.idAnswer === ac.idAnswer);
                    if (c) {
                        c.calcValue = ac.calcValue;
                    } else {
                        const answerCounter: AnswerCounter = {
                            idAnswer: ac.idAnswer,
                            calcValue: ac.calcValue
                        };
                        counter.answerCounters.push(answerCounter);
                    }
                });
            } else {
                counter = updatedCounter;
            }
            this.counterIndex = -1;
            this._disableInputs();
        }
    }

    private _disableInputs() {
        this.counterForm.disable();
        this.counterForm.get('name').enable();
        this.counterForm.get('description').enable();

    }

    cancelEditCounter(event) {
        event.stopPropagation();
        this._initCounterForm();
        this.counterIndex = -1;
        this._disableInputs();
    }

    removeCounter(counter: Counter, type: string) {
        questionAlert('This counter will be deleted').then((result) => {
            if (result.isConfirmed) {
                if (type === 'new') {
                    this.correction.counters = this.correction.counters.filter(c => c.id !== counter.id);
                    this.counterIndex = -1;
                } else {
                    this.correction.counters = this.correction.counters.filter(c => c.id !== counter.id);
                    this.counterIndex = -1;
                }
                this.disabledInput = true;
                this._disableInputs();
            }
        });
    }

    onCorrectionReset() {
        questionAlert('All your changes will be discarted!').then((result) => {
            if (result.isConfirmed) {
                const counter = this.correction.counters[this.counterIndex];
                this._initCounterForm(counter);
            }
        });
    }

    onCorrectCancel() {
        questionAlert(`Your correction won't be saved! Please consider saving first`).then((result) => {
            if (result.isConfirmed) {
                this._router.navigate(['../../../..'], {relativeTo: this._route});
            }
        });
    }

    onCorrectionSave() {
        questionAlert('Once sent you can update it').then((result) => {
            if (result.isConfirmed) {
                const correction: ICorrection = {
                    name: this.correction.name,
                    description: this.correction.description,
                    counters: [...this.correction.counters],
                    idTest: this.test.id,
                    id: this.correction.id
                };

                updateCorrection(correction, this._service, this._mapper).subscribe(
                    (correction) => {
                        //TODO think about what to do after success
                        successAlert('Correction was updated successfully');
                    }
                );
            }
        });
    }

}
