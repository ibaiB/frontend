import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {questionAlert, successAlert} from '../../../../../shared/error/custom-alerts';
import {createCorrection} from '../../../../correction/application/createCorrection';
import {ICorrection} from '../../../../correction/domain/ICorrection';
import {ICreateCorrectionResolved} from '../../../../correction/domain/ICreateCorrectionResolved';
import {CorrectionMapper} from '../../../../correction/infrastructure/CorrectionMapper';
import {ITest} from '../../../domain/ITest';
import {CorrectionService} from '../../../../correction/infrastructure/services/correction.service';

//TODO decouple this interfaces
interface AnswerCounter {
    calcValue: number;
    idAnswer: string;
}

interface Counter {
    answerCounters: AnswerCounter[];
    description: string;
    name: string;
    isClicked?: boolean;
}

@Component({
    selector: 'app-create-correction',
    templateUrl: './create-correction.component.html',
    styleUrls: ['./create-correction.component.scss'],
})
export class CreateCorrectionComponent implements OnInit {
    correctionForm: FormGroup;
    counterForm: FormGroup;

    counters: Counter[] = [];

    loading: boolean = true;
    showCounterPanel: boolean = false;
    showAlerts: boolean = true;
    showCountersForms: boolean = false;

    counterClicked = {
        backgroundColor: 'rgb(238, 238, 238)',
        transform: 'scale(0.95)',
    };

    counterIndex: number = -1;

    private _resolved: ICreateCorrectionResolved;
    test: ITest;

    constructor(
        private _fb: FormBuilder,
        private _route: ActivatedRoute,
        private _router: Router,
        private _service: CorrectionService,
        private _mapper: CorrectionMapper
    ) {
    }

    ngOnInit() {
        this._resolved = this._route.snapshot.data['response'];

        if (this._resolved.data) {
            this.test = this._resolved.data.test;
            this.loading = false;
            this._initCorrectionForm();
            this._initCounterForm();
        }
    }

    private _initCorrectionForm() {
        this.correctionForm = this._fb.group({
            name: ['', Validators.required],
            description: [''],
        });
    }

    private _initCounterForm(counter?: Counter) {
        let config = {
            name: [counter ? counter.name : '', Validators.required],
            description: [counter ? counter.description : ''],
        };

        if (!counter) {
            this.test.questions.forEach((question) => {
                question.answers.forEach((answer) => {
                    config[answer.id] = ['0'];
                });
            });
        } else {
            counter.answerCounters.forEach(ac => {
                config[ac.idAnswer] = [ac.calcValue];
            });
        }

        this.counterForm = this._fb.group(config);
    }

    addCounter() {
        if (this.counterForm.valid) {
            const counter = this._formatCounter();
            this.counters.push(counter);
            this._initCounterForm();
        }
    }

    private _formatCounter(): Counter {
        const {name, description} = this.counterForm.value;
        const keys = Object.keys(this.counterForm.value);
        const answersCounters: AnswerCounter[] = [];

        keys.forEach((key) => {
            if (key !== 'name' && key !== 'description') {
                answersCounters.push({
                    calcValue: Number.parseInt(this.counterForm.value[key]),
                    idAnswer: key,
                });
            }
        });

        return {
            name: name,
            description: description,
            answerCounters: answersCounters,
        };
    }

    editCounter(counter: Counter) {
        this.counterIndex = this.counters.indexOf(counter);
        this._initCounterForm(counter);
    }

    confirmEditCounter() {
        if (this.counterForm.valid) {
            const counter = this._formatCounter();
            this.counters[this.counterIndex] = counter;
            this._initCounterForm();
            this.counterIndex = -1;
        }
    }

    cancelEditCounter() {
        this._initCounterForm();
        this.counterIndex = -1;
    }

    removeCounter(counter: Counter) {
        questionAlert('This counter will be deleted').then((result) => {
            if (result.isConfirmed) {
                const index = this.counters.indexOf(counter);
                if (index > -1) {
                    this.counters.splice(index, 1);
                }
            }
        });
    }

    onCorrectionReset() {
        questionAlert('All your changes will be discarted!').then((result) => {
            if (result.isConfirmed) {
                this._initCorrectionForm();
                this._initCounterForm();
                this.counters = [];
            }
        });
    }

    onCorrectCancel() {
        questionAlert(`Your correction won't be saved! Please consider saving first`).then((result) => {
            if (result.isConfirmed) {
                this._router.navigate(['../..'], {relativeTo: this._route});
            }
        });
    }

    onCorrectionSave() {
        questionAlert('Once sent you can update it').then((result) => {
            if (result.isConfirmed) {
                const correction: ICorrection = {
                    ...this.correctionForm.value,
                    counters: this.counters,
                    idTest: this.test.id,
                };

                createCorrection(correction, this._service, this._mapper).subscribe(
                    () => {
                        successAlert('The test was created successfully');
                        this._initCorrectionForm();
                        this._initCounterForm();
                        this.counters = [];
                        this._router.navigate(['../..'], {relativeTo: this._route});
                    }
                );
            }
        });
    }
}
