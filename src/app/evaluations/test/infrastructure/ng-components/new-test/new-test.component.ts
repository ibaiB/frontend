import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {IAnswer} from 'src/app/evaluations/answer/domain/IAnswer';
import {IQuestion} from 'src/app/evaluations/question/domain/IQuestion';
import {QuestionMapper} from 'src/app/evaluations/question/infrastructure/QuestionMapper';
import {createTest} from 'src/app/evaluations/test/application/createTest';
import {ITest} from 'src/app/evaluations/test/domain/ITest';
import {TestMapper} from 'src/app/evaluations/test/infrastructure/TestMapper';
import {errorAlert, questionAlert, successAlert} from '../../../../../shared/error/custom-alerts';
import {dictionaries} from '../../../../../recruitment/dictionaries';
import {TestService} from '../../services/test.service';


@Component({
  selector: 'app-new-test',
  templateUrl: './new-test.component.html',
  styleUrls: ['./new-test.component.scss'],
})
export class NewTestComponent implements OnInit {
  // forms
  public formTest: FormGroup;
  public formQuestion: FormGroup;
  public formAnswer: FormGroup;

  // dictionaries
  public readonly testTypes = dictionaries.tests;
  public readonly questionTypes = dictionaries.inputTypes;

  // aux arrays
  public questions: IQuestion[] = [];

  // semaphores
  public showEditQuestion: boolean = false;
  public showEditAnswer: boolean = false;

  //state index
  questionIndex: number = -1;
  answerIndex: number = -1;

  constructor(
    private _fb: FormBuilder,
    private _service: TestService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _mapper: TestMapper,
    private _questionMapper: QuestionMapper
  ) { }

  ngOnInit(): void {
    this.formTestInit();
    this.formQuestionInit();
    this.formAnswerInit();
  }

  private formTestInit() {
    this.formTest = this._fb.group({
      name: ["", Validators.required],
      description: [""],
    });
  }

  private formQuestionInit(question?: IQuestion) {
    this.formQuestion = this._fb.group({
      description: [question ? question.description : ""],
      name: [question ? question.name : "", Validators.required],
      questionType: [
        question ? question.questionType : "",
        Validators.required,
      ],
      step: [
        question ? question.step : "",
        [Validators.required, Validators.pattern("([0-9])*")],
      ],
    });
  }

  private formAnswerInit(answer?: IAnswer) {
    this.formAnswer = this._fb.group({
      message: [answer ? answer.message : "", Validators.required],
      value: [answer ? answer.value : "", Validators.required],
    });
  }

  public addQuestion() {
    if (this.formQuestion.valid)
      this.questions.push({ ...this.formQuestion.value, answers: [] });
    this.formQuestionInit();
  }

  public confirmEditQuestion() {
    if (this.formQuestion.valid) {
      const answers = this.questions[this.questionIndex].answers;
      this.questions[this.questionIndex] = this.formQuestion.value;
      this.questions[this.questionIndex].answers = answers;
      this.formQuestionInit();
      this.showEditQuestion = false;
      this.questionIndex = -1;
    }
  }

  public cancelEditQuestion() {
    this.formQuestionInit();
    this.formAnswerInit();
    this.showEditQuestion = false;
    this.questionIndex = -1;
  }

  public editQuestion(question: IQuestion) {
    this.questionIndex = this.questions.indexOf(question);
    this.formQuestionInit(question);
    this.showEditQuestion = true;
  }

  public removeQuestion(question: IQuestion) {
    questionAlert('This question will be deleted').then((result) => {
      if (result.isConfirmed) {
        const index = this.questions.indexOf(question);
        if (index > -1) {
          this.questions.splice(index, 1);
        }
      }
    });
  }

  public addAnswer(question: IQuestion) {
    if (this.formAnswer.valid) question.answers.push(this.formAnswer.value);
    this.formAnswerInit();
  }

  public editAnswer(question: IQuestion, answer: IAnswer) {
    this.answerIndex = question.answers.indexOf(answer);
    this.formAnswerInit(answer);
    this.showEditAnswer = true;
  }

  public confirmEditAnswer(question: IQuestion) {
    if (this.formAnswer.valid) {
      question.answers[this.answerIndex] = this.formAnswer.value;
      this.formAnswerInit();
      this.showEditAnswer = false;
      this.answerIndex = -1;
    }
  }

  public cancelEditAnswer() {
    this.formAnswerInit();
    this.showEditAnswer = false;
    this.answerIndex = -1;
  }

  public removeAnswer(question: IQuestion, answer: IAnswer) {
    questionAlert('This answer will be deleted').then((result) => {
      if (result.isConfirmed) {
        const index = question.answers.indexOf(answer);
        if (index > -1) {
          question.answers.splice(index, 1);
        }
      }
    });
  }

  public onTestReset() {
    questionAlert('All your changes will be discarted!')
      .then((result) => {
        if (result.isConfirmed) {
          this.formTestInit();
          this.formQuestionInit();
          this.formAnswerInit();
          this.questions = [];
        }
      });
  }

  public onCancelClick() {
    questionAlert(`Your test won't be saved! Please consider saving first`)
      .then((result) => {
        if (result.isConfirmed) {
          this._router.navigate(["../"], { relativeTo: this._route });
        }
      });
  }

  public onTestSave() {
    questionAlert('Once sent you can update it')
      .then((result) => {
        if (result.isConfirmed && this.formTest.valid) {
        }
        const test: ITest = {
          name: this.formTest.get('name').value,
          description: this.formTest.get('description').value,
          questions: []
        };
        this.questions.forEach(question => {
          test.questions.push(question);
        });

        console.log(test);
        createTest(test, this._service, this._mapper).subscribe(() => {
          successAlert('Test was created successfully');
          this.formTestInit();
          this.formQuestionInit();
          this.formAnswerInit();
          this.questions = [];
        }, error => {
            console.error('Error on create test', error);
            const { error: { message, id } } = error;
            errorAlert(`Error creating test`, message, id);
        });
      });
  }
}
