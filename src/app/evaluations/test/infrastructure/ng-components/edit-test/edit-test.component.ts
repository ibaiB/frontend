import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {dictionaries} from 'src/app/recruitment/dictionaries';
import {IAnswer} from 'src/app/evaluations/answer/domain/IAnswer';
import {removeQuestion} from 'src/app/evaluations/question/application/removeQuestion';
import {IQuestion} from 'src/app/evaluations/question/domain/IQuestion';
import {QuestionMapper} from 'src/app/evaluations/question/infrastructure/QuestionMapper';
import {TestMapper} from 'src/app/evaluations/test/infrastructure/TestMapper';
import {TestService} from 'src/app/evaluations/test/infrastructure/services/test.service';
import {errorAlert, questionAlert, successAlert} from '../../../../../shared/error/custom-alerts';
import {ResolvedData} from '../../../../../shared/generics/IResolvedData';
import {updateTest} from '../../../application/updateTest';
import {IEditTestResolved} from '../../../domain/IEditTestResolved';
import {IWebTest} from '../../../domain/IWebTest';


@Component({
  selector: 'app-edit-test',
  templateUrl: './edit-test.component.html',
  styleUrls: ['./edit-test.component.scss']
})
export class EditTestComponent implements OnInit {
  private _resolved: ResolvedData<IEditTestResolved>;
  // forms
  public formTest: FormGroup;
  public formQuestion: FormGroup;
  public formAnswer: FormGroup;

  // dictionaries
  public readonly testTypes = dictionaries.tests;
  public readonly questionTypes = dictionaries.inputTypes;

  // aux arrays
  public questions: IQuestion[] = [];

  // semaphores
  public showEditQuestion: boolean = false;
  public showEditAnswer: boolean = false;

  //state index
  questionIndex: number = -1;
  answerIndex: number = -1;

  testId: string;
  test: IWebTest;
  constructor(
    private _fb: FormBuilder,
    private _service: TestService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _mapper: TestMapper,
    private _questionMapper: QuestionMapper
  ) {
  }

  ngOnInit(): void {
    this._resolved = this._route.snapshot.data['response'];

    if (this._resolved.error) {
      console.error(this._resolved.error.message, this._resolved.error.error);
      const { error: { message, id } } = this._resolved.error;
      errorAlert(`Couldn\'t retrieve data`, message, id);
    }

    if (this._resolved.data !== null) {
      this.test = this._resolved.data.test;
      this.questions = this._resolved.data.test.questions;
    }

    this.formTestInit();
    this.formQuestionInit();
    this.formAnswerInit();
  }

  private formTestInit() {
    this.formTest = this._fb.group({
      name: [this.test ? this.test.name : '', Validators.required],
      description: [this.test ? this.test.description : ''],
    });
  }

  private formQuestionInit(question?: IQuestion) {
    this.formQuestion = this._fb.group({
      description: [question ? question.description : ""],
      name: [question ? question.name : "", Validators.required],
      questionType: [
        question ? question.questionType : "",
        Validators.required,
      ],
      step: [
        question ? question.step : "",
        [Validators.required, Validators.pattern("([0-9])*")],
      ],
    });
  }

  private formAnswerInit(answer?: IAnswer) {
    this.formAnswer = this._fb.group({
      message: [answer ? answer.message : "", Validators.required],
      value: [answer ? answer.value : "", Validators.required],
    });
  }

  public addQuestion() {
    if (this.formQuestion.valid) {
      this.questions.push({ ...this.formQuestion.value, answers: [] });
    }
    this.formQuestionInit();
  }


  public confirmEditQuestion(){
    if (this.formQuestion.valid) {
      const questionId = this.questions[this.questionIndex].id;
      const answers = this.questions[this.questionIndex].answers;
      this.questions[this.questionIndex] = this.formQuestion.value;
      this.questions[this.questionIndex].answers = answers;
      this.questions[this.questionIndex].id = questionId;

      this.formQuestionInit();
      this.showEditQuestion = false;
      this.questionIndex = -1;
    }
  }

  public cancelEditQuestion() {
    this.formQuestionInit();
    this.formAnswerInit();
    this.showEditQuestion = false;
    this.questionIndex = -1;
  }

  public editQuestion(question: IQuestion) {
    this.questionIndex = this.questions.indexOf(question);
    this.formQuestionInit(question);
    this.showEditQuestion = true;
  }

  public removeQuestion(question: IQuestion) {
    questionAlert('This question will be deleted').then((result) => {
      if (result.isConfirmed) {
        questionAlert('This action will delete all answers related to this question for all sent exams').then(result2 =>{
          if (result2.isConfirmed){
            const index = this.questions.indexOf(question);
            if (index > -1) {
              this.questions.splice(index, 1);
            }
            removeQuestion(question.id, this.test.id, this._service).subscribe(test => console.log(test));
          }
        });
      }
    });
  }

  public addAnswer(question: IQuestion) {
    if (this.formAnswer.valid) question.answers.push(this.formAnswer.value);

    this.formAnswerInit();
  }

  public editAnswer(question: IQuestion, answer: IAnswer) {
    this.answerIndex = question.answers.indexOf(answer);
    this.formAnswerInit(answer);
    this.showEditAnswer = true;
  }

  public confirmEditAnswer(question: IQuestion) {
    if (this.formAnswer.valid) {
      const answerId = question.answers[this.answerIndex].id;
      question.answers[this.answerIndex] = this.formAnswer.value;
      question.answers[this.answerIndex].id = answerId;
      this.formAnswerInit();
      this.showEditAnswer = false;
      this.answerIndex = -1;
    }
  }

  public cancelEditAnswer() {
    this.formAnswerInit();
    this.showEditAnswer = false;
    this.answerIndex = -1;
  }

  public removeAnswer(question: IQuestion, answer: IAnswer) {
    questionAlert('This answer will be deleted').then((result) => {
      if (result.isConfirmed) {
        questionAlert('This action may cause changes in other parts of the application').then(result2 => {
          if (result2.isConfirmed){
            const index = question.answers.indexOf(answer);
            if (index > -1) {
              question.answers.splice(index, 1);
            }
          }
        });
      }
    });
  }


  public onTestReset() {
    questionAlert('All your changes will be discarted').then((result) => {
      if (result.isConfirmed) {
        this.formTestInit();
        this.formQuestionInit();
        this.formAnswerInit();
        this.questions = [];
      }
    });
  }

  public onCancelClick() {
    questionAlert(`Your test won't be saved! Please consider saving first`).then((result) => {
      if (result.isConfirmed) {
        this._router.navigate(["../../"], { relativeTo: this._route });
      }
    });
  }

  public onTestSave() {
    console.log(this.test);
    this.test.name = this.formTest.get('name').value;
    this.test.description = this.formTest.get('description').value;
    updateTest(this.test, this._service, this._mapper).subscribe(response => {
      successAlert('Test was created succesfully');
      this.formTestInit();
      this.formQuestionInit();
      this.formAnswerInit();
      this.questions = response.questions;
    },
    error => {
      console.error('Error on create test', error);
      const { error: { message, id } } = error;
      errorAlert(`Couldn\'t update test`, message, id);
    });
  }

}
