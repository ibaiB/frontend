import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment';
import {TestAbstractService} from '../TestAbstractService';
import {IWebQuestion} from '../../../question/domain/IWebQuestion';
import {IWebTest} from '../../domain/IWebTest';
import {IList} from 'src/app/shared/generics/IList';

@Injectable({
  providedIn: 'any',
})
export class TestService extends TestAbstractService {
  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'tests';

  constructor(private _http: HttpClient) {
    super();
  }

  findAll(params: any): Observable<IList<IWebTest>> {
    return this._http.get<IList<IWebTest>>(
      `${this.BASE_URL}${this.ENDPOINT}/search`,
      {params: new HttpParams({fromObject: this._clean(params)})});
  }
  create(test: IWebTest): Observable<IWebTest> {
    return this._http.post<IWebTest>(`${this.BASE_URL}${this.ENDPOINT}`, test);
  }
  delete(idTest: string): Observable<IWebTest> {
    return this._http.delete<IWebTest>(
      `${this.BASE_URL}${this.ENDPOINT}/${idTest}`
    );
  }
  update(test: IWebTest): Observable<IWebTest> {
    return this._http.put<IWebTest>(
      `${this.BASE_URL}${this.ENDPOINT}/${test.id}`,
      test
    );
  }
  addQuestion(question: IWebQuestion, idTest: string): Observable<IWebTest> {
    return this._http.post<IWebTest>(
      `${this.BASE_URL}${this.ENDPOINT}/${idTest}/questions`,
      question
    );
  }
  removeQuestion(idQuestion: string, idTest: string): Observable<IWebTest> {
    return this._http.delete<IWebTest>(
      `${this.BASE_URL}${this.ENDPOINT}/${idTest}/questions/${idQuestion}`
    );
  }
  finbById(id: string): Observable<IWebTest> {
    return this._http.get<IWebTest>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  private _clean(object): any { 
    const cleaned = {}; 
    const keys = Object.keys(object); 
    keys.forEach(key => { 
        if (object[key]) { cleaned[key] = object[key]; } 
    }); 
    return cleaned; 
}
}
