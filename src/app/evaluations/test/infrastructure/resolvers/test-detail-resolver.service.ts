import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot,} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ResolvedData} from 'src/app/shared/generics/IResolvedData';
import {searchCorrections} from '../../../correction/application/searchCorrections';
import {CorrectionMapper} from '../../../correction/infrastructure/CorrectionMapper';
import {getTest} from '../../application/getTests';
import {ITestResolved} from '../../domain/ITest';
import {TestMapper} from '../TestMapper';
import {CorrectionService} from '../../../correction/infrastructure/services/correction.service';
import {TestService} from '../services/test.service';

@Injectable({
  providedIn: 'any',
})
export class TestDetailResolverService implements Resolve<ResolvedData<ITestResolved>> {
  constructor(
      private _testService: TestService,
      private _testMapper: TestMapper,
      private _correctionService: CorrectionService,
      private _correctionMapper: CorrectionMapper
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ResolvedData<ITestResolved>> {
    const test = getTest(route.params["id"], this._testService);
    const corrections = searchCorrections(
      { size: 100, page: 0, idTest: route.params["id"] },
      this._correctionService,
      this._correctionMapper
    );

    return forkJoin(test, corrections).pipe(
      map(res => ({
        data: {
          test: this._testMapper.mapTo(res[0]),
          corrections: res[1]
        }
      })),
      catchError(err => {
        return of({
          data: null,
          message: 'Error on test detail resolver, data couldn\'t be fetched',
          error: err
        })
      })
    );
  }
}
