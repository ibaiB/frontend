import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {getTest} from '../../application/getTests';
import {IEditTestResolved} from '../../domain/IEditTestResolved';
import {TestService} from '../services/test.service';

@Injectable({
  providedIn: 'root'
})
export class EditTestResolverService implements Resolve<ResolvedData<IEditTestResolved>> {

  constructor(
      private _testService: TestService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<IEditTestResolved>> {
    return getTest(route.params['id'], this._testService).pipe(
      map(response => ({
        data: {
          test: response
        }
      })),
      catchError(error => {
        return of({
          data: null,
          message: 'Error on edit resolver',
          error
        });
      })
    );
  }
}
