import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {searchTests} from '../../application/searchTests';
import {ITestTable} from '../../domain/ITestTable';
import {TestService} from '../services/test.service';

@Injectable({
    providedIn: 'root'
})
export class TestManagementResolverService {

    constructor(private _testService: TestService,
                private _cookieService: CookieFacadeService
    ) {
    }

    resolve(): Observable<ResolvedData<ITestTable>> {
        const filtersCache = this._cookieService.get('testFilterCache');       
        const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('test-table-config'));
        const testParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
        const orderParams = {...tableFiltersConfig?.order} ?? {};

        return searchTests({...JSON.parse(filtersCache),...testParams,...orderParams}, this._testService).pipe(map(response => ({
            data: {
                tests: response,
                filtersValue:filtersCache,
                tableFiltersConfig: tableFiltersConfig
            }
        })),
            catchError(error => {
                return of({
                    data: null,
                    message: 'Error on test managment resolver, data couldn\'t be fetched',
                    error
                });
            }));
    }


}
