import {IList} from 'src/app/shared/generics/IList';
import {IWebCorrection} from '../../correction/domain/IWebCorrection';
import {IQuestion} from '../../question/domain/IQuestion';

export interface ITest {
    id?: string;
    name: string;
    description: string;
    questions?: IQuestion[];
}

export interface ITestResolved{
    test: ITest,
    //TODO
    corrections: IList<IWebCorrection>
}
