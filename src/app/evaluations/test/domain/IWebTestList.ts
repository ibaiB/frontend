import {IWebTest} from './IWebTest';

export interface IWebTestList {
    content: IWebTest[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
