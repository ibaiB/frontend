import {IQuestion} from '../../question/domain/IQuestion';

export interface IWebTest{
    id?: string,
    description: string,
    name: string,
    questions?: IQuestion[]
}

