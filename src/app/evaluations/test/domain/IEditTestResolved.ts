import {ITest} from './ITest';

export interface IEditTestResolved {
    test: ITest;
}
