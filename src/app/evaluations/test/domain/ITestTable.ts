import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {IWebTestList} from './IWebTestList';

export interface ITestTable {
    tests: IWebTestList;
    filtersValue:any;
    tableFiltersConfig: ITableFiltersConfig;
}
    
