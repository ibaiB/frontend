import {TestAbstractService} from '../infrastructure/TestAbstractService';

export function searchTests(query: any, service: TestAbstractService) {
    return service.findAll(query);
}
