import {map} from 'rxjs/operators';
import {ITest} from '../domain/ITest';
import {TestAbstractService} from '../infrastructure/TestAbstractService';
import {TestMapper} from '../infrastructure/TestMapper';

export function createTest(test: ITest, service: TestAbstractService, mapper: TestMapper){
    return service.create(mapper.mapFrom(test)).pipe(map(mapper.mapTo));
}
