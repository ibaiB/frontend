import {map} from 'rxjs/operators';
import {ITest} from '../domain/ITest';
import {TestAbstractService} from '../infrastructure/TestAbstractService';
import {TestMapper} from '../infrastructure/TestMapper';

export function updateTest(test: ITest, service: TestAbstractService, mapper: TestMapper) {
    return service.update(mapper.mapFrom(test)).pipe(map(mapper.mapTo));
}

