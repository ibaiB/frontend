import {IQuestion} from '../../question/domain/IQuestion';
import {QuestionMapper} from '../../question/infrastructure/QuestionMapper';
import {TestAbstractService} from '../infrastructure/TestAbstractService';

export function addQuestion(idTest: string, question: IQuestion, service: TestAbstractService, mapper: QuestionMapper){
    return service.addQuestion(mapper.mapFrom(question), idTest);
}
