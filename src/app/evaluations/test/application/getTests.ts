import {TestAbstractService} from '../infrastructure/TestAbstractService';

export function getTest(id: string, service: TestAbstractService) {
    return service.finbById(id);
}
