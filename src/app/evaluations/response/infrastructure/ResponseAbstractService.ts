import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, mergeMap, tap, toArray} from 'rxjs/operators';
import {ResponseMapper} from './ResponseMapper';
import {IResponse} from '../domain/IResponse';
import {IResponseList} from '../domain/IResponseList';

@Injectable({
  providedIn: 'root'
})
export class ResponseAbstractService {

  private BASE_URL: string = environment.crm;

  constructor(private http: HttpClient, private mapper: ResponseMapper) {
  }

  getList(params: {} = {}): Observable<IResponse[]> {
    return this.http
        .get<IResponseList>(`${this.BASE_URL}responses/search?page=0&size=2147483647`)
        .pipe(tap(responses => console.log('service responses', responses)))
        .pipe(mergeMap(responses => responses.content))
        .pipe(map(this.mapper.mapTo))
        .pipe(toArray());
  }

  update(response: any): Observable<IResponse> {
    return this.http
        .put<any>(`${this.BASE_URL}responses/${response.id}`, response)
        .pipe(map(this.mapper.mapTo));
  }

  create(response: IResponse): Observable<IResponse> {
    return this.http
        .post<any>(`${this.BASE_URL}responses/${response.id}`, response)
        .pipe(map(this.mapper.mapTo));
  }

  delete(id: string): Observable<any> {
    return this.http
        .delete<any>(`${this.BASE_URL}responses/${id}`);
  }

  searchAll(pageIndex: number, pageSize: number, id?: string, tipo?: string){
    return this.http.get<IResponse>(`${this.BASE_URL}/responses/searchAll?id=${id}&page=${pageIndex}&size=${pageSize}&tipo=${tipo}`);
  }

}
