import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class AnswerMapper{
    mapTo(params: any) {
        const {
            id,
            message,
            value,
        } = params;

        return {
            id: id,
            message: message,
            value: value,
        };
    }

    mapFrom(params: any) {
        const {
            id,
            message,
            value,
        } = params;

        return {
            id: id,
            message: message,
            value: value,
        };
    }
}
