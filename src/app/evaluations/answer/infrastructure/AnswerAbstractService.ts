import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AnswerMapper} from './AnswerMapper';
import {IAnswer} from '../domain/IAnswer';
import {IWebAnswerList} from '../domain/IWebAnswerList';
import {environment} from '../../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AnswerAbstractService {

  private BASE_URL: string = environment.crm;

  constructor(private http: HttpClient, private mapper: AnswerMapper) {
  }

  getList(params: {} = {}): Observable<IWebAnswerList> {
    return null;
    // return this.http
    //     .get<IWebAnswerList>(`${this.BASE_URL}answers/search?page=0&size=2147483647`)
    //     .pipe(tap(answers => console.log('service answers', answers)))
    //     .pipe(mergeMap(answers => answers.content))
    //     .pipe(map(this.mapper.mapTo))
    //     .pipe(toArray());
  }

  update(answer: any): Observable<IAnswer> {
    return this.http
        .put<any>(`${this.BASE_URL}answers/${answer.id}`, answer)
        .pipe(map(this.mapper.mapTo));
  }

  create(answer: IAnswer): Observable<IAnswer> {
    return this.http
        .post<any>(`${this.BASE_URL}answers/${answer.id}`, answer)
        .pipe(map(this.mapper.mapTo));
  }

  delete(id: string): Observable<any> {
    return this.http
        .delete<any>(`${this.BASE_URL}answers/${id}`);
  }

  searchAll(pageIndex: number, pageSize: number, id?: string, tipo?: string){
    return this.http.get<IAnswer>(`${this.BASE_URL}/answers/searchAll?id=${id}&page=${pageIndex}&size=${pageSize}&tipo=${tipo}`);
  }

}
