export interface IAnswer {
    id?: string;
    message: string;
    value: string;
}
