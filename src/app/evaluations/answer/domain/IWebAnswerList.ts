import {IWebAnswer} from './IWebAnswer';

export interface IWebAnswerList {
    content: IWebAnswer[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
