export interface IWebAnswer {
    id?: string;
    message: string;
    value: string;
}
