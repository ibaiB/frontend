import {IChat} from './IChat';

export interface IMessage {
    chatterEmail: string,
    date: string,
    id: string,
    idChat: string,
    messageLevelEnum: string,
    payload: string,
}

export interface IMessageFull extends IMessage {
    chat: IChat
}
