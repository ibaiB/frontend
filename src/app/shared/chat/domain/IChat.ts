export interface IChat {
    id?: string,
    idChat?: string,
    canDisconnect: boolean,
    canSuscribe: boolean,
    canWrite: boolean,
    chatScopeLevelEnum: string
    description: string
    name: string
    origin: string
    subscribers: string
}
