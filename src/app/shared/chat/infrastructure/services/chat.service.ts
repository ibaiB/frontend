import {Injectable} from '@angular/core';
import {ChatAbstractService} from '../abstract-services/chat-abstract-service';
import {IChat} from '../../domain/IChat';
import {Observable} from 'rxjs';
import {IMessage} from '../../domain/IMessage';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatService extends ChatAbstractService {

  BASE_URL = 'http://ec2-15-237-10-209.eu-west-3.compute.amazonaws.com/chat/api/v0/';
  ENDPOINT = 'chats';

  constructor(private _http: HttpClient) {
    super();
  }

  create(chat: IChat): Observable<IChat> {
    return this._http
        .post<any>(`${this.BASE_URL}${this.ENDPOINT}`, chat);
  }

  disconnect(query: any): Observable<any> {
    return this._http
        .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/disconnect`, {params: new HttpParams({fromObject: query})});
  }

  getMyChats(query?: any): Observable<IChat[]> {
    return this._http
        .get<IChat[]>(`${this.BASE_URL}${this.ENDPOINT}/me`, {params: new HttpParams({fromObject: query})});
  }

  getChatById(id: string): Observable<IChat> {
    return this._http
        .get<IChat>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  sendMessage(message: IMessage): Observable<any> {
    return this._http
        .post<any>(`${this.BASE_URL}${this.ENDPOINT}/messages`, message);
  }

  subscribe(query: any): Observable<any> {
    return this._http
        .post<any>(`${this.BASE_URL}${this.ENDPOINT}/subscribe`, {}, {params: new HttpParams({fromObject: query})});
  }
}
