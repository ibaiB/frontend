import {Injectable} from '@angular/core';
import {MessageAbstractService} from '../abstract-services/message-abstract-service';
import {Observable} from 'rxjs';
import {IList} from '../../../generics/IList';
import {IMessage, IMessageFull} from '../../domain/IMessage';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MessageService extends MessageAbstractService {

    BASE_URL = environment.baseUrl;
    ENDPOINT = 'messages';

    constructor(private _http: HttpClient) {
        super();
    }

    search(query: any): Observable<IList<IMessage | IMessageFull>> {
        //TODO: arreglar ruta
        return this._http.get<IList<IMessage | IMessageFull>>(`http://ec2-15-237-10-209.eu-west-3.compute.amazonaws.com/chat/api/v0/${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
    }
}
