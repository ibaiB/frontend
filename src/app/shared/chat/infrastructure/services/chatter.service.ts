import {Injectable} from '@angular/core';
import {ChatterAbstractService} from '../abstract-services/chatter-abstract-service';
import {Observable} from 'rxjs';
import {IChatter} from '../../domain/IChatter';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatterService extends ChatterAbstractService {

  BASE_URL = environment.baseUrl;
  ENDPOINT = 'chatters';

  constructor(private _http: HttpClient) {
    super();
  }

  me(query?: any): Observable<IChatter> {
    return this._http.get<IChatter>(`${this.BASE_URL}${this.ENDPOINT}/me`, {params: new HttpParams({fromObject: query})});
  }
}
