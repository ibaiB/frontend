import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { CookieFacadeService } from 'src/app/auth/session/infrastructure/services/cookie.service';
import { searchPublicEmployees } from 'src/app/internal/employee/application/searchPublicEmployee';
import { EmployeeService } from 'src/app/internal/employee/infrastructure/services/employee.service';
import { getChatById } from '../../../application/getChatById';
import { getMyChatters } from '../../../application/getMyChatters';
import { searchMessages } from '../../../application/searchMyMessages';
import { IChat } from '../../../domain/IChat';
import { IChatter } from '../../../domain/IChatter';
import { IMessage } from '../../../domain/IMessage';
import { ChatService } from '../../services/chat.service';
import { ChatterService } from '../../services/chatter.service';
import { MessageService } from '../../services/message.service';

/** Javascript websocket libraries */
declare var SockJS: any;
declare var Stomp: any;

interface Mention {
  fullName: string;
  start: number;
  end: number;
  valid: boolean;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnChanges {

  @Input() id: string;
  @Input() visible : boolean;
  @Output() closed = new EventEmitter<any>();

  @ViewChild('scrollTarget') scrollTarget: ElementRef;
  @ViewChild('msgContainer') msgContainer: ElementRef;
  @ViewChild('msgTextArea') msgTextArea: ElementRef;

  stompClient;
  messageInput: FormGroup;
  showChatUsers: boolean = false;
  showScrollButton: boolean = false;
  canScrollToBottom: boolean = false;
  lastScrollHeight: number = 0;
  lastScrollTop: number = 1000;
  currentOffset: number;
  
  chat: IChat;
  chatter: IChatter;
  messages: IMessage[] = [];
  users: Map<string, any> = new Map<string, any>();
  chatPageSize = 10;
  chatPagesLoaded = 0;
  isLastPage: boolean;
  isLoadingMessages: boolean = false;
  options: string[];
  connected: boolean = false;

  $inputChange: Observable<string>;

  constructor(private _fb: FormBuilder,
    private _messageService: MessageService,
    private _employeeService: EmployeeService,
    private _chatService: ChatService,
    private _chatterService: ChatterService,
    private _cookieFacade: CookieFacadeService) {
  }

  ngOnInit() {
    this.messageInput = this._fb.group({
      message: ['']
    });
  }

  ngOnChanges() {
    if (!this.id || this.connected) {
      return;
    }
    
    this.connected = true;

    getChatById(this._chatService, this.id).subscribe(chat => {
      if (chat) {
        this.chat = chat;
        this._loadNextPage();
      }
      console.log('chat', chat);
    });

    getMyChatters(this._chatterService).subscribe(chatter => {
      this.chatter = chatter;
      this._connect();
    });

    // TODO: Find a better way to scroll on init
    setTimeout(() => this.scrollToBottom(), 1000);
  }

  private _connect() {
    const socket = new SockJS('http://ec2-15-237-10-209.eu-west-3.compute.amazonaws.com/ws/chatter');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({Authorization: this._cookieFacade.get('staffit-auth')}, frame => {
      this.stompClient.subscribe('/topic/chatter/' + this.chatter.idChatter, messageOutput => {
          this._messageReceived(JSON.parse(messageOutput.body));
      });
    });
  }

  private _disconnect() {
    if (this.stompClient) {
      this.stompClient.disconnect();
    }
  }

  private _loadNextPage() {
    this.isLastPage = this.messages.length % this.chatPageSize !== 0;
    if (this.isLastPage ) return;

    // Search chat messages
    this.isLoadingMessages = true;
    searchMessages({'idChat': this.chat.id, 'size': this.chatPageSize, 'page': this.chatPagesLoaded++}, this._messageService).subscribe((messages => {
      this.messages = [...messages.content.slice().reverse(), ...this.messages];
      setTimeout(() => {
        const elem = this.msgContainer?.nativeElement;
        if (elem) elem.scrollTop = elem.scrollHeight - this.lastScrollHeight;
      });

      this.isLoadingMessages = false;

      // Search chat users
      this.messages.forEach(msg => {
        if (this.users.has(msg.chatterEmail)) {
          return;
        }

        searchPublicEmployees({'businessEmail': msg.chatterEmail}, this._employeeService).subscribe(employees => {
          if (employees.content.length === 0) {
            this.users.set(msg.chatterEmail, {
              'name': msg.chatterEmail.split('@')[0],
              'surname': '',
              'image': null
            });
            return;
          }

          this.users.set(msg.chatterEmail, {
            'name': employees.content[0].name,
            'surname': employees.content[0].surname,
            'image': employees.content[0].imageLink
          });
        });
      });
    }));
  }

  private _messageReceived(msg: IMessage) {
    if (msg.idChat !== this.chat.idChat) return;
    this.messages.push(msg);
    setTimeout(() => this.scrollToBottom());
  }
  
  sendMessage() {
    const text = this.messageInput.get('message').value.trim();
    this.messageInput.get('message').setValue('');
    if (!text) return;

    this.stompClient.send('/app/chatter', {}, JSON.stringify({
      'payload': text,
      'idChat': this.chat.idChat,
      'chatterEmail': this.chatter.email,
      'messageLevelEnum': 'DEFAULT'
    }));
  }

  // SCROLL

  scrollToBottom() {
    if (!this.scrollTarget) return;
    this.scrollTarget.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
  }

  onScroll(event: any) {
    const elem = this.msgContainer.nativeElement;
    const threshold = 70;

    // Save last scroll position
    this.lastScrollHeight = elem.scrollHeight;
    this.lastScrollTop = elem.scrollTop;

    // Check if scroll to bottom is possible
    this.canScrollToBottom = elem.scrollHeight - (elem.scrollTop + elem.clientHeight) > threshold;
    
    // Handle pagination
    if (!this.isLoadingMessages && elem.scrollTop === 0) {
      this._loadNextPage();
    }
  }

  // MENTIONS

  /*onInputChanged(event: any) {
    console.log(event);
    console.log(this.msgTextArea.nativeElement);
    console.log(this.msgTextArea.nativeElement.selectionStart);

    if (event.inputType === 'deleteContentBackward') {
      if (this.msgTextArea.nativeElement) {
        this.$inputChange;
      }
    }

    if (event.data === '@') {
      const cursorPosition = this.msgTextArea.nativeElement.selectionStart;
      this.messageInput.valueChanges.pipe(
        debounceTime(300),
      ).subscribe(value => this._searchEmployees(value.message.substring(cursorPosition)));
    }
  }

  private _searchEmployees(nameSurname: string) {
    searchPublicEmployees({nameSurname: nameSurname}, this._employeeService).subscribe(employees => {
      this.options = employees.content.map(e => e.name + ' ' + e.surname);
    });
  }

  selectedOption(fullName: string) {
    // TODO: Introducir fullName después del @ correspondiente
    console.log(fullName);
  }*/

  handleKeyEnter(event: any) {
    event.preventDefault();
    this.sendMessage();
  }

  close() {
    this.closed.emit();
  }

}
