import {Observable} from 'rxjs';
import {IChatter} from '../../domain/IChatter';

export abstract class ChatterAbstractService {
    abstract me(query?: any): Observable<IChatter>;
}
