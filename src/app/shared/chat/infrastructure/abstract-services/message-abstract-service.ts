import {Observable} from 'rxjs';
import {IList} from '../../../generics/IList';
import {IMessage, IMessageFull} from '../../domain/IMessage';

export abstract class MessageAbstractService {
    abstract search(query: any): Observable<IList<IMessage | IMessageFull>>
}
