import {IChat} from '../../domain/IChat';
import {Observable} from 'rxjs';
import {IMessage} from '../../domain/IMessage';

export abstract class ChatAbstractService {
    abstract create(chat: IChat): Observable<IChat>;

    abstract disconnect(query: any): Observable<any>;

    abstract getMyChats(query?: any): Observable<IChat[]>;

    abstract getChatById(id: string): Observable<IChat>;

    abstract sendMessage(message: IMessage): Observable<any>;

    abstract subscribe(query: any): Observable<any>;
}
