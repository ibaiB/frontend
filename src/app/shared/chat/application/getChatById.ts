import {ChatAbstractService} from '../infrastructure/abstract-services/chat-abstract-service';

export function getChatById(service: ChatAbstractService, id: string) {
    return service.getChatById(id);
}
