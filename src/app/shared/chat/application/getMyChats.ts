import {ChatAbstractService} from '../infrastructure/abstract-services/chat-abstract-service';

export function getMyChats(service: ChatAbstractService, query?: any) {
    return service.getMyChats(query);
}
