import {ChatterAbstractService} from '../infrastructure/abstract-services/chatter-abstract-service';

export function getMyChatters(service: ChatterAbstractService, query?: any) {
    return service.me(query);
}
