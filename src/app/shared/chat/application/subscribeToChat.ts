import {ChatAbstractService} from '../infrastructure/abstract-services/chat-abstract-service';

export function subscribeToChat(idChat: string, service: ChatAbstractService, query?: any) {
    return service.subscribe(idChat);
}
