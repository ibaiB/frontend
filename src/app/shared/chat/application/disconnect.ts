import {ChatAbstractService} from '../infrastructure/abstract-services/chat-abstract-service';

export function disconnect(idChat: string, service: ChatAbstractService, query?: any) {
    return service.disconnect(idChat);
}
