import {MessageAbstractService} from '../infrastructure/abstract-services/message-abstract-service';

export function searchMessages(query: any, service: MessageAbstractService) {
    return service.search(query);
}
