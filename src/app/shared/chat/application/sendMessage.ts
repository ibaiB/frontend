import {ChatAbstractService} from '../infrastructure/abstract-services/chat-abstract-service';

export function sendMessage(message: any, service: ChatAbstractService) {
    return service.sendMessage(message);
}
