import {ChatAbstractService} from '../infrastructure/abstract-services/chat-abstract-service';

export function create(chat: any, service: ChatAbstractService) {
    return service.create(chat);
}
