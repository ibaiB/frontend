import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DateAdapter, MAT_DATE_LOCALE} from '@angular/material/core';
import {MyDateAdapter} from './middleware/MyDateAdapter';

// Components
// Modules
import {AngularMaterialModule} from './material/angular-material.module';
import {NotFoundComponent} from './presentation/not-found/not-found.component';
import {ErrorResponseComponent} from './presentation/error-response/error-response.component';

// Mappers
import {ExamMapper} from '../evaluations/exam/infrastructure/ExamMapper';
import {EmployeeMapper} from '../internal/employee/infrastructure/EmployeeMapper';
import {CategoryMapper} from '../internal/category/infrastructure/CategoryMapper';
import {CarMapper} from '../internal/car/infrastructure/CarMapper';
import {CompanyMapper} from '../internal/company/infrastructure/CompanyMapper';
import {EmployeeTerminationMapper} from '../internal/employee-termination/infrastructure/EmployeeTerminationMapper';
import {DictionaryTypeMapper} from './entities/dictionary/infrastructure/dictionary-type-mapper';
import {DictionaryValueMapper} from './entities/dictionary/infrastructure/dictionary-value-mapper';
import {AutocompleteComponent} from './custom-mat-elements/autocomplete/autocomplete.component';
import {AutocompleteRequiredComponent} from './custom-mat-elements/autocomplete-required/autocomplete-required.component';
import {DynamicFiltersComponent} from './custom-mat-elements/dynamic-filters/dynamic-filters.component';
import {DynamicTableComponent} from './custom-mat-elements/dynamic-table/dynamic-table.component';
import {DynamicFiltersConfigPanelComponent} from './custom-mat-elements/dynamic-filters/dynamic-filters-config-panel/dynamic-filters-config-panel.component';
import {DynamicTableConfigPanelComponent} from './custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component';
import {ForbiddenComponent} from './presentation/forbidden/forbidden.component';
import {DragndropComponent} from './file/infrastructure/ng-components/dragndrop/dragndrop.component';
import {ProgressComponent} from './file/infrastructure/ng-components/dragndrop/progress/progress.component';
import {DndDirective} from './file/infrastructure/ng-components/dragndrop/dnd.directive';
import {AutocompleteMarkIIComponent} from './custom-mat-elements/autocomplete-mark-ii/autocomplete-mark-ii.component';
import {DynamicTableMarkIiComponent} from './custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {AutoselectComponent} from './custom-mat-elements/autoselect/autoselect.component';
import {PickerMarkIIComponent} from './custom-mat-elements/picker-mark-ii/picker-mark-ii.component';
import {Template1Component} from './custom-mat-elements/template1/template1.component';
import {EmployeeChickTableComponent} from '../internal/employee/infrastructure/ng-components/employee-chick-table/employee-chick-table.component';
import {SafeUrl} from './pipes/SafeUrl';
import {DragndropDialogComponent} from './file/infrastructure/ng-components/dragndrop-dialog/dragndrop-dialog.component';
import {PhotoUploaderComponent} from './file/infrastructure/ng-components/photo-uploader/photo-uploader.component';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {CkeditorComponent} from './custom-mat-elements/ckeditor/ckeditor.component';
import {EmployeeCardWhoIsWhoComponent} from '../internal/employee/infrastructure/ng-components/employee-card-who-is-who/employee-card-who-is-who.component';
import {Dragndrop2Component} from './file/infrastructure/ng-components/dragndrop2/dragndrop2.component';
import {EmployeeCardComponent} from '../internal/employee/infrastructure/ng-components/employee-card/employee-card.component';
import {ComingSoonComponent} from './presentation/coming-soon/coming-soon.component';
import {PipelineComponent} from './custom-mat-elements/pipeline/pipeline.component';
import {ChatComponent} from './chat/infrastructure/ng-components/chat/chat.component';
import {BubbleComponent} from './chat/infrastructure/ng-components/bubble/bubble.component';
import {SubcarMapper} from '../internal/subcar/infrastructure/SubcarMapper';
import {MaterialMapper} from '../internal/material/infrastructure/MaterialMapper';
import {CalendarContainerComponent} from './calendar/infrastructure/ng-componets/calendar-container/calendar-container.component';
import {DailyCalendarComponent} from './calendar/infrastructure/ng-componets/daily-calendar/daily-calendar.component';
import {WeeklyCalendarComponent} from './calendar/infrastructure/ng-componets/weekly-calendar/weekly-calendar.component';
import {MonthlyCalendarComponent} from './calendar/infrastructure/ng-componets/monthly-calendar/monthly-calendar.component';
import {StorageComponent} from './file/infrastructure/ng-components/storage/storage.component';
import {DragAndDropDirective} from './file/infrastructure/ng-components/storage/drag-and-drop.directive';


@NgModule({
    declarations: [
        NotFoundComponent,
        ErrorResponseComponent,
        AutocompleteComponent,
        AutocompleteRequiredComponent,
        DynamicFiltersComponent,
        DynamicTableComponent,
        DynamicFiltersConfigPanelComponent,
        DynamicTableConfigPanelComponent,
        ForbiddenComponent,
        DragndropComponent,
        ProgressComponent,
        DndDirective,
        AutocompleteMarkIIComponent,
        DynamicTableMarkIiComponent,
        AutoselectComponent,
        PickerMarkIIComponent,
        Template1Component,
        EmployeeChickTableComponent,
        DragndropDialogComponent,
        PhotoUploaderComponent,
        SafeUrl,
        CkeditorComponent,
        EmployeeCardWhoIsWhoComponent,
        Dragndrop2Component,
        EmployeeCardComponent,
        EmployeeCardWhoIsWhoComponent,
        ComingSoonComponent,
        PipelineComponent,
        ChatComponent,
        BubbleComponent,
        CalendarContainerComponent,
        DailyCalendarComponent,
        WeeklyCalendarComponent,
        MonthlyCalendarComponent,
        StorageComponent,
        DragAndDropDirective,

    ],
    imports: [
        CommonModule,
        AngularMaterialModule,
        CKEditorModule
    ],
    providers: [
        EmployeeMapper,
        CategoryMapper,
        CarMapper,
        CompanyMapper,
        SubcarMapper,
        MaterialMapper,
        EmployeeTerminationMapper,
        DictionaryTypeMapper,
        DictionaryValueMapper,
        ExamMapper,
        {provide: MAT_DATE_LOCALE, useValue: 'es-CL'},
        {provide: DateAdapter, useClass: MyDateAdapter},
    ],
    exports: [
        AutocompleteComponent,
        AutocompleteRequiredComponent,
        DynamicTableComponent,
        DynamicFiltersComponent,
        DynamicFiltersConfigPanelComponent,
        DynamicTableConfigPanelComponent,
        DragndropComponent,
        Dragndrop2Component,
        DynamicTableMarkIiComponent,
        AutocompleteMarkIIComponent,
        AutoselectComponent,
        PickerMarkIIComponent,
        Template1Component,
        EmployeeChickTableComponent,
        SafeUrl,
        PhotoUploaderComponent,
        CKEditorModule,
        CkeditorComponent,
        EmployeeCardWhoIsWhoComponent,
        EmployeeCardComponent,
        ChatComponent,
        CalendarContainerComponent,
        DailyCalendarComponent,
        WeeklyCalendarComponent,
        MonthlyCalendarComponent,
        StorageComponent,
    ]
})
export class SharedModule {}
