export interface ICalendarConfig {
    daily: ICalendarViewConfig,
    weekly: ICalendarViewConfig,
    monthly: ICalendarViewConfig
}

export interface ICalendarViewConfig {
    shown: boolean,
    dialog: any
}
