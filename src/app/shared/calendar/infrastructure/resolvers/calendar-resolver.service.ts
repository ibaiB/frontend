import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {RoomService} from '../../../../internal/room/infrastructure/services/room.service';
import {get} from '../../../../internal/room/application/get';
import {getActivities} from '../../../../crm/entities/activity/application/getActivities';
import {ActivityService} from '../../../../crm/services/activity.service';

@Injectable({
    providedIn: 'any'
})

export class CalendarResolver implements Resolve<ResolvedData<any>> {

    constructor(
        private _roomService: RoomService,
        private _activityService: ActivityService) {
    }


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        let $father = route.data.type === 'room' ? get(route.params['id'], this._roomService) : getActivities({
            page: 0,
            size: 100
        }, this._activityService);
        let config = route.data.config;

        return forkJoin($father)
            .pipe(map(response => ({
                    data: {
                        father: response[0],
                        config: config,
                        type: route.data.type
                    }
                })),
                catchError(error => {
                    return of({
                        data: null,
                        message: 'Error on room management resolver, data couldn\' be fetched',
                        error
                    });
                }));
    }
}
