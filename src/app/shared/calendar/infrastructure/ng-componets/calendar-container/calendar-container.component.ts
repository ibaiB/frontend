import {formatDate} from '@angular/common';
import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {getMyEmployee} from 'src/app/internal/employee/application/getMyEmployee';
import {EmployeeService} from 'src/app/internal/employee/infrastructure/services/employee.service';
import {ICalendarConfig} from '../../../domain/ICalendarConfig';

@Component({
    selector: 'app-calendar-container',
    templateUrl: './calendar-container.component.html',
    styleUrls: ['./calendar-container.component.scss']
})
export class CalendarContainerComponent implements OnInit {
    @Input() config: ICalendarConfig;

    showDate: any;
    days: Date[];
    increment: number = 0;
    father: any;
    view: string;
    me: any;

    constructor(private _route: ActivatedRoute,
                private _employeeService: EmployeeService) {
    }

    ngOnInit(): void {
        this.showDate = formatDate(new Date(), 'MMMM y', 'en-US');
        this.setView();
        this.getDays(this.view, 0);
        getMyEmployee(this._employeeService).subscribe(e => {
            this.me = e;
        });
    }

    setView(){
        if(this.config.monthly.shown) return this.view = 'month';
        if(this.config.weekly.shown) return this.view = 'week';
        return this.view = 'day';
    }

    changeView(view: string) {
        if (view === 'day' && !this.config.daily.shown) {
            this.config.daily.shown = true;
            this.config.monthly.shown = false;
            this.config.weekly.shown = false;
            this.getDays('day', 0);
        }
        if (view === 'month' && !this.config.monthly.shown) {
            this.config.daily.shown = false;
            this.config.monthly.shown = true;
            this.config.weekly.shown = false;
            this.getDays('month', 0);
        }
        if (view === 'week' && !this.config.weekly.shown) {
            this.config.daily.shown = false;
            this.config.monthly.shown = false;
            this.config.weekly.shown = true;
            this.getDays('week', 0);
        }
        this.view = view;
        this.increment = 0;
    }

    getDays(view: string, days: number) {
        const firstDay = this.setFirstDay(view, days);
        const lastDay = this.setLastDay(view, days);
        this.showDate = this.formatDate(view, firstDay, lastDay);

        this.days = [];

        if (view === 'month') {
            firstDay.setDate(this.completeMonth(firstDay, -1, 'first').getDate());
            lastDay.setDate(this.completeMonth(lastDay, 1, 'last').getDate());
        }

        if (firstDay === lastDay) {
            this.days.push(firstDay);
            return;
        }

        while (firstDay <= lastDay) {
            let dateMove = new Date(firstDay);
            this.days.push(dateMove);
            firstDay.setDate(firstDay.getDate() + 1);
        }
    }

    setFirstDay(view: string, days: number): Date{
        const currentDay = new Date();
        if(view === 'month') return new Date(currentDay.getFullYear(), currentDay.getMonth() + days, 1);
        if(view === 'week') return  new Date(currentDay.setDate((currentDay.getDate() + days) - (currentDay.getDay() - 1)));
        return new Date(currentDay.setDate((currentDay.getDate() + days)));
    }

    setLastDay(view: string, days: number): Date{
        const currentDay = new Date();
        if(view === 'month') return new Date(currentDay.getFullYear(), currentDay.getMonth() + 1 + days, 0);
        if(view === 'week') return  new Date(currentDay.setDate((currentDay.getDate()) - currentDay.getDay() + 7));
        return currentDay;
    }

    formatDate(view: string, firstDay: Date, lastDay: Date): string{
        if(view === 'month') return formatDate(firstDay, 'MMMM y', 'en-US');
        if(view === 'week') return `${formatDate(firstDay, 'MMM d', 'en-US')} - ${formatDate(lastDay, 'MMM d, y', 'en-US')}`;
        return formatDate(firstDay, 'MMMM d, y', 'en-US');
    }

    changeDate(direction: string) {
        let view = this.config.daily.shown ? 'day' : (this.config.monthly.shown ? 'month' : 'week');
        let number = this.config.daily.shown ? 1 : (this.config.monthly.shown ? 1 : 7);

        this.increment = direction === '+' ? number + this.increment : this.increment - number;
        this.getDays(view, this.increment);
    }

    completeMonth(date: Date, increment: number, type: String): Date {
        while ((date.getDay() !== 1 || type !== 'first') && (date.getDay() !== 0 || type !== 'last')) {
            date = new Date(date.setDate((date.getDate() + increment)));
        }
        return date;
    }
}
