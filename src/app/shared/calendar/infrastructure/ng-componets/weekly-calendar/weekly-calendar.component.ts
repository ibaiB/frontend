import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {DatePipe, formatDate} from '@angular/common';
import Swal from 'sweetalert2';
import {MatDialog} from '@angular/material/dialog';
import {ICalendarViewConfig} from '../../../domain/ICalendarConfig';

@Component({
    selector: 'app-weekly-calendar',
    templateUrl: './weekly-calendar.component.html',
    styleUrls: ['./weekly-calendar.component.scss']
})
export class WeeklyCalendarComponent implements OnInit, OnChanges {

    periods: number[];
    periodsDraw: any;
    currentDay: any;
    daysFormat: any;
    employees: any;
    width: number;

    @Input() days: any;
    @Input() father: any;
    @Input() config: ICalendarViewConfig;

    constructor(public dialog: MatDialog) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.daysFormat = this.days.map(day => formatDate(day, 'E, d', 'en-US'));
    }

    ngOnInit(): void {
        this.periodsDraw = this.father?.periods.filter(p => p.employees.length !== 0);
        this.periods = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
        this.currentDay = formatDate(new Date(), 'E, d', 'en-US',);
    }

    compareDays(year: number, month: number, day: number, hour: number, minute: number) {
        let today = new Date();
        let newDate = new Date(year, month, day, hour, minute);

        return today > newDate;
    }

    getPeriod(day: Date, hour: number, minute: string) {
        day.setHours(hour + 2);
        day.setMinutes(parseInt(minute));
        day.setSeconds(0);
        day.setMilliseconds(0);

        let currentPeriod = this.father.periods.find(p => new Date(p.start).toISOString() === day.toISOString());

        this.selectPeriod({periods: this.getIncrement(currentPeriod)});
    }

    drawPeriod(day: Date, hour: number, minute: string) {
        day.setHours(hour + 2);
        day.setMinutes(parseInt(minute));
        day.setSeconds(0);
        day.setMilliseconds(0);
        this.width = 100;

        let currentPeriod = this.periodsDraw.find(p => new Date(p.start).toISOString() === day.toISOString());

        if (!currentPeriod) {
            return false;
        }
        let previusPeriod = this.periodsDraw[this.periodsDraw.indexOf(currentPeriod) - 1];
        if (previusPeriod?.end === currentPeriod.start && this.compareEmployees(currentPeriod.employees, previusPeriod?.employees)) {
            return false;
        }
        this.width = this.width * this.getIncrement(currentPeriod).length;
        this.employees = currentPeriod.employees;
        return true;
    }

    getIncrement(period: any): any {
        let aux = period;
        let periods = [aux];

        if (!aux) {
            return 0;
        }
        this.periodsDraw.forEach(p => {
            if (p.start === aux.end && this.compareEmployees(p.employees, aux?.employees)) {
                aux = p;
                periods.push(aux);
            }
        });
        return periods;
    }

    compareEmployees(employees: any[], otherEmployees: any[]): boolean {
        employees.sort();
        otherEmployees?.sort();

        return (JSON.stringify(employees) == JSON.stringify(otherEmployees));
    }

    selectPeriod(event: any) {
        if (!event.periods) {
            Swal.fire({
                icon: 'warning',
                title: 'Oops...',
                text: 'Period not available',
                confirmButtonColor: '#db5e5e'
            });
            return;
        }
        const dialogRef = this.dialog.open(this.config.dialog, {
            data: {
                father: this.father,
                periods: event.periods,
                allPeriods: this.father.periods,
                //removable: this._route.snapshot.data['removable'],
                removable: true,
                me: null
                //me: this.me
            }
        });
    }
}
