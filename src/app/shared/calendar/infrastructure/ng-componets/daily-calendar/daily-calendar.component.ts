import {formatDate} from '@angular/common';
import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
    selector: 'app-daily-calendar',
    templateUrl: './daily-calendar.component.html',
    styleUrls: ['./daily-calendar.component.scss']
})
export class DailyCalendarComponent implements OnInit, OnChanges {

    periods: number[];
    periodsDraw: any;
    currentDay: any;
    daysFormat: any;
    employees: any;
    width: number;

    @Input() days: any;
    @Input() father: any;
    @Output() period = new EventEmitter<any>();

    constructor() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.daysFormat = this.days.map(day => formatDate(day, 'EEEE, d', 'en-US'));
    }

    ngOnInit(): void {
        this.periodsDraw = this.father?.periods.filter(p => p.employees.length !== 0);
        this.periods = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];
        this.currentDay = formatDate(new Date(), 'E, d', 'en-US',);
    }

    compareHours(year: number, month: number, day: number, hour: number, minute: number) {
        let today = new Date();
        let newDate = new Date(year, month, day, hour, minute);

        return today > newDate;
    }

    getPeriod(day: Date, hour: number, minute: string) {
        day.setHours(hour + 2);
        day.setMinutes(parseInt(minute));
        day.setSeconds(0);
        day.setMilliseconds(0);

        let currentPeriod = this.father.periods.find(p => new Date(p.start).toISOString() === day.toISOString());

        this.period.emit({periods: this.getIncrement(currentPeriod)});
    }

    drawPeriod(day: Date, hour: number, minute: string) {
        day.setHours(hour + 2);
        day.setMinutes(parseInt(minute));
        day.setSeconds(0);
        day.setMilliseconds(0);
        this.width = 100;

        let currentPeriod = this.periodsDraw.find(p => new Date(p.start).toISOString() === day.toISOString());

        if (!currentPeriod) {
            return false;
        }
        let previusPeriod = this.periodsDraw[this.periodsDraw.indexOf(currentPeriod) - 1];
        if (previusPeriod?.end === currentPeriod.start && this.compareEmployees(currentPeriod.employees, previusPeriod?.employees)) {
            return false;
        }
        this.width = this.width * this.getIncrement(currentPeriod).length;
        this.employees = currentPeriod.employees;
        return true;
    }

    getIncrement(period: any): any {
        let aux = period;
        let periods = [aux];

        if (!aux) {
            return 0;
        }
        this.periodsDraw.forEach(p => {
            if (p.start === aux.end && this.compareEmployees(p.employees, aux?.employees)) {
                aux = p;
                periods.push(aux);
            }
        });
        return periods;
    }

    compareEmployees(employees: any[], otherEmployees: any[]): boolean {
        employees.sort();
        otherEmployees?.sort();

        return (JSON.stringify(employees) === JSON.stringify(otherEmployees));
    }
}
