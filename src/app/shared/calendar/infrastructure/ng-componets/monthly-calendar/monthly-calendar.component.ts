import {formatDate} from '@angular/common';
import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {CreateActivityComponent} from 'src/app/crm/presentation/activities/create-activity/create-activity.component';

@Component({
    selector: 'app-monthly-calendar',
    templateUrl: './monthly-calendar.component.html',
    styleUrls: ['./monthly-calendar.component.scss']
})

export class MonthlyCalendarComponent implements OnInit, OnChanges {

    week: any = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];
    monthWeeks: any[] = new Array(
        this.getWeeksInMonth(
            new Date().getFullYear(),
            new Date().getMonth() + 1
            )
    );
    currentDay: any;
    @Input() days: any;

    constructor(public dialog: MatDialog) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.days = this.days.map(day => formatDate(day, 'E, d', 'en-US'));
    }

    ngOnInit(): void {
        this.currentDay = formatDate(new Date(), 'E, d', 'en-US');
    }

    getWeeksInMonth(year, monthNumber) {
        let firstOfMonth = new Date(year, monthNumber - 1, 1);
        let day = firstOfMonth.getDay() || 6;
        day = day === 1 ? 0 : day;
        if (day) { day-- }
        let diff = 7 - day;
        let lastOfMonth = new Date(year, monthNumber, 0);
        let lastDate = lastOfMonth.getDate();
        if (lastOfMonth.getDay() === 1) {
            diff--;
        }
        let result = Math.ceil((lastDate - diff) / 7);
        return result + 1;
    };

    clickDay() {
        const dialogRef = this.dialog.open(CreateActivityComponent, {});
    }

}


