import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-coming-soon',
  templateUrl: './coming-soon.component.html',
  styleUrls: ['./coming-soon.component.scss']
})
export class ComingSoonComponent implements OnInit {

  fechaActual: Date;
  fechaTarget: Date;
  
  days: number;
  hours: number;
  minutes: number;
  seconds: number;

  constructor() {
  }

  ngOnInit(): void {
    this.fechaActual = new Date();//date Now
    this.fechaTarget = new Date('2021-5-5');

    this.days = this.getNumRemaining('d', this.diffBetweenDates(this.fechaTarget, this.fechaActual));
    this.hours = this.getNumRemaining('h', this.diffBetweenDates(this.fechaTarget, this.fechaActual));
    this.minutes = this.getNumRemaining('m', this.diffBetweenDates(this.fechaTarget, this.fechaActual));
    this.seconds = this.getNumRemaining('s', this.diffBetweenDates(this.fechaTarget, this.fechaActual));

    const progress = setInterval(() => {
      this.fechaActual = new Date();//date Now
      let diff = this.diffBetweenDates(this.fechaTarget, this.fechaActual);

      this.days = this.getNumRemaining('d', diff);
      this.hours = this.getNumRemaining('h', diff);
      this.minutes = this.getNumRemaining('m', diff);
      this.seconds = this.getNumRemaining('s', diff);

      if(diff <= 0){
        clearInterval(progress);
        console.log('Expired')
      }
    }, 1000);
  }

  diffBetweenDates(dateLejano: Date, dateCercano: Date): number {
    let dtms1 = dateLejano.valueOf();
    let dtms2 = dateCercano.valueOf();
    if ((dtms1 - dtms2) > 0) {
      return (dtms1 - dtms2);
    }
  }

  getNumRemaining(mode: string, diffDates: number): number{
    let numb;
    switch(mode){
      case 'd':{
        numb = Math.floor(diffDates / (1000 * 60 * 60 * 24));
        break;
      }
      case 'h':{
        numb = Math.floor((diffDates % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        break;
      }
      case 'm':{
        numb = Math.floor((diffDates % (1000 * 60 * 60)) / (1000 * 60));
        break;
      }
      case 's':{
        numb = Math.floor((diffDates % (1000 * 60)) / 1000);
        break;
      }
    }
    return numb;
  }

}
