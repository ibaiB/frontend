export const errors = [
    {
        error: '400',
        motive: 'Error',
        explanation: 'Looks like this functionality is broken. We\'ll try to fix it as soon as possible.'
    },
    {
        error: '413',
        motive: 'Error',
        explanation: 'File too big, please choose a smaller one'
    },
    {
        error: '500',
        motive: 'Error',
        explanation: 'Looks like this functionality is broken. We\'ll try to fix it as soon as possible.'
    },
    {
        error: '502',
        motive: 'Maintenance',
        explanation: 'Our server is currently getting ready for you, please wait having a coffee. If after 2 min the problem still persists contact the maintenance team.'
    },
    {
        error: '503',
        motive: 'Maintenance',
        explanation: 'Our server is currently getting ready for you, please wait having a coffee. If after 2 min the problem still persists contact the maintenance team.'
    },
    {
        error: '401',
        motive: 'Rejection',
        explanation: 'Looks like your credentials have expired. Maybe you should renew them.'
    },
    {
        error: '403',
        motive: 'Rejection',
        explanation: 'Looks like you don\'t have enough access level.'
    },
    {
        error: '404',
        motive: 'Resource Not Found',
        explanation: 'Looks like we don\'t have what you are looking for'
    },
    {
        error: '???',
        motive: 'Crash',
        explanation: 'What have you done?'
    },
];
