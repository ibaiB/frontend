import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorFacade} from '../../error/ErrorManagement';
import {successAlert} from '../../error/custom-alerts';
import Swal from 'sweetalert2';
import {interval, Subscription} from 'rxjs';
import {Router} from '@angular/router';

export interface ErrorResponseData {
    error: Error
    code: string,
    motive: string,
    explanation: string
    timeout: number,
    reportable: boolean
}

@Component({
    selector: 'app-error-response',
    templateUrl: './error-response.component.html',
    styleUrls: ['./error-response.component.scss']
})
export class ErrorResponseComponent implements OnInit {

    originalError: Error;
    errorCode: string;
    errorMotive: string;
    errorExplanation: string;
    reportable: boolean;

    timeout: number;
    initialTimeout: number;
    timerSubscription: Subscription;
    
    isFrontError: boolean;

    constructor(
        private _error: ErrorFacade,
        public dialogRef: MatDialogRef<ErrorResponseComponent>,
        private _router: Router,
        @Inject(MAT_DIALOG_DATA) public data: ErrorResponseData
    ) {}

    ngOnInit(): void {
        this.originalError = this.data?.error;
        this.reportable = this.data?.reportable;
        this.timeout = this.data?.timeout / 1000;
        this.initialTimeout = this.timeout;
        this.errorCode = this.data?.code;
        this.errorMotive = this.data?.motive;
        this.errorExplanation = this.data?.explanation;

        this.isFrontError = !(this.originalError instanceof HttpErrorResponse);

        if (this.timeout) this.startTimer();
    }

    startTimer() {
        setTimeout(() => {
            this.dialogRef.close();
            this._router.routeReuseStrategy.shouldReuseRoute = () => false;
            this._router.onSameUrlNavigation = 'reload';
            this._router.navigate([this._router.url]);
        }, this.timeout * 1000);

        this.timerSubscription = interval(1000).subscribe(() => {
            if (this.timeout === 0) {
                this.timerSubscription.unsubscribe();
                return;
            }
            this.timeout--;
        });
    }

    report() {
        const $report = this.isFrontError 
            ? this._error.notifyFrontError(this.originalError.stack)
            : this._error.notifyBackError(this.originalError['error']['message'], this.originalError['error']['id'] ?? '');

        $report.subscribe(() => {
            successAlert('Your problem was successfully reported')
                .then(() => {
                    this.navigateHome();
                });
        }, error => {
            Swal.fire({
                icon: 'error',
                title: `Even this crashed. We're screwed :)`,
            });
        });
    }

    navigateHome() {
        this._router.navigate(['']);
        this.dialogRef.close();
    }

    mathFloor(i: number): number {
        return Math.floor(i);
    }
}
