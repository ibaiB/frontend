import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class DictionaryTypeMapper {
    mapTo(params: any){
        const {descripcion, id, nombre, valores} = params;
        return{
            description: descripcion,
            id: id,
            name: nombre,
            values: valores
        }
    }
}
