import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class DictionaryValueMapper {
    mapTo(params: any){
        const {id, idTipo, nombre, valor} = params;
        return {
            id: id,
            typeId: idTipo,
            name: nombre,
            value: valor
        }
    }
}
