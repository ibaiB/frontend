import {Observable} from 'rxjs';
import {IDictionaryType} from '../domain/IDictionaryType';

export abstract class DictionaryAbstractservice {
    abstract getList(params: any): Observable<IDictionaryType[]>
    abstract get(id): Observable<IDictionaryType>
}
