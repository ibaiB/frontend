import {Injectable} from '@angular/core';
import {DictionaryAbstractservice} from '../dictionary-abstractservice';
import {Observable} from 'rxjs';
import {IDictionaryType} from '../../domain/IDictionaryType';
import {environment} from '../../../../../../environments/environment.prod';
import {HttpClient, HttpParams} from '@angular/common/http';
import {DictionaryTypeMapper} from '../dictionary-type-mapper';
import {DictionaryValueMapper} from '../dictionary-value-mapper';
import {IDictionaryTypeList} from '../../domain/IDictionaryTypeList';
import {map, mergeMap, tap, toArray} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DictionaryService extends DictionaryAbstractservice {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'tipos';

  constructor(private http: HttpClient,
              private typeMapper: DictionaryTypeMapper,
              private valueMapper: DictionaryValueMapper) {
    super();
  }

  get(id): Observable<IDictionaryType> {
    return undefined;
  }

  getList(params: any): Observable<IDictionaryType[]> {
    let httpParams = new HttpParams();
    //@TODO separate in a function
    let paramsKeys = Object.keys(params);
    paramsKeys.forEach(key => {if(params[key] !== undefined) httpParams = httpParams.append(key, params[key].toString())});

    return this.http
        .get<IDictionaryTypeList>(`${this.BASE_URL}${this.ENDPOINT}`, {params: httpParams})
        .pipe(mergeMap(list => list.content))
        .pipe(tap(type => type.valores = type.valores.map(this.valueMapper.mapTo)))
        .pipe(map(this.typeMapper.mapTo))
        .pipe(toArray());
  }
}
