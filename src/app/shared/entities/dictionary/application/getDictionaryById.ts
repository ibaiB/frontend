import {IDictionaryType} from '../domain/IDictionaryType';

export function getDictionaryById(dictionaries: IDictionaryType[], id: string){
    return dictionaries.filter(dictionary => dictionary.id === id)[0];
}
