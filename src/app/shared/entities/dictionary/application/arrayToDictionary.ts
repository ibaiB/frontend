export function arrayToDictionary(array: any[], id: string, value: string): { id: string, value: string }[] {
    const dic: { id: string, value: string }[] = [];
    array.forEach(item => dic.push({id: item[id], value: item[value]}))
    return dic;
}