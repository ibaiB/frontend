import {DictionaryAbstractservice} from '../infrastructure/dictionary-abstractservice';

export function getDictionaries(params: any, service: DictionaryAbstractservice){
    return service.getList(params);
}
