import {IDictionaryValue} from '../domain/IDictionaryValue';

export function parseValues(value: IDictionaryValue[]){
    return value.map(value => value.name);
}
