import {IDictionaryType} from '../domain/IDictionaryType';

export function parseDictionary(dictionary: IDictionaryType){
    return dictionary.values.map(value => value.name);
}
