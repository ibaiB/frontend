import {IDictionaryType} from '../domain/IDictionaryType';

export function getValueById(dictionary: IDictionaryType, id: string){
    return dictionary.values.filter(elem => elem.id === id)[0];
}
