import {IDictionaryValue} from './IDictionaryValue';

export interface IDictionaryType{
    description: string,
    id: string,
    name: string,
    values: IDictionaryValue[]
}
