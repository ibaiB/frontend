export interface IDictionaryValue{
    id: string,
    typeId: string,
    name: string,
    value: string
}
