export interface IWebDictionaryValue{
    id: string,
    idTipo: string,
    nombre: string,
    valor: string
}
