import {IWebDictionaryType} from './IWebDictionaryType';

export interface IDictionaryTypeList{
    content: IWebDictionaryType[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
