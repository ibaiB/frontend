export interface IWebDictionaryType{
    descripcion: string,
    id: string,
    nombre: string,
    valores: any[]
}
