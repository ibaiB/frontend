import {ICenter} from '../domain/ICenter';

export class CenterMapper {
    mapTo(params: any): ICenter {
        const {
            id,
            location,
        } = params;

        return {
            id: id,
            location: location,
        };
    }
}
