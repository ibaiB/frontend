import {Observable} from 'rxjs';
import {IWebCenter} from 'src/app/internal/center/domain/IWebCenter';
import {IWebCenterList} from 'src/app/internal/center/domain/IWebCenterList';

export abstract class CenterAbstractService {
    abstract findById(id: string): Observable<IWebCenter>;

    abstract search(query: any): Observable<IWebCenterList>;
}
