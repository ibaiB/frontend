import {Injectable} from '@angular/core';
import {CenterAbstractService} from '../CenterAbstractService';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {IWebCenter} from 'src/app/internal/center/domain/IWebCenter';
import {IWebCenterList} from 'src/app/internal/center/domain/IWebCenterList';

@Injectable({
  providedIn: 'root'
})

export class CenterService extends CenterAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'centers';

  constructor(private http: HttpClient) {
    super();
  }

  findById(id: string): Observable<IWebCenter> {
    return this.http.get<IWebCenter>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  search(query: any): Observable<IWebCenterList> {
    return this.http.get<IWebCenterList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
  }

}
