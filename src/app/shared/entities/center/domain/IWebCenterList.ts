import {IWebCenter} from "./IWebCenter";

export interface IWebCenterList {
    content: IWebCenter[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}