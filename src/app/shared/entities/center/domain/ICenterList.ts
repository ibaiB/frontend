import { ICenter } from './ICenter';

export interface ICenterList{
    content: ICenter[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}