import {CenterAbstractService} from '../infrastructure/CenterAbstractService';

export function getSingleCenter(id: string, service: CenterAbstractService){
    return service.findById(id);
}
