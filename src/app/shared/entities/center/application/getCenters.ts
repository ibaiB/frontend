import {CenterAbstractService} from "../infrastructure/CenterAbstractService";

export function getCenters(query: any, service: CenterAbstractService) {
    return service.search(query);
}