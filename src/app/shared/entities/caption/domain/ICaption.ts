export interface ICaptionContent{
    content: ICaption[];
}
    
export interface ICaption{
    id?: string;
    value: string;
    description: string;
}
