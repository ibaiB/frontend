export class CaptionMapper{
    mapTo(params: any){
        const {
            id,
            value,
            description
        } = params;

        return {
            id: id,
            value: value,
            description: description
        }
    }
}