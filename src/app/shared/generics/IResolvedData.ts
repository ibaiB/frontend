import { HttpErrorResponse } from "@angular/common/http";

export interface ResolvedData<T>{
    data: T,
    error?: HttpErrorResponse,
    message?: string
}