import {TransferState, makeStateKey} from '@angular/platform-browser';

export class CacheFacade {
    constructor(private ngState: TransferState) {}

    save(key: string, value: any){
        const entryKey = makeStateKey(key)
        this.ngState.set(entryKey, value)
    }

    contains(key: string): boolean{
        const entryKey = makeStateKey(key);
        return this.ngState.hasKey(entryKey);
    }

    get(key: string): any{
        const entryKey = makeStateKey(key);
        return this.ngState.get(entryKey, null);
    }

    delete(key: string){
        const entryKey = makeStateKey(key);
        this.ngState.remove(entryKey);
    }
}
