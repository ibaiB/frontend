export interface IWebFile {
    originalName: string,
    owner: string,
    sharedUsers: string,
    extension: string,
    size: number,
    downloadLink: string,
    storageLocation: string,
    uploadedFileId: string,
    permission: string,
    idCustomFile: string,
    uploadName: string,
    sharedDomains: string,
    fileManagerId: string,
    folderId: string,
    tags: string
}
