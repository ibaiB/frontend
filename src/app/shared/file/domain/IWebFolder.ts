export interface IWebFolder {
    id?: string,
    description: string,
    fileManagerId: string,
    name: string,
    parentFolderId: string,
    tags: string
}
