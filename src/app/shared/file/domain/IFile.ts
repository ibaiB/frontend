export interface IFile {
    originalName: string,
    owner: string,
    sharedUsers: string,
    extension: string,
    size: string,
    downloadLink: string,
    storageLocation: string,
    uploadedFileId: string,
    permission: string,
    id: string,
    uploadName: string,
    sharedDomains: string,
    category: fileCategories,
    path: string,
    created: string,
    lastModified: string,
    tags: string[]
}

export enum fileCategories {
    'DEF',
    'WORD',
    'PPT',
    'EXCEL',
    'IMG',
    'PDF'
}

export interface IShownFile {
    header: string,
    id: string,
    url: string,
    size: string,
    name: string,
    extension: string
}
