export interface FileUploadQuery {
    fileManagerId: string,
    folderId: string,
    storageLocation: string,
    tags: string,
    uploadName: string
}

export const imageQuery: FileUploadQuery = {
    fileManagerId: '',
    folderId: '',
    storageLocation: 'AMAZON',
    tags: '',
    uploadName: ''
};

export const documentQuery: FileUploadQuery = {
    fileManagerId: '',
    folderId: '',
    storageLocation: 'GOOGLE_DRIVE',
    tags: '',
    uploadName: ''
};
