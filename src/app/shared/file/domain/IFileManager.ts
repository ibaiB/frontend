import {IFile} from './IFile';
import {IFolderRead} from './IFolder';

export interface IFileManagerWrite {
    id?: string,
    description: string,
    name: string,
    origin: string,
    owners: string,
    sharedDomains: string,
    sharedUsers: string
}

export interface IFileManagerRead extends IFileManagerWrite {
    folders: IFolderRead[],
    files: IFile[]
}
