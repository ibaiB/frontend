import {IWebFolder} from './IWebFolder';
import {IWebFile} from './IWebFile';

export interface IWebFileManagerWrite {
    id?: string,
    description: string,
    name: string,
    origin: string,
    owners: string,
    sharedDomains: string,
    sharedUsers: string
}

export interface IWebFileManagerRead extends IWebFileManagerWrite {
    folders: IWebFolder[],
    files: IWebFile[]
}
