import {IFile} from './IFile';

export interface IFolderWrite {
    id?: string,
    description: string,
    fileManagerId: string,
    name: string,
    parentFolderId: string,
    tags: string[],
    path: string,
    size: string,
    lastModified: string,
    created: string
}

export interface IFolderRead extends IFolderWrite {
    folders: IFolderRead[],
    files: IFile[]
}
