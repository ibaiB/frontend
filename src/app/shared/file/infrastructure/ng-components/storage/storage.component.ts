import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {IFolderRead} from '../../../domain/IFolder';
import {Observable} from 'rxjs';
import {IFileManagerRead} from '../../../domain/IFileManager';
import {getFileManager} from '../../../application/getFileManager';
import {FileManagerService} from '../../services/file-manager.service';
import {IFile} from '../../../domain/IFile';
import {FileService} from '../../services/file-service.service';
import {FolderService} from '../../services/folder.service';
import {deleteFile} from '../../../application/deleteFile';
import {deleteFolder} from '../../../application/deleteFolder';
import {createFolder} from '../../../application/createFolder';
import {uploadFile} from '../../../application/upload';
import {documentQuery} from '../../../domain/FileUploadQuery';
import {updateFile} from '../../../application/updateFile';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-storage',
    templateUrl: './storage.component.html',
    styleUrls: ['./storage.component.scss'],
    animations: [
        trigger(
            'inOutAnimation',
            [
                state('open', style({transform: 'translateX(-50%)'})),
                state('close', style({transform: 'translateX(0)'})),
                transition('open => close', animate('0.5s ease-in-out')),
                transition('close => open', animate('0.5s ease-in-out'))
            ]
        )
    ]
})
export class StorageComponent implements OnInit {
    @Input() id: string;
    @Input() visible: boolean;
    @Input() readonly: boolean = true;
    @Input() title: string = 'File manager';
    @Input() fileTags: string[] = [];
    @Output() closed = new EventEmitter<any>();

    fileManager: IFileManagerRead;
    folders: IFolderRead[] = [];
    files: IFile[] = [];

    //DETAIL
    currentItem: IFile | IFolderRead;
    thirdField: string;
    thirdFieldContent: string;

    //NAVIGATION
    path: IFolderRead[] = [];
    currentFolder: IFolderRead;

    //UPLOADING
    uploadingFiles: Observable<IFile[]>;

    //EDITING
    editName: boolean = false;
    nameForm: FormGroup;

    constructor(
        private _fileManagerService: FileManagerService,
        private _fileService: FileService,
        private _folderService: FolderService,
        private _fb: FormBuilder
    ) {
    }

    ngOnInit() {
        getFileManager(this.id, this._fileManagerService)
            .subscribe(res => {
                this.fileManager = res;
                this.folders = res.folders;
                this.files = res.files;
            });
    }

    onClickUpload(files: any) {
        const fileArray: File[] = Object.values(files);
        fileArray.forEach(file => {
            documentQuery.uploadName = file.name;
            documentQuery.fileManagerId = this.fileManager.id;
            documentQuery.folderId = this.currentFolder ? this.currentFolder.id : null;
            const cleaned: any = {};
            const keys = Object.keys(documentQuery);
            keys.forEach(key => {
                if (documentQuery[key]) {
                    cleaned[key] = documentQuery[key];
                }
            });
            uploadFile(file, cleaned, this._fileService)
                .subscribe(resFile => this.files.push(resFile));
        });
    }

    onClickCreateFolder() {
        const base = {
            name: 'New Folder',
            description: 'New Folder',
            fileManagerId: this.fileManager.id,
        };

        const extras = this.currentFolder ? {parentFolderId: this.currentFolder.id} : {};
        createFolder({...base, ...extras}, this._folderService)
            .subscribe(newFolder => {
                this.folders.push(newFolder);
            });
    }

    onClickNavigateToFolder(folder: IFolderRead) {
        this.folders = folder.folders;
        this.files = folder.files;
        this.currentFolder = folder;
        this.currentItem = undefined;

        const index = this.path.indexOf(folder);
        if (index > -1) {
            return this.path.splice(index + 1);
        }
        this.path.push(folder);
    }

    onClickClearNavigation() {
        this.folders = this.fileManager.folders;
        this.files = this.fileManager.files;
        this.currentFolder = undefined;
        this.currentItem = undefined;
        this.path = [];
    }

    setCurrentItem(item: any) {
        this.currentItem = item;
        this.thirdField = item['files'] ? 'Files' : 'Extension';
        this.thirdFieldContent = item['files'] ? item['files'].length : item['extension'];
    }

    setBreadcrumbStart(origin: string): string {
        const splatted = origin.split(':');
        return splatted[2] ?? '';
    }

    changeCurrentItemName() {
        const {name} = this.nameForm.value;
        if (this.currentItem['files']) {
            return updateFile({uploadName: name}, this._fileService);
        }
    }

    unSetCurrentItem() {
        this.currentItem = undefined;
    }

    onClickSetFileTag(tag: string) {
        this.currentItem['tags'][0] = tag;
        updateFile({
            id: this.currentItem.id,
            tags: `${tag};`
        }, this._fileService)
            .subscribe(res => this.currentItem = res);
    }

    onClickDownloadCurrentItem() {
        window.location.href = this.currentItem['downloadLink'];
    }

    onClickDeleteCurrentItem() {
        const $delete: Observable<any> = this.currentItem['files']
            ? deleteFolder(this.currentItem.id, this._folderService)
            : deleteFile(this.currentItem.id, this._fileService);

        $delete.subscribe(_ => {
            const array: string = this.currentItem['files'] ? 'folders' : 'files';
            const deleted = this[array].find(f => f.id === this.currentItem.id);
            const index = this[array].indexOf(deleted);
            if (index > -1) {
                this[array].splice(index, 1);
            }
            this.currentItem = null;
        });
    }
}
