import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragndropDialogComponent } from './dragndrop-dialog.component';

describe('DragndropDialogComponent', () => {
  let component: DragndropDialogComponent;
  let fixture: ComponentFixture<DragndropDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragndropDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragndropDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
