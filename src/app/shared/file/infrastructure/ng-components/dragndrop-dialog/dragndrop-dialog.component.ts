import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dragndrop-dialog',
  templateUrl: './dragndrop-dialog.component.html',
  styleUrls: ['./dragndrop-dialog.component.scss']
})
export class DragndropDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DragndropDialogComponent>,
    private _router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  sentFile($event) {
    this.dialogRef.close($event);
  }

}