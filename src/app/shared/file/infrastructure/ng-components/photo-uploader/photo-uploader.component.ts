import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DragndropDialogComponent } from '../dragndrop-dialog/dragndrop-dialog.component';

@Component({
  selector: 'app-photo-uploader',
  templateUrl: './photo-uploader.component.html',
  styleUrls: ['./photo-uploader.component.scss']
})
export class PhotoUploaderComponent implements OnInit {

  @Input() defaultMatIcon: string;
  @Input() defaultMatIconSize: number;
  @Input() photo: string;
  @Input() editable: boolean;
  @Output() photoChanged = new EventEmitter<File>();

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openUploadPhotoDialog() {
    const dialogRef = this.dialog.open(DragndropDialogComponent, {
        data: {
          fileLimit: 1,
          imagePreview: true
        }
    });
    dialogRef.afterClosed().subscribe((files) => {
        if (files) {
          this.photoChanged.emit(files[0].file);
          this.onPhotoUploaded(files[0].file);
        }
    });
  }

  onPhotoUploaded(photo) {
    var reader = new FileReader();
    reader.onload = (e: any) => {
      this.photo = e.target.result;
    };
    reader.readAsDataURL(photo);
  }

}
