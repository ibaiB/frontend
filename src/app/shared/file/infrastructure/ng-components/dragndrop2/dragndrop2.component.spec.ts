import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dragndrop2Component } from './dragndrop2.component';

describe('Dragndrop2Component', () => {
  let component: Dragndrop2Component;
  let fixture: ComponentFixture<Dragndrop2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dragndrop2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dragndrop2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
