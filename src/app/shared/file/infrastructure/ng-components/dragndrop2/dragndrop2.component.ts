import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild,} from '@angular/core';

import {ErrorFacade} from '../../../../error/ErrorManagement';
import Swal from 'sweetalert2';
import {FileUploadQuery} from '../../../domain/FileUploadQuery';
import {FileService} from '../../services/file-service.service';
import {searchFilesByDownloadLinks} from '../../../application/searchByLinks';
import {uploadFile} from '../../../application/upload';
import {IShownFile} from '../../../domain/IFile';

@Component({
  selector: 'app-dragndrop2',
  templateUrl: './dragndrop2.component.html',
  styleUrls: ['./dragndrop2.component.scss'],
})
export class Dragndrop2Component implements OnInit, OnChanges {

  @Input() savedFiles: string[];
  @Input() query: FileUploadQuery;
  @Input() service: FileService;
  @Input() fileLimit: number;
  @Output() saved = new EventEmitter<any>();
  @Output() fileDeleted = new EventEmitter<any>();
  @ViewChild("fileDropRef", { static: false }) fileDropEl: ElementRef;

  shownFiles: IShownFile[] = [];
  files: any[] = [];

  constructor(private _error: ErrorFacade) {}

  ngOnInit() {
    this.refresh();
  }

  ngOnChanges(changes: SimpleChanges) {
    const {savedFiles} = changes;
    if (savedFiles) {
      this.refresh();
    }
  }

  refresh() {
    const links = this.searchLinks();
    searchFilesByDownloadLinks(links, this.service)
        .subscribe(
            (res) => {
              this.shownFiles = res;
              this.shownFiles.sort((a, b) => a.name > b.name ? 1 : -1);
              this.files = [];
            },
            (error) => {
              this.handleSearchLinkError(error, links);
            }
    );
  }

  searchLinks(): string[] {
    const keys = Object.keys(this.savedFiles);
    const savedFiles: string[] = [];

    keys.forEach((k) => {
      if (this.savedFiles[k]) { savedFiles.push(this.savedFiles[k]); }
    });

    return savedFiles;
  }

  removeFile(file) {
    const index1 = this.files.indexOf(file);
    const index2 = this.shownFiles.indexOf(file);
    if (index1 !== -1) {
      this.files.splice(index1, 1);
    }
    if (index2 !== -1) {
      this.shownFiles.splice(index2, 1);
    }
    this.fileDeleted.emit(file);
  }

  uploadFileToServer(file) {
    this.query.uploadName = file.name;
    uploadFile(file, this.query, this.service).subscribe(
      (res) => {
        this.files[this.files.indexOf(file)] = res;
        this.saved.emit(res);
      },
      (error) => {
        this.handleUploadError(error, file);
      }
    );
  }

  handleUploadError(error, file){
    let auxFile = file;
    auxFile.error = true;
    this.files[this.files.indexOf(file)] = auxFile;
    const {error: { message, id } } = error;

    if (error.status === 413) {
      Swal.fire(
          `Your file ${file.name} couldn't be uploaded`,
          `file size it's too large.`,
          'error'
      );
    } else {
      this._error.notifyBackError(
          message,
          id
      );
    }
  }

  handleSearchLinkError(error, links){
    this.shownFiles = links.map((l) => {
      return {
        id: "",
        url: l,
        size: "unknown",
        name: "unknown",
        header: "unknown",
        extension: "unknown",
      };
    });

  }

  /** handle file from browsing */
  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {
    if (!this.fileLimit || files.length <= this.fileLimit) {
      for (const item of files) {
        item.progress = 0;
        let file = item;
        file.error = false;
        this.files.push(file);
        this.uploadFileToServer(item);
      }
      this.fileDropEl.nativeElement.value = "";
    } else {
      this.files = [];
      Swal.fire({
        icon: "warning",
        title: "Please",
        text: `Select only ${this.fileLimit} file/s`,
        confirmButtonColor: "#db5e5e",
      });
    }
  }
}
