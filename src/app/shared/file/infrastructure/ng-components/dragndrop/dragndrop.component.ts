import {Component, ElementRef, EventEmitter, Input, OnChanges, Output, ViewChild,} from '@angular/core';
import Swal from 'sweetalert2';

export interface TypedFile {
  type: string;
  file: any
}

@Component({
  selector: 'app-dragndrop',
  templateUrl: './dragndrop.component.html',
  styleUrls: ['./dragndrop.component.scss'],
})
export class DragndropComponent implements OnChanges {
  @Input() fileTypes: string[] = [];
  @Input() fileLimit: number;
  @Input() imagePreview: boolean;
  @Output() filesOut = new EventEmitter<TypedFile[]>();
 
  public typedFiles: TypedFile[] = [];
  image: string;

  constructor() {}

  ngOnChanges(): void {
    
  }

  @ViewChild("fileDropRef", { static: false }) fileDropEl: ElementRef;
  files: any[] = [];

  /**
   * on file drop handler
   */
  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  /**
   * Delete file from files list
   * @param file
   */
  deleteFile(file: any) {
    const index = this.files.indexOf(file);
    const typed = this.typedFiles.find(t => t.file === file);
    this.files.splice(index, 1);
    if(typed) this.typedFiles.splice(this.typedFiles.indexOf(typed), 1);
    this.image = null;
  }

  /**
   * Simulate the upload process
   */
  uploadFilesSimulator(index: number) {
    setTimeout(() => {
      if (index === this.files.length) {
        return;
      } else {
        const progressInterval = setInterval(() => {
          if (this.files[index] && this.files[index].progress === 100) {
            clearInterval(progressInterval);
            this.uploadFilesSimulator(index + 1);
          } else if(this.files[index] && this.files[index].progress !== 100){
            this.files[index].progress += 5;
          }
        }, 200);
      }
    }, 1000);
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {
    if (!this.fileLimit || files.length <= this.fileLimit) {
      for (const item of files) {
        item.progress = 0;
        this.files.push(item);
      }
      this.fileDropEl.nativeElement.value = "";
      this.uploadFilesSimulator(0);
      this.loadImagePreview();
    } else {
      this.files = [];
      Swal.fire({
        icon: "warning",
        title: "Please",
        text: `Select only ${ this.fileLimit } file/s`,
        confirmButtonColor: "#db5e5e",
      });
    }
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  loadImagePreview() {
    if (this.imagePreview) {
      var reader = new FileReader();
      reader.onload = (e: any) => {
        this.image = e.target.result;
      };
      reader.readAsDataURL(this.files[0]);;}
  }

  set(type: string, file: any){
    const existsTypedFile: TypedFile = this.typedFiles.find((f) => f.type === type);
    const fileWithOtherType: TypedFile = this.typedFiles.find((f) => f.file === file)

    if(existsTypedFile) {
      this.overwrite(type, existsTypedFile.file, file);
    } else if(fileWithOtherType){
      const index = this.typedFiles.indexOf(fileWithOtherType);
      this.changeType(index, fileWithOtherType, type);
    } else {
      this.typedFiles.push({type: type, file: file});
    }
  }

  changeType(index: number, file: any, newType: string){
    this.typedFiles[index] = {type: newType, file: file};
  }

  overwrite(type: string, oldFile: any, newFile: any){
    Swal.fire({
      icon: "warning",
      title: `You have already uploaded a ${type} file`,
      text: `Do you want to overwrite ${oldFile.name} file?`,
      confirmButtonColor: "#db5e5e",
      showCancelButton: true,
      cancelButtonColor: "#767676",
    }).then(result => {
      if(result.isConfirmed){
        this.deleteFile(oldFile);
        this.typedFiles.push({type: type, file: newFile});
      } else {
        this.deleteFile(newFile);
      }
    });
  }

  upload() {
    if (this.fileTypes.length === 0) {
      this.filesOut.emit(this.files.map(f => f = {
        type: "",
        file: f
      }));
    } else if (this.files.length === this.typedFiles.length) {
      this.filesOut.emit(this.typedFiles);
    } else {
      Swal.fire({
        icon: "error",
        title: "Please",
        text: "Select a type for all files",
        confirmButtonColor: "#db5e5e",
      });
    }
  }
}
