import {Observable} from 'rxjs';
import {IWebFolder} from '../../domain/IWebFolder';
import {IList} from '../../../generics/IList';

export abstract class FolderAbstractService {
    abstract create(folder: IWebFolder): Observable<IWebFolder>;

    abstract update(folder: IWebFolder): Observable<IWebFolder>;

    abstract delete(id: string): Observable<any>;

    abstract get(id: string): Observable<IWebFolder>;

    abstract search(query: string): Observable<IList<IWebFolder>>;
}
