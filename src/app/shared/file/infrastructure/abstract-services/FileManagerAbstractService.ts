import {Observable} from 'rxjs';
import {IWebFileManagerRead, IWebFileManagerWrite} from '../../domain/IWebFileManager';
import {IList} from '../../../generics/IList';

export abstract class FileManagerAbstractService {
    abstract create(fileManager: IWebFileManagerWrite): Observable<IWebFileManagerRead>;

    abstract update(fileManager: IWebFileManagerWrite): Observable<IWebFileManagerRead>;

    abstract delete(id: string): Observable<any>;

    abstract get(id: string): Observable<IWebFileManagerRead>;

    abstract search(query: any): Observable<IList<IWebFileManagerRead>>;

    abstract addOwner(idFileManager: string, newOwner: string): Observable<IWebFileManagerRead>;

    abstract removeOwner(idFileManager: string, removedOwner: string): Observable<IWebFileManagerRead>;
}
