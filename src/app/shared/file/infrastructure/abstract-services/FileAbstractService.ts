import {Observable} from 'rxjs';
import {FileUploadQuery} from '../../domain/FileUploadQuery';
import {IWebFile} from '../../domain/IWebFile';


export abstract class FileAbstractService {
    abstract upload(file: File, query: FileUploadQuery): Observable<IWebFile>;

    abstract update(file: any): Observable<IWebFile>;

    abstract findByIds(ids: string[]): Observable<IWebFile[]>;

    abstract findByLinks(links: string[]): Observable<IWebFile[]>;

    abstract delete(id: string): Observable<any>;
}
