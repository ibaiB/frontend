import {Injectable} from '@angular/core';
import {FileAbstractService} from '../abstract-services/FileAbstractService';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IWebFile} from '../../domain/IWebFile';

@Injectable({
    providedIn: 'root'
})
export class FileService extends FileAbstractService {
    private BASE_URL: string = 'http://ec2-15-237-10-209.eu-west-3.compute.amazonaws.com/storage/api/v0/';
    private ENDPOINT: string = 'customFiles';

    constructor(private _http: HttpClient) {
        super();
    }

    upload(file: File, query: any): Observable<IWebFile> {
        let formData: FormData = new FormData();
        formData.append('file', file, file.name);
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');

        return this._http.post<IWebFile>(`${this.BASE_URL}${this.ENDPOINT}/upload`, formData, {
            headers: headers,
            params: new HttpParams({fromObject: query})
        });
    }

    update(file: any): Observable<IWebFile> {
        return this._http.put<IWebFile>(`${this.BASE_URL}${this.ENDPOINT}/${file.id}`, file);
    }

    findByIds(ids: string[]): Observable<IWebFile[]> {
        return this._http.get<IWebFile[]>(`${this.BASE_URL}${this.ENDPOINT}/ids=${ids.join(',')}`);
    }

    findByLinks(links: string[]): Observable<IWebFile[]> {
        return this._http.post<IWebFile[]>(`${this.BASE_URL}${this.ENDPOINT}/search/downloadLink`, {downloadLinks: links});
    }

    delete(id: string): Observable<any> {
        return this._http.delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }
}
