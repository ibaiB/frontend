import {Injectable} from '@angular/core';
import {FolderAbstractService} from '../abstract-services/FolderAbstractService';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {IWebFolder} from '../../domain/IWebFolder';
import {IList} from '../../../generics/IList';

@Injectable({
    providedIn: 'root'
})
export class FolderService extends FolderAbstractService {
    private BASE_URL: string = 'http://ec2-15-237-10-209.eu-west-3.compute.amazonaws.com/storage/api/v0/';
    private ENDPOINT: string = 'folders';

    constructor(private _http: HttpClient) {
        super();
    }

    create(folder: IWebFolder): Observable<IWebFolder> {
        return this._http.post<IWebFolder>(`${this.BASE_URL}${this.ENDPOINT}`, folder);
    }

    delete(id: string): Observable<IWebFolder> {
        return this._http.delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    get(id: string): Observable<IWebFolder> {
        return this._http.get<IWebFolder>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    search(query: any): Observable<IList<IWebFolder>> {
        return this._http.get<IList<IWebFolder>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
    }

    update(folder: IWebFolder): Observable<IWebFolder> {
        return this._http.put<IWebFolder>(`${this.BASE_URL}${this.ENDPOINT}/${folder.id}`, folder);
    }
}
