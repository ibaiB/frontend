import {Injectable} from '@angular/core';
import {FileManagerAbstractService} from '../abstract-services/FileManagerAbstractService';
import {Observable} from 'rxjs';
import {IWebFileManagerRead, IWebFileManagerWrite} from '../../domain/IWebFileManager';
import {IList} from '../../../generics/IList';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class FileManagerService extends FileManagerAbstractService {
    //@TODO temporal
    private BASE_URL: string = 'http://ec2-15-237-10-209.eu-west-3.compute.amazonaws.com/storage/api/v0/';
    private ENDPOINT: string = 'fileManagers';

    constructor(private _http: HttpClient) {
        super();
    }

    addOwner(idFileManager: string, newOwner: string): Observable<IWebFileManagerRead> {
        return this._http.put<IWebFileManagerRead>(`${this.BASE_URL}${this.ENDPOINT}/${idFileManager}/owners?owner=${newOwner}`, '');
    }

    create(fileManager: IWebFileManagerWrite): Observable<IWebFileManagerRead> {
        return this._http.post<IWebFileManagerRead>(`${this.BASE_URL}${this.ENDPOINT}`, fileManager);
    }

    delete(id: string): Observable<any> {
        return this._http.delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    get(id: string): Observable<IWebFileManagerRead> {
        return this._http.get<IWebFileManagerRead>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    removeOwner(idFileManager: string, removedOwner: string): Observable<IWebFileManagerRead> {
        return this._http.delete<IWebFileManagerRead>(`${this.BASE_URL}${this.ENDPOINT}/${idFileManager}/owners?owner=${removedOwner}`);
    }

    search(query: any): Observable<IList<IWebFileManagerRead>> {
        return this._http.get<IList<IWebFileManagerRead>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
    }

    update(fileManager: IWebFileManagerWrite): Observable<IWebFileManagerRead> {
        return this._http.put<IWebFileManagerRead>(`${this.BASE_URL}${this.ENDPOINT}/${fileManager.id}`, fileManager);
    }


}
