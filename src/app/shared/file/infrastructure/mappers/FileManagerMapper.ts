import {IFileManagerRead} from '../../domain/IFileManager';
import {FileMapper} from './FileMapper';
import {FolderMapper} from './FolderMapper';

export class FileManagerMapper {
    mapTo(object: any): IFileManagerRead {
        const fileMapper = new FileMapper();
        const folderMapper = new FolderMapper();

        const {
            folders,
            customFiles,
            idFileManager,
            ...rest
        } = object;

        return {
            id: idFileManager,
            ...rest,
            folders: folders.map(f => folderMapper.mapTo(f)),
            files: customFiles.map(f => fileMapper.mapTo(f))
        };
    }
}
