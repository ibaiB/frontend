import {IFolderRead} from '../../domain/IFolder';
import {FileMapper} from './FileMapper';

export class FolderMapper {
    mapTo(object: any): IFolderRead {
        const folderMapper = new FolderMapper();
        const fileMapper = new FileMapper();

        const {
            idFolder,
            folders,
            files,
            createdDate,
            ...rest
        } = object;

        return {
            id: idFolder,
            ...rest,
            created: createdDate,
            path: './',
            folders: folders.map(f => folderMapper.mapTo(f)),
            files: files.map(f => fileMapper.mapTo(f))
        };
    }
}
