import {fileCategories, IFile, IShownFile} from '../../domain/IFile';
import {formatBytes} from '../../application/formatBytes';

export class FileMapper {
    mapTo(object: any): IFile {
        const mapper = new FileMapper();
        const {
            originalName,
            owner,
            sharedUsers,
            extension,
            size,
            downloadLink,
            storageLocation,
            uploadedFileId,
            permission,
            idCustomFile,
            uploadName,
            sharedDomains,
            createdDate,
            lastModified,
            tags
        } = object;

        return {
            originalName: originalName,
            owner: owner,
            sharedUsers: sharedUsers,
            extension: extension,
            size: formatBytes(size),
            downloadLink: downloadLink,
            storageLocation: storageLocation,
            uploadedFileId: uploadedFileId,
            permission: permission,
            id: idCustomFile,
            uploadName: uploadName,
            sharedDomains: sharedDomains,
            category: mapper.setCategory(extension),
            created: createdDate,
            lastModified,
            path: './',
            tags: tags ? tags.split(';').filter(i => i) : []
        };
    }

    setCategory(extension: string) {
        if (extension === '.docx') {
            return fileCategories.WORD;
        }
        if (extension === '.xls') {
            return fileCategories.EXCEL;
        }
        if (extension === '.ppt') {
            return fileCategories.PPT;
        }
        if (extension === ('.png' || '.jpeg' || '.jpg')) {
            return fileCategories.IMG;
        }
        if (extension === '.pdf') {
            return fileCategories.PDF;
        }
        return fileCategories.DEF;
    }

    mapToShownFile(arr: any[]): IShownFile[] {
        const mapped: IShownFile[] = [];

        arr.forEach(f => {
            const {
                originalName,
                extension,
                size,
                downloadLink,
                idCustomFile,
            } = f;

            mapped.push({
                header: '',
                id: idCustomFile,
                url: downloadLink,
                extension: extension,
                size: formatBytes(size),
                name: originalName
            });
        });

        return mapped;
    }
}
