import {FileManagerAbstractService} from '../infrastructure/abstract-services/FileManagerAbstractService';
import {FileManagerMapper} from '../infrastructure/mappers/FileManagerMapper';
import {map} from 'rxjs/operators';

export function getFileManager(id: string, service: FileManagerAbstractService) {
    const mapper = new FileManagerMapper();
    return service.get(id).pipe(map(mapper.mapTo));
}
