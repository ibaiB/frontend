import {FolderService} from '../infrastructure/services/folder.service';

export function deleteFolder(id: string, service: FolderService) {
    return service.delete(id);
}
