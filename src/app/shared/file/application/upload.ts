import {FileAbstractService} from '../infrastructure/abstract-services/FileAbstractService';
import {FileMapper} from '../infrastructure/mappers/FileMapper';
import {map} from 'rxjs/operators';
import {FileUploadQuery} from '../domain/FileUploadQuery';

export function uploadFile(file: File, query: FileUploadQuery, service: FileAbstractService) {
    const mapper: FileMapper = new FileMapper();
    return service.upload(file, query).pipe(map(mapper.mapTo));
}
