import {FileService} from '../infrastructure/services/file-service.service';

export function deleteFile(id: string, service: FileService) {
    return service.delete(id);
}
