import {FileAbstractService} from '../infrastructure/abstract-services/FileAbstractService';
import {FileMapper} from '../infrastructure/mappers/FileMapper';
import {map} from 'rxjs/operators';

export function searchFilesByDownloadLinks(links: string[], service: FileAbstractService) {
    const mapper = new FileMapper();
    return service.findByLinks(links)
        .pipe(map(mapper.mapToShownFile));
}
