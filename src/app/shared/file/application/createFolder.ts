import {FolderAbstractService} from '../infrastructure/abstract-services/FolderAbstractService';
import {FolderMapper} from '../infrastructure/mappers/FolderMapper';
import {map} from 'rxjs/operators';

export function createFolder(folder: any, service: FolderAbstractService) {
    const mapper = new FolderMapper();
    return service.create(folder).pipe(map(mapper.mapTo));
}
