import {FileAbstractService} from '../infrastructure/abstract-services/FileAbstractService';

export function searchFiles(ids: string[], service: FileAbstractService) {
    return service.findByIds(ids);
}
