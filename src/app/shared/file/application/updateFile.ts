import {FileAbstractService} from '../infrastructure/abstract-services/FileAbstractService';
import {FileMapper} from '../infrastructure/mappers/FileMapper';
import {map} from 'rxjs/operators';

export function updateFile(file: any, service: FileAbstractService) {
    const mapper = new FileMapper();
    return service.update(file).pipe(map(mapper.mapTo));
}
