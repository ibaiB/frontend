import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import * as CustomEditor from './custom-ckeditor/build/ckeditor.js';

@Component({
  selector: 'app-ckeditor',
  templateUrl: './ckeditor.component.html',
  styleUrls: ['./ckeditor.component.scss'],
})
export class CkeditorComponent implements OnInit {
  public Editor = CustomEditor;

  @Input() control: FormControl = new FormControl('');
  @Input() disabled = false;
  @Input() data = '';
  editorConfig = {
    toolbar: {
      items: [
        'heading',
        '|',
        'bold',
        'italic',
        'link',
        'bulletedList',
        'numberedList',
        '|',
        'undo',
        'redo',
        '|',
        'code',
        'codeBlock'
      ]
    },
    // This value must be kept in sync with the language defined in webpack.config.js.
    language: 'en'
  };

  constructor() {
  }

  ngOnInit(): void {
  }

}
