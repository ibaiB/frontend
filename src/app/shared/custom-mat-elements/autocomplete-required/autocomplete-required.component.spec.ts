import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AutocompleteRequiredComponent} from './autocomplete-required.component';

describe('AutocompleteRequiredComponent', () => {
  let component: AutocompleteRequiredComponent;
  let fixture: ComponentFixture<AutocompleteRequiredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteRequiredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteRequiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
