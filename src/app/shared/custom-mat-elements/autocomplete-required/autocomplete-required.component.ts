import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Observable} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {map, startWith} from "rxjs/operators";

@Component({
  selector: 'app-autocomplete-required',
  templateUrl: './autocomplete-required.component.html',
  styleUrls: ['./autocomplete-required.component.scss']
})
export class AutocompleteRequiredComponent implements OnInit, OnChanges {
  @Input() matLabel: string;
  @Input() placeholder: string;
  @Input() appearance: string;
  @Input() defaultValue: string;
  @Input() options: string[] = [];
  @Input() width: string;
  @Output() selectedValue = new EventEmitter<string>();
  @Output() searchValue = new EventEmitter<string>();
  
  filteredOptions: Observable<string[]>;
  form: FormGroup;
  isValid: boolean = true;
  value: string;
  started: boolean;

  constructor(private _fb: FormBuilder) { }

  ngOnInit(){
    this.started = true;
    this._initForm();
    this._setFilter();
  }

  private _initForm(){
    this.form = this._fb.group({
      input: [this.defaultValue ? this.defaultValue : '', Validators.required]
    })
  }

  private _setFilter(){
    this.filteredOptions = this.form.controls.input.valueChanges.pipe(
        startWith(''),
        map(value => this
            .options
            .filter(option => option.toLowerCase().includes(value.toString().toLowerCase())))
    );
  }

  setValue(value: string){
    this.isValid = true;
    this.form.controls.input.setValue(value);
    if(this.form.valid) this.selectedValue.emit(value);
  }

  selectionIsValid(){
    const input = this.form.get('input').value
    const selected = this.options.filter(elem => elem === input)[0];
    this.isValid = selected !== undefined;
  }

  emptySelection(event: any) {
    if (event.key === "Backspace") {
      this.selectedValue.emit('')
    }
    this.searchValue.emit(event.target.value);
  }

  ngOnChanges(data: SimpleChanges){
    if(this.started) {
      this._setFilter();
    }
  }  
}
