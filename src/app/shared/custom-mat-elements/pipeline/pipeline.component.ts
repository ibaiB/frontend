import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CdkDragDrop, transferArrayItem} from '@angular/cdk/drag-drop';

interface IPipelineGroup{
  columns: IPipelineColumn[];
  order: number;
}

interface IPipelineColumn{
  header: IPipelineColumnHeader;
  data: any[];
  order: number;
  isDraggable: boolean;
  id: string;
}

interface IPipelineColumnHeader{
  name: string;
  description: string;
  color: string;
}

interface IDropped{
  item: any,
  movedTo: string
}

@Component({
  selector: 'app-pipeline',
  templateUrl: './pipeline.component.html',
  styleUrls: ['./pipeline.component.scss']
})
export class PipelineComponent implements OnInit {

  @Input() groups: IPipelineGroup[];
  @Output() dropped = new EventEmitter<IDropped>();

  constructor() { }

  ngOnInit(): void {
  }

  drop(event: CdkDragDrop<any[], any>){
    if (event.previousContainer !== event.container) {
      transferArrayItem(
          event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex
      );

      this.dropped.emit({
        item: event.container.data[event.currentIndex],
        movedTo: event.container.id
      });
    }
  }

}
