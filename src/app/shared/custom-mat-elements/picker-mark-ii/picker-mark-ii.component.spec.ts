import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickerMarkIIComponent } from './picker-mark-ii.component';

describe('PickerMarkIIComponent', () => {
  let component: PickerMarkIIComponent;
  let fixture: ComponentFixture<PickerMarkIIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickerMarkIIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickerMarkIIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
