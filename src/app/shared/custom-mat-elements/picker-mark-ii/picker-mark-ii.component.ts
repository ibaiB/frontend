import { Component } from '@angular/core';
import { Field } from '../dynamic';
@Component({
  selector: 'app-picker-mark-ii',
  templateUrl: './picker-mark-ii.component.html',
  styleUrls: ['./picker-mark-ii.component.scss']
})
export class PickerMarkIIComponent extends Field{
  constructor() {super();}
}
