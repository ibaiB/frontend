import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {IAccountTable} from '../../../crm/entities/account/domain/IAccountTable';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {DynamicTableConfigPanelComponent} from './dynamic-table-config-panel/dynamic-table-config-panel.component';
import {IDynamicColumn} from './IDynamicColumn';

@Component({
  selector: 'app-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.scss']
})
export class DynamicTableComponent implements OnChanges {

  @Input() data: any[];
  @Input() class: string;
  @Input() columns: IDynamicColumn[];
  @Input() actions: { name: string, icon: string }[];
  @Input() totalElements: number;

  @Output() navigate = new EventEmitter<{ id: string, property: string }>();
  @Output() action = new EventEmitter<{ action: string, item: any }>();
  @Output() pageSize = new EventEmitter<PageEvent>();

  columnNames: string[] = [];
  noResults: boolean = false;

  dataSource = new MatTableDataSource<IAccountTable>();
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog) {
  }

  openConfigPanel() {
    const dialogRef = this.dialog.open(DynamicTableConfigPanelComponent, {data: this.columns});

    dialogRef
        .afterClosed()
        .subscribe((result: { name: string, shown: boolean }[]) => {
          //this.columns = result;
        })
  }

  navigateTo(event: { id: string, property: string, item?: any }) {
    this.navigate.emit(event);
  }

  emitAction(event: { action: string, item: any }) {
    this.action.emit(event);
  }

  private _setWarningDates() {
    this.data.forEach(item => {
      if (item.warningDate !== undefined && item.warningDate) {
        item['isLate'] = new Date(item.warningDate) <= new Date();
      }
    })
  }

  private _setEmployeeRequestsName() {
    this.data.forEach(item => {
      if (item.state !== undefined && item.state) {
        item['isIncidence'] = item.state === 'incidenciaSinResolver';
      }
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.dataSource = new MatTableDataSource<any>(changes.data.currentValue);
    this.dataSource.sort = this.sort;
    this.columnNames = this.columns.filter(item => item.shown === true).map(item => item.property);
    setTimeout(() => this.dataSource.paginator = this.paginator);
    this._setWarningDates();
    this._setEmployeeRequestsName();
    this.noResults = this.dataSource.data.length === 0;
  }

  pageChanged(event: PageEvent) {
    this.pageSize.emit(event);
  }

}
