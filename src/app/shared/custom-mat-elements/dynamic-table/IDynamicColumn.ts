export interface IDynamicColumn {
    name: string,
    property: string,
    shown: boolean,
    type: string,
    class: string
}
