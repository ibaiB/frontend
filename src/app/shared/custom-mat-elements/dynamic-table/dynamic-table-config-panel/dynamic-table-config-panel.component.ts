import {Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { FormGroup } from '@angular/forms';
import { MatCheckbox} from '@angular/material/checkbox';
import { IAuto, ITableConfig } from '../../dynamic';

@Component({
  selector: 'app-dynamic-table-config-panel',
  templateUrl: './dynamic-table-config-panel.component.html',
  styleUrls: ['./dynamic-table-config-panel.component.scss']
})
export class DynamicTableConfigPanelComponent implements OnInit {

  @Input() columnsConfig : ITableConfig;
  @Input() filtersConfig : IAuto[];
  @Input() visibility : boolean;
  @Output() configChanged = new EventEmitter<any[]>();
  @Output() closed = new EventEmitter<any>();
  
  checkboxGroup: FormGroup = new FormGroup({});
  @ViewChildren('tableCheckbox') checkboxes: QueryList<MatCheckbox>;
  
  constructor() {
  }

  ngOnInit(): void {
  }

  close() {
    this.closed.emit();
  }

  check(element: any, type: string) {
    if(type === 'column') {
      const columnConfig = this.columnsConfig.columns.find(column => column.name === element.name);
      const filterConfig = this.filtersConfig.find(filter => filter.label === element.name);
      if (columnConfig) {
        columnConfig.shown = !columnConfig.shown;
        if (filterConfig) filterConfig.shown = columnConfig.shown;
      }
    }
    if(type === 'filter') {
      const filterConfig = this.filtersConfig.find(filter => filter.label === element.label)
      if (filterConfig) filterConfig.shown = !filterConfig.shown;
    }
    this.configChanged.emit([this.columnsConfig, this.filtersConfig])
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columnsConfig.columns, event.previousIndex, event.currentIndex);
    moveItemInArray(this.filtersConfig, event.previousIndex, event.currentIndex);
    this.configChanged.emit([this.columnsConfig, this.filtersConfig])
  }

  filterDisable(name: string) {
    return !this.columnsConfig.columns.find(column => column.name === name)?.shown;
  }
}
