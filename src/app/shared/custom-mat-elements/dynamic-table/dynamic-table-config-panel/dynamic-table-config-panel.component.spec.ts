import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DynamicTableConfigPanelComponent} from './dynamic-table-config-panel.component';

describe('DynamicTableConfigPanelComponent', () => {
  let component: DynamicTableConfigPanelComponent;
  let fixture: ComponentFixture<DynamicTableConfigPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DynamicTableConfigPanelComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicTableConfigPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
