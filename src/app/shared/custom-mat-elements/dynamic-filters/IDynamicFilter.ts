export interface IDynamicFilter {
    name: string,
    property: string,
    type: string,
    defaultValue?: string,
    options?: string[],
    shown: boolean,
    appearance: string,
    class: string
}
