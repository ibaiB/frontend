import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DynamicFiltersConfigPanelComponent} from './dynamic-filters-config-panel.component';

describe('DynamicFiltersConfigPanelComponent', () => {
  let component: DynamicFiltersConfigPanelComponent;
  let fixture: ComponentFixture<DynamicFiltersConfigPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DynamicFiltersConfigPanelComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFiltersConfigPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
