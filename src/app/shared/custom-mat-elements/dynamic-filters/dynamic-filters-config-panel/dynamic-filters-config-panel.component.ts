import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DynamicFiltersComponent} from "../dynamic-filters.component";

@Component({
  selector: 'app-dynamic-filters-config-panel',
  templateUrl: './dynamic-filters-config-panel.component.html',
  styleUrls: ['./dynamic-filters-config-panel.component.scss']
})
export class DynamicFiltersConfigPanelComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: { name: string, shown: boolean }[],
              public dialogRef: MatDialogRef<DynamicFiltersComponent>) {
  }

  ngOnInit(): void {
  }

  onSaveClick() {
    this.dialogRef.close(this.data)
  }

  closeDialog() {
    this.dialogRef.close();
  }


}
