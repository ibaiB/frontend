import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {IAuto} from '../dynamic';


/*
  Filters required items:
    Input:
      options -> []
      prop -> The name of the field to search in the table.
      retProp -> blank
    Date:
      TODO:
    Autocomplete:
      options -> An array of strings
      prop -> blank
      retProp -> blank
      searchValue -> The name of the field to search in the table. Ex: accountId
    Autoselect:
      options -> Array of items
      prop -> property to be shown in the options. Ex: name
      retProp -> Return field of the selected item. Ex: id
      searchValue -> The name of the field to search in the table. Ex: accountId
      service -> The service used to fetch data. Has to have a search method
      mapper -> The mapper to change the name of the variables to fetch. Has to have a mapFrom method
*/
@Component({
  selector: 'app-dynamic-filters',
  templateUrl: './dynamic-filters.component.html',
  styleUrls: ['./dynamic-filters.component.scss']
})
export class DynamicFiltersComponent implements OnInit {

  @Input() filters: IAuto[];
  @Input() showFilters: boolean;
  @Output() query = new EventEmitter();
  @Output() search = new EventEmitter();

  form: FormGroup;

  constructor(private _fb: FormBuilder, public dialog: MatDialog) {
  }

  ngOnInit() {
    let controlsConfig = {};
    this.filters.forEach(filter => {
      if (filter.searchValue) {
        controlsConfig[filter.searchValue] = [filter.value];
      } else {
        controlsConfig[filter.prop] = [filter.value];
      }
    });


    this.form = this._fb.group(controlsConfig);
  }

  setAutocompleteValue(entry: { formFilter: string, value: any, type: string }) {

    this.form.controls[entry.formFilter].setValue(entry.value);
    this.emit();
  }

  setInputValue(entry: { formFilter: string, value: any, type: string }) {
    this.form.controls[entry.formFilter].setValue(entry.value.target.value);
    this.emit();
  }

  clearDate(entry: { formFilter: string, event: any }) {
    this.form.controls[entry.formFilter].setValue(null);
    this.emit();
  }

  filterDate(entry: { formFilter: string, event: any }) {
    this.form.controls[entry.formFilter].setValue(entry.event.value);
    this.emit();
  }

  hasValue(formFilter: string): boolean {
    return this.form.controls[formFilter].value;
  }

  selectedItem(entry: { searchValue: string, value: any, type: string }) {
    this.form.controls[entry.searchValue].setValue(entry.value.id);
    this.emit();
  }

  clear(entry: { formFilter: string }) {
    this.form.controls[entry.formFilter].setValue('');
    this.emit();
  }

  emit() {
    let query = {};
    let formKeys = Object.keys(this.form.value);

    formKeys.forEach(key => {
      if (this.form.value[key]) {
        query[key] = this.form.value[key];
      }
    });
    this.query.emit(query);
  }

  buildFilter(item) {
    return {
      options: item.options,
      prop: item.prop,
      retProp: item.retProp,
      class: item.class,
      appearance: item.appearance,
      label: item.label,
      placeholder: item.placeholder,
      searchValue: item.searchValue,
      defaultValue: item.defaultValue
    };
  }

  showAutocomplete(item) {
    return item.shown === true && item.type === 'autocomplete';
  }

  showAutoSelect(item) {
    return item.shown && item.type === 'autoselect';
  }

  showDate(item) {
    return item.shown === true && item.type === 'date';
  }

  showInput(item) {
    return item.shown === true && item.type === 'input';
  }
}
