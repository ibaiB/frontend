import { QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { AfterViewChecked } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MatCheckbox, MatCheckboxChange } from '@angular/material/checkbox';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { IList } from '../../generics/IList';
import { ITableColumn, ITableConfig } from '../dynamic';
const defaultList: IList<any> = {
  content: [],
  numberOfElements: 0,
  totalElements: 0,
  totalPages: 0,
};

const defaultConfig: ITableConfig = {
  class: 'default-table',
  actions: [],
  multiSelect: false,
  columns: [],
};

/**
 * This is a dynamic table that allows to generate dynamic columns
 * @input service
 * @input mapper
 * @input options
 * @input config
 * @output action
 * @output selected
 * **/
@Component({
  selector: 'app-dynamic-table-mark-ii',
  templateUrl: './dynamic-table-mark-ii.component.html',
  styleUrls: ['./dynamic-table-mark-ii.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicTableMarkIiComponent
  implements OnInit, AfterViewInit, OnChanges, AfterViewChecked {


  @Input() data: IList<any>;
  @Input() config: ITableConfig = defaultConfig;

  @Output() action = new EventEmitter<any>();
  @Output() selected = new EventEmitter<any[]>();
  @Output() pagination = new EventEmitter<any>();
  @Output() order = new EventEmitter<any>();

  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = [];
  resultsLength = 0;
  pageIndex;
  pageSize;
  columnSort;
  direccion;
  isLoadingResults = true;
  noResults: boolean = true;
  selection: any[] = [];
  checkboxGroup: FormGroup = new FormGroup({
  });

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChildren('tableCheckbox') checkboxes: QueryList<MatCheckbox>;
  @ViewChild('selectAllCheckbox') selectAllCheckbox: MatCheckbox;

  constructor(private _router: Router,
    private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>(this.data?.content);
    this.pageIndex=this.config.pagination.page;
    this.pageSize=this.config.pagination.size;
    this.columnSort =this.config.order.orderField;
    this.direccion = this.config.order.order;
  }

  resetPageIndex(){
    this.paginator.pageIndex=0;
  }

  ngAfterViewInit() {
    this.paginator.page.subscribe(() => {
      this.pagination.emit({
        page: this.paginator.pageIndex,
        size: this.paginator.pageSize,
      });
    });

    this.sort.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.order.emit({
        orderField: this.sort.active,
        order: this.sort.direction,
        page: 0,
        size: this.paginator.pageSize
      });
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.displayedColumns=[];
    if (this.config.multiSelect) { this.displayedColumns.push('select'); }
    this.config.columns.forEach((column) =>
      {if(column.shown)this.displayedColumns.push(column.prop)}
    );
    
    if (this.config.actions) { this.displayedColumns.push('Actions'); }
    if (changes.data?.currentValue) {
      this.isLoadingResults = false;
      this.resultsLength = changes.data.currentValue.totalElements;
      changes.data.currentValue.numberOfElements === 0 ? this.noResults = true : this.noResults = false;
      this.dataSource = new MatTableDataSource<any>(changes.data.currentValue.content);
      this.dataSource.data = changes.data.currentValue.content;
    }
    if (this.selectAllCheckbox) this.selectAllCheckbox.checked = false;
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  /** Emits an array of selected values whenever a row is selected/deselected */
  check(element: any) {
    if (this.selection.includes(element)) {
      this.selection = this.selection.filter(item => item.id !== element.id);
    } else {
      this.selection.push(element);
    }
    this.selectAllCheckbox.checked = this._checkIfAllSelected();
    this.selected.emit(this.selection);
  }

  /** Emits an array of all selected values */
  checkAll() {
    this.selection = this.selectAllCheckbox.checked ? this.data.content : [];
    this.checkboxes.forEach(c => c.checked = this.selectAllCheckbox.checked);
    this.selected.emit(this.selection);
  }

  _checkIfAllSelected(): boolean {
    return !this.checkboxes.some(cb => !cb.checked);
  }

  /** Emits selected action **/
  selectAction(action: string, item:any) {
    this.action.emit({action:action, item:item});
  }

  /** Shows/hides column when checkbox is selected**/
  showColumn(event: MatCheckboxChange) {
    this.config.columns.find((c) => c.name === event.source.value).shown = event.checked;
  }

  /** Navigates to specified route**/
  navigateTo(column: ITableColumn, item: any) {
    if(!column.routeId)this.action.emit({action:column.name, item:item});
    if(column.routeId)this._router.navigate([`${column.route}/${item[column.routeId]}`]);
  }

  _checkOverflow (element: any) {
    return (element.offsetHeight < element.scrollHeight ||
        element.offsetWidth < element.scrollWidth)
  }

  _isLate(date: string) {
      return new Date(date) <= new Date();
  }

}
