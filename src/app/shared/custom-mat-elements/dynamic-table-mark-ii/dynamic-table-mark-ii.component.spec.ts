import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicTableMarkIiComponent } from './dynamic-table-mark-ii.component';

describe('DynamicTableMarkIiComponent', () => {
  let component: DynamicTableMarkIiComponent;
  let fixture: ComponentFixture<DynamicTableMarkIiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicTableMarkIiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicTableMarkIiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
