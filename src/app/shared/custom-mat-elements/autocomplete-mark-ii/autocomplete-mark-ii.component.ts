import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { startWith } from 'rxjs/operators';
import { Auto, Field } from '../dynamic';


/**
 * This component is intended for limited sets
 */
@Component({
  selector: 'app-autocomplete-mark-ii',
  templateUrl: './autocomplete-mark-ii.component.html',
  styleUrls: ['./autocomplete-mark-ii.component.scss']
})
export class AutocompleteMarkIIComponent extends Auto implements OnInit{

  @Input() options: any[] = [];

  filteredOptions: Observable<any[]>;

  constructor() {super();}

  ngOnInit(): void {
    this._setFilter();
  }

  private _setFilter(){
    this.filteredOptions = this.myControl.valueChanges
        .pipe(
            startWith(''),
            map(value => typeof value === 'string' ? value : value[this.shownField]),
            map(field => field ? this._filter(field) : this.options.slice())
        );
  }

  private _filter(field: string): any[] {
    const filterValue = field.toLowerCase();
    return this.options.filter(option => option[this.shownField].toLowerCase().indexOf(filterValue) === 0);
  }
}
