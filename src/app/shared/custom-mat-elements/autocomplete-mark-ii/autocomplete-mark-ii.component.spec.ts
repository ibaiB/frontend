import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteMarkIIComponent } from './autocomplete-mark-ii.component';

describe('AutocompleteMarkIIComponent', () => {
  let component: AutocompleteMarkIIComponent;
  let fixture: ComponentFixture<AutocompleteMarkIIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteMarkIIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteMarkIIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
