import {Injectable, NgZone} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {ErrorResponseComponent} from '../presentation/error-response/error-response.component';
import {authConfig} from '../../auth/config/auth.config';
import {CookieFacadeService} from '../../auth/session/infrastructure/services/cookie.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {errors} from '../presentation/error-response/error-dictionary';

@Injectable({
    providedIn: 'root'
})
export class HttpErrorInterceptorService implements HttpInterceptor {

    timeout = 2 * 60 * 1000;
    maintenanceErrors = [503, 502];
    reportableErrors = [400, 500];

    errorData: any;

    constructor(
        private _dialog: MatDialog,
        private _cookieFacade: CookieFacadeService,
        private _router: Router,
        private _zone: NgZone
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
        return next.handle(request)
            .pipe(
                catchError((error: any) => {
                    // 401
                    if (error.status === 401) {
                        this._cookieFacade.delete(authConfig.SESSION_STORAGE_KEY);
                        return this._router.navigate([`/login`]);
                    }

                    // OTHER HTTP ERRORS
                    const isMaintenanceError = this.maintenanceErrors.includes(error.status);
                    const isReportable = this.reportableErrors.includes(error.status);

                    this.errorData = errors.find(e => e.error === error.status.toString());
                    if (!this.errorData) {
                        this.errorData = errors[errors.length - 1];
                    }

                    const dialogRef = this._dialog.open(ErrorResponseComponent, {
                        data: {
                            error: error,
                            code: this.errorData.error,
                            motive: this.errorData.motive,
                            explanation: this.errorData.explanation,
                            timeout: isMaintenanceError ? this.timeout : null,
                            reportable: isReportable
                        },
                        disableClose: true
                    });

                    dialogRef
                        .afterClosed()
                        .subscribe(_ => this._dialog.closeAll());

                    return dialogRef.afterClosed();
                }));
    }
}
