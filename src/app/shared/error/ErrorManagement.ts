import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpClient} from '@angular/common/http';

interface IErrorNotification {
    backHelpText: string,
    errorId: string,
    frontHelpText: string,
    idImages?: any[],
    message?: string
}

@Injectable({
    providedIn: 'any'
})
class ErrorService {
    private BASE: string = environment.baseUrl;
    private _URL = 'error/notification/notify';

    constructor(private _http: HttpClient) {
    }

    notify(err: IErrorNotification) {
        return this._http.post(`${this.BASE}${this._URL}`, err);
    }
}

@Injectable({
    providedIn: 'any'
})
export class ErrorFacade {
    constructor(private _service: ErrorService) {
    }

    notifyBackError(errorMessage, errorId) {
        return this._send('', errorMessage, errorId);
    }

    notifyFrontError(errorMessage) {
        return this._send(errorMessage, '', '');
    }

    private _send(frontMessage, backMessage, errorId) {
        return this._service.notify({
            errorId: errorId,
            frontHelpText: frontMessage,
            backHelpText: backMessage
        });
    }
}