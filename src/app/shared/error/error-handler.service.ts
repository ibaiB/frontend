import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandler, Injectable, NgZone} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {ErrorResponseComponent} from '../presentation/error-response/error-response.component';

@Injectable({
  providedIn: 'root',
})
export class ErrorHandlerService implements ErrorHandler {
  constructor(
      private _dialog: MatDialog,
      private _zone: NgZone,
      private _router: Router
  ) {
  }

  handleError(error: Error) {
    if (!(error instanceof HttpErrorResponse)) {
      this._zone.run(() => {
        const dialogRef = this._dialog.open(ErrorResponseComponent, {
          data: { 
            error: error,
            code: '???',
            motive: 'Error',
            explanation: 'Looks like this functionality is broken. We\'ll try to fix it as soon as possible.',
            timeout: null,
            reportable: true
          },
          disableClose: true
        });
        
        dialogRef.afterClosed().subscribe(() => {
          this._dialog.closeAll();
          this._router.navigate(['']);
        });
      });
    }

    console.error(error);
  }
}
