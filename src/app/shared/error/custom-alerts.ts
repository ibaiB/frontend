import Swal from 'sweetalert2';
import {ICaption} from '../entities/caption/domain/ICaption';

export const errorAlert = (info, errorMessage, errorId) => {
    return Swal.fire({
        icon: 'error',
        title: 'Something wrong happened',
        html: `
            ${info}</br>
            Please report the id </br>
            ${errorId.bold()} </br></br></br>
            ${'Reason'.bold()} was the following: </br>
            ${errorMessage}
        `,
        confirmButtonColor: '#db5e5e',
        showCancelButton: true,
        cancelButtonText: 'Report',
        cancelButtonColor: '#ffcb8e'
    });
};

export const successAlert = (info) => {
    return Swal.fire({
        icon: 'success',
        title: 'Success',
        text: info,
        timer: 1500,
        confirmButtonColor: '#db5e5e',
    });
};

export const questionAlert = (info) => {
    return Swal.fire({
        icon: 'warning',
        title: 'Are you sure?',
        html: info,
        showCancelButton: true,
        cancelButtonColor: '#767676',
        confirmButtonColor: '#db5e5e',
    });
};

export const captionAlert = (referenced: string, captions: ICaption[]) => {
    let printed: string = '';
    let valueStyle = `"
        display: inline;
        color: #b9aeff;
    "`;
    let descriptionStyle = `"
        display: inline;
    "`;
    let divStyle = `"
        text-align: left;
        margin: 10px 0px;
    "`;
    captions.forEach(caption => {
        printed += `<div style=${divStyle}><p style=${valueStyle}>${caption.value}: </p>  <p style=${descriptionStyle}>${caption.description}</p></div>`;
    });

    return Swal.fire({
        icon: 'info',
        title: `${capitalize(referenced)} captions`,
        html: printed,
        confirmButtonColor: '#db5e5e',
    });
};
const capitalize = (text: string) => {
    return (text) ? text[0].toUpperCase() + text.slice(1).toLowerCase() : text.toString();
};

