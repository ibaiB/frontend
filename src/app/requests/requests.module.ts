import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {routing} from '../bonuses/bonuses-routing.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    routing
  ]
})
export class RequestsModule { }
