import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AdminPanelsComponent} from './presentation/admin-panels/admin-panels.component';
import {UserTableComponent} from '../auth/user/infrastructure/ng-components/user-table/user-table.component';
import {UserDetailComponent} from '../auth/user/infrastructure/ng-components/user-detail/user-detail.component';
import {RolesTableComponent} from '../auth/role/infrastructure/ng-components/roles-table/roles-table.component';
import {RoleDetailComponent} from '../auth/role/infrastructure/ng-components/role-detail/role-detail.component';
import {UserTableResolverService} from '../auth/user/infrastructure/resolvers/user-table-resolver.service';
import {UserDetailResolverService} from '../auth/user/infrastructure/resolvers/user-detail-resolver.service';
import {RoleTableResolverService} from '../auth/role/infrastructure/resolvers/role-table-resolver.service';
import {RoleDetailResolverService} from '../auth/role/infrastructure/resolvers/role-detail-resolver.service';

const routes: Routes = [
    {
        path: 'admin-panels',
        component: AdminPanelsComponent,
        data: {breadcrumb: 'Admin Panels'}
    },
    {
        path: 'users',
        data: {breadcrumb: 'Users'},
        children: [
            {
                path: '',
                component: UserTableComponent,
                data: {breadcrumb: ''},
                resolve: {response: UserTableResolverService}
            },
            {
                path: 'user-detail/:id',
                component: UserDetailComponent,
                data: {breadcrumb: 'detail/:id'},
                resolve: {response: UserDetailResolverService}
            },
            {
                path: 'new',
                component: UserDetailComponent,
                data: {breadcrumb: 'new'},
                resolve: {response: UserDetailResolverService}
            }
        ]
    },
    {
        path: 'roles',
        data: {breadcrumb: 'Roles'},
        children: [
            {
                path: '',
                component: RolesTableComponent,
                data: {breadcrumb: ''},
                resolve: {response: RoleTableResolverService}
            },
            {
                path: 'role-detail/:id',
                component: RoleDetailComponent,
                data: {breadcrumb: 'detail/:id'},
                resolve: {response: RoleDetailResolverService}
            },
            {
                path: 'new',
                component: RoleDetailComponent,
                data: {breadcrumb: 'new'},
                resolve: {response: RoleDetailResolverService}
            }
        ]
    },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes)
