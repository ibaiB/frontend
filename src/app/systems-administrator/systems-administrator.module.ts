import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {routing} from './systems-administrator-routing.module';
import {AngularMaterialModule} from '../shared/material/angular-material.module';
import {HttpClientModule} from '@angular/common/http';
import {AdminPanelsComponent} from './presentation/admin-panels/admin-panels.component';
import {SharedModule} from '../shared/shared.module';
import {UserTableComponent} from '../auth/user/infrastructure/ng-components/user-table/user-table.component';
import {UserDetailComponent} from '../auth/user/infrastructure/ng-components/user-detail/user-detail.component';
import {RolesTableComponent} from '../auth/role/infrastructure/ng-components/roles-table/roles-table.component';
import {RoleDetailComponent} from '../auth/role/infrastructure/ng-components/role-detail/role-detail.component';
import {ScopeManagementComponent} from '../auth/scope/infrastructure/ng-components/scope-management/scope-management.component';
import {UserRegisterMapper} from '../auth/user/infrastructure/userRegister-mapper';

@NgModule({
    declarations: [
        AdminPanelsComponent,
        UserTableComponent,
        UserDetailComponent,
        RolesTableComponent,
        RoleDetailComponent,
        ScopeManagementComponent
    ],
    imports: [
        CommonModule,
        routing,
        AngularMaterialModule,
        HttpClientModule,
        SharedModule,
    ],
    providers:[
        UserRegisterMapper
    ]
})
export class SystemsAdministratorModule {
}
