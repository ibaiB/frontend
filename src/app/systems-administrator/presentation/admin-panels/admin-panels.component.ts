import {Component, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment.prod';


@Component({
  selector: 'app-admin-panels',
  templateUrl: './admin-panels.component.html',
  styleUrls: ['./admin-panels.component.scss']
})
export class AdminPanelsComponent implements OnInit {

  eureka: string = environment.eureka;
  wallboard: string = environment.wallboard;
  hytrix: string = environment.hytrix;
  zipkin: string = environment.zipkin;
  rabbitMq: string = environment.rabbitMq;
  kibana: string = environment.kibana;

  constructor() {
  }

  ngOnInit(): void {

  }

}
