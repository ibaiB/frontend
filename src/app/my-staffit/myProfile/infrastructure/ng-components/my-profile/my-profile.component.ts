import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EmployeeService} from '../../../../../internal/employee/infrastructure/services/employee.service';
import {IMyProfile} from '../../../domain/IMyProfile';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnChanges {

  @Input() myProfile: any;
  @Input() index: number;

  form: FormGroup;
  categories: IMyProfile;
  loading: boolean = true;

  constructor(
      private _fb: FormBuilder,
      private _employeeService: EmployeeService,
  ) {
  }

  formInit() {
    const controlsConfig = {};
    this.categories.categories.forEach(category =>{
      category.values.forEach(subcategory =>{
        controlsConfig[subcategory.id] = subcategory.value ? subcategory.value : subcategory.options[0];
      });
    });
    this.form = this._fb.group(controlsConfig);
    this.loading = false;
  }

  submit(apply ?: boolean) {
    let newProfile = {};
    const controls = this.form.controls;

    for (const name in controls) {
      if (this.form.get(name).value) {
        newProfile[name] = this.form.get(name).value.toString();
      }
    }

    this._employeeService.updateMyProfile({values: newProfile}, this.index).subscribe(ed => {
      if (apply) {
        this.categories = ed;
        Swal.fire({
          icon: 'success',
          title: 'Success',
          confirmButtonColor: '#db5e5e'
        });
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.categories = this.myProfile;
    this.formInit();
  }

}
