import {IMyProfile} from '../domain/IMyProfile';

export class MyProfileMapper {
    mapTo(params: any): IMyProfile {
        const {id, categories} = params;

        return {
            id: id,
            categories: categories
        };
    }
};
