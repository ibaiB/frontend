export interface IMyProfile {
    id: string;
    categories: IMyProfileCategory[];
}

interface IMyProfileCategory {
    name: string;
    values: IMyProfileValue[];
}

export interface IMyProfileValue {
    id: string,
    name: string,
    type: string,
    options: string[],
    value: string
}
