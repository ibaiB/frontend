import {TestBed} from '@angular/core/testing';

import {WhoIsWhoService} from './who-is-who.service';

describe('WhoIsWhoService', () => {
  let service: WhoIsWhoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WhoIsWhoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
