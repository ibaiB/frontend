import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {IEmployeeWhoIsWho} from 'src/app/internal/employee/domain/IEmployee';
import {IRanking} from '../../../domain/IRanking';
import {WhoIsWhoService} from '../../who-is-who.service';
import {getRandomEmployees} from '../../../application/getRamdomEmployees';
import {answerMe} from '../../../application/answerMe';
import {searchRankings} from '../../../application/searchRankings';

@Component({
    selector: 'app-who-is-who',
    templateUrl: './who-is-who.component.html',
    styleUrls: ['./who-is-who.component.scss'],
})
export class WhoIsWhoComponent implements OnInit {

    queryPagination = {page: 0, size: 3};
    score = 0;
    maxScore: number = 0;
    loading: boolean = true;
    lives: number = 3;

    ranking: IRanking[] = [];
    gameOver: boolean = false;

    employee1: IEmployeeWhoIsWho;
    employee2: IEmployeeWhoIsWho;
    employee3: IEmployeeWhoIsWho;

    isCorrect1: boolean;
    isCorrect2: boolean;
    isCorrect3: boolean;

    hasAnswered1: boolean;
    hasAnswered2: boolean;
    hasAnswered3: boolean;

    token: string;

    currentErrors: number = 0;

    askCounter: number = 3;

    constructor(
        private _service: WhoIsWhoService,
        private _router: Router
    ) {
    }

    ngOnInit(): void {
        this.get();
    }

    get() {
        getRandomEmployees(this.queryPagination, this._service).subscribe(
            (employees) => {
                this.employee1 = employees[0];
                this.employee2 = employees[1];
                this.employee3 = employees[2];

                this.isCorrect1 = false;
                this.isCorrect2 = false;
                this.isCorrect3 = false;

                this.hasAnswered1 = false;
                this.hasAnswered2 = false;
                this.hasAnswered3 = false;

                this.askCounter = 3;

                this.loading = false;
            }
        );
    }

    tryLuck(event: any) {
        const {idEmployee, value, index} = event;

        answerMe(idEmployee, value, {
            index: 0,
            style: 'full',
            token: this.token
        }, this._service)
            .subscribe(res => {
                this.token = res.token;
                this.score = res.correctAnswers;

                this[`isCorrect${index}`] = this.currentErrors === res.currentErrors;
                this[`hasAnswered${index}`] = true;
                this[`employee${index}`] = {employee: res.correctAnswer, randomNames: []};

                this.askCounter--;
                this.currentErrors = res.currentErrors;
                if (res.currentErrors > 0) {
                    this.removeLive();
                }
                if (this.askCounter === 0) {
                    this.reAsk();
                }
            });
    }

    restart() {
        this.loading = true;
        this.score = 0;
        this.lives = 3;
        this.gameOver = false;
        this.currentErrors = 0;
        this.token = '';
        this.get();
    }

    reAsk() {
        setTimeout(() => {
            this.get();
        }, 1000);
    }

    removeLive() {
        this.lives = 3 - this.currentErrors;
        if (this.lives === 0) {
            setTimeout(() => {
                this.showGameOver();
            }, 1000);
        }
    }

    showGameOver() {
        this.gameOver = true;
        searchRankings({
            page: 0,
            size: 10,
            order: 'desc',
            orderField: 'punctuation'
        }, this._service)
            .subscribe(res => {
                this.ranking = res.content;
            });
    }
}
