import {Injectable} from '@angular/core';
import {WhoIsWhoAbstractService} from './who-is-who-abstract-service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {IEmployeeWhoIsWho} from '../../../internal/employee/domain/IEmployee';
import {IList} from '../../../shared/generics/IList';
import {IRanking} from '../domain/IRanking';
import {IWhoIsWhoAnswer} from '../domain/IWhoIsWhoAnswer';

@Injectable({
  providedIn: 'root'
})
export class WhoIsWhoService extends WhoIsWhoAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'whoIsWho';

  constructor(private http: HttpClient) {
    super();
  }

  answer(idEmployeeToGuess: string, idEmployeePlaying: string, answer: string): Observable<IWhoIsWhoAnswer> {
    throw Error('METHOD_NOT_IMPLEMENTED');
  }

  answerMe(idEmployeeToGuess: string, answer: string, query: any): Observable<IWhoIsWhoAnswer> {
    return this.http.post<IWhoIsWhoAnswer>(`${this.BASE_URL}${this.ENDPOINT}/games/${idEmployeeToGuess}/${answer}`, answer, {params: new HttpParams({fromObject: query})});
  }

  createRanking(ranking: any) {
    throw Error('METHOD_NOT_IMPLEMENTED');
  }

  getRandomEmployees(params: any): Observable<IEmployeeWhoIsWho[]> {
    return this.http
        .get<IEmployeeWhoIsWho[]>(`${this.BASE_URL}/whoIsWho/employees`, {params: new HttpParams({fromObject: params})});
  }

  search(query: any) {
    throw Error('METHOD_NOT_IMPLEMENTED');
  }

  searchRanking(query: any): Observable<IList<IRanking>> {
    return this.http.get<IList<IRanking>>(`${this.BASE_URL}${this.ENDPOINT}/rankings/search`, {params: new HttpParams({fromObject: query})});
  }

  updateRanking(id: string, punctuation: number) {
    throw Error('METHOD_NOT_IMPLEMENTED');
  }
}
