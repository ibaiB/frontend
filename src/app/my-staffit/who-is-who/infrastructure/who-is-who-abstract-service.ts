import {Observable} from 'rxjs';
import {IEmployeeWhoIsWho} from '../../../internal/employee/domain/IEmployee';
import {IWhoIsWhoAnswer} from '../domain/IWhoIsWhoAnswer';
import {IList} from '../../../shared/generics/IList';
import {IRanking} from '../domain/IRanking';

export abstract class WhoIsWhoAbstractService {
    abstract answerMe(idEmployeeToGuess: string, answer: string, query: any): Observable<IWhoIsWhoAnswer>;

    abstract answer(idEmployeeToGuess: string, idEmployeePlaying: string, answer: string): Observable<IWhoIsWhoAnswer>;

    abstract search(query: any);

    abstract createRanking(ranking: any);

    abstract updateRanking(id: string, punctuation: number);

    abstract searchRanking(query: any): Observable<IList<IRanking>>;

    abstract getRandomEmployees(query: any): Observable<IEmployeeWhoIsWho[]>;
}

