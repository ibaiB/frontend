import {WhoIsWhoAbstractService} from '../infrastructure/who-is-who-abstract-service';
import {EmployeeMapper} from '../../../internal/employee/infrastructure/EmployeeMapper';
import {map} from 'rxjs/operators';

export function getRandomEmployees(query: any, service: WhoIsWhoAbstractService) {
    const mapper = new EmployeeMapper();
    return service.getRandomEmployees(query).pipe(map(mapper.mapRandomEmployeeList));
}
