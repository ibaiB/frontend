import {WhoIsWhoAbstractService} from '../infrastructure/who-is-who-abstract-service';

export function searchRankings(query: any, service: WhoIsWhoAbstractService) {
    return service.searchRanking(query);
}
