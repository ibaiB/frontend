import {WhoIsWhoAbstractService} from '../infrastructure/who-is-who-abstract-service';

export function answerMe(idEmployeeToGuess: string, answer: string, query: any, service: WhoIsWhoAbstractService) {
    return service.answerMe(idEmployeeToGuess, answer, query);
}
