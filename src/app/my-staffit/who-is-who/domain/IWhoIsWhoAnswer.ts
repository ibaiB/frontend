import {IPublicEmployee} from '../../../internal/employee/domain/IEmployee';

export interface IWhoIsWhoAnswer {
    correctAnswer: IPublicEmployee,
    correctAnswers: number,
    currentErrors: number,
    id: string,
    maxErrors: number,
    token: string
}
