import {IPublicEmployee} from '../../../internal/employee/domain/IEmployee';

export interface IRanking {
    id: string,
    punctuation: number,
    date: string,
    employee: IPublicEmployee
}
