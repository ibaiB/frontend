import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-public-documents',
  templateUrl: './public-documents.component.html',
  styleUrls: ['./public-documents.component.scss']
})
export class PublicDocumentsComponent implements OnInit {

  constructor(private _router: Router) {
  }

  ngOnInit(): void {
    this._router.navigate(['app/coming-soon']);
  }

}
