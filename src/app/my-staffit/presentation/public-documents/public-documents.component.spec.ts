import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PublicDocumentsComponent} from './public-documents.component';

describe('PublicDocumentsComponent', () => {
  let component: PublicDocumentsComponent;
  let fixture: ComponentFixture<PublicDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PublicDocumentsComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
