import {SharedModule} from '../shared/shared.module';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {AccordionModule} from 'primeng/accordion';
import {ChartModule} from 'primeng/chart';
import {CarMapper} from '../internal/car/infrastructure/CarMapper';
import {CenterMapper} from '../shared/entities/center/infrastructure/CenterMapper';
import {CompanyMapper} from '../internal/company/infrastructure/CompanyMapper';
import {EmployeeMapper} from '../internal/employee/infrastructure/EmployeeMapper';
import {AngularMaterialModule} from '../shared/material/angular-material.module';
import {MyAlertsComponent} from './presentation/my-alerts/my-alerts.component';
import {MyDashboardComponent} from './presentation/my-dashboard/my-dashboard.component';
import {MyEmployeeComponent} from '../internal/employee/infrastructure/ng-components/my-employee/my-employee.component';
import {MyProfileComponent} from './myProfile/infrastructure/ng-components/my-profile/my-profile.component';
import {WhoIsWhoComponent} from './who-is-who/infrastructure/ng-components/who-is-who/who-is-who.component';
import {EmployeesComponent} from '../internal/employee/infrastructure/ng-components/employees/employees.component';
import {RoomsComponent} from './presentation/rooms/rooms.component';
import {PublicDocumentsComponent} from './presentation/public-documents/public-documents.component';
import {SubcarMapper} from '../internal/subcar/infrastructure/SubcarMapper';


@NgModule({
    declarations: [
        MyEmployeeComponent,
        MyProfileComponent,
        MyAlertsComponent,
        MyDashboardComponent,
        WhoIsWhoComponent,
        EmployeesComponent,
        RoomsComponent,
        PublicDocumentsComponent,
    ],
    imports: [
        CommonModule,
        AngularMaterialModule,
        HttpClientModule,
        AccordionModule,
        ChartModule,
        SharedModule
    ],
    providers: [
        EmployeeMapper,
        CompanyMapper,
        CarMapper,
        SubcarMapper,
        CenterMapper,
    ]
})
export class MyStaffitModule {
}
