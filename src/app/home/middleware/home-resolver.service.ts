import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';
import {EmployeeService} from '../../internal/employee/infrastructure/services/employee.service';
import {map} from 'rxjs/operators';
import {getMyEmployee} from '../../internal/employee/application/getMyEmployee';
import {IEmployeeReadFull} from '../../internal/employee/domain/IEmployee';
import { getUserInfo } from 'src/app/auth/session/application/getUserInfo';
import { SessionService } from 'src/app/auth/session/infrastructure/services/session.service';
import { CookieFacadeService } from 'src/app/auth/session/infrastructure/services/cookie.service';

@Injectable({
    providedIn: 'root'
})
export class HomeResolverService implements Resolve<any> {

    constructor(
        private _sessionService: SessionService,
        private _employeeService: EmployeeService,
        private _cookieFacade: CookieFacadeService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        let profileIndex = this._cookieFacade.get('profileIndex');

        if (!profileIndex) {
            profileIndex = '0';
            this._cookieFacade.save('profileIndex', profileIndex);
        }

        const $userInfo = getUserInfo(this._sessionService);
        const $myEmployee = getMyEmployee(this._employeeService, {style: 'full', index: profileIndex});

        return forkJoin($userInfo, $myEmployee)
            .pipe(map(res => {
                const employee: IEmployeeReadFull = res[1] as IEmployeeReadFull;

                return {
                    email: res[0].email,
                    roles: res[0].roles,
                    scopes: res[0].scopes,
                    name: employee.name,
                    surname: employee.surname,
                    category: employee.category.name,
                    imageLink: employee.imageLink
                };
            }));
    }

}
