export interface Module {
  name: string;
  path?: string;
  children?: Submodule[];
}

export interface Submodule extends Module {
  icon: string;
  scopeCreate: string[];
  scopeRead: string[];
}

export function hasAccessToModule(moduleName: string, userScopes: string[]): boolean {
    const module = config.find(m => m.name === moduleName);
    return module?.children.some(submodule => hasAccessToSubmoduleByName(submodule.name, userScopes)) ?? false;
}

function hasAccessToSubmodule(submodule: Submodule, userScopes: string[]) {
    
    // TODO: Revisar scopes en el árbol de configuración y corregir
    if (['Users', 'Roles'].includes(submodule.name)) return true;
    //

    if (submodule?.children) {
        return submodule.children.some(children => hasAccessToSubmodule(children, userScopes));
    }
    return submodule.scopeRead.every(scope => userScopes.includes(scope));
}

export function hasAccessToSubmoduleByName(submoduleName: string, userScopes: string[]): boolean {
    const submodule = submodules.find(s => s.name === submoduleName);
    return submodule ? hasAccessToSubmodule(submodule, userScopes) : true;
}

export function hasAccessToSubmoduleByPath(submodulePath: string, userScopes: string[]): boolean {
    const submodule = submodules.find(s => s.path === submodulePath);
    return submodule ? hasAccessToSubmodule(submodule, userScopes) : true;
}

export const config: Module[] = [
  {
    name: "Staffit",
    path: "my-staffit",
    children: [
      {
        name: "All employees",
        path: "employees",
        icon: "face",
        scopeCreate: [],
        scopeRead: [],
      },
      {
        name: "Who is who?",
        path: "who-is-who",
        icon: "quiz",
        scopeCreate: [],
        scopeRead: [],
      },
      {
        name: "Rooms",
        path: "rooms",
        icon: "weekend",
        scopeCreate: [],
        scopeRead: [],
      },
      {
        name: "Know the company",
        path: "know-the-company",
        icon: "help",
        scopeCreate: [],
        scopeRead: [],
      }
    ],
  },
  {
    name: "Systems Administrator",
    path: "systems-administrator",
    children: [
      {
        name: "Admin panels",
        path: "admin-panels",
        icon: "analytics",
        scopeCreate: [],
        scopeRead: [],
      },
      {
        name: "Users",
        path: "users",
        icon: "group",
        scopeCreate: [
          "USUARIO_CREATE",
          "USUARIO_UPDATE",
          "USUARIO_DELETE",
          "Update scope users",
          "USUARIO_READ",
          "Read scope users",
        ],
        scopeRead: ["USUARIO_READ", "Read scope users"],
      },
      {
        name: "Roles",
        path: "roles",
        icon: "admin_panel_settings",
        scopeCreate: [
          "Create scope admin",
          "Update scope admin",
          "Delete scope admin",
          "Read scope admin",
        ],
        scopeRead: ["Read scope admin"],
      },
    ],
  },
  {
    name: "Internal",
    path: "internal",
    children: [
      {
        name: "Employee management",
        icon: "supervisor_account",
        path: "employee",
        scopeCreate: [],
        scopeRead: [],
        children: [
          {
            name: "Reports",
            path: "reports",
            icon: "groups",
            scopeCreate: [
              "EMPLOYEE_CREATE",
              "EMPLOYEE_UPDATE",
              "EMPLOYEE_DELETE",
              "EMPLOYEE_READ",
              "EMPLOYEEAUX_READ",
              "COMPANY_READ",
              "CATEGORY_READ",
              "CAR_READ",
              "SUBCAR_READ",
            ],
            scopeRead: [
              "EMPLOYEE_READ",
              "EMPLOYEEAUX_READ",
              "COMPANY_READ",
              "CATEGORY_READ",
              "CAR_READ",
              "SUBCAR_READ",
            ],
          },
          {
            name: "Requests",
            path: "new-requests",
            icon: "person_adds",
            scopeCreate: [
              "NEWEMPLOYEE_CREATE",
              "NEWEMPLOYEE_UPDATE",
              "NEWEMPLOYEE_DELETE",
              "NEWEMPLOYEE_READ",
              "CAR_READ",
              "SUBCAR_READ",
              "CENTER_READ",
            ],
            scopeRead: [
              "NEWEMPLOYEE_READ",
              "CAR_READ",
              "SUBCAR_READ",
              "CENTER_READ"
            ],
          },
        ],
      },
      {
        name: "Billing",
        path: "billing",
        icon: "euro",
        scopeCreate: [],
        scopeRead: [],
      },
      {
        name: "Room management",
        path: "room-management",
        icon: "weekend",
        scopeCreate: [],
        scopeRead: [],
      },
      {
        name: "Admin know the company",
        path: "admin-know-the-company",
        icon: "help",
        scopeCreate: [],
        scopeRead: [],
      },
    ],
  },
  {
    name: "Recruitment",
    path: "recruitment",
    children: [
      {
        name: "Campaigns",
        path: "campaigns",
        icon: "campaign",
        scopeCreate: [
          "CAMPAIGN_CREATE",
          "CAMPAIGN_UPDATE",
          "CAMPAIGN_DELETE",
          "CAMPAIGN_READ",
          "COLLEGE_READ",
          "STUDY_READ",
          "CANDIDATE_READ",
        ],
        scopeRead: [
          "CAMPAIGN_READ",
          "COLLEGE_READ",
          "STUDY_READ",
          "CANDIDATE_READ",
        ],
      },
      {
        name: "Candidates",
        path: "candidates",
        icon: "person_search",
        scopeCreate: [
          "CANDIDATE_CREATE",
          "CANDIDATE_UPDATE",
          "CANDIDATE_DELETE",
          "CANDIDATE_READ",
          "COLLEGE_READ",
          "EMPLOYEE_READ",
          "TECHNOLOGY_READ",
          "STUDY_READ",
        ],
        scopeRead: [
          "CANDIDATE_READ",
          "STUDY_READ",
          "COLLEGE_READ",
          "TECHNOLOGY_READ",
          "EMPLOYEE_READ",
        ],
      },
      {
        name: "Colleges",
        path: "colleges",
        icon: "local_library",
        scopeCreate: [
          "COLLEGE_CREATE",
          "COLLEGE_UPDATE",
          "COLLEGE_DELETE",
          "STUDY_READ",
          "COLLEGE_READ",
        ],
        scopeRead: ["COLLEGE_READ", "STUDY_READ"],
      },
      {
        name: "Studies",
        path: "studies",
        icon: "school",
        scopeCreate: [
          "STUDY_CREATE",
          "STUDY_UPDATE",
          "STUDY_DELETE",
          "STUDY_READ",
        ],
        scopeRead: ["STUDY_READ"],
      },
    ],
  },
  {
    name: "Evaluations",
    path: "evaluations",
    children: [
      {
        name: "Tests",
        path: "tests",
        icon: "note_add",
        scopeCreate: [
          "TEST_CREATE",
          "TEST_UPDATE",
          "TEST_DELETE",
          "TEST_READ",
          "CORRECTION_READ",
        ],
        scopeRead: ["TEST_READ", "CORRECTION_READ"],
      },
      {
        name: "Exams",
        path: "exams",
        icon: "assignment",
        scopeCreate: [
          "EXAM_CREATE",
          "EXAM_UPDATE",
          "EXAM_DELETE",
          "EXAMSENT_CREATE",
          "EXAMSENT_UPDATE",
          "EXAMSENT_DELETE",
          "TEST_READ",
          "CORRECTION_READ",
          "QUESTION_CREATE",
          "QUESTION_DELETE",
          "EXAM_READ",
          "EXAMSENT_READ",
        ],
        scopeRead: [
          "EXAM_READ",
          "CORRECTION_READ",
          "EXAMSENT_READ",
          "TEST_READ",
        ],
      },
    ],
  },
  {
    name: "Crm",
    path: "crm",
    children: [
      {
        name: "Accounts",
        path: "accounts",
        icon: "business",
        scopeCreate: [
          "CUENTA_CREATE",
          "CUENTA_UPDATE",
          "CUENTA_DELETE",
          "CUENTA_READ",
          "CAR_READ",
          "SUBCAR_READ",
          "TERRITORIO_READ",
          "USUARIO_READ",
        ],
        scopeRead: [
          "CUENTA_READ",
          "CAR_READ",
          "SUBCAR_READ",
          "TERRITORIO_READ",
          "USUARIO_READ"
        ],
      },
      {
        name: "Contacts",
        path: "contacts",
        icon: "contacts",
        scopeCreate: [
          "CONTACTO_CREATE",
          "CONTACTO_UPDATE",
          "CONTACTO_DELETE",
          "CONTACTO_READ",
          "CAR_READ",
          "SUBCAR_READ",
          "USUARIO_READ",
        ],
        scopeRead: ["CONTACTO_READ", "CAR_READ", "SUBCAR_READ", "USUARIO_READ"],
      },
      {
        name: "Activities",
        path: "activities",
        icon: "event",
        scopeCreate: [
          "ACTIVIDAD_CREATE",
          "ACTIVIDAD_UPDATE",
          "ACTIVIDAD_DELETE",
          "ACTIVIDAD_READ",
          "USUARIO_READ",
          "CAR_READ",
          "SUBCAR_READ",
        ],
        scopeRead: [
          "ACTIVIDAD_READ",
          "USUARIO_READ",
          "CAR_READ",
          "SUBCAR_READ",
        ],
      },
      {
        name: "Opportunities",
        path: "opportunities",
        icon: "euro",
        scopeCreate: [
          "OPORTUNIDAD_CREATE",
          "OPORTUNIDAD_UPDATE",
          "OPORTUNIDAD_DELETE",
          "TRACKING_OPORTUNIDAD_CREATE",
          "TRACKING_OPORTUNIDAD_UPDATE",
          "OPORTUNIDAD_READ",
          "TRACKING_OPORTUNIDAD_DELETE",
          "TRACKING_OPORTUNIDAD_READ",
          "USUARIO_READ",
          "CUENTA_READ",
          "CONTACTO_READ",
          "ESTADO_READ",
          "CAR_READ",
          "SUBCAR_READ",
        ],
        scopeRead: [
          "OPORTUNIDAD_READ",
          "TRACKING_OPORTUNIDAD_READ",
          "USUARIO_READ",
          "CUENTA_READ",
          "CONTACTO_READ",
          "ESTADO_READ",
          "CAR_READ",
          "SUBCAR_READ",
        ],
      },
    ],
  },
];

export const submodules: Submodule[] = parseConfig();

function parseConfig(): Submodule[] {
    const result = [];
    config.forEach(module => {
      module.children.forEach(submodule => {
        if (submodule.children) {
            submodule.children.forEach(submoduleChild => {
                // Add scopes of child to its parent
                submodule.scopeRead.push(...submoduleChild.scopeRead);
                result.push(submoduleChild);
            })
            // Remove duplicates
            submodule.scopeRead = [...new Set(submodule.scopeRead)];
        }
        result.push(submodule);
      });
    });
    return result;
}
