import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, NavigationStart, Router, RouterEvent} from '@angular/router';
import Swal from 'sweetalert2';
import {CookieFacadeService} from '../auth/session/infrastructure/services/cookie.service';
import {config as navbarConfig, hasAccessToModule, hasAccessToSubmoduleByName} from './middleware/navbar-config';
import {authConfig} from '../auth/config/auth.config';
import {BreakpointObserver, BreakpointState} from '@angular/cdk/layout';
import {EmployeeService} from '../internal/employee/infrastructure/services/employee.service';
import {ErrorFacade} from '../shared/error/ErrorManagement';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    // ===== APP =====
    opened: boolean = false;
    mode: string = 'side';
    modules = navbarConfig;
    version: string = '1.0.0';
    defaultLanguage: string = 'EN';
    loading: boolean;
    isCRM: boolean;
    mobile: boolean = false;

    // ===== USER =====
    category: string;
    username: string;
    email: string;
    roles: string[];
    scopes: string[] = [];
    imageLink: string;

    selected: string;
    options: string[];
    index: number = 0;

    private SESSION_STORAGE_KEY = authConfig.SESSION_STORAGE_KEY;

    public constructor(
        private _cookieFacade: CookieFacadeService,
        private _router: Router,
        private _route: ActivatedRoute,
        private employeeService: EmployeeService,
        private _error: ErrorFacade,
        private breakpointObserver: BreakpointObserver
    ) {
    }

    ngOnInit() {
        this.breakpointObserver
            .observe(['(min-width:1024px)'])
            .subscribe((state: BreakpointState) => {
                    if (state.matches) {
                        this.opened = true;
                        this.mode = 'side';
                        this.mobile = false;
                        return;
                    }

                    this.opened = false;
                    this.mode = 'over';
                    this.mobile = true;
                }
            );

        this.routerEvents();
        const {
            email, roles, scopes, name, surname, category, imageLink
        } = this._route.snapshot.data['response'];

        this.email = email;
        this.roles = roles;
        this.scopes = scopes;
        this.username = `${name} ${surname}`;
        this.category = category;
        this.imageLink = imageLink;
    }

    routerEvents() {
        this._router.events.subscribe((event: RouterEvent) => {
            switch (true) {
                case event instanceof NavigationStart: {
                    this.isCRM = this._router.url.includes('crm');
                    this.loading = true;
                    break;
                }
                case event instanceof NavigationEnd: {
                    this.loading = false;
                    break;
                }
            }
        });
    }

    hasAccessToModule(moduleName: string): boolean {
        return hasAccessToModule(moduleName, this.scopes);
    }

    hasAccessToSubmodule(submoduleName: string): boolean {
        return hasAccessToSubmoduleByName(submoduleName, this.scopes);
    }

    showLogoutDialog() {
        Swal.fire({
            title: 'Are you sure?',
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080',
            confirmButtonText: 'Yes, logout',
        }).then((result) => {
            if (result.isConfirmed) {
                this.logout();
            }
        });
    }

    logout() {
        this._cookieFacade.delete(this.SESSION_STORAGE_KEY);
        this._cookieFacade.delete('userInfo');
        this._router
            .navigate(['./login'])
            .then(() => {
                Swal.fire({
                    icon: 'success',
                    title: 'Bye bye',
                    text: 'You have successfully logout',
                    confirmButtonColor: '#db5e5e'
                });
            });
    }

}
