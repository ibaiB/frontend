export const global = {
    cars: [
        {name: 'Finance', subCars: ['Banca', 'Seguros', 'R. State']},
        {name: 'Corporates', subCars: ['N. Digitales', 'Industria', 'Adm. Pública']},
        {name: 'Elliot', subCars: ['Elliot']},
        {name: 'OnVenture', subCars: ['OnVenture']},
        {name: 'N360', subCars: ['N360']},
    ],
    getSubCars(cars: { name: string, subCars: string[] }[]): string[] {
        let subCars: string[] = [];
        cars.forEach(car => car.subCars.forEach(subCar => subCars.push(subCar)));
        return subCars;
    },
    territories: [
        'Rioja',
        'Navarra',
        'Aragón',
        'CastillaYLeon',
        'Madrid',
        'Cataluña',
        'Andalucia',
        'Global'
    ],
    allMineUsers: [
        'Mine'
    ],
    priorities: [
        'Muy Alta',
        'Alta',
        'Normal',
        'Baja'
    ],
    account: {
        categories: [
            'AAA',
            'AA',
            'A',
            'B',
            'C'
        ],
        sectors: []
    },
    opportunity: {
        probabilities: [
            '10%',
            '40%',
            '70%',
            '95%'
        ],
        rejectReason: [
            'Cancelada',
            'Precio',
            'Propuesta técnica',
            'Otros',
        ]
    },
    activity: {
        types: [
            'Prospección',
            'Contacto',
            'Reunión',
        ],
        states: [
            'Pendiente',
            'En curso',
            'Finalizada'
        ]
    },
    categories: [
        {value: 'AAA', description: 'TOP 100 facturación en España'},
        {value: 'AA', description: 'Cuentas estratégicas para alguna empresa del grupo'},
        {value: 'A', description: 'PYMEs'},
        {value: 'B', description: 'Partners'},
        {value: 'C', description: 'Otros'},
    ],
    categoriesShort: [
        {value: 'AAA', description: 'TOP 100'},
        {value: 'AA', description: 'Estratégicas'},
        {value: 'A', description: 'PYMEs'},
        {value: 'B', description: 'Partners'},
        {value: 'C', description: 'Otros'},
    ],
    communities: [
        {value: 'Andalucía'},
        {value: 'Cataluña'},
        {value: 'Comunidad de Madrid'},
        {value: 'Comunidad valenciana'},
        {value: 'Galicia'},
        {value: 'Castilla y León'},
        {value: 'País Vasco'},
        {value: 'Canarias'},
        {value: 'Castilla La Mancha'},
        {value: 'Región de Murcia'},
        {value: 'Aragón'},
        {value: 'Islas Baleares'},
        {value: 'Extremadura'},
        {value: 'Principado de Asturias'},
        {value: 'Navarra'},
        {value: 'Cantabria'},
        {value: 'La Rioja'},
        {value: 'Melilla'},
        {value: 'Ceuta'},
    ],
    countries: [
        {value: 'España'},
        {value: 'Francia'},
        {value: 'Rusia'},
        {value: 'Alemania'},
        {value: 'EEUU'},
        {value: 'Brasil'},
        {value: 'Colombia'},
        {value: 'China'},
    ]
};
