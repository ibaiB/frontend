import {IOpportunity} from '../domain/IOpportunity';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {getDictionaryById} from '../../../../shared/entities/dictionary/application/getDictionaryById';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export function joinOpportunity(opportunity: IOpportunity, dictionaries: IDictionaryType[], users: ICRMUser[], cars: IList<ICar>, subcars: IList<ISubcar>) {

    const parsedDictionaries = {
        priorities: getDictionaryById(dictionaries, 'PRIORIDAD').values,
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        rejectionReasons: getDictionaryById(dictionaries, 'MOTIVORECHAZO').values,
        states: getDictionaryById(dictionaries, 'ESTADO').values,
        probabilities: getDictionaryById(dictionaries, 'PROBABILIDAD').values
    };

    const user = users.find(user => user.id === opportunity.idUser);
    if (user) {
        opportunity.idUser = user.email;
    }

    const supportBosonit = users.find(user => user.id === opportunity.supportBosonit);
    if (supportBosonit) {
        opportunity.supportBosonit = supportBosonit.email;
    }

    const car = cars.content.find(car => car.id === opportunity.car);
    if (car) {
        opportunity.car = car.name;
    }

    const subCar = subcars.content.find(subCar => subCar.id === opportunity.subcar);
    if (subCar) {
        opportunity.subcar = subCar.name;
    }

    const territory = parsedDictionaries.territories.find(territory => territory.value === opportunity.territory);
    if (territory) {
        opportunity.territory = territory.name;
    }

    const priority = parsedDictionaries.priorities.find(priority => priority.value === opportunity.priority);
    if (priority) {
        opportunity.priority = priority.name;
    }

    const state = parsedDictionaries.priorities.find(state => state.value === opportunity.state);
    if (state) {
        opportunity.state = state.name;
    }

    const rejectionReason = parsedDictionaries.priorities.find(rejectionReason => rejectionReason.value === opportunity.rejectionReason);
    if (rejectionReason) {
        opportunity.rejectionReason = rejectionReason.name;
    }

    const probability = parsedDictionaries.priorities.find(probability => probability.value === opportunity.probability);
    if (probability) {
        opportunity.probability = probability.name;
    }

    return opportunity;
}
