import {AbstractOpportunityService} from '../infrastructure/abstract-opportunity-service';

export function addOpportunityActivity(activity: any, opportunityId: string, service: AbstractOpportunityService) {
    return service.addActivity(activity, opportunityId);
}
