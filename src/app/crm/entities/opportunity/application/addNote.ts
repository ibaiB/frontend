import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {INote} from '../../note/domain/INote';
import {NoteMapper} from '../../note/infrastructure/note-mapper';
import {AbstractOpportunityService} from '../infrastructure/abstract-opportunity-service';
import {OpportunityMapper} from '../infrastructure/opportunity-mapper';
import {IOpportunity} from '../domain/IOpportunity';

export function addNote(note: INote, opportunityId: string, _service: AbstractOpportunityService): Observable<IOpportunity> {
    const noteMapper: NoteMapper = new NoteMapper();
    const opportunityMapper: OpportunityMapper = new OpportunityMapper();
    return _service.addNote(noteMapper.mapFrom(note), opportunityId)
        .pipe(map(opportunityMapper.mapTo));
}
