import {IOpportunityOut} from '../domain/IOpportunityOut';
import {AbstractOpportunityService} from '../infrastructure/abstract-opportunity-service';

export function updateOpportunity(opportunity: IOpportunityOut, service: AbstractOpportunityService) {
    return service.update(opportunity);
}
