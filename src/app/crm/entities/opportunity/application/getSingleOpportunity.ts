import {AbstractOpportunityService} from '../infrastructure/abstract-opportunity-service';

export function getSingleOpportunity(id: string, service: AbstractOpportunityService) {
    return service.get(id);
}
