import {AbstractOpportunityService} from '../infrastructure/abstract-opportunity-service';

export function getOpportunities(params: any, service: AbstractOpportunityService) {
    return service.search(params);
}
