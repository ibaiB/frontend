import {IOpportunity} from '../domain/IOpportunity';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {getDictionaryById} from '../../../../shared/entities/dictionary/application/getDictionaryById';

export function switchOpportunityKeys(opportunity: IOpportunity, dictionaries: IDictionaryType[], users: ICRMUser[]) {
    const parsedDictionaries = {
        cars: getDictionaryById(dictionaries, 'CAR').values,
        subCars: getDictionaryById(dictionaries, 'SUBCAR').values,
        priorities: getDictionaryById(dictionaries, 'PRIORIDAD').values,
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        rejectionReasons: getDictionaryById(dictionaries, 'MOTIVORECHAZO').values,
        states: getDictionaryById(dictionaries, 'ESTADO').values,
        probabilities: getDictionaryById(dictionaries, 'PROBABILIDAD').values
    };

    const user = users.filter(user => user.email === opportunity.idUser)[0];
    if (user) {
        opportunity.idUser = user.id;
    }

    const supportBosonit = users.filter(user => user.email === opportunity.supportBosonit)[0];
    if (supportBosonit) {
        opportunity.supportBosonit = supportBosonit.id;
    }

    const car = parsedDictionaries.cars.filter(car => car.name === opportunity.car)[0];
    if (car) {
        opportunity.car = car.value;
    }

    const subCar = parsedDictionaries.subCars.filter(subCar => subCar.name === opportunity.subcar)[0];
    if (subCar) {
        opportunity.subcar = subCar.value;
    }

    const territory = parsedDictionaries.territories.filter(territory => territory.name === opportunity.territory)[0];
    if (territory) {
        opportunity.territory = territory.value;
    }

    const priority = parsedDictionaries.priorities.filter(priority => priority.name === opportunity.priority)[0];
    if (priority) {
        opportunity.priority = priority.value;
    }

    const state = parsedDictionaries.priorities.filter(state => state.name === opportunity.state)[0];
    if (state) {
        opportunity.state = state.value;
    }

    const rejectionReason = parsedDictionaries.priorities.filter(rejectionReason => rejectionReason.name === opportunity.rejectionReason)[0];
    if (rejectionReason) {
        opportunity.rejectionReason = rejectionReason.value;
    }

    const probability = parsedDictionaries.priorities.filter(probability => probability.name === opportunity.probability)[0];
    if (probability) {
        opportunity.probability = probability.value;
    }

    if (opportunity.warningDate && opportunity.warningDate !== '') {
        opportunity.warningDate = new Date(opportunity.warningDate).toISOString();
    }

    return opportunity;
}
