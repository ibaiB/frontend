import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class OpportunityTableMapper {
    mapTo(params: any) {
        const {
            id,
            name,
            priority,
            state,
            probability,
            quantity,
            warningDate,
            idUser,
            idContact,
            idAccount,
            isLate
        } = params;

        return {
            id,
            name,
            priority,
            state,
            probability,
            estimatedBudget: quantity,
            warningDate,
            idUser,
            idContact,
            idAccount,
            isLate
        };
    }
}
