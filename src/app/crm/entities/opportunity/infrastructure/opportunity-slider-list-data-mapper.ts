import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class OpportunitySliderListDataMapper {
    mapTo(params: any, father: any) {
        const {
            myCrm,
            crmUsers,
            opportunityStates,
            priorities,
            opportunityProbabilities,
            cars,
            subCars,
            territories,
            opportunityRejectionReasons
        } = params;

        return {
            myCrm: myCrm,
            crmUsers: crmUsers,
            father: father,
            states: opportunityStates,
            priorities: priorities,
            probabilities: opportunityProbabilities,
            cars: cars,
            subCars: subCars,
            territories: territories,
            rejectionReasons: opportunityRejectionReasons
        };
    }
}
