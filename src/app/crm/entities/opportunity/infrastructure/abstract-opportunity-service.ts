import {IOpportunityOut} from './../domain/IOpportunityOut';
import {Observable} from 'rxjs';
import {IOpportunity} from '../domain/IOpportunity';
import {IBudget} from '../../budget/domain/IBudget';
import {IWebActivity} from '../../activity/domain/IWebActivity';
import {IWebNote} from '../../note/domain/IWebNote';
import {IWebOpportunity} from '../domain/IWebOpportunity';
import {IList} from 'src/app/shared/generics/IList';


export abstract class AbstractOpportunityService {
    abstract getList(params: any): Observable<IOpportunity[]>;

    abstract get(id: string): Observable<IOpportunity>;

    abstract update(opportunity: IOpportunityOut): Observable<IOpportunity>;

    abstract addBudget(budget: IBudget, opportunityId: string): Observable<IOpportunity>;

    //@TODO this could be merged with getList
    abstract search(params: any): Observable<IList<IOpportunity>>;

    abstract addActivity(activity: IWebActivity, opportunityId: string): Observable<IOpportunity>;

    abstract addNote(note: IWebNote, opportunityId: string): Observable<IWebOpportunity>;
}
