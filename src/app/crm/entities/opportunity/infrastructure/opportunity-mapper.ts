import {Injectable} from '@angular/core';
import {AccountMapper} from '../../account/infrastructure/account-mapper';
import {ActivityMapper} from '../../activity/infrastructure/activity-mapper';
import {ContactMapper} from '../../contact/infrastructure/contact-mapper';
import { UserMapper } from '../../user/infrastructure/user-mapper';

@Injectable({
    providedIn: 'any',
})
export class OpportunityMapper {
    mapTo(params: any) {
        const activityMapper = new ActivityMapper();
        const accountMapper = new AccountMapper();
        const contactMapper = new ContactMapper();
        const userMapper = new UserMapper();
        const {
            cantidad,
            cuenta,
            contacto,
            diaRegistro,
            estado,
            fechaAviso,
            id,
            actividades,
            idContacto,
            idCuenta,
            idUsuario,
            idTerritorio,
            usuario,
            motivoRechazo,
            nombre,
            notas,
            personaApoyoBosonit,
            personaApoyoNfq,
            personaApoyoTech,
            presupuestoEstimado,
            presupuestos,
            prioridad,
            probabilidad,
            idCar,
            idSubcar,
            tipo,
            territorio,
            proyectoCerrado,
            recursoDedicado,
            numeroRecursos,
            porcentajeRecurrente,
            fechaCierre
        } = params;

        return {
            quantity: cantidad,
            account: cuenta ? accountMapper.mapTo(cuenta) : null,
            contact: contacto ? contactMapper.mapTo(contacto) : null,
            registerDate: diaRegistro,
            state: estado,
            warningDate: fechaAviso,
            id,
            activities: actividades ? actividades.map(activityMapper.mapTo) : null,
            idContact: idContacto,
            idAccount: idCuenta,
            idUser: idUsuario,
            user:usuario ? userMapper.mapTo(usuario) : null,
            rejectionReason: motivoRechazo,
            name: nombre,
            notes: notas,
            supportBosonit: personaApoyoBosonit,
            supportNfq: personaApoyoNfq,
            supportTech: personaApoyoTech,
            estimatedBudget: presupuestoEstimado,
            budgets: presupuestos,
            priority: prioridad,
            probability: probabilidad,
            car: idCar,
            subcar: idSubcar,
            type: tipo,
            territory: territorio ? territorio.nombre : (idTerritorio ?? ''),
            closedProject: proyectoCerrado,
            dedicatedResource: recursoDedicado,
            resourceNumbers: numeroRecursos,
            recurrentPercentage: porcentajeRecurrente,
            closeDate: fechaCierre
        };
    }

    mapFrom(params: any) {
        const {
            quantity,
            account,
            registerDate,
            state,
            warningDate,
            id,
            idActivity,
            idContact,
            idAccount,
            idUser,
            user,
            rejectionReason,
            name,
            notes,
            supportBosonit,
            supportNfq,
            supportTech,
            estimatedBudget,
            budgets,
            priority,
            probability,
            car,
            subcar,
            type,
            territory,
            closeDate
        } = params;

        return {
            cantidad: quantity,
            cuenta: account,
            diaRegistro: registerDate,
            estado: state,
            fechaAviso: warningDate,
            id,
            idActividad: idActivity,
            idContacto: idContact,
            idCuenta: idAccount,
            idUsuario: idUser,
            usuario:user,
            motivoRechazo: rejectionReason,
            nombre: name,
            notas: notes,
            personaApoyoBosonit: supportBosonit,
            personaApoyoNfq: supportNfq,
            personaApoyoTech: supportTech,
            presupuestoEstimado: estimatedBudget,
            presupuestos: budgets,
            prioridad: priority,
            probabilidad: probability,
            idCar: car,
            idSubCar: subcar,
            tipo: type,
            idTerritorio: territory,
            fechaCierre: closeDate
        };
    }
}
