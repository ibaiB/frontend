import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class OpportunitySliderDataMapper {
    mapTo(params: any) {
        const {
            myCrm,
            opportunity,
            crmUsers,
            cars,
            subCars,
            activityTypes,
            activityStates,
            priorities,
            opportunityStates,
            territories,
            opportunityProbabilities,
            opportunityRejectionReasons,
            categories
        } = params;

        return {
            myCrm: myCrm,
            opportunity: opportunity,
            contact: opportunity.contact,
            account: opportunity.account,
            crmUsers: crmUsers,
            cars: cars,
            subCars: subCars,
            activityTypes: activityTypes,
            activityStates: activityStates,
            priorities: priorities,
            opportunityStates: opportunityStates,
            territories: territories,
            opportunityProbabilities: opportunityProbabilities,
            opportunityRejectionReasons: opportunityRejectionReasons,
            categories: categories
        };
    }
}
