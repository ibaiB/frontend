import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class OpportunityOutMapper {
    mapFrom(params: any) {
        const {
            car,
            subcar,
            state,
            priority,
            probability,
            territory,
            rejectionReason,
            idUser,
            supportBosonit,
            supportTech,
            supportNfq,
            initialWarningDate,
            finalWarningDate,
            warningDate,
            name,
            estimatedBudget,
            projectClosed,
            dedicatedResource,
            resourceNumbers,
            recurrentPercentage,

            //TODO: Not used in create maybe in edit
            quantity,
            registerDate,
            id,
            idActivity,
            idContact,
            idAccount,
            notes,
            budget,
            type,
            sendDate,
            closeDate

        } = params;

        return {
            id: id,
            idCar: car,
            idSubcar: subcar,
            idEstado: state,
            prioridad: priority,
            probabilidad: probability,
            idTerritorio: territory,
            motivoRechazo: rejectionReason,
            idUsuario: idUser,
            personaApoyoBosonit: supportBosonit,
            personaApoyoNfq: supportNfq,
            personaApoyoTech: supportTech,
            fechaAvisoInicial: initialWarningDate,
            fechaAvisoFinal: finalWarningDate,
            fechaAviso:warningDate,
            nombre: name,
            presupuestoEstimado: estimatedBudget,
            proyectoCerrado: projectClosed,
            recursoDedicado: dedicatedResource,
            porcentajeRecurrente: recurrentPercentage,
            numeroRecursos: resourceNumbers,

            //TODO: Not used in create maybe in edit
            cantidad: quantity,
            diaRegistro: registerDate, //Data
            idActividad: idActivity,
            notas: notes,
            tipo: type,
            idContacto: idContact,
            idCuenta: idAccount,
            presupuestos: budget,
            fechaCierre: closeDate
        };
    }
}
