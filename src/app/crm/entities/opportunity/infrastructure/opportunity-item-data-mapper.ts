import {IOpportunity} from '../domain/IOpportunity';

export class OpportunityItemDataMapper {
    mapTo(params: any, opportunity: IOpportunity) {
        const {
            father,
            myCrm,
            crmUsers,
            states,
            priorities,
            probabilities,
            cars,
            subCars,
            territories,
            rejectionReasons
        } = params;

        return {
            opportunity: opportunity,
            father: father,
            myCrm: myCrm,
            crmUsers: crmUsers,
            states: states,
            priorities: priorities,
            probabilities: probabilities,
            cars: cars,
            subCars: subCars,
            territories: territories,
            rejectionReasons: rejectionReasons
        };
    }
}
