import {FormGroup} from '@angular/forms';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any'
})
export class OpportunityFacade {
    switch(form: FormGroup, data: any): any {
        const {
            accountId,
            contactId,
            car,
            subCar,
            supportBosonit,
            state,
            priority,
            probability,
            territory
        } = form.value;

        const carValue = data.cars.find(c => c.name === car);
        const subCarValue = data.subCars.find(s => s.name === subCar);
        const stateValue = data.states.find(s => s.name === state);
        const priorityValue = data.priorities.find(p => p.name === priority);
        const probabilityValue = data.probabilities.find(p => p.name === probability);
        const territoryValue = data.territories.find(t => t.name === territory);
        const supportBosonitValue = data.crmUsers.find(u => u.email === supportBosonit);
        const accountValue = data.accounts.find(account => account.commercialName === accountId);
        const contactValue = data.contacts.find(contact => contact.name === contactId);

        return {
            car: carValue ? carValue.value : null,
            subCar: subCarValue ? subCarValue.value : null,
            state: stateValue ? stateValue.value : null,
            priority: priorityValue ? priorityValue.value : null,
            probability: probabilityValue ? probabilityValue.value : null,
            territory: territoryValue ? territoryValue.value : null,
            idUser: data.myCrm.id,
            supportBosonit: supportBosonitValue ? supportBosonitValue.id : data.myCrm.id,
            supportNfq: null,
            supportTech: null,
            accountId: accountValue ? accountValue.id : null,
            contactId: contactValue ? contactValue.id : null
        };
    }


    setBase(form: FormGroup): any {
        const {opportunityName, estimatedBudget, warningDate} = form.value;

        return {
            opportunityName,
            estimatedBudget,
            warningDate: warningDate ? new Date(warningDate).toISOString() : null
        };
    }
}
