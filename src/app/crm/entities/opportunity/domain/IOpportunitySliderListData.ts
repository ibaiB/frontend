import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IOpportunitySliderListData {
    myCrm: ICRMUser,
    crmUsers: ICRMUser[],
    father: any,
    states: IDictionaryValue[],
    priorities: IDictionaryValue[],
    probabilities: IDictionaryValue[],
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    territories: IDictionaryValue[],
    rejectionReasons: IDictionaryValue[]
}
