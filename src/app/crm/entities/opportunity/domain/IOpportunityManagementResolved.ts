import {IOpportunityManagementData} from './IOpportunityManagementData';

export interface IOpportunityManagementResolved {
    tableData: IOpportunityManagementData;
    error?: any;
}
