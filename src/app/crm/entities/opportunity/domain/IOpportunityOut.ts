export interface IOpportunityOut {
    id?: string;
    cantidad: string;
    idCar: string;
    diaRegistro: string; //Data
    fechaAvisoInicial: string; //Data
    fechaAvisoFinal: string;
    idActividad: string;
    idEstado: string;
    idUsuario: string;
    motivoRechazo: string;
    nombre: string;
    notas: string;
    personaApoyoBosonit: string;
    personaApoyoNfq: string;
    personaApoyoTech: string;
    presupuestoEstimado: string;
    prioridad: string;
    probabilidad: number;
    idSubcar: string;
    tipo: string;
    idTerritorio: string;
    proyectoCerrado: boolean;
    fechaCierre: string;
}
