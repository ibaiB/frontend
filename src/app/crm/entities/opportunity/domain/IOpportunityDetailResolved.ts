import {IOpportunityDetailData} from './IOpportunityDetailData';

export interface IOpportunityDetailResolved {
    detailData: IOpportunityDetailData;
    error?: any;
}
