import {IWebOpportunity} from './IWebOpportunity';

export interface IWebOpportunityList {
    content: IWebOpportunity[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
