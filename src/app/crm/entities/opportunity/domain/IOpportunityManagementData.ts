import {IUserInfo} from '../../../../auth/session/domain/IUserInfo';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IOpportunity} from './IOpportunity';
import {IAccount} from '../../account/domain/IAccount';
import {IContact} from '../../contact/domain/IContact';
import {IState} from '../../state/domain/IState';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IOpportunityManagementData {
    myUser: IUserInfo;
    myCrm: ICRMUser;
    crmUsers: ICRMUser[];
    accounts: IAccount[];
    contacts: IContact[];
    cars: IList<ICar>;
    subCars: IList<ISubcar>;
    territories: IDictionaryValue[];
    priorities: IDictionaryValue[];
    states: IDictionaryValue[];
    probabilities: IDictionaryValue[];
    rejectionReasons: IDictionaryValue[];
    opportunities: IList<IOpportunity>;
    orderStates: IState[];
    filtersValues:any;
    tableFiltersConfig: ITableFiltersConfig;
}
