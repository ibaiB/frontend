import {IOpportunity} from './IOpportunity';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IList} from 'src/app/shared/generics/IList';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IOpportunityItemData {
    opportunity: IOpportunity,
    father: any,
    myCrm: ICRMUser,
    crmUsers: ICRMUser[],
    states: IDictionaryValue[],
    priorities: IDictionaryValue[],
    probabilities: IDictionaryValue[],
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    territories: IDictionaryValue[],
    rejectionReasons: IDictionaryValue[]
}
