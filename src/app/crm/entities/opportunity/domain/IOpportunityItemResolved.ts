import {IOpportunityItemData} from './IOpportunityItemData';

export interface IOpportunityItemResolved {
    detailData: IOpportunityItemData;
    error?: any;
}
