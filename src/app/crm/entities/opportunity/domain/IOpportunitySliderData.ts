import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IAccount} from '../../account/domain/IAccount';
import {IContact} from '../../contact/domain/IContact';
import {IOpportunity} from './IOpportunity';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IOpportunitySliderData {
    myCrm: ICRMUser;
    opportunity: IOpportunity;
    contact: IContact;
    account: IAccount;
    crmUsers: ICRMUser[];
    cars: IList<ICar>;
    subCars: IList<ISubcar>;
    activityTypes: IDictionaryValue[];
    activityStates: IDictionaryValue[];
    priorities: IDictionaryValue[];
    opportunityStates: IDictionaryValue[];
    territories: IDictionaryValue[];
    opportunityProbabilities: IDictionaryValue[];
    opportunityRejectionReasons: IDictionaryValue[];
    categories: IDictionaryValue[]
}
