import {IUserInfo} from '../../../../auth/session/domain/IUserInfo';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IOpportunity} from './IOpportunity';
import {IList} from 'src/app/shared/generics/IList';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IOpportunityDetailData {
    myUser: IUserInfo,
    myCrm: ICRMUser,
    opportunity: IOpportunity,
    crmUsers: ICRMUser[],
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    categories: IDictionaryValue[],
    priorities: IDictionaryValue[],
    territories: IDictionaryValue[],
    sectors: IDictionaryValue[],
    relationshipLevels: IDictionaryValue[],
    activityTypes: IDictionaryValue[],
    activityStates: IDictionaryValue[],
    rejectionReasons: IDictionaryValue[],
    states: IDictionaryValue[],
    probabilities: IDictionaryValue[],
}
