import {IWebActivity} from '../../activity/domain/IWebActivity';
import {ITerritory} from '../../territory/domain/ITerritory';

export interface IWebOpportunity {
    cantidad: string;
    idCar: string;
    cuenta: any;
    contacto: any;
    diaRegistro: string; // Date
    estado: any;
    fechaAviso: string; // Date
    id: string;
    actividades: IWebActivity[];
    usuario:any;
    idContacto: string;
    idCuenta: string;
    idUsuario: string;
    motivoRechazo: string;
    nombre: string;
    notas: string; // ???
    personaApoyoBosonit: string;
    personaApoyoNfq: string;
    personaApoyoTech: string;
    presupuestoEstimado: string;
    presupuestos: any[];
    prioridad: string;
    probabilidad: number;
    idSubcar: string;
    tipo: string;
    idTerritorio: ITerritory;
    proyectoCerrado: boolean;
    recursoDedicado: boolean;
    numeroRecursos: string;
    porcentajeRecurrente: string;
    fechaCierre: string; // Date
}
