import {IBudget} from '../../budget/domain/IBudget';
import {IAccount} from '../../account/domain/IAccount';
import {IContact} from '../../contact/domain/IContact';
import {IActivity} from '../../activity/domain/IActivity';
import { ICRMUser } from '../../user/domain/ICRMUser';

export interface IOpportunity {
    quantity: number;
    account: IAccount; // map into
    contact: IContact;
    registerDate: string;
    state: any; // map into
    warningDate: string;
    id?: string;
    activities: IActivity[];
    idContact: string;
    idAccount: string;
    idUser: string;
    user:ICRMUser;
    rejectionReason: string;
    name: string;
    notes: string;
    supportBosonit: string;
    supportNfq: string;
    supportTech: string;
    estimatedBudget: string;
    budgets: IBudget[]; // map into
    priority: string;
    probability: string;
    car: string;
    subcar: string;
    type: string;
    territory: string;
    closedProject: boolean;
    dedicatedResource: boolean;
    resourceNumbers: string;
    recurrentPercentage: string;
    closeDate: string;
}
