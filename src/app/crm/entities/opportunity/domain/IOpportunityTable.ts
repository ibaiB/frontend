export interface IOpportunityTable {
    id: string;
    name: string;
    priority: string;
    state: string;
    probability: string;
    estimatedBudget: string;
    warningDate: string;
    idUser: string;
    idContact: string;
    idAccount: string;
    isLate: boolean;
}
