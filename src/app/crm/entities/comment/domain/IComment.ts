export interface IComment {
    description: string,
    created: string,
    id?: string
}
