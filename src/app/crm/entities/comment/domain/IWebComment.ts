export interface IWebComment {
    mensaje: string;
    fecha: string;
    id: string;
}
