import {AbstractCommentService} from '../infrastructure/abstract-comment-service';
import {Observable} from 'rxjs';
import {IComment} from '../domain/IComment';
import {CommentMapper} from '../infrastructure/comment-mapper';
import {map} from 'rxjs/operators';

export function getComment(idComment: string, service: AbstractCommentService): Observable<IComment> {
    const commentMapper: CommentMapper = new CommentMapper();

    return service.get(idComment).pipe(map(commentMapper.mapTo));
}
