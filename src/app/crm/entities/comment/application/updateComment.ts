import {IComment} from '../domain/IComment';
import {AbstractCommentService} from '../infrastructure/abstract-comment-service';
import {CommentMapper} from '../infrastructure/comment-mapper';
import {map} from 'rxjs/operators';

export function updateComment(comment: IComment, service: AbstractCommentService) {
    const commentMapper: CommentMapper = new CommentMapper();

    return service.update(commentMapper.mapFrom(comment)).pipe(map(commentMapper.mapTo));
}
