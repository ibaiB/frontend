import {IComment} from '../domain/IComment';
import {IWebComment} from '../domain/IWebComment';
import {IList} from '../../../../shared/generics/IList';

export class CommentMapper {
    mapTo(params: any): IComment {
        const {
            id,
            fecha,
            mensaje
        } = params;

        return {
            id: id,
            created: fecha,
            description: mensaje
        };
    }

    mapFrom(params: any): IWebComment {
        const {
            id,
            created,
            description
        } = params;

        return {
            id: id,
            fecha: created,
            mensaje: description
        };
    }

    mapList(list: IList<any>): IList<IComment> {
        const {
            content,
            numberOfElements,
            totalElements,
            totalPages
        } = list;

        return {
            content: content.map(this.mapTo),
            numberOfElements,
            totalElements,
            totalPages
        };
    }
}
