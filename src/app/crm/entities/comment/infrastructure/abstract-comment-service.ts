import {Observable} from 'rxjs';
import {IWebComment} from '../domain/IWebComment';

export abstract class AbstractCommentService {
    abstract get(id: string): Observable<IWebComment>;

    abstract update(comment: IWebComment): Observable<IWebComment>;
}
