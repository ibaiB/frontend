import {ICRMUser} from '../../user/domain/ICRMUser';
import {IActivity} from './IActivity';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IActivityItemData {
    myCrm: ICRMUser,
    crmUsers: ICRMUser[],
    activity: IActivity,
    father: any,
    states: IDictionaryValue[],
    priorities: IDictionaryValue[],
    probabilities: IDictionaryValue[],
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    types: IDictionaryValue[],
    territories: IDictionaryValue[]
}
