export interface IActivityTable {
    id: string;
    name: string;
    idAccount: string;
    type: string;
    state: string;
    priority: string;
    warningDate: string;
    idUser: string;
    car: string;
    subCar: string;
    territory: string;
    isLate: boolean;
    idAssignedContact: string;
}
