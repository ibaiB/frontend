import {IActivityTableData} from './IActivityTableData';

export interface IActivityTableResolved {
    tableData: IActivityTableData;
    error?: any;
}
