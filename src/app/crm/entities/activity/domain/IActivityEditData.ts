import {IActivity} from './IActivity';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IActivityEditData {
    myCrm: ICRMUser,
    activity: IActivity,
    crmUsers: ICRMUser[]
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    territories: IDictionaryValue[],
    sectors: IDictionaryValue[],
    categories: IDictionaryValue[],
    priorities: IDictionaryValue[],
    activityTypes: IDictionaryValue[],
    activityStates: IDictionaryValue[]
}
