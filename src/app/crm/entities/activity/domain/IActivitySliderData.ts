import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IActivity} from './IActivity';
import {IAccount} from '../../account/domain/IAccount';
import {IContact} from '../../contact/domain/IContact';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IActivitySliderData {
    myCrm: ICRMUser;
    activity: IActivity;
    account: IAccount;
    contact: IContact;
    crmUsers: ICRMUser[];
    cars: IList<ICar>;
    subCars: IList<ISubcar>;
    activityTypes: IDictionaryValue[];
    activityStates: IDictionaryValue[];
    priorities: IDictionaryValue[];
    opportunityStates: IDictionaryValue[];
    territories: IDictionaryValue[];
    opportunityProbabilities: IDictionaryValue[];
    opportunityRejectionReasons: IDictionaryValue[];
    categories: IDictionaryValue[];
    relationshipLevels: IDictionaryValue[];
    states: IDictionaryValue[];
    probabilities: IDictionaryValue[];
    father: any;

}
