import {IActivityDetailData} from './IActivityDetailData';

export interface IActivityDetailResolved {
    detailData: IActivityDetailData;
    error?: any;
}
