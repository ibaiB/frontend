import {IWebActivity} from './IWebActivity';

export interface IWebActivityList {
    content: IWebActivity[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
