import { ICRMUser } from "../../user/domain/ICRMUser";

export interface IWebActivity {
    campana: string;
    color: string;
    descripcion: string;
    estado: string;
    fechaAlta: string; //Date
    fechaAviso: string; //Date
    fechaCierre: string; //Date
    fechaEjecucion: string; //Date
    fechaFin: string; //Date
    fechaInicio: string; //Date
    id: string;
    idActividadPadre: string;
    idContactoAsignado: string;
    idUsuario: string;
    usuario:ICRMUser;
    idUsuarioAutor: string;
    nombre: string;
    prioridad: string;
    tipo: string;
    tipoAlta: string;
    cifCuenta: string;
    idCar: string;
    idSubcar: string;
    apoyoNfq: string;
    apoyoBosonit: string;
    apoyoTech: string;
    idCuenta: string;
    idContacto: string;
    idTerritorio: string;
    notas: any;
    cuenta: any;
    contacto: any;
    oportunidad: any;
}
