import {IUserInfo} from '../../../../auth/session/domain/IUserInfo';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IActivity} from '../../activity/domain/IActivity';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IActivityTableData {
    myUser: IUserInfo;
    myCrm: ICRMUser;
    users: ICRMUser[];
    cars: IList<ICar>;
    subCars: IList<ISubcar>;
    territories: IDictionaryValue[];
    priorities: IDictionaryValue[];
    states: IDictionaryValue[];
    types: IDictionaryValue[];
    categories: IDictionaryValue[];
    activities: IList<IActivity>;
    filtersValue:any;
    tableFiltersConfig: ITableFiltersConfig;
}
