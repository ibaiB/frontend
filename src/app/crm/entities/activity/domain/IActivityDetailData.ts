import {IUserInfo} from '../../../../auth/session/domain/IUserInfo';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IActivity} from './IActivity';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IActivityDetailData {
    myUser: IUserInfo,
    myCrm: ICRMUser,
    activity: IActivity,
    crmUsers: ICRMUser[],
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    categories: IDictionaryValue[],
    priorities: IDictionaryValue[],
    territories: IDictionaryValue[],
    sectors: IDictionaryValue[],
    relationshipLevels: IDictionaryValue[],
    activityTypes: IDictionaryValue[],
    activityStates: IDictionaryValue[],
    opportunityRejectionReasons: IDictionaryValue[],
    opportunityStates: IDictionaryValue[],
    opportunityProbabilities: IDictionaryValue[],
}
