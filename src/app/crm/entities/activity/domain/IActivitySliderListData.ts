import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IActivitySliderListData {
    myCrm: ICRMUser;
    crmUsers: ICRMUser[];
    father: any;
    fatherType: string,
    cars: IList<ICar>;
    subCars: IList<ISubcar>;
    priorities: IDictionaryValue[];
    states: IDictionaryValue[];
    types: IDictionaryValue[];
    territories: IDictionaryValue[];
}
