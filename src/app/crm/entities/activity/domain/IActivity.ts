import {IAccount} from '../../account/domain/IAccount';
import {IContact} from '../../contact/domain/IContact';
import {INote} from '../../note/domain/INote';
import {IOpportunity} from '../../opportunity/domain/IOpportunity';
import {IState} from '../../state/domain/IState';
import { ICRMUser } from '../../user/domain/ICRMUser';

export interface IActivity {
    id?: string;
    name: string;
    car: string;
    subCar: string;
    supportNFQ: string;
    supportBosonit: string;
    supportTECH: string;
    state: string;
    type: string;
    warningDate: string;
    registerDate: Date;
    closeDate: Date;
    idAccount: string;
    idContact: string;
    idOpportunity: string,
    idUser: string;
    user:ICRMUser;
    priority: string;
    territory: string;
    cifAccount: string;
    notes: INote[];
    account: IAccount;
    contact: IContact;
    opportunity: IOpportunity;

    //No used
    idAuthorUser: IState;
    idAssignedContact: string;
    campaign: string;
    registerType: string;
    fatherActivityId: string;
    color: string;
    endDate: Date;
    execDate: Date;
    startDate: Date;
    description: string;
}
