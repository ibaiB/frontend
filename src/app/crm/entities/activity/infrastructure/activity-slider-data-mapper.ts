import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class ActivitySliderDataMapper {
    mapTo(params: any) {
        const {
            myCrm,
            activity,
            account,
            contact,
            crmUsers,
            cars,
            subCars,
            activityTypes,
            activityStates,
            priorities,
            opportunityStates,
            territories,
            opportunityProbabilities,
            opportunityRejectionReasons,
            categories,
            relationshipLevels,
            states,
            probabilities,
            father
        } = params;

        return {
            myCrm: myCrm,
            activity: activity,
            account: account,
            contact: contact,
            crmUsers: crmUsers,
            cars: cars,
            subCars: subCars,
            activityTypes: activityTypes,
            activityStates: activityStates,
            priorities: priorities,
            opportunityStates: opportunityStates,
            territories: territories,
            opportunityProbabilities: opportunityProbabilities,
            opportunityRejectionReasons: opportunityRejectionReasons,
            categories: categories,
            relationshipLevels: relationshipLevels,
            states: states,
            probabilities: probabilities,
            father: father
        };
    }
}
