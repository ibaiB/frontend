import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class ActivitySliderListDataMapper {
    mapTo(params: any, father: any, fatherType:string) {
        const {
            myCrm,
            crmUsers,
            cars,
            subCars,
            priorities,
            activityStates,
            activityTypes,
            territories,
        } = params;

        return {
            myCrm: myCrm,
            crmUsers: crmUsers,
            father: father,
            fatherType:fatherType,
            cars: cars,
            subCars: subCars,
            priorities: priorities,
            states: activityStates,
            types: activityTypes,
            territories: territories
        };
    }
}
