import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class ActivityTableMapper {
    mapTo(params: any) {
        const {
            id,
            name,
            idAccount,
            type,
            state,
            priority,
            warningDate,
            idUser,
            car,
            subCar,
            territory,
            isLate,
            idAssignedContact
        } = params;

        return {
            id,
            name,
            idAccount,
            type,
            state,
            priority,
            warningDate,
            idUser,
            car,
            subCar,
            territory,
            isLate,
            idAssignedContact
        };
    }
}
