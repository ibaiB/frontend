import {Observable} from 'rxjs';
import {IActivity} from '../../activity/domain/IActivity';
import {IWebNote} from '../../note/domain/IWebNote';
import {IWebActivity} from '../domain/IWebActivity';

export abstract class AbstractActivityService {
    abstract get(id: string): Observable<IActivity>;

    abstract update(activity: IActivity): Observable<IActivity>;

    abstract delete(id: string);

    abstract getList(params: any): Observable<IActivity[]>;

    abstract addNote(note: IWebNote, activityId: string): Observable<IWebActivity>
}
