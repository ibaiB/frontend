import {Injectable} from '@angular/core';
import {AccountMapper} from '../../account/infrastructure/account-mapper';
import {ContactMapper} from '../../contact/infrastructure/contact-mapper';
import {IActivity} from '../domain/IActivity';
import { UserMapper } from '../../user/infrastructure/user-mapper';
import { OpportunityMapper } from '../../opportunity/infrastructure/opportunity-mapper';

@Injectable({
    providedIn: 'any',
})
export class ActivityMapper {
    mapTo(params: any): IActivity {
        const accountMapper = new AccountMapper();
        const contactMapper = new ContactMapper();
        const opportunityMapper = new OpportunityMapper();
        const userMapper = new UserMapper();
        const {
            id,
            idActividadPadre,
            idContactoAsignado,
            idUsuario,
            usuario,
            idUsuarioAutor,
            nombre,
            prioridad,
            tipo,
            tipoAlta,
            campana,
            color,
            estado,
            descripcion,
            fechaAlta,
            fechaAviso,
            fechaCierre,
            fechaInicio,
            fechaEjecucion,
            fechaFin,
            cifCuenta,
            idCar,
            idSubcar,
            apoyoNfq,
            personaApoyoBosonit,
            apoyoTech,
            idCuenta,
            idContacto,
            idOportunidad,
            idTerritorio,
            notas,
            cuenta,
            contacto,
            oportunidad
        } = params;

        return {
            id: id,
            name: nombre,
            description: descripcion,
            idUser: idUsuario,
            user:usuario ? userMapper.mapTo(usuario) : null,
            idAssignedContact: idContactoAsignado,
            campaign: campana,
            state: estado,
            registerType: tipoAlta,
            type: tipo,
            fatherActivityId: idActividadPadre,
            color: color,
            idAuthorUser: idUsuarioAutor,
            priority: prioridad,
            endDate: fechaFin,
            closeDate: fechaFin,
            execDate: fechaEjecucion,
            warningDate: fechaAviso,
            registerDate: fechaAlta,
            startDate: fechaInicio,
            cifAccount: cifCuenta,
            car: idCar,
            subCar: idSubcar,
            supportNFQ: apoyoNfq,
            supportBosonit: personaApoyoBosonit,
            supportTECH: apoyoTech,
            idAccount: idCuenta,
            idContact: idContacto,
            idOpportunity: idOportunidad,
            territory: idTerritorio,
            notes: notas,
            account: cuenta ? accountMapper.mapTo(cuenta) : null,
            contact: contacto ? contactMapper.mapTo(contacto) : null,
            opportunity: oportunidad ? opportunityMapper.mapTo(oportunidad) : null
        };
    }

    mapFrom(params: any) {
        const {
            car,
            subCar,
            idUser,
            user,
            supportBosonit,
            supportNfq,
            supportTech,
            state,
            type,
            priority,
            initialWarningDate,
            finalWarningDate,
            name,
            territory,

            //TODO: not used in create maybe in edit
            idAccount,
            id,
            description,
            idAssignedContact,
            campaign,
            registerType,
            fatherActivityId,
            color,
            idAuthorUser,
            endDate,
            closeDate,
            execDate,
            registerDate,
            startDate,
            cifAccount,
            idContact,
            idOpportunity,
            warningDate
        } = params;

        return {
            idCar: car,
            idSubcar: subCar,
            idUsuario: idUser,
            usuario:user,
            apoyoBosonit: supportBosonit,
            apoyoNfq: supportNfq,
            apoyoTech: supportTech,
            estado: state,
            tipo: type,
            prioridad: priority,
            fechaAviso: warningDate,
            fechaAvisoFinal: finalWarningDate,
            fechaAvisoInicial: initialWarningDate,
            nombre: name,
            idTerritorio: territory,

            //TODO: not used in create maybe in edit
            idCuenta: idAccount,
            id: id,
            campana: campaign,
            color: color,
            descripcion: description,
            fechaEjecucion: execDate,
            fechaFin: endDate,
            fechaInicio: startDate,
            fechaAlta: registerDate,
            fechaCierre: closeDate,
            idActividadPadre: fatherActivityId,
            idContactoAsignado: idAssignedContact,
            idUsuarioAutor: idAuthorUser,
            tipoAlta: registerType,
            cifCuenta: cifAccount,
            idContacto: idContact,
            idOportunidad:idOpportunity
        };
    }
}
