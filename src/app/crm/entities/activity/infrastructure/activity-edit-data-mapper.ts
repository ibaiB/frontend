import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class ActivityEditDataMapper {
    mapTo(params: any) {
        const {
            cars,
            myCrm,
            activity,
            crmUsers,
            priorities,
            sectors,
            subCars,
            territories,
            categories,
            activityTypes,
            activityStates
        } = params;
        return {
            myCrm: myCrm,
            activity: activity,
            crmUsers: crmUsers,
            cars: cars,
            subCars: subCars,
            territories: territories,
            sectors: sectors,
            categories: categories,
            priorities: priorities,
            activityTypes: activityTypes,
            activityStates: activityStates
        };
    }
}
