import {IActivity} from '../domain/IActivity';

export class ActivityItemDataMapper {
    mapTo(params: any, activity: IActivity) {
        const {
            myCrm,
            crmUsers,
            father,
            states,
            priorities,
            probabilities,
            cars,
            subCars,
            types,
            territories
        } = params;

        return {
            myCrm: myCrm,
            crmUsers: crmUsers,
            activity: activity,
            father: father,
            states: states,
            priorities: priorities,
            probabilities: probabilities,
            cars: cars,
            subCars: subCars,
            types: types,
            territories: territories
        };
    }
}
