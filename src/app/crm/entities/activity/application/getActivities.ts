import {AbstractActivityService} from '../infrastructure/abstract-activity-service';

export function getActivities(params: any, service: AbstractActivityService) {
    return service.getList(params);
}
