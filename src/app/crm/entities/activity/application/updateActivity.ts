import {AbstractActivityService} from '../infrastructure/abstract-activity-service';

export function updateActivity(activity: any, service: AbstractActivityService) {
    return service.update(activity);
}
