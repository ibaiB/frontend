import {AbstractActivityService} from '../infrastructure/abstract-activity-service';

export function getSingleActivity(id: string, service: AbstractActivityService) {
    return service.get(id);
}
