import {ICRMUser} from '../../user/domain/ICRMUser';
import {IActivity} from '../domain/IActivity';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {getDictionaryById} from '../../../../shared/entities/dictionary/application/getDictionaryById';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export function joinActivity(activity: IActivity, dictionaries: IDictionaryType[], users: ICRMUser[], cars: IList<ICar>, subCars: IList<ISubcar>) {

    const parsedDictionaries = {
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        priorities: getDictionaryById(dictionaries, 'PRIORIDAD').values,
        types: getDictionaryById(dictionaries, 'TIPOACTIVIDAD').values,
        states: getDictionaryById(dictionaries, 'ESTADOACTIVIDAD').values,
    };
    //TODO: detele user change
    const user = users.find(user => user.id === activity.idUser);
    if (user) {
        activity.idUser = user.email;
    }

    const supportBosonit = users.find(user => user.id === activity.supportBosonit);
    if (supportBosonit) {
        activity.supportBosonit = supportBosonit.email;
    }

    const car = cars.content.find(car => car.id === activity.car);
    if (car) {
        activity.car = car.name;
    }

    const subCar = subCars.content.find(subCar => subCar.id === activity.subCar);
    if (subCar) {
        activity.subCar = subCar.name;
    }

    const territory = parsedDictionaries.territories.find(territory => territory.value === activity.territory);
    if (territory) {
        activity.territory = territory.name;
    }

    const priority = parsedDictionaries.priorities.find(priority => priority.value === activity.priority);
    if (priority) {
        activity.priority = priority.name;
    }

    const state = parsedDictionaries.priorities.find(state => state.value === activity.state);
    if (state) {
        activity.state = state.name;
    }

    const type = parsedDictionaries.types.find(type => type.value === activity.type);
    if (type) {
        activity.type = type.name;
    }

    return activity;
}
