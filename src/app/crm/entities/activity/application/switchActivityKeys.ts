import {IActivity} from '../domain/IActivity';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {getDictionaryById} from '../../../../shared/entities/dictionary/application/getDictionaryById';

export function switchActivityKeys(activity: IActivity, dictionaries: IDictionaryType[], users: ICRMUser[]) {
    const parsedDictionaries = {
        cars: getDictionaryById(dictionaries, 'CAR').values,
        subCars: getDictionaryById(dictionaries, 'SUBCAR').values,
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        priorities: getDictionaryById(dictionaries, 'PRIORIDAD').values,
        types: getDictionaryById(dictionaries, 'TIPOACTIVIDAD').values,
        states: getDictionaryById(dictionaries, 'ESTADOACTIVIDAD').values,
    };

    const user = users.filter(user => user.email === activity.idUser)[0];
    if (user) {
        activity.idUser = user.id;
    }

    const supportBosonit = users.filter(user => user.email === activity.supportBosonit)[0];
    if (supportBosonit) {
        activity.idUser = supportBosonit.id;
    }

    const car = parsedDictionaries.cars.filter(car => car.name === activity.car)[0];
    if (car) {
        activity.car = car.value;
    }

    const subCar = parsedDictionaries.subCars.filter(subCar => subCar.name === activity.subCar)[0];
    if (subCar) {
        activity.subCar = subCar.value;
    }

    const territory = parsedDictionaries.territories.filter(territory => territory.name === activity.territory)[0];
    if (territory) {
        activity.territory = territory.value;
    }

    const priority = parsedDictionaries.priorities.filter(priority => priority.name === activity.priority)[0];
    if (priority) {
        activity.priority = priority.value;
    }

    const state = parsedDictionaries.priorities.filter(state => state.name === activity.state)[0];
    if (state) {
        activity.state = state.value;
    }

    const type = parsedDictionaries.types.filter(type => type.name === activity.type)[0];
    if (type) {
        activity.type = type.value;
    }

    if (activity.warningDate && activity.warningDate != '') {
        activity.warningDate = new Date(activity.warningDate).toISOString();
    }

    return activity
}
