import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {INote} from '../../note/domain/INote';
import {NoteMapper} from '../../note/infrastructure/note-mapper';
import {AbstractActivityService} from '../infrastructure/abstract-activity-service';
import {ActivityMapper} from '../infrastructure/activity-mapper';
import {IActivity} from '../domain/IActivity';

export function addNote(note: INote, activityId: string, _service: AbstractActivityService): Observable<IActivity> {
    const noteMapper: NoteMapper = new NoteMapper();
    const activityMapper: ActivityMapper = new ActivityMapper();
    return _service.addNote(noteMapper.mapFrom(note), activityId)
        .pipe(map(activityMapper.mapTo));
}
