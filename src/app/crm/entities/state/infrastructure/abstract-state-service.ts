import {Observable} from 'rxjs';
import {IState} from '../domain/IState';

export abstract class AbstractStateService {
    abstract getList(params: any): Observable<IState[]>;

    abstract create(state: IState): Observable<IState>;
}
