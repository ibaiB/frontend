import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class StateMapper {
    mapTo(params: any) {
        const {
            id,
            nombre,
            orden
        } = params;

        return {
            id: id,
            name: nombre,
            order: orden
        };
    }
}
