export interface IState {
    id?: string;
    name: string;
    order: number;
}
