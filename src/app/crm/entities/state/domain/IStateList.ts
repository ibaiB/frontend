import {IWebState} from './IWebState';

export interface IStateList {
    content: IWebState[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
