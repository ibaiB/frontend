import {AbstractStateService} from '../infrastructure/abstract-state-service';

export function getStates(params: any, service: AbstractStateService) {
    return service.getList(params);
}
