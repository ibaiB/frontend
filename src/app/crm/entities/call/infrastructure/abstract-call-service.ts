import {Observable} from 'rxjs';
import {ICall} from '../domain/ICall';

export abstract class AbstractCallService {
    abstract delete(id: string);

    abstract update(call: ICall): Observable<ICall>;
}
