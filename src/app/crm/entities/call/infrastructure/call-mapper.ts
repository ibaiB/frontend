export class CallMapper {
    mapTo(params: any) {
        const {
            id,
            descripcion,
            fecha,
            resultado
        } = params;

        return {
            id: id,
            description: descripcion,
            date: fecha,
            result: resultado
        };
    }
}
