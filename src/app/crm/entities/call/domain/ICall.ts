export interface ICall {
    id?: string;
    description: string;
    date: Date;
    result: string;
}
