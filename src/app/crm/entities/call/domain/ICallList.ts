import {ICall} from './ICall';

export interface ICallList {
    content: ICall[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
