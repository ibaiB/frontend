import {IWebNote} from '../domain/IWebNote';
import {IList} from '../../../../shared/generics/IList';
import {INote} from '../domain/INote';
import {CommentMapper} from '../../comment/infrastructure/comment-mapper';

export class NoteMapper {

    mapTo(params: any) {
        const commentaryMapper = new CommentMapper();
        const {
            descripcion,
            fecha,
            nombre,
            id,
            comentarios,

        } = params;


        return {
            description: descripcion,
            date: fecha,
            name: nombre,
            id,
            comments: comentarios ? comentarios.map(commentaryMapper.mapTo) : []
        };
    }

    mapFrom(params: any): IWebNote {
        const commentaryMapper = new CommentMapper();

        const {
            description,
            date,
            name,
            id,
            comments
        } = params;

        return {
            descripcion: description,
            fecha: date,
            nombre: name,
            id: id,
            comentarios: comments ? comments.map(commentaryMapper.mapFrom) : []
        };
    }

    mapList(list: IList<any>): IList<INote> {
        const {
            content,
            numberOfElements,
            totalElements,
            totalPages
        } = list;

        return {
            content: content.map(this.mapTo),
            numberOfElements,
            totalElements,
            totalPages
        };
    }
}
