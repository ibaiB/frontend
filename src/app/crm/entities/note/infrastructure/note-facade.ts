//classes
import {Injectable} from '@angular/core';
import {IComment} from '../../comment/domain/IComment';
import {INote} from '../domain/INote';
import {Observable} from 'rxjs';

//services
import {NoteService} from '../../../services/note.service';

//functions
import {addNote as addAccountNote} from '../../account/application/addNote';
import {addNote as addContactNote} from '../../contact/application/addNote';
import {addNote as addOpportunityNote} from '../../opportunity/application/addNote';
import {addNote as addActivityNote} from '../../activity/application/addNote';
import {createComment} from '../application/createComment';
import {modifyNote} from '../application/modifyNote';
import {getNote} from '../application/getNote';
import {getNotes} from '../application/getNotes';
import {IList} from 'src/app/shared/generics/IList';
import {deleteNote} from '../application/deleteNote';
import {ActivityService} from 'src/app/crm/services/activity.service';
import {OpportunityService} from 'src/app/crm/services/opportunity.service';
import {ContactService} from 'src/app/crm/services/contact.service';
import {AccountService} from 'src/app/crm/services/account.service';
import {AccountMapper} from '../../account/infrastructure/account-mapper';
import {ContactMapper} from '../../contact/infrastructure/contact-mapper';
import {OpportunityMapper} from '../../opportunity/infrastructure/opportunity-mapper';
import {ActivityMapper} from '../../activity/infrastructure/activity-mapper';
import {IAccount} from '../../account/domain/IAccount';
import {IActivity} from '../../activity/domain/IActivity';
import {IOpportunity} from '../../opportunity/domain/IOpportunity';
import {IContact} from '../../contact/domain/IContact';

@Injectable({
    providedIn: 'any'
})
export class NoteFacade {
    constructor(
        private _noteService: NoteService,
        private _accountService: AccountService,
        private _contactService: ContactService,
        private _opportunityService: OpportunityService,
        private _activityService: ActivityService,
        private _accountMapper: AccountMapper,
        private _contactMapper: ContactMapper,
        private _opportunityMapper: OpportunityMapper,
        private _activityMapper: ActivityMapper
    ) {

    }

    addCommentToNote(comment: IComment, noteId: string): Observable<INote> {
        return createComment(comment, noteId, this._noteService);
    }


    createNote(note: INote, id: string, type: string): Observable<IAccount | IContact | IActivity | IOpportunity> {
        let returned: Observable<IAccount | IContact | IActivity | IOpportunity>;

        switch (type) {
            case 'Account': {
                returned = addAccountNote(note, id, this._accountService);
                break;
            }
            //TODO: fix error with this (constraints error)
            case 'Contact': {
                returned = addContactNote(note, id, this._contactService);
                break;
            }
            //TODO: test createNote in a Opportunity
            case 'Opportunity': {
                returned = addOpportunityNote(note, id, this._opportunityService);
                break;
            }
            //TODO: test create Note in an Activity
            case 'Activity': {
                returned = addActivityNote(note, id, this._activityService);
                break;
            }
        }

        return returned;
    }

    // TODO:test modifyNote
    modifyNote(note: INote): Observable<INote> {
        return modifyNote(note, this._noteService);
    }

    //TODO:test getNote, it depends of mapper's bug
    getNote(noteId: string): Observable<INote> {
        return getNote(noteId, this._noteService);
    }

    //TODO:test searchNotes
    searchNotes(query: any): Observable<IList<INote>> {
        return getNotes(query, this._noteService);
    }

    //TODO:test deleteNote
    deleteNote(noteId: string) {
        return deleteNote(noteId, this._noteService);
    }
}
