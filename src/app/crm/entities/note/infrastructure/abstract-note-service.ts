import {Observable} from 'rxjs';
import {IWebNote} from '../domain/IWebNote';
import {IList} from '../../../../shared/generics/IList';
import {IWebComment} from '../../comment/domain/IWebComment';

export abstract class AbstractNoteService {

    //this haven't sense, account have this function already
    //abstract create(note: INote): Observable<INote>;
    //Technically this is an update
    abstract addComment(comment: IWebComment, noteId: string): Observable<IWebNote>;

    abstract get(id: string): Observable<IWebNote>;

    abstract search(query: any): Observable<IList<IWebNote>>;

    abstract delete(id: string);

    abstract update(id: string, note: IWebNote);
}
