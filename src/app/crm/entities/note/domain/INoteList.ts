import {INote} from './INote';

export interface INoteList {
    content: INote[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
