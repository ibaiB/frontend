import {IComment} from '../../comment/domain/IComment';

export interface INote {
    name: string;
    description: string;
    date: string;
    id?: string,
    comments?: IComment[]
}
