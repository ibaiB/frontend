import {IComment} from '../../comment/domain/IComment';

/**
 * model to send note attributes to backend
 */
export interface IWebNote {

    descripcion: string;
    fecha: string;
    nombre: string;
    id?: string;
    comentarios?: IComment[];
}


