import {AbstractNoteService} from '../infrastructure/abstract-note-service';
import {NoteMapper} from '../infrastructure/note-mapper';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {INote} from '../domain/INote';

export function getNote(id: string, service: AbstractNoteService): Observable<INote> {
    const noteMapper: NoteMapper = new NoteMapper();

    return service.get(id).pipe(map(noteMapper.mapTo));
}
