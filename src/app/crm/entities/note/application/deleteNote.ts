import {AbstractNoteService} from '../infrastructure/abstract-note-service';

export function deleteNote(idNote: string, service: AbstractNoteService) {
    return service.delete(idNote);
}
