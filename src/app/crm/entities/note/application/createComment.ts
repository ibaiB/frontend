import {IComment} from '../../comment/domain/IComment';
import {AbstractNoteService} from '../infrastructure/abstract-note-service';
import {CommentMapper} from '../../comment/infrastructure/comment-mapper';
import {NoteMapper} from '../infrastructure/note-mapper';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {INote} from '../domain/INote';

export function createComment(comment: IComment, noteId: string, service: AbstractNoteService): Observable<INote> {
    const commentMapper: CommentMapper = new CommentMapper();
    const noteMapper: NoteMapper = new NoteMapper();

    return service
        .addComment(commentMapper.mapFrom(comment), noteId)
        .pipe(
            map(noteMapper.mapTo),
        );
}
