import {AbstractNoteService} from '../infrastructure/abstract-note-service';
import {NoteMapper} from '../infrastructure/note-mapper';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {INote} from '../domain/INote';
import {IList} from '../../../../shared/generics/IList';

export function getNotes(params: any, service: AbstractNoteService): Observable<IList<INote>> {
    const noteMapper: NoteMapper = new NoteMapper();

    return service
        .search(params)
        .pipe(map(noteMapper.mapList));
}
