import {INote} from '../domain/INote';
import {AbstractNoteService} from '../infrastructure/abstract-note-service';
import {NoteMapper} from '../infrastructure/note-mapper';
import {Observable} from 'rxjs';

export function modifyNote(note: INote, service: AbstractNoteService): Observable<INote> {
    const noteMapper: NoteMapper = new NoteMapper();

    return service
        .update(note.id, noteMapper.mapFrom(note));
}
