import {Observable} from 'rxjs';
import {ITrackingList} from '../domain/ITrackingList';

export abstract class AbstractTrackingService {
    abstract getList(params: any): Observable<ITrackingList>;
}
