export interface ITrackingList {
    content: [];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
