import {AbstractTrackingService} from '../infrastructure/abstract-tracking-service';

export function getTracking(params: any, service: AbstractTrackingService) {
    return service.getList(params);
}
