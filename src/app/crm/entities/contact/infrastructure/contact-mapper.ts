import {Injectable} from '@angular/core';
import { AccountMapper } from '../../account/infrastructure/account-mapper';

@Injectable({
    providedIn: 'any',
})
export class ContactMapper {
    mapTo(params: any) {
        const accountMapper = new AccountMapper();
        const {
            actividades,
            nombre,
            apellido,
            apellido2,
            idCar,
            cargo,
            ciudad,
            codigoPostal,
            comunidadAutonoma,
            correo,
            correos,
            direccion,
            estadoLead,
            etapaCicloVida,
            fechaCreacion,
            fechaUltimoContactado,
            fuenteOrigen,
            id,
            idCuenta,
            idUsuario,
            usuario,
            industria,
            llamadas,
            notas,
            oportunidades,
            pais,
            provincia,
            telefono1,
            telefono2,
            tipo,
            area,
            nivelRelacion,
            idSubcar,
            idTerritorio,
            cuenta,
            linkImagen
        } = params;

        return {
            activities: actividades,
            name: nombre,
            surname: apellido,
            surname2: apellido2,
            car: idCar,
            position: cargo,
            city: ciudad,
            postCode: codigoPostal,
            region: comunidadAutonoma,
            email: correo,
            emails: correos,
            address: direccion,
            leadState: estadoLead,
            lifeCycleState: etapaCicloVida,
            created: fechaCreacion,
            lastContacted: fechaUltimoContactado,
            source: fuenteOrigen,
            id: id,
            accountId: idCuenta,
            userId: idUsuario,
            user:usuario,
            industry: industria,
            calls: llamadas,
            notes: notas,
            opportunities: oportunidades,
            country: pais,
            province: provincia,
            phone1: telefono1,
            phone2: telefono2,
            type: tipo,
            area: area,
            relationshipLevel: nivelRelacion,
            territory: idTerritorio,
            subCar: idSubcar,
            account: cuenta ? accountMapper.mapTo(cuenta) : null,
            idLinkImage:linkImagen
        };
    }
}
