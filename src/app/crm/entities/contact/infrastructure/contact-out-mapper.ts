import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class ContactOutMapper {
    mapFrom(params: any) {
        const {
            name,
            surname,
            position,
            area,
            userId,
            user,
            relationshipLevel,
            car,
            subCar,
            email,
            phone1,
            accountId,
            territory,

            //TODO: not used in create maybe in edit
            id,
            surname2,
            city,
            postCode,
            region,
            address,
            leadState,
            lifeCycleState,
            created,
            lastContacted,
            source,
            industry,
            country,
            province,
            phone2,
            type,
            calls,
            notes,
            opportunities,
            activities,
            idLinkImage
        } = params;

        return {
            nombre: name,
            apellido: surname,
            cargo: position,
            area: area,
            idUsuario: userId,
            nivelRelacion: relationshipLevel,
            idCar: car,
            idSubcar: subCar,
            email: email,
            telefono1: phone1,
            idCuenta: accountId,
            idTerritorio: territory,

            //TODO: this are not used at create maybe at edit
            id: id,
            apellido2: surname2,
            ciudad: city,
            codigoPostal: postCode,
            comunidadAutonoma: region,
            correo: email,
            direccion: address,
            estadoLead: leadState,
            etapaCicloVida: lifeCycleState,
            fechaCreacion: created,
            fechaUltimoContactado: lastContacted,
            fuenteOrigen: source,
            industria: industry,
            pais: country,
            provincia: province,
            telefono2: phone2,
            tipo: type,
            llamadas: calls,
            notas: notes,
            actividades: activities,
            oportunidades: opportunities,
            idLinkImagen:idLinkImage,
            usuario:user
        };
    }
}
