import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class ContactEditDataMapper {
    mapTo(params: any) {
        const {
            myCrm,
            contact,
            crmUsers,
            cars,
            subCars,
            territories,
            relationshipLevels
        } = params;

        return {
            myCrm: myCrm,
            contact: contact,
            crmUsers: crmUsers,
            cars: cars,
            subCars: subCars,
            territories: territories,
            relationshipLevels: relationshipLevels
        };
    }
}
