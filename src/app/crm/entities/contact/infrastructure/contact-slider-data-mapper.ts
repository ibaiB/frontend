import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class ContactSliderDataMapper {
    mapTo(params: any) {
        const {
            myCrm,
            contact,
            contactAccount,
            crmUsers,
            cars,
            subCars,
            activityTypes,
            activityStates,
            priorities,
            opportunityStates,
            territories,
            opportunityProbabilities,
            opportunityRejectionReasons,
            categories
        } = params;

        return {
            myCrm: myCrm,
            contact: contact,
            contactAccount: contactAccount,
            crmUsers: crmUsers,
            cars: cars,
            subCars: subCars,
            activityTypes: activityTypes,
            activityStates: activityStates,
            priorities: priorities,
            opportunityStates: opportunityStates,
            territories: territories,
            opportunityProbabilities: opportunityProbabilities,
            opportunityRejectionReasons: opportunityRejectionReasons,
            categories: categories
        };
    }
}
