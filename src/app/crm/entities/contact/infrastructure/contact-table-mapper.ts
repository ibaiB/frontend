export class ContactTableMapper {
    mapTo(params: any) {
        const {id, name, surname, position, area, userId, car, subCar, territory, relationshipLevel, phone1, email, accountId} = params;

        return {
            id: id,
            name: name,
            surname: surname,
            charge: position,
            area: area,
            user: userId,
            car: car,
            subCar: subCar,
            territory: territory,
            relationshipLevel: relationshipLevel,
            phone1: phone1,
            email: email,
            account: accountId
        };
    }
}
