import {IContactOut} from './../domain/IContactOut';
import {ICall} from './../../call/domain/ICall';
import {Observable} from 'rxjs';
import {IEmail} from '../../email/domain/IEmail';
import {IContact} from '../domain/IContact';
import {IOpportunityOut} from '../../opportunity/domain/IOpportunityOut';
import {IWebActivity} from '../../activity/domain/IWebActivity';
import {IWebNote} from '../../note/domain/IWebNote';
import {IList} from 'src/app/shared/generics/IList';

export abstract class AbstractContactService {
    abstract getList(params: any): Observable<IContact[]>;

    abstract get(id: string): Observable<IContact>;

    abstract create(contact: IContactOut): Observable<IContact>;

    abstract update(contact: IContactOut): Observable<IContact>;

    abstract addActivity(activity: IWebActivity, contactId: string): Observable<IContact>;

    abstract addEmail(email: IEmail, contactId: string): Observable<IContact>;

    abstract addNote(note: IWebNote, contactId: string): Observable<IContact>;

    abstract addOpportunity(opportunity: IOpportunityOut, contactId: string): Observable<IContact>;

    abstract addCall(call: ICall, contactId: string): Observable<IContact>;

    //@TODO this endpoint could de merged with getAccounts
    abstract search(params: any): Observable<IList<IContact>>;
}
