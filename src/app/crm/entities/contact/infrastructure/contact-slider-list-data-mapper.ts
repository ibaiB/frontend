import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class ContactSliderListDataMapper {
    mapTo(params: any) {
        const {
            myCrm,
            account,
            relationshipLevels,
            cars,
            subCars,
            territories
        } = params;

        return {
            myCrm: myCrm,
            account: account,
            relationshipLevels: relationshipLevels,
            cars: cars,
            subCars: subCars,
            territories: territories
        };
    }
}
