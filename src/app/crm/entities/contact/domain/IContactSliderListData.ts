import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IAccount} from '../../account/domain/IAccount';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IContactSliderListData {
    myCrm: ICRMUser;
    account: IAccount;
    relationshipLevels: IDictionaryValue[];
    cars: IList<ICar>;
    subCars: IList<ISubcar>;
    territories: IDictionaryValue[];
}
