export interface IWebContact {
    actividades: any;
    apellido: string;
    apellido2: string;
    idCar: string;
    idSubcar: string;
    cargo: string;
    ciudad: string;
    codigoPostal: string;
    comunidadAutonoma: string;
    correo: string;
    correos: any;
    cuenta: any;
    direccion: string;
    estadoLead: string;
    etapaCicloVida: string;
    fechaCreacion: string; //Date
    fechaUltimoContactado: string; //Date
    fuenteOrigen: string;
    id: string;
    idCuenta: string;
    idUsuario: string;
    usuario:any;
    industria: string;
    llamadas: any;
    nombre: string;
    notas: any;
    oportunidades: any;
    pais: string;
    provincia: string;
    telefono1: string;
    telefono2: string;
    tipo: string;
    idTerritorio: string;
    area: string;
}
