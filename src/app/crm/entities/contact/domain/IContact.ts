import {IActivity} from '../../activity/domain/IActivity';
import {IEmail} from '../../email/domain/IEmail';
import {ICall} from '../../call/domain/ICall';
import {INote} from '../../note/domain/INote';
import {IOpportunity} from '../../opportunity/domain/IOpportunity';
import {IAccount} from '../../account/domain/IAccount';
import { ICRMUser } from '../../user/domain/ICRMUser';

export interface IContact {
    activities: IActivity[],
    name: string,
    surname: string,
    surname2: string,
    car: string,
    subCar: string,
    position: string,
    city: string,
    postCode: string,
    region: string,
    email: string,
    emails: IEmail[],
    address: string,
    leadState: string,
    lifeCycleState: string,
    created: Date,
    lastContacted: Date,
    source: string,
    id?: string,
    accountId: string,
    account: IAccount,
    userId: string,
    user: ICRMUser,
    industry: string,
    calls: ICall[],
    notes: INote[],
    opportunities: IOpportunity[],
    country: string,
    province: string,
    phone1: string,
    phone2: string,
    type: string,
    area: string,
    relationshipLevel: string,
    territory: string,
    idLinkImage:string
}
