import {IWebContact} from './IWebContact';

export interface IWebContactList {
    content: IWebContact[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
