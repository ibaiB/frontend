import {IContactTableData} from './IContactTableData';

export interface IContactTableResolved {
    tableData: IContactTableData;
    error?: any;
}
