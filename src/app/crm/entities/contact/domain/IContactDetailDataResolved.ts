import {IContactDetailData} from './IContactDetailData';

export interface IContactDetailDataResolved {
    detailData: IContactDetailData;
    error?: any;
}
