import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IContact} from './IContact';
import {IAccount} from '../../account/domain/IAccount';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IContactSliderData {
    myCrm: ICRMUser;
    contact: IContact;
    contactAccount: IAccount;
    crmUsers: ICRMUser[];
    cars: IList<ICar>;
    subCars: IList<ISubcar>;
    activityTypes: IDictionaryValue[];
    activityStates: IDictionaryValue[];
    priorities: IDictionaryValue[];
    opportunityStates: IDictionaryValue[];
    territories: IDictionaryValue[];
    opportunityProbabilities: IDictionaryValue[];
    opportunityRejectionReasons: IDictionaryValue[];
    categories: IDictionaryValue[]
}
