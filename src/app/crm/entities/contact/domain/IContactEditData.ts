import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IContact} from './IContact';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IContactEditData {
    myCrm: ICRMUser,
    contact: IContact,
    crmUsers: ICRMUser[],
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    territories: IDictionaryValue[],
    relationshipLevels: IDictionaryValue[]
}
