import {IUserInfo} from '../../../../auth/session/domain/IUserInfo';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IContact} from '../../contact/domain/IContact';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IContactTableData {
    myUser: IUserInfo;
    myCrm: ICRMUser;
    users: ICRMUser[];
    cars: IList<ICar>;
    subCars: IList<ISubcar>;
    territories: IDictionaryValue[];
    relationshipLevels: IDictionaryValue[];
    contacts: IList<IContact>;
    filtersValues:any;
    tableFiltersConfig: ITableFiltersConfig;
}
