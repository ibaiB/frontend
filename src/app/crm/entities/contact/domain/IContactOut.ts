export interface IContactOut {
    id: string;
    apellido: string;
    apellido2: string;
    idCar: string;
    idSubcar: string;
    cargo: string;
    ciudad: string;
    codigoPostal: string;
    comunidadAutonoma: string;
    correo: string;
    direccion: string;
    estadoLead: string;
    etapaCicloVida: string;
    fechaCreacion: string; //Date
    fechaUltimoContactado: string; //Date
    fuenteOrigen: string;
    idCuenta: string;
    idUsuario: string;
    industria: string;
    nombre: string;
    pais: string;
    provincia: string;
    telefono1: string;
    telefono2: string;
    tipo: string;
    idTerritorio: string;
    nivelRelacion: string;
    area: string;
    idLinkImagen:string
}
