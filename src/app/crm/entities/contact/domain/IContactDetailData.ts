import {IContact} from './IContact';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICar} from '../../../../internal/car/domain/ICar';
import {IList} from '../../../../shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IContactDetailData {
    myCrm: ICRMUser,
    crmUsers: ICRMUser[],
    contact: IContact,
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    categories: IDictionaryValue[],
    priorities: IDictionaryValue[],
    territories: IDictionaryValue[],
    relationshipLevels: IDictionaryValue[],
    activityTypes: IDictionaryValue[],
    activityStates: IDictionaryValue[],
    opportunityRejectionReasons: IDictionaryValue[],
    opportunityStates: IDictionaryValue[],
    opportunityProbabilities: IDictionaryValue[]
}
