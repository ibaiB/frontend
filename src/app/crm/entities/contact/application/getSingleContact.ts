import {AbstractContactService} from '../infrastructure/abstract-contact-service';

export function getSingleContact(id: string, service: AbstractContactService) {
    return service.get(id);
}
