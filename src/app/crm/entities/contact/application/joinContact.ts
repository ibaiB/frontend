import {IContact} from '../domain/IContact';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {joinOpportunity} from '../../opportunity/application/joinOpportunity';
import {joinActivity} from '../../activity/application/joinActivity';
import {joinAccount} from '../../account/application/joinAccount';
import {getDictionaryById} from '../../../../shared/entities/dictionary/application/getDictionaryById';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export function joinContact(contact: IContact, dictionaries: IDictionaryType[], users: ICRMUser[], cars: IList<ICar>, subCars: IList<ISubcar>) {

    const parsedDictionaries = {
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        relationshipLevels: getDictionaryById(dictionaries, 'NIVEL_RELACION').values,
    };

    const user = users.filter(user => user.id === contact.userId)[0];
    if (user) {
        contact.userId = user.email;
    }

    const car = cars.content.find(car => car.id === contact.car);
    if (car) {
        contact.car = car.name;
    }

    const subCar = subCars.content.find(subCar => subCar.id === contact.subCar);
    if (subCar) {
        contact.subCar = subCar.name;
    }

    const territory = parsedDictionaries.territories.find(territory => territory.value === contact.territory);
    if (territory) {
        contact.territory = territory.name;
    }

    const relationshipLevel = parsedDictionaries.relationshipLevels.find(relationshipLevel => relationshipLevel.value === contact.relationshipLevel);
    if (relationshipLevel) {
        contact.relationshipLevel = relationshipLevel.name;
    }

    if (contact.opportunities) {
        contact.opportunities = contact.opportunities.map(opportunity => joinOpportunity(opportunity, dictionaries, users, cars, subCars));
    }
    if (contact.activities) {
        contact.activities = contact.activities.map(activity => joinActivity(activity, dictionaries, users, cars, subCars));
    }
    if (contact.account) {
        contact.account = joinAccount(contact.account, dictionaries, users, cars, subCars);
    }

    return contact;
}
