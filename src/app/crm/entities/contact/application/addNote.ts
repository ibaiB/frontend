import {Observable} from 'rxjs';
import {INote} from '../../note/domain/INote';
import {NoteMapper} from '../../note/infrastructure/note-mapper';
import {AbstractContactService} from '../infrastructure/abstract-contact-service';
import {IContact} from '../domain/IContact';

export function addNote(note: INote, contactId: string, service: AbstractContactService): Observable<IContact> {
    const noteMapper: NoteMapper = new NoteMapper();
    return service.addNote(noteMapper.mapFrom(note), contactId);
}
