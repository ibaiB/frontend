import {AbstractContactService} from '../infrastructure/abstract-contact-service';


export function addContactActivity(activity: any, contactId: string, service: AbstractContactService) {
    return service.addActivity(activity, contactId);
}
