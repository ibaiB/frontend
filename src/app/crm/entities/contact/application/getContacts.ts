import {AbstractContactService} from '../infrastructure/abstract-contact-service';

export function getContacts(params: any, service: AbstractContactService) {
    return service.getList(params);
}
