import {IContactOut} from './../domain/IContactOut';
import {AbstractContactService} from '../infrastructure/abstract-contact-service';

export function editContact(contact: IContactOut, service: AbstractContactService) {
    return service.update(contact);
}
