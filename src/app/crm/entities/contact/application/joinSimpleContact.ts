import {ICar} from 'src/app/internal/car/domain/ICar';
import {getDictionaryById} from 'src/app/shared/entities/dictionary/application/getDictionaryById';
import {IDictionaryType} from 'src/app/shared/entities/dictionary/domain/IDictionaryType';
import {IList} from 'src/app/shared/generics/IList';
import {ICRMUser} from './../../user/domain/ICRMUser';
import {IContact} from './../domain/IContact';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export function joinSimpleContact(contact: IContact, dictionaries: IDictionaryType[], users: ICRMUser[], cars: IList<ICar>, subCars: IList<ISubcar>) {

    const parsedDictionaries = {
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        relationshipLevels: getDictionaryById(dictionaries, 'NIVEL_RELACION').values,
    };

    const user = users.find(user => user.id === contact.userId);
    if (user) {
        contact.userId = user.email;
    }

    const car = cars.content.find(car => car.id === contact.car);
    if (car) {
        contact.car = car.name;
    }

    const subCar = subCars.content.find(subCar => subCar.id === contact.subCar);
    if (subCar) {
        contact.subCar = subCar.name;
    }

    const territory = parsedDictionaries.territories.find(territory => territory.value === contact.territory);
    if (territory) {
        contact.territory = territory.name;
    }

    const relationshipLevel =
        parsedDictionaries.relationshipLevels.find(relationshipLevel => relationshipLevel.value === contact.relationshipLevel);
    if (relationshipLevel) {
        contact.relationshipLevel = relationshipLevel.name;
    }

    return contact;
}
