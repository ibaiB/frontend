import {IContact} from '../domain/IContact';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {getDictionaryById} from '../../../../shared/entities/dictionary/application/getDictionaryById';

export function switchContactKeys(contact: IContact, dictionaries: IDictionaryType[], users: ICRMUser[]) {
    const parsedDictionaries = {
        cars: getDictionaryById(dictionaries, 'CAR').values,
        subCars: getDictionaryById(dictionaries, 'SUBCAR').values,
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        relationshipLevels: getDictionaryById(dictionaries, 'NIVEL_RELACION').values,
    };

    const user = users.filter(user => user.email === contact.userId)[0];
    if (user) {
        contact.userId = user.id;
    }

    const car = parsedDictionaries.cars.filter(car => car.name === contact.car)[0];
    if (car) {
        contact.car = car.value;
    }

    const subCar = parsedDictionaries.subCars.filter(subCar => subCar.name === contact.subCar)[0];
    if (subCar) {
        contact.subCar = subCar.value;
    }

    const territory = parsedDictionaries.territories.filter(territory => territory.name === contact.territory)[0];
    if (territory) {
        contact.territory = territory.value;
    }

    const relationshipLevel = parsedDictionaries.relationshipLevels.filter(relationshipLevel => relationshipLevel.name === contact.relationshipLevel)[0];
    if (relationshipLevel) {
        contact.relationshipLevel = relationshipLevel.value;
    }

    return contact;
}
