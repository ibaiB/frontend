import {IContactOut} from './../domain/IContactOut';
import {AbstractContactService} from '../infrastructure/abstract-contact-service';

export function createContact(contact: IContactOut, service: AbstractContactService) {
    return service.create(contact);
}
