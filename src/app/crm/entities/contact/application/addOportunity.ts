import {AbstractContactService} from '../infrastructure/abstract-contact-service';
import {IOpportunityOut} from '../../opportunity/domain/IOpportunityOut';

export function addContactOpportunity(opportunity: IOpportunityOut, contactId: string, service: AbstractContactService) {
    return service.addOpportunity(opportunity, contactId);
}
