export interface IEmail {
    description: string,
    created: Date,
    id?: string,
    contactId?: string,
    name: string
}
