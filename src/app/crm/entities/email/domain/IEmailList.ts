import {IEmail} from './IEmail';

export interface IEmailList {
    content: IEmail[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
