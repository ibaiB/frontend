import {IEmail} from '../domain/IEmail';
import {Observable} from 'rxjs';

export abstract class AbstractEmailService {
    abstract update(email: IEmail): Observable<IEmail>;

    abstract delete(id: string);
}
