export class EmailMapper {
    mapTo(params: any) {
        const {
            id,
            idContacto,
            nombre,
            fecha,
            descripcion
        } = params;

        return {
            id: id,
            contactId: idContacto,
            name: nombre,
            created: fecha,
            description: descripcion
        };
    }
}
