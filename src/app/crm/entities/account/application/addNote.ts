import {Observable} from 'rxjs';
import {INote} from '../../note/domain/INote';
import {NoteMapper} from '../../note/infrastructure/note-mapper';
import {AbstractAccountService} from '../infrastructure/abstract-account-service';
import {IAccount} from '../domain/IAccount';

export function addNote(note: INote, idAccount: string, _service: AbstractAccountService): Observable<IAccount> {
    const noteMapper: NoteMapper = new NoteMapper();
    return _service.addNote(noteMapper.mapFrom(note), idAccount);
}
