import {AbstractAccountService} from '../infrastructure/abstract-account-service';
import {IOpportunityOut} from '../../opportunity/domain/IOpportunityOut';

export function addAccountOpportunity(opportunity: IOpportunityOut, accountId: string, service: AbstractAccountService) {
    return service.addOpportunity(opportunity, accountId);
}
