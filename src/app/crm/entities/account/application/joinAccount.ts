import {IAccount} from '../domain/IAccount';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {joinOpportunity} from '../../opportunity/application/joinOpportunity';
import {joinActivity} from '../../activity/application/joinActivity';
import {joinContact} from '../../contact/application/joinContact';
import {getDictionaryById} from '../../../../shared/entities/dictionary/application/getDictionaryById';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export function joinAccount(account: IAccount, dictionaries: IDictionaryType[], users: ICRMUser[], cars: IList<ICar>, subcars: IList<ISubcar>) {

    const parsedDictionaries = {
        categories: getDictionaryById(dictionaries, 'CATEGORIA').values,
        priorities: getDictionaryById(dictionaries, 'PRIORIDAD').values,
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        sectors: getDictionaryById(dictionaries, 'SECTOR').values,
    };

    const car = cars.content.find(car => car.id === account.car);
    if (car) {
        account.car = car.name;
    }

    const subCar = subcars.content.find(subCar => subCar.id === account.subCar);
    if (subCar) {
        account.subCar = subCar.name;
    }

    const territory = parsedDictionaries.territories.find(territory => territory.value === account.territory);
    if (territory) {
        account.territory = territory.name;
    }

    const category = parsedDictionaries.categories.find(category => category.value === account.category);
    if (category) {
        account.category = category.name;
    }

    const sector = parsedDictionaries.sectors.find(sector => sector.value === account.sector);
    if (sector) {
        account.sector = sector.name;
    }

    if (account.opportunities) {
        account.opportunities = account.opportunities.map(opportunity => joinOpportunity(opportunity, dictionaries, users, cars, subcars));
    }
    if (account.activities) {
        account.activities = account.activities.map(activity => joinActivity(activity, dictionaries, users, cars, subcars));
    }
    if (account.contacts) {
        account.contacts = account.contacts.map(contact => joinContact(contact, dictionaries, users, cars, subcars));
    }

    return account;
}
