import {AbstractAccountService} from '../infrastructure/abstract-account-service';

export function editAccount(account: any, service: AbstractAccountService) {
    return service.update(account);
}
