import {ICar} from 'src/app/internal/car/domain/ICar';
import {getDictionaryById} from 'src/app/shared/entities/dictionary/application/getDictionaryById';
import {IDictionaryType} from 'src/app/shared/entities/dictionary/domain/IDictionaryType';
import {IList} from 'src/app/shared/generics/IList';
import {ICRMUser} from './../../user/domain/ICRMUser';
import {IAccount} from './../domain/IAccount';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export function joinSimpleAccount(account: IAccount, dictionaries: IDictionaryType[], users: ICRMUser[], cars: IList<ICar>, subCars: IList<ISubcar>) {

    const parsedDictionaries = {
        categories: getDictionaryById(dictionaries, 'CATEGORIA').values,
        priorities: getDictionaryById(dictionaries, 'PRIORIDAD').values,
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        sectors: getDictionaryById(dictionaries, 'SECTOR').values,
    };

    const user = users.find(user => user.id === account.userId);
    if (user) {
        account.userId = user.email;
    }

    const car = cars.content.find(car => car.id === account.car);
    if (car) {
        account.car = car.name;
    }

    const subCar = subCars.content.find(subCar => subCar.id === account.subCar);
    if (subCar) {
        account.subCar = subCar.name;
    }

    const territory = parsedDictionaries.territories.find(territory => territory.value === account.territory);
    if (territory) {
        account.territory = territory.name;
    }

    const category = parsedDictionaries.categories.find(category => category.value === account.category);
    if (category) {
        account.category = category.name;
    }

    const sector = parsedDictionaries.sectors.find(sector => sector.value === account.sector);
    if (sector) {
        account.sector = sector.name;
    }

    return account;
}
