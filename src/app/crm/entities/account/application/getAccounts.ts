import {AbstractAccountService} from '../infrastructure/abstract-account-service';

export function getAccounts(params: any, service: AbstractAccountService) {
    return service.getList(params);
}
