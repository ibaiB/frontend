import {IAccount} from '../domain/IAccount';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {getDictionaryById} from '../../../../shared/entities/dictionary/application/getDictionaryById';

export function switchAccountKeys(account: IAccount, dictionaries: IDictionaryType[], users: ICRMUser[]) {
    const parsedDictionaries = {
        cars: getDictionaryById(dictionaries, 'CAR').values,
        subCars: getDictionaryById(dictionaries, 'SUBCAR').values,
        categories: getDictionaryById(dictionaries, 'CATEGORIA').values,
        priorities: getDictionaryById(dictionaries, 'PRIORIDAD').values,
        territories: getDictionaryById(dictionaries, 'TERRITORIO').values,
        sectors: getDictionaryById(dictionaries, 'SECTOR').values,
    };

    const user = users.filter(user => user.email === account.userId)[0];
    if (user) {
        account.userId = user.id;
    }

    const car = parsedDictionaries.cars.filter(car => car.name === account.car)[0];
    if (car) {
        account.car = car.value;
    }

    const subCar = parsedDictionaries.subCars.filter(subCar => subCar.name === account.subCar)[0];
    if (subCar) {
        account.subCar = subCar.value;
    }

    const territory = parsedDictionaries.territories.filter(territory => territory.name === account.territory)[0];
    if (territory) {
        account.territory = territory.value;
    }

    const category = parsedDictionaries.categories.filter(category => category.name === account.category)[0];
    if (category) {
        account.category = category.value;
    }

    const sector = parsedDictionaries.sectors.filter(sector => sector.name === account.sector)[0];
    if (sector) {
        account.sector = sector.value;
    }

    return account;
}
