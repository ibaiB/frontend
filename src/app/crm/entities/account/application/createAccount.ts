import {AbstractAccountService} from '../infrastructure/abstract-account-service';
import {IAccountOut} from '../domain/IAccountOut';

export function createAccount(account: IAccountOut, service: AbstractAccountService) {
    return service.createAccount(account);
}
