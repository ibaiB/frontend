import {AbstractAccountService} from '../infrastructure/abstract-account-service';

export function addAccountActivity(activity: any, accountId: string, service: AbstractAccountService) {
    return service.addActivity(activity, accountId);
}
