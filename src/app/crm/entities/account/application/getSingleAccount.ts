import {AbstractAccountService} from '../infrastructure/abstract-account-service';

export function getSingleAccount(id: string, service: AbstractAccountService) {
    return service.get(id);
}
