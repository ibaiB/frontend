import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class AccountEditDataMapper {
    mapTo(params: any) {
        const {cars, myCrm, account, crmUsers, priorities, sectors, subCars, territories, categories} = params;
        return {
            myCrm: myCrm,
            account: account,
            crmUsers: crmUsers,
            cars: cars,
            subCars: subCars,
            territories: territories,
            sectors: sectors,
            categories: categories,
            priorities: priorities
        };
    }
}
