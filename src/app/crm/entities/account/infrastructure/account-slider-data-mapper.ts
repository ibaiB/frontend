import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class AccountSliderDataMapper {
    mapTo(params: any) {
        const {
            account,
            opportunityRejectionReasons,
            activityStates,
            activityTypes,
            cars,
            categories,
            crmUsers,
            myCrm,
            opportunityProbabilities,
            opportunityStates,
            priorities,
            relationshipLevels,
            sectors,
            subCars,
            territories
        } = params;

        return {
            account: account,
            opportunityRejectionReasons: opportunityRejectionReasons,
            activityStates: activityStates,
            activityTypes: activityTypes,
            cars: cars,
            categories: categories,
            crmUsers: crmUsers,
            myCrm: myCrm,
            opportunityProbabilities: opportunityProbabilities,
            opportunityStates: opportunityStates,
            priorities: priorities,
            relationshipLevels: relationshipLevels,
            sectors: sectors,
            subCars: subCars,
            territories: territories
        };
    }
}
