import {Observable} from 'rxjs';
import {IEmail} from '../../email/domain/IEmail';
import {IAccount} from '../domain/IAccount';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IAccountOut} from '../domain/IAccountOut';
import {IOpportunityOut} from '../../opportunity/domain/IOpportunityOut';
import {IWebActivity} from '../../activity/domain/IWebActivity';
import {IWebAccount} from '../domain/IWebAccount';
import {IWebNote} from '../../note/domain/IWebNote';
import {IList} from 'src/app/shared/generics/IList';

export abstract class AbstractAccountService {
    abstract getList(params: any): Observable<IAccount[]>;

    abstract get(id: string): Observable<IAccount>;

    abstract update(account: IWebAccount): Observable<IAccount>;

    abstract createAccount(account: IAccountOut): Observable<IAccount>;

    abstract addActivity(activity: IWebActivity, accountId: string): Observable<IAccount>;

    abstract addEmail(email: IEmail, accountId: string): Observable<IAccount>;

    abstract addNote(note: IWebNote, accountId: string): Observable<IAccount>;

    abstract addOpportunity(opportunity: IOpportunityOut, accountId: string): Observable<IAccount>;

    abstract addUser(user: ICRMUser, accountId: string): Observable<IAccount>;

    //@TODO this endpoint could de merged with getAccounts
    abstract search(params: any): Observable<IList<IAccount>>;
}
