import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class AccountMapper {
    mapTo(params: any) {
        const {
            actividadCNAE,
            actividadSIC,
            actividades,
            idCar,
            cif,
            ciudad,
            clasificacion,
            clienteExistente,
            codigoPostal,
            comunidadAutonoma,
            contactos,
            correoCuenta,
            correos,
            descripcion,
            direccion,
            dominio,
            ebitda,
            ejercicioBalance,
            estadoLead,
            etapaCicloVida,
            facturacion,
            fechaCreacion,
            fechaDescarga,
            fuenteOrigen,
            id,
            idCrm360,
            idUsuario,
            usuario,
            industria,
            nempleados,
            nombreComercial,
            nombreComercial2,
            notas,
            nventas,
            oportunidades,
            origen,
            pais,
            propietarioRegistro,
            provincia,
            razonSocial,
            responsableCuentaExterna,
            sector,
            idSubcar,
            telefono,
            tipo,
            web,
            prioridad,
            idTerritorio,
            categoria,
            linkImagen
        } = params;

        return {
            CNAEActivity: actividadCNAE,
            SICActivity: actividadSIC,
            activities: actividades,
            car: idCar,
            cif: cif,
            city: ciudad,
            classification: clasificacion,
            existsClient: clienteExistente,
            postCode: codigoPostal,
            region: comunidadAutonoma,
            contacts: contactos,
            email: correoCuenta,
            emails: correos,
            description: descripcion,
            address: direccion,
            domain: dominio,
            ebitda: ebitda,
            balance: ejercicioBalance,
            leadState: estadoLead,
            lifeCycleState: etapaCicloVida,
            billing: facturacion,
            created: fechaCreacion,
            downloaded: fechaDescarga,
            source: fuenteOrigen,
            id: id,
            idCrm360: idCrm360,
            userId: idUsuario,
            user:usuario,
            industry: industria,
            employeeNum: nempleados,
            commercialName: nombreComercial,
            commercialName2: nombreComercial2,
            notes: notas,
            sales: nventas,
            opportunities: oportunidades,
            origin: origen,
            country: pais,
            registerPropietary: propietarioRegistro,
            province: provincia,
            socialReason: razonSocial,
            extAccountManager: responsableCuentaExterna,
            sector: sector,
            subCar: idSubcar,
            phone: telefono,
            category: categoria,
            web: web,
            priority: prioridad,
            territory: idTerritorio,
            idLinkImage: linkImagen
        };
    }
}
