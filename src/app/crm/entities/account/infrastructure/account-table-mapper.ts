import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class AccountTableMapper {
    mapTo(params: any) {
        const {id, cif, commercialName, userId, category, priority, car, subCar, sector, territory} = params;

        return {
            id: id,
            cif: cif,
            commercialName: commercialName,
            user: userId,
            category: category,
            priority: priority,
            car: car,
            subCar: subCar,
            sector: sector,
            territory: territory
        };
    }
}
