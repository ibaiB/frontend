import {IAccount} from './IAccount';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IAccountSliderData {
    account: IAccount,
    myCrm: ICRMUser,
    crmUsers: ICRMUser[],
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    territories: IDictionaryValue[],
    sectors: IDictionaryValue[],
    categories: IDictionaryValue[],
    priorities: IDictionaryValue[],
    relationshipLevels: IDictionaryValue[],
    activityTypes: IDictionaryValue[],
    activityStates: IDictionaryValue[],
    opportunityRejectionReasons: IDictionaryValue[],
    opportunityStates: IDictionaryValue[],
    opportunityProbabilities: IDictionaryValue[],
}
