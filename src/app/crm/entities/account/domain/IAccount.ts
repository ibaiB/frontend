import {IActivity} from '../../activity/domain/IActivity';
import {IContact} from '../../contact/domain/IContact';
import {IEmail} from '../../email/domain/IEmail';
import {IOpportunity} from '../../opportunity/domain/IOpportunity';
import {INote} from '../../note/domain/INote';
import { ICRMUser } from '../../user/domain/ICRMUser';

export interface IAccount {
    CNAEActivity: string,
    SICActivity: string,
    activities: IActivity[],
    car: string,
    cif: string,
    city: string,
    classification: string,
    existsClient: boolean,
    postCode: string,
    region: string,
    contacts: IContact[],
    email: string,
    emails: IEmail[],
    description: string,
    address: string,
    domain: string,
    ebitda: string,
    balance: string,
    leadState: string,
    lifeCycleState: string,
    billing: string,
    created: string,
    downloaded: string,
    source: string,
    id?: string,
    idCrm360: string,
    userId: string,
    user:ICRMUser,
    industry: string,
    employeeNum: number,
    commercialName: string,
    commercialName2: string,
    notes: INote[],
    sales: string,
    opportunities: IOpportunity[],
    origin: string,
    country: string,
    registerPropietary: string,
    province: string,
    socialReason: string,
    extAccountManager: string,
    sector: string,
    subCar: string,
    phone: string,
    category: string,
    web: string,
    priority: string,
    territory: string,
    idLinkImage: string
}
