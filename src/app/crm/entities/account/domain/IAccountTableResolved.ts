import {IAccountTableData} from './IAccountTableData';

export interface IAccountTableResolved {
    tableData: IAccountTableData,
    error?: any
}
