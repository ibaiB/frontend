import {IAccount} from './IAccount';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IAccountEditData {
    myCrm: ICRMUser,
    account: IAccount,
    crmUsers: ICRMUser[]
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    territories: IDictionaryValue[],
    sectors: IDictionaryValue[],
    categories: IDictionaryValue[],
    priorities: IDictionaryValue[],
}
