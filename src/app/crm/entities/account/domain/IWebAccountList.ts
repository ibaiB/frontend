import {IWebAccount} from './IWebAccount';

export interface IWebAccountList {
    content: IWebAccount[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
