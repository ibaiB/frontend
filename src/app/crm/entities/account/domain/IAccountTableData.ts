import {IUserInfo} from '../../../../auth/session/domain/IUserInfo';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IAccount} from './IAccount';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IAccountTableData {
    myUser: IUserInfo,
    myCrm: ICRMUser,
    users: ICRMUser[],
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    categories: IDictionaryValue[],
    priorities: IDictionaryValue[],
    territories: IDictionaryValue[],
    sectors: IDictionaryValue[],
    accounts: IList<IAccount>,
    filtersValues: any;
    tableFiltersConfig: ITableFiltersConfig
}
