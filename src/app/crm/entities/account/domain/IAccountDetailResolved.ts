import {IAccountDetailData} from './IAccountDetailData';

export interface IAccountDetailResolved {
    detailData: IAccountDetailData;
    error?: any;
}
