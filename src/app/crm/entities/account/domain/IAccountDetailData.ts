import {IUserInfo} from '../../../../auth/session/domain/IUserInfo';
import {ICRMUser} from '../../user/domain/ICRMUser';
import {IDictionaryValue} from '../../../../shared/entities/dictionary/domain/IDictionaryValue';
import {IAccount} from './IAccount';
import {IList} from 'src/app/shared/generics/IList';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

export interface IAccountDetailData {
    myUser: IUserInfo,
    myCrm: ICRMUser,
    account: IAccount,
    crmUsers: ICRMUser[],
    cars: IList<ICar>,
    subCars: IList<ISubcar>,
    categories: IDictionaryValue[],
    priorities: IDictionaryValue[],
    territories: IDictionaryValue[],
    sectors: IDictionaryValue[],
    relationshipLevels: IDictionaryValue[],
    activityTypes: IDictionaryValue[],
    activityStates: IDictionaryValue[],
    opportunityRejectionReasons: IDictionaryValue[],
    opportunityStates: IDictionaryValue[],
    opportunityProbabilities: IDictionaryValue[]
}
