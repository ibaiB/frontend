export interface IAccountTable {
    id: string,
    cif: string,
    commercialName: string,
    user: string,
    category: string,
    priority: string,
    car: string,
    subCar: string,
    sector: string,
    territory: string
}
