import {IBudget} from './IBudget';

export interface IBudgetList {
    content: IBudget[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
