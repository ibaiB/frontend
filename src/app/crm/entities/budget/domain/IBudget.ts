import {IComment} from '../../comment/domain/IComment';
import {IState} from '../../state/domain/IState';

export interface IBudget {
    id?: string,
    name: string,
    state: IState,
    quantity: number,
    commentary: IComment,
}
