export class BudgetMapper {
    mapTo(params: any) {
        const {
            id,
            nombre,
            estado,
            cantidad,
            comentario
        } = params;

        return {
            id: id,
            name: nombre,
            state: estado,
            quantity: cantidad,
            commentary: comentario
        };
    }
}
