import {Observable} from 'rxjs';
import {IBudget} from '../domain/IBudget';

export abstract class AbstractBudgetService {
    abstract getList(params: any): Observable<IBudget[]>

    abstract get(id: string): Observable<IBudget>;

    abstract update(budget: IBudget): Observable<IBudget>;

    abstract delete(id: string);
}
