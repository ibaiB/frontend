import {ITeam} from './ITeam';

export interface ITeamList {
    content: ITeam[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
