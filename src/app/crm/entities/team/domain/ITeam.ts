import {ICRMUser} from '../../user/domain/ICRMUser';

export interface ITeam {
    description: string,
    id: string,
    name: string,
    users: ICRMUser[]

}
