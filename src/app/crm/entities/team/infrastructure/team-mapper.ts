export class TeamMapper {
    mapTo(params: any) {
        const {
            id,
            nombre,
            usuarios,
            descripcion
        } = params;

        return {
            id: id,
            name: nombre,
            users: usuarios,
            description: descripcion
        };
    }
}
