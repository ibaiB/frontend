import {Observable} from 'rxjs';
import {ITeam} from '../domain/ITeam';

export abstract class AbstractTeamService {
    abstract getList(params: any): Observable<ITeam[]>;

    abstract create(team: ITeam): Observable<ITeam>;

    abstract get(id: string): Observable<ITeam>;

    abstract delete(id: string);
}
