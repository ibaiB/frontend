import {ITerritory} from './ITerritory';

export interface ITerritoryList {
    content: ITerritory[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
