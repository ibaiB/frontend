import {IAccount} from '../../account/domain/IAccount';
import {ITeam} from '../../team/domain/ITeam';

export interface ITerritory {
    accounts: IAccount[],
    description: string,
    teams: ITeam[],
    id?: string,
    name: string
}
