export class TerritoryMapper {
    mapTo(params: any) {
        const {
            cuentas,
            descripcion,
            equipos,
            id,
            nombre
        } = params;

        return {
            accounts: cuentas,
            description: descripcion,
            teams: equipos,
            id: id,
            name: nombre
        };
    }
}
