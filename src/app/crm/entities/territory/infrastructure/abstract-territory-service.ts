import {Observable} from 'rxjs';
import {ITerritory} from '../domain/ITerritory';

export abstract class AbstractTerritoryService {
    abstract getList(params: any): Observable<ITerritory[]>;

    abstract create(territory: ITerritory): Observable<ITerritory>;

    abstract get(id: string): Observable<ITerritory>;

    abstract delete(id: string);
}
