import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class UserMapper {
    mapTo(params: any) {
        const {
            actividades,
            contactos,
            cuentas,
            email,
            id,
            id_equipo,
            oportunidades,
            rol,
            trackingOportunidades,
            territorio
        } = params;

        return {
            activities: actividades,
            contacts: contactos,
            accounts: cuentas,
            email: email,
            teamId: id_equipo,
            id: id,
            opportunities: oportunidades,
            rol: rol,
            trackingOpportunities: trackingOportunidades,
            territory: territorio
        };
    }
}
