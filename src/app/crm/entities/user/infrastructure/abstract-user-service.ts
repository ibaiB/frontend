import {Observable} from 'rxjs';
import {ICRMUser} from '../domain/ICRMUser';
import {IOpportunity} from '../../opportunity/domain/IOpportunity';

export abstract class AbstractUserService {
    abstract getList(params: any): Observable<ICRMUser[]>;

    abstract create(user: ICRMUser): Observable<ICRMUser>;

    abstract get(id: string): Observable<ICRMUser>;

    abstract addOpportunity(opportunity: IOpportunity, userId: ICRMUser): Observable<ICRMUser>;

    abstract getMe(): Observable<ICRMUser>;

    abstract delete(id: string);
}
