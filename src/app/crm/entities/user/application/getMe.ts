import {AbstractUserService} from '../infrastructure/abstract-user-service';

export function getMe(service: AbstractUserService) {
    return service.getMe();
}
