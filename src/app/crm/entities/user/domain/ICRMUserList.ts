import {ICRMUser} from './ICRMUser';

export interface ICRMUserList {
    content: ICRMUser[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number
}
