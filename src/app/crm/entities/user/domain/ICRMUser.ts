import {IAccount} from '../../account/domain/IAccount';
import {IContact} from '../../contact/domain/IContact';
import {IActivity} from '../../activity/domain/IActivity';
import {IOpportunity} from '../../opportunity/domain/IOpportunity';

export interface ICRMUser {
    id?: string,
    teamId?: string,
    email: string,
    accounts: IAccount[];
    contacts: IContact[];
    activities: IActivity[];
    opportunities: IOpportunity[];
    rol: string,
    trackingOpportunities: [];
    territory: string;
}
