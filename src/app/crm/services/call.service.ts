import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {AbstractCallService} from '../entities/call/infrastructure/abstract-call-service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ICall} from '../entities/call/domain/ICall';
import {CallMapper} from '../entities/call/infrastructure/call-mapper';

@Injectable({
    providedIn: 'any'
})
export class CallService extends AbstractCallService {

    private BASE_URL: string = environment.crm;
    private ENDPOINT: string = 'llamadas';

    constructor(private http: HttpClient, private mapper: CallMapper) {
        super();
    }

    delete(id: string) {
        return this.http
            .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    update(call: ICall): Observable<ICall> {
        return this.http
            .put<ICall>(`${this.BASE_URL}${this.ENDPOINT}/${call.id}`, call)
            .pipe(map(this.mapper.mapTo));
    }
}
