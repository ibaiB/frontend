import {Observable} from 'rxjs';
import {map, mergeMap, toArray} from 'rxjs/operators';
import {ITerritoryList} from '../entities/territory/domain/ITerritoryList';
import {HttpClient} from '@angular/common/http';
import {TerritoryMapper} from '../entities/territory/infrastructure/territory-mapper';
import {environment} from '../../../environments/environment.prod';
import {Injectable} from '@angular/core';
import {ITerritory} from '../entities/territory/domain/ITerritory';
import {AbstractTerritoryService} from '../entities/territory/infrastructure/abstract-territory-service';

@Injectable({
  providedIn: 'any'
})
export class TerritoryService extends AbstractTerritoryService {

  private BASE_URL: string = environment.crm;
  private ENDPOINT: string = 'territorios';

  constructor(private http: HttpClient, private mapper: TerritoryMapper) { super(); }

  getList(params: any): Observable<ITerritory[]> {
    return this.http
      .get<ITerritoryList>(`${this.BASE_URL}${this.ENDPOINT}`)
      .pipe(mergeMap(list => list.content))
      .pipe(map(this.mapper.mapTo))
      .pipe(toArray());
  }
  create(territory: ITerritory): Observable<ITerritory> {
    return this.http
      .post<ITerritory>(`${this.BASE_URL}${this.ENDPOINT}`, territory)
      .pipe(map(this.mapper.mapTo));
  }
  get(id: string): Observable<ITerritory> {
    return this.http
      .get<ITerritory>(`${this.BASE_URL}${this.ENDPOINT}/${id}`)
      .pipe(map(this.mapper.mapTo));
  }
  delete(id: string) {
    return this.http
      .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }
}
