import {Injectable} from '@angular/core';
import {AbstractAccountService} from '../entities/account/infrastructure/abstract-account-service';
import {Observable} from 'rxjs';
import {IAccount} from '../entities/account/domain/IAccount';
import {IEmail} from '../entities/email/domain/IEmail';
import {ICRMUser} from '../entities/user/domain/ICRMUser';
import {map, mergeMap, tap, toArray} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {AccountMapper} from '../entities/account/infrastructure/account-mapper';
import {IWebAccountList} from '../entities/account/domain/IWebAccountList';
import {ContactMapper} from '../entities/contact/infrastructure/contact-mapper';
import {ActivityMapper} from '../entities/activity/infrastructure/activity-mapper';
import {OpportunityMapper} from '../entities/opportunity/infrastructure/opportunity-mapper';
import {IWebAccount} from '../entities/account/domain/IWebAccount';
import {IAccountOut} from '../entities/account/domain/IAccountOut';
import {IOpportunityOut} from '../entities/opportunity/domain/IOpportunityOut';
import {IWebActivity} from '../entities/activity/domain/IWebActivity';
import {IWebNote} from '../entities/note/domain/IWebNote';
import {IList} from 'src/app/shared/generics/IList';

@Injectable({
    providedIn: 'any',
})
export class AccountService extends AbstractAccountService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'cuentas';

    constructor(private http: HttpClient,
                private mapper: AccountMapper,
                private contactMapper: ContactMapper,
                private activityMapper: ActivityMapper,
                private opportunityMapper: OpportunityMapper) {
        super();
    }

    createAccount(account: IAccountOut): Observable<IAccount> {
        return this.http
            .post<IWebAccount>(`${this.BASE_URL}${this.ENDPOINT}`, account)
            .pipe(tap(account => account = this._accountTap(account)))
            .pipe(map(this.mapper.mapTo));
    }

    addActivity(activity: IWebActivity, accountId: string): Observable<IAccount> {
        return this.http
            .post<IWebAccount>(`${this.BASE_URL}${this.ENDPOINT}/${accountId}/actividades`, activity)
            .pipe(tap(account => account = this._accountTap(account)))
            .pipe(map(this.mapper.mapTo));
    }

    addEmail(email: IEmail, accountId: string): Observable<IAccount> {
        return this.http
            .post<IWebAccount>(`${this.BASE_URL}${this.ENDPOINT}/${accountId}/correos`, email)
            .pipe(tap(account => account = this._accountTap(account)))
            .pipe(map(this.mapper.mapTo));
    }

    addNote(note: IWebNote, accountId: string): Observable<IAccount> {
        return this.http
            .post<IWebAccount>(`${this.BASE_URL}${this.ENDPOINT}/${accountId}/notas`, note)
            .pipe(tap(account => account = this._accountTap(account)))
            .pipe(map(this.mapper.mapTo));
    }

    addOpportunity(opportunity: IOpportunityOut, accountId: string): Observable<IAccount> {
        return this.http
            .post<IWebAccount>(`${this.BASE_URL}${this.ENDPOINT}/${accountId}/oportunidades`, opportunity)
            .pipe(tap(account => account = this._accountTap(account)))
            .pipe(map(this.mapper.mapTo));
    }

    addUser(user: ICRMUser, accountId: string): Observable<IAccount> {
        return this.http
            .put<IWebAccount>(`${this.BASE_URL}${this.ENDPOINT}/${accountId}/usuarios/${user.id}`, user)
            .pipe(tap(account => account = this._accountTap(account)))
            .pipe(map(this.mapper.mapTo));
    }

    get(id: string): Observable<IAccount> {
        return this.http
            .get<IWebAccount>(`${this.BASE_URL}${this.ENDPOINT}/${id}`)
            .pipe(tap(account => account = this._accountTap(account)))
            .pipe(map(this.mapper.mapTo));
    }

    getList(params: any): Observable<IAccount[]> {
        let httpParams = new HttpParams();
        let paramsKeys = Object.keys(params);
        paramsKeys.forEach(key => {if(params[key]!== undefined) httpParams = httpParams.append(key, params[key].toString())});

        return this.http
            .get<IWebAccountList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: httpParams})
            .pipe(mergeMap(list => list.content))
            .pipe(tap(account => account = this._accountTap(account)))
            .pipe(map(this.mapper.mapTo))
            .pipe(toArray());
    }

    search(params: any): Observable<IList<IAccount>> {
        return this.http
            .get<IList<any>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: this._clean(params)})})
            .pipe(tap(accountList => accountList.content = accountList.content.map(this._accountTap)))
            .pipe(tap(accountList => accountList.content = accountList.content.map(this.mapper.mapTo)));
    }

    update(account: IWebAccount): Observable<IAccount> {
        return this.http
            .put<IWebAccount>(`${this.BASE_URL}${this.ENDPOINT}/${account.id}`, account)
            .pipe(tap(account => account = this._accountTap(account)))
            .pipe(map(this.mapper.mapTo));
    }

    private _accountTap(account: IWebAccount) {
        //TODO
        const accMapper = new ActivityMapper();
        const conMapper = new ContactMapper();
        const oppMapper = new OpportunityMapper();

        if (account.actividades !== undefined) account.actividades = account.actividades.map(accMapper.mapTo);
        if (account.contactos !== undefined) account.contactos = account.contactos.map(conMapper.mapTo);
        if (account.oportunidades !== undefined) account.oportunidades = account.oportunidades.map(oppMapper.mapTo);

        return account;
    }

    private _clean(object): any { 
        const cleaned = {}; 
        const keys = Object.keys(object); 
        keys.forEach(key => { 
            if (object[key]) { cleaned[key] = object[key]; } 
        }); 
        return cleaned; 
    }
}
