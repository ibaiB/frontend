import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {AbstractCommentService} from '../entities/comment/infrastructure/abstract-comment-service';
import {IWebComment} from '../entities/comment/domain/IWebComment';

@Injectable({
  providedIn: 'any'
})
export class CommentService extends AbstractCommentService {

  private BASE_URL: string = environment.crm;
  private ENDPOINT: string = 'comentarios';

  constructor(private http: HttpClient) { super() }

  get(id: string): Observable<IWebComment> {
    return this.http
      .get<IWebComment>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  update(comment: IWebComment): Observable<IWebComment> {
    return this.http
      .put<IWebComment>(`${this.BASE_URL}${this.ENDPOINT}/${comment.id}`, comment);
  }
}
