import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, mergeMap, tap, toArray} from 'rxjs/operators';
import {ActivityMapper} from '../entities/activity/infrastructure/activity-mapper';
import {AbstractActivityService} from '../entities/activity/infrastructure/abstract-activity-service';
import {environment} from 'src/environments/environment';
import {IActivity} from '../entities/activity/domain/IActivity';
import {IWebNote} from '../entities/note/domain/IWebNote';
import {IWebActivity} from '../entities/activity/domain/IWebActivity';
import {IList} from '../../shared/generics/IList';

@Injectable({
  providedIn: 'any'
})
export class ActivityService extends AbstractActivityService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'actividades';

  constructor(private httpClient: HttpClient, private mapper: ActivityMapper) {
    super();
  }

  get(id: string): Observable<IActivity> {
    return this.httpClient
      .get<IActivity>(`${this.BASE_URL}${this.ENDPOINT}/${id}`)
      .pipe(map(this.mapper.mapTo));
  }

  update(activity: IActivity): Observable<IActivity> {
    return this.httpClient
        .put<IActivity>(`${this.BASE_URL}${this.ENDPOINT}/${activity.id}`, this._clean(activity))
    .pipe(map(this.mapper.mapTo));
  }

  delete(id: string) {
    return this.httpClient
        .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  addNote(note: IWebNote, activityId: string): Observable<IWebActivity> {
    return this.httpClient.post<IWebActivity>(`${this.BASE_URL}${this.ENDPOINT}/${activityId}/notas`, note);
  }

  search(params: any): Observable<IList<IActivity>> {
    return this.httpClient
        .get<IList<any>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: this._clean(params)})})
        .pipe(tap(activityList => activityList.content = activityList.content.map(this.mapper.mapTo)));
}
  getList(params: any): Observable<IActivity[]> {
    const query = this._clean(params);

    return this.httpClient
        .get<IList<IWebActivity>>(
            `${this.BASE_URL}${this.ENDPOINT}/search`,
            {params: new HttpParams({fromObject: query})}
        )
        .pipe(
            mergeMap(list => list.content),
            map(this.mapper.mapTo),
            toArray()
        );
  }

  private _clean(object) {
    const cleaned = {};
    const keys = Object.keys(object);
    keys.forEach(key => {
      if (object[key]) {
        cleaned[key] = object[key];
      }
    });
    return cleaned;
  }
}
