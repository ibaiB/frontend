import {Injectable} from '@angular/core';
import {AbstractNoteService} from '../entities/note/infrastructure/abstract-note-service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {IWebNote} from '../entities/note/domain/IWebNote';
import {IWebComment} from '../entities/comment/domain/IWebComment';
import {IList} from '../../shared/generics/IList';

@Injectable({
    providedIn: 'any'
})
export class NoteService extends AbstractNoteService {


    private BASE_URL = environment.baseUrl;
    private ENDPOINT = 'notas';

    constructor(private httpClient: HttpClient) {
        super();
    }

    addComment(comment: IWebComment, noteId: string): Observable<IWebNote> {
        return this.httpClient
            .post<IWebNote>(`${this.BASE_URL}${this.ENDPOINT}/${noteId}/comentario`, comment);
    }

    get(id: string): Observable<IWebNote> {
        return this.httpClient
            .get<IWebNote>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    delete(id: string) {
        return this.httpClient
            .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    search(query: any): Observable<IList<IWebNote>> {
        return this.httpClient.get<IList<IWebNote>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
    }

    update(id: string, note: IWebNote): Observable<IWebNote> {
        return this.httpClient.put<IWebNote>(`${this.BASE_URL}${this.ENDPOINT}/${id}`, note);
    }
}
