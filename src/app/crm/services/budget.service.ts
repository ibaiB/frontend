import {IBudgetList} from '../entities/budget/domain/IBudgetList';
import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, mergeMap, toArray} from 'rxjs/operators';
import {AbstractBudgetService} from '../entities/budget/infrastructure/abstract-budget-service';
import {BudgetMapper} from '../entities/budget/infrastructure/budget-mapper';
import {IBudget} from '../entities/budget/domain/IBudget';

@Injectable({
    providedIn: 'any'
})
export class BudgetService extends AbstractBudgetService {

    private BASE_URL: string = environment.crm;
    private ENDPOINT: string = 'presupuestos';

    constructor(private http: HttpClient, private mapper: BudgetMapper) {
        super();
    }

    getList(params: any): Observable<IBudget[]> {
        return this.http
            .get<IBudgetList>(`${this.BASE_URL}${this.ENDPOINT}`)
            .pipe(mergeMap(list => list.content))
            .pipe(map(this.mapper.mapTo))
            .pipe(toArray());
    }

    get(id: string): Observable<IBudget> {
        return this.http
            .get<IBudget>(`${this.BASE_URL}${this.ENDPOINT}/${id}`)
            .pipe(map(this.mapper.mapTo));
    }

    update(budget: any): Observable<IBudget> {
        return this.http
            .put<IBudget>(`${this.BASE_URL}${this.ENDPOINT}/${budget.id}`, budget)
            .pipe(map(this.mapper.mapTo));
    }

    delete(id: string) {
        return this.http
            .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }
}
