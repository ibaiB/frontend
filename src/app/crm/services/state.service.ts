import {StateMapper} from '../entities/state/infrastructure/state-mapper';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {AbstractStateService} from '../entities/state/infrastructure/abstract-state-service';
import {IState} from '../entities/state/domain/IState';
import {Observable} from 'rxjs';
import {IStateList} from '../entities/state/domain/IStateList';
import {map, mergeMap, toArray} from 'rxjs/operators';


@Injectable({
    providedIn: 'any'
})
export class StateService extends AbstractStateService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'estados';

    constructor(private http: HttpClient, private mapper: StateMapper) {
        super();
    }

    create(state: IState): Observable<IState> {
        return this.http
            .post<IState>(`${this.BASE_URL}${this.ENDPOINT}`, state)
            .pipe(map(this.mapper.mapTo));
    }

    getList(params: any): Observable<IState[]> {
        return this.http
            .get<IStateList>(`${this.BASE_URL}${this.ENDPOINT}/search`)
            .pipe(mergeMap(list => list.content))
            .pipe(map(this.mapper.mapTo))
            .pipe(toArray());
    }
}
