import {Injectable} from '@angular/core';
import {AbstractOpportunityService} from '../entities/opportunity/infrastructure/abstract-opportunity-service';
import {environment} from '../../../environments/environment.prod';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map, mergeMap, tap, toArray} from 'rxjs/operators';
import {IOpportunity} from '../entities/opportunity/domain/IOpportunity';
import {IWebOpportunityList} from '../entities/opportunity/domain/IWebOpportunityList';
import {OpportunityMapper} from '../entities/opportunity/infrastructure/opportunity-mapper';
import {IBudget} from '../entities/budget/domain/IBudget';
import {StateMapper} from '../entities/state/infrastructure/state-mapper';
import {IWebOpportunity} from '../entities/opportunity/domain/IWebOpportunity';
import {IWebActivity} from '../entities/activity/domain/IWebActivity';
import {IWebNote} from '../entities/note/domain/IWebNote';
import {IList} from 'src/app/shared/generics/IList';

@Injectable({
    providedIn: 'any'
})
export class OpportunityService extends AbstractOpportunityService {
    

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'oportunidades';

    constructor(private http: HttpClient,
                private mapper: OpportunityMapper,
                private stateMapper: StateMapper) {
        super();
    }

    getList(params: any): Observable<IOpportunity[]> {
        let httpParams = new HttpParams();
        let paramsKeys = Object.keys(params);
        paramsKeys.forEach(key => {if(params[key]!== undefined) httpParams = httpParams.append(key, params[key].toString())});

        return this.http
            .get<IWebOpportunityList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: httpParams})
            .pipe(mergeMap(list => list.content))
            .pipe(tap(opportunity => opportunity.estado = this.stateMapper.mapTo(opportunity.estado)))
            .pipe(map(this.mapper.mapTo))
            .pipe(toArray());
    }

    update(opportunity: any): Observable<IOpportunity> {
        return this.http
            .put<IWebOpportunity>(`${this.BASE_URL}${this.ENDPOINT}/${opportunity.id}`, opportunity)
            .pipe(tap(opportunity => opportunity.estado = this.stateMapper.mapTo(opportunity.estado)))
            .pipe(map(this.mapper.mapTo));
    }

    // @TODO these two methods doesnt appear on swagger
    create(opportunity: IOpportunity): Observable<IOpportunity> {
        return this.http
            // @TODO doesn't make sense ¿Id?
            .post<IWebOpportunity>(`${this.BASE_URL}${this.ENDPOINT}/${opportunity.id}`, opportunity)
            .pipe(tap(opportunity => opportunity.estado = this.stateMapper.mapTo(opportunity.estado)))
            .pipe(map(this.mapper.mapTo));
    }

    delete(id: string) {
        return this.http
            .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    //

    search(params: any): Observable<IList<IOpportunity>> {
        return this.http
            .get<IList<any>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: this._clean(params)})})
            .pipe(tap(oppList => oppList.content.map(o => o.estado = this.stateMapper.mapTo(o.estado))))
            .pipe(tap(oppList => oppList.content = oppList.content.map(this.mapper.mapTo)));
    }

    addBudget(budget: IBudget, opportunityId: string): Observable<IOpportunity> {
        throw new Error('METHOD_NOT_IMPLEMENTED');
    }

    get(id: string): Observable<IOpportunity> {
        return this.http
            .get<IWebOpportunity>(`${this.BASE_URL}${this.ENDPOINT}/${id}`)
            .pipe(tap(opportunity => opportunity.estado = this.stateMapper.mapTo(opportunity.estado)))
            .pipe(map(this.mapper.mapTo));
    }

    addActivity(activity: IWebActivity, opportunityId: string): Observable<IOpportunity> {
        return this.http
            .post<IWebOpportunity>(`${this.BASE_URL}${this.ENDPOINT}/${opportunityId}/actividades`, activity)
            .pipe(tap(opportunity => opportunity.estado = this.stateMapper.mapTo(opportunity.estado)))
            .pipe(map(this.mapper.mapTo));
    }
    
    addNote(note: IWebNote, opportunityId: string): Observable<IWebOpportunity> {
        return this.http.post<IWebOpportunity>(`${this.BASE_URL}${this.ENDPOINT}/${opportunityId}/notas`, note);
    }

    private _clean(object): any { 
        const cleaned = {}; 
        const keys = Object.keys(object); 
        keys.forEach(key => { 
            if (object[key]) { cleaned[key] = object[key]; } 
        }); 
        return cleaned; 
    }
}
