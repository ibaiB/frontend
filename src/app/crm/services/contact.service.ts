import {ContactMapper} from '../entities/contact/infrastructure/contact-mapper';
import {IWebContactList} from '../entities/contact/domain/IWebContactList';
import {environment} from '../../../environments/environment.prod';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map, mergeMap, tap, toArray} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {AbstractContactService} from '../entities/contact/infrastructure/abstract-contact-service';
import {Observable} from 'rxjs';
import {IContact} from '../entities/contact/domain/IContact';
import {ICall} from '../entities/call/domain/ICall';
import {IEmail} from '../entities/email/domain/IEmail';
import {AccountMapper} from '../entities/account/infrastructure/account-mapper';
import {OpportunityMapper} from '../entities/opportunity/infrastructure/opportunity-mapper';
import {ActivityMapper} from '../entities/activity/infrastructure/activity-mapper';
import {IWebContact} from '../entities/contact/domain/IWebContact';
import {IContactOut} from '../entities/contact/domain/IContactOut';
import {IOpportunityOut} from '../entities/opportunity/domain/IOpportunityOut';
import {IWebActivity} from '../entities/activity/domain/IWebActivity';
import {IWebNote} from '../entities/note/domain/IWebNote';
import {IList} from 'src/app/shared/generics/IList';

@Injectable({
    providedIn: 'any'
})
export class ContactService extends AbstractContactService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'contactos';

    constructor(private http: HttpClient, private mapper: ContactMapper,
        private accountMapper: AccountMapper,
        private opportunityMapper: OpportunityMapper,
        private activityMapper: ActivityMapper) {
        super();
    }

    getList(params: any): Observable<IContact[]> {
        let httpParams = new HttpParams();
        //@TODO separate in a function
        let paramsKeys = Object.keys(params);
        paramsKeys.forEach(key => { if (params[key] !== undefined) httpParams = httpParams.append(key, params[key].toString()) });

        return this.http
            .get<IWebContactList>(`${this.BASE_URL}${this.ENDPOINT}/search`, { params: httpParams })
            .pipe(mergeMap(list => list.content))
            .pipe(tap(contact => contact = this._contactTap(contact)))
            .pipe(map(this.mapper.mapTo))
            .pipe(toArray());
    }

    get(id: string): Observable<IContact> {
        return this.http
            .get<IWebContact>(`${this.BASE_URL}${this.ENDPOINT}/${id}`)
            .pipe(tap(contact => contact = this._contactTap(contact)))
            .pipe(map(this.mapper.mapTo));
    }

    create(contact: IContactOut): Observable<IContact> {
        return this.http
            .post<IWebContact>(`${this.BASE_URL}${this.ENDPOINT}`, contact)
            .pipe(tap(contact => contact = this._contactTap(contact)))
            .pipe(map(this.mapper.mapTo));
    }

    update(contact: IContactOut): Observable<IContact> {
        return this.http
            .put<IWebContact>(`${this.BASE_URL}${this.ENDPOINT}/${contact.id}`, contact)
            .pipe(tap(contact => contact = this._contactTap(contact)))
            .pipe(map(this.mapper.mapTo));
    }

    addActivity(activity: IWebActivity, contactId: string): Observable<IContact> {
        return this.http
            .post<IWebContact>(`${this.BASE_URL}${this.ENDPOINT}/${contactId}/actividades`, activity)
            .pipe(tap(contact => contact = this._contactTap(contact)))
            .pipe(map(this.mapper.mapTo));
    }

    addEmail(email: IEmail, contactId: string): Observable<IContact> {
        return this.http
            .post<IWebContact>(`${this.BASE_URL}${this.ENDPOINT}/${contactId}/correos`, email)
            .pipe(tap(contact => contact = this._contactTap(contact)))
            .pipe(map(this.mapper.mapTo));
    }

    addNote(note: IWebNote, contactId: string): Observable<IContact> {
        return this.http
            .post<IWebContact>(`${this.BASE_URL}${this.ENDPOINT}/${contactId}/notas`, note)
            .pipe(tap(contact => contact = this._contactTap(contact)))
            .pipe(map(this.mapper.mapTo));
    }

    addOpportunity(opportunity: IOpportunityOut, contactId: string): Observable<IContact> {
        return this.http
            .post<IWebContact>(`${this.BASE_URL}${this.ENDPOINT}/${contactId}/oportunidades`, opportunity)
            .pipe(tap(contact => contact = this._contactTap(contact)))
            .pipe(map(this.mapper.mapTo));
    }

    addCall(call: ICall, contactId: string): Observable<IContact> {
        return this.http
            .post<IWebContact>(`${this.BASE_URL}${this.ENDPOINT}/${contactId}/llamadas`, call)
            .pipe(tap(contact => contact = this._contactTap(contact)))
            .pipe(map(this.mapper.mapTo));
    }

    search(params: any): Observable<IList<IContact>> {
        return this.http
            .get<IList<any>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: this._clean(params)})})
            .pipe(tap(contactList => contactList.content = contactList.content.map(this._contactTap)))
            .pipe(tap(contactList => contactList.content = contactList.content.map(this.mapper.mapTo)));
    }

    private _contactTap(contact: IWebContact) {
        const accMapper = new ActivityMapper();
        const oppMapper = new OpportunityMapper();

        if (contact.actividades !== undefined) contact.actividades = contact.actividades.map(accMapper.mapTo);
        if (contact.oportunidades !== undefined) contact.oportunidades = contact.oportunidades.map(oppMapper.mapTo);

        return contact;
    }

    private _clean(object): any { 
        const cleaned = {}; 
        const keys = Object.keys(object); 
        keys.forEach(key => { 
            if (object[key]) { cleaned[key] = object[key]; } 
        }); 
        return cleaned; 
    }
}
