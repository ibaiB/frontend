import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment.prod';
import {EmailMapper} from '../entities/email/infrastructure/email-mapper';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {IEmail} from '../entities/email/domain/IEmail';
import {AbstractEmailService} from '../entities/email/infrastructure/abstract-email-service';

@Injectable({
    providedIn: 'any'
})
export class EmailService extends AbstractEmailService {

    private BASE_URL: string = environment.crm;
    private ENDPOINT: string = 'correo';

    constructor(private http: HttpClient, private mapper: EmailMapper) {
        super();
    }

    update(email: IEmail): Observable<IEmail> {
        return this.http
            .put<IEmail>(`${this.BASE_URL}${this.ENDPOINT}/${email.id}`, email)
            .pipe(map(this.mapper.mapTo));
    }

    delete(id: string) {
        return this.http
            .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }

    /*
    deleteCorreo(id: string){
      return this.http.delete<Correo>(`${this.URL_BASE}/correo/${id}`);
    }*/
}
