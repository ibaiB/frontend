import {map, mergeMap, toArray} from 'rxjs/operators';
import {environment} from '../../../environments/environment.prod';
import {UserMapper} from '../entities/user/infrastructure/user-mapper';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AbstractUserService} from '../entities/user/infrastructure/abstract-user-service';
import {IOpportunity} from '../entities/opportunity/domain/IOpportunity';
import {ICRMUser} from '../entities/user/domain/ICRMUser';
import {Observable} from 'rxjs';
import {ICRMUserList} from '../entities/user/domain/ICRMUserList';

@Injectable({
    providedIn: 'any'
})
export class UserService extends AbstractUserService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'usuarios';

    constructor(private http: HttpClient, private mapper: UserMapper) {
        super();
    }

    addOpportunity(opportunity: IOpportunity, userId: ICRMUser): Observable<ICRMUser> {
        return this.http
            .post<ICRMUser>(`${this.BASE_URL}${this.ENDPOINT}/${userId}/oportunidades`, opportunity)
            .pipe(map(this.mapper.mapTo));
    }

    create(user: ICRMUser): Observable<ICRMUser> {
        return this.http
            .post<ICRMUser>(`${this.BASE_URL}${this.ENDPOINT}`, user)
            .pipe(map(this.mapper.mapTo));
    }

    get(id: string): Observable<ICRMUser> {
        return this.http
            .get<ICRMUser>(`${this.BASE_URL}${this.ENDPOINT}/${id}`)
            .pipe(map(this.mapper.mapTo));
    }

    getList(params: any): Observable<ICRMUser[]> {
        let httpParams = new HttpParams();
        let paramsKeys = Object.keys(params);
        paramsKeys.forEach(key => {
            if (params[key] !== undefined) {
                httpParams = httpParams.append(key, params[key].toString());
            }
        });

        return this.http
            .get<ICRMUserList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: httpParams})
            .pipe(mergeMap(list => list.content))
            .pipe(map(this.mapper.mapTo))
            .pipe(toArray());
    }

    search(params: any): Observable<ICRMUser[]> {
        let httpParams = new HttpParams();
        let paramsKeys = Object.keys(params);
        paramsKeys.forEach(key => {
            if (params[key] !== undefined) {
                httpParams = httpParams.append(key, params[key].toString());
            }
        });

        return this.http
            .get<ICRMUserList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: httpParams})
            .pipe(mergeMap(list => list.content))
            .pipe(map(this.mapper.mapTo))
            .pipe(toArray());
    }

    getMe(): Observable<ICRMUser> {
        return this.http
            .get<ICRMUser>(`${this.BASE_URL}${this.ENDPOINT}/me`)
            .pipe(map(this.mapper.mapTo));
    }

    delete(id: string) {
        return this.http
            .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }
}
