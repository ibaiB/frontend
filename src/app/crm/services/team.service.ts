import {Observable} from 'rxjs';
import {ITeamList} from '../entities/team/domain/ITeamList';
import {map, mergeMap, toArray} from 'rxjs/operators';
import {TeamMapper} from '../entities/team/infrastructure/team-mapper';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment.prod';
import {Injectable} from '@angular/core';
import {ITeam} from '../entities/team/domain/ITeam';
import {AbstractTeamService} from '../entities/team/infrastructure/abstract-team-service';

@Injectable({
    providedIn: 'any'
})
export class TeamService extends AbstractTeamService {

    private BASE_URL: string = environment.crm;
    private ENDPOINT: string = 'equipos';

    constructor(private http: HttpClient, private mapper: TeamMapper) {
        super();
    }

    getList(params: any): Observable<ITeam[]> {
        return this.http
            .get<ITeamList>(`${this.BASE_URL}${this.ENDPOINT}`)
            .pipe(mergeMap(list => list.content))
            .pipe(map(this.mapper.mapTo))
            .pipe(toArray());
    }

    create(team: ITeam): Observable<ITeam> {
        return this.http
            .post<ITeam>(`${this.BASE_URL}${this.ENDPOINT}`, team)
            .pipe(map(this.mapper.mapTo));
    }

    get(id: string): Observable<ITeam> {
        return this.http
            .get<ITeam>(`${this.BASE_URL}${this.ENDPOINT}/${id}`)
            .pipe(map(this.mapper.mapTo));
    }

    delete(id: string) {
        return this.http
            .delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
    }


}
