// Angular
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {routing} from './crm-routing.module';
import {DragDropModule} from '@angular/cdk/drag-drop';

// Components
import {AccountsComponent} from './presentation/accounts/accounts.component';
import {ContactsComponent} from './presentation/contacts/contacts.component';
import {OpportunitiesComponent} from './presentation/opportunities/opportunities.component';
import {CreateAccountComponent} from './presentation/accounts/create-account/create-account.component';
import {CreateContactComponent} from './presentation/contacts/create-contact/create-contact.component';
import {CreateOpportunityComponent} from './presentation/opportunities/create-opportunity/create-opportunity.component';
import {ContactTableComponent} from './presentation/contacts/contact-table/contact-table.component';
import {AccountTableComponent} from './presentation/accounts/account-table/account-table.component';
import {AccountDetailComponent} from './presentation/accounts/account-detail/account-detail.component';
import {ContactDetailComponent} from './presentation/contacts/contact-detail/contact-detail.component';
import {OpportunityGridComponent} from './presentation/opportunities/opportunity-management/opportunity-grid/opportunity-grid.component';
import {OpportunityPipeLineComponent} from './presentation/opportunities/opportunity-management/opportunity-pipe-line/opportunity-pipe-line.component';
import {OpportunityTrackingComponent} from './presentation/opportunities/opportunity-management/opportunity-tracking/opportunity-tracking.component';
import {ActivitiesComponent} from './presentation/activities/activities.component';
import {ActivitySliderListComponent} from './presentation/activities/activity-slider-list/activity-slider-list.component';
import {CreateActivityComponent} from './presentation/activities/create-activity/create-activity.component';
import {OpportunitySliderListComponent} from './presentation/opportunities/opportunity-slider-list/opportunity-slider-list.component';
import {ActivityItemComponent} from './presentation/activities/activity-slider-list/activity-item/activity-item.component';
import {OpportunityManagementComponent} from './presentation/opportunities/opportunity-management/opportunity-management.component';
import {EditContactComponent} from './presentation/contacts/contact-detail/edit-contact/edit-contact.component';
import {EditAccountComponent} from './presentation/accounts/account-detail/edit-account/edit-account.component';
import {ContactSliderComponent} from './presentation/contacts/contact-detail/contact-slider/contact-slider.component';
import {AccountSliderComponent} from './presentation/accounts/account-detail/account-slider/account-slider.component';
import {ActivityTableComponent} from './presentation/activities/activity-table/activity-table.component';
import {ContactSliderListComponent} from './presentation/contacts/contact-slider-list/contact-slider-list.component';
import {ContactItemComponent} from './presentation/contacts/contact-slider-list/contact-item/contact-item.component';
import {OpportunityItemComponent} from './presentation/opportunities/opportunity-slider-list/opportunity-item/opportunity-item.component';
import {AccountItemComponent} from './presentation/accounts/account-item/account-item.component';
import {ActivitySliderComponent} from './presentation/activities/activity-detail/activity-slider/activity-slider.component';
import {OpportunityDetailComponent} from './presentation/opportunities/opportunity-detail/opportunity-detail.component';
import {FullEditOpportunityComponent} from './presentation/opportunities/full-edit-opportunity/full-edit-opportunity.component';

// Mappers
import {AccountMapper} from './entities/account/infrastructure/account-mapper';
import {ActivityMapper} from './entities/activity/infrastructure/activity-mapper';
import {BudgetMapper} from './entities/budget/infrastructure/budget-mapper';
import {CallMapper} from './entities/call/infrastructure/call-mapper';
import {CommentMapper} from './entities/comment/infrastructure/comment-mapper';
import {ContactMapper} from './entities/contact/infrastructure/contact-mapper';
import {ContactOutMapper} from './entities/contact/infrastructure/contact-out-mapper';
import {EmailMapper} from './entities/email/infrastructure/email-mapper';
import {NoteMapper} from './entities/note/infrastructure/note-mapper';
import {OpportunityMapper} from './entities/opportunity/infrastructure/opportunity-mapper';
import {OpportunityOutMapper} from './entities/opportunity/infrastructure/opportunity-out-mapper';
import {StateMapper} from './entities/state/infrastructure/state-mapper';
import {TeamMapper} from './entities/team/infrastructure/team-mapper';
import {TerritoryMapper} from './entities/territory/infrastructure/territory-mapper';
import {TrackingMapper} from './entities/tracking/infrastructure/tracking-mapper';
import {UserMapper} from './entities/user/infrastructure/user-mapper';
import {WebActivityMapper} from './entities/activity/infrastructure/web-activity-mapper';

//Modules
import {AngularMaterialModule} from '../shared/material/angular-material.module';
import {MatDialogRef} from '@angular/material/dialog';
import {EditActivityComponent} from './presentation/activities/edit-activity/edit-activity.component';
import {ActivityDetailComponent} from './presentation/activities/activity-detail/activity-detail.component';
import {OpportunityFullDetailComponent} from './presentation/opportunities/opportunity-full-detail/opportunity-full-detail.component';
import {ContactSimpleDetailComponent} from './presentation/contacts/contact-simple-detail/contact-simple-detail.component';
import {AccountSimpleDetailComponent} from './presentation/accounts/account-simple-detail/account-simple-detail.component';
import {AccountTableMapper} from './entities/account/infrastructure/account-table-mapper';
import {ContactTableMapper} from './entities/contact/infrastructure/contact-table-mapper';
import {WebAccountMapper} from './entities/account/infrastructure/web-account-mapper';
import {SharedModule} from '../shared/shared.module';
import {AccountEditDataMapper} from './entities/account/infrastructure/account-edit-data-mapper';
import {AccountSliderDataMapper} from './entities/account/infrastructure/account-slider-data-mapper';
import {ActivitySliderListDataMapper} from './entities/activity/infrastructure/activity-slider-list-data-mapper';
import {OpportunitySliderListDataMapper} from './entities/opportunity/infrastructure/opportunity-slider-list-data-mapper';
import {ContactSliderListDataMapper} from './entities/contact/infrastructure/contact-slider-list-data-mapper';
import {OpportunityItemDataMapper} from './entities/opportunity/infrastructure/opportunity-item-data-mapper';
import {ActivityItemDataMapper} from './entities/activity/infrastructure/activity-item-data-mapper';
import {OpportunitySliderComponent} from './presentation/opportunities/opportunity-slider/opportunity-slider.component';
import {ContactSingleItemComponent} from './presentation/contacts/contact-single-item/contact-single-item.component';
import {EditOpportunityComponent} from './presentation/opportunities/edit-opportunity/edit-opportunity.component';
import {NotesSliderListComponent} from './presentation/notes/notes-slider-list/notes-slider-list.component';
import {NoteItemComponent} from './presentation/notes/notes-slider-list/note-item/note-item.component';
import {NoteDetailComponent} from './presentation/notes/note-detail/note-detail.component';
import {ActivityDetailDialogComponent} from './presentation/activities/activity-detail-dialog/activity-detail-dialog.component';

// import { MyCrmComponent } from './presentation/my-crm/my-crm.component';

@NgModule({
    declarations: [
        AccountsComponent,
        ContactsComponent,
        OpportunitiesComponent,
        CreateAccountComponent,
        CreateContactComponent,
        CreateOpportunityComponent,
        ContactTableComponent,
        AccountTableComponent,
        AccountDetailComponent,
        ContactDetailComponent,
        OpportunityGridComponent,
        OpportunityPipeLineComponent,
        OpportunityTrackingComponent,
        OpportunityManagementComponent,
        EditOpportunityComponent,
        FullEditOpportunityComponent,
        EditContactComponent,
        EditAccountComponent,
        ContactSliderComponent,
        AccountSliderComponent,
        ActivitiesComponent,
        ActivitySliderListComponent,
        CreateActivityComponent,
        OpportunitySliderListComponent,
        ActivityItemComponent,
        ActivityTableComponent,
        ContactSliderListComponent,
        ContactItemComponent,
        OpportunityItemComponent,
        AccountItemComponent,
        EditActivityComponent,
        ActivityDetailComponent,
        OpportunityFullDetailComponent,
        ContactSimpleDetailComponent,
        AccountSimpleDetailComponent,
        OpportunityDetailComponent,
        ContactSimpleDetailComponent,
        AccountSimpleDetailComponent,
        NotesSliderListComponent,
        NoteItemComponent,
        NoteDetailComponent,
        AccountSimpleDetailComponent,
        ActivitySliderComponent,
        OpportunitySliderComponent,
        ContactSingleItemComponent,
        ActivitySliderComponent,
        ActivityDetailDialogComponent
        // MyCrmComponent,
    ],
    imports: [
        CommonModule,
        routing,
        AngularMaterialModule,
        DragDropModule,
        SharedModule
    ],
    providers: [
        AccountMapper,
        ActivityMapper,
        ContactMapper,
        OpportunityMapper,
        StateMapper,
        BudgetMapper,
        CallMapper,
        CommentMapper,
        EmailMapper,
        NoteMapper,
        TeamMapper,
        TerritoryMapper,
        TrackingMapper,
        UserMapper,
        ContactOutMapper,
        OpportunityOutMapper,
        AccountTableMapper,
        ContactTableMapper,
        WebAccountMapper,
        WebActivityMapper,
        AccountEditDataMapper,
        AccountSliderDataMapper,
        ActivitySliderListDataMapper,
        OpportunitySliderListDataMapper,
        ContactSliderListDataMapper,
        OpportunityItemDataMapper,
        ActivityItemDataMapper,
        { provide: MatDialogRef, useValue: {} }
    ]
})
export class CrmModule {
}
