import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {AccountService} from '../services/account.service';
import {SessionService} from '../../auth/session/infrastructure/services/session.service';
import {UserService} from '../services/user.service';
import {DictionaryService} from '../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {getUserInfo} from '../../auth/session/application/getUserInfo';
import {getUsers} from '../entities/user/application/getUsers';
import {getDictionaries} from '../../shared/entities/dictionary/application/getDictionaries';
import {getMe} from '../entities/user/application/getMe';
import {getDictionaryById} from '../../shared/entities/dictionary/application/getDictionaryById';
import {joinAccount} from '../entities/account/application/joinAccount';
import {catchError, map, tap} from 'rxjs/operators';
import {IAccountTableResolved} from '../entities/account/domain/IAccountTableResolved';
import {CarService} from 'src/app/internal/car/infrastructure/services/car.service';
import {getCars} from 'src/app/internal/car/application/getCars';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {SubcarService} from '../../internal/subcar/infrastructure/services/subcar.service';
import {getSubCars} from '../../internal/subcar/application/getSubCars';

@Injectable({
    providedIn: 'any'
})
export class AccountTableResolverService implements Resolve<IAccountTableResolved> {

    constructor(private _accountService: AccountService,
                private _sessionService: SessionService,
                private _userService: UserService,
                private _dictionaryService: DictionaryService,
                private _carService: CarService,
                private _subcarService: SubcarService,
                private _cookieService : CookieFacadeService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAccountTableResolved> | Promise<IAccountTableResolved> | IAccountTableResolved {
        let params = {page: 0, size: 100};
        const filtersCache = this._cookieService.get('accountFilterCache');
        const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('account-table-config'));
        const accountSearchParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
        const orderParams = {...tableFiltersConfig?.order} ?? {};

        const $myUser = getUserInfo(this._sessionService);
        const $myCrm = getMe(this._userService);
        const $users = getUsers(params, this._userService);
        const $dictionaries = getDictionaries(params, this._dictionaryService);
        const $accounts = this._accountService.search({...JSON.parse(filtersCache),...accountSearchParams,...orderParams});
        const $cars = getCars(params, this._carService);
        const $subcars = getSubCars(params, this._subcarService);

        return forkJoin($myUser, $myCrm, $users, $dictionaries, $accounts, $cars, $subcars)
            .pipe(
                tap(response => {
                    response[4].content = response[4].content.map(account => joinAccount(account, response[3], response[2], response[5], response[6]))
                }),
                map(response => ({
                    tableData: {
                        myUser: response[0],
                        myCrm: response[1],
                        users: response[2],
                        cars: response[5],
                        subCars: response[6],
                        categories: getDictionaryById(response[3], 'CATEGORIA').values,
                        priorities: getDictionaryById(response[3], 'PRIORIDAD').values,
                        territories: getDictionaryById(response[3], 'TERRITORIO').values,
                        sectors: getDictionaryById(response[3], 'SECTOR').values,
                        accounts: response[4],
                        filtersValues:filtersCache,
                        tableFiltersConfig: tableFiltersConfig
                    }
                })),
                catchError(error => {
                    return of({
                        tableData: null,
                        error: {message: 'Error on account table resolver, data couldn\'t be fetched', error: error}
                    })
                })
            );
    }
}
