import {TestBed} from '@angular/core/testing';

import {AccountDetailResolverService} from './account-detail-resolver.service';

describe('AccountDetailResolverService', () => {
  let service: AccountDetailResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountDetailResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
