import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {catchError, map, tap} from 'rxjs/operators';
import {forkJoin, Observable, of} from 'rxjs';

import {UserService} from '../services/user.service';
import {DictionaryService} from '../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {SessionService} from '../../auth/session/infrastructure/services/session.service';
import {getOpportunities} from '../entities/opportunity/application/getOpportunities';
import {getDictionaries} from '../../shared/entities/dictionary/application/getDictionaries';
import {getMe} from '../entities/user/application/getMe';
import {OpportunityService} from '../services/opportunity.service';
import {getUserInfo} from '../../auth/session/application/getUserInfo';
import {getDictionaryById} from '../../shared/entities/dictionary/application/getDictionaryById';
import {joinOpportunity} from '../entities/opportunity/application/joinOpportunity';
import {IOpportunityManagementResolved} from '../entities/opportunity/domain/IOpportunityManagementResolved';
import {AccountService} from '../services/account.service';
import {ContactService} from '../services/contact.service';
import {getAccounts} from '../entities/account/application/getAccounts';
import {getContacts} from '../entities/contact/application/getContacts';
import {joinAccount} from '../entities/account/application/joinAccount';
import {joinContact} from '../entities/contact/application/joinContact';
import {StateService} from '../services/state.service';
import {getStates} from '../entities/state/application/getStates';
import {CarService} from 'src/app/internal/car/infrastructure/services/car.service';
import {getCars} from 'src/app/internal/car/application/getCars';
import {getUsers} from '../entities/user/application/getUsers';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {SubcarService} from '../../internal/subcar/infrastructure/services/subcar.service';
import {getSubCars} from '../../internal/subcar/application/getSubCars';


@Injectable({
    providedIn: 'any'
})
export class OpportunityManagementResolverService implements Resolve<IOpportunityManagementResolved> {

    constructor(private _userService: UserService,
                private _dictionaryService: DictionaryService,
                private _opportunityService: OpportunityService,
                private _sessionService: SessionService,
                private _accountService: AccountService,
                private _contactService: ContactService,
                private _stateService: StateService,
                private _carService: CarService,
                private _subcarService: SubcarService,
                private _cookieService : CookieFacadeService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IOpportunityManagementResolved | Observable<IOpportunityManagementResolved> | Promise<IOpportunityManagementResolved> {
        const filtersCache = this._cookieService.get('opportunityFilterCache');
        const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('opportunity-table-config'));

        const params = {page: 0, size: 100};
        //const opportunitySearchParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
        const orderParams = {...tableFiltersConfig?.order} ?? {};
        const $myUser = getUserInfo(this._sessionService);
        const $myCrm = getMe(this._userService);
        const $users = getUsers(params, this._userService);
        const $opportunities = getOpportunities({...JSON.parse(filtersCache),...params,...orderParams}, this._opportunityService);
        const $dictionaries = getDictionaries(params, this._dictionaryService);
        const $accounts = getAccounts(params, this._accountService);
        const $contacts = getContacts(params, this._contactService);
        const $orderStates = getStates(params, this._stateService);
        const $cars = getCars({page: 0, size: 200}, this._carService);
        const $subcars = getSubCars({page: 0, size: 200}, this._subcarService);

        return forkJoin(
            $myUser,
            $myCrm,
            $cars,
            $dictionaries,
            $opportunities,
            $accounts,
            $contacts,
            $orderStates,
            $subcars,
            $users
        ).pipe(
            tap(response => {
                response[4].content = response[4].content.map(opportunity => joinOpportunity(opportunity, response[3], [], response[2], response[8]));
            }),
            map(response => ({
                tableData: {
                    myUser: response[0],
                    myCrm: response[1],
                    crmUsers: response[9],
                    accounts: response[5].map(account => joinAccount(account, response[3], [], response[2], response[8])),
                    contacts: response[6].map(contact => joinContact(contact, response[3], [], response[2], response[8])),
                    opportunities: response[4],
                    orderStates: response[7],
                    rejectionReasons: getDictionaryById(response[3], 'MOTIVORECHAZO').values,
                    cars: response[2],
                    subCars: response[8],
                    territories: getDictionaryById(response[3], 'TERRITORIO').values,
                    priorities: getDictionaryById(response[3], 'PRIORIDAD').values,
                    states: getDictionaryById(response[3], 'ESTADO').values,
                    probabilities: getDictionaryById(response[3], 'PROBABILIDAD').values,
                    filtersValues:filtersCache,
                    tableFiltersConfig: tableFiltersConfig
                }
            })),
            catchError(error => {
                return of({tableData: null, error: {message: 'Error on opportunity table resolver, data couldn\'t be fetched', error}});
            }));
    }


}

