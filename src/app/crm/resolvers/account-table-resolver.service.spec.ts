import {TestBed} from '@angular/core/testing';

import {AccountTableResolverService} from './account-table-resolver.service';

describe('AccountTableResolverService', () => {
  let service: AccountTableResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountTableResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
