import {Injectable} from '@angular/core';
import {IContactDetailDataResolved} from '../entities/contact/domain/IContactDetailDataResolved';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {ContactService} from '../services/contact.service';
import {UserService} from '../services/user.service';
import {DictionaryService} from '../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {getSingleContact} from '../entities/contact/application/getSingleContact';
import {getMe} from '../entities/user/application/getMe';
import {getDictionaries} from '../../shared/entities/dictionary/application/getDictionaries';
import {getUsers} from '../entities/user/application/getUsers';
import {catchError, map} from 'rxjs/operators';
import {getDictionaryById} from '../../shared/entities/dictionary/application/getDictionaryById';
import {joinContact} from '../entities/contact/application/joinContact';
import {getCars} from '../../internal/car/application/getCars';
import {CarService} from '../../internal/car/infrastructure/services/car.service';
import {getSubCars} from '../../internal/subcar/application/getSubCars';
import {SubcarService} from '../../internal/subcar/infrastructure/services/subcar.service';

@Injectable({
    providedIn: 'root'
})
export class ContactDetailResolverService implements Resolve<IContactDetailDataResolved> {

    constructor(
        private _contactService: ContactService,
        private _userService: UserService,
        private _dictionaryService: DictionaryService,
        private _carService: CarService,
        private _subCarsService: SubcarService
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IContactDetailDataResolved> | Promise<IContactDetailDataResolved> | IContactDetailDataResolved {
        const $myCrm = getMe(this._userService);
        const $crmUsers = getUsers({page: 0, size: 100}, this._userService);
        const $contact = getSingleContact(route.params['id'], this._contactService);
        const $dictionaries = getDictionaries({page: 0, size: 100}, this._dictionaryService);
        const $cars = getCars({page: 0, size: 100}, this._carService);
        const $subCars = getSubCars({page: 0, size: 100}, this._subCarsService);
        
        return forkJoin($myCrm, $crmUsers, $contact, $dictionaries, $cars, $subCars)
            .pipe(
                map(response => ({
                    detailData: {
                        myCrm: response[0],
                        crmUsers: response[1],
                        contact: joinContact(response[2], response[3], response[1], response[4], response[5]),
                        cars: response[4],
                        subCars: response[5],
                        categories: getDictionaryById(response[3], 'CATEGORIA').values,
                        priorities: getDictionaryById(response[3], 'PRIORIDAD').values,
                        territories: getDictionaryById(response[3], 'TERRITORIO').values,
                        relationshipLevels: getDictionaryById(response[3], 'NIVEL_RELACION').values,
                        activityTypes: getDictionaryById(response[3], 'TIPOACTIVIDAD').values,
                        activityStates: getDictionaryById(response[3], 'ESTADOACTIVIDAD').values,
                        opportunityRejectionReasons: getDictionaryById(response[3], 'MOTIVORECHAZO').values,
                        opportunityStates: getDictionaryById(response[3], 'ESTADO').values,
                        opportunityProbabilities: getDictionaryById(response[3], 'PROBABILIDAD').values
                    }
                })),
                catchError(error => {
                    return of({
                        detailData: null,
                        error: {message: 'Error on contact detail resolver, data couldn\'t be fetched', error: error}
                    })
                })
            );
    }
}
