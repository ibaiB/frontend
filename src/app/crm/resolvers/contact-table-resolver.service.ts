import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {getUsers} from '../entities/user/application/getUsers';
import {getDictionaries} from '../../shared/entities/dictionary/application/getDictionaries';
import {getMe} from '../entities/user/application/getMe';
import {DictionaryService} from '../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {ContactService} from '../services/contact.service';
import {getUserInfo} from '../../auth/session/application/getUserInfo';
import {SessionService} from '../../auth/session/infrastructure/services/session.service';
import {UserService} from '../services/user.service';
import {catchError, map, tap} from 'rxjs/operators';
import {getDictionaryById} from '../../shared/entities/dictionary/application/getDictionaryById';
import {joinContact} from '../entities/contact/application/joinContact';
import {forkJoin, Observable, of} from 'rxjs';
import {IContactTableResolved} from '../entities/contact/domain/IContactTableResolved';
import {CarService} from 'src/app/internal/car/infrastructure/services/car.service';
import {getCars} from 'src/app/internal/car/application/getCars';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {SubcarService} from '../../internal/subcar/infrastructure/services/subcar.service';
import {getSubCars} from '../../internal/subcar/application/getSubCars';


@Injectable({
    providedIn: 'any'
})
export class ContactTableResolverService implements Resolve<IContactTableResolved> {

    constructor(private _userService: UserService,
                private _dictionaryService: DictionaryService,
                private _contactService: ContactService,
                private _sessionService: SessionService,
                private _carService: CarService,
                private _subCarsService: SubcarService,
                private _cookieService : CookieFacadeService) {
    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<IContactTableResolved> | Promise<IContactTableResolved> | IContactTableResolved {
        let params = {page: 0, size: 100};
        const filtersCache = this._cookieService.get('contactFilterCache');
        const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('contact-table-config'));
        const contactSearchParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
        const orderParams = {...tableFiltersConfig?.order} ?? {};
        
        const $myUser = getUserInfo(this._sessionService);
        const $myCrm = getMe(this._userService);
        const $users = getUsers(params, this._userService);
        const $dictionaries = getDictionaries(params, this._dictionaryService);
        const $contacts = this._contactService.search({...JSON.parse(filtersCache),...contactSearchParams,...orderParams});
        const $cars = getCars(params, this._carService);
        const $subCars = getSubCars(params, this._subCarsService);
        
        return forkJoin($myUser, $myCrm, $users, $dictionaries, $contacts, $cars, $subCars)
        .pipe(
            tap(response => {
                response[4].content = response[4].content.map(contact => joinContact(contact, response[3], response[2], response[5], response[6]))
            }),
            map(response => ({
                tableData: {
                    myUser: response[0],
                    myCrm: response[1],
                    users: response[2],
                    cars: response[5],
                    subCars: response[6],
                    territories: getDictionaryById(response[3], 'TERRITORIO').values,
                    relationshipLevels: getDictionaryById(response[3], 'NIVEL_RELACION').values,
                    contacts: response[4],
                    filtersValues:filtersCache,
                    tableFiltersConfig: tableFiltersConfig
                }
            })),
            catchError(error => {
                return of({tableData: null, error: {message: 'Error on contact table resolver, data couldn\'t be fetched', error: error}});
            })
        );

    }
}
