import {TestBed} from '@angular/core/testing';

import {ContactDetailResolverService} from './contact-detail-resolver.service';

describe('ContactDetailResolverService', () => {
  let service: ContactDetailResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContactDetailResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
