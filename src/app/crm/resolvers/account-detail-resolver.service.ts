import {Injectable} from '@angular/core';
import {AccountService} from '../services/account.service';
import {SessionService} from '../../auth/session/infrastructure/services/session.service';
import {UserService} from '../services/user.service';
import {DictionaryService} from '../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {getUserInfo} from '../../auth/session/application/getUserInfo';
import {getMe} from '../entities/user/application/getMe';
import {getUsers} from '../entities/user/application/getUsers';
import {getDictionaries} from '../../shared/entities/dictionary/application/getDictionaries';
import {getSingleAccount} from '../entities/account/application/getSingleAccount';
import {catchError, map} from 'rxjs/operators';
import {getDictionaryById} from '../../shared/entities/dictionary/application/getDictionaryById';
import {joinAccount} from '../entities/account/application/joinAccount';
import {IAccountDetailResolved} from '../entities/account/domain/IAccountDetailResolved';
import {getCars} from 'src/app/internal/car/application/getCars';
import {CarService} from 'src/app/internal/car/infrastructure/services/car.service';
import {SubcarService} from '../../internal/subcar/infrastructure/services/subcar.service';
import {getSubCars} from '../../internal/subcar/application/getSubCars';

@Injectable({
  providedIn: 'any'
})
export class AccountDetailResolverService implements Resolve<IAccountDetailResolved> {

  constructor(private _accountService: AccountService,
              private _sessionService: SessionService,
              private _userService: UserService,
              private _dictionaryService: DictionaryService,
              private _carService: CarService,
              private _subcarService: SubcarService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAccountDetailResolved> | Promise<IAccountDetailResolved> | IAccountDetailResolved {
    let params = {page: 0, size: 100};
    const $myUser = getUserInfo(this._sessionService);
    const $myCrm = getMe(this._userService);
    const $users = getUsers(params, this._userService);
    const $dictionaries = getDictionaries(params, this._dictionaryService);
    //@TODO not certain if this is the correct way to obtain id
    const $account = getSingleAccount(route.params['id'], this._accountService);
    const $cars = getCars(params, this._carService);
    const $subcars = getSubCars(params, this._subcarService);


    return forkJoin($myUser, $myCrm, $users, $dictionaries, $account, $cars, $subcars)
        .pipe(
            map(response => ({
              detailData: {
                myUser: response[0],
                myCrm: response[1],
                account: joinAccount(response[4], response[3], response[2], response[5], response[6]),
                crmUsers: response[2],
                cars: response[5],
                subCars: response[6],
                categories: getDictionaryById(response[3], 'CATEGORIA').values,
                priorities: getDictionaryById(response[3], 'PRIORIDAD').values,
                territories: getDictionaryById(response[3], 'TERRITORIO').values,
                sectors: getDictionaryById(response[3], 'SECTOR').values,
                relationshipLevels: getDictionaryById(response[3], 'NIVEL_RELACION').values,
                activityTypes: getDictionaryById(response[3], 'TIPOACTIVIDAD').values,
                activityStates: getDictionaryById(response[3], 'ESTADOACTIVIDAD').values,
                opportunityRejectionReasons: getDictionaryById(response[3], 'MOTIVORECHAZO').values,
                opportunityStates: getDictionaryById(response[3], 'ESTADO').values,
                opportunityProbabilities: getDictionaryById(response[3], 'PROBABILIDAD').values
              }
            })),
            catchError(error => {
              return of({detailData: null, error: {message: 'Error on account detail resolver, data couldn\'t be fetched', error: error}})
            })
        );
  }
}
