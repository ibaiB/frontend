import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {catchError, map, tap} from 'rxjs/operators';
import {forkJoin, Observable, of} from 'rxjs';

import {DictionaryService} from '../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {SessionService} from '../../auth/session/infrastructure/services/session.service';
import {UserService} from '../services/user.service';
import {getUsers} from '../entities/user/application/getUsers';
import {getDictionaries} from '../../shared/entities/dictionary/application/getDictionaries';
import {getMe} from '../entities/user/application/getMe';
import {getUserInfo} from '../../auth/session/application/getUserInfo';
import {getDictionaryById} from '../../shared/entities/dictionary/application/getDictionaryById';
import {IActivityTableResolved} from '../entities/activity/domain/IActivityTableResolved';
import {ActivityService} from '../services/activity.service';
import {joinActivity} from '../entities/activity/application/joinActivity';
import {getCars} from '../../internal/car/application/getCars';
import {CarService} from '../../internal/car/infrastructure/services/car.service';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {SubcarService} from '../../internal/subcar/infrastructure/services/subcar.service';
import {getSubCars} from '../../internal/subcar/application/getSubCars';


@Injectable({
  providedIn: 'any'
})
export class ActivityTableResolverService implements Resolve<IActivityTableResolved> {

  constructor(
      private _userService: UserService,
      private _dictionaryService: DictionaryService,
    private _activityService: ActivityService,
    private _sessionService: SessionService,
    private _carsService: CarService,
    private _subCarsService: SubcarService,
    private _cookieService : CookieFacadeService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IActivityTableResolved | Observable<IActivityTableResolved> | Promise<IActivityTableResolved> {

    const params = { page: 0, size: 100 };
    const filtersCache = this._cookieService.get('activityFilterCache');
    const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('activity-table-config'));
    const activitySearchParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
    const orderParams = {...tableFiltersConfig?.order} ?? {};

    const $myUser = getUserInfo(this._sessionService);
    const $myCrm = getMe(this._userService);
    const $users = getUsers(params, this._userService);
    const $activities = this._activityService.search({...JSON.parse(filtersCache),...activitySearchParams,...orderParams});
    const $dictionaries = getDictionaries(params, this._dictionaryService);
    const $cars = getCars(params,this._carsService);
    const $subCars = getSubCars(params,this._subCarsService);


    return forkJoin($myUser, $myCrm, $users, $cars, $dictionaries, $activities, $subCars)
    .pipe(
      tap(response => {
          response[5].content = response[5].content.map(activity => joinActivity(activity, response[4], response[2], response[3], response[6]))
      }),
        map(response => ({
          tableData: {
            myUser: response[0],
            myCrm: response[1],
            users: response[2],
            cars: response[3],
            subCars: response[6],
            territories: getDictionaryById(response[4], 'TERRITORIO').values,
            priorities: getDictionaryById(response[4], 'PRIORIDAD').values,
            states: getDictionaryById(response[4], 'ESTADOACTIVIDAD').values,
            types: getDictionaryById(response[4], 'TIPOACTIVIDAD').values,
            categories: getDictionaryById(response[4], 'CATEGORIA').values,
            activities: response[5],
            filtersValue:filtersCache,
            tableFiltersConfig: tableFiltersConfig
          }
        })), catchError(error => {
          return of({
            tableData: null,
            error: {
              message: 'Error on activity table resolver, data couldn\'t be fetched',
              error
            }
          });
        }));
  }

}
