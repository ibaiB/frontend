import {Injectable} from '@angular/core';
import {SessionService} from '../../auth/session/infrastructure/services/session.service';
import {UserService} from '../services/user.service';
import {DictionaryService} from '../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {getUserInfo} from '../../auth/session/application/getUserInfo';
import {getMe} from '../entities/user/application/getMe';
import {getUsers} from '../entities/user/application/getUsers';
import {getDictionaries} from '../../shared/entities/dictionary/application/getDictionaries';
import {catchError, map} from 'rxjs/operators';
import {getDictionaryById} from '../../shared/entities/dictionary/application/getDictionaryById';
import {OpportunityService} from '../services/opportunity.service';
import {getSingleOpportunity} from '../entities/opportunity/application/getSingleOpportunity';
import {joinOpportunity} from '../entities/opportunity/application/joinOpportunity';
import {IOpportunityDetailResolved} from '../entities/opportunity/domain/IOpportunityDetailResolved';
import {CarService} from 'src/app/internal/car/infrastructure/services/car.service';
import {getCars} from 'src/app/internal/car/application/getCars';
import {SubcarService} from '../../internal/subcar/infrastructure/services/subcar.service';
import {getSubCars} from '../../internal/subcar/application/getSubCars';

@Injectable({
  providedIn: 'any'
})
export class OpportunityDetailResolverService implements Resolve<IOpportunityDetailResolved> {

  constructor(private _opportunityService: OpportunityService,
              private _sessionService: SessionService,
              private _userService: UserService,
              private _dictionaryService: DictionaryService,
              private _carService: CarService,
              private _subcarService: SubcarService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IOpportunityDetailResolved> | Promise<IOpportunityDetailResolved> | IOpportunityDetailResolved {
    let params = {page: 0, size: 100};
    const $myUser = getUserInfo(this._sessionService);
    const $myCrm = getMe(this._userService);
    const $users = getUsers(params, this._userService);
    const $dictionaries = getDictionaries(params, this._dictionaryService);
    const $opportunity = getSingleOpportunity(route.params['id'], this._opportunityService);
    const $cars = getCars(params, this._carService);
    const $subcars = getSubCars(params, this._subcarService);

    return forkJoin($myUser, $myCrm, $users, $cars, $subcars, $dictionaries, $opportunity)
        .pipe(
            map(response => ({
              detailData: {
                myUser: response[0],
                myCrm: response[1],
                opportunity: joinOpportunity(response[6], response[5], response[2], response[3], response[4]),
                crmUsers: response[2],
                cars: response[3],
                subCars: response[4],
                categories: getDictionaryById(response[5], 'CATEGORIA').values,
                priorities: getDictionaryById(response[5], 'PRIORIDAD').values,
                territories: getDictionaryById(response[5], 'TERRITORIO').values,
                sectors: getDictionaryById(response[5], 'SECTOR').values,
                relationshipLevels: getDictionaryById(response[5], 'NIVEL_RELACION').values,
                activityTypes: getDictionaryById(response[5], 'TIPOACTIVIDAD').values,
                activityStates: getDictionaryById(response[5], 'ESTADOACTIVIDAD').values,
                rejectionReasons: getDictionaryById(response[5], 'MOTIVORECHAZO').values,
                states: getDictionaryById(response[5], 'ESTADO').values,
                probabilities: getDictionaryById(response[5], 'PROBABILIDAD').values
              }
            })),
            catchError(error => {
              return of({detailData: null, error: {message: 'Error on opporunity detail resolver, data couldn\'t be fetched', error: error}})
            })
        );
  }
}
