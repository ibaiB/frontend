import {Injectable} from '@angular/core';
import {ActivityService} from '../services/activity.service';
import {SessionService} from '../../auth/session/infrastructure/services/session.service';
import {UserService} from '../services/user.service';
import {DictionaryService} from '../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {getUserInfo} from '../../auth/session/application/getUserInfo';
import {getMe} from '../entities/user/application/getMe';
import {getUsers} from '../entities/user/application/getUsers';
import {getDictionaries} from '../../shared/entities/dictionary/application/getDictionaries';
import {getSingleActivity} from '../entities/activity/application/getSingleActivity';
import {catchError, map} from 'rxjs/operators';
import {getDictionaryById} from '../../shared/entities/dictionary/application/getDictionaryById';
import {joinActivity} from '../entities/activity/application/joinActivity';
import {IActivityDetailResolved} from '../entities/activity/domain/IActivityDetailResolved';
import {getCars} from '../../internal/car/application/getCars';
import {CarService} from '../../internal/car/infrastructure/services/car.service';
import {SubcarService} from '../../internal/subcar/infrastructure/services/subcar.service';
import {getSubCars} from '../../internal/subcar/application/getSubCars';

@Injectable({
  providedIn: 'any'
})
export class ActivityDetailResolverService implements Resolve<IActivityDetailResolved> {

  constructor(private _activityService: ActivityService,
              private _sessionService: SessionService,
              private _userService: UserService,
              private _dictionaryService: DictionaryService,
              private _carsService: CarService,
              private _subCarsService: SubcarService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IActivityDetailResolved> | Promise<IActivityDetailResolved> | IActivityDetailResolved {
    let params = {page: 0, size: 100};
    const $myUser = getUserInfo(this._sessionService);
    const $myCrm = getMe(this._userService);
    const $users = getUsers(params, this._userService);
    const $dictionaries = getDictionaries(params, this._dictionaryService);
    const $cars = getCars(params,this._carsService);
    const $subCars = getSubCars(params,this._subCarsService);
    //@TODO not certain if this is the correct way to obtain id
    const $activity = getSingleActivity(route.params['id'], this._activityService);

    return forkJoin($myUser, $myCrm, $users, $cars, $dictionaries, $activity, $subCars)
        .pipe(
            map(response => ({
              detailData: {
                myUser: response[0],
                myCrm: response[1],
                activity: joinActivity(response[5], response[4], response[2], response[3],response[6]),
                crmUsers: response[2],
                cars: response[3],
                subCars: response[6],
                categories: getDictionaryById(response[4], 'CATEGORIA').values,
                priorities: getDictionaryById(response[4], 'PRIORIDAD').values,
                territories: getDictionaryById(response[4], 'TERRITORIO').values,
                sectors: getDictionaryById(response[4], 'SECTOR').values,
                relationshipLevels: getDictionaryById(response[4], 'NIVEL_RELACION').values,
                activityTypes: getDictionaryById(response[4], 'TIPOACTIVIDAD').values,
                activityStates: getDictionaryById(response[4], 'ESTADOACTIVIDAD').values,
                opportunityRejectionReasons: getDictionaryById(response[4], 'MOTIVORECHAZO').values,
                opportunityStates: getDictionaryById(response[4], 'ESTADO').values,
                opportunityProbabilities: getDictionaryById(response[4], 'PROBABILIDAD').values,
              }
            })),
            catchError(error => {
              return of({detailData: null, error: {message: 'Error on activity detail resolver, data couldn\'t be fetched', error: error}})
            })
        );
  }
}
