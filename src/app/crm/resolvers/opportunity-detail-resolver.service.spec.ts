import {TestBed} from '@angular/core/testing';

import {OpportunityDetailResolverService} from './opportunity-detail-resolver.service';

describe('OpportunityDetailResolverService', () => {
  let service: OpportunityDetailResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpportunityDetailResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
