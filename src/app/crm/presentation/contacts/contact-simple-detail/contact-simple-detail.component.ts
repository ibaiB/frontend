import {Component, Inject, OnInit} from '@angular/core';
import {IContact} from '../../../entities/contact/domain/IContact';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';


@Component({
    selector: 'app-contact-simple-detail',
    templateUrl: './contact-simple-detail.component.html',
    styleUrls: ['./contact-simple-detail.component.scss']
})
export class ContactSimpleDetailComponent implements OnInit {


    contact: IContact;
    loading: boolean = true;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<ContactSimpleDetailComponent>,
        private router: Router,
    ){}

    ngOnInit() {
        this.showValues();
        
    }

    closeDialog() {
        this.dialogRef.close();
    }

    goToDetails(id: string) {
        this.dialogRef.close();
        this.router.navigate([`./app/crm/contacts/contact-detail/${id}`]);
    }

    showValues(){
        this.data.contact.carValue = this.data.contact.car && this.data.contact.car.length===2  ? this.data.carDictionary.content.filter(car=>car.id === this.data.contact.car)[0].name : this.data.contact.car;
        this.data.contact.subCarValue = this.data.contact.subCar && this.data.contact.subCar.length===2? this.data.subCarDictionary.content.filter(subcar=>subcar.id === this.data.contact.subCar)[0].name: this.data.contact.subCar;
        this.data.contact.accountValue = this.data.contact.account?.commercialName ?? this.data.contact.accountId;
    }

}
