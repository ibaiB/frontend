import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ContactSimpleDetailComponent} from './contact-simple-detail.component';

describe('ContactSimpleDetailComponent', () => {
  let component: ContactSimpleDetailComponent;
  let fixture: ComponentFixture<ContactSimpleDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactSimpleDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactSimpleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
