import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

import {IContact} from '../../../entities/contact/domain/IContact';
import {CreateContactComponent} from '../create-contact/create-contact.component';
import {IContactSliderListData} from '../../../entities/contact/domain/IContactSliderListData';

@Component({
    selector: 'app-contact-slider-list',
    templateUrl: './contact-slider-list.component.html',
    styleUrls: ['./contact-slider-list.component.scss']
})
export class ContactSliderListComponent implements OnInit {

    @Input() contactSliderListData: IContactSliderListData;

    @Output() newContact = new EventEmitter<IContact>();

    constructor(public dialog: MatDialog) {
    }

    ngOnInit() {
    }

    openContactDialog() {
        const dialogRef = this.dialog.open(CreateContactComponent, {
            data: {
                myCrm: this.contactSliderListData.myCrm,
                account: this.contactSliderListData.account,
                relationshipLevels: this.contactSliderListData.relationshipLevels,
                cars: this.contactSliderListData.cars,
                subCars: this.contactSliderListData.subCars,
                territories: this.contactSliderListData.territories
            }
        });

        dialogRef
            .afterClosed()
            .subscribe(contact => {
                if (contact !== undefined) {
                    this.newContact.emit(contact);
                }
            });
    }

}
