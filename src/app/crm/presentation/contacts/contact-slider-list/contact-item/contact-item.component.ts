import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

import {IContact} from '../../../../entities/contact/domain/IContact';
import {ContactSimpleDetailComponent} from '../../contact-simple-detail/contact-simple-detail.component';
import {IDictionaryValue} from '../../../../../shared/entities/dictionary/domain/IDictionaryValue';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent implements OnInit {

  photo: string;

  @Input() contact: IContact;
  @Input() carDictionary: IDictionaryValue[];
  @Input() subCarDictionary: IDictionaryValue[];

  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.photo = this.contact.idLinkImage;
  }

  openSimpleDetailDialog() {
    const dialogRef = this.dialog.open(ContactSimpleDetailComponent, {
      data: {
        contact: this.contact,
        carDictionary: this.carDictionary,
        subCarDictionary: this.subCarDictionary,
      }
    });
  }

}
