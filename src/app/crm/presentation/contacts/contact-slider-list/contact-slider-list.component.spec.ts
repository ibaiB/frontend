import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ContactSliderListComponent} from './contact-slider-list.component';

describe('ContactSliderListComponent', () => {
  let component: ContactSliderListComponent;
  let fixture: ComponentFixture<ContactSliderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactSliderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactSliderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
