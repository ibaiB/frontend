import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IActivity} from '../../../../entities/activity/domain/IActivity';
import {IOpportunity} from '../../../../entities/opportunity/domain/IOpportunity';
import {IActivitySliderListData} from '../../../../entities/activity/domain/IActivitySliderListData';
import {IOpportunitySliderListData} from '../../../../entities/opportunity/domain/IOpportunitySliderListData';
import {ActivitySliderListDataMapper} from '../../../../entities/activity/infrastructure/activity-slider-list-data-mapper';
import {OpportunitySliderListDataMapper} from '../../../../entities/opportunity/infrastructure/opportunity-slider-list-data-mapper';
import {IContactSliderData} from '../../../../entities/contact/domain/IContactSliderData';
import {INoteSliderListData} from '../../../../entities/note/domain/INoteSliderListData';

@Component({
  selector: 'app-contact-slider',
  templateUrl: './contact-slider.component.html',
  styleUrls: ['./contact-slider.component.scss']
})
export class ContactSliderComponent implements OnInit {

  activitySliderListData: IActivitySliderListData;
  opportunitySliderListData: IOpportunitySliderListData;
  noteSliderListData: INoteSliderListData;

  @Input() contactSliderData: IContactSliderData;
  @Output() addedActivity = new EventEmitter<IActivity>();
  @Output() addedOpportunity = new EventEmitter<IOpportunity>();
  @Output() modActivity = new EventEmitter<any>();
  @Output() modOpportunity = new EventEmitter<any>();

  constructor(
      private _activitySliderListDataMapper: ActivitySliderListDataMapper,
      private _opportunitySliderListDataMapper: OpportunitySliderListDataMapper) {
  }

  ngOnInit(): void {
    this.opportunitySliderListData = this._opportunitySliderListDataMapper.mapTo(this.contactSliderData, this.contactSliderData.contact);
    this.activitySliderListData = this._activitySliderListDataMapper.mapTo(this.contactSliderData, this.contactSliderData.contact,'Contact');
    this.noteSliderListData = {father: this.contactSliderData.contact, type: 'Contact'};
  }

  addActivity(event: any) {
    //Propagate up information
    this.addedActivity.emit(event);
  }

  addOpportunity(event: any) {
    //Propagate up information
    this.addedOpportunity.emit(event);
  }

  editActivity(event: any) {
    //Propagate up information
    this.modActivity.emit(event);
  }

  editOpportunity(event: any) {
    //Propagate up information
    this.modOpportunity.emit(event);
  }

}
