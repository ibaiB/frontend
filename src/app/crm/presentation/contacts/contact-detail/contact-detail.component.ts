import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ContactService} from '../../../services/contact.service';
import {IContact} from '../../../entities/contact/domain/IContact';
import {addContactOpportunity} from 'src/app/crm/entities/contact/application/addOportunity';
import {addContactActivity} from 'src/app/crm/entities/contact/application/addActivity';
import {editContact} from '../../../entities/contact/application/editContact';
import {IAccount} from '../../../entities/account/domain/IAccount';
import {OpportunityOutMapper} from '../../../entities/opportunity/infrastructure/opportunity-out-mapper';
import {ActivityMapper} from '../../../entities/activity/infrastructure/activity-mapper';
import {ContactOutMapper} from '../../../entities/contact/infrastructure/contact-out-mapper';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {ICRMUser} from '../../../entities/user/domain/ICRMUser';
import {IContactSliderData} from '../../../entities/contact/domain/IContactSliderData';
import {IContactDetailDataResolved} from '../../../entities/contact/domain/IContactDetailDataResolved';
import {IContactEditData} from '../../../entities/contact/domain/IContactEditData';
import {ContactEditDataMapper} from '../../../entities/contact/infrastructure/contact-edit-data-mapper';
import {ContactSliderDataMapper} from '../../../entities/contact/infrastructure/contact-slider-data-mapper';
import {updateActivity} from '../../../entities/activity/application/updateActivity';
import {updateOpportunity} from '../../../entities/opportunity/application/updateOpportunity';
import {ActivityService} from '../../../services/activity.service';
import {OpportunityService} from '../../../services/opportunity.service';
import {joinOpportunity} from '../../../entities/opportunity/application/joinOpportunity';
import {joinActivity} from '../../../entities/activity/application/joinActivity';
import {successAlert} from 'src/app/shared/error/custom-alerts';

@Component({
    selector: 'app-contact-detail',
    templateUrl: './contact-detail.component.html',
    styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent implements OnInit {

    private _contactDetailDataResolved: IContactDetailDataResolved;

    contactSliderData: IContactSliderData;
    contactEditData: IContactEditData;

    contact: IContact;
    contactAccount: IAccount;
    mine: ICRMUser;
    crmUsers: ICRMUser[];

    loading: boolean = true;

    carDictionary: IDictionaryType;
    subCarDictionary: IDictionaryType;
    activityTypeDictionary: IDictionaryType;
    activityStateDictionary: IDictionaryType;
    activityPriorityDictionary: IDictionaryType;
    opportunityStateDictionary: IDictionaryType;
    territoryDictionary: IDictionaryType;
    opportunityProbabilityDictionary: IDictionaryType;
    rejectionReasonDictionary: IDictionaryType;
    categoryDictionary: IDictionaryType;
    relationshipDictionary: IDictionaryType;

    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        private _contactEditDataMapper: ContactEditDataMapper,
        private _contactSliderDataMapper: ContactSliderDataMapper,
        private contactService: ContactService,
        private opportunityOutMapper: OpportunityOutMapper,
        private activityMapper: ActivityMapper,
        private contactOutMapper: ContactOutMapper,
        private _activityService: ActivityService,
        private _opportunityService: OpportunityService,
    ) {
    }

    ngOnInit() {
        this._contactDetailDataResolved = this._route.snapshot.data['response'];

        if (this._contactDetailDataResolved.detailData !== null) {

            this.contactSliderData = this._contactSliderDataMapper.mapTo(this._contactDetailDataResolved.detailData);
            this.contactEditData = this._contactEditDataMapper.mapTo(this._contactDetailDataResolved.detailData);
            this.contact = this._contactDetailDataResolved.detailData.contact;
            this.loading = false;
        }
    }

    editContact(event: any) {
        editContact(this.contactOutMapper.mapFrom(event), this.contactService)
            .subscribe(edContact => {
                this.contact = edContact;
                successAlert('The activity was edited successfully!');
            });
    }

    addActivity(event: any) {
        addContactActivity(this.activityMapper.mapFrom(event), this.contact.id, this.contactService)
            .subscribe(edContact => {
                this.contact.activities = edContact.activities.map(activity => joinActivity(activity, this._dictionariesToArray(), this._contactDetailDataResolved.detailData.crmUsers, this._contactDetailDataResolved.detailData.cars, this._contactDetailDataResolved.detailData.subCars));
                successAlert('The activity was added successfully!');
            });
    }

    addOpportunity(event: any) {
        addContactOpportunity(this.opportunityOutMapper.mapFrom(event), this.contact.id, this.contactService)
            .subscribe(edContact => {
                this.contact.opportunities = edContact.opportunities.map(opportunity => joinOpportunity(opportunity, this._dictionariesToArray(), this._contactDetailDataResolved.detailData.crmUsers, this._contactDetailDataResolved.detailData.cars, this._contactDetailDataResolved.detailData.subCars));
                successAlert('The opportunity was added successfully!');
            });
    }

    editActivity(event: any) {
        updateActivity(this.activityMapper.mapFrom(event), this._activityService)
            .subscribe(edActivity => {
                for (let i = 0; i < this.contact.activities.length; i++) {
                    if (this.contact.activities[i].id === edActivity.id) {
                        this.contact.activities.splice(i, 1);
                        this.contact.activities.push(joinActivity(edActivity, this._dictionariesToArray(), this._contactDetailDataResolved.detailData.crmUsers, this._contactDetailDataResolved.detailData.cars, this._contactDetailDataResolved.detailData.subCars));
                    }
                }
                successAlert('The activity was added successfully!');
            });
    }

    editOpportunity(event: any) {
        updateOpportunity(this.opportunityOutMapper.mapFrom(event), this._opportunityService)
            .subscribe(newOpportunity => {
                for (let i = 0; i < this.contact.opportunities.length; i++) {
                    if (this.contact.opportunities[i].id === newOpportunity.id) {
                        this.contact.opportunities.splice(i, 1);
                        this.contact.opportunities.push(joinOpportunity(newOpportunity, this._dictionariesToArray(), this._contactDetailDataResolved.detailData.crmUsers, this._contactDetailDataResolved.detailData.cars, this._contactDetailDataResolved.detailData.subCars));
                    }
                }
                successAlert('The opportunity was added successfully!');
            });
    }

    private _dictionariesToArray() {
        let dictionaries = [];
        dictionaries.push({id: 'CAR', values: this._contactDetailDataResolved.detailData.cars});
        dictionaries.push({id: 'SUBCAR', values: this._contactDetailDataResolved.detailData.subCars});
        dictionaries.push({id: 'TERRITORIO', values: this._contactDetailDataResolved.detailData.territories});
        dictionaries.push({id: 'CATEGORIA', values: this._contactDetailDataResolved.detailData.categories});
        dictionaries.push({id: 'PRIORIDAD', values: this._contactDetailDataResolved.detailData.priorities});
        dictionaries.push({id: 'NIVEL_RELACION', values: this._contactDetailDataResolved.detailData.relationshipLevels});
        dictionaries.push({id: 'ESTADOACTIVIDAD', values: this._contactDetailDataResolved.detailData.activityStates});
        dictionaries.push({id: 'TIPOACTIVIDAD', values: this._contactDetailDataResolved.detailData.activityTypes});
        dictionaries.push({id: 'PROBABILIDAD', values: this._contactDetailDataResolved.detailData.opportunityProbabilities});
        dictionaries.push({id: 'ESTADO', values: this._contactDetailDataResolved.detailData.opportunityStates});
        dictionaries.push({id: 'MOTIVORECHAZO', values: this._contactDetailDataResolved.detailData.opportunityRejectionReasons});
        return dictionaries;
    }
}
