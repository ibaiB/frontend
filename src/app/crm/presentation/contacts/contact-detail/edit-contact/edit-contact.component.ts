import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import Swal from 'sweetalert2';

import {IContact} from '../../../../entities/contact/domain/IContact';
import {IContactEditData} from '../../../../entities/contact/domain/IContactEditData';
import {FileService} from '../../../../../shared/file/infrastructure/services/file-service.service';
import {uploadFile} from '../../../../../shared/file/application/upload';
import {FileUploadQuery, imageQuery} from '../../../../../shared/file/domain/FileUploadQuery';

@Component({
    selector: 'app-edit-contact',
    templateUrl: './edit-contact.component.html',
    styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent implements OnInit {

    @Input() contactEditData: IContactEditData;

    @Output() modContact = new EventEmitter<IContact>();

    filteredTerritoryOptions: Observable<string[]>;
    filteredCarOptions: Observable<string[]>;
    filteredSubCarOptions: Observable<string[]>;
    filteredRelationshipOptions: Observable<string[]>;

    territoryOptions: string[] = [];
    carOptions: string[] = [];
    subCarOptions: string[] = [];
    relationshipOptions: string[] = [];
    userOptions: string[] = [];
    width = '100%';

    paises = [
        {value: 'España'},
        {value: 'Francia'},
        {value: 'Rusia'},
        {value: 'Alemania'},
        {value: 'EEUU'},
        {value: 'Brasil'},
        {value: 'Colombia'},
        {value: 'China'},
    ];


    form: FormGroup;
    userValue;
    userFiltered;
    territoryValue;
    territoryFiltered;
    carValue;
    carFiltered;
    subCarValue;
    subCarFiltered;
    relationshipValue;
    relationshipFiltered;

    photo: string;
    newPhoto: File;
    newPhotoId: string;
    fileUploadQuery: FileUploadQuery = imageQuery;

    constructor(
        private fb: FormBuilder,
        private _fileService: FileService,
    ) {
    }

    ngOnInit() {
        this.territoryOptions = this.contactEditData.territories.map(territory => territory.name);
        this.carOptions = this.contactEditData.cars.content.map(car => car.name);
        this.subCarOptions=this.contactEditData.contact.car ? this.contactEditData.cars.content.filter(car=>car.name===this.contactEditData.contact.car)[0].subCars.map(subcar => subcar.name) 
                                      :this.contactEditData.subCars.content.map(subcar => subcar.name);
        this.relationshipOptions = this.contactEditData.relationshipLevels.map(relationshipLevel => relationshipLevel.name);
        this.userOptions = this.contactEditData.crmUsers.map(user => user.email);

        this.formInit();
        this.photo = this.contactEditData.contact.idLinkImage;
    }

    onSubmit() {
    }

    formInit() {

        this.territoryValue = this.contactEditData.contact.territory;
        this.carValue = this.contactEditData.contact.car;
        this.subCarValue = this.contactEditData.contact.subCar;
        this.userValue= this.contactEditData.contact.user?.email;
        this.relationshipValue = this.contactEditData.contact.relationshipLevel;

        this.form = this.fb.group({
            name: [this.contactEditData.contact.name, Validators.required],
            surname: [this.contactEditData.contact.surname],
            surname2: [this.contactEditData.contact.surname2],
            email: [this.contactEditData.contact.email, [Validators.required, Validators.email]],
            city: [this.contactEditData.contact.city],
            address: [this.contactEditData.contact.address],
            postCode: [this.contactEditData.contact.postCode],
            province: [this.contactEditData.contact.province],
            accountId: [this.contactEditData.contact.accountId],
            industry: [this.contactEditData.contact.industry],
            phone1: [this.contactEditData.contact.phone1],
            type: [this.contactEditData.contact.type],
            leadState: [this.contactEditData.contact.leadState],
            lifeCycleState: [this.contactEditData.contact.lifeCycleState],
            source: [this.contactEditData.contact.source],
            country: [this.contactEditData.contact.country],
            region: [this.contactEditData.contact.region],
            userId: [this.userValue],
            position: [this.contactEditData.contact.position],
            lastContacted: [this.contactEditData.contact.lastContacted],
            id: [this.contactEditData.contact.id],
            phone2: [this.contactEditData.contact.phone2],
            territory: [this.territoryValue],
            car: [this.carValue],
            subCar: [this.subCarValue],
            charge: [this.contactEditData.contact.position],
            area: [this.contactEditData.contact.area],
            relationship: [this.relationshipValue],
            created: [this.contactEditData.contact.created]
        });

        this.filteredTerritoryOptions = this.form.controls.territory.valueChanges.pipe(
            startWith(''),
            map(value => this
                .territoryOptions
                .filter(option => option.toLowerCase().includes(value.toString().toLowerCase())))
        );

        this.filteredCarOptions = this.form.controls.car.valueChanges.pipe(
            startWith(''),
            map(value => this
                .carOptions
                .filter(option => option.toLowerCase().includes(value.toString().toLowerCase())))
        );

        this.filteredSubCarOptions = this.form.controls.subCar.valueChanges.pipe(
            startWith(''),
            map(value => this
                .subCarOptions
                .filter(option => option.toLowerCase().includes(value.toString().toLowerCase())))
        );

        this.filteredRelationshipOptions = this.form.controls.relationship.valueChanges.pipe(
            startWith(''),
            map(value => this
                .relationshipOptions
                .filter(option => option.toLowerCase().includes(value.toString().toLowerCase())))
        );
        this.form.get('car').valueChanges.subscribe(carValue => this.carValueChanged(carValue));
    }

    carValueChanged(carValue: string) {
        let car = this.contactEditData.cars.content.find(car => car.name === carValue)
        if (car) this.subCarOptions = car.subCars.map(subCar => subCar.name)
        else this.subCarOptions = this.contactEditData.subCars.content.map(subCar => subCar.name);
    }

    photoChanged($event) {
        this.newPhoto = $event;
        this.fileUploadQuery.uploadName = $event.name;
    }

    filterTerritory(value) {
        this.form.controls.territory.setValue(value);
    }

    filterCar(value) {
        this.form.controls.car.setValue(value);
    }

    filterSubCar(value) {
        this.form.controls.subCar.setValue(value);
    }

    filterRelationship(value) {
        this.form.controls.relationship.setValue(value);
    }

    autocompleteSelected(event: { formField: string, value: string }) {
        this.form.controls[event.formField].setValue(event.value);
    }

    validateInputs() {
        if (this.form.valid) {
            let returned = {};
            const keys = Object.keys(this.form.value);
            keys.forEach(key => returned = {...returned, ...{key: this.form.value[key]}});
            return returned;
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops... Looks like you are missing some fields',
                text: 'Please check that all compulsory fields are filled!',
                confirmButtonColor: '#db5e5e'
            });
        }
    }

    applyChanges() {
        if (this.newPhoto) this._updatePhotoAndContact();
        else this._updateContact();
    }

    openEmail(email: string){
        window.location.href = `mailto:${email}?subject=Subject&body=message%20goes%20here`;
    }

    _updatePhotoAndContact() {
        uploadFile(
            this.newPhoto,
            this.fileUploadQuery,
            this._fileService
        ).subscribe(
            (retFile) => {
                this.newPhotoId = retFile.id;
                this._updateContact();
            }
        );
    }

    _updateContact() {
        if (this.form.valid) {

            this.userValue = this.form.get('userId').value;
            this.userFiltered = this.contactEditData.crmUsers.filter(user => user.email === this.userValue)[0];

            this.territoryValue = this.form.get('territory').value;
            this.territoryFiltered = this.contactEditData.territories.filter(territory => territory.name === this.territoryValue)[0];
            this.carValue = this.form.get('car').value;
            this.carFiltered = this.contactEditData.cars.content.filter(car => car.name === this.carValue)[0];
            this.subCarValue = this.form.get('subCar').value;
            this.subCarFiltered = this.contactEditData.subCars.content.filter(subCar => subCar.name === this.subCarValue)[0];
            this.relationshipValue = this.form.get('relationship').value;
            this.relationshipFiltered = this.contactEditData.relationshipLevels.filter(relationship => relationship.name === this.relationshipValue)[0];

            this.modContact.emit({
                id: this.contactEditData.contact.id,
                name: this.form.get('name').value,
                surname: this.form.get('surname').value,
                surname2: this.form.get('surname2').value,
                email: this.form.get('email').value,
                city: this.form.get('city').value,
                address: this.form.get('address').value,
                postCode: this.form.get('postCode').value,
                province: this.form.get('province').value,
                industry: this.form.get('industry').value,
                phone1: this.form.get('phone1').value,
                phone2: this.form.get('phone2').value,
                leadState: this.form.get('leadState').value,
                source: this.form.get('source').value,
                country: this.form.get('country').value,
                region: this.form.get('region').value,
                position: this.form.get('charge').value,
                lastContacted: this.form.get('lastContacted').value,
                area: this.form.get('area').value,
                created: this.form.get('created').value,
                userId: this.userFiltered !== undefined ? this.userFiltered.id : this.contactEditData.contact.userId,
                territory: this.territoryFiltered !== undefined ? this.territoryFiltered.value : this.contactEditData.contact.territory,
                car: this.carFiltered !== undefined ? this.carFiltered.id : this.contactEditData.contact.car,
                subCar: this.subCarFiltered !== undefined ? this.subCarFiltered.id : this.contactEditData.contact.subCar,
                lifeCycleState: this.form.get('charge').value ? this.form.get('lifeCycleState').value : this.contactEditData.contact.lifeCycleState,
                relationshipLevel: this.relationshipFiltered !== undefined ? this.relationshipFiltered.value : this.contactEditData.contact.relationshipLevel,
                // Estos campos no están en el html
                emails: this.contactEditData.contact.emails,
                activities: this.contactEditData.contact.activities,
                accountId: this.contactEditData.contact.accountId,
                calls: this.contactEditData.contact.calls,
                notes: this.contactEditData.contact.notes,
                opportunities: this.contactEditData.contact.opportunities,
                type: this.contactEditData.contact.type,
                account: this.contactEditData.contact.account,
                idLinkImage: this.newPhotoId ?? null,
                user:null
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops... Looks like you are missing some fields',
                text: 'Please check that all compulsory fields are filled!',
                confirmButtonColor: '#db5e5e'
            });
        }
    }
}
