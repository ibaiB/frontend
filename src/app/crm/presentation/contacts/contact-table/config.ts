import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
    multiSelect: false,
    columns: [
      {
        name: 'Id',
        prop: 'id',
        shown: true,
        route: './app/crm/contacts/contact-detail/',
        cellClass: 'id-table-column',
        routeId: 'id'
      },
      {
        name: "Name",
        prop: "name",
        shown: true,
      },
      {
        name: "SurName",
        prop: "surname",
        shown: true,
      },
      {
        name: "Charge",
        prop: "position",
        shown: true,
      },
      {
        name: "Area",
        prop: "area",
        shown: true,
      },
      {
        name: "Car",
        prop: "car",
        shown: true,
      },
      {
        name: "SubCar",
        prop: "subCar",
        shown: true,
      },
      {
        name: "Territory",
        prop: "territory",
        shown: true,
      },
      {
        name: "Assigned User",
        prop: "userEmail",
        shown: false,
      },
      {
        name: "Relationship level",
        prop: "relationshipLevel",
        shown: false,
      },
      {
        name: "Phone",
        prop: "phone1",
        shown: false,
      },
      {
        name: "Email",
        prop: "email",
        shown: false,
      },
      {
        name: 'Assigned Account',
        prop: 'accountName',
        shown: false,
        cellClass: 'id-table-column',
        route: './app/crm/accounts/account-detail/',
        routeId: 'accountId'
      }
    ],
  };
