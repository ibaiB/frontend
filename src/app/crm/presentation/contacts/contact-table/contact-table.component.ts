import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {joinSimpleContact} from 'src/app/crm/entities/contact/application/joinSimpleContact';
import {parseValues} from '../../../../shared/entities/dictionary/application/parseValues';
import {createContact} from '../../../entities/contact/application/createContact';
import {IContactTableResolved} from '../../../entities/contact/domain/IContactTableResolved';
import {ContactOutMapper} from '../../../entities/contact/infrastructure/contact-out-mapper';
import {ContactTableMapper} from '../../../entities/contact/infrastructure/contact-table-mapper';
import {ContactService} from '../../../services/contact.service';
import {CreateContactComponent} from '../create-contact/create-contact.component';
import {IActivity} from '../../../entities/activity/domain/IActivity';
import {PageEvent} from '@angular/material/paginator';
import {getUsers} from 'src/app/crm/entities/user/application/getUsers';
import {UserService} from 'src/app/crm/services/user.service';
import {getAccounts} from 'src/app/crm/entities/account/application/getAccounts';
import {AccountService} from 'src/app/crm/services/account.service';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from '../../../../shared/custom-mat-elements/dynamic';
import {table} from './config';
import {IList} from '../../../../shared/generics/IList';
import {DynamicTableMarkIiComponent} from 'src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ContactMapper} from 'src/app/crm/entities/contact/infrastructure/contact-mapper';


@Component({
    selector: 'app-contact-table',
    templateUrl: './contact-table.component.html',
    styleUrls: ['./contact-table.component.scss'],
})
export class ContactTableComponent implements OnInit {
    private _resolved: IContactTableResolved;

    tableConfig: ITableConfig = table;
    tableData: IList<any>;
    queryPagination;
    queryOrder = {orderField: '', order: ''};
    queryFilters;
    filters: IAuto[];
    size: number = 10;
    loading: boolean = true;
    query = {page: 0, size: 100};
    configVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private _route: ActivatedRoute,
        private _contactService: ContactService,
        private _fb: FormBuilder,
        public dialog: MatDialog,
        private _contactTableMapper: ContactTableMapper,
        private _contactOutMapper: ContactOutMapper,
        private _router: Router,
        private _userService: UserService,
        private _accountService: AccountService,
        private _contactMapper: ContactMapper,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit() {
        this._resolved = this._route.snapshot.data['response'];

        if (this._resolved.tableData) {
            this._buildFilters();
            parseTableFiltersConfig(this._resolved.tableData.tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = this._resolved.tableData.filtersValues ? this._clean((JSON.parse(this._resolved.tableData.filtersValues))) : {};
            this.tableConfig.pagination = this._resolved.tableData.tableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = this._resolved.tableData.tableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries(this._contactMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
            this.tableData = this._resolved.tableData.contacts;
            this.tableData.content = this.tableData.content.map(ContactTableComponent._extend);
            this.loading = false;
        }
        this.configFilterValue();
        this._updateSubCarOptions();
    }

    configFilterValue() {
        let queryFilterAux = this._resolved.tableData.filtersValues ? this._clean(this._contactMapper.mapTo(JSON.parse(this._resolved.tableData.filtersValues))) : {};

        queryFilterAux['car'] = queryFilterAux['car'] ? this._resolved.tableData.cars.content.find(car => car.id === queryFilterAux['car']).name : '';
        queryFilterAux ['subCar'] = queryFilterAux['subCar'] ? this._resolved.tableData.subCars.content.find(subcar => subcar.id === queryFilterAux['subCar']).name : '';
        queryFilterAux['territory'] = queryFilterAux['territory'] ? this._resolved.tableData.territories.find(territory => territory.value === queryFilterAux['territory']).name : '';
        queryFilterAux['accountId'] = queryFilterAux['accountId'] ? this.tableData.content.find(contact => contact.accountId === queryFilterAux['accountId'])?.accountName : '';

        queryFilterAux['userId'] = queryFilterAux['userId'] ? this._resolved.tableData.users.find(user => user.id === queryFilterAux['userId']).email : '';
        this.filters.forEach(filter => {
            filter.defaultValue = queryFilterAux[this.getFilterProp(filter)];
            filter.value = queryFilterAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    private _querySwitcher(query: any, _resolved, filters) {
        const user = _resolved.tableData.users.filter(user => user.email === query.userId)[0];
        if (user) {
            query.userId = user.id;
        }

        const car = _resolved.tableData.cars.content.filter(car => car.name === query.car)[0];
        if (car) {
            query.car = car.id;
        }

        const subCar = _resolved.tableData.subCars.content.filter(subCar => subCar.name === query.subCar)[0];
        if (subCar) {
            query.subCar = subCar.id;
        }

        const territory = _resolved.tableData.territories.find(territory => territory.name === query.territory);
        if (territory) {
            query.territory = territory.value;
        }

        const relationshipLevel = _resolved.tableData.relationshipLevels.find(relationshipLevel => relationshipLevel.name === query.relationshipLevel);
        if (relationshipLevel) {
            query.relationshipLevel = relationshipLevel.value;
        }
        const accountId = this.tableData.content.filter(contact => contact.accountName === query.accountId)[0];
        if (accountId) {
            query.accountId = accountId;
        }
        return query;
    }

    private static _extend(a: any): any {
        return {
            ...a,
            accountName: a.account ? a.account.commercialName : a.idAccount,
            userEmail: a.user ? a.user.email : a.idUser
        };
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('contact-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    backSearch() {
        this._contactService.search({...this.queryPagination, ...this.queryOrder, ...this.queryFilters})
            .subscribe((result) => {
                this.tableData = {
                    content: result.content.map(contact => joinSimpleContact(contact, this._dictionariesToArray(), this._resolved.tableData.users, this._resolved.tableData.cars, this._resolved.tableData.subCars)),
                    totalElements: result.totalElements,
                    numberOfElements: result.numberOfElements,
                    totalPages: result.totalPages
                };
                this.tableData.content = this.tableData.content.map(ContactTableComponent._extend);
            });
        this._updateSubCarOptions();
    }

    private _updateSubCarOptions() {
        const subCarFilter = this.filters.find(filter => filter.searchValue === 'subCar');

        if (this.queryFilters.hasOwnProperty('idCar')) {
            const carId = this.queryFilters['idCar'];
            const car = this._resolved.tableData.cars.content.find(car => car.id === carId);
            if (car) {
                subCarFilter.options = car.subCars.map(subCar => subCar.name);
            }
        } else {
            subCarFilter.options = this._resolved.tableData.subCars.content.map(subCar => subCar.name);
        }
    }

    changeOptions(event) {
        if (event.searchValue === 'userId') {
            getUsers({email: event.value}, this._userService).subscribe(users => {
                this.filters.find(filter => filter.label === 'Assigned User').options = users;
            });
        }
        if (event.searchValue === 'accountId') {
            getAccounts({nombreComercial: event.value}, this._accountService).subscribe(accounts => {
                this.filters.find(filter => filter.label === 'Assigned Account').options = accounts;
            });
        }
    }

    openNewContactDialog() {
        const dialogRef = this.dialog.open(CreateContactComponent, {
            data: {},
        });

        dialogRef.afterClosed().subscribe(
            (contact) => {
                if (contact !== undefined) {
                    const newContact = null;

                    createContact(newContact, this._contactService)
                        .subscribe(newContact => {
                            //TODO:
                        });
                }
            });
    }

    doAction(event: { action: string; item: IActivity }) {
        console.log('action', event);
    }

    navigate(event: { id: string; property: string }) {
        if (event.property === 'id') {
            this._router.navigate([`./app/contact-detail/${event.id}`], {relativeTo: this._route});
        }
        if (event.property === 'idAccount') {
            this._router.navigate([`./app/crm/accounts/account-detail/${event.id}`]);
        }
    }

    setSize(event: PageEvent) {
        this.backSearch();
    }

    private _dictionariesToArray() {
        let dictionaries = [];
        dictionaries.push({id: 'TERRITORIO', values: this._resolved.tableData.territories});
        dictionaries.push({id: 'NIVEL_RELACION', values: this._resolved.tableData.relationshipLevels});

        return dictionaries;
    }


    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const switched = this._querySwitcher(params, this._resolved, this.filters);
            const mapped = this._contactOutMapper.mapFrom(switched);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('contactFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('contact-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('contact-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        if (event.orderField === 'userEmail') {
            event.orderField = 'userId';
        }
        if (event.orderField === 'accountName') {
            event.orderField = 'accountId';
        }

        let aux = this._contactOutMapper.mapFrom({[event.orderField]: ''});

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('contact-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    private _buildFilters() {
        this.filters = [
            {
                prop: 'id',
                retProp: '',
                type: 'input',
                label: 'Id',
                placeholder: 'id',
                options: [],
                shown: true,
                appearance: 'standard',
                class: 'input'
            },
            {
                prop: 'name',
                retProp: '',
                type: 'input',
                label: 'Name',
                placeholder: 'Name',
                options: [],
                shown: true,
                appearance: 'standard',
                class: 'input',
            },
            {
                prop: 'surname',
                retProp: '',
                type: 'input',
                label: 'SurName',
                placeholder: 'Surname',
                options: [],
                shown: true,
                appearance: 'standard',
                class: 'input',
            },
            {
                prop: 'position',
                retProp: '',
                type: 'input',
                label: 'Charge',
                placeholder: 'Charge',
                options: [],
                shown: true,
                appearance: 'standard',
                class: 'input',
            },
            {
                prop: 'area',
                retProp: '',
                type: 'input',
                label: 'Area',
                placeholder: 'Area',
                options: [],
                shown: false,
                appearance: 'standard',
                class: 'input',
            },
            {
                prop: '',
                retProp: '',
                searchValue: 'car',
                type: 'autocomplete',
                label: 'Car',
                placeholder: '',
                options: this._resolved.tableData.cars.content.map(car => car.name),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: '',
                retProp: '',
                searchValue: 'subCar',
                type: 'autocomplete',
                label: 'SubCar',
                placeholder: '',
                options: this._resolved.tableData.subCars.content.map(subcar => subcar.name),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: '',
                retProp: '',
                type: 'autocomplete',
                searchValue: 'territory',
                label: 'Territory',
                placeholder: '',
                options: parseValues(this._resolved.tableData.territories),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: '',
                retProp: '',
                type: 'autocomplete',
                searchValue: 'userId',
                label: 'Assigned User',
                placeholder: '',
                options: this._resolved.tableData.users.map(user => user.email),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: '',
                retProp: '',
                type: 'autocomplete',
                searchValue: 'relationshipLevel',
                label: 'Relationship level',
                placeholder: '',
                options: parseValues(this._resolved.tableData.relationshipLevels),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: 'phone1',
                retProp: '',
                type: 'input',
                label: 'Phone',
                placeholder: '',
                options: [],
                shown: false,
                appearance: 'standard',
                class: 'input',
            },
            {
                prop: 'email',
                retProp: '',
                type: 'input',
                label: 'Email',
                placeholder: 'Email',
                options: [],
                shown: false,
                appearance: 'standard',
                class: 'input',
            },
            {
                prop: 'commercialName',
                retProp: '',
                type: 'autoselect',
                options: [],
                shown: false,
                searchValue: 'accountId',
                appearance: 'standard',
                class: 'autoselect',
                label: 'Assigned Account',
                placeholder: 'Assigned Account',
                defaultValue: ''
            },
        ];
    }
}
