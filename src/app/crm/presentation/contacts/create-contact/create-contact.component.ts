import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.scss']
})
export class CreateContactComponent implements OnInit {

  form: FormGroup;

  defaultCar: string = '';
  defaultSubCar: string = '';
  defaultTerritory: string = '';

  relationshipOptions: string[] = [];
  carOptions: string[] = [];
  subCarOptions: string[] = [];
  territoryOptions: string[] = [];

  constructor(
      private fb: FormBuilder,
      public dialogRef: MatDialogRef<CreateContactComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.formInit();
    console.log(this.data)
    this.relationshipOptions = this.data.relationshipLevels.map(relationship => relationship.name);
    this.carOptions = this.data.cars.content.map(car => car.name);
    this.subCarOptions=this.data.account.car ? this.data.cars.content.filter(car=>car.name===this.data.account.car)[0].subCars.map(subcar => subcar.name) 
                                      : this.data.subCars.content.map(subcar => subcar.name);
                                      console.log(this.data);
    this.territoryOptions = this.data.territories.map(territory => territory.name);
  }

  formInit() {
    this.defaultTerritory = this.data.account.territory !== undefined ? this.data.account.territory : '';
    this.defaultCar = this.data.account.car !== undefined ? this.data.account.car : '';
    this.defaultSubCar = this.data.account.subCar !== undefined ? this.data.account.subCar : '';

    this.form = this.fb.group({
      name: ['', Validators.required],
      surname: [''],
      charge: [''],
      area: [''],
      user: [`${this.data.myCrm.email}`],
      relationship: [''],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      account: [`${this.data.account.cif}`],
      car: [this.defaultCar],
      subCar: [this.defaultSubCar],
      territory: [this.defaultTerritory]
    })
    this.form.get('car').valueChanges.subscribe(carValue => this.carValueChanged(carValue));
  }

  carValueChanged(carValue: string) {
    let car = this.data.cars.content.find(car => car.name === carValue)
    if (car) this.subCarOptions = car.subCars.map(subCar => subCar.name)
    else this.subCarOptions = this.data.subCars.content.map(subCar => subCar.name);
  }

  autocompleteSelected(event: { formField: string, value: string }) {
    this.form.controls[event.formField].setValue(event.value);
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onSaveClick() {
    if(this.form.valid) {
      const relationshipValue = this.form.get('relationship').value;
      const carValue = this.form.get('car').value;
      const subCarValue = this.form.get('subCar').value;
      const territoryValue = this.form.get('territory').value;

      const relationshipFiltered = this.data.relationshipLevels.filter(rl => rl.name === relationshipValue)[0];
      const carFiltered = this.data.cars.content.filter(car => car.name === carValue)[0];
      const subCarFiltered = this.data.subCars.content.filter(subCar => subCar.name === subCarValue)[0];
      const territoryFiltered = this.data.territories.filter(territory => territory.name === territoryValue)[0];

      const result = {
        name: this.form.get('name').value,
        surname: this.form.get('surname').value,
        position: this.form.get('charge').value,
        area: this.form.get('area').value,
        userId: this.data.myCrm.id,
        relationshipLevel: relationshipFiltered !== undefined ? relationshipFiltered.value : null,
        car: carFiltered !== undefined ? carFiltered.id : null,
        subCar: subCarFiltered !== undefined ? subCarFiltered.id : null,
        email: this.form.get('email').value,
        phone1: this.form.get('phone').value,
        accountId: this.data.account.id,
        territory: territoryFiltered !== undefined ? territoryFiltered.value : null
      }

      this.dialogRef.close(result);
    }
  }

}
