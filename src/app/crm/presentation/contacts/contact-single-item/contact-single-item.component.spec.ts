import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ContactSingleItemComponent} from './contact-single-item.component';

describe('ContactItemComponent', () => {
  let component: ContactSingleItemComponent;
  let fixture: ComponentFixture<ContactSingleItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactSingleItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactSingleItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
