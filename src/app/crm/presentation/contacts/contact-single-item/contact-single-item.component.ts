import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {IContact} from 'src/app/crm/entities/contact/domain/IContact';
import {IDictionaryValue} from 'src/app/shared/entities/dictionary/domain/IDictionaryValue';
import {ContactSimpleDetailComponent} from '../contact-simple-detail/contact-simple-detail.component';


@Component({
  selector: 'app-contact-single-item',
  templateUrl: './contact-single-item.component.html',
  styleUrls: ['./contact-single-item.component.scss']
})
export class ContactSingleItemComponent implements OnInit {

  @Input() contact: IContact;
  @Input() carDictionary: IDictionaryValue[];
  @Input() subCarDictionary: IDictionaryValue[];


  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  openSimpleDetailDialog() {
    const dialogRef = this.dialog.open(ContactSimpleDetailComponent, {
      data: {
        contact: this.contact,
        carDictionary: this.carDictionary,
        subCarDictionary: this.subCarDictionary,
      }
    });
  }

}
