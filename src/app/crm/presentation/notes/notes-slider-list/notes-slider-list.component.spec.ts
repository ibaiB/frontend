import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NotesSliderListComponent} from './notes-slider-list.component';

describe('NotesSliderListComponent', () => {
  let component: NotesSliderListComponent;
  let fixture: ComponentFixture<NotesSliderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesSliderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesSliderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
