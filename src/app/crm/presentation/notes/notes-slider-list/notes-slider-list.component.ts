import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {INote} from '../../../entities/note/domain/INote';
import {INoteSliderListData} from '../../../entities/note/domain/INoteSliderListData';
import {NoteMapper} from '../../../entities/note/infrastructure/note-mapper';
import {NoteDetailComponent} from '../note-detail/note-detail.component';

@Component({
    selector: 'app-notes-slider-list',
    templateUrl: './notes-slider-list.component.html',
    styleUrls: ['./notes-slider-list.component.scss']
})
export class NotesSliderListComponent implements OnInit {

    @Input() noteSliderListData: INoteSliderListData;
    @Output() newNote = new EventEmitter<INote>();
    @Output() noteDetail = new EventEmitter<INote>();

    mappedNotes: INote;

    constructor(
        private _noteMapper: NoteMapper,
        private _dialog: MatDialog
    ) {
    }

    ngOnInit(): void {
        this._mapNotes();
    }

    private _mapNotes() {
        this.mappedNotes = this.noteSliderListData.father.notes.map(note => this._noteMapper.mapTo(note));
    }

    createNote() {

        const dialogRef = this._dialog.open(NoteDetailComponent, {
            data: {father: this.noteSliderListData.father, type: this.noteSliderListData.type}
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data) {
                this.noteSliderListData.father.notes = data.notes;
                this._mapNotes();
            }
        });
    }


    openDetail(note: INote) {
        const dialogRef = this._dialog.open(NoteDetailComponent, {
            data: {note, father: this.noteSliderListData.father, type: this.noteSliderListData.type}
        });

        dialogRef.afterClosed().subscribe(data => {
            console.log(data);
        });
    }

}
