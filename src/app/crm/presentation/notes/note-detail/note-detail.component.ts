import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import {Component, Inject, NgZone, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {forkJoin} from 'rxjs';
import {IComment} from '../../../entities/comment/domain/IComment';
import {INote} from '../../../entities/note/domain/INote';
import {NoteFacade} from '../../../entities/note/infrastructure/note-facade';
import {successAlert} from '../../../../shared/error/custom-alerts';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-note-detail',
    templateUrl: './note-detail.component.html',
    styleUrls: ['./note-detail.component.scss']
})
export class NoteDetailComponent implements OnInit {

    editTitle: boolean = false;
    editDescription: boolean = false;
    commentaries: IComment[] = [];
    note: INote;
    noteForm: FormGroup;
    commentForm: FormGroup;

    @ViewChild('autosize') autosize: CdkTextareaAutosize;

    constructor(
        public dialogRef: MatDialogRef<NoteDetailComponent>,
        private _ngZone: NgZone,
        @Inject(MAT_DIALOG_DATA) public data: {
            note: INote,
            father: any,
            type: string
        },
        private _noteFacade: NoteFacade,
        private _fb: FormBuilder) {
    }


    ngOnInit(): void {
        this.note = this.data.note;
        this.commentaries = this.note ? this.note.comments : [];
        this.noteFormInit(this.note ?? null);
        this.commentFormInit();
    }

    noteFormInit(note?: INote) {
        this.noteForm = this._fb.group({
            name: [note ? note.name : '', Validators.required],
            description: [note ? note.description : '']
        });
    }

    commentFormInit() {
        this.commentForm = this._fb.group({
            comment: ['']
        });
    }

    addCommentary() {
        if (this.commentForm.value['comment']) {
            const comment: IComment = {
                created: new Date().toISOString(),
                description: this.commentForm.value['comment']
            };
            this.commentaries.push(comment);
            this.commentForm.controls['comment'].setValue('');
        }
    }

    //TODO: this may cause problems
    deleteComment(comment) {
        const index = this.commentaries.indexOf(comment);
        if (index > -1) {
            this.commentaries.splice(index, 1);
        }

    }

    saveNote() {
        if (this.noteForm.valid) {
            const {note, father, type} = this.data;
            note ? this.update() : this.create(father, type);
        }
    }

    create(father, type) {
        const note: INote = {
            ...this.noteForm.value,
            date: new Date().toISOString(),
            comments: this.commentaries
        };

        this._noteFacade.createNote(note, father.id, type)
            .subscribe(father => {
                    successAlert('Your note was saved')
                        .then(() => this.dialogRef.close(father));
                });
    }

    addComments(comments: IComment[], note) {
        const addComments = comments.map(c => this._noteFacade.addCommentToNote(c, note.id));
        return forkJoin(addComments);
    }

    update() {
        const mod: INote = {
            id: this.note.id,
            date: this.note.date,
            ...this.noteForm.value,
            comments: this.commentaries,
        };
        this._noteFacade
            .modifyNote(mod)
            .subscribe(() => {
                    const index = this.data.father.notes.indexOf(n => n.id = mod.id);
                    this.data.father.notes[index] = mod;
                    successAlert('Your changes where saved')
                        .then(() => this.dialogRef.close(this.data.father));
                }
            );
    }
}
