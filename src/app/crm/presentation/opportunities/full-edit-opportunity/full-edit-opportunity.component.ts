import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IOpportunityDetailData} from 'src/app/crm/entities/opportunity/domain/IOpportunityDetailData';

@Component({
    selector: 'app-full-edit-opportunity',
    templateUrl: './full-edit-opportunity.component.html',
    styleUrls: ['./full-edit-opportunity.component.scss']
})
export class FullEditOpportunityComponent implements OnInit {

    @Input() data: IOpportunityDetailData;

    @Output() modOpportunity = new EventEmitter<any>();

    stateColors = [
        '#5bbcaf',
        '#83eabe',
        '#57c4e5',
        '#568eaf',
        '#975eb0',
        '#7467f0',
        '#393939',
        '#e46e75'
    ];
    color;

    width = '100%';
    
    form: FormGroup;

    defaultState: string = '';
    defaultPriority: string = '';
    defaultProbability: string = '';
    defaultCar: string = '';
    defaultSubCar: string = '';
    defaultBosonitSupport: string = '';
    defaultTerritory: string = '';
    defaultRejectionReason: string = '';
    defaultUser: string = '';


    stateOptions: string[] = [];
    territoryOptions: string[] = [];
    priorityOptions: string[] = [];
    probabilityOptions: string[] = [];
    carOptions: string[] = [];
    subCarOptions: string[] = [];
    userOptions: string[] = [];
    supportNfqOptions: string[] = [];
    supportTechOptions: string[] = [];
    rejectionOptions: string[] = [];

    constructor(private fb: FormBuilder) {}

    ngOnInit() {
        //@TODO: fix mapper to avoid this kinds of problems
        this.color = this.data.opportunity.state['orden'] ? this.stateColors[this.data.opportunity.state['orden'] - 1] : this.stateColors[this.data.opportunity.state['order'] - 1];

        this.userOptions = this.data.crmUsers.map(user => user.email);
        this.stateOptions = this.data.states.map(state => state.name);
        this.probabilityOptions = this.data.probabilities.map(probability => probability.name);
        this.priorityOptions = this.data.priorities.map(priority => priority.name);
        this.carOptions = this.data.cars.content.map(car => car.name);
        this.subCarOptions = this.data.opportunity.car ? this.data.cars.content.find(car => car.name === this.data.opportunity.car).subCars.map(subcar=> subcar.name)
                                                        :this.data.subCars.content.map(subCar => subCar.name);
        this.territoryOptions = this.data.territories.map(territory => territory.name);
        this.rejectionOptions = this.data.rejectionReasons.map(rejectionReason => rejectionReason.name);

        this.formInit();
        this.disableSupp();
    }

    // @TODO: future functionality
    disableSupp() {
        this.form.controls['supportNfq'].disable();
        this.form.controls['supportTech'].disable();
    }

    //TODO: Refactor
    formInit() {
        //@TODO: fix mapper to avoid this kinds of problems
        this.defaultState = this.data.opportunity.state['nombre'] ? this.data.opportunity.state['nombre'] : this.data.opportunity.state['name'];
        this.defaultPriority = this.data.opportunity.priority;
        this.defaultProbability = this.data.opportunity.probability;
        this.defaultCar = this.data.opportunity.car;
        this.defaultSubCar = this.data.opportunity.subcar;
        this.defaultBosonitSupport = this.data.opportunity.supportBosonit;
        this.defaultTerritory = this.data.opportunity.territory;
        this.defaultRejectionReason = this.data.opportunity.rejectionReason;
        this.defaultUser=this.data.opportunity.user?.email;

        this.form = this.fb.group({
            id: [this.data.opportunity.id],
            car: [this.defaultCar, Validators.required],
            subCar: [this.defaultSubCar, Validators.required],
            rejectionReason: [this.defaultRejectionReason],
            supportBosonit: [this.data.opportunity.supportBosonit],
            supportNfq: [''],
            supportTech: [''],
            state: [this.defaultState, Validators.required],
            priority: [this.defaultPriority],
            probability: [this.defaultProbability, Validators.required],
            territory: [this.defaultTerritory],
            name: [this.data.opportunity.name, Validators.required],
            estimatedBudget: [this.data.opportunity.estimatedBudget, [Validators.required, Validators.pattern('([0-9])+')]],
            user: [this.defaultUser],
            warningDate: [new Date(this.data.opportunity.warningDate)],
            projectClosed: [this.data.opportunity.closedProject],
            contact: [this.data.opportunity.idContact],
            account: [this.data.opportunity.idAccount],
            registerDate: [this.data.opportunity.registerDate],
            sendDate: [''],
            closeDate: [this.data.opportunity.closeDate],
            dedicatedResource: [this.data.opportunity.dedicatedResource],
            resourceNumbers: [this.data.opportunity.resourceNumbers],
            recurrentPercentage: [this.data.opportunity.recurrentPercentage]
        });

        this.form.get('car').valueChanges.subscribe(carValue => this.carValueChanged(carValue));
        this.form.get('dedicatedResource').valueChanges.subscribe(value => this.dedicatedResourceChanged(value));
        this.form.get('projectClosed').valueChanges.subscribe(value => this.projectClosedChanged(value));
    }

    autocompleteSelected(event: { formField: string, value: string }) {
        this.form.controls[event.formField].setValue(event.value);
    }

    dedicatedResourceChanged(checked: boolean) {
        if (checked) {
          this.form.get('recurrentPercentage').setValue('');
        }
    }

    projectClosedChanged(checked: boolean) {
      if (checked) {
        this.form.get('resourceNumbers').setValue('');
      }
    }

    carValueChanged(carValue: string) {
        let car = this.data.cars.content.find(car => car.name === carValue)
        if (car) this.subCarOptions = car.subCars.map(subCar => subCar.name)
        else this.subCarOptions = this.data.subCars.content.map(subCar => subCar.name);
    }

    applyChanges() {
        if (this.form.valid) {
            const carValue = this.data.cars.content.find(car => car.name === this.form.get('car').value);
            const subCarValue = this.data.subCars.content.find(subCar => subCar.name === this.form.get('subCar').value);
            const stateValue = this.data.states.find(state => state.name === this.form.get('state').value);
            const priorityValue = this.data.priorities.find(priority => priority.name === this.form.get('priority').value);
            const probabilityValue = this.data.probabilities.find(probability => probability.name === this.form.get('probability').value);
            const territoryValue = this.data.territories.find(territory => territory.name === this.form.get('territory').value);
            const supportBosonitValue = this.data.crmUsers.find(user => user.email === this.form.get('supportBosonit').value);
            const rejectionValue = this.data.rejectionReasons.find(rejectionReason => rejectionReason.name === this.form.get('rejectionReason').value);
            const opportunityNameValue = this.form.get('name').value;
            const estimatedBudgetValue = this.form.get('estimatedBudget').value;
            const warningDateValue = this.form.get('warningDate').value;
            const closeDateValue = this.form.get('closeDate').value;
            const sendDateValue = this.form.get('sendDate').value;
            const registerDateValue = this.form.get('registerDate').value;
            const userValue = this.form.get('user').value;
            const userFiltered = this.data.crmUsers.find(user => user.email === userValue);

            this.modOpportunity.emit({
                car: carValue !== undefined ? carValue.id : this.data.opportunity.car,
                subcar: subCarValue !== undefined ? subCarValue.id : this.data.opportunity.subcar,
                state: stateValue !== undefined ? stateValue.value : this.data.opportunity.state['id'],
                priority: priorityValue !== undefined ? priorityValue.value : this.data.opportunity.priority,
                probability: probabilityValue !== undefined ? probabilityValue.value : this.data.opportunity.probability,
                territory: territoryValue !== undefined ? territoryValue.value : this.data.opportunity.territory,
                rejectionReason: rejectionValue !== undefined ? rejectionValue.value : this.data.opportunity.rejectionReason,
                idUser: userFiltered !== undefined ? userFiltered.id : this.data.opportunity.idUser,
                supportBosonit: supportBosonitValue !== undefined ? supportBosonitValue.id : this.data.myCrm.id,
                supportNfq: null,
                supportTech: null,
                warningDate: warningDateValue ? new Date(warningDateValue).toISOString() : null,
                name: opportunityNameValue,
                estimatedBudget: estimatedBudgetValue,
                projectClosed: this.form.get('projectClosed').value,
                registerDate: registerDateValue,
                id: this.data.opportunity.id,
                idAccount: this.data.opportunity.idAccount,
                sendDate: sendDateValue ? new Date(sendDateValue).toISOString() : '',
                closeDate: closeDateValue ? new Date(closeDateValue).toISOString() : null,
                dedicatedResource: this.form.get('dedicatedResource').value,
                resourceNumbers: this.form.get('resourceNumbers').value,
                recurrentPercentage: this.form.get('recurrentPercentage').value
            });
        }
    }

}
