import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';

@Component({
  selector: 'app-opportunity-detail',
  templateUrl: './opportunity-detail.component.html',
  styleUrls: ['./opportunity-detail.component.scss']
})
export class OpportunityDetailComponent implements OnInit {

  stateColors = [
    '#5bbcaf',
    '#83eabe',
    '#57c4e5',
    '#568eaf',
    '#975eb0',
    '#7467f0',
    '#393939',
    '#e46e75'
  ];

  color = this.data.opportunity.state['orden'] ? this.stateColors[this.data.opportunity.state['orden'] - 1] : this.stateColors[this.data.opportunity.state['order'] - 1];

  public warningDateParsed: Date;
  public closingDateParsed: Date;
  public today: Date;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<OpportunityDetailComponent>,
    private _router: Router
  ) { }

  ngOnInit() {
    this.warningDateParsed = new Date(this.data.opportunity.warningDate);
    this.closingDateParsed = new Date(this.data.opportunity.closeDate);
    this.today = new Date();
    if(this.data.opportunity)this.showValues();
  }

  closeDialog() {
    this.dialogRef.close();
  }

  goToDetails(id: string) {
    this.dialogRef.close();
    this._router.navigate([`./app/crm/opportunities/opportunity-detail/${id}`]);
}

showValues(){
  const carValue = this.data.opportunity.car && this.data.opportunity.car.length===2  ? this.data.cars.content.filter(car=>car.id === this.data.opportunity.car)[0].name : this.data.opportunity.car;
  const subCarValue = this.data.opportunity.subcar && this.data.opportunity.subcar.length===2? this.data.subCars.content.filter(subcar=>subcar.id === this.data.opportunity.subcar)[0].name: this.data.opportunity.subcar;
  this.data.opportunity.car = carValue;
  this.data.opportunity.subcar = subCarValue;
}
}
