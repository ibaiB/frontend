import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

import {addContactActivity} from '../../../entities/contact/application/addActivity';
import {CreateOpportunityComponent} from '../create-opportunity/create-opportunity.component';
import {AccountService} from '../../../services/account.service';
import {ContactService} from '../../../services/contact.service';
import {addAccountOpportunity} from '../../../entities/account/application/addOpportunity';
import {IOpportunityManagementResolved} from 'src/app/crm/entities/opportunity/domain/IOpportunityManagementResolved';
import {ActivatedRoute} from '@angular/router';
import {IOpportunityManagementData} from '../../../entities/opportunity/domain/IOpportunityManagementData';
import {IOpportunity} from '../../../entities/opportunity/domain/IOpportunity';
import {EditOpportunityComponent} from '../edit-opportunity/edit-opportunity.component';
import {updateOpportunity} from '../../../entities/opportunity/application/updateOpportunity';
import {OpportunityOutMapper} from '../../../entities/opportunity/infrastructure/opportunity-out-mapper';
import {OpportunityService} from '../../../services/opportunity.service';
import {joinOpportunity} from '../../../entities/opportunity/application/joinOpportunity';
import {OpportunityDetailComponent} from '../opportunity-detail/opportunity-detail.component';
import {switchOpportunityKeys} from '../../../entities/opportunity/application/switchOpportunityKeys';
import {getOpportunities} from '../../../entities/opportunity/application/getOpportunities';
import {Observable} from 'rxjs';
import {getSingleAccount} from '../../../entities/account/application/getSingleAccount';
import {getSingleContact} from '../../../entities/contact/application/getSingleContact';
import {parseValues} from '../../../../shared/entities/dictionary/application/parseValues';
import {errorAlert} from 'src/app/shared/error/custom-alerts';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {IList} from 'src/app/shared/generics/IList';
import {table} from './config';
import {OpportunityGridComponent} from './opportunity-grid/opportunity-grid.component';
import {getUsers} from 'src/app/crm/entities/user/application/getUsers';
import {UserService} from 'src/app/crm/services/user.service';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {OpportunityMapper} from 'src/app/crm/entities/opportunity/infrastructure/opportunity-mapper';

@Component({
    selector: 'app-opportunity-management',
    templateUrl: './opportunity-management.component.html',
    styleUrls: ['./opportunity-management.component.scss']
})
export class OpportunityManagementComponent implements OnInit {

    private _resolved: IOpportunityManagementResolved;
    resolvedData: IOpportunityManagementData;
    opportunities: IOpportunity[];

    filters: IAuto[];
    size: number = 10;

    tableConfig: ITableConfig = table;
    tableData: IList<any>;
    pipelineData: IOpportunity[] = [];

    queryPagination;
    queryOrder;
    queryFilters;

    loading: boolean = true;
    configVisibility: boolean = false;

    @ViewChild('opportunityGrid') opportunityGrid: OpportunityGridComponent;

    constructor(
        private _accountService: AccountService,
        private _contactService: ContactService,
        private _opportunityService: OpportunityService,
        private _opportunityOutMapper: OpportunityOutMapper,
        public dialog: MatDialog,
        private _route: ActivatedRoute,
        private _userService: UserService,
        private _opportunityMapper: OpportunityMapper,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    private static _querySwitcher(query: any, _resolved) {

        const user = _resolved.tableData.crmUsers.filter(user => user.email === query.idUser)[0];
        if (user) {
            query.idUser = user.id;
        }

        const car = _resolved.tableData.cars.content.find(car => car.name === query.car);
        if (car) {
            query.car = car.id;
        }
        const subCar = _resolved.tableData.subCars.content.find(subCar => subCar.name === query.subCar);
        if (subCar) {
            query.subcar = subCar.id;
        }
        const territory = _resolved.tableData.territories.find(territory => territory.name === query.territory);
        if (territory) {
            query.territory = territory.value;
        }
        const priority = _resolved.tableData.priorities.find(priority => priority.name === query.priority);
        if (priority) {
            query.priority = priority.value;
        }
        const state = _resolved.tableData.priorities.find(state => state.name === query.state);
        if (state) {
            query.state = state.value;
        }
        const rejectionReason = _resolved.tableData.priorities.find(rejectionReason => rejectionReason.name === query.rejectionReason);
        if (rejectionReason) {
            query.rejectionReason = rejectionReason.value;
        }
        const probability = _resolved.tableData.priorities.find(probability => probability.name === query.probability);
        if (probability) {
            query.probability = probability.value;
        }
        if (query.warningDate) {
            let date = new Date(query.warningDate);
            date = new Date(date.setDate(date.getDate()));
            query.WarningDate = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
        }
        return query;
    }

    ngOnInit() {
        this._resolved = this._route.snapshot.data['response'];

        this._buildFilters();
        parseTableFiltersConfig(this._resolved.tableData.tableFiltersConfig, this.tableConfig, this.filters);
        this.queryFilters = this._resolved.tableData.filtersValues ? this._clean((JSON.parse(this._resolved.tableData.filtersValues))) : {};
        this.tableConfig.pagination = this._resolved.tableData.tableFiltersConfig?.pagination ?? {page: 0, size: 10};
        this.queryPagination = this.tableConfig.pagination;
        this.tableConfig.order = this._resolved.tableData.tableFiltersConfig?.order ?? {orderField: '', order: ''};
        this.queryOrder = {...this.tableConfig.order};
        this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
            Object.entries(this._opportunityMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
        this.resolvedData = this._resolved.tableData;
        this.tableData = {...this._resolved.tableData.opportunities};
        this.tableData.content = this.tableData.content.slice(this.queryPagination.page * this.queryPagination.size, (this.queryPagination.page * this.queryPagination.size) + this.queryPagination.size);
        this.pipelineData = this._resolved.tableData.opportunities.content;
        this.loading = false;
        this.configFilterValue();
        this._updateSubCarOptions();
    }

    configFilterValue() {
        let queryFiltersAux = this._resolved.tableData.filtersValues ? this._clean(this._opportunityMapper.mapTo(JSON.parse(this._resolved.tableData.filtersValues))) : {};

        queryFiltersAux['car'] = queryFiltersAux['car'] ? this._resolved.tableData.cars.content.find(car => car.id === queryFiltersAux['car']).name : '';
        queryFiltersAux['subcar'] = queryFiltersAux['subcar'] ? this._resolved.tableData.subCars.content.find(subcar => subcar.id === queryFiltersAux['subcar']).name : '';
        queryFiltersAux['territory'] = queryFiltersAux['territory'] ? this._resolved.tableData.territories.find(territory => territory.value === queryFiltersAux['territory']).name : '';
        queryFiltersAux['idUser'] = queryFiltersAux['idUser'] ? this._resolved.tableData.crmUsers.find(user => user.id === queryFiltersAux['idUser']).email : '';
        queryFiltersAux['warningDate'] = queryFiltersAux['warningDate'] ? new Date(queryFiltersAux['warningDate']).toISOString() : '';

        this.filters.forEach(filter => {
            filter.defaultValue = queryFiltersAux[this.getFilterProp(filter)];
            filter.value = queryFiltersAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    openNewOpportunityDialog() {
        let data = {
            myCrm: this.resolvedData.myCrm,
            crmUsers: this.resolvedData.crmUsers,
            territories: this.resolvedData.territories,
            states: this.resolvedData.states,
            cars: this.resolvedData.cars,
            subCars: this.resolvedData.subCars,
            priorities: this.resolvedData.priorities,
            probabilities: this.resolvedData.probabilities,
            accounts: this.resolvedData.accounts,
            contacts: this.resolvedData.contacts
        };

        const dialogRef = this.dialog.open(CreateOpportunityComponent, {
            data: data
        });

        dialogRef
            .afterClosed()
            .subscribe(opportunity => {
                if (opportunity) {
                    if (!opportunity.idContact) {
                        addAccountOpportunity(this._opportunityOutMapper.mapFrom(opportunity), opportunity.accountId, this._accountService)
                            .subscribe(account => {
                                    //@TODO decide what to do here push or not push
                                },
                            );
                    } else {
                        addContactActivity(this._opportunityOutMapper.mapFrom(opportunity), opportunity.contactId, this._contactService)
                            .subscribe(contact => {
                                //@TODO decide what to do here push or not push
                            });
                    }
                }
            });
    }

    openModifyOpportunity(opportunity: IOpportunity) {
        let father: Observable<any> = new Observable<any>();
        //TODO: this could be avoided if opportunity has joined data
        //TODO: this may not work in the future with future data management model
        if (opportunity.idAccount) {
            father = getSingleAccount(opportunity.idAccount, this._accountService);
        }
        if (opportunity.idContact) {
            father = getSingleContact(opportunity.idContact, this._contactService);
        }
        father.subscribe(response => {
            const dialogRef = this.dialog.open(EditOpportunityComponent, {
                data: {
                    opportunity: opportunity,
                    states: this.resolvedData.states,
                    probabilities: this.resolvedData.probabilities,
                    priorities: this.resolvedData.priorities,
                    cars: this.resolvedData.cars,
                    subCars: this.resolvedData.subCars,
                    myCrm: this.resolvedData.myCrm,
                    father: {type: opportunity.idAccount ? 'Account' : 'Contact', value: response},
                    territories: this.resolvedData.territories,
                    rejectionReasons: this.resolvedData.rejectionReasons
                }
            });

            dialogRef
                .afterClosed()
                .subscribe(edOpportunity => {
                    if (edOpportunity !== undefined) {
                        this._editOpportunity(edOpportunity);
                    }
                });
        });
    }

    //TODO: this should be merged with other functions
    changeOpportunityState(opportunity: any) {
        let switched = switchOpportunityKeys(opportunity, this._dictionariesToArray(), []);
        this._editOpportunity(switched);
    }

    openDetailOpportunityDialog(opportunity: IOpportunity) {
        let father: Observable<any> = new Observable<any>();
        //TODO: this could be avoided if opportunity has joined data
        //TODO: this may not work in the future with future data management model
        if (opportunity.idAccount) {
            father = getSingleAccount(opportunity.idAccount, this._accountService);
        }
        if (opportunity.idContact) {
            father = getSingleContact(opportunity.idContact, this._contactService);
        }
        if (opportunity.supportBosonit) {
            opportunity.supportBosonit = this.resolvedData.crmUsers.find(user => user.id === opportunity.supportBosonit)?.email ?? opportunity.supportBosonit;
        }

        father.subscribe(response => {
            const dialogRef = this.dialog.open(OpportunityDetailComponent, {
                data: {
                    father: {type: opportunity.idAccount ? 'Account' : 'Contact', value: response},
                    opportunity: opportunity
                }
            });
        });

    }

    private _editOpportunity(opportunity: any) {
        updateOpportunity(this._opportunityOutMapper.mapFrom(opportunity), this._opportunityService)
            .subscribe(returned => {
                    this.tableBackSearch();
                    this.pipelineBackSearch();
                },
                (error) => {
                    console.error('Error on edit opportunity dialog', error);
                    const {error: {message, id}} = error;
                    errorAlert(`Opportunity couldn\'t be edited`, message, id);
                });
    }

    pipelineBackSearch() {
        getOpportunities({size: 100, ...this.queryFilters}, this._opportunityService)
            .subscribe(result => {
                    this.pipelineData = result.content.map(opportunity => joinOpportunity(
                        opportunity,
                        this._dictionariesToArray(),
                        [],
                        this.resolvedData.cars,
                        this.resolvedData.subCars)
                    );
                },
                (error) => {
                    console.error('Error on back search', error);
                    const {error: {message, id}} = error;
                    errorAlert(`Something went wrong!`, message, id);
                });

        this._updateSubCarOptions();
    }

    tableBackSearch() {
        getOpportunities({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._opportunityService)
            .subscribe(result => {
                this.tableData = {
                    content: result.content.map(opportunity => joinOpportunity(
                        opportunity,
                        this._dictionariesToArray(),
                        [],
                        this.resolvedData.cars,
                        this.resolvedData.subCars)),
                    totalElements: result.totalElements,
                    numberOfElements: result.numberOfElements,
                    totalPages: result.totalPages
                };
            });

        this._updateSubCarOptions();
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('opportunity-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    private _updateSubCarOptions() {
        const subCarFilter = this.filters.find(filter => filter.prop === 'subcar');

        if (this.queryFilters.hasOwnProperty('idCar')) {
            const carId = this.queryFilters['idCar'];
            const car = this.resolvedData.cars.content.find(car => car.id === carId);
            if (car) {
                subCarFilter.options = car.subCars.map(subCar => subCar.name);
            }
        } else {
            subCarFilter.options = this.resolvedData.subCars.content.map(subCar => subCar.name);
        }
    }


    changeOptions(event: { searchValue: string, value: string }) {
        if (event.searchValue === 'idUser') {
            getUsers({email: event.value}, this._userService).subscribe(users => {
                this.filters.find(filt => filt.label === 'Assigned User').options = users;
            });
        }
    }

    private _dictionariesToArray() {
        let dictionaries = [];
        dictionaries.push({id: 'TERRITORIO', values: this.resolvedData.territories});
        dictionaries.push({id: 'PRIORIDAD', values: this.resolvedData.priorities});
        dictionaries.push({id: 'PROBABILIDAD', values: this.resolvedData.probabilities});
        dictionaries.push({id: 'ESTADO', values: this.resolvedData.states});
        dictionaries.push({id: 'MOTIVORECHAZO', values: this.resolvedData.rejectionReasons});
        return dictionaries;
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const switched = OpportunityManagementComponent._querySwitcher(params, this._resolved);
            const mapped = this._opportunityOutMapper.mapFrom(switched);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('opportunityFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('opportunity-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.opportunityGrid.resetPageIndex();

        this.pipelineBackSearch();
        this.tableBackSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('opportunity-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.tableBackSearch();
    }

    orderChanged(event: any) {
        if (event.orderField === 'userEmail') {
            event.orderField = 'idUser';
        }
        if (event.orderField === 'accountCommercialName') {
            event.orderField = 'idAccount';
        }
        if (event.orderField === 'contactName') {
            event.orderField = 'idContact';
        }
        if (event.orderField === 'stateName') {
            event.orderField = 'state';
        }
        let aux = this._opportunityOutMapper.mapFrom({[event.orderField]: ''});

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('opportunity-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.tableBackSearch();
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Id',
                placeholder: 'Id',
                shown: true
            },
            {
                options: [],
                prop: 'name',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Name',
                placeholder: 'Name',
                shown: true
            },
            {
                options: parseValues(this._resolved.tableData.priorities),
                prop: '',
                retProp: '',
                searchValue: 'priority',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Priority',
                placeholder: '',
                shown: true
            },
            {
                options: this._resolved.tableData.cars.content.map(car => car.name),
                prop: '',
                retProp: '',
                searchValue: 'car',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Car',
                placeholder: '',
                shown: true
            },
            {
                options: this._resolved.tableData.subCars.content.map(subcar => subcar.name),
                prop: 'subcar',
                retProp: '',
                searchValue: 'subCar',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Subcar',
                placeholder: '',
                shown: false,
            },
            {
                options: [],
                prop: 'estimatedBudget',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Budget',
                placeholder: 'Budget',
                shown: false
            },
            {
                options: [],
                prop: 'warningDate',
                retProp: 'warningDate',
                type: 'date',
                appearance: 'standard',
                class: 'date',
                label: 'Warning Date',
                placeholder: 'Warning date',
                shown: false
            },
            {
                options: [],
                prop: 'email',
                retProp: '',
                searchValue: 'idUser',
                type: 'autoselect',
                appearance: 'standard',
                class: 'autoselect',
                label: 'Assigned User',
                placeholder: 'Assigned user',
                shown: false
            },
            {
                options: parseValues(this._resolved.tableData.territories),
                prop: '',
                retProp: '',
                searchValue: 'territory',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Territory',
                placeholder: '',
                shown: false
            },
        ];
    }

    setSize(event) {
        this.size = event;
        this.tableBackSearch();
        this.pipelineBackSearch();
    }
}
