import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
  multiSelect: false,
  columns: [
    {
      name: 'Id',
      prop: 'id',
      shown: true,
      route: './app/crm/opportunities/opportunity-detail/',
      routeId: 'id',
      cellClass: 'id-table-column',
    },
    {
      name: "Name",
      prop: "name",
      shown: true,
    },
    {
      name: "Priority",
      prop: "priority",
      shown: true,
    },
    {
      name: "Car",
      prop: "car",
      shown: true,
    },
    {
      name: "Subcar",
      prop: "subcar",
      shown: true,
    },
    {
      name: "State",
      prop: "stateName",
      shown: true,
    },
    {
      name: "Budget",
      prop: "quantity",
      shown: true,
    },
    {
      name: "Warning Date",
      prop: "warningDate",
      type: "warning-date",
      shown: true,
    },
    {
      name: "Assigned User",
      prop: "userEmail",
      shown: false,
    },
    {
      name: "Probability",
      prop: "probability",
      shown: false,
    },
    {
      name: "Territory",
      prop: "territory",
      shown: false,
    },
    {
      name: 'Assigned Account',
      prop: 'accountCommercialName',
      shown: false,
      route: './app/crm/accounts/account-detail/',
      routeId: 'idAccount',
      cellClass: 'id-table-column',
    },
    {
      name: 'Assigned Contact',
      prop: 'contactName',
      shown: false,
      route: './app/crm/contacts/contact-detail/',
      routeId: 'idContact',
      cellClass: 'id-table-column',
    }
  ],
};
