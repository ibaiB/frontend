import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OpportunityGridComponent} from './opportunity-grid.component';

describe('OpportunityGridComponent', () => {
  let component: OpportunityGridComponent;
  let fixture: ComponentFixture<OpportunityGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunityGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunityGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
