import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import {Router} from '@angular/router';
import {IOpportunity} from '../../../../entities/opportunity/domain/IOpportunity';
import {ITableConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {IList} from 'src/app/shared/generics/IList';
import {DynamicTableMarkIiComponent} from 'src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';

@Component({
    selector: 'app-opportunity-grid',
    templateUrl: './opportunity-grid.component.html',
    styleUrls: ['./opportunity-grid.component.scss']
})
export class OpportunityGridComponent implements OnInit, OnChanges {

    @Input() config: ITableConfig;
    @Input() data: IList<IOpportunity>;

    @Output() pagination = new EventEmitter<any>();
    @Output() order = new EventEmitter<any>();
    @Output() pageSizeChanged = new EventEmitter<number>();

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(private _router: Router) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.data.content = this.data.content.map(OpportunityGridComponent._extend);
    }

    private static _extend(o: IOpportunity): any {
        o.idAccount = o.idAccount ?? o.contact.accountId;
        return {
            ...o,
            stateName: o.state['name'] ?? '',
            accountCommercialName: o.account ? o.account.commercialName : o.contact.account?.commercialName,
            contactName: o.contact ? `${o.contact.name} ${o.contact.surname ?? ''} ${o.contact.surname2 ?? ''}` : o.idContact,
            userEmail: o.user ? o.user.email : o.idUser
        };
    }

    ngOnInit() {
    }

    setSize(event: PageEvent) {
        //this.pageSizeChanged.emit(event);
    }

    paginationChanged(event: any) {
        this.pagination.emit(event);
    }

    orderChanged(event: any) {
        this.order.emit(event);
    }

    resetPageIndex() {
        this.dynamicTable.resetPageIndex();
    }
}
