import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunityManagementComponent } from './opportunity-management.component';

describe('OpportunityManagementComponent', () => {
  let component: OpportunityManagementComponent;
  let fixture: ComponentFixture<OpportunityManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunityManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunityManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
