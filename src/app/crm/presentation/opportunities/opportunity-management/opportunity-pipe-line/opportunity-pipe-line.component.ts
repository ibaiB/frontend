import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {CdkDragDrop, transferArrayItem} from '@angular/cdk/drag-drop';
import {IOpportunity} from '../../../../entities/opportunity/domain/IOpportunity';
import {IOpportunityManagementData} from '../../../../entities/opportunity/domain/IOpportunityManagementData';
import {Router} from '@angular/router';

@Component({
  selector: 'app-opportunity-pipe-line',
  templateUrl: './opportunity-pipe-line.component.html',
  styleUrls: ['./opportunity-pipe-line.component.scss'],
})
export class OpportunityPipeLineComponent implements OnChanges {

  @Input() data: IOpportunityManagementData;
  @Input() opportunities: IOpportunity[];
  //TODO this should be merged in future updates
  @Output() modOpportunity = new EventEmitter<any>();
  @Output() stateChangedOpt = new EventEmitter<any>();
  @Output() detailOpportunity = new EventEmitter<any>();

  loading = true;

  columns: {
    name: string,
    order: number,
    total: number,
    opts: number,
    state: string,
    opportunities: IOpportunity[]
  }[] = [];

  constructor(private _router: Router) { }

  drop(event: CdkDragDrop<IOpportunity[], any>) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(
          event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex
      );

      const updated = event.container.data[event.currentIndex];
      updated['state'] = event.container.id;
      this.stateChangedOpt.emit(updated);
    }
  }

  private _filterOpportunities() {
    let returned: {
      name: string,
      order: number,
      total: number,
      opts: number,
      state: string,
      opportunities: IOpportunity[]
    }[] = [];

    this.data.orderStates.forEach(state => {
      let opportunitiesInStateN = this.opportunities.filter(opportunity => opportunity.state['id'] === state.id);
      let amountInStateN = 0;
      opportunitiesInStateN.forEach(opportunity => {
        if (opportunity.quantity) {
          let quantNumber: number =+ opportunity.quantity;
          amountInStateN += quantNumber;
        }
      });

      returned.push({
        state: state.id,
        name: state.name,
        order: state.order,
        total: amountInStateN,
        opts: opportunitiesInStateN.length,
        opportunities: opportunitiesInStateN
      })

      returned.sort((a, b)=> {
        if(a.name > b.name){
          return 1
        } else {
          return -1
        }
      })
    })

    returned.sort(function (a, b) {
      if (a.order > b.order) {
        return 1
      }
      if (a.order < b.order) {
        return -1
      }
    })

    this.loading = false;

    return returned;
  }

  openEditOpportunity(opportunity: IOpportunity) {
    this._router.navigate([`./app/crm/opportunities/opportunity-detail/${opportunity.id}`]);
  }

  openDetailOpportunity(opportunity: IOpportunity) {
    this.detailOpportunity.emit(opportunity)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.columns = this._filterOpportunities();
  }

  isLate(date: string): boolean {
    return new Date(date) <= new Date();
  }
}
