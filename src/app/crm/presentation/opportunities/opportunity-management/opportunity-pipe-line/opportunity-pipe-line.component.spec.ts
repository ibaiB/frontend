import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunityPipeLineComponent } from './opportunity-pipe-line.component';

describe('OpportunityPipeLineComponent', () => {
  let component: OpportunityPipeLineComponent;
  let fixture: ComponentFixture<OpportunityPipeLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunityPipeLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunityPipeLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
