import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {IOpportunity} from '../../../../entities/opportunity/domain/IOpportunity';
import {OpportunityDetailComponent} from '../../opportunity-detail/opportunity-detail.component';
import {IOpportunityItemData} from '../../../../entities/opportunity/domain/IOpportunityItemData';
import {Router} from '@angular/router';

@Component({
    selector: 'app-opportunity-item',
    templateUrl: './opportunity-item.component.html',
    styleUrls: ['./opportunity-item.component.scss']
})
export class OpportunityItemComponent implements OnInit {

    stateColors = [
        '#5bbcaf',
        '#83eabe',
        '#57c4e5',
        '#568eaf',
        '#975eb0',
        '#7467f0',
        '#393939',
        '#e46e75'
    ];
    public color: string;

  @Input() opportunityItemData: IOpportunityItemData;
  @Output() modOpportunity = new EventEmitter<IOpportunity>();

  public warningDateParsed: Date;
  public today: Date;

  constructor(
      public dialog: MatDialog,
      private _router: Router
  ) { }

  ngOnInit() {
      this.warningDateParsed = new Date(this.opportunityItemData.opportunity.warningDate);
      this.today = new Date();
      const order = this.opportunityItemData.opportunity.state['orden'] ? this.opportunityItemData.opportunity.state['orden'] : this.opportunityItemData.opportunity.state['order']
      this.color = this.stateColors[order - 1];
  }

  // not used(?)
  opportunityOnChange(opportunity: IOpportunity) {
    const state = opportunity.state['id'];
    opportunity['idEstado'] = state.toString();
    this.modOpportunity.emit(opportunity);
  }

  openDetailOpportunity(opportunity: IOpportunity) {
      this._router.navigate([`./app/crm/opportunities/opportunity-detail/${opportunity.id}`]);
  }

  openDetailOpportunityDialog() {
    const dialogRef = this.dialog.open(OpportunityDetailComponent, {
      data:
          {
            opportunity: this.opportunityItemData.opportunity,
            father: {
              type: this.opportunityItemData.father.cif !== undefined ? 'Account' : 'Contact',
              value: this.opportunityItemData.father
            },
            crmUsers: this.opportunityItemData.crmUsers,
            me: this.opportunityItemData.myCrm,
            states: this.opportunityItemData.states,
            priorities: this.opportunityItemData.priorities,
            probabilities: this.opportunityItemData.probabilities,
            cars: this.opportunityItemData.cars,
            subCars: this.opportunityItemData.subCars,
          }
    });
  }

}
