import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import Swal from "sweetalert2";

import {IOpportunity} from '../../../entities/opportunity/domain/IOpportunity';
import {CreateOpportunityComponent} from '../create-opportunity/create-opportunity.component';
import {IOpportunitySliderListData} from "../../../entities/opportunity/domain/IOpportunitySliderListData";
import {IOpportunityItemData} from "../../../entities/opportunity/domain/IOpportunityItemData";
import {OpportunityItemDataMapper} from "../../../entities/opportunity/infrastructure/opportunity-item-data-mapper";

@Component({
  selector: 'app-opportunity-slider-list',
  templateUrl: './opportunity-slider-list.component.html',
  styleUrls: ['./opportunity-slider-list.component.scss']
})
export class OpportunitySliderListComponent implements OnInit {

  @Input() opportunitySliderListData: IOpportunitySliderListData;
  @Output() newOpportunity = new EventEmitter<IOpportunity>();
  @Output() modOpportunity = new EventEmitter<IOpportunity>();

  constructor(
      public dialog: MatDialog,
      private _opportunityItemDataMapper: OpportunityItemDataMapper
  ) { }

  ngOnInit() {

  }

  openNewOpportunityDialog() {
    let data = {
      father: {
        type: this.opportunitySliderListData.father.cif !== undefined ? 'Account' : 'Contact',
        value: this.opportunitySliderListData.father
      },
      crmUsers: this.opportunitySliderListData.crmUsers,
      myCrm: this.opportunitySliderListData.myCrm,
      states: this.opportunitySliderListData.states,
      territories: this.opportunitySliderListData.territories,
      priorities: this.opportunitySliderListData.priorities,
      probabilities: this.opportunitySliderListData.probabilities,
      cars: this.opportunitySliderListData.cars,
      subCars: this.opportunitySliderListData.subCars,
      rejectionReasons: this.opportunitySliderListData.rejectionReasons,
      //@TODO future versions
      supportTech: [],
      supportNFQ: []
    }

    const dialogRef = this.dialog.open(CreateOpportunityComponent, {
      data: data
    });

    dialogRef
      .afterClosed()
      .subscribe(opportunity => {
        if (opportunity !== undefined) this.newOpportunity.emit(opportunity);
      }, error => {
        console.error('Error on contact dialog', error);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
          confirmButtonColor: '#db5e5e'
        });
      })
  }

  modifyOpportunity(event: any) {
    this.modOpportunity.emit(event);
  }

  mapOpportunity(item: IOpportunity): IOpportunityItemData {
    return this._opportunityItemDataMapper.mapTo(this.opportunitySliderListData, item);
  }
}