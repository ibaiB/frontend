import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunitySliderListComponent } from './opportunity-slider-list.component';

describe('OpportunitySliderListComponent', () => {
  let component: OpportunitySliderListComponent;
  let fixture: ComponentFixture<OpportunitySliderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunitySliderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunitySliderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
