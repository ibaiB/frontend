import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {updateOpportunity} from 'src/app/crm/entities/opportunity/application/updateOpportunity';
import {OpportunityOutMapper} from '../../../entities/opportunity/infrastructure/opportunity-out-mapper';
import {IOpportunity} from '../../../entities/opportunity/domain/IOpportunity';
import {OpportunityService} from '../../../services/opportunity.service';
import {OpportunitySliderDataMapper} from 'src/app/crm/entities/opportunity/infrastructure/opportunity-slider-data-mapper';
import {IOpportunityDetailData} from 'src/app/crm/entities/opportunity/domain/IOpportunityDetailData';
import {IOpportunityDetailResolved} from 'src/app/crm/entities/opportunity/domain/IOpportunityDetailResolved';
import {IActivity} from 'src/app/crm/entities/activity/domain/IActivity';
import {ActivityMapper} from 'src/app/crm/entities/activity/infrastructure/activity-mapper';
import {addOpportunityActivity} from 'src/app/crm/entities/opportunity/application/addActivity';
import {joinActivity} from 'src/app/crm/entities/activity/application/joinActivity';
import {updateActivity} from 'src/app/crm/entities/activity/application/updateActivity';
import {ActivityService} from 'src/app/crm/services/activity.service';
import {successAlert} from '../../../../shared/error/custom-alerts';

@Component({
    selector: 'app-opportunity-detail',
    templateUrl: './opportunity-full-detail.component.html',
    styleUrls: ['./opportunity-full-detail.component.scss']
})
export class OpportunityFullDetailComponent implements OnInit {

    private _opportunityDetailResolved: IOpportunityDetailResolved;
    opportunitySliderData: any;
    opportunityDetailData: IOpportunityDetailData;
    opportunity: IOpportunity;

    loading: boolean = true;

    constructor(
        private _route: ActivatedRoute,
        private _opportunitySliderDataMapper: OpportunitySliderDataMapper,
        private _opportunityService: OpportunityService,
        private _activityService: ActivityService,
        private _opportunityOutMapper: OpportunityOutMapper,
        private _activityMapper: ActivityMapper) {
    }

    ngOnInit() {
        this._opportunityDetailResolved = this._route.snapshot.data['response'];

        if (this._opportunityDetailResolved.detailData !== null) {
            this.opportunitySliderData = this._opportunitySliderDataMapper.mapTo(this._opportunityDetailResolved.detailData);
            this.opportunityDetailData = this._opportunityDetailResolved.detailData;
            this.opportunity = this._opportunityDetailResolved.detailData.opportunity;
            this.loading = false;
        }
    }

    addActivity(event: any) {
        addOpportunityActivity(this._activityMapper.mapFrom(event), this._opportunityDetailResolved.detailData.opportunity.id, this._opportunityService)
            .subscribe(edOpportunity => {
                this.opportunity.activities = edOpportunity.activities.map(activity => joinActivity(
                    activity,
                    this._dictionariesToArray(),
                    this._opportunityDetailResolved.detailData.crmUsers,
                    this._opportunityDetailResolved.detailData.cars,
                    this._opportunityDetailResolved.detailData.subCars
                ));

                successAlert('The activity has been updated successfully!');
            });
    }

    modifyActivity(activity: IActivity) {
        updateActivity(this._activityMapper.mapFrom(activity), this._activityService)
            .subscribe(edActivity => {
                for (let i = 0; i < this.opportunity.activities.length; i++) {
                    if (this.opportunity.activities[i].id === edActivity.id) {
                        this.opportunity.activities.splice(i, 1);
                        this.opportunity.activities.push(
                            joinActivity(
                                edActivity,
                                this._dictionariesToArray(),
                                this._opportunityDetailResolved.detailData.crmUsers,
                                this._opportunityDetailResolved.detailData.cars,
                                this._opportunityDetailResolved.detailData.subCars
                            ));
                    }
                }
                successAlert('The activity has been updated successfully!');
            });
    }

    editOpportunity(opportunity: any) {
        console.log(opportunity);
        updateOpportunity(this._opportunityOutMapper.mapFrom(opportunity), this._opportunityService)
            .subscribe(edOpportunity => {
                this.opportunity = edOpportunity;
                successAlert('The opportunity has been updated successfully!');
            });
    }

    private _dictionariesToArray() {
        let dictionaries = [];
        dictionaries.push({id: 'CAR', values: this._opportunityDetailResolved.detailData.cars});
        dictionaries.push({id: 'SUBCAR', values: this._opportunityDetailResolved.detailData.subCars});
        dictionaries.push({id: 'TERRITORIO', values: this._opportunityDetailResolved.detailData.territories});
        dictionaries.push({id: 'CATEGORIA', values: this._opportunityDetailResolved.detailData.categories});
        dictionaries.push({id: 'PRIORIDAD', values: this._opportunityDetailResolved.detailData.priorities});
        dictionaries.push({id: 'SECTOR', values: this._opportunityDetailResolved.detailData.sectors});
        dictionaries.push({id: 'NIVEL_RELACION', values: this._opportunityDetailResolved.detailData.relationshipLevels});
        dictionaries.push({id: 'ESTADOACTIVIDAD', values: this._opportunityDetailResolved.detailData.activityStates});
        dictionaries.push({id: 'TIPOACTIVIDAD', values: this._opportunityDetailResolved.detailData.activityTypes});
        dictionaries.push({id: 'PROBABILIDAD', values: this._opportunityDetailResolved.detailData.probabilities});
        dictionaries.push({id: 'ESTADO', values: this._opportunityDetailResolved.detailData.states});
        dictionaries.push({id: 'MOTIVORECHAZO', values: this._opportunityDetailResolved.detailData.rejectionReasons});
        return dictionaries;
    }

}
