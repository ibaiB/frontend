import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunityFullDetailComponent } from './opportunity-full-detail.component';

describe('OpportunityDetailComponent', () => {
  let component: OpportunityFullDetailComponent;
  let fixture: ComponentFixture<OpportunityFullDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunityFullDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunityFullDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
