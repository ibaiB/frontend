export interface ICreateOpportunityData {
    account?: {
        name: string,
        car: string,
        subCar: string,
        territory: string
    },
    contact?: {
        name: string,
        car: string,
        subCar: string,
        territory: string
    },
    cars: string[],
    subCars: string[],
    states: string[],
    priorities: string[],
    probabilities: string[],
    territories: string[],
    crmUsers: string[],
    myCRM: string
}