import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AccountService} from '../../../services/account.service';
import {ContactService} from '../../../services/contact.service';
import {getAccounts} from '../../../entities/account/application/getAccounts';
import {getContacts} from '../../../entities/contact/application/getContacts';
import { IAuto } from '../../../../shared/custom-mat-elements/dynamic';
import { getUsers } from '../../../entities/user/application/getUsers';
import { UserService } from '../../../../crm/services/user.service';

@Component({
  selector: 'app-create-opportunity',
  templateUrl: './create-opportunity.component.html',
  styleUrls: ['./create-opportunity.component.scss']
})
export class CreateOpportunityComponent implements OnInit {

  form: FormGroup;

  defaultCar: string = '';
  defaultSubCar: string = '';
  defaultTerritory: string = '';
  defaultAccountName:string;
  defaultContactName:string;

  allMineUserOptions: string[] = [];
  allStateOptions: string[] = [];
  allProbabilityOptions: string[] = [];
  allCarOptions: string[] = [];
  allSubCarOptions: string[] = [];
  allPriorityOptions: string[] = [];
  allTerritoryOptions: string[] = [];
  allRejectionOptions: string[] = [];
  allAccountOptions: string[] = [];
  allContactOptions: string[] = [];

  accountConfig: IAuto = {
    prop: 'commercialName',
    retProp: '',
    type: 'autoselect',
    options: [],
    shown: true,
    searchValue: 'idAccount',
    appearance: 'outline',
    class: 'autoselect',
    label: 'Assigned Account',
    placeholder: 'Assigned Account',
}

supportBosonitConfig: IAuto = {
  options: [],
  prop: 'email',
  retProp: '',
  searchValue: 'supportBosonit',
  type: 'autoselect',
  appearance: 'outline',
  class: 'autoselect',
  label: 'Bosonit Support',
  placeholder: 'Bosonit Support',
  shown: true
}

  constructor(
      private _fb: FormBuilder,
      public dialogRef: MatDialogRef<CreateOpportunityComponent>,
      private _accountService: AccountService,
      private _contactService: ContactService,
      private _userService: UserService,
      @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.allMineUserOptions = this.data.crmUsers.map(user => user.email);
    this.allTerritoryOptions = this.data.territories.map(territory => territory.name);
    this.allStateOptions = this.data.states.map(state => state.name);
    this.allCarOptions = this.data.cars.content.map(car => car.name);
    this.allSubCarOptions = this.data.subCars.content.map(subCar => subCar.name);
    this.allPriorityOptions = this.data.priorities.map(priority => priority.name);
    this.allProbabilityOptions = this.data.probabilities.map(probability => probability.name);
    this.formInit();
  }

  formInit() {
    if (this.data.father !== undefined) {
      this.defaultTerritory = this.data.father.value.territory;
      this.defaultCar = this.data.father.value.car;
      this.defaultSubCar = this.data.father.value.subCar;

      this.defaultContactName= this.data.father.type === 'Contact' ? this.data.father.value.name + ' ' + this.data.father.value.surname : '';
      this.defaultAccountName = this.data.father.type === 'Account' ? this.data.father.value.commercialName : this.data.father.value.account.commercialName;

      this.form = this._fb.group({
        account: [this.defaultAccountName],
        contact:[this.defaultContactName],
        car: [this.defaultCar, Validators.required],
        subCar: [this.defaultSubCar, Validators.required],
        supportBosonit: [''],
        supportNfq: [{ value: '', disabled: true }],
        supportTech: [{ value: '', disabled: true }],
        state: ['', Validators.required],
        priority: ['Normal'],
        probability: ['', Validators.required],
        territory: [this.defaultTerritory],
        opportunityName: ['', Validators.required],
        estimatedBudget: ['', [Validators.required, Validators.pattern('([0-9])+')]],
        user: [this.data.myCrm.email, Validators.required],
        warningDate: [''],
        closeDate:[''],
        projectClosed: [false],
        dedicatedResource: [false],
        resourceNumbers: [''],
        recurrentPercentage: ['']
      });
    } else {
      this.allAccountOptions = this.data.accounts.map(account => account.commercialName);
      this.data.contacts.forEach(contact => {
        if (contact.name !== null) {
          let surname = contact.surname ? contact.surname : '';
          this.allContactOptions.push(contact.name + ' ' + surname)
        }
      })
      this.form = this._fb.group({
        accountId: [''],
        contactId: [''],
        car: [this.defaultCar, Validators.required],
        subCar: [this.defaultSubCar, Validators.required],
        supportBosonit: [''],
        supportNfq: [''],
        supportTech: [''],
        state: ['', Validators.required],
        priority: ['Normal'],
        probability: ['', Validators.required],
        territory: [this.defaultTerritory],
        opportunityName: ['', Validators.required],
        estimatedBudget: ['', [Validators.required, Validators.pattern('([0-9])+')]],
        user: [this.data.myCrm.email, Validators.required],
        warningDate: [''],
        projectClosed: [false],
        dedicatedResource: [false],
        resourceNumbers: [''],
        recurrentPercentage: [''],
        closeDate: [''],
      });
    }

    this.form.get('car').valueChanges.subscribe(carValue => this.carValueChanged(carValue));
    this.form.get('dedicatedResource').valueChanges.subscribe(value => this.dedicatedResourceChanged(value));
    this.form.get('projectClosed').valueChanges.subscribe(value => this.projectClosedChanged(value));
  }

  autocompleteSelected(event: { formField: string, value: string }) {
    this.form.controls[event.formField].setValue(event.value);
  }

  changeOptions(event: { searchValue: string, value: any }) {
    if (event.searchValue === 'idAccount') {
      getAccounts({nombreComercial: event.value}, this._accountService).subscribe((accounts) => {
        this.accountConfig = {...this.accountConfig, options: accounts};
      });
    }

    if (event.searchValue === 'supportBosonit') {
      getUsers({email: event.value}, this._userService).subscribe(users =>
        this.supportBosonitConfig = {...this.supportBosonitConfig, options: users});
      };
  }

  dedicatedResourceChanged(checked: boolean) {
    if (checked) {
      this.form.get('recurrentPercentage').setValue('');
    }
  }

  projectClosedChanged(checked: boolean) {
    if (checked) {
      this.form.get('resourceNumbers').setValue('');
    }
  }

  carValueChanged(carValue: string) {
    let car = this.data.cars.content.find(car => car.name === carValue)
    if (car) this.allSubCarOptions = car.subCars.map(subCar => subCar.name)
    else this.allSubCarOptions = this.data.subCars.content.map(subCar => subCar.name);
  }

  backSearchAccount(event: any){
    getAccounts({page: 0, size: 10, nombreComercial: event, style: 'simple'}, this._accountService)
      .subscribe(accounts => {
        this.allAccountOptions = accounts.map(account => account.commercialName);
        this.data.accounts = accounts;
      });
  }

  backSearchContact(event: any) {
    const split = event.toString().split('');
    getContacts({page: 0, size: 10, nombre: split[0], apellido1: split[1], style: 'simple'}, this._contactService)
        .subscribe(contacts => {
          this.data.contacts.forEach(contact => {
            if (contact.name !== null) {
              let surname = contact.surname ? contact.surname : '';
              this.allContactOptions.push(contact.name + ' ' + surname)
            }
          })
          this.data.contacts = contacts;
        });
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  onSaveClick() {
    if (this.form.valid) {
      const carValue = this.data.cars.content.find(car => car.name === this.form.get('car').value);
      const subCarValue = this.data.subCars.content.find(subCar => subCar.name === this.form.get('subCar').value);
      const stateValue = this.data.states.find(state => state.name === this.form.get('state').value);
      const priorityValue = this.data.priorities.find(priority => priority.name === this.form.get('priority').value);
      const probabilityValue = this.data.probabilities.find(probability => probability.name === this.form.get('probability').value);
      const territoryValue = this.data.territories.find(territory => territory.name === this.form.get('territory').value);
      const supportBosonitValue = this.form.get('supportBosonit').value.id;
      let accountIdValue;
      let contactIdValue;
      if (!this.data.father) accountIdValue = this.form.get('accountId').value.id;
      if (!this.data.father) contactIdValue = this.data.contacts.find(contact => contact.name === this.form.get('contactId').value);
      const opportunityNameValue = this.form.get('opportunityName').value;
      const estimatedBudgetValue = this.form.get('estimatedBudget').value;
      const warningDateValue = this.form.get('warningDate').value;
      const closeDateValue = this.form.get('closeDate').value;

      console.log(supportBosonitValue)
      let newOpportunity = {
        car: carValue !== undefined ? carValue.id : null,
        subCar: subCarValue !== undefined ? subCarValue.id : null,
        state: stateValue !== undefined ? stateValue.value : null,
        priority: priorityValue !== undefined ? priorityValue.value : null,
        probability: probabilityValue !== undefined ? probabilityValue.value : null,
        territory: territoryValue !== undefined ? territoryValue.value : null,
        idUser: this.data.myCrm.id,
        supportBosonit: supportBosonitValue !== undefined ? supportBosonitValue : this.data.myCrm.id,
        supportNfq: null,
        supportTech: null,
        warningDate: warningDateValue !== '' ? new Date(warningDateValue).toISOString() : null,
        closeDate: closeDateValue ? new Date(closeDateValue).toISOString() : null,
        name: opportunityNameValue,
        estimatedBudget: estimatedBudgetValue,
        accountId: accountIdValue ? accountIdValue : null,
        contactId: contactIdValue ? contactIdValue.id : null,
        projectClosed: this.form.get('projectClosed').value,
        dedicatedResource: this.form.get('dedicatedResource').value,
        resourceNumbers: this.form.get('resourceNumbers').value,
        recurrentPercentage: this.form.get('recurrentPercentage').value
      }
      this.dialogRef.close(newOpportunity);
    }
  }

}
