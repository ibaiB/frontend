import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IAccount} from 'src/app/crm/entities/account/domain/IAccount';
import {IActivity} from 'src/app/crm/entities/activity/domain/IActivity';
import {IActivitySliderListData} from 'src/app/crm/entities/activity/domain/IActivitySliderListData';
import {ActivitySliderListDataMapper} from 'src/app/crm/entities/activity/infrastructure/activity-slider-list-data-mapper';
import {IContact} from 'src/app/crm/entities/contact/domain/IContact';
import {INoteSliderListData} from 'src/app/crm/entities/note/domain/INoteSliderListData';
import {IOpportunitySliderData} from 'src/app/crm/entities/opportunity/domain/IOpportunitySliderData';

@Component({
  selector: 'app-opportunity-slider',
  templateUrl: './opportunity-slider.component.html',
  styleUrls: ['./opportunity-slider.component.scss']
})
export class OpportunitySliderComponent implements OnInit {

  activitySliderListData: IActivitySliderListData;
  noteSliderListData: INoteSliderListData;
  account: IAccount;
  contact: IContact;

  @Input() opportunitySliderData: IOpportunitySliderData;
  @Output() addedActivity = new EventEmitter<IActivity>();
  @Output() modActivity = new EventEmitter<any>();

  constructor(
    private _activitySliderListDataMapper: ActivitySliderListDataMapper) {
  }

  ngOnInit(): void {
    this.opportunitySliderData.account= this.opportunitySliderData.account ?? this.opportunitySliderData.contact.account;
    this.activitySliderListData = this._activitySliderListDataMapper.mapTo(this.opportunitySliderData, this.opportunitySliderData.opportunity,'Opportunity');
    this.noteSliderListData = { father: this.opportunitySliderData.opportunity, type: 'Opportunity' };
  }

  addActivity(event: any) {
    this.addedActivity.emit(event);
  }

  editActivity(event: any) {
    this.modActivity.emit(event);
  }

}
