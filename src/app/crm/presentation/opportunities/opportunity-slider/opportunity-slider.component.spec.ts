import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OpportunitySliderComponent} from './opportunity-slider.component';

describe('OpportunitySliderComponent', () => {
  let component: OpportunitySliderComponent;
  let fixture: ComponentFixture<OpportunitySliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunitySliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunitySliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
