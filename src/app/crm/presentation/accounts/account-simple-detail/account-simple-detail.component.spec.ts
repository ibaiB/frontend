import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AccountSimpleDetailComponent} from './account-simple-detail.component';

describe('AccountSimpleDetailComponent', () => {
  let component: AccountSimpleDetailComponent;
  let fixture: ComponentFixture<AccountSimpleDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountSimpleDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountSimpleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
