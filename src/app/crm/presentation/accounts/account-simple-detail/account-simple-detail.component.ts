import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';


@Component({
    selector: 'app-account-simple-detail',
    templateUrl: './account-simple-detail.component.html',
    styleUrls: ['./account-simple-detail.component.scss']
})
export class AccountSimpleDetailComponent implements OnInit {

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<AccountSimpleDetailComponent>,
        private _router: Router,
    ){}

    ngOnInit() {
        this.showValues();
    }

    closeDialog() {
        this.dialogRef.close();
    }

    goToDetails(cif: string) {
        this.dialogRef.close();
        this._router.navigate([`./app/crm/accounts/account-detail/${cif}`]);
    }

    showValues(){
        const carValue = this.data.account.car && this.data.account.car.length===2  ? this.data.carDictionary.content.filter(car=>car.id === this.data.account.car)[0].name : this.data.account.car;
        const subCarValue = this.data.account.subCar && this.data.account.subCar.length===2? this.data.subCarDictionary.content.filter(subcar=>subcar.id === this.data.account.subCar)[0].name: this.data.account.subCar;
        this.data.account.carValue = carValue;
        this.data.account.subCarValue = subCarValue;
    }
}
