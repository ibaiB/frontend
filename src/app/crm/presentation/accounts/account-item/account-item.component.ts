import {Component, Input, OnInit} from '@angular/core';
import {IAccount} from '../../../entities/account/domain/IAccount';
import {MatDialog} from '@angular/material/dialog';
import {AccountSimpleDetailComponent} from '../account-simple-detail/account-simple-detail.component';
import {IDictionaryType} from 'src/app/shared/entities/dictionary/domain/IDictionaryType';
import {IList} from 'src/app/shared/generics/IList';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {ISubcar} from '../../../../internal/subcar/domain/ISubcar';

@Component({
  selector: 'app-account-item',
  templateUrl: './account-item.component.html',
  styleUrls: ['./account-item.component.scss']
})
export class AccountItemComponent implements OnInit {

  photo: string;

  @Input() account: IAccount;
  @Input() carDictionary: IList<ICar>;
  @Input() subCarDictionary: IList<ISubcar>;
  @Input() categoryDictionary: IDictionaryType;

  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.photo = this.account.idLinkImage;
    if(this.account) this.showValues();
  }

  openSimpleDetailDialog() {
    const dialogRef = this.dialog.open(AccountSimpleDetailComponent, {
      data: {
        account: this.account,
        carDictionary: this.carDictionary,
        subCarDictionary: this.subCarDictionary,
        categoryDictionary: this.categoryDictionary
      }
    });
  }

  showValues(){
    const carValue = this.account.car && this.account.car.length===2  ? this.carDictionary.content.filter(car=>car.id === this.account.car)[0].name : this.account.car;
    const subCarValue = this.account.subCar && this.account.subCar.length===2? this.subCarDictionary.content.filter(subcar=>subcar.id === this.account.subCar)[0].name: this.account.subCar;
    this.account.car = carValue;
    this.account.subCar = subCarValue;
}
}
