import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {captionAlert} from 'src/app/shared/error/custom-alerts';
import {global} from '../../../config/global';


@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {

  form: FormGroup;

  allMineUserOptions: string[] = [];
  carOptions: string[] = [];
  subCarOptions: string[] = [];
  territoryOptions: string[] = [];
  categoryOptions: string[] = [];
  priorityOptions: string[] = [];
  sectorOptions: string[] = [];
  legends = ['TOP 100', 'Estratégicas', 'PYMEs', 'Partners', 'Otros'];

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CreateAccountComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.allMineUserOptions = this.data.users.map(user => user.email);
    this.carOptions = this.data.cars.content.map(car => car.name);
    this.subCarOptions = this.data.subCars.content.map(subCar => subCar.name);
    this.territoryOptions = this.data.territories.map(subCar => subCar.name);
    this.categoryOptions = this.data.categories.map((category, i) => `${category.name} | ${this.legends[i]}`);
    this.priorityOptions = this.data.priorities.map(priority => priority.name);
    this.sectorOptions = this.data.sectors.map(sector => sector.name);

    this.formInit();
    console.log(this.form.controls.category.value);
  }

  formInit() {
    this.form = this.fb.group({
      cif: ['', Validators.required],
      socialReason: [''],
      commercialName: ['', Validators.required],
      user: [`${this.data.me.email}`],
      category: [`${this.categoryOptions[2]}`, Validators.required],
      priority: ['Normal'],
      car: [''],
      subCar: [''],
      sector: [''],
      territory: [''],
      origin: ['']
    });

    this.form.get('car').valueChanges.subscribe(carValue => this.carValueChanged(carValue));
  }

  autocompleteSelected(event: { formField: string, value: string }) {
    this.form.controls[event.formField].setValue(event.value);
  }

  carValueChanged(carValue: string) {
    let car = this.data.cars.content.find(car => car.name === carValue)
    if (car) this.subCarOptions = car.subCars.map(subCar => subCar.name)
    else this.subCarOptions = this.data.subCars.content.map(subCar => subCar.name);
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onCancelClick() {
    this.dialogRef.close();
  }

  formatCategory(value: string) {
    const splatted: string[] = value?.split('|');
    return splatted?.length > 0 ? splatted[0].trim() : value;
  }

  onSaveClick() {
    if (this.form.valid) {
      const cif = this.form.get('cif').value;
      const socialReason = this.form.get('socialReason').value;
      const commercialName = this.form.get('commercialName').value;
      const categoryValue = this.form.get('category').value.split(' ')[0];
      const priorityValue = this.form.get('priority').value;
      const carValue = this.form.get('car').value;
      const subCarValue = this.form.get('subCar').value;
      const sectorValue = this.form.get('sector').value;
      const territoryValue = this.form.get('territory').value;
      const originValue = this.form.get('origin').value;

      const categoryFiltered = this.data.categories.find(category => category.name === this.formatCategory(categoryValue));
      const priorityFiltered = this.data.priorities.find(priority => priority.name === priorityValue);
      const carFiltered = this.data.cars.content.find(car => car.name === carValue);
      const subCarFiltered = this.data.subCars.content.find(subCar => subCar.name === subCarValue);
      const sectorFiltered = this.data.sectors.find(sector => sector.name === sectorValue);
      const territoryFiltered = this.data.territories.find(territory => territory.name === territoryValue);

      const result = {
        cif: cif,
        socialReason: socialReason,
        commercialName: commercialName,
        category: categoryFiltered !== undefined ? categoryFiltered.value : null,
        priority: priorityFiltered !== undefined ? priorityFiltered.value : null,
        car: carFiltered !== undefined ? carFiltered.id : null,
        subCar: subCarFiltered !== undefined ? subCarFiltered.id : null,
        sector: sectorFiltered !== undefined ? sectorFiltered.value : null,
        territory: territoryFiltered !== undefined ? territoryFiltered.value : null,
        origin: originValue
      }

      this.dialogRef.close(result);
    }
  }

  showCaptions(){
    captionAlert("account categories", global.categories);
}

}
