import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AccountSliderComponent} from './account-slider.component';

describe('AccountSliderComponent', () => {
  let component: AccountSliderComponent;
  let fixture: ComponentFixture<AccountSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
