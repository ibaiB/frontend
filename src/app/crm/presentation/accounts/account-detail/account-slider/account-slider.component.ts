import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatDrawer} from '@angular/material/sidenav';
import {IAccountSliderData} from '../../../../entities/account/domain/IAccountSliderData';
import {IContactSliderListData} from '../../../../entities/contact/domain/IContactSliderListData';
import {IActivitySliderListData} from '../../../../entities/activity/domain/IActivitySliderListData';
import {IOpportunitySliderListData} from '../../../../entities/opportunity/domain/IOpportunitySliderListData';
import {ContactSliderListDataMapper} from '../../../../entities/contact/infrastructure/contact-slider-list-data-mapper';
import {ActivitySliderListDataMapper} from '../../../../entities/activity/infrastructure/activity-slider-list-data-mapper';
import {OpportunitySliderListDataMapper} from '../../../../entities/opportunity/infrastructure/opportunity-slider-list-data-mapper';
import {INoteSliderListData} from '../../../../entities/note/domain/INoteSliderListData';


@Component({
    selector: 'app-account-slider',
    templateUrl: './account-slider.component.html',
    styleUrls: ['./account-slider.component.scss']
})
export class AccountSliderComponent implements OnInit {

    contactSliderListData: IContactSliderListData;
    activitySliderListData: IActivitySliderListData;
    opportunitySliderListData: IOpportunitySliderListData;
    noteSliderListData: INoteSliderListData;

    @Input() accountSliderData: IAccountSliderData;

    @Output() addedContact = new EventEmitter<any>();
    @Output() addedActivity = new EventEmitter<any>();
    @Output() addedOpportunity = new EventEmitter<any>();
    @Output() modActivity = new EventEmitter<any>();
    @Output() modOpportunity = new EventEmitter<any>();
    @Output() sliderToggle = new EventEmitter<string>();

    @ViewChild(MatDrawer, { static: true }) drawer: MatDrawer;

    constructor(
        private _contactSliderListDataMapper: ContactSliderListDataMapper,
        private _activitySliderListDataMapper: ActivitySliderListDataMapper,
        private _opportunitySliderListDataMapper: OpportunitySliderListDataMapper) {
    }

    ngOnInit() {
        this.opportunitySliderListData = this._opportunitySliderListDataMapper.mapTo(this.accountSliderData, this.accountSliderData.account);
        this.activitySliderListData = this._activitySliderListDataMapper.mapTo(this.accountSliderData, this.accountSliderData.account,'Account');
        this.contactSliderListData = this._contactSliderListDataMapper.mapTo(this.accountSliderData);
        this.noteSliderListData = { father: this.accountSliderData.account, type: 'Account' };
    }

    addContact(event: any) {
        //Propagate up information
        this.addedContact.emit(event);
    }

    addActivity(event: any) {
        //Propagate up information
        this.addedActivity.emit(event);
    }

    addOpportunity(event: any) {
        //Propagate up information
        this.addedOpportunity.emit(event);
    }

    editActivity(event: any) {
        //Propagate up information
        this.modActivity.emit(event);
    }

    editOpportunity(event: any) {
        //Propagate up information
        this.modOpportunity.emit(event);
    }

    sideNavOpen() {
        this.drawer.toggle();
        this.sliderToggle.emit('open');
    }

    sideNavClose() {
        this.drawer.toggle();
        this.sliderToggle.emit('close');
    }

}
