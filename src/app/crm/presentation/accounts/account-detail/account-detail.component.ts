import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {updateOpportunity} from 'src/app/crm/entities/opportunity/application/updateOpportunity';
import {addAccountActivity} from '../../../entities/account/application/addActivity';
import {addAccountOpportunity} from '../../../entities/account/application/addOpportunity';
import {editAccount} from '../../../entities/account/application/editAccount';
import {IAccount} from '../../../entities/account/domain/IAccount';
import {IAccountDetailResolved} from '../../../entities/account/domain/IAccountDetailResolved';
import {IAccountEditData} from '../../../entities/account/domain/IAccountEditData';
import {IAccountSliderData} from '../../../entities/account/domain/IAccountSliderData';
import {AccountEditDataMapper} from '../../../entities/account/infrastructure/account-edit-data-mapper';
import {AccountSliderDataMapper} from '../../../entities/account/infrastructure/account-slider-data-mapper';
import {WebAccountMapper} from '../../../entities/account/infrastructure/web-account-mapper';
import {updateActivity} from '../../../entities/activity/application/updateActivity';
import {IActivity} from '../../../entities/activity/domain/IActivity';
import {ActivityMapper} from '../../../entities/activity/infrastructure/activity-mapper';
import {createContact} from '../../../entities/contact/application/createContact';
import {ContactOutMapper} from '../../../entities/contact/infrastructure/contact-out-mapper';
import {OpportunityOutMapper} from '../../../entities/opportunity/infrastructure/opportunity-out-mapper';
import {AccountService} from '../../../services/account.service';
import {ActivityService} from '../../../services/activity.service';
import {ContactService} from '../../../services/contact.service';
import {IOpportunity} from './../../../entities/opportunity/domain/IOpportunity';
import {OpportunityService} from './../../../services/opportunity.service';
import {joinActivity} from '../../../entities/activity/application/joinActivity';
import {joinOpportunity} from '../../../entities/opportunity/application/joinOpportunity';
import {successAlert} from '../../../../shared/error/custom-alerts';

@Component({
    selector: 'app-account-detail',
    templateUrl: './account-detail.component.html',
    styleUrls: ['./account-detail.component.scss']
})
export class AccountDetailComponent implements OnInit {

    private _accountDetailResolved: IAccountDetailResolved;
    accountSliderData: IAccountSliderData;
    accountEditData: IAccountEditData;
    account: IAccount;

    loading: boolean = true;

    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        private _accountEditDataMapper: AccountEditDataMapper,
        private _accountSliderDataMapper: AccountSliderDataMapper,
        private _accountService: AccountService,
        private _contactService: ContactService,
        private _activityMapper: ActivityMapper,
        private _contactOutMapper: ContactOutMapper,
        private _webAccountMapper: WebAccountMapper,
        private _opportunityService: OpportunityService,
        private _opportunityOutMapper: OpportunityOutMapper,
        private _activityService: ActivityService
    ) {
    }

    ngOnInit() {
        this._accountDetailResolved = this._route.snapshot.data['response'];

        if (this._accountDetailResolved.detailData) {
            this.accountSliderData = this._accountSliderDataMapper.mapTo(this._accountDetailResolved.detailData);
            this.accountEditData = this._accountEditDataMapper.mapTo(this._accountDetailResolved.detailData);
            this.account = this._accountDetailResolved.detailData.account;
            this.loading = false;
        }
    }

    editAccount(event: any) {
        editAccount(this._webAccountMapper.mapFrom(event), this._accountService)
            .subscribe(edAccount => {
                this.account = edAccount;
                successAlert('The account was edited succesfully!');
            });
    }

    addContact(event: any) {
        createContact(this._contactOutMapper.mapFrom(event), this._contactService)
            .subscribe(newContact => {
                this.account.contacts.push(newContact);
                successAlert('The contact was added successfully!');
            });
    }

    addActivity(event: any) {
        addAccountActivity(this._activityMapper.mapFrom(event), this._accountDetailResolved.detailData.account.id, this._accountService)
            .subscribe(edAccount => {
                this.account.activities = edAccount.activities.map(activity => joinActivity(activity, this._dictionariesToArray(), this._accountDetailResolved.detailData.crmUsers, this._accountDetailResolved.detailData.cars, this._accountDetailResolved.detailData.subCars));
                successAlert('The activity was added successfully!');
            });
    }

    addOpportunity(event: any) {
        addAccountOpportunity(this._opportunityOutMapper.mapFrom(event), this._accountDetailResolved.detailData.account.id, this._accountService)
            .subscribe(edAccount => {
                this.account.opportunities = edAccount.opportunities.map(opportunity => joinOpportunity(opportunity, this._dictionariesToArray(), this._accountDetailResolved.detailData.crmUsers, this._accountDetailResolved.detailData.cars, this._accountDetailResolved.detailData.subCars));
                successAlert('The opportunity was added successfully!');
            });
    }

    modifyOpportunity(opportunity: IOpportunity) {
        if (opportunity !== undefined) {
            updateOpportunity(this._opportunityOutMapper.mapFrom(opportunity), this._opportunityService)
                .subscribe(edOpportunity => {
                    for (let i = 0; i < this.account.opportunities.length; i++) {
                        if (this.account.opportunities[i].id === edOpportunity.id) {
                            this.account.opportunities.splice(i, 1);
                            this.account.opportunities.push(joinOpportunity(edOpportunity, this._dictionariesToArray(), this._accountDetailResolved.detailData.crmUsers, this._accountDetailResolved.detailData.cars, this._accountDetailResolved.detailData.subCars));
                        }
                    }
                    successAlert('The opportunity was edited correctly');
                });
        }
    }

    modifyActivity(activity: IActivity) {
        updateActivity(this._activityMapper.mapFrom(activity), this._activityService)
            .subscribe(edActivity => {
                for (let i = 0; i < this.account.activities.length; i++) {
                    if (this.account.activities[i].id === edActivity.id) {
                        this.account.activities.splice(i, 1);
                        this.account.activities.push(joinActivity(edActivity, this._dictionariesToArray(), this._accountDetailResolved.detailData.crmUsers, this._accountDetailResolved.detailData.cars, this._accountDetailResolved.detailData.subCars));
                    }
                }
                successAlert('The activity was edited correctly');
            });
    }

    //TODO: this needs an urgent refactor
    private _dictionariesToArray() {
        let dictionaries = [];
        dictionaries.push({id: 'CAR', values: this._accountDetailResolved.detailData.cars});
        dictionaries.push({id: 'SUBCAR', values: this._accountDetailResolved.detailData.subCars});
        dictionaries.push({id: 'TERRITORIO', values: this._accountDetailResolved.detailData.territories});
        dictionaries.push({id: 'CATEGORIA', values: this._accountDetailResolved.detailData.categories});
        dictionaries.push({id: 'PRIORIDAD', values: this._accountDetailResolved.detailData.priorities});
        dictionaries.push({id: 'SECTOR', values: this._accountDetailResolved.detailData.sectors});
        dictionaries.push({id: 'NIVEL_RELACION', values: this._accountDetailResolved.detailData.relationshipLevels});
        dictionaries.push({id: 'ESTADOACTIVIDAD', values: this._accountDetailResolved.detailData.activityStates});
        dictionaries.push({id: 'TIPOACTIVIDAD', values: this._accountDetailResolved.detailData.activityTypes});
        dictionaries.push({id: 'PROBABILIDAD', values: this._accountDetailResolved.detailData.opportunityProbabilities});
        dictionaries.push({id: 'ESTADO', values: this._accountDetailResolved.detailData.opportunityStates});
        dictionaries.push({id: 'MOTIVORECHAZO', values: this._accountDetailResolved.detailData.opportunityRejectionReasons});
        return dictionaries;
    }
}
