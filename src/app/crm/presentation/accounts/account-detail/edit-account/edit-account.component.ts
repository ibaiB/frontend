import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {captionAlert} from 'src/app/shared/error/custom-alerts';

import {IAccount} from '../../../../entities/account/domain/IAccount';
import {IAccountEditData} from '../../../../entities/account/domain/IAccountEditData';
import {global} from '../../../../config/global';
import {MatDialog} from '@angular/material/dialog';
import {FileUploadQuery, imageQuery} from '../../../../../shared/file/domain/FileUploadQuery';
import {FileService} from '../../../../../shared/file/infrastructure/services/file-service.service';
import {uploadFile} from '../../../../../shared/file/application/upload';

@Component({
    selector: 'app-edit-account',
    templateUrl: './edit-account.component.html',
    styleUrls: ['./edit-account.component.scss']
})
export class EditAccountComponent implements OnInit {

    @Input() accountEditData: IAccountEditData;
    @Output() modAccount = new EventEmitter<any>();

    account: IAccount;
    width = '100%';

    allMineUserOptions: string[] = [];
    carOptions: string[] = [];
    subCarOptions: string[] = [];
    territoryOptions: string[] = [];
    categoryOptions: string[] = [];
    priorityOptions: string[] = [];
    sectorOptions: string[] = [];
    regionOptions: string[] = [];
    countryOptions: string[] = [];

    userValue: string;
    carValue: string;
    subCarValue: string;
    categoryValue: string;
    priorityValue: string;
    territoryValue: string;
    sectorValue: string;

    form: FormGroup;

    categories = global.categories;
    categoriesShort = global.categoriesShort;

    photo: string;
    newPhoto: File;
    newPhotoId: string;
    fileUploadQuery: FileUploadQuery = imageQuery;

    edit: boolean = false;

    constructor(private fb: FormBuilder, 
        private _route: ActivatedRoute,
        public dialog: MatDialog,
        private _fileService: FileService) {
    }

    ngOnInit() {
        this.carOptions = this.accountEditData.cars.content.map(car => car.name);
        this.subCarOptions = this.accountEditData.subCars.content.map(subcar => subcar.name);
        this.territoryOptions = this.accountEditData.territories.map(territory => territory.name);
        this.categoryOptions = this.accountEditData.categories.map(categories => this.getCatWithLegend(categories.name));
        this.priorityOptions = this.accountEditData.priorities.map(priority => priority.name);
        this.sectorOptions = this.accountEditData.sectors.map(sector => sector.name);
        this.account = this.accountEditData.account;
        this.regionOptions = global.communities.map(comunidad => comunidad.value);
        this.countryOptions = global.countries.map(pais => pais.value);
        this.accountEditData.crmUsers.forEach(user => {
            this.allMineUserOptions.push(user.email);
        });

        this.photo = this.accountEditData.account.idLinkImage;

        this.formInit();
    }

    formInit() {

        this.carValue = this.account.car;
        this.subCarValue = this.account.subCar;
        this.userValue = this.account.user?.email;
        this.categoryValue = this.getCatWithLegend(this.account.category);
        this.priorityValue = this.account.priority;
        this.territoryValue = this.account.territory;
        this.sectorValue = this.account.sector;

        this.form = this.fb.group({
            CNAEActivity: [this.account.CNAEActivity],
            SICActivity: [this.account.SICActivity],
            car: [this.carValue],
            cif: [this.account.cif, Validators.required],
            city: [this.account.city],
            postCode: [this.account.postCode],
            province: [this.account.province],
            email: [this.account.email],
            description: [this.account.description],
            address: [this.account.address],
            domain: [this.account.domain],
            ebitda: [this.account.ebitda],
            balance: [this.account.balance],
            leadState: [this.account.leadState],
            lifeCycleState: [this.account.lifeCycleState],
            billing: [this.account.billing],
            idCrm360: [this.account.idCrm360],
            industry: [this.account.industry],
            employeeNum: [this.account.employeeNum, Validators.pattern('([0-9])*')],
            commercialName: [this.account.commercialName, Validators.required],
            commercialName2: [this.account.commercialName2],
            origin: [this.account.origin],
            country: [this.account.country],
            user: [this.userValue],
            region: [this.account.region],
            socialReason: [this.account.socialReason],
            sector: [this.sectorValue],
            subCar: [this.subCarValue],
            phone: [this.account.phone],
            category: [this.categoryValue, Validators.required],
            web: [this.account.web],
            existsClient: [this.account.existsClient],
            territory: [this.territoryValue],
            priority: [this.priorityValue]
        });

        this.form.get('car').valueChanges.subscribe(carValue => this.carValueChanged(carValue));
    }

    formatCategory(value: string) {
        const splatted: string[] = value?.split('|');
        return splatted?.length > 0 ? splatted[0].trim() : value;
    }

    applyChanges() {
        if (this.newPhoto) this._updatePhotoAndAccount();
        else this._updateAccount();
    }

    autocompleteSelected(event: {formField: string, value: string}){
        this.form.controls[event.formField].setValue(event.value);
    }

    carValueChanged(carValue: string) {
        let car = this.accountEditData.cars.content.find(car => car.name === carValue)
        if (car) this.subCarOptions = car.subCars.map(subCar => subCar.name)
        else this.subCarOptions = this.accountEditData.subCars.content.map(subCar => subCar.name);
    }

    photoChanged($event) {
        this.newPhoto = $event;
        this.fileUploadQuery.uploadName = $event.name;
    }

    openEmail(email: string){
        window.location.href = `mailto:${email}?subject=Subject&body=message%20goes%20here`;
    }

    getCatWithLegend(cat: string): string{
        let result = this.categoriesShort.find(category => category.value === cat);
        return `${result.value} | ${result.description}`;
    }

    showCaptions(){
        captionAlert("account categories", this.categories);
    }

    _updatePhotoAndAccount() {
        uploadFile(
            this.newPhoto,
            this.fileUploadQuery,
            this._fileService,
        ).subscribe(
            (retFile) => {
                this.newPhotoId = retFile.id;
                this._updateAccount();
            }
        );
    }

    _updateAccount() {
        if (this.form.valid) {
            const cif = this.form.get('cif').value;
            const socialReason = this.form.get('socialReason').value;
            const commercialName = this.form.get('commercialName').value;
            this.userValue = this.form.get('user').value;
            this.categoryValue = this.formatCategory(this.form.get('category').value);
            this.priorityValue = this.form.get('priority').value;
            this.carValue = this.form.get('car').value;
            this.subCarValue = this.form.get('subCar').value;
            this.sectorValue = this.form.get('sector').value;
            this.territoryValue = this.form.get('territory').value;
            const originValue = this.form.get('origin').value;

            const userFiltered = this.accountEditData.crmUsers.find(user => user.email === this.userValue);
            const categoryFiltered = this.accountEditData.categories.find(category => category.name === this.categoryValue);
            const priorityFiltered = this.accountEditData.priorities.find(priority => priority.name === this.priorityValue);
            const carFiltered = this.accountEditData.cars.content.find(car => car.name === this.carValue);
            const subCarFiltered = this.accountEditData.subCars.content.find(subCar => subCar.name === this.subCarValue);
            const sectorFiltered = this.accountEditData.sectors.find(sector => sector.name === this.sectorValue);
            const territoryFiltered = this.accountEditData.territories.find(territory => territory.name === this.territoryValue);

            this.modAccount.emit({
                id: this.account.id,
                cif: this.form.get('cif').value,
                socialReason: socialReason,
                commercialName: commercialName,
                userId: userFiltered !== undefined ? userFiltered.id : this.account.userId,
                category: categoryFiltered !== undefined ? categoryFiltered.value : this.account.category,
                priority: priorityFiltered !== undefined ? priorityFiltered.value : this.account.priority,
                car: carFiltered !== undefined ? carFiltered.id : this.account.car,
                subCar: subCarFiltered !== undefined ? subCarFiltered.id : this.account.subCar,
                sector: sectorFiltered !== undefined ? sectorFiltered.value : this.account.sector,
                territory: territoryFiltered !== undefined ? territoryFiltered.value : this.account.territory,
                origin: originValue,
                CNAEActivity: this.form.get('CNAEActivity').value,
                SICActivity: this.form.get('SICActivity').value,
                city: this.form.get('city').value,
                postCode: this.form.get('postCode').value,
                province: this.form.get('province').value,
                email: this.form.get('email').value,
                description: this.form.get('description').value,
                address: this.form.get('address').value,
                domain: this.form.get('domain').value,
                ebitda: this.form.get('ebitda').value,
                balance: this.form.get('balance').value,
                leadState: this.form.get('leadState').value,
                lifeCycleState: this.form.get('lifeCycleState').value,
                billing: this.form.get('billing').value,
                idCrm360: this.form.get('idCrm360').value,
                industry: this.form.get('industry').value,
                employeeNum: this.form.get('employeeNum').value,
                commercialName2: this.form.get('commercialName2').value,
                country: this.form.get('country').value,
                region: this.form.get('region').value,
                phone: this.form.get('phone').value,
                web: this.form.get('web').value,
                existsClient: this.form.get('existsClient').value,
                idLinkImage: this.newPhotoId ?? null
            });
        }
    }
}
