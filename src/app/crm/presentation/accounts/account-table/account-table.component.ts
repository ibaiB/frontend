import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {parseValues} from '../../../../shared/entities/dictionary/application/parseValues';
import {createAccount} from '../../../entities/account/application/createAccount';
import {IAccountTableResolved} from '../../../entities/account/domain/IAccountTableResolved';
import {AccountOutMapper} from '../../../entities/account/infrastructure/account-out-mapper';
import {AccountTableMapper} from '../../../entities/account/infrastructure/account-table-mapper';
import {ICRMUser} from '../../../entities/user/domain/ICRMUser';
import {AccountService} from '../../../services/account.service';
import {CreateAccountComponent} from '../create-account/create-account.component';
import {IDynamicColumn} from '../../../../shared/custom-mat-elements/dynamic-table/IDynamicColumn';
import {joinSimpleAccount} from '../../../entities/account/application/joinSimpleAccount';
import {PageEvent} from '@angular/material/paginator';
import {IAccount} from '../../../entities/account/domain/IAccount';
import {successAlert} from 'src/app/shared/error/custom-alerts';
import {getUsers} from 'src/app/crm/entities/user/application/getUsers';
import {UserService} from 'src/app/crm/services/user.service';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {table} from './config';
import {AccountMapper} from 'src/app/crm/entities/account/infrastructure/account-mapper';
import {DynamicTableMarkIiComponent} from 'src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {IList} from 'src/app/shared/generics/IList';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';

@Component({
    selector: 'app-account-table',
    templateUrl: './account-table.component.html',
    styleUrls: ['./account-table.component.scss']
})
export class AccountTableComponent implements OnInit {

    private _resolved: IAccountTableResolved;

    tableConfig: ITableConfig = table;
    filters: IAuto[] = [];
    tableData: IList<any>;

    loading: boolean = true;
    myCrm: ICRMUser;
    size: number = 10;
    columns: IDynamicColumn[] = [];
    queryPagination = {page: 0, size: 10};
    queryOrder = {orderField: '', order: ''};
    queryFilters = {};
    subCarFilter = null;
    configVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        public _accountService: AccountService,
        private _accountOutMapper: AccountOutMapper,
        public _accountMapper: AccountMapper,
        private _accountTableMapper: AccountTableMapper,
        private _fb: FormBuilder,
        private _route: ActivatedRoute,
        private _router: Router,
        public dialog: MatDialog,
        private _userService: UserService,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit() {
        this._resolved = this._route.snapshot.data['response'];


        if (this._resolved.tableData) {
            this._buildFilters();
            this._buildColumns();
            parseTableFiltersConfig(this._resolved.tableData.tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = this._resolved.tableData.filtersValues ? this._clean((JSON.parse(this._resolved.tableData.filtersValues))) : {};
            this.tableConfig.pagination = this._resolved.tableData.tableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = this._resolved.tableData.tableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries(this._accountMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';

            this.tableData = this._resolved.tableData.accounts;
            this.tableData.content = this.tableData.content.map(AccountTableComponent._extend);
            this.loading = false;
        }
        this.subCarFilter = this.filters.find(filter => filter.prop === 'subCar');
        this._updateSubCarOptions();
        this.configureFiltersValue();
    }

    configureFiltersValue() {
        let queryFilterAux = this._resolved.tableData.filtersValues ? this._clean(this._accountMapper.mapTo(JSON.parse(this._resolved.tableData.filtersValues))) : {};

        queryFilterAux['car'] = queryFilterAux['car'] ? this._resolved.tableData.cars.content.find(car => car.id === queryFilterAux['car']).name : '';
        queryFilterAux['subCar'] = queryFilterAux['subCar'] ? this._resolved.tableData.subCars.content.find(subcar => subcar.id === queryFilterAux['subCar']).name : '';
        queryFilterAux['territory'] = queryFilterAux['territory'] ? this._resolved.tableData.territories.find(territory => territory.value === queryFilterAux['territory']).name : '';
        queryFilterAux['userId'] = queryFilterAux['userId'] ? this.tableData.content.find(account => account.userId === queryFilterAux['userId'])?.userEmail : '';
        this.filters.forEach(filter => {
            filter.defaultValue = queryFilterAux[this.getFilterProp(filter)];
            filter.value = queryFilterAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    private static _extend(a: any): any {
        return {
            ...a,
            userEmail: a.user ? a.user.email : a.idUser
        };
    }

    changeOptions(event: { formField: string, value: string }) {
        getUsers({email: event.value}, this._userService).subscribe(users =>
            this.filters.find(filt => filt.label === 'Assigned user').options = users);
    }

    backSearch() {
        this._accountService.search({...this.queryPagination, ...this.queryOrder, ...this.queryFilters})
            .subscribe(result => {
                this.tableData = {
                    content: result.content.map(account => joinSimpleAccount(account, this._dictionariesToArray(), this._resolved.tableData.users, this._resolved.tableData.cars, this._resolved.tableData.subCars)),
                    totalElements: result.totalElements,
                    numberOfElements: result.numberOfElements,
                    totalPages: result.totalPages
                };
                this.tableData.content = this.tableData.content.map(AccountTableComponent._extend);
            });

        this._updateSubCarOptions();
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('account-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    private _updateSubCarOptions() {
        const subCarFilter = this.filters.find(filter => filter.searchValue === 'subCar');

        if (this.queryFilters.hasOwnProperty('idCar')) {
            const carId = this.queryFilters['idCar'];
            const car = this._resolved.tableData.cars.content.find(car => car.id === carId);
            if (car) {
                subCarFilter.options = car.subCars.map(subCar => subCar.name);
            }
        } else {
            subCarFilter.options = this._resolved.tableData.subCars.content.map(subCar => subCar.name);
        }
    }

    private _querySwitcher(query: any, _resolved) {
        const user = _resolved.tableData.users.find(user => user.email === query.userId);
        if (user) {
            query.userId = user.id;
        }

        const car = _resolved.tableData.cars.content.find(car => car.name === query.car);
        if (car) {
            query.car = car.id;
        }

        const subCar = _resolved.tableData.subCars.content.find(subCar => subCar.name === query.subCar);
        if (subCar) {
            query.subCar = subCar.id;
        }

        const territory = _resolved.tableData.territories.find(territory => territory.name === query.territory);
        if (territory) {
            query.territory = territory.value;
        }

        const category = _resolved.tableData.categories.find(category => category.name === this.formatCategory(query.category));
        if (category) {
            query.category = category.value;
        }

        const sector = _resolved.tableData.sectors.find(sector => sector.name === query.sector);
        if (sector) {
            query.sector = sector.value;
        }

        return query;
    }

    formatCategory(value: string) {
        const splatted: string[] = value?.split('|');
        return splatted?.length > 0 ? splatted[0].trim() : value;
    }

    openNewAccountDialog() {
        const dialogRef = this.dialog.open(CreateAccountComponent, {
            data: {
                me: this._resolved.tableData.myCrm,
                users: this._resolved.tableData.users,
                categories: this._resolved.tableData.categories,
                priorities: this._resolved.tableData.priorities,
                cars: this._resolved.tableData.cars,
                subCars: this._resolved.tableData.subCars,
                sectors: this._resolved.tableData.sectors,
                territories: this._resolved.tableData.territories
            }
        });

        dialogRef
            .afterClosed()
            .subscribe(account => {
                if (account !== undefined) {
                    createAccount(this._accountOutMapper.mapFrom(account), this._accountService)
                        .subscribe((newAccount) => {
                            this.backSearch();
                            successAlert('Account was created successfully');
                        });
                }
            });
    }

    doAction(event: { action: string, item: IAccount }) {
        console.log('action', event);
    }

    setSize(event: PageEvent) {
        this.backSearch();
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const switched = this._querySwitcher(params, this._resolved);
            const mapped = this._accountOutMapper.mapFrom(switched);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }

        this._cookieFacade.save('accountFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('account-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('account-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        if (event.orderField === 'userEmail') {
            event.orderField = 'userId';
        }
        let aux = this._accountOutMapper.mapFrom({[event.orderField]: ''});

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('account-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    private _dictionariesToArray() {
        let dictionaries = [];
        dictionaries.push({id: 'CAR', values: this._resolved.tableData.cars});
        dictionaries.push({id: 'SUBCAR', values: this._resolved.tableData.subCars});
        dictionaries.push({id: 'TERRITORIO', values: this._resolved.tableData.territories});
        dictionaries.push({id: 'CATEGORIA', values: this._resolved.tableData.categories});
        dictionaries.push({id: 'PRIORIDAD', values: this._resolved.tableData.priorities});
        dictionaries.push({id: 'SECTOR', values: this._resolved.tableData.sectors});

        return dictionaries;
    }

    private _buildFilters() {
        let catLegends = [];
        let legends = ['TOP 100', 'Estratégicas', 'PYMEs', 'Partners', 'Otros'];
        this._resolved.tableData.categories.forEach((cat, i) => catLegends.push(`${cat.name} | ${legends[i]}`));
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Id',
                placeholder: 'Id',
                shown: true
            },
            {
                options: [],
                prop: 'cif',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Cif',
                placeholder: 'CIF',
                shown: true
            },
            {
                options: [],
                prop: 'commercialName',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Commercial Name',
                placeholder: 'Commercial Name',
                shown: true
            },
            {
                options: [],
                prop: 'email',
                retProp: '',
                searchValue: 'userId',
                type: 'autoselect',
                appearance: 'standard',
                class: 'autoselect',
                label: 'Assigned user',
                placeholder: 'Assigned user',
                shown: true
            },
            {
                options: catLegends,
                prop: '',
                retProp: '',
                searchValue: 'category',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Category',
                placeholder: '',
                shown: false
            },
            {
                options: parseValues(this._resolved.tableData.priorities),
                prop: '',
                retProp: '',
                searchValue: 'priority',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Priority',
                placeholder: '',
                shown: false
            },
            {
                options: this._resolved.tableData.cars.content.map(car => car.name),
                prop: '',
                retProp: '',
                searchValue: 'car',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Car',
                placeholder: '',
                shown: false
            },
            {
                options: this._resolved.tableData.subCars.content.map(subcar => subcar.name),
                prop: 'subCar',
                retProp: '',
                searchValue: 'subCar',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Subcar',
                placeholder: '',
                shown: false,
            },
            {
                options: parseValues(this._resolved.tableData.sectors),
                prop: '',
                retProp: '',
                searchValue: 'sector',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Sector',
                placeholder: '',
                shown: false
            },
            {
                options: parseValues(this._resolved.tableData.territories),
                prop: '',
                retProp: '',
                searchValue: 'territory',
                type: 'autocomplete',
                appearance: 'standard',
                class: 'autocomplete',
                label: 'Territory',
                placeholder: '',
                shown: false
            }
        ];
    }

    private _buildColumns() {
        this.columns = [
            {name: 'Id', property: 'id', shown: true, type: 'id', class: ''},
            {name: 'Cif', property: 'cif', shown: true, type: 'string', class: ''},
            {name: 'Commercial Name', property: 'commercialName', shown: true, type: 'string', class: ''},
            {name: 'Assigned User', property: 'userId', shown: true, type: 'string', class: ''},
            {name: 'Category', property: 'category', shown: true, type: 'string', class: ''},
            {name: 'Priority', property: 'priority', shown: true, type: 'priority', class: ''},
            {name: 'Car', property: 'car', shown: true, type: 'string', class: ''},
            {name: 'Subcar', property: 'subCar', shown: true, type: 'string', class: ''},
            {name: 'Sector', property: 'sector', shown: true, type: 'string', class: ''},
            {name: 'Territory', property: 'territory', shown: true, type: 'string', class: ''},
            {name: 'CNAEActivity', property: 'CNAEActivity', shown: false, type: 'string', class: ''},
            {name: 'SICActivity', property: 'SICActivity', shown: false, type: 'string', class: ''},
            {name: 'City', property: 'city', shown: false, type: 'string', class: ''},
            {name: 'PostCode', property: 'postCode', shown: false, type: 'string', class: ''},
            {name: 'Region', property: 'region', shown: false, type: 'string', class: ''},
            {name: 'Email', property: 'email', shown: false, type: 'string', class: ''},
            {name: 'Description', property: 'description', shown: false, type: 'string', class: ''},
            {name: 'Address', property: 'address', shown: false, type: 'string', class: ''},
            {name: 'Domain', property: 'domain', shown: false, type: 'string', class: ''},
            {name: 'Ebitda', property: 'ebitda', shown: false, type: 'string', class: ''},
            {name: 'Balance', property: 'balance', shown: false, type: 'string', class: ''},
            {name: 'LeadState', property: 'leadState', shown: false, type: 'string', class: ''},
            {name: 'LifeCycleState', property: 'lifeCycleState', shown: false, type: 'string', class: ''},
            {name: 'Billing', property: 'billing', shown: false, type: 'string', class: ''},
            {name: 'Created', property: 'created', shown: false, type: 'date', class: ''},
            {name: 'Downloaded', property: 'downloaded', shown: false, type: 'string', class: ''},
            {name: 'Source', property: 'source', shown: false, type: 'string', class: ''},
            {name: 'IdCrm360', property: 'idCrm360', shown: false, type: 'string', class: ''},
            {name: 'Industry', property: 'industry', shown: false, type: 'string', class: ''},
            {name: 'EmployeeNum', property: 'employeeNum', shown: false, type: 'string', class: ''},
            {name: 'Commercial Name 2', property: 'commercialName2', shown: false, type: 'string', class: ''},
            {name: 'Sales', property: 'sales', shown: false, type: 'string', class: ''},
            {name: 'Origin', property: 'origin', shown: false, type: 'string', class: ''},
            {name: 'Country', property: 'country', shown: false, type: 'string', class: ''},
            {name: 'Province', property: 'province', shown: false, type: 'string', class: ''},
            {name: 'Social Reason', property: 'socialReason', shown: false, type: 'string', class: ''},
            {name: 'Phone', property: 'phone', shown: false, type: 'string', class: ''},
            {name: 'Web', property: 'web', shown: false, type: 'string', class: ''},
        ];
    }
}


