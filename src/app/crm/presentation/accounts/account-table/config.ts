import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
  multiSelect: false,
  columns: [
    {
      name: 'Id',
      prop: 'id',
      shown: true,
      route: './app/crm/accounts/account-detail/',
      cellClass: 'id-table-column',
      routeId: 'id'
    },
    {
      name: "Cif",
      prop: "cif",
      shown: true,
    },
    {
      name: "Commercial Name",
      prop: "commercialName",
      shown: true,
    },
    {
      name: "Assigned user",
      prop: "userEmail",
      shown: true,
    },
    {
      name: "Category",
      prop: "category",
      shown: true,
    },
    {
      name: "Priority",
      prop: "priority",
      shown: true,
    },
    {
      name: "Car",
      prop: "car",
      shown: true,
    },
    {
      name: "Subcar",
      prop: "subCar",
      shown: true,
    },
    {
      name: "Sector",
      prop: "sector",
      shown: false,
    },
    {
      name: "Territory",
      prop: "territory",
      shown: false,
    }
  ],
};
