import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
    multiSelect: false,
    columns: [
      {
        name: 'Id',
        prop: 'id',
        shown: true,
        route: './app/crm/activities/activity-detail/',
        cellClass: 'id-table-column',
        routeId: 'id'
      },
      {
        name: 'Assigned Contact',
        prop: 'contactName',
        shown: true,
        route: './app/crm/contacts/contact-detail/',
        cellClass: 'id-table-column',
        routeId: 'idContact'

      },
      {
        name: "Name",
        prop: "name",
        shown: true,
      },
      {
        name: "Car",
        prop: "car",
        shown: true,
      },
      {
        name: "SubCar",
        prop: "subCar",
        shown: true,
      },
      {
        name: "State",
        prop: "state",
        shown: true,
      },
      {
        name: "Type",
        prop: "type",
        shown: true,
      },
      {
        name: "Assigned User",
        prop: "userEmail",
        shown: false,
      },
      {
        name: "Priority",
        prop: "priority",
        shown: false,
      },
      {
        name: "Territory",
        prop: "territory",
        shown: false,
      },
      {
        name: 'Assigned Account',
        prop: 'accountName',
        shown: false,
        route: './app/crm/accounts/account-detail/',
        cellClass: 'id-table-column',
        routeId: 'idAccount'
      },
      {
        name: 'Assigned Opportunity',
        prop: 'opportunityName',
        shown: false,
        route: './app/crm/opportunities/opportunity-detail/',
        cellClass: 'id-table-column',
        routeId: 'idOpportunity'
      }
    ],
  };
