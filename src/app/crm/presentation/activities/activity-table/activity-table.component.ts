import {Component, OnInit, ViewChild} from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {getAccounts} from 'src/app/crm/entities/account/application/getAccounts';
import {getContacts} from 'src/app/crm/entities/contact/application/getContacts';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from '../../../../shared/custom-mat-elements/dynamic';
import {parseValues} from '../../../../shared/entities/dictionary/application/parseValues';
import {joinActivity} from '../../../entities/activity/application/joinActivity';
import {IActivity} from '../../../entities/activity/domain/IActivity';
import {IActivityTableResolved} from '../../../entities/activity/domain/IActivityTableResolved';
import {ActivityMapper} from '../../../entities/activity/infrastructure/activity-mapper';
import {AccountService} from '../../../services/account.service';
import {ActivityService} from '../../../services/activity.service';
import {ContactService} from '../../../services/contact.service';
import {table} from './config';
import {IList} from '../../../../shared/generics/IList';
import {DynamicTableMarkIiComponent} from 'src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {getSingleAccount} from 'src/app/crm/entities/account/application/getSingleAccount';

@Component({
    selector: 'app-activity-table',
    templateUrl: './activity-table.component.html',
    styleUrls: ['./activity-table.component.scss'],
})
export class ActivityTableComponent implements OnInit {
    private _resolved: IActivityTableResolved;

    tableConfig: ITableConfig = table;
    tableData: IList<any>;
    filters: IAuto[] = [];

    queryPagination = {page: 0, size: 10};
    queryOrder = {orderField: '', order: ''};
    queryFilters = {};

    totalElements: number;
    loading: boolean = true;
    configVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private _service: ActivityService,
        private _activityMapper: ActivityMapper,
        private _accountService: AccountService,
        private _contactService: ContactService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit() {
        this._resolved = this._route.snapshot.data['response'];

        if (this._resolved.tableData) {
            this._buildFilters();
            parseTableFiltersConfig(this._resolved.tableData.tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = this._resolved.tableData.filtersValue ? this._clean((JSON.parse(this._resolved.tableData.filtersValue))) : {};
            this.tableConfig.pagination = this._resolved.tableData.tableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = this._resolved.tableData.tableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries(this._activityMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
            this.tableData = this._resolved.tableData.activities;
            this.tableData.content = this.tableData.content.map(this._extend);
            //TODO: Delete forEach
            this.tableData.content.forEach(a => a.accountName = a.accountName ?? this.getAccount(a.idAccount, a));
            this.loading = false;
        }
        this.configFilterValue();
        this._updateSubCarOptions();
    }

    configFilterValue() {
        let queryFiltersAux = this._resolved.tableData.filtersValue ? this._clean(this._activityMapper.mapTo(JSON.parse(this._resolved.tableData.filtersValue))) : {};
        queryFiltersAux['car'] = queryFiltersAux['car'] ? this._resolved.tableData.cars.content.find(car => car.id === queryFiltersAux['car']).name : '';
        queryFiltersAux['subCar'] = queryFiltersAux['subCar'] ? this._resolved.tableData.subCars.content.find(subcar => subcar.id === queryFiltersAux['subCar']).name : '';
        queryFiltersAux['territory'] = queryFiltersAux['territory'] ? this._resolved.tableData.territories.find(territory => territory.value === queryFiltersAux['territory']).name : '';
        queryFiltersAux['idAccount'] = queryFiltersAux['idAccount'] ? this.tableData.content.filter(activity => activity.idAccount === queryFiltersAux['idAccount'])[0]?.accountName : '';
        queryFiltersAux['idAssignedContact'] = queryFiltersAux['idAssignedContact'] ?
            this.tableData.content.find(activity => activity.idContact === queryFiltersAux['idAssignedContact'])?.contact.contactName : '';
        this.filters.forEach(filter => {
            filter.defaultValue = queryFiltersAux[this.getFilterProp(filter)];
            filter.value = queryFiltersAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    backSearch() {

        this._service.search({...this.queryPagination, ...this.queryOrder, ...this.queryFilters})
            .subscribe(result => {
                this.tableData = {
                    content: result.content.map(activity => joinActivity(activity,
                        this._dictionariesToArray(),
                        this._resolved.tableData.users,
                        this._resolved.tableData.cars,
                        this._resolved.tableData.subCars
                    )),
                    totalElements: result.totalElements,
                    numberOfElements: result.numberOfElements,
                    totalPages: result.totalPages
                };
                this.tableData.content = this.tableData.content.map(this._extend);
                //TODO: delete forEach
                this.tableData.content.forEach(a => a.accountName = a.accountName ? a.accountName : this.getAccount(a.idAccount, a));
            });
        this._updateSubCarOptions();
    }

    private _updateSubCarOptions() {
        const subCarFilter = this.filters.find(filter => filter.searchValue === 'subCar');

        if (this.queryFilters.hasOwnProperty('idCar')) {
            const carId = this.queryFilters['idCar'];
            const car = this._resolved.tableData.cars.content.find(car => car.id === carId);
            if (car) {
                subCarFilter.options = car.subCars.map(subCar => subCar.name);
            }
        } else {
            subCarFilter.options = this._resolved.tableData.subCars.content.map(subCar => subCar.name);
        }
    }

    //TODO: Delete and change _extend accountName and delete forEach ↑↑↑ --> a.opportunity.contact.account.commercialName
    getAccount(id: string, a: any) {
        getSingleAccount(id, this._accountService).subscribe(account => a.accountName = account.commercialName);
    }

    _extend(a: IActivity): any {
        a.idAccount = a.idAccount ? a.idAccount : (a.contact ? a.contact.accountId : (a.opportunity.idAccount ?? a.opportunity.contact.accountId));
        a.idContact = a.idContact ? a.idContact : (a.opportunity ? a.opportunity?.idContact : '');

        return {
            ...a,
            accountName: a.account ? a.account.commercialName : (a.contact ? a.contact.account.commercialName : (a.opportunity.account ? a.opportunity.account.commercialName : '')),
            contactName: a.contact ? `${a.contact.name} ${a.contact.surname ?? ''} ${a.contact.surname2 ?? ''}`
                : (a.opportunity?.contact ? `${a.opportunity.contact.name} ${a.opportunity.contact.surname ?? ''} ${a.opportunity.contact.surname2 ?? ''}` : a.idContact),
            opportunityName: a.opportunity ? a.opportunity.name : a.idOpportunity,
            userEmail: a.user ? a.user.email : a.idUser
        };
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('activity-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    private _querySwitcher(query: any, _resolved, filters) {
        const user = _resolved.tableData.users.filter(user => user.email === query.idUser)[0];
        if (user) {
            query.idUser = user.id;
        }

        const supportBosonit = _resolved.tableData.users.filter(user => user.email === query.supportBosonit)[0];
        if (supportBosonit) {
            query.idUser = supportBosonit.id;
        }

        const car = _resolved.tableData.cars.content.filter(car => car.name === query.car)[0];
        if (car) {
            query.car = car.id;
        }

        const subCar = _resolved.tableData.subCars.content.filter(subCar => subCar.name === query.subCar)[0];
        if (subCar) {
            query.subCar = subCar.id;
        }

        const territory = _resolved.tableData.territories.filter(territory => territory.name === query.territory)[0];
        if (territory) {
            query.territory = territory.value;
        }

        const priority = _resolved.tableData.priorities.filter(priority => priority.name === query.priority)[0];
        if (priority) {
            query.priority = priority.value;
        }

        const state = _resolved.tableData.priorities.filter(state => state.name === query.state)[0];
        if (state) {
            query.state = state.value;
        }

        const type = _resolved.tableData.types.filter(type => type.name === query.type)[0];
        if (type) {
            query.type = type.value;
        }

        if (query.warningDate) {
            query.warningDate = new Date(query.warningDate).toISOString();
        }
        const idAccount = this.tableData.content.filter(activity => activity.accountName === query.idAccount)[0];
        if (idAccount) {
            query.idAccount = idAccount.idAccount;
        }

        const idContact = this.tableData.content.filter(activity => activity.contactName === query.idAssignedContact)[0];
        if (idContact) {
            query.idAssignedContact = idContact.idContact;
        }
        return query;
    }

    private _dictionariesToArray() {
        let dictionaries = [];
        dictionaries.push({id: 'TERRITORIO', values: this._resolved.tableData.territories});
        dictionaries.push({id: 'PRIORIDAD', values: this._resolved.tableData.priorities});
        dictionaries.push({id: 'TIPOACTIVIDAD', values: this._resolved.tableData.types});
        dictionaries.push({id: 'ESTADOACTIVIDAD', values: this._resolved.tableData.states});

        return dictionaries;
    }

    changeOptions(event: { searchValue: string; value: string }) {
        if (event.searchValue === 'idAccount') {
            getAccounts({nombreComercial: event.value}, this._accountService).subscribe((accounts) => {
                this.filters.find((filt) => filt.label === 'Assigned Account').options = accounts;
            });
        }
        if (event.searchValue === 'idAssignedContact') {
            getContacts({nombre: event.value}, this._contactService).subscribe((users) => {
                users.map((user) => (user['nombreApellido'] = user.name + ' ' + user.surname ?? ''));
                this.filters.find((filt) => filt.label === 'Assigned Contact').options = users;
            });
        }
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Id',
                placeholder: 'id',
                shown: true,
            },
            {
                prop: 'nombreApellido',
                retProp: '',
                type: 'autoselect',
                options: [],
                shown: true,
                searchValue: 'idAssignedContact',
                appearance: 'standard',
                class: 'autoselect',
                label: 'Assigned Contact',
                placeholder: 'Assigned Contact',
                defaultValue: ''
            },
            {
                prop: 'name',
                retProp: '',
                type: 'input',
                options: [],
                shown: true,
                appearance: 'standard',
                class: 'input',
                label: 'Name',
                placeholder: 'name',
            },
            {
                prop: 'commercialName',
                retProp: '',
                type: 'autoselect',
                options: [],
                shown: false,
                appearance: 'standard',
                label: 'Account',
                placeholder: 'Account',
                class: 'input',
            },
            {
                prop: '',
                retProp: '',
                searchValue: 'car',
                type: 'autocomplete',
                label: 'Car',
                placeholder: '',
                options: this._resolved.tableData.cars.content.map((car) => car.name),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: '',
                retProp: '',
                searchValue: 'subCar',
                type: 'autocomplete',
                label: 'SubCar',
                placeholder: '',
                options: this._resolved.tableData.subCars.content.map((subcar) => subcar.name),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: '',
                retProp: '',
                type: 'autocomplete',
                searchValue: 'type',
                label: 'Type',
                placeholder: '',
                options: parseValues(this._resolved.tableData.types),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: '',
                retProp: '',
                type: 'autocomplete',
                searchValue: 'priority',
                label: 'Priority',
                placeholder: '',
                options: parseValues(this._resolved.tableData.priorities),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: '',
                retProp: '',
                type: 'autocomplete',
                searchValue: 'territory',
                label: 'Territory',
                placeholder: '',
                options: parseValues(this._resolved.tableData.territories),
                shown: false,
                appearance: 'standard',
                class: 'autocomplete',
            },
            {
                prop: 'commercialName',
                retProp: '',
                type: 'autoselect',
                options: [],
                shown: false,
                searchValue: 'idAccount',
                appearance: 'standard',
                class: 'autoselect',
                label: 'Assigned Account',
                placeholder: 'Assigned Account',
                defaultValue: ''
            },
        ];
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const switched = this._querySwitcher(params, this._resolved, this.filters);
            const mapped = this._activityMapper.mapFrom(switched);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('activityFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('activity-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('activity-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        if (event.orderField === 'userEmail') {
            event.orderField = 'idUser';
        }
        if (event.orderField === 'accountName') {
            event.orderField = 'idAccount';
        }
        if (event.orderField === 'contactName') {
            event.orderField = 'idContact';
        }
        if (event.orderField === 'opportunityName') {
            event.orderField = 'idOpportunity';
        }

        let aux = this._activityMapper.mapFrom({[event.orderField]: ''});

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('activity-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    navigate(event: { id: string; property: string; item?: any }) {
        if (!event.id) {
            return;
        }
        if (event.property === 'id') {
            this._router.navigate([`./app/activity-detail/${event.id}`], {relativeTo: this._route});
        }
        if (event.property === 'accountName') {
            this._router.navigate([`./app/crm/accounts/account-detail/${event.item.idAccount}`]);
        }
        if (event.property === 'contactName') {
            this._router.navigate([`./app/crm/contacts/contact-detail/${event.item.idContact}`]);
        }
        if (event.property === 'opportunityName') {
            this._router.navigate([`./app/crm/opportunities/opportunity-detail/${event.item.idOpportunity}`]);
        }
    }

    setSize(event: PageEvent) {
        this.backSearch();
    }

}
