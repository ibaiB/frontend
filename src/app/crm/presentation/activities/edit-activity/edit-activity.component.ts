import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IActivity} from '../../../entities/activity/domain/IActivity';

import {IActivityEditData} from '../../../entities/activity/domain/IActivityEditData';

@Component({
    selector: 'app-edit-activity',
    templateUrl: './edit-activity.component.html',
    styleUrls: ['./edit-activity.component.scss']
})
export class EditActivityComponent implements OnInit {
    @Input() activityEditData: IActivityEditData;
    @Output() modActivity = new EventEmitter<any>();

    form: FormGroup;
    activity: IActivity;
    width = '100%';

    defaultState: string;
    defaultPriority: string;
    defaultType: string;
    defaultTerritory: string;
    defaultCar: string;
    defaultSubCar: string;
    defaultSupportBosonit: string;
    defaultUser: string;
    defaultAccount: string;
    activityType: string;
    activityTypeId: string;

    carOptions: string[] = [];
    subCarOptions: string[] = [];
    stateOptions: string[] = [];
    priorityOptions: string[] = [];
    probabilityOptions: string[] = [];
    typeOptions: string[] = [];
    territoryOptions: string[] = [];
    supportBosonitOptions: string[] = [];
    supportNfqOptions: string[] = [];
    supportTechOptions: string[] = [];

    constructor(
        private fb: FormBuilder,
    ) {
    }

    ngOnInit() {
        this.stateOptions = this.activityEditData.activityStates.map(state => state.name);
        this.priorityOptions = this.activityEditData.priorities.map(priority => priority.name);
        this.typeOptions = this.activityEditData.activityTypes.map(type => type.name);
        this.territoryOptions = this.activityEditData.territories.map(territory => territory.name);
        this.carOptions = this.activityEditData.cars.content.map(car => car.name);
        this.supportBosonitOptions = this.activityEditData.crmUsers.map(user => user.email);
        this.activity = this.activityEditData.activity;
        this.subCarOptions = this.activity.car ? this.activityEditData.cars.content.find(car => car.name === this.activity.car).subCars.map(subcar => subcar.name)
                                               : this.activityEditData.subCars.content.map(subcar => subcar.name);
        this.formInit();
        this.disableSupp();
    }

    // @TODO: future functionality
    disableSupp() {
        this.form.controls['supportNfq'].disable();
        this.form.controls['supportTech'].disable();
    }

    formInit() {
        this.defaultTerritory = this.activity.territory;
        this.defaultState = this.activity.state;
        this.defaultPriority = this.activity.priority;
        this.defaultType = this.activity.type;
        this.defaultCar = this.activity.car;
        this.defaultSubCar = this.activity.subCar;
        this.defaultSupportBosonit = this.activity.supportBosonit;
        this.defaultUser = this.activity.user?.email;

        this.form = this.fb.group({
            father: [this.activityTypeId],
            car: [this.defaultCar],
            subCar: [this.defaultSubCar],
            user: [this.defaultUser],
            supportBosonit: [this.defaultSupportBosonit],
            supportNfq: [''],
            supportTech: [''],
            type: [this.defaultType],
            state: [this.defaultState],
            priority: [this.defaultPriority],
            warningDate: [this.activity.warningDate],
            activityName: [this.activity.name, Validators.required],
            territory: [this.defaultTerritory],

            id: [this.activity.id],
            registerDate: [this.activity.registerDate],
            closeDate: [this.activity.closeDate],
        });
        this.form.get('car').valueChanges.subscribe(carValue => this.carValueChanged(carValue));
    }

    autocompleteSelected(event: { formField: string, value: string }) {
        this.form.controls[event.formField].setValue(event.value);
    }

    carValueChanged(carValue: string) {
        let car = this.activityEditData.cars.content.find(car => car.name === carValue);
        if (car) {
            this.subCarOptions = car.subCars.map(subCar => subCar.name);
        } else {
            this.subCarOptions = this.activityEditData.subCars.content.map(subCar => subCar.name);
        }
    }

    onSaveClick() {
        if (this.form.valid) {
            const priorityValue = this.form.get('priority').value;
            const carValue = this.form.get('car').value;
            const subCarValue = this.form.get('subCar').value;
            const supportBosonitValue = this.form.get('supportBosonit').value;
            const supportNFQValue = this.form.get('supportNfq').value;
            const supportTechValue = this.form.get('supportTech').value;
            const stateValue = this.form.get('state').value;
            const typeValue = this.form.get('type').value;
            const warningDateValue = this.form.get('warningDate').value;
            const closeDateValue = this.form.get('closeDate').value;
            const activityNameValue = this.form.get('activityName').value;
            const territoryValue = this.form.get('territory').value;
            const userValue = this.form.get('user').value;
            const registerDateValue = this.form.get('registerDate').value;

            const carFiltered = this.activityEditData.cars.content.find(car => car.name === carValue);
            const subCarFiltered = this.activityEditData.subCars.content.find(subCar => subCar.name === subCarValue);
            const supportBosonitFiltered = this.activityEditData.crmUsers.find(user => user.email === supportBosonitValue);
            const userFiltered = this.activityEditData.crmUsers.find(user => user.email === userValue);
            const priorityFiltered = this.activityEditData.priorities.find(priority => priority.name === priorityValue);
            const territoryFiltered = this.activityEditData.territories.find(territory => territory.name === territoryValue);

            this.modActivity.emit({
                id: this.activity.id,
                name: activityNameValue ? activityNameValue : this.activity.name,
                car: carFiltered ? carFiltered.id : null,
                subCar: subCarFiltered ? subCarFiltered.id : null,
                supportNFQ: this.activity.supportNFQ,
                supportBosonit: supportBosonitFiltered ? supportBosonitFiltered.id : null,
                supportTECH: this.activity.supportTECH,
                state: stateValue ? stateValue : this.activity.state,
                type: typeValue ? typeValue : this.activity.type,
                warningDate: warningDateValue ? new Date(warningDateValue).toISOString() : this.activity.warningDate,
                registerDate: registerDateValue ? new Date(registerDateValue).toISOString() : this.activity.registerDate,
                closeDate: closeDateValue ? new Date(closeDateValue).toISOString() : this.activity.closeDate,
                idAccount: this.activity.idAccount,
                idcontact: this.activity.idContact,
                idOpportunity: this.activity.idOpportunity,
                idUser: userFiltered ? userFiltered.id : null,
                priority: priorityFiltered ? priorityFiltered.value : this.activity.priority,
                territory: territoryFiltered ? territoryFiltered.value : null,
                cifAccount: this.activity.cifAccount
            });
        }
    }
}
