import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Router} from '@angular/router';

@Component({
  selector: 'app-activity-detail-dialog',
  templateUrl: './activity-detail-dialog.component.html',
  styleUrls: ['./activity-detail-dialog.component.scss']
})
export class ActivityDetailDialogComponent implements OnInit {

  public warningDateParsed: Date;
  public today: Date;
  color = this.data.activity.color;

  constructor(
    public dialogRef: MatDialogRef<ActivityDetailDialogComponent>,
    private _router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.warningDateParsed = new Date(this.data.activity.warningDate);
    this.today = new Date();
    if(this.data.activity)this.showValues();
  }

  closeDialog() {
    this.dialogRef.close();
  }

  goToDetails(id: string) {
    this.dialogRef.close();
    this._router.navigate([`./app/crm/activities/activity-detail/${id}`]);
}

showValues(){
  const carValue = this.data.activity.car && this.data.activity.car.length===2  ? this.data.cars.content.filter(car=>car.id === this.data.activity.car)[0].name : this.data.activity.car;
  const subCarValue = this.data.activity.subCar && this.data.activity.subCar.length===2? this.data.subCars.content.filter(subcar=>subcar.id === this.data.activity.subCar)[0].name: this.data.activity.subCar;
  this.data.activity.car = carValue;
  this.data.activity.subCar = subCarValue;
}
}
