import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActivityDetailDialogComponent} from './activity-detail-dialog.component';

describe('ActivityDetailDialogComponent', () => {
  let component: ActivityDetailDialogComponent;
  let fixture: ComponentFixture<ActivityDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
