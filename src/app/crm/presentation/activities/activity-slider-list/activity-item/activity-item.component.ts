import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivityService} from '../../../../services/activity.service';
import {ActivityDetailDialogComponent} from '../../activity-detail-dialog/activity-detail-dialog.component';
import {IActivityItemData} from '../../../../entities/activity/domain/IActivityItemData';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-activity-item',
    templateUrl: './activity-item.component.html',
    styleUrls: ['./activity-item.component.scss']
})
export class ActivityItemComponent implements OnInit {

    @Input() activityItemData: IActivityItemData;
    @Output() modActivity = new EventEmitter<any>();
    activityType:string;

    public warningDateParsed: Date;
    public today: Date;

    constructor(
        public dialog: MatDialog,
        public activityService: ActivityService,
        private _router: Router,
        private _route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.warningDateParsed = new Date(this.activityItemData.activity.warningDate);
        this.today = new Date();
        if(this.activityItemData.activity.idAccount){
            this.activityType="Account";
          }else if(this.activityItemData.activity.idContact){
            this.activityType="Contact";
          }else if(this.activityItemData.activity.idOpportunity){
            this.activityType="Opportunity";
          }
    }

    openEditActivityDialog() {
            this._router.navigate([`./app/crm/activities/activity-detail/${this.activityItemData.activity.id}`]);
    }

    openDetailActivityDialog() {
        const dialogRef = this.dialog.open(ActivityDetailDialogComponent, {
            data:
                {
                    activity: this.activityItemData.activity,
                    father: {
                        type: this.activityType,
                        value: this.activityItemData.father
                    },
                    crmUsers: this.activityItemData.crmUsers,
                    myCrm: this.activityItemData.myCrm,
                    states: this.activityItemData.states,
                    priorities: this.activityItemData.priorities,
                    types: this.activityItemData.types,
                    territories: this.activityItemData.territories,
                    cars: this.activityItemData.cars,
                    subCars: this.activityItemData.subCars
                }
        });
    }
}
