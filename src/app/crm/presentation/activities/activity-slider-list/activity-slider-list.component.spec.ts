import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ActivitySliderListComponent} from './activity-slider-list.component';

describe('ActivitySliderListComponent', () => {
  let component: ActivitySliderListComponent;
  let fixture: ComponentFixture<ActivitySliderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivitySliderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitySliderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
