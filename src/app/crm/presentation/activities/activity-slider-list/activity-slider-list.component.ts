import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {IActivity} from '../../../entities/activity/domain/IActivity';
import {CreateActivityComponent} from '../create-activity/create-activity.component';
import {IActivitySliderListData} from '../../../entities/activity/domain/IActivitySliderListData';
import {ActivityItemDataMapper} from '../../../entities/activity/infrastructure/activity-item-data-mapper';

@Component({
    selector: 'app-activity-slider-list',
    templateUrl: './activity-slider-list.component.html',
    styleUrls: ['./activity-slider-list.component.scss']
})
export class ActivitySliderListComponent implements OnInit {

    @Input() activitySliderListData: IActivitySliderListData;

    @Output() newActivity = new EventEmitter<IActivity>();
    @Output() modActivity = new EventEmitter<IActivity>();

    constructor(
        public dialog: MatDialog,
        private _activityItemDataMapper: ActivityItemDataMapper
    ) {
    }

    ngOnInit() {

    }

    openNewActivityDialog() {
        //console.log(this.activitySliderListData)
        let data = {
            father: {
                type: this.activitySliderListData.fatherType === 'Account' ? 'Account' : (this.activitySliderListData.fatherType === 'Contact' ? 'Contact' : 'Opportunity'),
                value: this.activitySliderListData.father
            },
            crmUsers: this.activitySliderListData.crmUsers,
            myCrm: this.activitySliderListData.myCrm,
            states: this.activitySliderListData.states,
            territories: this.activitySliderListData.territories,
            priorities: this.activitySliderListData.priorities,
            cars: this.activitySliderListData.cars,
            subCars: this.activitySliderListData.subCars,
            types: this.activitySliderListData.types,
            //@TODO future versions
            supportNFQ: [],
            supportTech: []
        };

        const dialogRef = this.dialog.open(CreateActivityComponent, {
            data: data
        });

        dialogRef
            .afterClosed()
            .subscribe(activity => {
                if (activity !== undefined) {
                    this.newActivity.emit(activity);
                }
            });
    }

    editActivity(event: any) {
        this.modActivity.emit(event);
    }

    mapActivity(item: IActivity) {
        return this._activityItemDataMapper.mapTo(this.activitySliderListData, item);
    }
}
