import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {getSingleAccount} from 'src/app/crm/entities/account/application/getSingleAccount';
import {AccountService} from 'src/app/crm/services/account.service';
import {ContactService} from '../../../services/contact.service';
import {OpportunityService} from '../../../services/opportunity.service';
import {UserService} from '../../../services/user.service';
import {IAuto} from '../../../../shared/custom-mat-elements/dynamic';
import {accountAutoSelect, contactAutoSelect, opportunityAutoSelect} from './config';
import {Observable} from 'rxjs';
import {getAccounts} from '../../../entities/account/application/getAccounts';
import {getContacts} from '../../../entities/contact/application/getContacts';
import {IAccount} from '../../../entities/account/domain/IAccount';
import {IContact} from '../../../entities/contact/domain/IContact';
import {IOpportunity} from '../../../entities/opportunity/domain/IOpportunity';
import {getOpportunities} from '../../../entities/opportunity/application/getOpportunities';

@Component({
    selector: 'app-create-activity',
    templateUrl: './create-activity.component.html',
    styleUrls: ['./create-activity.component.scss']
})
export class CreateActivityComponent implements OnInit {

    form: FormGroup;

    defaultCar: string = '';
    defaultSubCar: string = '';
    defaultTerritory: string = '';
    activityType: string = '';
    activityTypeId: string = '';
    defaultAccountName: string = '';
    defaultContactName: string = '';
    defaultOpportunityName: string = '';

    carOptions: string[] = [];
    subCarOptions: string[] = [];
    supportBosonitOptions: string[] = [];
    supportNFQOptions: string[] = [];
    supportTechOptions: string[] = [];
    typeOptions: string[] = [];
    stateOptions: string[] = [];
    priorityOptions: string[] = [];
    territoryOptions: string[] = [];

    accountAutoSelect: IAuto = accountAutoSelect;
    contactAutoSelect: IAuto = contactAutoSelect;
    opportunityAutoSelect: IAuto = opportunityAutoSelect;

    account: IAccount;
    contact: IContact;
    opportunity: IOpportunity;

    constructor(
        private fb: FormBuilder,
        public dialogRef: MatDialogRef<CreateActivityComponent>,
        private _accountService: AccountService,
        private _contactService: ContactService,
        private _opportunityService: OpportunityService,
        private _userService: UserService
    ) {
    }

    ngOnInit() {
        this.formInit();
    }

    getAccount(id: string) {
        getSingleAccount(id, this._accountService).subscribe(account => this.form.get('account').setValue(account.commercialName));
    }

    formInit() {
        this.form = this.fb.group({
            car: [''],
            subCar: [''],
            user: [''],
            supportBosonit: [''],
            type: [''],
            state: [''],
            priority: ['Normal'],
            warningDate: [''],
            activityName: ['', Validators.required],
            territory: ['']
        });
    }

    autocompleteSelected(event: { formField: string, value: string }) {
        this.form.controls[event.formField].setValue(event.value);
    }

    search(field: string, value: string, query: any) {
        //TODO: hablar con back para implementar nameSurname en contacto
        let $query: Observable<any>;
        if (field === 'account') {
            $query = getAccounts(query, this._accountService);
        }
        if (field === 'contact') {
            $query = getContacts(query, this._contactService);
        }
        if (field === 'opportunity') {
            $query = getOpportunities(query, this._opportunityService);
        }

        $query
            .subscribe((queryResult: any[]) => {
                this.setOptions(field, queryResult);
            });
    }

    setOptions(field: string, options: any) {
        if (field === 'account') {
            this.accountAutoSelect.options = options;
        }
        if (field === 'contact') {
            this.contactAutoSelect.options = options.map(this.addContactVirtualName);
        }
        if (field === 'opportunity') {
            this.opportunityAutoSelect.options = options.content;
        }
    }

    addContactVirtualName(contact: IContact): any {
        return {
            contact,
            virtualName: this.chainContactNameSurname(contact)
        };
    }

    chainContactNameSurname(contact: IContact): string {
        return `${contact.name} ${contact.surname ?? ''}`;
    }

    setValue(field: string, value: any) {
        if (field === 'account') {
            this.handleSetAccount(value);
        }
        if (field === 'contact') {
            this.handleSetContact(value);
        }
        if (field === 'opportunity') {
            this.handleSetOpportunity(value);
        }
    }

    handleSetAccount(account: IAccount) {
        this.account = account;
        this.contactAutoSelect.options = account.contacts.map(c => this.addContactVirtualName(c));
        this.opportunityAutoSelect.options = account.opportunities;
    }

    handleSetContact(contact: IContact) {
        this.account = contact.account ?? this.account;
        this.accountAutoSelect.defaultValue = contact.account?.commercialName ?? 'No account associated';
        this.contact = contact;
        this.opportunityAutoSelect.options = contact.opportunities;
    }

    handleSetOpportunity(opportunity: IOpportunity) {
        this.account = opportunity.account ?? this.account;
        this.accountAutoSelect.defaultValue = opportunity.account?.commercialName ?? 'No account associated';
        this.contact = opportunity.contact ?? this.contact;
        this.accountAutoSelect.defaultValue = opportunity.contact ? this.chainContactNameSurname(opportunity.contact) : 'No contact associated';
        this.opportunity = opportunity;
    }

    clear(field: string) {
        if (field === 'account') {
            this.account = null;
        }
        if (field === 'contact') {
            this.contact = null;
        }
        if (field === 'opportunity') {
            this.opportunity = null;
        }
    }

    onCancelClick() {
        this.dialogRef.close();
    }

    onSaveClick() {
        if (this.form.valid) {
            this.dialogRef.close();
        }
    }

}
