import {IAuto} from '../../../../shared/custom-mat-elements/dynamic';

export const accountAutoSelect: IAuto = {
    options: [],
    retProp: 'id',
    prop: 'commercialName',
    searchValue: 'commercialName',
    appearance: 'standard',
    class: '',
    label: 'Account',
    placeholder: 'Account',
    defaultValue: '',
    type: 'autoselect'
};

export const contactAutoSelect: IAuto = {
    options: [],
    retProp: 'id',
    prop: 'virtualName',
    searchValue: 'name',
    appearance: 'standard',
    class: '',
    label: 'Contact',
    placeholder: 'Contact',
    defaultValue: '',
    type: 'autoselect'
};

export const opportunityAutoSelect: IAuto = {
    options: [],
    retProp: 'id',
    prop: 'name',
    searchValue: 'name',
    appearance: 'standard',
    class: '',
    label: 'Opportunity',
    placeholder: 'Opportunity',
    defaultValue: '',
    type: 'autoselect'
};
