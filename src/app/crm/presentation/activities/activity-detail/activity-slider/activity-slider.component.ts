import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IActivitySliderData} from '../../../../entities/activity/domain/IActivitySliderData';
import {IContactSliderListData} from '../../../../entities/contact/domain/IContactSliderListData';
import {IContact} from '../../../../entities/contact/domain/IContact';
import {IOpportunitySliderListData} from '../../../../entities/opportunity/domain/IOpportunitySliderListData';
import {IOpportunity} from '../../../../entities/opportunity/domain/IOpportunity';
import {IOpportunityItemData} from '../../../../entities/opportunity/domain/IOpportunityItemData';
import {OpportunityItemDataMapper} from '../../../../entities/opportunity/infrastructure/opportunity-item-data-mapper';
import {OpportunitySliderListDataMapper} from '../../../../entities/opportunity/infrastructure/opportunity-slider-list-data-mapper';
import {INoteSliderListData} from '../../../../entities/note/domain/INoteSliderListData';
import { getSingleAccount } from 'src/app/crm/entities/account/application/getSingleAccount';
import { AccountService } from 'src/app/crm/services/account.service';

@Component({
    selector: 'app-activity-slider',
    templateUrl: './activity-slider.component.html',
    styleUrls: ['./activity-slider.component.scss']
})
export class ActivitySliderComponent implements OnInit {
    contactSliderListData: IContactSliderListData;
    opportunitySliderListData: IOpportunitySliderListData;
    noteSliderListData: INoteSliderListData;

    contact: IContact;
    contactNotNull = false;
    opportunityNotNull = false;
    opportunity: IOpportunity;
    opportunityItemData: IOpportunityItemData;

    @Input() activitySliderData: IActivitySliderData;

    @Output() newContact = new EventEmitter<IContact>();
    @Output() newOpportunity = new EventEmitter<IOpportunity>();

    constructor(private _opportunityItemDataMapper: OpportunityItemDataMapper,
                private _opportunitySliderListDataMapper: OpportunitySliderListDataMapper,
                private _accountService : AccountService) {
    }

    ngOnInit() {
        if (this.activitySliderData.activity.idAccount) {
            this.activitySliderData.account = this.activitySliderData.activity.account;
        }

        if (this.activitySliderData.activity.idContact) {
            this.contact = this.activitySliderData.activity.contact;
            this.getAccount(this.contact.accountId);
            this.activitySliderData.account=this.contact.account;
        }
        if (this.activitySliderData.activity.idOpportunity) {
            this.opportunity = this.activitySliderData.activity.opportunity;
            //TODO: Delete getAccount and put -->
            //this.opportunity.contact.account
            this.opportunity.account ? this.activitySliderData.account=this.opportunity.account 
                                     : this.getAccount(this.opportunity.contact.accountId);
            this.contact= this.opportunity.contact ?? null;
        }
        if(this.contact)this.contactNotNull=true;
        if(this.opportunity)this.opportunityNotNull=true;

        this.opportunitySliderListData = this._opportunitySliderListDataMapper.mapTo(this.activitySliderData, this.activitySliderData.activity);
        this.noteSliderListData = {father: this.activitySliderData.activity, type: 'Activity'};
    }

    mapOpportunity(): IOpportunityItemData {
        return this._opportunityItemDataMapper.mapTo(this.opportunitySliderListData, this.opportunity);
    }

    //TODO: Delete and change ngOnInit
    getAccount(id:string) {
        getSingleAccount(id,this._accountService).
        subscribe(account=> this.activitySliderData.account=account);
    }
}
