import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IActivity} from '../../../entities/activity/domain/IActivity';
import {IActivityDetailResolved} from '../../../entities/activity/domain/IActivityDetailResolved';
import {IActivityEditData} from '../../../entities/activity/domain/IActivityEditData';
import Swal from 'sweetalert2';
import {ActivityEditDataMapper} from '../../../entities/activity/infrastructure/activity-edit-data-mapper';
import {IActivitySliderData} from '../../../entities/activity/domain/IActivitySliderData';
import {ActivitySliderDataMapper} from '../../../entities/activity/infrastructure/activity-slider-data-mapper';
import {ActivityService} from '../../../services/activity.service';
import {updateActivity} from '../../../entities/activity/application/updateActivity';
import {WebActivityMapper} from '../../../entities/activity/infrastructure/web-activity-mapper';

@Component({
  selector: 'app-activity-detail',
  templateUrl: './activity-detail.component.html',
  styleUrls: ['./activity-detail.component.scss']
})
export class ActivityDetailComponent implements OnInit {
  loading:boolean=false;
  public warningDateParsed: Date;
  public today: Date;
  activity:IActivity;
  activityEditData:IActivityEditData;
  activitySliderData: IActivitySliderData;
  private _activityDetailResolved:IActivityDetailResolved;

  constructor(
    private _route: ActivatedRoute,
    private _activityEditDataMapper: ActivityEditDataMapper,
    private _activitySliderDataMapper: ActivitySliderDataMapper,
    private _webActivityMapper: WebActivityMapper,
    private _activityService:ActivityService
  ) { }

  ngOnInit() {
    this._activityDetailResolved = this._route.snapshot.data['response'];
    if (this._activityDetailResolved.error) {
      console.error(this._activityDetailResolved.error.message, this._activityDetailResolved.error.error);
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
        confirmButtonColor: '#db5e5e'
      });
    }
      if (this._activityDetailResolved.detailData !== null) {
        this.activitySliderData = this._activitySliderDataMapper.mapTo(this._activityDetailResolved.detailData);
        this.activityEditData = this._activityEditDataMapper.mapTo(this._activityDetailResolved.detailData);
        this.activity = this._activityDetailResolved.detailData.activity;
        this.warningDateParsed = new Date(this.activity.warningDate);
        this.loading = false;
      }
      this.today = new Date();
    }
    
    editActivity(event:any){
      updateActivity(this._webActivityMapper.mapFrom(event), this._activityService)
      .subscribe(edActivity => {
        this.activity=edActivity;
        Swal.fire({
          icon: 'success',
          title: 'Success',
          text: 'The account was edited successfully!',
          confirmButtonColor: '#db5e5e'
        })
      }, error => {
        console.error('error at edit account', error);
        Swal.fire({
          icon: 'error',
          title: 'Something went wrong!',
          text: "Changes couldn't be saved",
          confirmButtonColor: '#db5e5e'
        });
      })
    }
  }

