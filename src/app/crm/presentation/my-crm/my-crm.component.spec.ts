import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MyCrmComponent} from './my-crm.component';

describe('MyCrmComponent', () => {
  let component: MyCrmComponent;
  let fixture: ComponentFixture<MyCrmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCrmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCrmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
