import {RouterModule, Routes} from "@angular/router";
import {ModuleWithProviders} from '@angular/core';
import {AccountTableComponent} from './presentation/accounts/account-table/account-table.component';
import {ContactTableComponent} from './presentation/contacts/contact-table/contact-table.component';
import {AccountDetailComponent} from './presentation/accounts/account-detail/account-detail.component';
import {ContactDetailComponent} from './presentation/contacts/contact-detail/contact-detail.component';
import {OpportunityManagementComponent} from './presentation/opportunities/opportunity-management/opportunity-management.component';
import {ActivityTableComponent} from './presentation/activities/activity-table/activity-table.component';
import {AccountTableResolverService} from './resolvers/account-table-resolver.service';
import {ContactTableResolverService} from './resolvers/contact-table-resolver.service';
import {AccountDetailResolverService} from './resolvers/account-detail-resolver.service';
import {ContactDetailResolverService} from "./resolvers/contact-detail-resolver.service";
import {ActivityTableResolverService} from './resolvers/activity-table-resolver.service';
import {ActivityDetailResolverService} from './resolvers/activity-detail-resolver.service';
import {OpportunityManagementResolverService} from './resolvers/opportunity-management-resolver.service';
import { ActivityDetailComponent } from "./presentation/activities/activity-detail/activity-detail.component";
import { OpportunityFullDetailComponent } from "./presentation/opportunities/opportunity-full-detail/opportunity-full-detail.component";
import { OpportunityDetailResolverService } from "./resolvers/opportunity-detail-resolver.service";

const routes: Routes = [
    {
        path: 'accounts',
        data: {breadcrumb: 'Accounts'},
        children: [
            {
                path: '',
                component: AccountTableComponent,
                resolve: {response: AccountTableResolverService},
                data: {breadcrumb: ''},
            },
            {
                path: 'account-detail/:id',
                component: AccountDetailComponent,
                data: {breadcrumb: ''},
                resolve: {response: AccountDetailResolverService}
            }
        ]
    },
    {
        path: 'contacts',
        data: {breadcrumb: 'Contacts'},
        children: [
            {
                path: '',
                component: ContactTableComponent,
                data: {breadcrumb: ''},
                resolve: {response: ContactTableResolverService}
            },
            {
                path: 'contact-detail/:id',
                component: ContactDetailComponent,
                data: {breadcrumb: ''},
                resolve: {response: ContactDetailResolverService}
            }
        ]
    },
    {
        path: 'activities',
        data: {breadcrumb: 'Activities'},
        children: [
            {
                path: '',
                component: ActivityTableComponent,
                data: {breadcrumb: ''},
                resolve: {response: ActivityTableResolverService}
            },
            {
                path: 'activity-detail/:id',
                component: ActivityDetailComponent,
                data: {breadcrumb: ''},
                resolve: {response: ActivityDetailResolverService}
            }
        ]
    },
    {
        path: 'opportunities',
        data: {breadcrumb: 'Opportunities'},
        children: [
            {
                path: '',
                component: OpportunityManagementComponent,
                data: {breadcrumb: ''},
                resolve: {response: OpportunityManagementResolverService}
            },
            {
                path: 'opportunity-detail/:id',
                component: OpportunityFullDetailComponent,
                data: {breadcrumb: ''},
                resolve: {response: OpportunityDetailResolverService}
            }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes)
