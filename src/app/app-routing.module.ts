import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './auth/session/infrastructure/ng-components/login/login.component';

import {HomeComponent} from './home/home.component';
import {AuthGuardService} from './auth/config/auth-guard.service';
import {AuthNoGuardService} from './auth/config/auth-no-guard.service';
import {NotFoundComponent} from './shared/presentation/not-found/not-found.component';
import {ForbiddenComponent} from './shared/presentation/forbidden/forbidden.component';
import {EvaluationComponent} from './evaluations/exam/infrastructure/ng-components/evaluation/evaluation.component';
import {MyStaffitResolverService} from './internal/employee/infrastructure/resolvers/my-staffit-resolver.service';
import {HomeResolverService} from './home/middleware/home-resolver.service';
import {ComingSoonComponent} from './shared/presentation/coming-soon/coming-soon.component';
import {NewEmployeeRequestManagementComponent} from './internal/new-employee-request/infrastructure/ng-components/new-employee-request-management/new-employee-request-management.component';
import {NewEmployeeRequestResolverService} from './internal/new-employee-request/infrastructure/resolvers/new-employee-request-resolver';
import {CreateNewEmployeeRequestComponent} from './internal/new-employee-request/infrastructure/ng-components/create-new-employee-request/create-new-employee-request.component';
import {DetailNewEmployeeRequestResolverService} from './internal/new-employee-request/infrastructure/resolvers/detail-new-employee-request-resolver.service';
import {FromCandidateResolverService} from './internal/new-employee-request/infrastructure/resolvers/from-candidate-resolver.service';
import {EmployeeManagementComponent} from './internal/employee/infrastructure/ng-components/employee-management/employee-management.component';
import {EditEmployeeResolverService} from './internal/employee/infrastructure/resolvers/edit-employee-resolver.service';
import {EmployeeTableComponent} from './internal/employee/infrastructure/ng-components/employee-table/employee-table.component';
import {EmployeeTableResolverService} from './internal/employee/infrastructure/resolvers/employee-table-resolver.service';
import {BillingComponent} from './internal/billing/billing.component';
import {MyEmployeeComponent} from './internal/employee/infrastructure/ng-components/my-employee/my-employee.component';
import {EmployeesComponent} from './internal/employee/infrastructure/ng-components/employees/employees.component';
import {WhoIsWhoComponent} from './my-staffit/who-is-who/infrastructure/ng-components/who-is-who/who-is-who.component';
import {ScopesGuardService} from './auth/middelware/scopes-guard.service';
import {StorageComponent} from './shared/file/infrastructure/ng-components/storage/storage.component';
import {RoomListComponent} from './internal/room/infrastructure/ng-components/room-list/room-list.component';
import {RoomListManagementResolverService} from './internal/room/infrastructure/resolvers/room-list-resolver.service';
import {RoomDetailComponent} from './internal/room/infrastructure/ng-components/room-detail/room-detail.component';
import {RoomDetailResolverService} from './internal/room/infrastructure/resolvers/room-detail-resolver.service';
import {AdminCompanyResolverService} from './internal/company/infrastructure/resolvers/admin-company-resolver.service';
import {AdminCompanyComponent} from './internal/company/infrastructure/ng-components/admin-company/admin-company.component';


const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [AuthNoGuardService]
    },
    {
        path: 'app',
        component: HomeComponent,
        canActivate: [AuthGuardService],
        canActivateChild: [ScopesGuardService],
        resolve: {response: HomeResolverService},
        children: [
            {
                path: 'administration',
                data: {breadcrumb: 'Administration'},
                loadChildren: () => import('./administration/administration.module')
                    .then(mod => mod.AdministrationModule)
            },
            {
                path: 'crm',
                data: {breadcrumb: 'Crm'},
                loadChildren: () => import('./crm/crm.module')
                    .then(mod => mod.CrmModule)
            },
            {
                path: 'my-staffit',
                data: {breadcrumb: 'My Staffit'},
                children: [
                    {
                        path: 'my-employee',
                        data: {breadcrumb: 'My Employee'},
                        resolve: {response: MyStaffitResolverService},
                        component: MyEmployeeComponent,
                    },
                    {
                        path: 'who-is-who',
                        data: {breadcrumb: 'Who is who'},
                        component: WhoIsWhoComponent
                    },
                    {
                        path: 'employees',
                        data: {breadcrumb: 'Employees'},
                        component: EmployeesComponent,
                    },
                    {
                        path: 'rooms',
                        data: {breadcrumb: 'Rooms'},
                        children: [
                            {
                                path: ':id',
                                component: RoomDetailComponent,
                                resolve: {response: RoomDetailResolverService},
                                data: {
                                    breadcrumb: ':id',
                                    removable: false
                                },
                            },
                            {
                                path: '',
                                component: RoomListComponent,
                                data: {readOnly: true},
                                resolve: {response: RoomListManagementResolverService},
                            }
                        ]
                    },
                    {
                        path: 'know-the-company',
                        data: {
                            breadcrumb: 'Know the company',
                            readOnly: true
                        },
                        component: AdminCompanyComponent,
                        resolve: {response: AdminCompanyResolverService}
                    },
                    {
                        path: 'test',
                        component: StorageComponent
                    }
                ]
            },
            {
                path: 'recruitment',
                loadChildren: () => import('./recruitment/recruitment.module')
                    .then(mod => mod.RecruitmentModule),
                data: {breadcrumb: 'Recruitment'},
            },
            {
                path: 'internal',
                data: {breadcrumb: 'Internal'},
                children: [
                    {
                        path: 'employee',
                        data: {breadcrumb: 'Employee management'},
                        children: [
                            {
                                path: 'new-requests',
                                data: {breadcrumb: 'New employee requests'},
                                children: [
                                    {
                                        path: '',
                                        component: NewEmployeeRequestManagementComponent,
                                        data: {breadcrumb: 'All'},
                                        resolve: {response: NewEmployeeRequestResolverService}
                                    },
                                    {
                                        path: 'new',
                                        component: CreateNewEmployeeRequestComponent,
                                        data: {breadcrumb: 'New'},
                                        resolve: {response: DetailNewEmployeeRequestResolverService}
                                    },
                                    {
                                        path: 'fromCandidate/:id',
                                        component: CreateNewEmployeeRequestComponent,
                                        data: {breadcrumb: 'From candidate/:id'},
                                        resolve: {response: FromCandidateResolverService}
                                    },
                                    {
                                        path: 'detail/:id',
                                        component: CreateNewEmployeeRequestComponent,
                                        data: {breadcrumb: 'detail/:id'},
                                        resolve: {response: DetailNewEmployeeRequestResolverService}
                                    }
                                ]
                            },
                            {
                                path: 'reports',
                                data: {breadcrumb: 'Reports'},
                                children: [
                                    {
                                        path: 'edit/:id',
                                        component: EmployeeManagementComponent,
                                        data: {breadcrumb: 'edit/:id'},
                                        resolve: {response: EditEmployeeResolverService}
                                    },
                                    {
                                        path: '',
                                        component: EmployeeTableComponent,
                                        data: {breadcrumb: 'All'},
                                        resolve: {response: EmployeeTableResolverService}
                                    },
                                ]
                            },
                        ]
                    },
                    {
                        path: 'billing',
                        component: BillingComponent,
                        data: {breadcrumb: 'Billing'},
                    },
                    {
                        path: 'admin-know-the-company',
                        component: AdminCompanyComponent,
                        data: {
                            breadcrumb: 'Admin know the company',
                            readOnly: false
                        },
                        resolve: {response: AdminCompanyResolverService}
                    },
                    {
                        path: 'room-management',
                        data: {breadcrumb: 'Room management'},
                        children: [
                            {
                                path: ':id',
                                component: RoomDetailComponent,
                                resolve: {response: RoomDetailResolverService},
                                data: {
                                    breadcrumb: ':id',
                                    removable: true
                                },
                            }, {
                                path: '',
                                component: RoomListComponent,
                                data: {breadcrumb: ''},
                                resolve: {response: RoomListManagementResolverService},
                            }
                        ]
                    }
                ]
            },
            {
                path: 'systems-administrator',
                data: {breadcrumb: 'Systems administrator'},
                loadChildren: () => import('./systems-administrator/systems-administrator.module')
                    .then(mod => mod.SystemsAdministratorModule)
            },
            {
                path: 'bonuses',
                data: {breadcrumb: 'Bonuses'},
                loadChildren: () => import('./bonuses/bonuses.module')
                    .then(mod => mod.BonusesModule)
            },
            {
                path: 'evaluations',
                data: {breadcrumb: 'Evaluations'},
                loadChildren: () => import('./evaluations/evaluations.module')
                    .then(mod => mod.EvaluationsModule)
            },
            {
                path: 'requests',
                data: {breadcrumb: 'Requests'},
                loadChildren: () => import('./requests/requests.module')
                    .then(mod => mod.RequestsModule)
            },
            {
                path: 'coming-soon',
                component: ComingSoonComponent
            },
        ]
    },
    {
        path: 'exams',
        component: EvaluationComponent
    },
    {
        path: 'forbidden',
        component: ForbiddenComponent
    },
    {
        path: 'coming-soon',
        component: ComingSoonComponent
    },
    {
        path: '',
        redirectTo: 'app/my-staffit/my-employee',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
