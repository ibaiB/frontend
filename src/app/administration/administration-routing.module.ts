import { RouterModule, Routes } from "@angular/router";
import { ModuleWithProviders } from '@angular/core';
import { RoomPortfolioComponent } from './presentation/room-portfolio/room-portfolio.component';
import { NewFlatComponent } from './presentation/new-flat/new-flat.component';
import { OccupationComponent } from './presentation/occupation/occupation.component';
import { BookingRoomComponent } from './presentation/booking-room/booking-room.component';

const routes: Routes = [
    {
        path: 'room-portfolio',
        component: RoomPortfolioComponent
    },
    {
        path: 'new-flat',
        component: NewFlatComponent
    },
    {
        path: 'occupation',
        component: OccupationComponent
    },
    {
        path: 'booking-room',
        component: BookingRoomComponent
    },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes)
