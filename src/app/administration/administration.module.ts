import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {routing} from './administration-routing.module';
import {RoomPortfolioComponent} from './presentation/room-portfolio/room-portfolio.component';
import {NewFlatComponent} from './presentation/new-flat/new-flat.component';
import {OccupationComponent} from './presentation/occupation/occupation.component';
import {BookingRoomComponent} from './presentation/booking-room/booking-room.component';
import {AngularMaterialModule} from '../shared/material/angular-material.module';
import {HttpClientModule} from '@angular/common/http';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import {AccordionModule} from 'primeng/accordion';
import {ChartModule} from 'primeng/chart';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ScrollingModule} from '@angular/cdk/scrolling';

@NgModule({
    declarations: [
        RoomPortfolioComponent,
        NewFlatComponent,
        OccupationComponent,
        BookingRoomComponent,
    ],
    imports: [
        CommonModule,
        routing,
        AngularMaterialModule,
        HttpClientModule,
        NgxMatSelectSearchModule,
        AccordionModule,
        ChartModule,
        DragDropModule,
        ScrollingModule
    ]
})
export class AdministrationModule {
}
