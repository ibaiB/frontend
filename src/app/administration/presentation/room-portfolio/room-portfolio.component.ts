import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-room-portfolio',
  templateUrl: './room-portfolio.component.html',
  styleUrls: ['./room-portfolio.component.scss'],
})
export class RoomPortfolioComponent implements OnInit {
  panelOpenState = false;

  constructor() {}

  cities: string[] = [
    'Logroño',
    'Madrid',
    'Pamplona',
    'Bilbao',
    'Jaén',
    'Zaragoza',
  ];

  ngOnInit(): void {}
}
