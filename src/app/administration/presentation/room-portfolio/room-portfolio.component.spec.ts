import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomPortfolioComponent } from './room-portfolio.component';

describe('RoomPortfolioComponent', () => {
  let component: RoomPortfolioComponent;
  let fixture: ComponentFixture<RoomPortfolioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomPortfolioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
