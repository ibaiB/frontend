import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';

@Component({
  selector: 'app-new-flat',
  templateUrl: './new-flat.component.html',
  styleUrls: ['./new-flat.component.scss'],
})
export class NewFlatComponent implements OnInit {
  isLinear = true;
  firstFormGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder) {}

  types: string[] = ['Trial period', 'End of practices', 'Dismissal'];
  cities: string[] = [
    'Logroño',
    'Bilbao',
    'Madrid',
    'Jaén',
    'Pamplona',
    'Zaragoza',
  ];

  ngOnInit(): void {
    this.firstFormGroup = this._formBuilder.group({
      adress: ['', Validators.required],
      roomNumber: ['', Validators.required],
      roomPrice: ['', Validators.required],
      city: ['', Validators.required],
    });
  }
}
