import {ModuleWithProviders} from '@angular/core';
import {ChildrenOutletContexts, RouterModule, Routes} from '@angular/router';
import {EmployeeManagementComponent} from './employee/infrastructure/ng-components/employee-management/employee-management.component';
import {EditEmployeeResolverService} from './employee/infrastructure/resolvers/edit-employee-resolver.service';
import {EmployeeTableComponent} from './employee/infrastructure/ng-components/employee-table/employee-table.component';
import {EmployeeTableResolverService} from './employee/infrastructure/resolvers/employee-table-resolver.service';
import {BillingComponent} from './billing/billing.component';
import {NewEmployeeRequestManagementComponent} from './new-employee-request/infrastructure/ng-components/new-employee-request-management/new-employee-request-management.component';
import {NewEmployeeRequestResolverService} from './new-employee-request/infrastructure/resolvers/new-employee-request-resolver';
import {CreateNewEmployeeRequestComponent} from './new-employee-request/infrastructure/ng-components/create-new-employee-request/create-new-employee-request.component';
import {DetailNewEmployeeRequestResolverService} from './new-employee-request/infrastructure/resolvers/detail-new-employee-request-resolver.service';
import {FromCandidateResolverService} from './new-employee-request/infrastructure/resolvers/from-candidate-resolver.service';
import {KnowTheCompanyManagementComponent} from './know-the-company/know-the-company-management/know-the-company-management.component';
import {RoomListComponent} from './room/infrastructure/ng-components/room-list/room-list.component';
import { RoomListManagementResolverService } from './room/infrastructure/resolvers/room-list-resolver.service';
import { CalendarContainerComponent } from '../shared/calendar/infrastructure/ng-componets/calendar-container/calendar-container.component';

const routes: Routes = [
    {
        path: 'employee',
        data: {breadcrumb: 'Employee management'},
        children: [
            {
                path: 'new-requests',
                data: {breadcrumb: 'New employee requests'},
                children: [
                    {
                        path: '',
                        component: NewEmployeeRequestManagementComponent,
                        data: {breadcrumb: 'All'},
                        resolve: {response: NewEmployeeRequestResolverService}
                    },
                    {
                        path: 'new',
                        component: CreateNewEmployeeRequestComponent,
                        data: {breadcrumb: 'New'},
                        resolve: {response: DetailNewEmployeeRequestResolverService}
                    },
                    {
                        path: 'fromCandidate/:id',
                        component: CreateNewEmployeeRequestComponent,
                        data: {breadcrumb: 'From candidate/:id'},
                        resolve: {response: FromCandidateResolverService}
                    },
                    {
                        path: 'detail/:id',
                        component: CreateNewEmployeeRequestComponent,
                        data: {breadcrumb: 'detail/:id'},
                        resolve: {response: DetailNewEmployeeRequestResolverService}
                    }
                ]
            },
            {
                path: 'reports',
                data: {breadcrumb: 'Reports'},
                children: [
                    {
                        path: 'edit/:id',
                        component: EmployeeManagementComponent,
                        data: {breadcrumb: 'edit/:id'},
                        resolve: {response: EditEmployeeResolverService}
                    },
                    {
                        path: '',
                        component: EmployeeTableComponent,
                        data: {breadcrumb: 'All'},
                        resolve: {response: EmployeeTableResolverService}
                    },
                ]
            },
        ]
    },
    {
        path: 'billing',
        component: BillingComponent,
        data: {breadcrumb: 'Billing'},
    },
    {
        path: 'admin-know-the-company',
        component: KnowTheCompanyManagementComponent,
        data: {breadcrumb: 'Admin know the company'}
    },
    {
        path: 'room-management',
        data: {breadcrumb: 'Room management'},
        children:[
            {
                path: 'room/:id',
                component: CalendarContainerComponent,
                data: {breadcrumb: 'room/:id',
                       type:'room'},
            }, {
                path: '',
                component: RoomListComponent,
                data: {breadcrumb: ''},
                resolve: {response: RoomListManagementResolverService},
            }
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
