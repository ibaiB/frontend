import {RoomAbstractService} from '../infrastructure/room-abstract-service';

export function create(room: any, service: RoomAbstractService) {
    return service.create(room);
}
