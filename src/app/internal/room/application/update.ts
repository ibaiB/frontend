import {RoomAbstractService} from '../infrastructure/room-abstract-service';

export function update(room: any, service: RoomAbstractService) {
    return service.update(room);
}
