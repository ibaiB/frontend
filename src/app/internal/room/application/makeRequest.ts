import {RoomAbstractService} from '../infrastructure/room-abstract-service';

export function makeRequest(idRoom: string, idPeriod: string, service: RoomAbstractService, query?: any) {
    return service.makeRequest(idRoom, idPeriod, query);
}
