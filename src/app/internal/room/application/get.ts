import {RoomAbstractService} from '../infrastructure/room-abstract-service';

export function get(idRoom: string, service: RoomAbstractService) {
    return service.get(idRoom);
}
