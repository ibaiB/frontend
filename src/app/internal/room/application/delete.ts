import {RoomAbstractService} from '../infrastructure/room-abstract-service';

export function remove(idRoom: string, service: RoomAbstractService) {
    return service.delete(idRoom);
}
