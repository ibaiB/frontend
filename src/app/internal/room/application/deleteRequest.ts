import {RoomAbstractService} from '../infrastructure/room-abstract-service';

export function deleteRequest(idRoom: string, idPeriod: string, service: RoomAbstractService, query?: any) {
    return service.deleteRequest(idRoom, idPeriod, query);
}
