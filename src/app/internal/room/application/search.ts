import {RoomAbstractService} from '../infrastructure/room-abstract-service';

export function search(query: any, service: RoomAbstractService) {
    return service.search(query);
}
