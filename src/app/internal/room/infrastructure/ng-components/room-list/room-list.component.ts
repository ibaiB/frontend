import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {IAuto} from '../../../../../shared/custom-mat-elements/dynamic';
import {RoomService} from '../../services/room.service';
import {search} from '../../../application/search';
import {IRoom} from '../../../domain/IRoom';
import {ActivatedRoute} from '@angular/router';
import {ICenter} from '../../../../../shared/entities/center/domain/ICenter';
import {CookieFacadeService} from '../../../../../auth/session/infrastructure/services/cookie.service';

@Component({
    selector: 'app-room-list',
    templateUrl: './room-list.component.html',
    styleUrls: ['./room-list.component.scss']
})
export class RoomListComponent implements OnInit {
    private _resolved: any;

    filters: IAuto[];
    rooms: IRoom[];
    newRoom: any;
    centers: ICenter[];
    queryPagination = {page: 0, size: 10};
    total: number = 0;
    totalPages: number = 0;
    editable: boolean;
    queryFilters;
    title: string;

    @ViewChild('cards') private scrollContainer: ElementRef;

    constructor(private _roomService: RoomService,
                private _route: ActivatedRoute,
                private _cookieFacade: CookieFacadeService) {
    }

    ngOnInit(): void {
        this._resolved = this._route.snapshot.data['response'];
        this.editable = !this._route.snapshot.data['readOnly'];
        this.rooms = this._resolved.data.rooms.content;
        this.centers = this._resolved.data.centers.content;
        this.queryFilters = this._resolved.data?.filtersValue ?? {};
        this.total = this._resolved.data.rooms.totalElements;
        this.totalPages = this._resolved.data.rooms.totalPages;
        this._buildFilters();
        this.configFilterValue();
        this.title = this.editable ? 'ROOM MANAGEMENT' : 'ROOMS';
    }

    configFilterValue() {
        let queryFiltersAux = this._resolved.data.filtersValue ? JSON.parse(this._resolved.data.filtersValue) : {};
        queryFiltersAux['address'] = queryFiltersAux['address'] ? this.centers.find(center => center.id === queryFiltersAux['address']).location : '';

        this.filters.forEach(filter => {
            filter.defaultValue = queryFiltersAux[this.getFilterProp(filter)];
            filter.value = queryFiltersAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    public backSearch() {
        search({...this.queryPagination, ...this.queryFilters}, this._roomService).subscribe(e => {
            this.rooms = e.content;
            this.total = e.totalElements;
            this.totalPages = e.totalPages;
        });
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const switched = this._querySwitcher(params);
            this.queryFilters = switched;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('roomManagementFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this.backSearch();
    }

    private _querySwitcher(params: any) {
        const center = this.centers.filter(center => center.location === params.address)[0];
        if (center) {
            params.address = center.id;
        }

        return params;
    }

    paginationChanged(event: string) {
        if (event === 'a') {
            if (this.queryPagination.page !== 0) {
                this.queryPagination.page--;
            }
        } else {
            if (this.queryPagination.page < this.totalPages) {
                this.queryPagination.page++;
            }
        }
        this.backSearch();
    }

    goToNewRoom() {
        this.newRoom = {
            newRoom: true
        };

        this.rooms.push(this.newRoom);
        this.scrollContainer.nativeElement.scrollTop = this.scrollContainer.nativeElement.scrollHeight;
    }

    deleteNewRoom() {
        this.rooms.splice(this.rooms.length - 1);
        this.scrollContainer.nativeElement.scrollTop = 0;
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'name',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Name',
                placeholder: 'Name',
                shown: true
            },
            {
                options: this.centers.map(c => c.location),
                prop: '',
                retProp: '',
                searchValue: 'address',
                type: 'autocomplete',
                appearance: 'outline',
                class: 'autocomplete',
                label: 'Address',
                placeholder: 'Address',
                shown: true
            },
            {
                prop: '',
                retProp: '',
                type: 'autocomplete',
                searchValue: 'roomType',
                label: 'Room Type',
                placeholder: 'Room Type',
                options: ['MEETING', 'EATING', 'LIVING'],
                shown: true,
                appearance: 'standard',
                class: 'autocomplete',
            },
        ]
    }
}
