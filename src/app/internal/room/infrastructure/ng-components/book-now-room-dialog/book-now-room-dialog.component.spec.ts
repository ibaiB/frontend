import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookNowRoomDialogComponent } from './book-now-room-dialog.component';

describe('BookNowRoomDialogComponent', () => {
  let component: BookNowRoomDialogComponent;
  let fixture: ComponentFixture<BookNowRoomDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookNowRoomDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookNowRoomDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
