import { COMMA, ENTER, SPACE } from '@angular/cdk/keycodes';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { searchPublicEmployees } from 'src/app/internal/employee/application/searchPublicEmployee';
import { EmployeeService } from 'src/app/internal/employee/infrastructure/services/employee.service';
import { IRoom } from '../../../domain/IRoom';
import { makeRequest } from '../../../application/makeRequest';
import { RoomService } from '../../services/room.service';
import { getMyEmployee } from 'src/app/internal/employee/application/getMyEmployee';
import { deleteRequest } from '../../../application/deleteRequest';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-book-now-room-dialog',
  templateUrl: './book-now-room-dialog.component.html',
  styleUrls: ['./book-now-room-dialog.component.scss']
})
export class BookNowRoomDialogComponent implements OnInit {

  form: FormGroup;
  room:IRoom;
  checkIn:any;
  checkOut:any;
  periods:any;
  allPeriods:any[];
  startDay:Date;
  endDay:Date;
  allUsers=[];
  users=[];
  periodOptions:string[];
  periodsOutOptions:string[];
  joinButton:string;
  me:any;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
  userCtrl = new FormControl();

  @ViewChild('userInput') userInput: ElementRef<HTMLInputElement>;
  
  constructor(private _fb: FormBuilder,
              private _employeeService: EmployeeService,
              private _roomService: RoomService,
              public dialogRef: MatDialogRef<BookNowRoomDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { 
  }

  ngOnInit(): void {
    this.room = this.data.father;
    this.periods=this.data.periods;
    this.startDay = new Date(this.periods[0].start);
    this.endDay = new Date(this.periods[this.periods.length-1].end);
    this.allPeriods = this.data.allPeriods.filter(p=> new Date(p.start).getDate()===this.startDay.getDate());
    this.checkIn = `${this.startDay.getHours()-2}:${this.startDay.getMinutes()}`;
    this.checkIn = this.startDay.getMinutes()===0 ? `${this.checkIn}0`: this.checkIn;
    this.checkOut = `${this.endDay.getHours()-2}:${this.endDay.getMinutes()}`;
    this.checkOut = this.endDay.getMinutes()===0 ? `${this.checkOut}0`: this.checkOut;
    this.users=this.periods[0].employees;
    this.joinButton = this.users.length===0 ? 'Booking' : 'Join';
    this.periodOptions=[ '8:00','8:30','9:00','9:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30',
                         '14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30'];
    this.periodsOutOptions = this.periodOptions.slice(this.periodOptions.indexOf(this.checkIn)+1);
    this.me=this.data.me;
    this.formInit();
  }

  formInit(){
    if(this.room){
      let defaultDay = this.startDay ?? new Date();
      let defaultCheckIn = this.checkIn ?? ``;
      let defaultCheckOut = this.checkOut ?? '';

      this.form = this._fb.group({
        title:'',
        day:[defaultDay],
        user:[],
        checkIn:[defaultCheckIn, Validators.required],
        checkOut:[defaultCheckOut, Validators.required],
        room:[this.room.name],
        description:[]
      })
    }
    this.changeOptions('');
    this.form.get('user').valueChanges.subscribe(userValue => this.changeOptions(userValue));
  } 

  removeUser(user: any): void {
    if(user.id!==this.me.id)return;
    /*this.data.periods.forEach(period => {
      deleteRequest(this.room.id,period.id,this._roomService).subscribe(r=>{
        console.log(r)
      })
    });*/
    for (let i of this.data.periods){
      deleteRequest(this.room.id,i.id,this._roomService).subscribe(r=>{
        console.log(r)
      })
      console.log(i)
    }
    this.dialogRef.close();
  }

  showJoinButton(){
    if(this.room.maxCapacity===this.users.length)return false;
    this.users.forEach(u=>{
      if(u.id===this.me.id)return false;
    })
  }

  addUser(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      if(this.allUsers.includes(value.trim())
          && !this.users.includes(value.trim())) {
        this.users.push(value.trim());
      }
    }
    if (input) input.value = '';
    this.userCtrl.setValue(null);
    this.changeOptions('');
  }

 userSelected(event: MatAutocompleteSelectedEvent) {
    this.addUser({
      input: this.userInput.nativeElement,
      value: event.option.viewValue
    });
  }

  changeOptions(event: any) {
     /* searchPublicEmployees({businessEmail: event}, this._employeeService).subscribe((users) => {
        this.allUsers=users.content.map(user => user.businessEmail);
      });*/
  }

  autocompleteSelected(event: { formField: string, value: string }) {
    this.form.controls[event.formField].setValue(event.value);
    this.checkOut='111';
    if(event.formField==='checkIn' && event.value){
      this.startDay.setHours(parseInt(event.value.split(':')[0])+2);
      this.startDay.setMinutes(parseInt(event.value.split(':')[1]));
      this.periodsOutOptions = this.periodOptions.slice(this.periodOptions.indexOf(event.value)+1);
    }

    if(event.formField==='checkOut' && event.value){
      this.endDay.setHours(parseInt(event.value.split(':')[0])+2);
      this.endDay.setMinutes(parseInt(event.value.split(':')[1]));
    }

    let start = this.allPeriods.find(p => new Date(p.start).toISOString()===this.startDay.toISOString());
    let end = this.allPeriods.find(p => new Date(p.end).toISOString()===this.endDay.toISOString());
    this.periods=this.allPeriods.slice(this.allPeriods.indexOf(start),this.allPeriods.indexOf(end)+1);

  }

  onBookingClick(){
    console.log('1',this.periods)
    if(this.form.valid){
      this.periods.forEach(p => {
        makeRequest(this.room.id,p.id,this._roomService).subscribe(r=> {
          console.log(r)
             console.log('2',p.id)
         });
       });
      /* Swal.fire({
        icon: 'success',
        title: 'Success',
        text: 'You have book this room!',
        confirmButtonColor: '#db5e5e'
      })*/
       this.dialogRef.close()
    }
  }

  onCancelClick() {
    this.dialogRef.close();
  }
}
