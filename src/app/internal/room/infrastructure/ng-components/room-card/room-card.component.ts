import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {uploadFile} from 'src/app/shared/file/application/upload';
import {FileUploadQuery, imageQuery} from 'src/app/shared/file/domain/FileUploadQuery';
import {FileService} from 'src/app/shared/file/infrastructure/services/file-service.service';
import {ICenter} from '../../../../../internal/center/domain/ICenter';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {create} from '../../../application/create';
import {update} from '../../../application/update';
import {RoomService} from '../../services/room.service';

@Component({
    selector: 'app-room-card',
    templateUrl: './room-card.component.html',
    styleUrls: ['./room-card.component.scss']
})
export class RoomCardComponent implements OnInit {

    @Input() room: any;
    @Input() editable: boolean;
    @Input() centers: ICenter[];

    @Output() closeNewRoom = new EventEmitter<any>();

    maxLength = 70;
    editRoom;
    defaultImagen: string;
    newPhoto: File;
    icon: string;
    form: FormGroup;
    centersOptions: string[];

    fileUploadQuery: FileUploadQuery = imageQuery;

    constructor(private _roomService: RoomService,
                private fb: FormBuilder,
                public _fileService: FileService,
                private _router: Router,
                private _route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.editRoom = this.room.newRoom;
        this.defaultImagen = this.room.roomType === 'MEETING' ? '../../../assets/icons/room-1.svg' : (this.room.roomType === 'EATING' ? '../../../assets/icons/room-2.svg'
            : '../../../assets/icons/room-3.svg');
        this.icon = this.room.roomType === 'MEETING' ? 'meeting_room' : (this.room.roomType === 'EATING' ? 'dining' : 'living');
        this.centersOptions = this.centers.map(c => c.location);
        this.formInit();
    }

    formInit() {
        this.form = this.fb.group({
            name: [this.room.name ?? ''],
            address: [this.room.address ?? ''],
            description: [this.room.description ?? ''],
            roomType: [this.room.roomType ?? '', Validators.required],
            maxCapacity: [this.room.maxCapacity ?? ''],
        });
    }

    autocompleteSelected(event: { formField: string, value: string }) {
        this.form.controls[event.formField].setValue(event.value);
    }

    photoChanged($event) {
        this.newPhoto = $event;
        this.fileUploadQuery.uploadName = $event.name;
    }

    onSubmit() {
        if (this.newPhoto) {
            this.updatePhotoAndRoom();
        } else {
            this.saveRoom();
        }
    }

    saveRoom() {
        if (this.form.valid) {
            this.editRoom = !this.editRoom;
            this.room.name = this.form.get('name').value;
            this.room.description = this.form.get('description').value;
            this.room.address = this.form.get('address').value;
            this.room.roomType = this.form.get('roomType').value;
            this.room.maxCapacity = this.form.get('maxCapacity').value;

            if (this.room.newRoom) {
                create(this.room, this._roomService).subscribe((e) => {
                    this.room = e;
                    this.defaultImagen = this.room.roomType === 'MEETING' ? '../../../assets/icons/room-1.svg' : (this.room.roomType === 'EATING' ? '../../../assets/icons/room-2.svg'
                        : '../../../assets/icons/room-3.svg');
                    this.icon = this.room.roomType === 'MEETING' ? 'meeting_room' : (this.room.roomType === 'EATING' ? 'dining' : 'living');
                    successAlert('Room where created');
                });
            } else {
                update(this.room, this._roomService).subscribe((e) => {
                    this.room = e;
                    successAlert('Changes where saved');
                });
            }
        }
    }

    updatePhotoAndRoom() {
        uploadFile(this.newPhoto, this.fileUploadQuery, this._fileService).subscribe((retFile) => {
            this.room.idLinkImagen = retFile.id;
            this.room.linkImagen = retFile.downloadLink;
            this.saveRoom();
        });
    }

    hasValue(formFilter: string): boolean {
        return this.form.controls[formFilter].value;
    }

    clear(entry: { formFilter: string }) {
        this.form.controls[entry.formFilter].setValue('');
    }

    closeRoom() {
        this.editRoom = !this.editRoom;
        if (this.room.newRoom) {
            this.closeNewRoom.emit();
        }
    }

    goToCalendar() {
        this._router.navigate([`./${this.room.id}`], {relativeTo: this._route});
    }
}
