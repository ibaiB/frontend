import {Component, OnInit} from '@angular/core';
import {IRoom} from '../../../domain/IRoom';
import {ActivatedRoute} from '@angular/router';
import {config} from './IConfig';
import {ICalendarConfig} from '../../../../../shared/calendar/domain/ICalendarConfig';

@Component({
    selector: 'app-room-detail',
    templateUrl: './room-detail.component.html',
    styleUrls: ['./room-detail.component.scss']
})
export class RoomDetailComponent implements OnInit {

    defaultImgUrl: string;
    room: IRoom;
    calendarConfig: ICalendarConfig = config;

    constructor(private _route: ActivatedRoute) {
    }

    ngOnInit() {
        this.room = this._route.snapshot.data['response'];
        this.setDefaultImgUrl();
    }

    setDefaultImgUrl() {
        if (this.room.roomType === 'MEETING') {
            return this.defaultImgUrl = '../../../assets/icons/room-1.svg';
        }
        if (this.room.roomType === 'EATING') {
            return this.defaultImgUrl = '../../../assets/icons/room-2.svg';
        }
        return this.defaultImgUrl = '../../../assets/icons/room-3.svg';
    }
}
