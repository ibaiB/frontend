import {ICalendarConfig} from '../../../../../shared/calendar/domain/ICalendarConfig';
import {BookNowRoomDialogComponent} from '../book-now-room-dialog/book-now-room-dialog.component';

export const config: ICalendarConfig = {
    daily: {
        shown: false,
        dialog: BookNowRoomDialogComponent
    },
    weekly: {
        shown: true,
        dialog: BookNowRoomDialogComponent
    },
    monthly: {
        shown: false,
        dialog: BookNowRoomDialogComponent
    }
};
