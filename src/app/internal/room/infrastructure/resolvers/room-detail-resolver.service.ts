import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IRoom} from '../../domain/IRoom';
import {Observable} from 'rxjs';
import {get} from '../../application/get';
import {RoomService} from '../services/room.service';

@Injectable({
  providedIn: 'root'
})
export class RoomDetailResolverService implements Resolve<IRoom>{

  constructor(private _roomService: RoomService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRoom> {
    return get(route.params['id'], this._roomService);
  }
}
