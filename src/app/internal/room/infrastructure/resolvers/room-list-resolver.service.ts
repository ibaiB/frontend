import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { forkJoin, Observable, of } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { getCenters } from "../../../../shared/entities/center/application/getCenters";
import { CookieFacadeService } from "../../../../auth/session/infrastructure/services/cookie.service";
import { ResolvedData } from "../../../../shared/generics/IResolvedData";
import { search } from "../../application/search";
import { RoomService } from "../services/room.service";
import { CenterService } from "../../../../shared/entities/center/infrastructure/service/center.service";

@Injectable({
    providedIn: 'any'
})

export class RoomListManagementResolverService implements Resolve<ResolvedData<any>> {

    constructor(
        private _roomService: RoomService,
        private _cookieService : CookieFacadeService,
        private _centerService : CenterService) {
    }


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const filtersCacheRoom = this._cookieService.get('roomManagementFilterCache');

        const $rooms = search({...JSON.parse(filtersCacheRoom),page:0,size:11, style:'simple'}, this._roomService);
        const $centers = getCenters({page:0,size:100}, this._centerService); 

        return forkJoin($rooms, $centers)
            .pipe(map(response => ({
                data: {
                    rooms:response[0],
                    centers:response[1],
                    filtersValue: filtersCacheRoom,
                }
            })),
                catchError(error => {
                    return of({
                        data: null,
                        message: 'Error on room management resolver, data couldn\' be fetched',
                        error
                    });
                }));
    }
}