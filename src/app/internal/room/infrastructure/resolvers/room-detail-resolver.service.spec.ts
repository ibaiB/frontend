import { TestBed } from '@angular/core/testing';

import { RoomDetailResolverService } from './room-detail-resolver.service';

describe('RoomDetailResolverService', () => {
  let service: RoomDetailResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoomDetailResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
