import {Injectable} from '@angular/core';
import {RoomAbstractService} from '../room-abstract-service';
import {IRoom} from '../../domain/IRoom';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {IList} from '../../../../shared/generics/IList';

@Injectable({
  providedIn: 'root'
})
export class RoomService extends RoomAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'rooms';

  constructor(private _http: HttpClient) {
    super();
  }

  create(room: IRoom): Observable<IRoom> {
    return this._http.post<IRoom>(`${this.BASE_URL}${this.ENDPOINT}`, room);
  }

  delete(id: string): Observable<any> {
    return this._http.delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  deleteRequest(idRoom: string, idPeriod: string, query?: any): Observable<boolean> {
    const base = `${this.BASE_URL}${this.ENDPOINT}/deleteRequest/${idRoom}/${idPeriod}`;
    const route = query?.me ? base : base + '/me';
    return this._http.delete<boolean>(route);
  }

  get(id: string): Observable<IRoom> {
    return this._http.get<IRoom>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  makeRequest(idRoom: string, idPeriod: string, query?: any): Observable<boolean> {
    const base = `${this.BASE_URL}${this.ENDPOINT}/request/${idRoom}/${idPeriod}`;
    const route = query?.me ? base : base + '/me';
    return this._http.put<boolean>(route, {});
  }

  search(query: any): Observable<IList<IRoom>> {
    return this._http.get<IList<IRoom>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
  }

  update(room: IRoom): Observable<IRoom> {
    return this._http.put<IRoom>(`${this.BASE_URL}${this.ENDPOINT}/${room.id}`, room);
  }
}
