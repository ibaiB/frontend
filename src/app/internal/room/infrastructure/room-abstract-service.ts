import {IRoom} from '../domain/IRoom';
import {Observable} from 'rxjs';
import {IList} from '../../../shared/generics/IList';

export abstract class RoomAbstractService {
    abstract create(room: IRoom): Observable<IRoom>;

    abstract update(room: IRoom): Observable<IRoom>;

    abstract get(id: string): Observable<IRoom>;

    abstract delete(id: string): Observable<any>;

    abstract deleteRequest(idRoom: string, idPeriod: string, query?: any): Observable<boolean>;

    abstract makeRequest(idRoom: string, idPeriod: string, query?: any): Observable<boolean>;

    abstract search(query: any): Observable<IList<IRoom>>;
}
