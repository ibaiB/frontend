export interface IRoom {
    id?: string
    address: string,
    description: string,
    linkImagen: string,
    idLinkImagen?:string,
    maxCapacity: number,
    name: string,
    roomType: string
}
