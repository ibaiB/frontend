import {ICompany} from './ICompany';

export interface ICompanyList{
    content: ICompany[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}
