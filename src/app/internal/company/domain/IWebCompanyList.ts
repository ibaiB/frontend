import {IWebCompany} from './IWebCompany';

export interface IWebCompanyList {
    content: IWebCompany[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}
