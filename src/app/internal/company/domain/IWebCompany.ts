import {IMaterial} from '../../material/domain/IMaterial';

export interface IWebCompany {
    employees: any[],
    id: string,
    materials: IMaterial[],
    name: string,
}
