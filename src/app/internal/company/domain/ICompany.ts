import {IPublicEmployee} from '../../employee/domain/IEmployee';
import {IMaterial} from 'src/app/internal/material/domain/IMaterial';

export interface ISimpleCompany {
    id: string,
    name: string,
    idFileManager: string;
}

export interface ICompany extends ISimpleCompany {
    employees: IPublicEmployee[],
    materials: IMaterial[],
}
