import {CompanyAbstractService} from '../infrastructure/CompanyAbstractService';

export function getSingleCompany(id: string, params: any, service: CompanyAbstractService){
    return service.findById(id, params);
}
