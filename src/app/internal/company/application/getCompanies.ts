import {CompanyAbstractService} from '../infrastructure/CompanyAbstractService';

export function getCompanies(params: any, service: CompanyAbstractService){
    return service.search(params)
}
