import {Component, OnInit} from '@angular/core';
import {ICompany} from '../../../domain/ICompany';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-admin-company',
  templateUrl: './admin-company.component.html',
  styleUrls: ['./admin-company.component.scss']
})
export class AdminCompanyComponent implements OnInit {

  company: ICompany;
  readonly: boolean;

  constructor(private _route: ActivatedRoute) {
  }

  ngOnInit() {
    this.company = this._route.snapshot.data['response']['company'];
    this.readonly = this._route.snapshot.data['readOnly'];
  }

}
