import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {CompanyAbstractService} from '../CompanyAbstractService';
import {IWebCompanyList} from '../../domain/IWebCompanyList';
import {IWebCompany} from '../../domain/IWebCompany';

@Injectable({
  providedIn: 'root'
})

export class CompanyService extends CompanyAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'companies';

  constructor(private http: HttpClient) {
    super();
  }

  findById(id: string): Observable<IWebCompany> {
    return this.http.get<IWebCompany>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  search(query: any): Observable<IWebCompanyList> {
    return this.http.get<IWebCompanyList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
  }

}
