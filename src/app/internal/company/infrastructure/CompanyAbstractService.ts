import {Observable} from 'rxjs';
import {IWebCompany} from '../domain/IWebCompany';
import {IWebCompanyList} from '../domain/IWebCompanyList';

export abstract class CompanyAbstractService {
    abstract findById(id: string, params: any): Observable<IWebCompany>;

    abstract search(query: any): Observable<IWebCompanyList>;
}
