import {ICompany, ISimpleCompany} from '../domain/ICompany';

export class CompanyMapper {
    mapTo(params: any): ICompany {
        const {
            employees,
            id,
            materials,
            name,
            idFileManager
        } = params;

        return {
            employees: employees,
            id: id,
            materials: materials,
            name: name,
            idFileManager
        };
    }

    mapSimple(params: any): ISimpleCompany {
        const {
            id,
            name,
            idFileManager
        } = params;

        return {
            id: id,
            name: name,
            idFileManager
        };
    }
}
