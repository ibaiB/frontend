import {TestBed} from '@angular/core/testing';

import {AdminCompanyResolverService} from './admin-company-resolver.service';

describe('AdminCompanyResolverService', () => {
  let service: AdminCompanyResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminCompanyResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
