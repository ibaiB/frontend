import {Injectable} from '@angular/core';
import {CompanyService} from '../services/company.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {getCompanies} from '../../application/getCompanies';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminCompanyResolverService implements Resolve<any> {

  constructor(private _service: CompanyService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return getCompanies({name: 'Bosonit'}, this._service)
        .pipe(map(res => ({
          company: res.content[0]
        })));
  }
}
