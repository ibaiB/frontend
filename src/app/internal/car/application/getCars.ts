import {CarAbstractService} from '../infrastructure/CarAbstractService';

export function getCars(params: any, service: CarAbstractService){
    return service.search(params);
}
