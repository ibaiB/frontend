import {CarAbstractService} from '../infrastructure/CarAbstractService';

export function getSingleCar(id: string, service: CarAbstractService){
    return service.findById(id);
}
