import {CarMapper} from './CarMapper';

export class CarListMapper{
    mapTo(params: any) {
        const carMapper = new CarMapper();
        const {
            content,
            numberOfElements,
            totalElements,
            totalPages,
        } = params;

        return {
            content: content.map(carMapper.mapTo),
            numberOfElements: numberOfElements,
            totalElements: totalElements,
            totalPages: totalPages,
        }
    }
}
