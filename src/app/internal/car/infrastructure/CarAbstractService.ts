import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';
import {IWebCar} from '../domain/IWebCar';

export abstract class CarAbstractService {
    abstract findById(id: string): Observable<IWebCar>;

    abstract search(query: any): Observable<IList<IWebCar>>;
}
