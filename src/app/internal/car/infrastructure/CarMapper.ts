export class CarMapper{
    mapTo(params: any) {
        const {
            description,
            employees,
            id,
            name,
            subCars,
        } = params;

        return {
            description: description,
            employees: employees,
            id: id,
            name: name,
            subCars: subCars,
        }
    }
}