import {Injectable} from '@angular/core';
import {CarAbstractService} from '../CarAbstractService';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {IWebCar} from '../../domain/IWebCar';
import {IList} from '../../../../shared/generics/IList';

@Injectable({
  providedIn: 'root'
})

export class CarService extends CarAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'cars';

  constructor(private http: HttpClient) {
    super();
  }

  findById(id: string): Observable<IWebCar> {
    return this.http.get<IWebCar>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  search(query: any): Observable<IList<IWebCar>> {
    return this.http.get<IList<IWebCar>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
  }

}
