import {IWebCar} from './IWebCar';

export interface IWebCarList {
    content: IWebCar[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}
