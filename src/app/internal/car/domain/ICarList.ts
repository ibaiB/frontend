import {ICar} from './ICar';

export interface ICarList{
    content: ICar[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}
