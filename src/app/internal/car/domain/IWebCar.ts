import {ISubcar} from '../../subcar/domain/ISubcar';

export interface IWebCar {
    description: string,
    employees: any[],
    id: string,
    name: string,
    subCars: ISubcar[],
}
