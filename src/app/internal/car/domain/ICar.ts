import {IEmployeeRead} from '../../employee/domain/IEmployee';
import {ISubcar} from '../../subcar/domain/ISubcar';

export interface ICar {
    description: string,
    employees: IEmployeeRead[],
    id: string,
    name: string,
    subCars: ISubcar[],
}
