import {ITechnology} from '../../technology/domain/ITechnology';
import {ICategory} from '../../category/domain/ICategory';
import {ISimpleCompany} from '../../company/domain/ICompany';
import {ICar} from '../../car/domain/ICar';
import {ISubcar} from '../../subcar/domain/ISubcar';
import {IDocument} from '../../document/domain/IDocument';
import {ICostPerMonth} from './ICostPerMonth';
import {IEmployeeTermination} from '../../employee-termination/domain/IEmployeeTermination';
import {ICenter} from '../../../shared/entities/center/domain/ICenter';
import {IWorkingDay} from '../../working-day/domain/IWorkingDay';
import {IMaterial} from '../../material/domain/IMaterial';
import {IEmployeeAuxRead} from '../../employee-aux/domain/IEmployeeAux';
import {INewEmployeeRequest} from '../../new-employee-request/domain/INewEmployeeRequest';

interface ICommon {
    id?: string;
    idChat: string;
    idFileManager: string;
    idPrivateFileManager: string;

    // ===== BILLING =====
    startingMonth: string;
    studiesTotalCost: number;
    internshipMonths: number;
    expandableMonths: number;
    costsPerMonth: ICostPerMonth[];

    // ===== PERSONAL =====
    personalEmail: string;
    personalPhoneNumber: string;
    landLinePhoneNumber: string;
    postalCodeDel: string;
    postalCodeHab: string;
    provinceDel: string;
    provinceHab: string;
    addressDel: string;
    addressHab: string;
    cityDel: string;
    cityHab: string;
    countryDel: string;
    countryHab: string;
    englishLevel: string;

    // ===== BUSINESS =====
    studyYear: string;
    businessPhoneNumber: string;
    cityOfPreference: string;
    availableToHire: boolean;
    generalMark: number;
    hosting: boolean;
    hostingCity: string;
    recommendedItinerary: string;
    profileType: string;

    // ===== DATES =====
    studiesEndDate: string;
    previsionEndDate: string;
    internshipEndDate: string;

    // ===== HISTORIC =====
    idCandidate: string;
    idNewEmployeeRequest: string;
}

export interface IEmployeeWrite extends ICommon{

    // ===== BUSINESS =====
    idCar: string;
    idCenter: string;
    idSubcar: string;
    idCategory: string;

    // ===== HIERARCHY =====
    idEmployeeCoach: string;
    idEmployeeManager: string;
    idEmployeeMentor: string;

    // ===== MISC =====
    idImageLink: string;
    idTechnologies: string[];
}

export interface IEmployeeRead extends ICommon {
    dni: string;
    name: string;
    surname: string;
    imageLink: string;

    // ===== BUSINESS
    idNfq: string;
    socialSecurityNumber: string;
    geographicAvailability: boolean;
    contractType: string;
    internshipType: string;
    bankAccountNumber: string;
    fixedRemuneration: number;
    variableRemuneration: number;
    status: boolean;
    comercialFlag: boolean;
    businessEmail: string;
    businessPhone: string;

    // ===== HIERARCHY =====
    isCoach: boolean;
    isManager: boolean;
    isMentor: boolean;

    // ===== DATES =====
    startDate: string;
    endDate: string;
    testEndDate: string;
}

export interface IEmployeeReadFull extends IEmployeeRead {
    employeeTermination?: IEmployeeTermination;
    employeeAux: IEmployeeAuxRead;
    category: ICategory;
    center: ICenter;
    workingDay: IWorkingDay;
    materials: IMaterial[];
    technologies: ITechnology[];
    documents: IDocument[];
    company: ISimpleCompany;
    car: ICar;
    subcar: ISubcar;
    newEmployeeRequest: INewEmployeeRequest
}

export interface IEmployeeReadRelated extends IEmployeeReadFull{
    coachOf: IEmployeeReadFull[];
    managerOf: IEmployeeReadFull[];
    mentorOf: IEmployeeReadFull[];
    coach: IPublicEmployee;
    mentor: IPublicEmployee;
    manager: IPublicEmployee;
}

export interface IPublicEmployee {
    id?: string;
    name: string;
    surname: string;
    categoryName: string;
    businessEmail: string;
    businessPhone: string;
    imageLink: string;
    status: boolean;
    startDate: string;
    technologies: ITechnology[];
    center: ICenter;
    linkedin: string;

}

export interface IEmployeeWhoIsWho {
    randomNames: string[];
    employee: IPublicEmployee
}
