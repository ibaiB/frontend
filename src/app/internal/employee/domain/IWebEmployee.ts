import {IWebCostPerMonth} from './IWebCostPerMonth';
import {IWebCategory} from '../../category/domain/IWebCategory';
import {IWebCandidate} from '../../../recruitment/candidate/domain/IWebCandidate';
import {IWebNewEmployeeRequest} from '../../new-employee-request/domain/IWebNewEmployeeRequest';

export interface IWebEmployeeWrite {
    addressDel: string;
    addressHab: string;
    annioEstudios: string;
    businessPhoneNumber: string;
    cityDel: string;
    cityHab: string;
    cityOfPreference: string;
    contratable: boolean;
    costeTotalEstudios: number;
    costesPorMes: IWebCostPerMonth[];
    countryDel: string;
    countryHab: string;
    englishLevel: string;
    generalMark: number;
    hosting: boolean;
    hostingCity: string;
    idCandidate: string;
    idCar: string;
    idCategory: string;
    idCenter: string;
    idEmployeeCoach: string;
    idEmployeeManager: string;
    idEmployeeMentor: string;
    idLinkImagen: string;
    idSubcar: string;
    idTechnologies: string[];
    internshipEndDate: string;
    mesInicio: string;
    numeroMesesAmpliables: number;
    numeroMesesDePracticas: number;
    personalEmail: string;
    personalPhoneNumber: string;
    personalPhoneNumberFijo: string;
    postalCodeDel: string;
    postalCodeHab: string;
    previsionEndDate: string;
    profileType: string;
    provinceDel: string;
    provinceHab: string;
    recommendedItinerary: string;
    studiesEndDate: string;
    car: any;
    subcar: any;
    company: any;
}

export interface IWebEmployeeSimple extends IWebEmployeeWrite{
    id?: string;
    idNfq: string;
    name: string;
    surname: string;
    dni: string;
    businessEmail: string;
    socialSecurityNumber: string;
    geographicAvailability: boolean;
    contractType: string;
    internshipType: string;
    bankAccountNumber: string;
    linkImagen: string;
    coachOfCount: number;
    mentorOfCount: number;
    managerOfCount: number;
    fixedRemuneration: string;
    variableRemuneration: string;
    status: boolean;
    comercialFlag: boolean;
    isCoach: boolean;
    isMentor: boolean;
    isManager: boolean;
    startDate: string;
    endDate: string;
    idDefaultProject: string;
    idCompany: string;
}

export interface IWebEmployeeFull extends IWebEmployeeSimple {
    employeeTermination: any;
    category: IWebCategory;
    center: any;
    workingDay: any;
    employeeAux: any;
    candidate: IWebCandidate;
    newEmployeeRequest: IWebNewEmployeeRequest;
    materials: any[];
    technologies: any[];
    documentos: any[]
}

export interface IWebEmployeeRelated extends IWebEmployeeFull {
    coach: string;
    mentor: string;
    manager: string;
    coachOf: any[];
    managerOf: any[];
    mentorOf: any[]
}
