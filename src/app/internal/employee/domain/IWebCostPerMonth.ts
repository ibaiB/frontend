export interface IWebCostPerMonth {
    id?: string;
    fecha: string;
    coste: number;
}
