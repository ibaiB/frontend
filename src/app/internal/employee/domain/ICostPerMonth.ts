export interface ICostPerMonth{
    id?: string;
    fecha: string;
    coste: number;
}
