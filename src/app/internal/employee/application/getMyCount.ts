import {EmployeeAbstractService} from '../infrastructure/EmployeeAbstractService';

export function getMyCount(service: EmployeeAbstractService) {
 return service.getMyCount();
}
