import {EmployeeAbstractService} from '../infrastructure/EmployeeAbstractService';
import {EmployeeMapper} from '../infrastructure/EmployeeMapper';
import {map} from 'rxjs/operators';

export function searchPublicEmployees(query: any, service: EmployeeAbstractService){
    const mapper = new EmployeeMapper();
    return service.searchPublic(query).pipe(map(mapper.mapPublicList));
}
