import {EmployeeAbstractService} from '../infrastructure/EmployeeAbstractService';
import {EmployeeMapper} from '../infrastructure/EmployeeMapper';
import {map} from 'rxjs/operators';

export function getEmployee(id: string, service: EmployeeAbstractService, query?: any) {
    const mapper = new EmployeeMapper();
    return service.get(id, query).pipe(map(mapper.mapTo));
}
