import {EmployeeAbstractService} from '../infrastructure/EmployeeAbstractService';
import {EmployeeMapper} from '../infrastructure/EmployeeMapper';
import {map} from 'rxjs/operators';

export function getMyEmployee(service: EmployeeAbstractService, query?: any){
    const mapper = new EmployeeMapper();
    return service.me(query).pipe(map(mapper.mapTo));
}
