import {EmployeeAbstractService} from '../infrastructure/EmployeeAbstractService';
import {EmployeeMapper} from '../infrastructure/EmployeeMapper';
import {map} from 'rxjs/operators';

export function updateEmployee(employee: any, service: EmployeeAbstractService, query?: any) {
    const mapper = new EmployeeMapper();
    return service.update(mapper.mapFrom(employee)).pipe(map(mapper.mapTo));
}
