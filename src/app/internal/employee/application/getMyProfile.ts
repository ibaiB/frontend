import {EmployeeAbstractService} from '../infrastructure/EmployeeAbstractService';

export function getMyProfile(service: EmployeeAbstractService, index?: number) {
    return service.getMyProfile(index);
}
