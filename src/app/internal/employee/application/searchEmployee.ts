import {EmployeeAbstractService} from '../infrastructure/EmployeeAbstractService';
import {EmployeeMapper} from '../infrastructure/EmployeeMapper';
import {map} from 'rxjs/operators';

export function searchEmployee(query: any, service: EmployeeAbstractService){
    const mapper = new EmployeeMapper();
    return service.search(query).pipe(map(mapper.mapList));
}
