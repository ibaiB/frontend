import {IMyProfile} from '../../../../my-staffit/myProfile/domain/IMyProfile';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {EmployeeAbstractService} from '../EmployeeAbstractService';
import {IPublicEmployee} from '../../domain/IEmployee';
import {IList} from '../../../../shared/generics/IList';
import {IWebEmployeeFull, IWebEmployeeRelated, IWebEmployeeSimple} from '../../domain/IWebEmployee';
import {IEmployeeTermination} from '../../../employee-termination/domain/IEmployeeTermination';

@Injectable({
    providedIn: 'root'
})

export class EmployeeService extends EmployeeAbstractService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'employees';

    constructor(private http: HttpClient) {
        super();
    }

    update(employee: any, query?: any): Observable<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated> {
        return this.http
            .put<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>(`${this.BASE_URL}${this.ENDPOINT}/${employee.id}`, employee, {params: new HttpParams({fromObject: query})});
    }

    terminate(terminate: IEmployeeTermination, id: string): Observable<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated> {
        return this.http
            .post<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>(`${this.BASE_URL}${this.ENDPOINT}/${id}/termination`, terminate);
    }

    me(query?: any): Observable<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated> {
        return this.http
            .get<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>(`${this.BASE_URL}${this.ENDPOINT}/me`, {params: new HttpParams({fromObject: query})});
    }

    search(query: any): Observable<IList<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>> {
        return this.http
            .get<IList<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
    }

    searchPublic(query: any): Observable<IList<IPublicEmployee>> {
        return this.http
            .get<IList<IPublicEmployee>>(`${this.BASE_URL}${this.ENDPOINT}/search/public`, {params: new HttpParams({fromObject: query})});
    }

    get(id: string, query?: any): Observable<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated> {
        return this.http
            .get<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>(`${this.BASE_URL}${this.ENDPOINT}/${id}`, {params: new HttpParams({fromObject: query})});
    }

    getMyProfile(query?: any): Observable<IMyProfile> {
        return this.http.get<IMyProfile>(`${this.BASE_URL}myProfile/me`, {params: new HttpParams({fromObject: query})});
    }

    updateMyProfile(myProfile, query?: any): Observable<IMyProfile> {
        return this.http.post<IMyProfile>(`${this.BASE_URL}myProfile/me`, myProfile, {params: new HttpParams({fromObject: query})});
    }

    getMyCount(): Observable<any> {
        return this.http.get<any>(`${this.BASE_URL}${this.ENDPOINT}/me/count`);
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }
}
