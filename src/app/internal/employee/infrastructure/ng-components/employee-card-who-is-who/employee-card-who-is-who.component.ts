import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IEmployeeWhoIsWho} from '../../../domain/IEmployee';

@Component({
    selector: 'app-employee-card-who-is-who',
    templateUrl: './employee-card-who-is-who.component.html',
    styleUrls: ['./employee-card-who-is-who.component.scss']
})
export class EmployeeCardWhoIsWhoComponent implements OnInit {

    @Input() employee: IEmployeeWhoIsWho;
    @Input() isCorrect: boolean;
    @Input() hasAnswered: boolean;
    @Input() index: number;
    @Output() correct = new EventEmitter<{
        idEmployee: string,
        value: string,
        index: number
    }>();

    photoLink: string;

    constructor() {
    }

    ngOnInit(): void {
        if (this.employee.employee.imageLink.includes('https://drive.google.com/')) {
            this.employee.employee.imageLink = null;
        }
    }

    tryLuck(option: string) {
        this.correct.emit({
            idEmployee: this.employee.employee.id,
            value: option,
            index: this.index
        });
    }
}
