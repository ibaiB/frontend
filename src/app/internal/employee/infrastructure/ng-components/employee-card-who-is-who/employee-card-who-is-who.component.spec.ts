import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EmployeeCardWhoIsWhoComponent} from './employee-card-who-is-who.component';

describe('EmployeeCardWhoIsWhoComponent', () => {
  let component: EmployeeCardWhoIsWhoComponent;
  let fixture: ComponentFixture<EmployeeCardWhoIsWhoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeCardWhoIsWhoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeCardWhoIsWhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
