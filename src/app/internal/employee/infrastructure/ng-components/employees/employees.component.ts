import {Component, OnInit} from '@angular/core';
import {IAuto} from 'src/app/shared/custom-mat-elements/dynamic';
import {searchPublicEmployees} from 'src/app/internal/employee/application/searchPublicEmployee';
import {IPublicEmployee} from 'src/app/internal/employee/domain/IEmployee';
import {EmployeeService} from 'src/app/internal/employee/infrastructure/services/employee.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-employees',
    templateUrl: './employees.component.html',
    styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

    employee: IPublicEmployee[];
    filters: IAuto[];
    queryPagination = {page: 0, size: 12};
    total: number = 0;
    totalPages: number = 0;

    constructor(private _employeeService: EmployeeService) {
    }

    ngOnInit(): void {
        this.backSearch(this.queryPagination);
        this._buildFilters();
    }

    public backSearch(query: any) {
        searchPublicEmployees({...this.queryPagination, ...query}, this._employeeService)
            .subscribe(e => {
                this.employee = e.content;
                this.total = e.totalElements;
                this.totalPages = e.totalPages - 1;
            }, error => {
                console.error('Error on back search', error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    confirmButtonColor: '#db5e5e'
                });
            });
    }

    paginationChanged(event: string) {
        if (event === 'a') {
            if (this.queryPagination.page !== 0) {
                this.queryPagination.page--;
            }
        } else {
            if (this.queryPagination.page < this.totalPages) {
                this.queryPagination.page++;
            }
        }
        searchPublicEmployees(this.queryPagination, this._employeeService)
            .subscribe(e => {
                this.employee = e.content;
            }, error => {
                console.error('Error on back search', error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    confirmButtonColor: '#db5e5e'
                });
            });
    }

  private _buildFilters(){
    this.filters = [
      {
        options: [],
        prop: 'name',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Name',
        placeholder: 'Name',
        shown: true
      },
      {
        options: [],
        prop: 'surname',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Surname',
        placeholder: 'Surname',
        shown: true
      },
      {
        options: [],
        prop: 'businessEmail',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Business Email',
        placeholder: 'Business Email',
        shown: true
      },
      {
        options: [],
        prop: 'businessPhone',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Business Phone',
        placeholder: 'Business Phone',
        shown: true
      },
      {
        options: [],
        prop: 'categoryName',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Category Name',
        placeholder: 'Category Name',
        shown: true
      }   
    ]
  }

}
