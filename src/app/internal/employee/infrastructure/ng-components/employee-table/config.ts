import {IAuto, ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
    multiSelect: false,
    columns: [
        {
            name: 'Id',
            prop: 'id',
            shown: true,
            route: './app/internal/employee/reports/edit',
            cellClass: 'id-table-column',
            routeId: 'id'
        },
        {
            name: 'Name',
            prop: 'name',
            shown: true,
        },
        {
            name: 'Surname',
            prop: 'surname',
            shown: true,
        },
        {
            name: 'Company',
            prop: 'companyName',
            shown: true,
        },
        {
            name: 'City Hab',
            prop: 'cityHab',
            shown: false,
        },
        {
            name: 'Category',
            prop: 'categoryName',
            shown: true,
        },
        {
            name: 'Start Date',
            prop: 'startDate',
            shown: true,
            type: 'date'
        },
        {
            name: 'Status',
            prop: 'statusString',
            shown: true,
        },
        {
            name: 'Car',
            prop: 'carName',
            shown: true,
        }
    ]
};

export const filters: IAuto[] = [
    {
        options: [],
        prop: 'id',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Id',
        placeholder: 'Id',
        shown: true,
    },
    {
        options: [],
        prop: 'name',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Name',
        placeholder: 'Name',
        shown: true,
    },
    {
        options: [],
        prop: 'surname',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Surname',
        placeholder: 'Surname',
        shown: true,
    },
    {
        options: [],
        prop: '',
        retProp: '',
        searchValue: 'idCompany',
        type: 'autocomplete',
        appearance: 'outline',
        class: 'autocomplete',
        label: 'Company',
        placeholder: '',
        shown: true,
    },
    {
        options: [],
        prop: '',
        retProp: '',
        searchValue: 'categoryName',
        type: 'autocomplete',
        appearance: 'outline',
        class: 'autocomplete',
        label: 'Category',
        placeholder: '',
        shown: false,
    },
    {
        options: [],
        prop: 'startDate',
        retProp: 'startDate',
        type: 'date',
        appearance: 'outline',
        class: 'date',
        label: 'Start Date',
        placeholder: 'Start Date',
        shown: false,
    },
    {
        options: ['ACTIVE', 'INACTIVE'],
        prop: '',
        retProp: '',
        searchValue: 'status',
        type: 'autocomplete',
        appearance: 'outline',
        class: 'autocomplete',
        label: 'Status',
        placeholder: '',
        shown: false,
    },
    {
        options: ['All', 'Coach', 'Manager', 'Mentor'],
        prop: '',
        retProp: '',
        searchValue: 'rol',
        type: 'autocomplete',
        appearance: 'outline',
        class: 'autocomplete',
        label: 'Rol',
        placeholder: '',
        shown: true,
    },
];

export const tableAux: ITableConfig = {
    multiSelect: false,
    columns: [
        {
            name: 'Id',
            prop: 'id',
            shown: true,
            route: './app/internal/employee/reports/edit',
            cellClass: 'id-table-column',
            routeId: 'idEmployee'
        },
        {
            name: 'DNI',
            prop: 'dniEmployee',
            shown: true,
        },
        {
            name: 'Name',
            prop: 'name',
            shown: true,
        },
        {
            name: 'Surname',
            prop: 'surname',
            shown: true,
        },
        {
            name: 'Internship End Date',
            prop: 'internshipEndDate',
            shown: true,
            type: 'date'
        },
        {
            name: 'First Car',
            prop: 'firstCarName',
            shown: true,
        },
        {
            name: 'First Car date',
            prop: 'firstCarDate',
            shown: true,
            type: 'date'
        },
        {
            name: 'Productive Car',
            prop: 'productiveCarName',
            shown: true,
        },
        {
            name: 'First Productive Car date',
            prop: 'firstProductiveCarDate',
            shown: true,
            type: 'date'
        },
        {
            name: 'Incorporation Type',
            prop: 'typeIncorporation',
            shown: true,
        },
    ]
};

export const filtersAux: IAuto[] = [
    {
        options: [],
        prop: 'id',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Id',
        placeholder: 'Id',
        shown: true,
    },
    {
        options: [],
        prop: '',
        retProp: '',
        searchValue: 'idCarProductivo',
        type: 'autocomplete',
        appearance: 'outline',
        class: 'autocomplete',
        label: 'Productive Car',
        placeholder: '',
        shown: true,
    },
    {
        options: [],
        prop: '',
        retProp: '',
        searchValue: 'idPrimerCar',
        type: 'autocomplete',
        appearance: 'outline',
        class: 'autocomplete',
        label: 'First Car',
        placeholder: '',
        shown: true,
    },
    {
        options: [],
        prop: 'tipoIncorporacion',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Incorporation Type',
        placeholder: 'Incorporation Type',
        shown: true,
    }
];
