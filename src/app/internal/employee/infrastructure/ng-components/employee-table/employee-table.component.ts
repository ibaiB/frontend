import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {searchEmployeeAux} from '../../../../employee-aux/application/searchEmployeeAux';
import {EmployeeAuxMapper} from '../../../../employee-aux/infrastructure/employee-aux-mapper';

import {ICar} from '../../../../car/domain/ICar';
import {ICategory} from '../../../../category/domain/ICategory';
import {ICompany} from '../../../../company/domain/ICompany';
import {errorAlert} from '../../../../../shared/error/custom-alerts';
import {ResolvedData} from '../../../../../shared/generics/IResolvedData';
import {EmployeeAuxService} from '../../../../employee-aux/infrastructure/services/employee-aux.service';
import {EmployeeTerminationComponent} from '../../../../employee-termination/infrastructure/ng-components/employee-termination/employee-termination.component';
import {filters, filtersAux, table, tableAux} from './config';
import {DynamicTableMarkIiComponent} from '../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {EmployeeMapper} from '../../EmployeeMapper';
import {IList} from '../../../../../shared/generics/IList';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {EmployeeService} from '../../services/employee.service';
import {searchEmployee} from '../../../application/searchEmployee';
import {IEmployeeReadFull} from '../../../domain/IEmployee';


@Component({
    selector: 'app-employee-table',
    templateUrl: './employee-table.component.html',
    styleUrls: ['./employee-table.component.scss']
})
export class EmployeeTableComponent implements OnInit {

    private _resolved: ResolvedData<any>;

    private companies: IList<ICompany>;
    private cars: IList<ICar>;
    categories: IList<ICategory>;

    tableConfig: ITableConfig = table;
    queryPagination = {page: 0, size: 10};
    queryOrder = {orderField: '', order: ''};
    queryFilters = {};
    tableData: IList<any>;

    filters: IAuto[] = filters;

    tableConfigAux: ITableConfig = tableAux;
    queryPaginationAux = {page: 0, size: 10};
    queryOrderAux = {orderField: '', order: ''};
    queryFiltersAux = {};
    tableDataAux: IList<any>;
    filtersAux: IAuto[] = filtersAux;

    status = true;

    loading: boolean = true;
    configVisibility: boolean = false;
    configAuxVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;
    @ViewChild('tableAux') dynamicTableAux: DynamicTableMarkIiComponent;

    constructor(
        private _employeeService: EmployeeService,
        private _employeeAuxMapper: EmployeeAuxMapper,
        private _employeeMapper: EmployeeMapper,
        private _employeeAuxService: EmployeeAuxService,
        private _route: ActivatedRoute,
        private _router: Router,
        private _dialog: MatDialog,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit() {
        const {data, error} = this._route.snapshot.data['response'];

        if (error) {
            console.error('Error in employee table resolver', error);
            //const {error: {message, id}} = error;
            //this._error.notify(`Couldn\'t retrieve data`, message, id);
        }

        if (data) {
            // Employee report
            this.tableData = this.mapToTable(data.employees);
            this.companies = data.companies;
            this.categories = data.categories;
            this.cars = data.cars;
            this.configFiltersOptions();
            this.configFiltersOptionsAux();
            parseTableFiltersConfig(data.employeeTableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = data.filterValueReports ? this._clean((JSON.parse(data.filterValueReports))) : {};
            this.tableConfig.pagination = data.employeeTableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = data.employeeTableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries(this._employeeMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
            this.configFilterValue(data);
            // Employee Planification
            this.tableDataAux = this.mapToTableAux(data.employeesAux);

            parseTableFiltersConfig(data.employeeAuxTableFiltersConfig, this.tableConfigAux, this.filtersAux);
            this.queryFiltersAux = data.filterValuePlanification ? this._clean((JSON.parse(data.filterValuePlanification))) : {};
            this.tableConfigAux.pagination = data.employeeAuxTableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPaginationAux = this.tableConfigAux.pagination;
            this.tableConfigAux.order = data.employeeAuxTableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrderAux = {...this.tableConfigAux.order};
            this.tableConfigAux.order.orderField = this.tableConfigAux.order.orderField ?
                Object.entries(this._employeeAuxMapper.mapTo({[this.tableConfigAux.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
            this.configFilterValueAux(data);
            this.loading = false;
        }
    }

    configFilterValueAux(data) {
        let queryFilterEmployeeAux = data.filterValuePlanification ? this._clean(JSON.parse(data.filterValuePlanification)) : {};
        queryFilterEmployeeAux['idCarProductivo'] = queryFilterEmployeeAux['idCarProductivo'] ? data.cars.content.find(car => car.id === queryFilterEmployeeAux['idCarProductivo']).name : '';
        queryFilterEmployeeAux['idPrimerCar'] = queryFilterEmployeeAux['idPrimerCar'] ? data.cars.content.find(car => car.id === queryFilterEmployeeAux['idPrimerCar']).name : '';
        this.filtersAux.forEach(filter => {
            filter.defaultValue = queryFilterEmployeeAux[this.getFilterProp(filter)];
            filter.value = queryFilterEmployeeAux[this.getFilterProp(filter)];
        });
    }

    configFilterValue(data) {
        let queryFilterEmployee = data.filterValueReports ? this._clean(JSON.parse(data.filterValueReports)) : {};
        queryFilterEmployee['idCompany'] = queryFilterEmployee['idCompany'] ? data.companies.content.find(company => company.id === queryFilterEmployee['idCompany']).name : '';
        queryFilterEmployee['status'] = queryFilterEmployee['status'] === false ? 'INACTIVE' : 'ACTIVE';
        queryFilterEmployee['categoryName'] = queryFilterEmployee['idCategory'] ? data.categories.content.find(category => category.id === queryFilterEmployee['idCategory']).name : '';
        queryFilterEmployee['rol'] = queryFilterEmployee['isCoach'] ? 'Coach' : (queryFilterEmployee['isMentor'] ? 'Mentor' :
            (queryFilterEmployee['isManager'] ? 'Manager' : ''));
        this.filters.forEach(filter => {
            filter.defaultValue = queryFilterEmployee[this.getFilterProp(filter)];
            filter.value = queryFilterEmployee[this.getFilterProp(filter)];
        });
    }

    configFiltersOptions() {
        this.filters.find(filter => filter.searchValue === 'idCompany').options = this.companies.content.map(company => company.name);
        this.filters.find(filter => filter.searchValue === 'categoryName').options = this.categories.content.map(category => category.name);
    }

    configFiltersOptionsAux() {
        this.filtersAux.find(filter => filter.searchValue === 'idCarProductivo').options = this.cars.content.map(car => car.name);
        this.filtersAux.find(filter => filter.searchValue === 'idPrimerCar').options = this.cars.content.map(car => car.name);
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    backSearch() {
        searchEmployee({
            ...this.queryPagination,
            ...this.queryOrder,
            ...this._clean(this.queryFilters),
            status: this.status,
            style: 'full'
        }, this._employeeService)
            .subscribe(result => {
                this.tableData = this.mapToTable(result);
            }, error => {
                console.error('error on back search', error);
                const {error: {message, id}} = error;
                errorAlert(`Couldn\'t retrieve data`, message, id);
            });
    }

    private _querySwitcher(params: any) {
        if (params.status) {
            if (params.status === 'ACTIVE') {
                this.status = true;
            } else {
                this.status = false;
                params.status = params.status === 'ACTIVE';
            }
            if (params.rol) {
                if (params.rol === 'Coach') {
                    params['isCoach'] = true;
                }
                if (params.rol === 'Manager') {
                    params['isManager'] = true;
                }
                if (params.rol === 'Mentor') {
                    params['isMentor'] = true;
                }
                params.rol = '';

            }
            if (params.startDate) {
                let date = new Date(params.startDate);
                date = new Date(date.setDate(date.getDate() - 1)); // -1 To include the current day
                params.startDateInitial = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
                date = new Date(date.setDate(date.getDate() + 1)); // Date adding one day
                params.startDateFinal = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
            }
            if (params.idCompany) {
                params.idCompany = this.companies.content.filter(company => company.name === params.idCompany)[0].id;
            }
            if (params.idCategory) {
                params.idCategory = params.idCategory.id;
            }
        }
        return params;
    }

    private _querySwitcherAux(query) {
        if (query.idCarProductivo) {
            query.idCarProductivo = this.cars.content.find(car => car.name === query.idCarProductivo).id;
        }
        if (query.idPrimerCar) {
            query.idPrimerCar = this.cars.content.find(car => car.name === query.idPrimerCar).id;
        }
        return query;
    }

    backSearchAux() {
        searchEmployeeAux({
            ...this.queryPaginationAux, ...this.queryOrderAux, ...this.queryFiltersAux,
            style: 'full'
        }, this._employeeAuxService)
            .subscribe((response) => {
                    this.tableDataAux = this.mapToTableAux(response);
                }
            );
    }

    terminateEmployee(employee) {
        this._dialog.open(EmployeeTerminationComponent, {
            width: '600px',
            data: employee
        });
    }

    doAction(event: { action: string, item: IEmployeeReadFull }) {
        if (event.action === 'Terminate') {
            this.terminateEmployee(event.item);
        }
    }

    filtersChanged(params?: any) {
        if (Object.entries(params).find(entry => entry[0] === 'categoryName')) {
            params.idCategory = this.categories.content.find(category => category.name === params.categoryName);
        }
        if (params && Object.keys(params).length !== 0) {
            const switched = this._querySwitcher(params);
            const mapped = this._employeeMapper.mapFrom(switched);
            this.queryFilters = mapped;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('reportsFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('employee-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('employee-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged(event: any) {
        event.orderField = event.orderField === 'statusString' ? 'status' : event.orderField;
        event.orderField = event.orderField === 'categoryName' ? 'idCategory' : event.orderField;
        event.orderField = event.orderField === 'companyName' ? 'idCompany' : event.orderField;

        let aux = this._employeeMapper.mapFrom({[event.orderField]: ''});

        this.queryOrder.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('employee-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('employee-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    filtersChangedAux(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            this.queryFiltersAux = this._querySwitcherAux(params);
        } else {
            this.queryFiltersAux = {};
        }
        this._cookieFacade.save('planificationFilterCache', JSON.stringify(this.queryFiltersAux));
        this.queryPaginationAux = {page: 0, size: this.queryPaginationAux.size};
        this._cookieFacade.save('employee-aux-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfigAux, this.filtersAux, this.queryPaginationAux, this.queryOrderAux)));
        this.dynamicTableAux.resetPageIndex();
        this.backSearchAux();
    }

    paginationChangedAux(event: any) {
        this.queryPaginationAux.page = event.page;
        this.queryPaginationAux.size = event.size;
        this._cookieFacade.save('employee-aux-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfigAux, this.filtersAux, this.queryPaginationAux, this.queryOrderAux)));
        this.backSearchAux();
    }

    orderChangedAux(event: any) {
        event.orderField = event.orderField === 'firstCarName' ? 'idFirstCar' : event.orderField;
        event.orderField = event.orderField === 'productiveCarName' ? 'idProductiveCar' : event.orderField;
        event.orderField = event.orderField === 'typeIncorporation' ? 'incorporationType' : event.orderField;
        let aux = this._employeeAuxMapper.mapFrom({[event.orderField]: ''});

        this.queryOrderAux.orderField = Object.entries(aux).find(entry => entry[1] === '')[0];
        this.queryOrderAux.order = event.order;

        this.queryPaginationAux.page = event.page;
        this.queryPaginationAux.size = event.size;
        this._cookieFacade.save('employee-aux-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfigAux, this.filtersAux, this.queryPaginationAux, this.queryOrderAux)));
        this.backSearchAux();
    }

    configAuxChange(event: any) {
        this.tableConfigAux = {...event[0]};
        this.filtersAux = event[1];
        this._cookieFacade.save('employee-aux-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfigAux, this.filtersAux, this.queryPaginationAux, this.queryOrderAux)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    setConfigAuxVisibility(visible: boolean) {
        this.configAuxVisibility = visible;
    }

    mapToTable(employees: IList<any>): IList<any> {
        const {content, ...common} = employees;

        return {
            content: content.map(item => {
                const {
                    category,
                    car,
                    company,
                    status,
                    ...itemCommon
                } = item;

                return {
                    categoryName: category?.name,
                    carName: car?.name,
                    companyName: company?.name,
                    statusString: status ? 'ACTIVE' : 'INACTIVE',
                    ...itemCommon
                };
            }),
            ...common
        };
    }

    mapToTableAux(employeesAux: IList<any>): IList<any> {
        let {content, ...common} = employeesAux;
        content = content.map(this._employeeAuxMapper.mapTo);

        return {
            content: content.map(item => {
                const {
                    firstCar,
                    productiveCar,
                    employees,
                    ...itemCommon
                } = item;

                return {
                    firstCarName: firstCar?.name,
                    productiveCarName: productiveCar?.name,
                    name: employees[0]?.name,
                    surname: employees[0]?.surname,
                    idEmployee: employees[0]?.id,
                    ...itemCommon
                };
            }),
            ...common
        };
    }
}
