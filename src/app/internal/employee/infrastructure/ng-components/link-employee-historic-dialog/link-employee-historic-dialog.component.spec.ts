import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LinkEmployeeHistoricDialogComponent} from './link-employee-historic-dialog.component';

describe('LinkEmployeeHistoricDialogComponent', () => {
  let component: LinkEmployeeHistoricDialogComponent;
  let fixture: ComponentFixture<LinkEmployeeHistoricDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LinkEmployeeHistoricDialogComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkEmployeeHistoricDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
