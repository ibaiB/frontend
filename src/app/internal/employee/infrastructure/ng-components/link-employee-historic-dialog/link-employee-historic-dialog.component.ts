import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CandidateService} from '../../../../../recruitment/candidate/infrastructure/services/candidate.service';
import {NewEmployeeRequestService} from '../../../../new-employee-request/infrastructure/services/new-employee-request.service';
import {IAuto} from '../../../../../shared/custom-mat-elements/dynamic';
import {searchCandidates} from '../../../../../recruitment/candidate/application/searchCandidates';
import {CandidateMapper} from '../../../../../recruitment/candidate/infrastructure/CandidateMapper';
import {searchNewEmployeeRequest} from '../../../../new-employee-request/application/searchNewEmployeeRequest';
import {IList} from '../../../../../shared/generics/IList';
import {ICandidate} from '../../../../../recruitment/candidate/domain/ICandidate';
import {INewEmployeeRequest} from '../../../../new-employee-request/domain/INewEmployeeRequest';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-link-employee-historic-dialog',
  templateUrl: './link-employee-historic-dialog.component.html',
  styleUrls: ['./link-employee-historic-dialog.component.scss']
})
export class LinkEmployeeHistoricDialogComponent implements OnInit {

  autoSelect: IAuto = {
    options: [],
    retProp: 'id',
    prop: 'virtualName',
    searchValue: 'nameSurname',
    appearance: 'standard',
    class: '',
    label: '',
    placeholder: '',
    defaultValue: '',
    type: 'autoselect'
  };

  selectedId: string;

  source: string;

  constructor(
      public dialogRef: MatDialogRef<LinkEmployeeHistoricDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,
      private _candidateService: CandidateService,
      private _newEmployeeRequestService: NewEmployeeRequestService,
      private _candidateMapper: CandidateMapper
  ) {
  }

  ngOnInit() {
    const {source} = this.data;
    this.source = source.toLowerCase();
    this.autoSelect.placeholder = source;
    this.autoSelect.label = source;
  }

  search(query: string) {
    const {source} = this.data;
    const $backQuery: Observable<IList<ICandidate | INewEmployeeRequest>> = source === 'Candidate'
        ? searchCandidates({nameSurname: query.trim()}, this._candidateService, this._candidateMapper)
        : searchNewEmployeeRequest({nameSurname: query.trim()}, this._newEmployeeRequestService);


    $backQuery.subscribe((res: IList<ICandidate | INewEmployeeRequest>) => {
      this.autoSelect.options = res.content.map(item => {
        if (source === 'Candidate') {
          const candidate = item as ICandidate;
          return {
            id: candidate.id,
            virtualName: `${candidate.name} ${candidate.surname} (${candidate.email.join(', ')})`
          };
        }

        const request = item as INewEmployeeRequest;
        return {
          id: request.id,
          virtualName: `${request.name} ${request.surname} (${request.personalEmail || 'No email'})`
        };
      });
    }, (error) => {

    });

  }

  setValue(value: any) {
    this.selectedId = value.id;
  }

  clear() {
    this.selectedId = '';
  }

  save() {
    this.dialogRef.close(this.selectedId);
  }
}
