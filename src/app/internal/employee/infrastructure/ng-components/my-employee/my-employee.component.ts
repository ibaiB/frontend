import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import Swal from 'sweetalert2';
import {EmployeeService} from '../../services/employee.service';
import {ActivatedRoute, Router} from '@angular/router';
import {types} from 'src/app/internal/document/domain/IDocument';
import {DocumentService} from 'src/app/internal/document/infrastructure/services/document.service';
import {forkJoin} from 'rxjs';
import {IEmployeeReadRelated, IPublicEmployee} from '../../../domain/IEmployee';
import {getMyEmployee} from '../../../application/getMyEmployee';
import {CookieFacadeService} from '../../../../../auth/session/infrastructure/services/cookie.service';
import {updateEmployee} from '../../../application/updateEmployee';
import {documentQuery, FileUploadQuery, imageQuery} from '../../../../../shared/file/domain/FileUploadQuery';
import {FileService} from '../../../../../shared/file/infrastructure/services/file-service.service';
import {uploadFile} from '../../../../../shared/file/application/upload';
import {searchFilesByDownloadLinks} from '../../../../../shared/file/application/searchByLinks';

@Component({
    selector: 'app-my-employee',
    templateUrl: './my-employee.component.html',
    styleUrls: ['./my-employee.component.scss'],
})
export class MyEmployeeComponent implements OnInit {

    idStorage: string;
    storageHidden = true;
    tags = ['CV_ESP', 'CV_ENG'];

    employee: IEmployeeReadRelated;
    myProfile: any;
    options: any[];

    loading: boolean = true;
    index: string = '0';
    counts;

    carName: string;
    subcarName: string;
    categoryName: string;

    coach: IPublicEmployee;
    mentor: IPublicEmployee;
    manager: IPublicEmployee;

    form: FormGroup;
    newPhoto: File;
    newPhotoLink: string;
    photoLink: string;
    links: string[] = [];

    photoUploadQuery: FileUploadQuery = imageQuery;
    documentUploadQuery: FileUploadQuery = documentQuery;
    fileTypes = types;
    uploadedFiles: any[] = [];
    localFiles: any[] = [];
    allFiles: any[] = [];

    updatingDocuments: boolean = false;
    updating: boolean = false;
    selected;

    constructor(
        private fb: FormBuilder,
        private _employeeService: EmployeeService,
        public fileService: FileService,
        public router: Router,
        public activatedRoute: ActivatedRoute,
        private _documentService: DocumentService,
        private _route: ActivatedRoute,
        private _cookieFacade: CookieFacadeService
    ) {

    }

    ngOnInit() {
        const {count, employee, profile} = this._route.snapshot.data['response'];

        const empl = employee as IEmployeeReadRelated;

        this.categoryName = empl.category?.name;
        this.carName = empl.car?.name;
        this.subcarName = empl.subcar?.name;

        this.coach = empl.coach;
        this.manager = empl.manager;
        this.mentor = empl.mentor;

        this.counts = count;
        this.options = Object.values(count.count);
        this.index = this._cookieFacade.get('profileIndex');
        this.employee = employee;

        this.myProfile = profile;
        this.photoLink = this.employee.imageLink;
        this.links = this.employee.documents.map(doc => doc.link);

        this.loadFilesFromLinks();
        this.formInit(employee);

        this.idStorage = empl.idFileManager;
        this.loading = false;
    }

    changeProfile(option: string) {
        const index = this.options.indexOf(option);
        getMyEmployee(this._employeeService, {index: index}).subscribe(
            () => {
                this._cookieFacade.save('profileIndex', `${index}`);
                window.location.reload();
            }
        );
    }

    formInit(employee: IEmployeeReadRelated) {
        this.form = this.fb.group({
            addressHab: [employee.addressHab],
            addressDel: [employee.addressDel],
            cityHab: [employee.cityHab],
            cityDel: [employee.cityDel],
            countryHab: [employee.countryHab],
            countryDel: [employee.countryDel],
            postalCodeHab: [employee.postalCodeHab],
            postalCodeDel: [employee.postalCodeDel],
            provinceHab: [employee.provinceHab],
            provinceDel: [employee.provinceDel],
            personalPhoneNumber: [employee.personalPhoneNumber],
            personalPhoneNumberFijo: [employee.landLinePhoneNumber],
        });
    }

    onSubmit() {
        if (this.updating) {
            return;
        }
        this.updating = true;
        if (this.newPhoto) {
            this._updatePhotoAndEmployee();
        } else {
            this._updateEmployee();
        }
    }

    photoChanged($event) {
        this.newPhoto = $event;
        this.photoUploadQuery.uploadName = $event.name;
    }


    private _updatePhotoAndEmployee() {
        uploadFile(
            this.newPhoto,
            this.photoUploadQuery,
            this.fileService)
            .subscribe((retFile) => {
                this.newPhotoLink = retFile.id;
                this._updateEmployee();
            });
    }

    private _updateEmployee() {
        let change = {
            addressHab: this.form.get('addressHab').value,
            addressDel: this.form.get('addressDel').value,
            cityHab: this.form.get('cityHab').value,
            cityDel: this.form.get('cityDel').value,
            countryHab: this.form.get('countryHab').value,
            countryDel: this.form.get('countryDel').value,
            postalCodeHab: this.form.get('postalCodeHab').value,
            postalCodeDel: this.form.get('postalCodeDel').value,
            provinceHab: this.form.get('provinceHab').value,
            provinceDel: this.form.get('provinceDel').value,
            personalPhoneNumber: this.form.get('personalPhoneNumber').value,
            personalPhoneNumberFijo: this.form.get('personalPhoneNumberFijo').value,
            idLinkImagen: this.newPhotoLink ?? null,
            id: this.employee.id
        };

        updateEmployee(change, this._employeeService, {index: this.index}).subscribe(
            (ed) => {
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    confirmButtonColor: '#db5e5e',
                });
            },
            () => {
                this.updating = false;
                this.newPhoto = null;
            }
        );
    }

    updateEmployeeDocuments() {
        if (this.updatingDocuments) {
            return;
        }
        this.updatingDocuments = true;
        const $tasks = [];

        this.localFiles.forEach(file => {
            $tasks.push(this._documentService.create({
                idEmployee: this.employee.id,
                idLink: file.id,
                employeeDocumentTypeEnum: file.type,
            }, true, this.index));
        });

        this.uploadedFiles.forEach(file => {
            if (!this.allFiles.includes(file)) {
                const docId = this.employee.documents.find(doc => doc.link === file.url)?.id;
                if (docId) {
                    $tasks.push(this._documentService.delete(docId, true));
                }
            }
        });

        forkJoin(...$tasks).subscribe(res => {
            this.links = this.allFiles.map(file => file.url ?? file.downloadLink);
            this.localFiles = [];
            this.uploadedFiles = [];
            this.loadFilesFromLinks();

            this.updatingDocuments = false;
        });
    }

    changeFileType(file, type) {
        const conflictingTypeFile = this.allFiles.find(f => f.type === type);
        if (conflictingTypeFile) {
            Swal.fire({
                title: 'Other file has the same type',
                text: 'Do you want to change it?',
                showDenyButton: true,
                confirmButtonText: 'Change',
                denyButtonText: 'Cancel',
                icon: 'warning',
            }).then((result) => {
                if (result.isConfirmed) {
                    conflictingTypeFile.type = null;
                    file.type = type;
                } else {
                    file.type = null;
                    file.type = {...file.type};
                }
            });
        } else {
            file.type = type;
        }
    }

    sentFile(event: any) {
        event.type = null;
        this.localFiles.push(event);
        this.allFiles.push(event);
        this.sortFiles();
    }

    deleteFile(event: any) {
        this.localFiles = this.localFiles.filter(f => f.url !== event.url);
        this.allFiles = this.allFiles.filter(f => f.url !== event.url);
    }

    clearType(file) {
        file.type = null;
    }

    loadFilesFromLinks() {
        searchFilesByDownloadLinks(this.links, this.fileService).subscribe(
            (files) => {

                files.forEach((file) => {
                    this.uploadedFiles.push(file);
                });

                this.uploadedFiles = this.uploadedFiles.map(file => {
                    return {
                        ...file,
                        type: this.employee.documents.find(doc => doc.link === file.url)?.type ?? null
                    };
                });

                this.allFiles = [...this.uploadedFiles];
                this.sortFiles();
            },
            (error) => {
                console.info(error);
            });
    }

    sortFiles() {
        this.allFiles.sort((a, b) => (a.name ?? a.originalName) > (b.name ?? b.originalName) ? 1 : -1);
        this.allFiles = [...this.allFiles];
    }

    setStorageVisibility(visibility: boolean) {
        this.storageHidden = !visibility;
    }
}
