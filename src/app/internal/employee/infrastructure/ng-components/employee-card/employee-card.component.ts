import {Component, Input, OnInit} from '@angular/core';
import {IPublicEmployee} from '../../../domain/IEmployee';

@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.scss']
})

export class EmployeeCardComponent implements OnInit {

  @Input() employee: IPublicEmployee;

  photoLink: string;
  color: string;

  constructor() { }

  ngOnInit(): void {
    this.photoLink = this.employee.imageLink;
  }

  


}