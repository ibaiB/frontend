import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ICar} from '../../../../car/domain/ICar';
import {ITechnology} from '../../../../technology/domain/ITechnology';
import {ISubcar} from '../../../../subcar/domain/ISubcar';
import {ICompany} from '../../../../company/domain/ICompany';
import {ICategory} from '../../../../category/domain/ICategory';
import {debounceTime} from 'rxjs/operators';
import Swal from 'sweetalert2';
import {ICostPerMonth} from '../../../domain/ICostPerMonth';
import {EmployeeService} from '../../services/employee.service';
import {EmployeeAuxService} from '../../../../employee-aux/infrastructure/services/employee-aux.service';
import {EmployeeAuxMapper} from '../../../../employee-aux/infrastructure/employee-aux-mapper';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {IAuto} from '../../../../../shared/custom-mat-elements/dynamic';
import {searchEmployee} from '../../../application/searchEmployee';
import {IEmployeeReadFull, IEmployeeReadRelated, IPublicEmployee} from '../../../domain/IEmployee';
import {ICenter} from '../../../../../shared/entities/center/domain/ICenter';
import {updateEmployeeAux} from '../../../../employee-aux/application/updateEmployeeAux';
import {updateEmployee} from '../../../application/updateEmployee';
import {IEmployeeAuxRead} from '../../../../employee-aux/domain/IEmployeeAux';
import {MatDialog} from '@angular/material/dialog';
import {LinkEmployeeHistoricDialogComponent} from '../link-employee-historic-dialog/link-employee-historic-dialog.component';
import {updateNewEmployeeRequest} from '../../../../new-employee-request/application/updateNewEmployeeRequest';
import {NewEmployeeRequestMapper} from '../../../../new-employee-request/infrastructure/new-employee-request-mapper';
import {NewEmployeeRequestService} from '../../../../new-employee-request/infrastructure/services/new-employee-request.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
    selector: 'app-employee-management',
    templateUrl: './employee-management.component.html',
    styleUrls: ['./employee-management.component.scss'],
    animations: [
        trigger(
            'inOutAnimation',
            [
                state('open', style({transform: 'translateX(-50%)'})),
                state('close', style({transform: 'translateX(0)'})),
                transition('open => close', animate('0.5s ease-in-out')),
                transition('close => open', animate('0.5s ease-in-out'))
            ]
        )
    ]
})
export class EmployeeManagementComponent implements OnInit {

    idChat: string;
    idStorage: string;

    // ===== HIERARCHY =====
    coach: IPublicEmployee;
    mentor: IPublicEmployee;
    manager: IPublicEmployee;

    coachOf: IEmployeeReadFull[] = [];
    mentorOf: IEmployeeReadFull[] = [];
    managerOf: IEmployeeReadFull[] = [];

    coachAutoSelect: IAuto = {
        options: [],
        retProp: 'id',
        prop: 'virtualName',
        searchValue: 'name',
        appearance: 'standard',
        class: '',
        label: 'Coach',
        placeholder: 'Coach',
        defaultValue: '',
        type: 'autoselect'
    };

    mentorAutoSelect: IAuto = {
        options: [],
        retProp: 'id',
        prop: 'virtualName',
        searchValue: 'name',
        appearance: 'standard',
        class: '',
        label: 'Mentor',
        placeholder: 'Mentor',
        defaultValue: '',
        type: 'autoselect'
    };

    managerAutoSelect: IAuto = {
        options: [],
        retProp: 'id',
        prop: 'virtualName',
        searchValue: 'name',
        appearance: 'standard',
        class: '',
        label: 'Manager',
        placeholder: 'Manager',
        defaultValue: '',
        type: 'autoselect'
    };
    // ===== BILLING =====
    billingForm: FormGroup;
    public costsPerMonth: ICostPerMonth[] = [];

    // ===== EMPLOYEE ======
    currentEmployee: IEmployeeReadRelated;
    employeeOptions: string[] = [];

    photoLink: string;
    carName: string;
    subcarName: string;
    centerName: string;
    companyName: string;
    englishLevel: string;
    primaryTechName: string;
    secondaryTechName: string;
    categoryName: string;

    car: ICar;
    subcar: ISubcar;
    category: ICategory;
    center: ICenter;
    studyYear: string;
    tech1: ITechnology;
    tech2: ITechnology;

    personalForm: FormGroup;
    businessForm: FormGroup;
    leftForm: FormGroup;
    techForm: FormGroup;

    // ===== EMPLOYEE AUX =====
    firstCarName: string;
    productiveCarName: string;

    firstCar: ICar;
    productiveCar: ICar;
    empAux: any;

    empAuxForm: FormGroup;

    // ===== COMBOS =====
    cars: string[] = [];
    centers: string[] = [];
    technologies: string[] = [];
    subcars: string[] = [];
    companies: string[] = [];
    categories: string[] = [];
    years: string[] = ['1º', '2º', '3º', '4º'];
    englishLevels: string[] = ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'];

    // ===== FK TABLES =====
    carList: ICar[] = [];
    centerList: ICenter[] = [];
    technologyList: ITechnology[] = [];
    subcarList: ISubcar[] = [];
    companyList: ICompany[] = [];
    categoryList: ICategory[] = [];

    // ===== COMPONENT =====
    loading = false;
    editProfile = false;
    editCoach = false;
    editMentor = false;
    editManager = false;
    chatHidden = true;
    storageHidden = true;

    constructor(
        private _route: ActivatedRoute,
        private _fb: FormBuilder,
        private _employeeService: EmployeeService,
        private _newEmployeeRequestService: NewEmployeeRequestService,
        private _employeeAuxService: EmployeeAuxService,
        private _employeeAuxMapper: EmployeeAuxMapper,
        private _router: Router,
        private _dialog: MatDialog) {
    }


    ngOnInit(): void {
        const {data} = this._route.snapshot.data['response'];

        if (data) {
            const {employee} = data;
            this.currentEmployee = employee;
            this.initLists(data);
            this.initCombos(data);
            this.initHierarchy(employee);
            this.initDefaults(employee);
            this.initEmpAuxForm(employee.employeeAux);
            this.initOptions(employee.employeeAux);
            this.initLeftForm(employee);
            this.initPersonalForm(employee);
            this.initBusinessForm(employee);
            this.initTechForm(employee);
            this.initBillingForm(employee);
            this.loading = false;
            this.idChat = employee.idChat;
            this.idStorage = employee.idPrivateFileManager;
        }
    }

    // ===== AUX =====
    initDefaults(employee: IEmployeeReadRelated) {
        this.firstCarName = employee.employeeAux?.firstCar?.name ?? '';
        this.productiveCarName = employee.employeeAux?.productiveCar?.name ?? '';

        this.photoLink = employee.imageLink;
        this.carName = employee.car?.name ?? '';
        this.subcarName = employee.subcar?.name ?? '';
        this.companyName = employee.company?.name ?? '';
        this.categoryName = employee.category?.name ?? '';
        this.centerName = employee.center?.location ?? '';
        this.englishLevel = employee.englishLevel;
        this.primaryTechName = employee.technologies[0]?.name ?? '';
        this.secondaryTechName = employee.technologies[1]?.name ?? '';
        this.center = employee.center;
        this.tech1 = employee.technologies[0];
        this.tech2 = employee.technologies[1];
        this.studyYear = employee.studyYear;
    }

    // ===== EMPLOYEE AUX =====
    initEmpAuxForm(employee?: IEmployeeAuxRead) {
        this.empAux = employee;

        this.empAuxForm = this._fb.group({
            endTrainingDate: [employee?.internshipEndDate || ''],
            dateFirstProductiveCar: [employee?.firstProductiveCarDate || ''],
            firstCarDate: [employee?.firstCarDate || ''],
            dniEmployee: [{value: employee?.dniEmployee, disabled: true}],
            idProductiveCar: [this.productiveCarName],
            idFirstCar: [this.firstCarName],
            technologyInvestment: [employee?.technologyInvestment || 'Pendiente de determinar'],
            incorporationType: [employee?.typeIncorporation || ''],
        });

        this.empAuxForm.get('idProductiveCar')
            .valueChanges
            .subscribe((value) => {
                const car = this.carList.find(c => c.name === value);
                if (car) {
                    this.productiveCar = car;
                    this.productiveCarName = car.name;
                }
            });

        this.empAuxForm.get('idFirstCar')
            .valueChanges
            .subscribe((value) => {
                const car = this.carList.find(c => c.name === value);
                if (car) {
                    this.firstCar = car;
                    this.firstCarName = car.name;
                }
            });
    }

    initOptions(employee?: IEmployeeAuxRead) {
        this.employeeOptions = employee ? employee.employees.map(e => `(${e.id})`) : [];
    }

    auxAutocompleteSelected(field: string, value: string) {
        this.empAuxForm.controls[field].setValue(value);
    }

    saveEmpAux() {
        this.empAuxForm.value.idFirstCar = this.firstCar?.id ?? null;
        this.empAuxForm.value.idProductiveCar = this.productiveCar?.id ?? null;

        updateEmployeeAux(this.empAuxForm.value, this.empAux.id, this._employeeAuxService)
            .subscribe(() => {
                successAlert('Changes where saved');
            });
    }

    // ===== EMPLOYEE =====
    initHierarchy(employee: IEmployeeReadRelated) {
        this.coach = employee.coach;
        this.mentor = employee.mentor;
        this.manager = employee.manager;
        this.coachOf = employee.coachOf;
        this.mentorOf = employee.mentorOf;
        this.managerOf = employee.managerOf;

        this.coachAutoSelect.defaultValue = `${this.coach?.name ?? ''} ${this.coach?.surname ?? ''}`;
        this.mentorAutoSelect.defaultValue = `${this.mentor?.name ?? ''} ${this.mentor?.surname ?? ''}`;
        this.managerAutoSelect.defaultValue = `${this.manager?.name ?? ''} ${this.manager?.surname ?? ''}`;
    }

    initLeftForm(employee: IEmployeeReadRelated) {
        this.leftForm = this._fb.group({
            name: [employee.name],
            surname: [employee.surname],
            category: [employee.category?.name ?? ''],
            car: [this.carName],
            subcar: [this.subcarName],
            coachName: [this.coach?.name ?? ''],
            mentorName: [this.mentor?.name ?? ''],
            managerName: [this.manager?.name ?? '']
        });

        this.leftForm.get('car')
            .valueChanges
            .subscribe((value) => {
                const car = this.carList.find((car) => car.name === value);
                if (car) {
                    this.car = car;
                    this.carName = car.name;
                    this.subcars = car.subCars.map((subcar) => subcar.name);
                }
            });

        this.leftForm.get('subcar')
            .valueChanges
            .subscribe((value) => {
                const subcar = this.subcarList.find((subcar) => subcar.name === value);
                if (subcar) {
                    this.subcar = subcar;
                    this.subcarName = subcar.name;
                }
            });

        this.leftForm.get('category')
            .valueChanges
            .subscribe((value) => {
                const category = this.categoryList.find((category) => category.name === value);
                if (category) {
                    this.category = category;
                    this.categoryName = category.name;
                }
            });
    }

    searchSuperior(value: string, event: string) {
        searchEmployee({nameSurname: event.trim(), status: true}, this._employeeService)
            .subscribe(employees => {
                const virtEmployees = employees.content.map(e => {
                    return {
                        ...e,
                        virtualName: e.name + ' ' + e.surname
                    };
                });

                if (value === 'coach') {
                    this.coachAutoSelect.options = virtEmployees;
                }
                if (value === 'mentor') {
                    this.mentorAutoSelect.options = virtEmployees;
                }
                if (value === 'manager') {
                    this.managerAutoSelect.options = virtEmployees;
                }
            });
    }

    clearSuperior(field: string) {
        switch (field) {
            case 'coach': {
                this.coach = null;
                break;
            }
            case 'mentor': {
                this.mentor = null;
                break;
            }
            case 'manager': {
                this.manager = null;
                break;
            }
        }
    }

    selectedItem(event: any) {
        const mapped = {
            id: event.value.id,
            name: event.value.name,
            surname: event.value.surname,
            businessEmail: event.value.businessEmail,
            categoryName: event.value.category.name,
            linkImagen: event.value.linkImagen,
            businessPhone: '',
            nameSurname: '',
            imageLink: '',
            startDate: '',
            center: null,
            status: null,
            technologies: [], 
            linkedin: ''
        };

        switch (event.searchValue) {
            case 'coach': {
                this.coach = mapped;
                break;
            }
            case 'mentor': {
                this.mentor = mapped;
                break;
            }
            case 'manager': {
                this.manager = mapped;
                break;
            }
        }
    }

    initPersonalForm(employee: IEmployeeReadRelated) {
        this.personalForm = this._fb.group({
            personalPhoneNumber: [employee.personalPhoneNumber],
            socialSecurityNumber: [{value: employee.socialSecurityNumber, disabled: true}],
            personalEmail: [employee.personalEmail],
            personalPhoneNumberFijo: [employee.landLinePhoneNumber || ''],
            englishLevel: [employee.englishLevel],
            countryHab: [employee.countryHab || ''],
            countryDel: [employee.countryDel || ''],
            provinceHab: [employee.provinceHab || ''],
            provinceDel: [employee.provinceDel || ''],
            cityHab: [employee.cityHab || ''],
            cityDel: [employee.cityDel || ''],
            postalCodeHab: [employee.postalCodeHab || ''],
            postalCodeDel: [employee.postalCodeDel || ''],
            addressHab: [employee.addressHab || ''],
            addressDel: [employee.addressDel || ''],
        });
    }

    initBusinessForm(employee: IEmployeeReadRelated) {
        this.businessForm = this._fb.group({
            businessPhoneNumber: [employee.businessPhoneNumber || ''],
            startDate: [{value: employee.startDate, disabled: true}],
            endDate: [{value: employee.endDate, disabled: true}],
            company: [{value: this.companyName, disabled: true}],
            center: [this.centerName],
            contractType: [{value: employee.contractType, disabled: true}],
            internshipType: [employee.internshipType],
            workingDate: [{value: employee.workingDay.description, disabled: true}],
            fixedRemuneration: [{value: employee.fixedRemuneration, disabled: true}],
            variableRemuneration: [{value: employee.variableRemuneration, disabled: true}],
            hosting: [employee.hosting?.toString() ?? ''],
            contratable: [employee.availableToHire?.toString() ?? ''],
            hostingCity: [employee.hostingCity],
            generalMark: [employee.generalMark],
            geographicAvailability: [{value: employee.geographicAvailability, disabled: true}],
            primaryTechnology: [this.primaryTechName],
            secondaryTechnology: [this.secondaryTechName],
            profileType: [employee.profileType],
            recommendedItinerary: [employee.recommendedItinerary],
            cityOfPreference: [employee.cityOfPreference],
            studiesEndDate: [employee.studiesEndDate || ''],
            previsionEndDate: [employee.previsionEndDate || ''],
            studyYear: [employee.studyYear || ''],
            testEndDate: [employee.internshipEndDate || ''],
        });


        this.businessForm.get('center')
            .valueChanges
            .subscribe((value) => {
                const center = this.centerList.find((t) => t.location === value);
                if (center) {
                    this.center = center;
                }
            });

        this.businessForm.get('studyYear')
            .valueChanges
            .subscribe((value) => {
                this.studyYear = value;
            });
    }

    initTechForm(employee) {
        this.techForm = this._fb.group({
            primaryTechnology: [this.primaryTechName],
            secondaryTechnology: [this.secondaryTechName],
            profileType: [employee.profileType],
            recommendedItinerary: [employee.recommendedItinerary],
        });

        this.businessForm.get('primaryTechnology')
            .valueChanges
            .subscribe((value) => {
                const tech = this.technologyList.find((t) => t.name === value);
                if (tech) {
                    this.tech1 = tech;
                }
            });

        this.businessForm.get('secondaryTechnology')
            .valueChanges
            .subscribe((value) => {
                const tech = this.technologyList.find((t) => t.name === value);
                if (tech) {
                    this.tech2 = tech;
                }
            });
    }

    changeProfile(index: number) {

    }

    personalAutocompleteSelected(field: string, value: string) {
        this.personalForm.controls[field].setValue(value);
    }

    businessAutocompleteSelected(field: string, value: string) {
        this.businessForm.controls[field].setValue(value);
    }

    profileAutocompleteSelected(field: string, value: string) {
        this.leftForm.controls[field].setValue(value);
    }

    saveProfile() {
        this.editProfile = false;
        updateEmployee({
            id: this.currentEmployee.id,
            idCar: this.car?.id ?? null,
            idSubcar: this.subcar?.id ?? null,
            idCategory: this.category?.id ?? null,
            idEmployeeCoach: this.coach?.id ?? null,
            idEmployeeMentor: this.mentor?.id ?? null,
            idEmployeeManager: this.manager?.id ?? null
        }, this._employeeService).subscribe(() => {
            successAlert('Changes where saved');
        });
    }

    saveCoach() {
        this.editCoach = false;
        updateEmployee({
            id: this.currentEmployee.id,
            idEmployeeCoach: this.coach?.id ?? null,
            idEmployeeMentor: this.mentor?.id ?? null,
            idEmployeeManager: this.manager?.id ?? null
        }, this._employeeService).subscribe(() => {
            successAlert('Changes where saved');
        });
    }

    saveMentor() {
        this.editMentor = false;
        updateEmployee({
            id: this.currentEmployee.id,
            idEmployeeCoach: this.coach?.id ?? null,
            idEmployeeMentor: this.mentor?.id ?? null,
            idEmployeeManager: this.manager?.id ?? null
        }, this._employeeService).subscribe(() => {
            successAlert('Changes where saved');
        });
    }

    saveManager() {
        this.editManager = false;
        updateEmployee({
            id: this.currentEmployee.id,
            idEmployeeCoach: this.coach?.id ?? null,
            idEmployeeMentor: this.mentor?.id ?? null,
            idEmployeeManager: this.manager?.id ?? null
        }, this._employeeService).subscribe(() => {
            successAlert('Changes where saved');
        });
    }

    savePersonal() {
        updateEmployee({
            id: this.currentEmployee.id,
            idEmployeeCoach: this.coach?.id ?? null,
            idEmployeeMentor: this.mentor?.id ?? null,
            idEmployeeManager: this.manager?.id ?? null,
            ...this.personalForm.value
        }, this._employeeService).subscribe(() => {
            successAlert('Changes where saved');
        });
    }

    saveBusiness() {
        const {testEndDate} = this.businessForm.value;

        updateEmployee({
            id: this.currentEmployee.id,
            idEmployeeCoach: this.coach?.id ?? null,
            idEmployeeMentor: this.mentor?.id ?? null,
            idEmployeeManager: this.manager?.id ?? null,
            ...this.businessForm.value,
            idCenter: this.center?.id ?? null,
            annioEstudios: this.studyYear,
            internshipEndDate: testEndDate
        }, this._employeeService).subscribe(() => {
            successAlert('Changes where saved');
        });
    }

    saveTechs() {
        const techs = [];
        if (this.tech1) {
            techs.push(this.tech1.id);
        }
        if (this.tech2) {
            techs.push(this.tech2.id);
        }

        updateEmployee({
            id: this.currentEmployee.id,
            idEmployeeCoach: this.coach?.id ?? null,
            idEmployeeMentor: this.mentor?.id ?? null,
            idEmployeeManager: this.manager?.id ?? null,
            ...this.techForm.value,
            annioEstudios: this.studyYear,
            idTechnologies: techs
        }, this._employeeService).subscribe(() => {
            successAlert('Changes where saved');
        });
    }

    // ===== BILLING =====
    initBillingForm(employee: IEmployeeReadRelated) {
        this.costsPerMonth = employee.costsPerMonth;

        this.billingForm = this._fb.group({
            startingMonth: [employee.startingMonth || new Date()],
            studiesTotalCost: [employee.studiesTotalCost || 0],
            internshipMonths: [employee.internshipMonths || 0],
            expandableMonths: [employee.expandableMonths || 0],
        });

        this.billingForm
            .get('internshipMonths')
            .valueChanges.pipe(debounceTime(400))
            .subscribe(() => this.generateFields());
        this.billingForm
            .get('expandableMonths')
            .valueChanges.pipe(debounceTime(400))
            .subscribe(() => this.generateFields());
        this.billingForm
            .get('startingMonth')
            .valueChanges
            .subscribe((value) => this._catastrofe(value));
    }

    generateFields() {
        const date: Date = new Date(this.billingForm.get('startingMonth').value);
        const costsNumber =
            Number.parseInt(this.billingForm.get('internshipMonths').value) +
            Number.parseInt(this.billingForm.get('expandableMonths').value);
        if (costsNumber < this.costsPerMonth.length) {
            if (this.currentEmployee.costsPerMonth.some((costsMont) => costsMont.coste > 0)) {
                Swal.fire({
                    icon: 'warning',
                    title: 'Are you sure?',
                    text: 'The number of months entered is less than the current one. There are months that will be lost',
                    showCancelButton: true,
                    confirmButtonColor: '#DE7373',
                    cancelButtonColor: '#8C8C8C',
                    confirmButtonText: 'Yes',
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.currentEmployee.costsPerMonth = this.currentEmployee.costsPerMonth.slice(0, costsNumber);
                        this.costsPerMonth = this.currentEmployee.costsPerMonth;
                    } else {
                        this.billingForm.get('internshipMonths').setValue(this.currentEmployee.internshipMonths);
                        this.billingForm.get('expandableMonths').setValue(this.currentEmployee.expandableMonths);
                    }
                });
            } else {
                this.costsPerMonth = [];
                this._iterate(date, costsNumber);
            }
        } else {
            this._iterate(date, costsNumber);
        }
    }

    private _iterate(date, costsNumber) {
        let newDate = new Date(date.setMonth(date.getMonth() + this.costsPerMonth.length));
        for (let i = this.costsPerMonth.length; i < costsNumber; i++) {
            this.costsPerMonth.push({fecha: newDate.toISOString(), coste: 0});
            newDate = new Date(date.setMonth(date.getMonth() + 1));
        }
    }

    private _catastrofe(date) {
        let auxDate: Date = new Date(date);
        this.costsPerMonth.forEach(costMonth => {
            costMonth.fecha = auxDate.toISOString();
            auxDate = new Date(auxDate.setMonth(auxDate.getMonth() + 1));
        });
    }

    saveBilling() {
        const {
            startingMonth,
            studiesTotalCost,
            internshipMonths,
            expandableMonths
        } = this.billingForm.value;

        updateEmployee({
            id: this.currentEmployee.id,
            idEmployeeCoach: this.coach?.id ?? null,
            idEmployeeMentor: this.mentor?.id ?? null,
            idEmployeeManager: this.manager?.id ?? null,
            mesInicio: startingMonth,
            costeTotalEstudios: studiesTotalCost,
            numeroMesesDePracticas: internshipMonths,
            numeroMesesAmpliables: expandableMonths,
            costesPorMes: this.costsPerMonth
        }, this._employeeService).subscribe(() => {
            successAlert('Changes where saved');
        });
    }

    // ===== COMBOS =====
    initLists(data: any) {
        this.carList = data.cars.content;
        this.subcarList = data.subCars.content;
        this.companyList = data.companies.content;
        this.categoryList = data.categories.content;
        this.centerList = data.center.content;
        this.technologyList = data.technologies.content;
    }

    initCombos(data: any) {
        this.cars = data.cars.content.map((car) => car.name);
        this.subcars = data.subCars.content.map((subcar) => subcar.name);
        this.companies = data.companies.content.map((company) => company.name);
        this.categories = data.categories.content.map((category) => category.name);
        this.centers = data.center.content.map((center) => center.location);
        this.technologies = data.technologies.content.map((tech) => tech.name);
    }

    // ===== NAVIGATION =====
    goToCandidate() {
        const url: string = '/app/recruitment/candidates/candidate-detail/';

        if (this.currentEmployee.idCandidate) {
            return this._router.navigate([`${url}/${this.currentEmployee.idCandidate}`]);
        }
        //539
        if (this.currentEmployee.newEmployeeRequest?.candidate?.id) {
            Swal.fire({
                icon: 'info',
                text: 'Looks like there is a candidate linked to the employee request associated with this employee' +
                    'Do you want to link it to the employee?',
                showCancelButton: true,
                showConfirmButton: true,
                cancelButtonColor: '#8C8C8C',
                confirmButtonColor: '#DE7373'
            }).then(result => {
                if (result.isConfirmed) {
                    this.linkCandidate({
                        id: this.currentEmployee.id,
                        idEmployeeCoach: this.coach?.id ?? null,
                        idEmployeeMentor: this.mentor?.id ?? null,
                        idEmployeeManager: this.manager?.id ?? null,
                        idCandidate: this.currentEmployee.newEmployeeRequest?.candidate?.id
                    });
                    return;
                }
                return this.openLinkHistoricModal('Candidate');
            });
        }

        this.openLinkHistoricModal('Candidate');
    }

    goToRequest() {
        if (!this.currentEmployee.idNewEmployeeRequest) {
            return this.openLinkHistoricModal('Employee request');
        }

        this._router.navigate(['/app/internal/employee/new-requests/detail/' + this.currentEmployee.idNewEmployeeRequest]);
    }

    openLinkHistoricModal(source: string) {
        const dialogRef = this._dialog.open(LinkEmployeeHistoricDialogComponent, {
            data: {source: source}
        });

        dialogRef
            .afterClosed()
            .subscribe(res => {
                if (!res) {
                    return;
                }

                if (source === 'Candidate') {
                    return this.linkCandidate({
                        id: this.currentEmployee.id,
                        idEmployeeCoach: this.coach?.id ?? null,
                        idEmployeeMentor: this.mentor?.id ?? null,
                        idEmployeeManager: this.manager?.id ?? null,
                        idCandidate: res
                    });
                }

                this.linkEmployeeRequest({
                    id: res,
                    idEmployee: this.currentEmployee.id
                });
            });

    }

    linkCandidate(query) {
        updateEmployee(query, this._employeeService)
            .subscribe(response => {
                successAlert('Changes where saved');
                this.currentEmployee = response as IEmployeeReadRelated;
            });
    }

    linkEmployeeRequest(query) {
        const mapper = new NewEmployeeRequestMapper();
        updateNewEmployeeRequest(query, this._newEmployeeRequestService, mapper)
            .subscribe(() => {
                successAlert('Changes where saved');
            });
    }

    setChatVisibility(visibility: boolean) {
        this.chatHidden = !visibility;
    }

    setStorageVisibility(visibility: boolean) {
        this.storageHidden = !visibility;
    }
}
