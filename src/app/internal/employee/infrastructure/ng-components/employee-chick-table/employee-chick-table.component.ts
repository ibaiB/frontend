import {Router} from '@angular/router';
import {IAuto} from 'src/app/shared/custom-mat-elements/dynamic';
import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatSelectChange} from '@angular/material/select';
import {IEmployeeReadFull} from '../../../domain/IEmployee';

@Component({
  selector: 'app-employee-chick-table',
  templateUrl: './employee-chick-table.component.html',
  styleUrls: ['./employee-chick-table.component.scss']
})
export class EmployeeChickTableComponent implements OnInit, AfterViewInit {

  @Input() chicks: IEmployeeReadFull[] = [];
  displayedColumns: string[] = ['id', 'name', 'surname', 'businessEmail', 'personalPhoneNumber'];
  dataSource: MatTableDataSource<any>;
  noResults = false;
  filters: IAuto[] = [];
  query;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  
  selected = "true";
  constructor(private _router: Router) {}

  ngOnInit(){
    const activeChicks = this.chicks.filter(chick => chick.status === true);
    if (activeChicks.length === 0){this.noResults = true; }
    this.dataSource = new MatTableDataSource(activeChicks);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.filteredData.length === 0){
      this.noResults = true;
    }else{
      this.noResults = false;
    }
  }

  navigateTo(row){
    this._router.navigate(['./app/recruitment/employee/reports/edit', row.id]);
  }

  change(evento: MatSelectChange){
    if (evento.value){
      const filteredChicks = this.chicks.filter(chick => chick.status.toString() == evento.value);
      this.dataSource = new MatTableDataSource(filteredChicks);
    }else{
      this.dataSource = new MatTableDataSource(this.chicks);
    }
  }
}

