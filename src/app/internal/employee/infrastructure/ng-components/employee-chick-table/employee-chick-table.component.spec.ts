import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EmployeeChickTableComponent} from './employee-chick-table.component';

describe('EmployeeCoachingTableComponent', () => {
  let component: EmployeeChickTableComponent;
  let fixture: ComponentFixture<EmployeeChickTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeChickTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeChickTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
