import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {EmployeeService} from '../services/employee.service';
import {getMyProfile} from '../../application/getMyProfile';
import {getMyCount} from '../../application/getMyCount';
import {catchError, map} from 'rxjs/operators';
import {getMyEmployee} from '../../application/getMyEmployee';
import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {CookieFacadeService} from '../../../../auth/session/infrastructure/services/cookie.service';

@Injectable({
    providedIn: 'root'
})
export class MyStaffitResolverService implements Resolve<ResolvedData<any>> {

    constructor(
        private _employeeService: EmployeeService,
        private _cookieFacade: CookieFacadeService,
        private _router: Router
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const $meCount: Observable<any> = getMyCount(this._employeeService);
        const $meRelated: Observable<any> = getMyEmployee(this._employeeService, {style: 'related', index:  this._cookieFacade.get('profileIndex')});
        const $myProfile: Observable<any> = getMyProfile(this._employeeService);

        return forkJoin($meCount, $meRelated, $myProfile)
            .pipe(
                map(res => ({
                    count: res[0],
                    employee: res[1],
                    profile: res[2]
                })),
                catchError(err => {
                    //TODO: Handle error and reroute
                    this._router.navigate(['/error']);
                    return of({
                        count: null,
                        employee: null,
                        profile: null,
                    });
                })
            );
    }
}
