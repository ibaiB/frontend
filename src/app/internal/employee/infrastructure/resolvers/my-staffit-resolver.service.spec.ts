import {TestBed} from '@angular/core/testing';

import {MyStaffitResolverService} from './my-staffit-resolver.service';

describe('MyStaffitResolverService', () => {
  let service: MyStaffitResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyStaffitResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
