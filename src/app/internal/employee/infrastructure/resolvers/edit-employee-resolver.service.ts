import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {getCars} from '../../../car/application/getCars';
import {getCategories} from '../../../category/application/getCategories';
import {getCenters} from '../../../../shared/entities/center/application/getCenters';
import {getCompanies} from '../../../company/application/getCompanies';
import {getSubCars} from '../../../subcar/application/getSubCars';
import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {CarService} from '../../../car/infrastructure/services/car.service';
import {CategoryService} from '../../../category/infrastructure/services/category.service';
import {CenterService} from '../../../../shared/entities/center/infrastructure/service/center.service';
import {CompanyService} from '../../../company/infrastructure/services/company.service';
import {EmployeeService} from '../services/employee.service';
import {SubcarService} from '../../../subcar/infrastructure/services/subcar.service';
import {searchTechnology} from '../../../technology/application/searchTechnology';
import {TechnologyService} from '../../../technology/infrastructure/services/technology.service';
import {getDictionaries} from '../../../../shared/entities/dictionary/application/getDictionaries';
import {DictionaryService} from '../../../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {IList} from '../../../../shared/generics/IList';
import {ICar} from '../../../car/domain/ICar';
import {ISubcar} from '../../../subcar/domain/ISubcar';
import {IDictionaryType} from '../../../../shared/entities/dictionary/domain/IDictionaryType';
import {getEmployee} from '../../application/getEmployee';
import {searchEmployee} from '../../application/searchEmployee';

@Injectable({
    providedIn: 'root'
})
export class EditEmployeeResolverService implements Resolve<ResolvedData<any>> {

    constructor(
        private _carService: CarService,
        private _subcarService: SubcarService,
        private _companyService: CompanyService,
        private _categoryService: CategoryService,
        private _centerService: CenterService,
        private _technologyService: TechnologyService,
        private _employeeService: EmployeeService,
        private _dictionaryService: DictionaryService
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<any>> {
        const params = {page: 0, size: 100};
        const $cars = getCars(params, this._carService);
        const $subCars = getSubCars(params, this._subcarService);
        const $companies = getCompanies(params, this._companyService);
        const $categories = getCategories(params, this._categoryService);
        const $centers = getCenters(params, this._centerService);
        const $technologies = searchTechnology(params, this._technologyService);
        const $employee = getEmployee(route.params['id'], this._employeeService, {style: 'related'});
        const $employees = searchEmployee({...params, status: true}, this._employeeService);
        const $dictionaries = getDictionaries(params, this._dictionaryService);

        return forkJoin($cars, $subCars, $companies, $categories, $centers, $technologies, $employee, $employees, $dictionaries).pipe(
            map(response => ({
                data: {
                    ...this.filter(response[0], response[1], response[8]),
                    companies: response[2],
                    categories: response[3],
                    center: response[4],
                    technologies: response[5],
                    employee: response[6],
                    employees: response[7]
                }
            })),
            catchError(err => {
                return of({
                    data: null,
                    message: 'Error on test detail resolver, data couldn\'t be fetched',
                    error: err
                });
            })
        );
    }

    filter(cars: IList<ICar>, subcars: IList<ISubcar>, dics: IDictionaryType[]): any{
        const carContent: ICar[] = [];
        let subcarContent: ISubcar[] = [];
        const carDic: IDictionaryType = dics.find(d => d.name === 'CAR');

        carDic.values.forEach(v => {
            const car = cars.content.find(c => c.id === v.value);
            carContent.push(car);
            subcarContent = subcarContent.concat(car.subCars.map(s => ({id: s.id, name: s.name, employees: []})));
        });

        return {
            cars: {
                content: carContent,
                totalPages: cars.totalPages,
                totalElements: cars.totalElements,
                numberOfElements: cars.numberOfElements
            },
            subCars: {
                content: subcarContent,
                totalPages: subcars.totalPages,
                totalElements: subcars.totalElements,
                numberOfElements: subcars.numberOfElements
            }
        }
    }
}
