import {TestBed} from '@angular/core/testing';

import {EmployeeTableResolverService} from './employee-table-resolver.service';

describe('EmployeeTableResolverService', () => {
  let service: EmployeeTableResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployeeTableResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
