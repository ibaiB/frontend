import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {ResolvedData} from '../../../../shared/generics/IResolvedData';
import {getUserInfo} from '../../../../auth/session/application/getUserInfo';
import {getCompanies} from '../../../company/application/getCompanies';
import {SessionService} from '../../../../auth/session/infrastructure/services/session.service';
import {CompanyService} from '../../../company/infrastructure/services/company.service';
import {searchEmployee} from '../../application/searchEmployee';
import {searchEmployeeAux} from '../../../employee-aux/application/searchEmployeeAux';
import {EmployeeAuxService} from '../../../employee-aux/infrastructure/services/employee-aux.service';
import {getCategories} from '../../../category/application/getCategories';
import {CategoryService} from '../../../category/infrastructure/services/category.service';
import {CarService} from '../../../car/infrastructure/services/car.service';
import {getCars} from '../../../car/application/getCars';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {EmployeeService} from '../services/employee.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeeTableResolverService implements Resolve<ResolvedData<any>> {

  constructor(
      private _employeeService: EmployeeService,
      private _employeeAuxService: EmployeeAuxService,
      private _sessionService: SessionService,
      private _companyService: CompanyService,
      private _categoryService: CategoryService,
      private _carsService: CarService,
      private _cookieService: CookieFacadeService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<any>> {
    let params = { page: 0, size: 100 };
    const filtersCacheReports = this._cookieService.get('reportsFilterCache');
    const filtersCachePlanificacion = this._cookieService.get('planificationFilterCache');
    const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('employee-table-config'));
    const tableFiltersConfigAux: ITableFiltersConfig = JSON.parse(this._cookieService.get('employee-aux-table-config'));
    const employeesParams=tableFiltersConfig?.pagination ?? {page: 0, size: 10};
    const orderParams = {...tableFiltersConfig?.order} ?? {};
    const employeesParamsAux=tableFiltersConfigAux?.pagination ?? {page: 0, size: 10};
    const orderParamsAux = {...tableFiltersConfigAux?.order} ?? {};

    const myUser = getUserInfo(this._sessionService);
    const employees = searchEmployee({ ...JSON.parse(filtersCacheReports),...employeesParams,...orderParams, status: true }, this._employeeService);
    const employeesAux = searchEmployeeAux({ ...JSON.parse(filtersCachePlanificacion),...employeesParamsAux,...orderParamsAux}, this._employeeAuxService);
    const companies = getCompanies(params, this._companyService);
    const categories = getCategories(params, this._categoryService);
    const cars = getCars(params, this._carsService);
    
    return forkJoin(myUser, employees, employeesAux, companies, categories, cars)
      .pipe(
        map(response => ({
          data: {
            myUser: response[0],
            employees: response[1],
            employeesAux: response[2],
            companies: response[3],
            categories: response[4],
            cars: response[5],
            filterValueReports: filtersCacheReports,
            filterValuePlanification: filtersCachePlanificacion,
            employeeTableFiltersConfig: tableFiltersConfig,
            employeeAuxTableFiltersConfig: tableFiltersConfigAux
          }
        })),
        catchError(error => {

          return of({
            data: null,
            message: 'Error on account table resolver, data couldn\'t be fetched',
            error: error
          });
        })
      );
  }
}
