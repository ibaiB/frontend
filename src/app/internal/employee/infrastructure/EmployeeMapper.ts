import {IEmployeeRead, IEmployeeReadFull, IEmployeeReadRelated, IEmployeeWhoIsWho, IPublicEmployee} from '../domain/IEmployee';
import {IWebEmployeeWrite} from '../domain/IWebEmployee';
import {IList} from '../../../shared/generics/IList';
import {CarMapper} from '../../car/infrastructure/CarMapper';
import {SubcarMapper} from '../../subcar/infrastructure/SubcarMapper';
import {CategoryMapper} from '../../category/infrastructure/CategoryMapper';
import {CenterMapper} from '../../../shared/entities/center/infrastructure/CenterMapper';
import {CompanyMapper} from '../../company/infrastructure/CompanyMapper';
import {WorkingDayMapper} from '../../working-day/infrastructure/WokingDayMapper';
import {TechnologyMapper} from '../../technology/infrastructure/TechnologyMapper';
import {DocumentMapper} from '../../document/infrastructure/document-mapper';
import {MaterialMapper} from '../../material/infrastructure/MaterialMapper';
import {EmployeeAuxMapper} from '../../employee-aux/infrastructure/employee-aux-mapper';
import {NewEmployeeRequestMapper} from '../../new-employee-request/infrastructure/new-employee-request-mapper';

export class EmployeeMapper {
    mapTo(object: any): IEmployeeRead | IEmployeeReadFull | IEmployeeReadRelated {
        const carMapper = new CarMapper();
        const subcarMapper = new SubcarMapper();
        const categoryMapper = new CategoryMapper();
        const centerMapper = new CenterMapper();
        const employeeMapper = new EmployeeMapper();
        const companyMapper = new CompanyMapper();
        const workingDayMapper = new WorkingDayMapper();
        const technologyMapper = new TechnologyMapper();
        const materialMapper = new MaterialMapper();
        const documentMapper = new DocumentMapper();
        const employeeAuxMapper = new EmployeeAuxMapper();
        const newEmployeeRequestMapper = new NewEmployeeRequestMapper();

        const {
            addressDel,
            addressHab,
            annioEstudios,
            businessPhoneNumber,
            cityDel,
            cityHab,
            cityOfPreference,
            contratable,
            costeTotalEstudios,
            costesPorMes,
            countryDel,
            countryHab,
            englishLevel,
            generalMark,
            hosting,
            hostingCity,
            car,
            subcar,
            internshipEndDate,
            mesInicio,
            numeroMesesAmpliables,
            numeroMesesDePracticas,
            personalEmail,
            personalPhoneNumber,
            personalPhoneNumberFijo,
            postalCodeDel,
            postalCodeHab,
            previsionEndDate,
            profileType,
            provinceDel,
            provinceHab,
            recommendedItinerary,
            studiesEndDate,
            id,
            idNfq,
            name,
            surname,
            dni,
            businessEmail,
            socialSecurityNumber,
            geographicAvailability,
            contractType,
            internshipType,
            bankAccountNumber,
            linkImagen,
            fixedRemuneration,
            variableRemuneration,
            status,
            comercialFlag,
            isCoach,
            isMentor,
            isManager,
            startDate,
            endDate,
            company,
            category,
            center,
            workingDay,
            newEmployeeRequest,
            materials,
            technologies,
            documentos,
            coach,
            mentor,
            manager,
            coachOf,
            managerOf,
            mentorOf,
            employeeAux,
            candidate,
            idChat,
            idFileManager,
            privateIdFileManager
        } = object;

        return {
            addressDel,
            addressHab,
            availableToHire: contratable,
            bankAccountNumber,
            businessEmail,
            businessPhone: businessPhoneNumber,
            businessPhoneNumber,
            car: car ? carMapper.mapTo(car) : car,
            category: category ? categoryMapper.mapTo(category) : category,
            center: center ? centerMapper.mapTo(center) : center,
            cityDel,
            cityHab,
            cityOfPreference,
            coach: coach ? employeeMapper.mapPublic(coach) : coach,
            coachOf: coachOf?.map(employeeMapper.mapPublic) ?? [],
            comercialFlag,
            company: company ? companyMapper.mapSimple(company) : company,
            contractType,
            //TODO:
            costsPerMonth: costesPorMes,
            countryDel,
            countryHab,
            dni,
            documents: documentos?.map(documentMapper.mapTo) ?? [],
            endDate,
            englishLevel,
            expandableMonths: numeroMesesAmpliables,
            fixedRemuneration,
            generalMark,
            geographicAvailability,
            hosting,
            hostingCity,
            id,
            idNfq,
            idCandidate: candidate?.id ?? '',
            idNewEmployeeRequest: newEmployeeRequest?.id ?? '',
            newEmployeeRequest: newEmployeeRequest ? newEmployeeRequestMapper.mapTo(newEmployeeRequest) : null,
            imageLink: linkImagen,
            internshipEndDate,
            internshipMonths: numeroMesesDePracticas,
            internshipType,
            isCoach,
            isManager,
            isMentor,
            landLinePhoneNumber: personalPhoneNumberFijo,
            manager: manager ? employeeMapper.mapPublic(manager) : manager,
            managerOf: managerOf?.map(employeeMapper.mapPublic) ?? [],
            materials: materials?.map(materialMapper.mapTo) ?? [],
            mentor: mentor ? employeeMapper.mapPublic(mentor) : mentor,
            mentorOf: mentorOf?.map(employeeMapper.mapPublic) ?? [],
            name,
            personalEmail,
            personalPhoneNumber,
            postalCodeDel,
            postalCodeHab,
            previsionEndDate,
            profileType,
            provinceDel,
            provinceHab,
            recommendedItinerary,
            socialSecurityNumber,
            startDate,
            startingMonth: mesInicio,
            status,
            studiesEndDate,
            studiesTotalCost: costeTotalEstudios,
            studyYear: annioEstudios,
            subcar: subcar ? subcarMapper.mapTo(subcar) : subcar,
            surname,
            technologies: technologies?.map(technologyMapper.mapTo) ?? [],
            testEndDate: internshipEndDate,
            variableRemuneration,
            workingDay: workingDay ? workingDayMapper.mapTo(workingDay) : workingDay,
            employeeAux: employeeAux ? employeeAuxMapper.mapTo(employeeAux) : employeeAux,
            idChat,
            idFileManager,
            idPrivateFileManager: privateIdFileManager
        };
    }

    mapFrom(object: any): IWebEmployeeWrite {
        const {
            studyYear,
            availableToHire,
            studiesTotalCost,
            costsPerMonth,
            idImageLink,
            startingMonth,
            expandableMonths,
            internshipMonths,
            landLinePhoneNumber,
            ...common
        } = object;

        return {
            annioEstudios: studyYear,
            contratable: availableToHire,
            costeTotalEstudios: studiesTotalCost,
            costesPorMes: costsPerMonth,
            idLinkImagen: idImageLink,
            mesInicio: startingMonth,
            numeroMesesAmpliables: expandableMonths,
            numeroMesesDePracticas: internshipMonths,
            personalPhoneNumberFijo: landLinePhoneNumber,
            ...common
        };
    }

    mapPublic(object: any): IPublicEmployee {
        const centerMapper = new CenterMapper();
        const technologyMapper = new TechnologyMapper();

        const {
            businessEmail,
            businessPhone,
            categoryName,
            center,
            id,
            linkImagen,
            name,
            startDate,
            status,
            surname,
            technologies, 
            linkedin,


        } = object;

        return {
            businessEmail,
            businessPhone,
            categoryName,
            center: center ? centerMapper.mapTo(center) : center,
            id,
            imageLink: linkImagen,
            name,
            startDate,
            status,
            surname,
            technologies: technologies?.map(technologyMapper.mapTo) ?? [], 
            linkedin, 

        };
    }

    mapPublicList(list: IList<any>): IList<IPublicEmployee> {
        const {content, ...common} = list;
        const mapper = new EmployeeMapper();

        return {
            content: content.map(mapper.mapPublic),
            ...common
        };
    }

    mapList(list: IList<any>): IList<IEmployeeRead | IEmployeeReadFull | IEmployeeReadRelated> {
        const {content, ...common} = list;
        const mapper = new EmployeeMapper();

        return {
            content: content.map(mapper.mapTo),
            ...common
        };
    }

    mapRandomEmployeeList(list: any[]): IEmployeeWhoIsWho[] {
        const mapper = new EmployeeMapper();

        return list.map(item => {
            return {
                randomNames: item.randomNames,
                employee: mapper.mapPublic(item.employee)
            };
        });
    }
}
