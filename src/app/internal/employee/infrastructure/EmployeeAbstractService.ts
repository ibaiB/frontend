import {Observable} from 'rxjs';
import {IPublicEmployee} from '../domain/IEmployee';
import {IMyProfile} from '../../../my-staffit/myProfile/domain/IMyProfile';
import {IList} from 'src/app/shared/generics/IList';
import {IWebEmployeeFull, IWebEmployeeRelated, IWebEmployeeSimple} from '../domain/IWebEmployee';
import {IEmployeeTermination} from '../../employee-termination/domain/IEmployeeTermination';

export abstract class EmployeeAbstractService {
    abstract update(employee: any, query?: any): Observable<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>;

    abstract terminate(terminate: IEmployeeTermination, idEmployee: string): Observable<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>;

    abstract me(query?: any): Observable<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>;

    abstract search(query: any): Observable<IList<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>>;

    abstract searchPublic(query: any): Observable<IList<IPublicEmployee>>;

    abstract get(id: string, query?: any): Observable<IWebEmployeeSimple | IWebEmployeeFull | IWebEmployeeRelated>;

    abstract getMyCount(): Observable<any>;

    // ===== MY PROFILE =====
    abstract getMyProfile(query?: any): Observable<IMyProfile>;

    abstract updateMyProfile(myProfile, query?: any): Observable<IMyProfile>;

}
