import {Component, Injectable, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment.prod';
import {successAlert} from '../../shared/error/custom-alerts';

@Injectable({
    providedIn: 'any'
})
export class BillingService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'facturas';

    constructor(private _http: HttpClient) {
    }

    save(bill: string, project: string) {
        const item = {
            idFactura: bill,
            idProyecto: project
        };

        return this._http.put<any>(`${this.BASE_URL}${this.ENDPOINT}`, item, {params: new HttpParams({fromObject: item})});
    }
}

@Component({
    selector: 'app-billing',
    templateUrl: './billing.component.html',
    styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit {

    form: FormGroup;

    constructor(
        private _fb: FormBuilder,
        private _service: BillingService) {
    }

    ngOnInit(): void {
        this.form = this._fb.group({
            bill: ['', Validators.required],
            project: ['', Validators.required]
        });
    }

    submit() {
        const {bill, project} = this.form.value;

        this._service.save(bill, project)
            .subscribe(res => {
                successAlert('Yeah');
            });
    }

}
