export interface IWebCenter {
    id: string;
    idOffice: string;
    location: string;
}
