export interface ICenter {
    id: string;
    idOffice: string;
    location: string;
}
