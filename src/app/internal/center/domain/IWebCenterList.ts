import {IWebCenter} from './IWebCenter';

export interface IWebCenterList {
    content: IWebCenter[];
    totalElements: number;
    numberOfElements: number;
    totalPages: number;
}
