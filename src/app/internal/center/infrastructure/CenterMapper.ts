import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class CenterMapper {
    mapTo(params: any) {
        const {
            id,
            idOffice,
            location,
        } = params;

        return {
            id: id,
            idOffice: idOffice,
            location: location,
        };
    }

    mapFrom(params: any) {
        const {
            id,
            idOffice,
            location,
        } = params;

        return {
            id: id,
            idOffice: idOffice,
            location: location,
        };
    }
}
