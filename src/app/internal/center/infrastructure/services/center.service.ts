import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment.prod';
import {IWebCenterList} from '../../domain/IWebCenterList';
import {CenterAbstractService} from '../CenterAbstractService';

@Injectable({
    providedIn: 'any'
})
export class CenterService extends CenterAbstractService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'centers';

    constructor(private _http: HttpClient) {
        super();
    }

    search(query: any): Observable<IWebCenterList> {
        return this._http.get<IWebCenterList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
    }

}
