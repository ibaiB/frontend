import {Observable} from 'rxjs';

import {IWebCenterList} from '../domain/IWebCenterList';

export abstract class CenterAbstractService {

  abstract search(query: any): Observable<IWebCenterList>;
}
