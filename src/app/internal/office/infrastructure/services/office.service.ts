import {Injectable} from '@angular/core';
import {OfficeAbstractService} from '../OfficeAbstractService';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment.prod';
import {HttpClient, HttpParams} from '@angular/common/http';
import {IWebOfficeList} from '../../domain/IWebOfficeList';

@Injectable({
    providedIn: 'any'
})
export class OfficeService extends OfficeAbstractService {

    private BASE_URL: string = environment.baseUrl;
    private ENDPOINT: string = 'offices';

    constructor(private _http: HttpClient) {
        super();
    }


    search(query: any): Observable<IWebOfficeList> {
        return this._http.get<IWebOfficeList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})})
    }
}
