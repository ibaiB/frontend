import {Observable} from 'rxjs';
import {IWebOfficeList} from '../domain/IWebOfficeList';

export abstract class OfficeAbstractService {
  abstract search(query: any): Observable<IWebOfficeList>;
}
