import {OfficeAbstractService} from '../infrastructure/OfficeAbstractService';

export function getOffices(query: any, service: OfficeAbstractService) {
    return service.search(query);
}
