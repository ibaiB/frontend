import {IWebOffice} from './IWebOffice';

export interface IWebOfficeList {
    content: IWebOffice[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}
