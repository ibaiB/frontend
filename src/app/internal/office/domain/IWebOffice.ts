import {IWebCenter} from '../../center/domain/IWebCenter';

export interface IWebOffice {
    id: string,
    location: string,
    centers: IWebCenter[]
}
