import {ICenter} from '../../center/domain/ICenter';

export interface IOffice {
    id: string,
    location: string,
    centers: ICenter[]
}
