import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class EmployeeTechnologyMapper{
    mapTo(params: any){
        const {
            id,
            order,
            technology
        } = params;

        return {
            id: id,
            order: order,
            technology: technology
        };
    }

    mapFrom(params: any){
        const {
            id,
            order,
            technology
        } = params;

        return {
            id: id,
            order: order,
            technology: technology
        };
    }
}
