import {Observable} from 'rxjs';

import {IWebEmployeeTechnologyList} from '../domain/IWebEmployeeTechnologyList';

export abstract class EmployeeTechnologyAbstractService {
  abstract search(query: any): Observable<IWebEmployeeTechnologyList>;
}
