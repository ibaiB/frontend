import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment.prod';
import {IWebEmployeeTechnologyList} from '../../domain/IWebEmployeeTechnologyList';
import {EmployeeTechnologyAbstractService} from '../EmployeeTechnologyAbstractService';

@Injectable({
  providedIn: 'any'
})
export class EmployeeTechnologyService extends EmployeeTechnologyAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'employee-technologies';

  constructor(
    private http: HttpClient,
  ) {
    super();
  }

  search(query: any): Observable<IWebEmployeeTechnologyList> {
    return this.http.get<IWebEmployeeTechnologyList>(`${this.BASE_URL}${this.ENDPOINT}/search`, { params: new HttpParams({ fromObject: query }) });
  }

}
