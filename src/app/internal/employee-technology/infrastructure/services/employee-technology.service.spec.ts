import {TestBed} from '@angular/core/testing';

import {EmployeeTechnologyService} from './employee-technology.service';

describe('EmployeeTechnologyService', () => {
  let service: EmployeeTechnologyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployeeTechnologyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
