import {IWebEmployeeTechnology} from './IWebEmployeeTechnology';

export interface IWebEmployeeTechnologyList {
    content: IWebEmployeeTechnology[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
