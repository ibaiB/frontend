import {ITechnology} from '../../technology/domain/ITechnology';

export interface IWebEmployeeTechnology {
    id?: number;
    order: number;
    technology: ITechnology;
}
