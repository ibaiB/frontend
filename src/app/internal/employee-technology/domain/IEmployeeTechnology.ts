import {ITechnology} from '../../technology/domain/ITechnology';

export interface IEmployeeTechnology {
    id: number;
    order: number;
    technology: ITechnology;
}
