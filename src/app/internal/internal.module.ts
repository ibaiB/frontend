import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {AngularMaterialModule} from '../shared/material/angular-material.module';
import {SharedModule} from '../shared/shared.module';
import {routing} from '../recruitment/recruitment-routing.module';
import {RoomListComponent} from './room/infrastructure/ng-components/room-list/room-list.component';
import {KnowTheCompanyManagementComponent} from './know-the-company/know-the-company-management/know-the-company-management.component';
import {RoomCardComponent} from './room/infrastructure/ng-components/room-card/room-card.component';
import {LinkEmployeeHistoricDialogComponent} from './employee/infrastructure/ng-components/link-employee-historic-dialog/link-employee-historic-dialog.component';
import {EmployeeTerminationComponent} from './employee-termination/infrastructure/ng-components/employee-termination/employee-termination.component';
import {EmployeeTableComponent} from './employee/infrastructure/ng-components/employee-table/employee-table.component';
import {CreateNewEmployeeRequestComponent} from './new-employee-request/infrastructure/ng-components/create-new-employee-request/create-new-employee-request.component';
import {NewEmployeeRequestTableComponent} from './new-employee-request/infrastructure/ng-components/new-employee-request-management/new-employee-request-table/new-employee-request-table.component';
import {NewEmployeeRequestManagementComponent} from './new-employee-request/infrastructure/ng-components/new-employee-request-management/new-employee-request-management.component';
import {NewEmployeeRequestPipelineComponent} from './new-employee-request/infrastructure/ng-components/new-employee-request-management/new-employee-request-pipeline/new-employee-request-pipeline.component';
import {EmployeeManagementComponent} from './employee/infrastructure/ng-components/employee-management/employee-management.component';
import {BillingComponent} from './billing/billing.component';
import {BookNowRoomDialogComponent} from './room/infrastructure/ng-components/book-now-room-dialog/book-now-room-dialog.component';
import {RoomDetailComponent} from './room/infrastructure/ng-components/room-detail/room-detail.component';
import {AdminCompanyComponent} from './company/infrastructure/ng-components/admin-company/admin-company.component';

@NgModule({
    declarations: [
        RoomListComponent,
        KnowTheCompanyManagementComponent,
        RoomCardComponent,
        LinkEmployeeHistoricDialogComponent,
        EmployeeTerminationComponent,
        EmployeeTableComponent,
        CreateNewEmployeeRequestComponent,
        NewEmployeeRequestTableComponent,
        NewEmployeeRequestManagementComponent,
        NewEmployeeRequestPipelineComponent,
        EmployeeManagementComponent,
        BillingComponent,
        BookNowRoomDialogComponent,
        RoomDetailComponent,
        AdminCompanyComponent
    ],
    exports: [],
    imports: [
        CommonModule,
        routing,
        AngularMaterialModule,
        SharedModule,
    ]
})
export class InternalModule {
}

