import {TestBed} from '@angular/core/testing';

import {EmployeeTerminationService} from './employee-termination.service';

describe('EmployeeTerminationService', () => {
  let service: EmployeeTerminationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployeeTerminationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
