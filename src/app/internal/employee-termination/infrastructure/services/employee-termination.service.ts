import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment.prod';
import {IList} from '../../../../shared/generics/IList';
import {EmployeeTerminationAbstractService} from '../EmployeeTerminationAbstractService';
import {IEmployeeTermination} from '../../domain/IEmployeeTermination';

@Injectable({
  providedIn: 'any'
})
export class EmployeeTerminationService extends EmployeeTerminationAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'employee-terminations';

  constructor(
      private http: HttpClient,
  ) {
    super();
  }

  search(query: any): Observable<IList<any>> {
    return this.http.get<IList<any>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
  }

  findById(id: string, params: any): Observable<IEmployeeTermination> {
    throw Error('METHOD_NOT_IMPLEMENTED');
  }
}
