export class EmployeeTerminationMapper{
    mapTo(params: any){
        const {
            id,
            leavingDate,
            holidaysEnjoyed,
            terminationReason,
            terminationType
        } = params;

        return {
            id: id,
            leavingDate: leavingDate,
            holidaysEnjoyed: holidaysEnjoyed,
            terminationReason: terminationReason,
            terminationType: terminationType
        }
    }
}
