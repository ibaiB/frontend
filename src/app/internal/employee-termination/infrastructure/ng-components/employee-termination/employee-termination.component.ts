import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, NgForm} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSelect} from '@angular/material/select';
import {Observable, ReplaySubject} from 'rxjs';
import {dictionaries} from '../../../../../recruitment/dictionaries';
import {IEmployeeTermination} from '../../../domain/IEmployeeTermination';

@Component({
  selector: 'app-employee-termination',
  templateUrl: './employee-termination.component.html',
  styleUrls: ['./employee-termination.component.scss'],
})
export class EmployeeTerminationComponent
    implements OnInit, OnDestroy {
  isLinear = true;
  searchEmployee = '';
  public findEmp;
  myControl = new FormControl();
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  public confirm = false;
  public textDelete: string;


  employeeTermination: IEmployeeTermination;
  leavingDate: Date;
  holidaysEnjoyed: string;
  terminationType;
  terminationReason: string;


  public employeeFilter: FormControl = new FormControl();
  public employeeCtrl: FormControl = new FormControl();
  public deleteCtrl: FormControl = new FormControl();

  public filteredEmployee: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);

  @ViewChild("formEmployeeTermination") private formEmpTerm: NgForm;
  @ViewChild("formDelete") private formDeleteEmp: NgForm;
  @ViewChild("selectEmployee", { static: true }) selectEmployee: MatSelect;

  constructor(
    @Inject(MAT_DIALOG_DATA) public emp: any,
    public dialogRef: MatDialogRef<any>,
  ) { }

  ngOnDestroy(): void {
        throw new Error("Method not implemented.");
    }

  //  employees: string[] = ['Employee1', 'Employee2', 'Employee3'];
  types = dictionaries.types;
  cities = dictionaries.cities;

  ngOnInit(): void {
  }
}
