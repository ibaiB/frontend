import {Observable} from 'rxjs';
import {IEmployeeTermination} from '../domain/IEmployeeTermination';
import {IList} from '../../../shared/generics/IList';

export abstract class EmployeeTerminationAbstractService {
    abstract findById(id: string, params: any): Observable<IEmployeeTermination>;

    abstract search(params: any): Observable<IList<any>>;
}
