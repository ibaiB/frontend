import {IEmployeeTermination} from './IEmployeeTermination';

export interface IEmployeeTerminationList{
    content: IEmployeeTermination[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}
