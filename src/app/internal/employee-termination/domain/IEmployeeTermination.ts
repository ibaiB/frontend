

export interface IEmployeeTermination{
    id: string,
    holidaysEnjoyed: string,
    leavingDate: Date,
    terminationReason: string,
    terminationType: string,
}