import {EmployeeTerminationAbstractService} from '../infrastructure/EmployeeTerminationAbstractService';

export function getSingleEmployeeTermination(id: string, params: any, service: EmployeeTerminationAbstractService){
    return service.findById(id, params);
}
