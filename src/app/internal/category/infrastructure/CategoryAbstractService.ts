import {Observable} from 'rxjs';
import {IWebCategory} from '../domain/IWebCategory';
import {IWebCategoryList} from '../domain/IWebCategoryList';

export abstract class CategoryAbstractService {
  abstract findById(id: string): Observable<IWebCategory>;

  abstract search(query: any): Observable<IWebCategoryList>;
}
