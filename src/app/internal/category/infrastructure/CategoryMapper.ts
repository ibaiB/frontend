export class CategoryMapper{
    mapTo(params: any){
        const {
            id,
            name,
        } = params;

        return {
            id: id,
            name: name,
        }
    }
}
