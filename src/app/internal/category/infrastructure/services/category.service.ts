import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../../environments/environment';
import {CategoryAbstractService} from '../CategoryAbstractService';
import {IWebCategory} from '../../domain/IWebCategory';
import {IWebCategoryList} from '../../domain/IWebCategoryList';

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends CategoryAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'categories';

  constructor(private http: HttpClient) {
    super();
  }

  findById(id: string): Observable<IWebCategory> {
    return this.http.get<IWebCategory>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  search(query: any): Observable<IWebCategoryList> {
    let httpParams = new HttpParams();
        let paramsKeys = Object.keys(query);
        paramsKeys.forEach(key => {if(query[key]!== undefined) httpParams = httpParams.append(key, query[key].toString())});
    return this.http.get<IWebCategoryList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: httpParams });
  }
}
