import {IWebCategory} from './IWebCategory';

export interface IWebCategoryList {
    content: IWebCategory[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}
