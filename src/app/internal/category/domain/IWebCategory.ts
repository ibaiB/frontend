export interface IWebCategory {
    id: string,
    name: string,
}