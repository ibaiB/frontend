import {ICategory} from './ICategory';

export interface ICategoryList{
    content: ICategory[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number
}
