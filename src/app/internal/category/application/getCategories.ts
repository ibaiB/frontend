import {CategoryAbstractService} from '../infrastructure/CategoryAbstractService';

export function getCategories(params: any, service: CategoryAbstractService){
    return service.search(params);
}
