import {NewEmployeeRequestAbstractService} from '../infrastructure/new-employee-request-abstract-service';
import {NewEmployeeRequestMapper} from '../infrastructure/new-employee-request-mapper';
import {map} from 'rxjs/operators';

export function getNewEmployeeRequest(id: string, service: NewEmployeeRequestAbstractService, mapper: NewEmployeeRequestMapper) {
    return service.get(id).pipe(map(mapper.mapTo));
}
