import {INewEmployeeRequest} from '../domain/INewEmployeeRequest';
import {NewEmployeeRequestAbstractService} from '../infrastructure/new-employee-request-abstract-service';
import {NewEmployeeRequestMapper} from '../infrastructure/new-employee-request-mapper';
import {map} from 'rxjs/operators';

export function createNewEmployeeRequest(newRequest: INewEmployeeRequest, service: NewEmployeeRequestAbstractService, mapper: NewEmployeeRequestMapper) {
    return service.create(mapper.mapFrom(newRequest)).pipe(map(mapper.mapTo));
}
