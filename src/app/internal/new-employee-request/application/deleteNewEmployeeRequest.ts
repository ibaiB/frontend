import {NewEmployeeRequestAbstractService} from '../infrastructure/new-employee-request-abstract-service';

export function deleteNewEmployeeRequest(id: string, service: NewEmployeeRequestAbstractService) {
    return service.delete(id);
}
