import {NewEmployeeRequestAbstractService} from '../infrastructure/new-employee-request-abstract-service';

export function searchNewEmployeeRequest(query: any, service: NewEmployeeRequestAbstractService) {
    return service.search(query);
}
