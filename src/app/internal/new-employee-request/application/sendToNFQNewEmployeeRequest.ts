import {map} from 'rxjs/operators';
import {NewEmployeeRequestAbstractService} from '../infrastructure/new-employee-request-abstract-service';
import {NewEmployeeRequestMapper} from '../infrastructure/new-employee-request-mapper';

export function sendToNFQNewEmployeeRequest(id: string, service: NewEmployeeRequestAbstractService, mapper: NewEmployeeRequestMapper) {
    return service.sendToNFQ(id).pipe(map(mapper.mapTo));
}
