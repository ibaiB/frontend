import {Injectable} from '@angular/core';
import {IWebNewEmployeeRequest} from '../domain/IWebNewEmployeeRequest';

@Injectable({
    providedIn: 'any'
})
export class NewEmployeeRequestTableMapper {
    mapTo(object: any) {
        const {
            id,
            address,
            ageChild1,
            ageChild2,
            ageChild3,
            ageChild4,
            annualSalary,
            bankAccountNumber,
            birthday,
            bonusIncorporation,
            bonusPermanence,
            businessEmail,
            businessPhone,
            changeResidence,
            city,
            civilState,
            comments,
            currentStudiesLevel,
            dni,
            englishLevel,
            hasChilds,
            idCar,
            idCategory,
            idCenter,
            idCompany,
            idOffice,
            idWorkingDay,
            idLinkCVI,
            idLinkDni,
            idLinkEnglishCV,
            idLinkJustificationOfEmployment,
            idLinkOffer,
            idLinkSS,
            idLinkSocialSecurityCertificate,
            idLinkSpanishCV,
            linkedin,
            mensualities,
            monthBonusIncoporation,
            monthBonusPermanence,
            name,
            nationality,
            ongoingStudies,
            personalEmail,
            personalPhone,
            postalCode,
            province,
            socialSecurityNumber,
            startDate,
            surname,
            technologyGroup,
            variableSalary,
            category,
            center,
            company,
            office,
            subcar,
        } = object;

        return {
            id: id,
            address: address,
            ageChild1: ageChild1,
            ageChild2: ageChild2,
            ageChild3: ageChild3,
            ageChild4: ageChild4,
            annualSalary: annualSalary,
            bankAccountNumber: bankAccountNumber,
            birthday: birthday,
            bonusIncorporation: bonusIncorporation,
            bonusPermanence: bonusPermanence,
            businessEmail: businessEmail,
            businessPhone: businessPhone,
            changeResidence: changeResidence,
            city: city,
            civilState: civilState,
            comments: comments,
            currentStudiesLevel: currentStudiesLevel,
            dni: dni,
            englishLevel: englishLevel,
            hasChildren: hasChilds,
            idCar: idCar,
            idCategory: idCategory,
            idCenter: idCenter,
            idCompany: idCompany,
            idOffice: idOffice,
            idWorkingDay: idWorkingDay,
            idLinkCVI,
            idLinkDni,
            idLinkEnglishCV,
            idLinkJustificationOfEmployment,
            idLinkOffer,
            idLinkSS,
            idLinkSocialSecurityCertificate,
            idLinkSpanishCV,
            linkedin,
            monthlyPayments: mensualities,
            monthBonusIncorporation: monthBonusIncoporation,
            monthBonusPermanence,
            name,
            nationality,
            ongoingStudies,
            personalEmail,
            personalPhone,
            postalCode,
            province,
            socialSecurityNumber,
            startDate,
            surname,
            technologyGroup,
            variableSalary,
            category,
            center,
            company,
            office,
            subcar
        }
    }

    mapFrom(object: any): IWebNewEmployeeRequest {
        const {
            id,
            address,
            ageChild1,
            ageChild2,
            ageChild3,
            ageChild4,
            annualSalary,
            bankAccountNumber,
            birthday,
            bonusIncorporation,
            bonusPermanence,
            businessEmail,
            businessPhone,
            changeResidence,
            city,
            civilState,
            comments,
            currentStudiesLevel,
            dni,
            englishLevel,
            hasChilds,
            idSubcar,
            idCategory,
            idCenter,
            idCompany,
            idOffice,
            idWorkingDay,
            idLinkCVI,
            idLinkDni,
            idLinkEnglishCV,
            idLinkJustificationOfEmployment,
            idLinkOffer,
            idLinkSS,
            idLinkSocialSecurityCertificate,
            idLinkSpanishCV,
            linkedin,
            mensualities,
            monthBonusIncoporation,
            monthBonusPermanence,
            name,
            nationality,
            ongoingStudies,
            personalEmail,
            personalPhone,
            postalCode,
            province,
            socialSecurityNumber,
            startDate,
            surname,
            technologyGroup,
            variableSalary,
            referenced,
            typeOfContract
        } = object;

        return {
            referenced: referenced,
            typeOfContract: typeOfContract,
            id,
            address,
            ageChild1,
            ageChild2,
            ageChild3,
            ageChild4,
            annualSalary,
            bankAccountNumber,
            birthday,
            bonusIncorporation,
            bonusPermanence,
            businessEmail,
            businessPhone,
            changeResidence,
            city,
            civilState,
            comments,
            currentStudiesLevel,
            dni,
            englishLevel,
            hasChilds,
            idSubcar: idSubcar,
            idCategory,
            idCenter,
            idCompany,
            idOffice,
            idWorkingDay,
            idLinkCVI,
            idLinkDni,
            idLinkEnglishCV,
            idLinkJustificationOfEmployment,
            idLinkOffer,
            idLinkSS,
            idLinkSocialSecurityCertificate,
            idLinkSpanishCV,
            linkedin,
            mensualities,
            monthBonusIncoporation,
            monthBonusPermanence,
            name,
            nationality,
            ongoingStudies,
            personalEmail,
            personalPhone,
            postalCode,
            province,
            socialSecurityNumber,
            startDate,
            surname,
            technologyGroup,
            variableSalary
        }
    }
}
