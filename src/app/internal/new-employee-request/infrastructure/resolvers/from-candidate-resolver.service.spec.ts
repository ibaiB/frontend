import {TestBed} from '@angular/core/testing';

import {FromCandidateResolverService} from './from-candidate-resolver.service';

describe('FromCandidateResolverService', () => {
  let service: FromCandidateResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FromCandidateResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
