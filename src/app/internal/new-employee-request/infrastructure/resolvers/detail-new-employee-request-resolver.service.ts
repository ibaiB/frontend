import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {NewEmployeeRequestMapper} from 'src/app/internal/new-employee-request/infrastructure/new-employee-request-mapper';
import {ResolvedData} from 'src/app/shared/generics/IResolvedData';
import {getCars} from '../../../car/application/getCars';
import {getCategories} from '../../../category/application/getCategories';
import {getCenters} from '../../../../shared/entities/center/application/getCenters';
import {getCompanies} from '../../../company/application/getCompanies';
import {getSubCars} from '../../../subcar/application/getSubCars';
import {CarService} from '../../../car/infrastructure/services/car.service';
import {CategoryService} from '../../../category/infrastructure/services/category.service';
import {CenterService} from '../../../../shared/entities/center/infrastructure/service/center.service';
import {CompanyService} from '../../../company/infrastructure/services/company.service';
import {SubcarService} from '../../../subcar/infrastructure/services/subcar.service';
import {getNewEmployeeRequest} from '../../application/getNewEmployeeRequest';
import {INewEmployeeRequestDetailData} from '../../domain/INewEmployeeRequestDetailData';
import {getOffices} from '../../../office/application/getOffices';
import {getWorkingDays} from '../../../working-day/application/getWorkingDays';
import {NewEmployeeRequestService} from '../services/new-employee-request.service';
import {OfficeService} from '../../../office/infrastructure/services/office.service';
import {WorkingDayService} from '../../../working-day/infrastructure/services/working-day.service';


@Injectable({
  providedIn: 'any'
})
export class DetailNewEmployeeRequestResolverService implements Resolve<ResolvedData<INewEmployeeRequestDetailData>> {

  constructor(
      private _carService: CarService,
      private _categoryService: CategoryService,
    private _centerService: CenterService,
    private _companyService: CompanyService,
    private _officeService: OfficeService,
    private _workingDayService: WorkingDayService,
    private _newEmployeeRequestService: NewEmployeeRequestService,
    private _newEmployeeRequestMapper: NewEmployeeRequestMapper,
    private _subcarService: SubcarService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<INewEmployeeRequestDetailData>> {
    
    let params = { page: 0, size: 100 };
    const $cars = getCars(params, this._carService);
    const $categories = getCategories(params, this._categoryService);
    const $centers = getCenters(params, this._centerService);
    const $companies = getCompanies(params, this._companyService);
    const $offices = getOffices(params, this._officeService);
    const $workingDays = getWorkingDays(params, this._workingDayService);
    const $subcars = getSubCars(params, this._subcarService);
  
    const $newEmp = route.params['id'] ? getNewEmployeeRequest(route.params['id'], this._newEmployeeRequestService, this._newEmployeeRequestMapper) : null;
    const fork: Observable<any> = !route.params['id'] 
    ? forkJoin($cars, $categories, $centers, $companies, $offices, $workingDays, $subcars) 
    : forkJoin($cars, $categories, $centers, $companies, $offices, $workingDays, $subcars, $newEmp)
    
    return fork
      .pipe(
        map(response => ({
          data: {
            cars: response[0],
            categories: response[1],
            centers: response[2],
            companies: response[3],
            offices: response[4],
            workingDays: response[5],
            subcars: response[6],
            newEmployeeRequest: response[7] ?? null,
          }
        })),
        catchError(error => {
          return of({
            data: null,
            error: error,
            message: 'Error on create new request resolver, data couldn\'t be fetched'
          });
        })
      );
  }
}
