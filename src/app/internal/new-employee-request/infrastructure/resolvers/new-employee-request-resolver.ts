import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {catchError, map, tap} from 'rxjs/operators';
import {forkJoin, Observable, of} from 'rxjs';

import {DictionaryService} from '../../../../shared/entities/dictionary/infrastructure/services/dictionary.service';
import {SessionService} from '../../../../auth/session/infrastructure/services/session.service';
import {getUserInfo} from '../../../../auth/session/application/getUserInfo';

import {NewEmployeeRequestService} from '../services/new-employee-request.service';
import {INewEmployeeRequestManagementResolved} from '../../domain/INewEmployeeRequestManagementResolved';
import {searchNewEmployeeRequest} from '../../application/searchNewEmployeeRequest';
import {getCars} from 'src/app/internal/car/application/getCars';
import {getCenters} from 'src/app/shared/entities/center/application/getCenters';
import {CarService} from 'src/app/internal/car/infrastructure/services/car.service';
import {CenterService} from 'src/app/shared/entities/center/infrastructure/service/center.service';
import {CenterMapper} from '../../../center/infrastructure/CenterMapper';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import { getSubCars } from 'src/app/internal/subcar/application/getSubCars';
import { SubcarService } from 'src/app/internal/subcar/infrastructure/services/subcar.service';


@Injectable({
    providedIn: 'any'
})
export class NewEmployeeRequestResolverService implements Resolve<INewEmployeeRequestManagementResolved> {

    constructor(private _sessionService: SessionService,
                private _dictionaryService: DictionaryService,
                private _newEmployeeRequestService: NewEmployeeRequestService,
                private _subcarService: SubcarService,
                private _centerService: CenterService,
                private _centerMapper: CenterMapper,
                private _cookieService : CookieFacadeService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): INewEmployeeRequestManagementResolved | Observable<INewEmployeeRequestManagementResolved> | Promise<INewEmployeeRequestManagementResolved> {

        const params = {page: 0, size: 100};
        const filtersCache = this._cookieService.get('requestFilterCache');
        const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('new-employee-request-table-config'));
        const orderParams = {...tableFiltersConfig?.order} ?? {};

        const $myUser = getUserInfo(this._sessionService);
        const $newEmployeeRequests = searchNewEmployeeRequest({...JSON.parse(filtersCache), ...params,...orderParams}, this._newEmployeeRequestService);
        const $subcars = getSubCars(params, this._subcarService);
        const $centers = getCenters(params, this._centerService);

        return forkJoin($myUser, $newEmployeeRequests, $subcars, $centers)
        .pipe(
            tap(response => response[3].content = response[3].content.map(this._centerMapper.mapTo)),
            map(response => ({
                tableData: {
                    myUser: response[0],
                    newEmployeeRequests: response[1],
                    subcars: response[2],
                    centers: response[3],
                    filtersValue:filtersCache,
                    tableFiltersConfig: tableFiltersConfig
                }
            })),
            catchError(error => {
                return of({tableData: null, error: {message: 'Error on opportunity table resolver, data couldn\'t be fetched', error}});
            }));
    }


}

