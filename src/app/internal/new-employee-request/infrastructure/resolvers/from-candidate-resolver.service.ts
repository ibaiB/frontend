import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {getCars} from 'src/app/internal/car/application/getCars';
import {getCategories} from 'src/app/internal/category/application/getCategories';
import {getCenters} from 'src/app/shared/entities/center/application/getCenters';
import {getCompanies} from 'src/app/internal/company/application/getCompanies';
import {getSubCars} from 'src/app/internal/subcar/application/getSubCars';
import {ResolvedData} from 'src/app/shared/generics/IResolvedData';
import {CarService} from 'src/app/internal/car/infrastructure/services/car.service';
import {CategoryService} from 'src/app/internal/category/infrastructure/services/category.service';
import {CenterService} from 'src/app/shared/entities/center/infrastructure/service/center.service';
import {CompanyService} from 'src/app/internal/company/infrastructure/services/company.service';
import {SubcarService} from 'src/app/internal/subcar/infrastructure/services/subcar.service';
import {getSingleCandidate} from '../../../../recruitment/candidate/application/getSingleCandidate';
import {CandidateMapper} from '../../../../recruitment/candidate/infrastructure/CandidateMapper';
import {getOffices} from '../../../office/application/getOffices';
import {getWorkingDays} from '../../../working-day/application/getWorkingDays';
import {CandidateService} from '../../../../recruitment/candidate/infrastructure/services/candidate.service';
import {OfficeService} from '../../../office/infrastructure/services/office.service';
import {WorkingDayService} from '../../../working-day/infrastructure/services/working-day.service';

@Injectable({
  providedIn: 'any'
})
export class FromCandidateResolverService implements Resolve<ResolvedData<any>> {

  constructor(
      private _carService: CarService,
      private _categoryService: CategoryService,
      private _centerService: CenterService,
    private _companyService: CompanyService,
    private _officeService: OfficeService,
    private _workingDayService: WorkingDayService,
    private _subcarService: SubcarService,
    private _candidateMapper: CandidateMapper,
    private _candidateService: CandidateService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResolvedData<any>> {
    
    let params = { page: 0, size: 100 };
    const $cars = getCars(params, this._carService);
    const $categories = getCategories(params, this._categoryService);
    const $centers = getCenters(params, this._centerService);
    const $companies = getCompanies(params, this._companyService);
    const $offices = getOffices(params, this._officeService);
    const $workingDays = getWorkingDays(params, this._workingDayService);
    const $subcars = getSubCars(params, this._subcarService);
    const $candidate = getSingleCandidate(route.params['id'], this._candidateService);
    
    return forkJoin($cars, $categories, $centers, $companies, $offices, $workingDays, $subcars, $candidate)
      .pipe(
        map(response => ({
          data: {
            cars: response[0],
            categories: response[1],
            centers: response[2],
            companies: response[3],
            offices: response[4],
            workingDays: response[5],
            subcars: response[6],
            candidate: response[7],
          }
        })),
        catchError(error => {
          return of({
            data: null,
            error: error,
            message: 'Error on create new request resolver, data couldn\'t be fetched'
          });
        })
      );
  }
}
