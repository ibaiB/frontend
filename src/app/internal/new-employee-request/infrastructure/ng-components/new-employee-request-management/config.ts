import {ITableConfig,} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
  multiSelect: false,
  actions: [
    {name: 'Send to NFQ', icon: 'send'},
    //TODO: disabled until we can apply conditionals to rows
    //{name: 'Delete', icon: 'delete'}
  ],
  columns: [
    {
      name: 'Id',
      prop: 'id',
      shown: true,
      route: './app/internal/employee/new-requests/detail/',
      routeId: 'id',
      cellClass: 'id-table-column',
    },
    {
      name: "Name",
      prop: "name",
      shown: true,
    },
    {
      name: "Surname",
      prop: "surname",
      shown: true,
    },
    {
      name: "Business Email",
      prop: "businessEmail",
      shown: true,
    },
    {
      name: "SubCar",
      prop: "subcar",
      shown: true,
    },
    {
      name: "Center",
      prop: "center",
      shown: true,
    },
    {
      name: "DNI",
      prop: "dni",
      shown: true,
    },
    {
      name: "Personal Phone",
      prop: "personalPhone",
      shown: false,
    },
    {
      name: "Personal Email",
      prop: "personalEmail",
      shown: false,
    },
    {
      name: "State",
      prop: "state",
      shown: false,
    },
  ],
};
