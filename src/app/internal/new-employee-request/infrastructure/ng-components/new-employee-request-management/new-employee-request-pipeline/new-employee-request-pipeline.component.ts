import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {CdkDragDrop, transferArrayItem} from '@angular/cdk/drag-drop';
import {INewEmployeeRequest} from '../../../../domain/INewEmployeeRequest';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-new-employee-request-pipeline',
  templateUrl: './new-employee-request-pipeline.component.html',
  styleUrls: ['./new-employee-request-pipeline.component.scss']
})
export class NewEmployeeRequestPipelineComponent implements OnChanges {

  // @Input() data: any;
  @Output() actions = new EventEmitter<any>();
  @Input() newEmployeeRequests: INewEmployeeRequest[];
  @Output() stateChangedEmp = new EventEmitter<any>();
  @Output() sendToNFQ = new EventEmitter<any>();
  @Output() deleteRequest = new EventEmitter<any>();

  loading = true;

  columns: {
    total: number,
    emps: number,
    name: string,
    order: number,
    newEmployeeRequests: INewEmployeeRequest[]
  }[] = [];

  constructor(private _router: Router, private _route: ActivatedRoute) {
  }

  drop(event: CdkDragDrop<INewEmployeeRequest[], any>) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(
          event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex
      );

      const updated = event.container.data[event.currentIndex];
      updated['state'] = event.container.id;
      console.log('emite?', event.container.id);
      if (event.container.id === "Lanzado") {
        this.sendToNFQ.emit(event.container.data[event.currentIndex].id);
      }else{
        this.stateChangedEmp.emit(updated);
      }
    }
  }

  private _filterStates() {
    let newENL: INewEmployeeRequest[] = this.newEmployeeRequests.filter(e => e['state'] === 'Nuevo' && e['lanzar'] === false);
    let newEL: INewEmployeeRequest[] = this.newEmployeeRequests.filter(e => e['state'] === 'Nuevo' && e['lanzar'] === true);
    let incNS: INewEmployeeRequest[] = this.newEmployeeRequests.filter(e => e['state'] === 'incidencias' && e['incidenciasSolucionadas'] === false);
    let incS: INewEmployeeRequest[] = this.newEmployeeRequests.filter(e => e['state'] === 'incidencias' && e['incidenciasSolucionadas'] === true);
    let ep: INewEmployeeRequest[] = this.newEmployeeRequests.filter(e => e['state'] === 'enProceso');
    let fin: INewEmployeeRequest[] = this.newEmployeeRequests.filter(e => e['state'] === 'finalizado');

    this.loading = false;

    return [
      {
        total: newENL.length,
        emps: newENL.length,
        order: 1,
        name: 'Sin lanzar',
        newEmployeeRequests: newENL
      },
      {
        total: newEL.length,
        emps: newEL.length,
        order: 2,
        name: 'Lanzado',
        newEmployeeRequests: newEL
      },
      {
        total: ep.length,
        emps: ep.length,
        order: 3,
        name: 'En Proceso',
        newEmployeeRequests: ep
      },
      {
        total: fin.length,
        emps: fin.length,
        order: 4,
        name: 'Finalizado',
        newEmployeeRequests: fin
      },
      {
        total: incNS.length,
        emps: incNS.length,
        order: 5,
        name: 'Inc. sin resolver',
        newEmployeeRequests: incNS
      },
      {
        total: incS.length,
        emps: incS.length,
        order: 6,
        name: 'Incidencia resuelta',
        newEmployeeRequests: incS
      } 
    ];
  }

  openDetailRequest(request: INewEmployeeRequest) {
    this._router.navigate(['detail/' + request.id], {relativeTo: this._route});
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.columns = this._filterStates();
  }

  isLate(date: string): boolean {
    return new Date(date) <= new Date();
  }

  delete(item: INewEmployeeRequest) {
    this.deleteRequest.emit(item);
  }
}
