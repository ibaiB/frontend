import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewEmployeeRequestPipelineComponent} from './new-employee-request-pipeline.component';

describe('NewEmployeeRequestPipelineComponent', () => {
  let component: NewEmployeeRequestPipelineComponent;
  let fixture: ComponentFixture<NewEmployeeRequestPipelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewEmployeeRequestPipelineComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEmployeeRequestPipelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
