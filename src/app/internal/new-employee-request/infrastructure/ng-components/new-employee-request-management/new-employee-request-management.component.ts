import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {updateNewEmployeeRequest} from 'src/app/internal/new-employee-request/application/updateNewEmployeeRequest';
import {INewEmployeeRequest} from 'src/app/internal/new-employee-request/domain/INewEmployeeRequest';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {IList} from 'src/app/shared/generics/IList';
import {errorAlert, successAlert} from '../../../../../shared/error/custom-alerts';
import {searchNewEmployeeRequest} from '../../../application/searchNewEmployeeRequest';
import {sendToNFQNewEmployeeRequest} from '../../../application/sendToNFQNewEmployeeRequest';
import {INewEmployeeRequestManagementData} from '../../../domain/INewEmployeeRequestManagementData';
import {INewEmployeeRequestManagementResolved} from '../../../domain/INewEmployeeRequestManagementResolved';
import {NewEmployeeRequestMapper} from '../../new-employee-request-mapper';
import {NewEmployeeRequestService} from '../../services/new-employee-request.service';
import {table} from './config';
import {NewEmployeeRequestTableComponent} from './new-employee-request-table/new-employee-request-table.component';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';
import Swal from 'sweetalert2';
import {deleteNewEmployeeRequest} from '../../../application/deleteNewEmployeeRequest';


@Component({
    selector: 'app-new-employee-request-management',
    templateUrl: './new-employee-request-management.component.html',
    styleUrls: ['./new-employee-request-management.component.scss']
})
export class NewEmployeeRequestManagementComponent implements OnInit {

    private _resolved: INewEmployeeRequestManagementResolved;
    resolvedData: INewEmployeeRequestManagementData;

    tableConfig: ITableConfig = table;
    tableData: IList<INewEmployeeRequest>;
    pipelineData: INewEmployeeRequest[];

    queryPagination = {page: 0, size: 10};
    queryOrder = {orderField: '', order: ''};
    queryFilters = {};

    filters: IAuto[];
    size: number = 10;
    data: any = [];

    loading: boolean = true;
    configVisibility: boolean = false;

    @ViewChild('newEmployeeRequestTable') newEmployeeRequestTable: NewEmployeeRequestTableComponent;

    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        private _newEmployeeRequestMapper: NewEmployeeRequestMapper,
        private _newEmployeeRequestService: NewEmployeeRequestService,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit(): void {
        this._resolved = this._route.snapshot.data['response'];

        this._buildFilters();
        parseTableFiltersConfig(this._resolved.tableData.tableFiltersConfig, this.tableConfig, this.filters);
        this.queryFilters = this._resolved.tableData.filtersValue ? this._clean((JSON.parse(this._resolved.tableData.filtersValue))) : {};
        this.tableConfig.pagination = this._resolved.tableData.tableFiltersConfig?.pagination ?? {page: 0, size: 10};
        this.queryPagination = this.tableConfig.pagination;
        this.tableConfig.order = this._resolved.tableData.tableFiltersConfig?.order ?? {orderField: '', order: ''};
        this.queryOrder = {...this.tableConfig.order};
        this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
            Object.entries(this._newEmployeeRequestMapper.mapTo({[this.tableConfig.order.orderField]: ''})).find(entry => entry[1] === '')[0] : '';
        this.tableData = {...this._resolved.tableData.newEmployeeRequests};
        this.resolvedData = this._resolved.tableData;
        this.tableData.content = this.tableData.content.slice(this.queryPagination.page * this.queryPagination.size, (this.queryPagination.page * this.queryPagination.size) + this.queryPagination.size);
        this.pipelineData = this._resolved.tableData.newEmployeeRequests.content;

        this.loading = false;
        this.configFilterValue();
    }

    configFilterValue() {
        let queryFilterAux = this._resolved.tableData.filtersValue ? this._clean(this._newEmployeeRequestMapper.mapTo(JSON.parse(this._resolved.tableData.filtersValue))) : {};

        queryFilterAux['subcar'] = queryFilterAux['subcar'] ? this._resolved.tableData.subcars.content.find(subcar => subcar.id === queryFilterAux['subcar']).name : '';
        queryFilterAux['center'] = queryFilterAux['center'] ? this._resolved.tableData.centers.content.find(center => center.id === queryFilterAux['center']).location : '';
        this.filters.forEach(filter => {
            filter.defaultValue = queryFilterAux[this.getFilterProp(filter)];
            filter.value = queryFilterAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    goToNewRequest() {
        this._router.navigate(['./new'], {relativeTo: this._route});
    }

    pipelineBackSearch() {
        searchNewEmployeeRequest({size: 100, ...this.queryFilters}, this._newEmployeeRequestService).subscribe(data =>
                this.pipelineData = data.content,
            error => {
                console.error('Error on back search', error);
                const {error: {message, id}} = error;
                errorAlert(`Something went wrong!`, message, id);
            }
        );
    }

    tableBackSearch() {
        searchNewEmployeeRequest({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._newEmployeeRequestService).subscribe(data =>
                this.tableData = data,
            error => {
                console.error('Error on back search', error);
                const {error: {message, id}} = error;
                errorAlert(`Something went wrong!`, message, id);
            }
        );
    }

    filtersChanged(params?: any) {
        if (params && Object.keys(params).length !== 0) {
            const switched = NewEmployeeRequestManagementComponent._querySwitcher(params, this._resolved);
            this.queryFilters = switched;
        } else {
            this.queryFilters = {};
        }
        this._cookieFacade.save('requestFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('new-employee-request-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.newEmployeeRequestTable.resetPageIndex();
        this.pipelineBackSearch();
        this.tableBackSearch();
    }

    paginationChanged(event: any) {
        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('new-employee-request-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.tableBackSearch();
    }

    orderChanged(event: any) {
        let aux: any = {[event.orderField]: ''};
        aux = this._newEmployeeRequestMapper.mapFrom(aux);

        this.queryOrder.orderField = event.orderField;
        this.queryOrder.order = event.order;

        this.queryPagination.page = event.page;
        this.queryPagination.size = event.size;
        this._cookieFacade.save('new-employee-request-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

        this.tableBackSearch();
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('new-employee-request-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    private static _querySwitcher(query: any, _resolved) {

        const subcar = _resolved.tableData.subcars.content.find(subcar => subcar.name === query.subcar);
        if (subcar) {
            query.subcar = subcar.id;
        }

        const center = _resolved.tableData.centers.content.find(center => center.location === query.center);
        if (center) {
            query.center = center.id;
        }

        return query;
    }

    private _buildFilters() {
        this.filters = [
            {
                options: [],
                prop: 'id',
                retProp: '',
                type: 'input',
                appearance: 'standard',
                class: 'input',
                label: 'Id',
                placeholder: 'Id',
                shown: true
            },
            {
                options: [],
                prop: 'name',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Name',
                placeholder: 'Name',
                shown: true
            },
            {
                options: [],
                prop: 'surname',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Surname',
                placeholder: 'Surname',
                shown: true
            },
            {
                options: [],
                prop: 'businessEmail',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Business Email',
                placeholder: 'Business Email',
                shown: true
            },
            {
                options: this._resolved.tableData.subcars.content.map(subcar => subcar.name),
                prop: '',
                retProp: '',
                searchValue: 'subcar',
                type: 'autocomplete',
                appearance: 'outline',
                class: 'autocomplete',
                label: 'SubCar',
                placeholder: '',
                shown: false
            },
            {
                options: this._resolved.tableData.centers.content.map(c => c.location),
                prop: '',
                retProp: '',
                searchValue: 'center',
                type: 'autocomplete',
                appearance: 'outline',
                class: 'autocomplete',
                label: 'Center',
                placeholder: '',
                shown: false
            },
            {
                options: [],
                prop: 'dni',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'DNI',
                placeholder: 'DNI',
                shown: false
            },
            {
                options: [],
                prop: 'personalPhone',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Personal Phone',
                placeholder: 'Personal Phone',
                shown: false
            },
            {
                options: [],
                prop: 'personalEmail',
                retProp: '',
                type: 'input',
                appearance: 'outline',
                class: 'input',
                label: 'Personal Email',
                placeholder: 'Personal Email',
                shown: false
            }
        ];
    }

    editEmployee(employee: any) {
        updateNewEmployeeRequest(employee, this._newEmployeeRequestService, this._newEmployeeRequestMapper)
            .subscribe(returned => {
                this.tableBackSearch();
                this.pipelineBackSearch();
            });
    }

    sendToNfq(idNewEmployeeRequest: string) {
        sendToNFQNewEmployeeRequest(
            idNewEmployeeRequest,
            this._newEmployeeRequestService,
            this._newEmployeeRequestMapper
        ).subscribe(res => {
            successAlert('Correctly sent to NFQ');
        });
    }

    delete(item: INewEmployeeRequest) {
        Swal.fire({
            icon: 'warning',
            title: 'Warning',
            text: `You are about to delete ${item.name} ${item.surname} employee request. Are you sure?`,
            confirmButtonColor: '#db5e5e',
            showCancelButton: true,
            cancelButtonColor: '#8C8C8C'
        }).then((res) => {
            if (res.isConfirmed) {
                deleteNewEmployeeRequest(item.id, this._newEmployeeRequestService)
                    .subscribe(() => {
                            successAlert('New employee request deleted');

                            this.data = {
                                content: this.data.content.filter(i => i.id !== item.id),
                                totalPages: this.data.totalPages,
                                totalElements: this.data.totalElements--,
                                numberOfElements: this.data.numberOfElements
                            };
                        }
                    );
            }
        });
    }
}
