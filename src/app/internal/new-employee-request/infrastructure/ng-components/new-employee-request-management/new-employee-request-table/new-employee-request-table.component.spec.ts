import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewEmployeeRequestTableComponent} from './new-employee-request-table.component';

describe('NewEmployeeRequestTableComponent', () => {
  let component: NewEmployeeRequestTableComponent;
  let fixture: ComponentFixture<NewEmployeeRequestTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewEmployeeRequestTableComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEmployeeRequestTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
