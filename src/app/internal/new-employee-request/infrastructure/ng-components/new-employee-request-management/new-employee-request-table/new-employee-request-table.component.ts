import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PageEvent} from '@angular/material/paginator';
import {IDynamicColumn} from '../../../../../../shared/custom-mat-elements/dynamic-table/IDynamicColumn';
import {INewEmployeeRequest} from '../../../../domain/INewEmployeeRequest';
import {IList} from 'src/app/shared/generics/IList';
import {DynamicTableMarkIiComponent} from 'src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {ITableConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {INewEmployeeRequestManagementData} from 'src/app/internal/new-employee-request/domain/INewEmployeeRequestManagementData';

@Component({
    selector: 'app-new-employee-request-table',
    templateUrl: './new-employee-request-table.component.html',
    styleUrls: ['./new-employee-request-table.component.scss']
})
export class NewEmployeeRequestTableComponent implements OnInit, OnChanges {

    @Input() config: ITableConfig;
    @Input() dataResolved: INewEmployeeRequestManagementData;
    @Input() data: IList<INewEmployeeRequest>;

    @Output() pagination = new EventEmitter<any>();
    @Output() order = new EventEmitter<any>();

    @Output() modRequest = new EventEmitter<any>();
    @Output() pageSizeChanged = new EventEmitter<number>();
    @Output() sendToNFQ = new EventEmitter<any>();
    @Output() deleteRequest = new EventEmitter<any>();

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    columns: IDynamicColumn[];

    data2: any[] = [];

    constructor(
        private _router: Router,
        private _route: ActivatedRoute
    ) {
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.data.content = this.data.content.map(e => this._extend(e, this.dataResolved));
    }

    private _extend(e: INewEmployeeRequest, resolved: INewEmployeeRequestManagementData): any {
        let newState = '';
        if (e.state === 'Nuevo' && e.lanzar === false) {
            newState = 'sinLanzar';
        }
        if (e.state === 'Nuevo' && e.lanzar === true) {
            newState = 'lanzado';
        }
        if (e.state === 'enProceso' && e.incidenciasSolucionadas === false) {
            newState = 'incidenciaSinResolver';
        }
        if (e.state === 'enProceso' && e.incidenciasSolucionadas === true) {
            newState = 'incidenciaResuelta';
        }
        if (e.state === 'enProceso') {
            newState = 'enProceso';
        }
        if (e.state === 'finalizado') {
            newState = 'finalizado';
        }

        return {
            ...e,
            subcar: resolved?.subcars.content.find(subcar => subcar.id === e.idSubcar)?.name ?? '',
            center: resolved?.centers.content.find(center => center.id === e.idCenter)?.location ?? '',
            state: newState
        };
    }

    setSize(event: PageEvent) {

    }

    doAction(event: { action: string, item: INewEmployeeRequest }) {
        if (event.action === 'Send to NFQ') {
            this.sendToNFQ.emit(event.item.id);
        }

        if (event.action === 'Delete') {
            this.deleteRequest.emit(event.item);
        }

    }

    resetPageIndex() {
        this.dynamicTable.resetPageIndex();
    }

    paginationChanged(event: any) {
        this.pagination.emit(event);
    }

    orderChanged(event: any) {
        this.order.emit(event);
    }
}
