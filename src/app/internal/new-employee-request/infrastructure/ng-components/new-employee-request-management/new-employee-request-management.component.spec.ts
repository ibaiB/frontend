import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewEmployeeRequestManagementComponent} from './new-employee-request-management.component';

describe('NewEmployeeRequestManagementComponent', () => {
  let component: NewEmployeeRequestManagementComponent;
  let fixture: ComponentFixture<NewEmployeeRequestManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewEmployeeRequestManagementComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEmployeeRequestManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
