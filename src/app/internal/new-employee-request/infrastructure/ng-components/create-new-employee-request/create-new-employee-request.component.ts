import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ICenter} from 'src/app/internal/center/domain/ICenter';
import {updateNewEmployeeRequest} from 'src/app/internal/new-employee-request/application/updateNewEmployeeRequest';
import {IOffice} from 'src/app/internal/office/domain/IOffice';
import {ICompany} from 'src/app/internal/company/domain/ICompany';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {dictionaries} from '../../../../../recruitment/dictionaries';
import {createNewEmployeeRequest} from '../../../application/createNewEmployeeRequest';
import {INewEmployeeRequest} from '../../../domain/INewEmployeeRequest';
import {NewEmployeeRequestMapper} from '../../new-employee-request-mapper';
import {NewEmployeeRequestService} from '../../services/new-employee-request.service';
import {ICandidate} from '../../../../../recruitment/candidate/domain/ICandidate';
import Swal from 'sweetalert2';
import {EmployeeService} from '../../../../employee/infrastructure/services/employee.service';
import {debounceTime} from 'rxjs/operators';
import {FileService} from '../../../../../shared/file/infrastructure/services/file-service.service';
import {searchFilesByDownloadLinks} from '../../../../../shared/file/application/searchByLinks';
import {documentQuery, FileUploadQuery} from '../../../../../shared/file/domain/FileUploadQuery';

interface INewEmployeeLinks {
    idLinkCVI: string;
    idLinkDni: string;
    idLinkEnglishCV: string;
    idLinkJustificationOfEmployment: string;
    idLinkOffer: string;
    idLinkSS: string;
    idLinkSocialSecurityCertificate: string;
    idLinkSpanishCV: string;
}

@Component({
    selector: 'app-create-new-employee-request',
    templateUrl: './create-new-employee-request.component.html',
    styleUrls: ['./create-new-employee-request.component.scss'],
})
export class CreateNewEmployeeRequestComponent implements OnInit {
    idChat: string;
    idStorage: string;
    chatHidden = true;
    storageHidden = true;
    tags = ['offer', 'ss', 'cv_esp', 'cv_eng', 'dni', 'cvi', 'employee_justification'];

    fileTypes = dictionaries.fileTypes;
    query: FileUploadQuery = documentQuery;
    englishLevelOptions = dictionaries.englishLevels;
    exists: INewEmployeeRequest;
    personalData: FormGroup;
    businessData: FormGroup;
    links: INewEmployeeLinks;

    loading: boolean = true;
    isFromCandidate: boolean = false;
    canBeSent: boolean = true;

    emailOptions: string[] = [];

    subcarOptions: string[] = [];
    categoryOptions: string[] = [];
    centerOptions: string[] = [];
    officeOptions: string[] = [];
    companyOptions: string[] = [];
    typeOfContractOptions: string[] = [];
    workingDayOptions: string[] = [];

    files: any[] = [];
    sentFiles: any[] = [];
    notSentFiles: any[] = [];
    savedFiles: string[] = [];

    centers: ICenter[] = [];
    companies: ICompany[] = [];
    offices: IOffice[] = [];

    selectedTypeOfFile = new Map();
    filesWoType: boolean = true;

    subcarName: string;
    categoryName: string;
    officeName: string;
    centerName: string;
    companyName: string;
    workingDayName: string;
    englishLevel: string;

    idCandidate: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _fb: FormBuilder,
        private _service: NewEmployeeRequestService,
        private _mapper: NewEmployeeRequestMapper,
        public fileService: FileService,
        private _employeeService: EmployeeService
    ) {
    }

    ngOnInit(): void {
        const {data} = this._route.snapshot.data['response'];

        if (data) {
            const newEmployee: INewEmployeeRequest = !data.candidate
                ? data.newEmployeeRequest
                : this.initFormFromCandidate(data.candidate);
            this.isFromCandidate = !!data.candidate;
            this.initLists(data);
            this.initLinks(newEmployee);
            this.initForms(newEmployee);
            this.initDefaults(newEmployee, data);
            this.exists = data.newEmployeeRequest;
            this.loading = false;
        }
    }

    private initDefaults(request: INewEmployeeRequest, data: any) {
        this.idChat = request.idChat;
        this.idStorage = request.idFileManager;
        this.subcarName = request?.subcar ? `${request.subcar.name} | (${request.subcar.id})` : '';
        this.categoryName = request?.category ? `${request.category.name} | (${request.category.id})` : '';
        this.officeName = request?.office?.location ?? '';
        this.centerName = request?.center?.location ?? '';
        this.companyName = request?.company?.name ?? '';
        this.englishLevel = request?.englishLevel ?? '';
        const wk = data.workingDays.content.find(w => w.id === request?.idWorkingDay);
        this.workingDayName = wk ? `${wk.description} | (${wk.id})` : '';
    }

    private initForms(newEmployeeRequest: INewEmployeeRequest) {
        this.initPersonalDataForm(newEmployeeRequest);
        this.initBusinessForm(newEmployeeRequest);
    }

    private initFormFromCandidate(candidate: ICandidate): INewEmployeeRequest {
        candidate.candidateEmails.forEach((mail) => {
            this.emailOptions.push(mail.email);
        });
        this.idCandidate = candidate.id;
        return this._mapper.mapFromCandidateToNewEmployeeRequest(candidate);
    }

    initLinks(newEmployee?: INewEmployeeRequest) {
        this.links = {
            idLinkCVI: newEmployee?.idLinkCVI ?? null,
            idLinkDni: newEmployee?.idLinkDni ?? null,
            idLinkEnglishCV: newEmployee?.idLinkEnglishCV ?? null,
            idLinkJustificationOfEmployment:
                newEmployee?.idLinkJustificationOfEmployment ?? null,
            idLinkOffer: newEmployee?.idLinkOffer ?? null,
            idLinkSS: newEmployee?.idLinkSS ?? null,
            idLinkSocialSecurityCertificate:
                newEmployee?.idLinkSocialSecurityCertificate ?? null,
            idLinkSpanishCV: newEmployee?.idLinkSpanishCV ?? null,
        };
        this.getFileInfo();
    }

    initLists(data) {
        const {centers, companies, offices} = data;
        this.centers = centers.content;
        this.companies = companies.content;
        this.offices = offices.content;

        this.subcarOptions = data.subcars.content.map(
            (subcar) => `${subcar.name} | (${subcar.id})`
        );
        this.categoryOptions = data.categories.content.map(
            (category) => `${category.name} | (${category.id})`
        );
        this.centerOptions = data.centers.content.map((center) => center.location);
        this.officeOptions = data.offices.content.map((office) => office.location);
        this.companyOptions = data.companies.content.map((company) => company.name);
        this.workingDayOptions = data.workingDays.content.map(
            (workingDay) => `${workingDay.description} | (${workingDay.id})`
        );
    }

    initPersonalDataForm(newEmployeeRequest?: INewEmployeeRequest) {
        this.personalData = this._fb.group({
            name: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.name ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.name ?? '',
            ],
            surname: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.surname ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.surname ?? '',
            ],
            personalEmail: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.personalEmail ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.personalEmail ?? '',
            ],
            personalPhone: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.personalPhone ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.personalPhone ?? '',
            ],
            dni: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.dni ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.dni ?? '',
            ],
            socialSecurityNumber: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.socialSecurityNumber ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.socialSecurityNumber ?? '',
            ],
            bankAccountNumber: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.bankAccountNumber ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.bankAccountNumber ?? '',
            ],
            nationality: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.nationality ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.nationality ?? '',
            ],
            civilState: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.civilState ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.civilState ?? '',
            ],
            // txt area
            currentStudiesLevel: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.currentStudiesLevel ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.currentStudiesLevel ?? '',
            ],
            // dic
            englishLevel: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.englishLevel ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.englishLevel ?? '',
            ],
            // txt area
            ongoingStudies: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.ongoingStudies ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.ongoingStudies ?? '',
            ],
            linkedin: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.linkedin ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.linkedin ?? '',
            ],
            address: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.address ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.address ?? '',
            ],
            city: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.name ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.city ?? '',
            ],
            province: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.province ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.province ?? '',
            ],
            postalCode: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.postalCode ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.postalCode ?? '',
            ],
            birthday: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.birthday ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.birthday ?? '',
            ],
            startDate: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.startDate ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.startDate ?? '',
            ],

            hasChildren: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.hasChildren ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.hasChildren ?? ''],
            ageChild1: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.ageChild1 ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.ageChild1 ?? '',
                Validators.pattern('([0-9])*'),
            ],
            ageChild2: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.ageChild2 ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.ageChild2 ?? '',
                Validators.pattern('([0-9])*'),
            ],
            ageChild3: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.ageChild3 ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.ageChild3 ?? '',
                Validators.pattern('([0-9])*'),
            ],
            ageChild4: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.ageChild4 ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.ageChild4 ?? '',
                Validators.pattern('([0-9])*'),
            ],
        });
    }

    initBusinessForm(newEmployeeRequest?: INewEmployeeRequest) {
        this.businessData = this._fb.group({
            technologyGroup: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.technologyGroup ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.technologyGroup ?? '',
            ],
            annualSalary: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.annualSalary ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.annualSalary ?? '',
                Validators.pattern('([0-9])*'),
            ],
            bonusIncorporation: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.bonusIncorporation ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.bonusIncorporation ?? '',
                Validators.pattern('([0-9])*'),
            ],
            bonusPermanence: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.bonusPermanence ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.bonusPermanence ?? '',
                Validators.pattern('([0-9])*'),
            ],
            businessEmail: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.businessEmail ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.businessEmail ?? '',
                Validators.email,
            ],
            businessPhone: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.businessPhone ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.businessPhone ?? '',
            ],
            referenced: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.referenced ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.referenced ?? '',
            ],

            // txt area
            comments: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.comments ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.comments ?? '',
            ],
            monthlyPayments: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.monthlyPayments ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.monthlyPayments ?? '',
                Validators.pattern('([0-9])*'),
            ],
            monthBonusIncorporation: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.monthBonusIncorporation ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.monthBonusIncorporation ?? '',
                Validators.pattern('([0-9])*'),
            ],
            monthBonusPermanence: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.monthBonusPermanence ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.monthBonusPermanence ?? '',
                Validators.pattern('([0-9])*'),
            ],
            variableSalary: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.variableSalary ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.variableSalary ?? '',
                Validators.pattern('([0-9])*'),
            ],

            idSubcar: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: this.subcarName ?? '',
                        disabled: true,
                    }
                    : this.subcarName ?? '',
            ],
            idCategory: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: this.categoryName ?? '',
                        disabled: true,
                    }
                    : this.categoryName ?? '',
            ],
            idCenter: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: this.centerName ?? '',
                        disabled: true,
                    }
                    : this.centerName ?? '',
            ],
            idCompany: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: this.companyName ?? '',
                        disabled: true,
                    }
                    : this.companyName ?? '',
            ],
            idOffice: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: this.officeName ?? '',
                        disabled: true,
                    }
                    : this.officeName ?? '',
            ],
            idWorkingDay: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: this.workingDayName ?? '',
                        disabled: true,
                    }
                    : this.workingDayName ?? '',
            ],
            typeOfContract: [
                !newEmployeeRequest
                    ? ''
                    : !newEmployeeRequest.canBeUpdated
                    ? {
                        value: newEmployeeRequest.typeOfContract ?? '',
                        disabled: true,
                    }
                    : newEmployeeRequest.typeOfContract ?? '',
            ],
        });

        this.businessData
            .get('businessEmail')
            .valueChanges
            .pipe(debounceTime(1000))
            .subscribe(email => {

            });
    }

    autocompleteSelected(event: {
        form: FormGroup;
        formField: string;
        value: string;
    }) {
        event.form.controls[event.formField].setValue(event.value);
    }

    switch(newEmployee: any): any {
        const {
            idSubcar,
            idCategory,
            idCenter,
            idCompany,
            idOffice,
            idWorkingDay,
            technologyGroup,
            annualSalary,
            bonusIncorporation,
            bonusPermanence,
            businessEmail,
            businessPhone,
            referenced,
            comments,
            monthlyPayments,
            monthBonusIncorporation,
            monthBonusPermanence,
            variableSalary,
            typeOfContract
        } = newEmployee;

        return {
            technologyGroup,
            annualSalary,
            bonusIncorporation,
            bonusPermanence,
            businessEmail,
            businessPhone,
            referenced,
            comments,
            monthlyPayments,
            monthBonusIncorporation,
            monthBonusPermanence,
            variableSalary,
            typeOfContract,
            idSubcar: this.format(idSubcar),
            idCategory: this.format(idCategory),
            idCenter:
                this.centers.find((center) => center.location === idCenter)?.id ?? null,
            idCompany:
                this.companies.find((company) => company.name === idCompany)?.id ??
                null,
            idOffice:
                this.offices.find((office) => office.location === idOffice)?.id ?? null,
            idWorkingDay: this.format(idWorkingDay),
        };
    }

    format(value: string): string {
        return value?.split('|')[1]?.replace('(', '').replace(')', '').trim();
    }

    assignFileType(id, type) {
        let oldType = this.selectedTypeOfFile.get(id);
        this.selectedTypeOfFile.forEach((value, key) => {
            if (value === type && key !== id) {
                Swal.fire({
                    title: 'Other file have the same type',
                    text: 'Do you want to change it?',
                    showDenyButton: true,
                    confirmButtonText: `Change`,
                    denyButtonText: `Cancel`,
                    icon: 'warning',
                }).then((result) => {

                    if (result.isConfirmed) {
                        this.selectedTypeOfFile.set(key, null);
                        this.selectedTypeOfFile.set(id, type);
                    }
                    if (result.isDenied) {
                        this.selectedTypeOfFile.set(id, oldType);
                    }
                });
            } else {
                this.selectedTypeOfFile.set(id, type);
            }
        });
    }

    sentFile(event: any) {
        this.sentFiles.push(event);
        this.selectedTypeOfFile.set(event.id, null);
    }

    deleteFile(event: any) {
        this.selectedTypeOfFile.delete(event.id);
        this.sentFiles.splice(this.sentFiles.indexOf(event), 1);
    }

    setFile(type, id) {
        switch (type) {
            case 'CVI':
                this.links.idLinkCVI = id;
                break;
            case 'Dni':
                this.links.idLinkDni = id;
                break;
            case 'EnglishCV':
                this.links.idLinkEnglishCV = id;
                break;
            case 'JustificationOfEmployment':
                this.links.idLinkJustificationOfEmployment = id;
                break;
            case 'Offer':
                this.links.idLinkOffer = id;
                break;
            case 'SS':
                this.links.idLinkSS = id;
                break;
            case 'SocialSecurityCertificate':
                this.links.idLinkSocialSecurityCertificate = id;
                break;
            case 'SpanishCV':
                this.links.idLinkSpanishCV = id;
                break;
            default:
                break;
        }
    }

    send() {
        this.emptyLinks();
        if (this.selectedTypeOfFile.size === 0) {
            return this.sendToServer();
        }
        this.updateLinks();
        if (!this.haveFilesWoType()) {
            return this.sendToServer();
        }
        Swal.fire({
            title: 'There are untyped files',
            text:
                'Do you want to continue process having untyped files? \n Untyped files will not be send to server',
            showCancelButton: true,
            confirmButtonText: `Continue`,
            cancelButtonText: `Cancel`,
            icon: 'question',
        }).then((result) => {
            if (result.isConfirmed) {
                this.sendToServer();
            }
        });
    }

    private sendToServer() {

        const full: INewEmployeeRequest = {
            ...this.personalData.value,
            ...this.switch(this.businessData.value),
            ...this.links,
            idCandidate: this.idCandidate
        };

        const $obs: Observable<INewEmployeeRequest> = !this.exists
            ? createNewEmployeeRequest(full, this._service, this._mapper)
            : updateNewEmployeeRequest(
                {...full, id: this.exists.id},
                this._service,
                this._mapper
            );

        $obs.subscribe(
            () => {
                successAlert('Employee request saved.').then(() => {
                    if (!this.exists) {
                        this._router.navigate(['./app/internal/employee/new-requests']);
                    }
                });
            }
        );
    }

    haveFilesWoType() {
        let flag = false;
        this.selectedTypeOfFile.forEach((value) => {
            if (value == null) {
                flag = true;
            }
        });
        return flag;
    }

    emptyLinks() {
        this.links.idLinkCVI = null;
        this.links.idLinkDni = null;
        this.links.idLinkEnglishCV = null;
        this.links.idLinkJustificationOfEmployment = null;
        this.links.idLinkOffer = null;
        this.links.idLinkSS = null;
        this.links.idLinkSocialSecurityCertificate = null;
        this.links.idLinkSpanishCV = null;
    }

    updateLinks() {
        this.selectedTypeOfFile.forEach((value, key) => {
            this.setFile(value, key);
        });
    }

    getFileInfo() {
        let linksArray: string[] = Object.values(this.links).filter(l => l);
        searchFilesByDownloadLinks(linksArray, this.fileService)
            .subscribe((res) => {
                    res.forEach((file) => {
                        this.sentFiles.push(file);
                        const prop = Object.keys(this.links).find(key => this.links[key] === file.url);
                        console.log('prop', prop, file.id, file);
                        this.selectedTypeOfFile.set(
                            file.id,
                            prop.substring(6, prop.length)
                        );
                    });
                }
            );
    }

    clearType(id) {
        this.selectedTypeOfFile.set(id, null);
    }

    setChatVisibility(visibility: boolean) {
        this.chatHidden = !visibility;
    }

    setStorageVisibility(visibility: boolean) {
        this.storageHidden = !visibility;
    }
}
