import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateNewEmployeeRequestComponent} from './create-new-employee-request.component';

describe('CreateNewEmployeeRequestComponent', () => {
  let component: CreateNewEmployeeRequestComponent;
  let fixture: ComponentFixture<CreateNewEmployeeRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateNewEmployeeRequestComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewEmployeeRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
