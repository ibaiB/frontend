import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {NewEmployeeRequestAbstractService} from '../new-employee-request-abstract-service';
import {IWebNewEmployeeRequest} from '../../domain/IWebNewEmployeeRequest';
import {environment} from '../../../../../environments/environment.prod';
import {IList} from 'src/app/shared/generics/IList';
import {INewEmployeeRequest} from '../../domain/INewEmployeeRequest';
import {NewEmployeeRequestMapper} from '../new-employee-request-mapper';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'any'
})
export class NewEmployeeRequestService extends NewEmployeeRequestAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'new-employee-requests';

  constructor(private _http: HttpClient,
    private mapper: NewEmployeeRequestMapper) {
    super();
  }

  create(newRequest: IWebNewEmployeeRequest): Observable<IWebNewEmployeeRequest> {
    return this._http.post<IWebNewEmployeeRequest>(`${this.BASE_URL}${this.ENDPOINT}`, this._clean(newRequest));
  }

  delete(id: string): Observable<any> {
    return this._http.delete<any>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  get(id: string): Observable<IWebNewEmployeeRequest> {
    return this._http.get<IWebNewEmployeeRequest>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  search(query: any): Observable<IList<INewEmployeeRequest>> {
    return this._http
      .get<IList<any>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})})
      .pipe(tap(list => list.content = list.content.map(this.mapper.mapTo)));
  }

  sendToNFQ(id: string): Observable<IWebNewEmployeeRequest> {
    return this._http.put<IWebNewEmployeeRequest>(`${this.BASE_URL}${this.ENDPOINT}/${id}/sendtonfq`, id);
  }

  update(newRequest: IWebNewEmployeeRequest): Observable<IWebNewEmployeeRequest> {
    return this._http.put<IWebNewEmployeeRequest>(`${this.BASE_URL}${this.ENDPOINT}/${newRequest.id}`, this._clean(newRequest));
  }

  private _clean(object) {
    const cleaned = {};
    const keys = Object.keys(object);
    keys.forEach(key => {
      if (object[key]!==undefined) {
        cleaned[key] = object[key];
      }
    });
    return cleaned;
  }
}
