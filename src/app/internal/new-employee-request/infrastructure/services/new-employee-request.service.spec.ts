import {TestBed} from '@angular/core/testing';

import {NewEmployeeRequestService} from './new-employee-request.service';

describe('NewEmployeeRequestService', () => {
  let service: NewEmployeeRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NewEmployeeRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
