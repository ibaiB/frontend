import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';
import {INewEmployeeRequest} from '../domain/INewEmployeeRequest';

import {IWebNewEmployeeRequest} from '../domain/IWebNewEmployeeRequest';

export abstract class NewEmployeeRequestAbstractService {
    abstract create(newRequest: IWebNewEmployeeRequest): Observable<IWebNewEmployeeRequest>;

    abstract update(newRequest: IWebNewEmployeeRequest): Observable<IWebNewEmployeeRequest>;

    abstract get(id: string): Observable<IWebNewEmployeeRequest>;

    abstract sendToNFQ(id: string): Observable<IWebNewEmployeeRequest>;

    abstract delete(id: string): Observable<any>;

    abstract search(query: any): Observable<IList<INewEmployeeRequest>>;
}
