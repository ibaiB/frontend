import {ITableFiltersConfig} from 'src/app/shared/custom-mat-elements/dynamic';
import {ICar} from 'src/app/internal/car/domain/ICar';
import {IList} from 'src/app/shared/generics/IList';
import {IUserInfo} from '../../../auth/session/domain/IUserInfo';
import {ICenter} from '../../center/domain/ICenter';
import {INewEmployeeRequest} from './INewEmployeeRequest';
import { ISubcar } from '../../subcar/domain/ISubcar';

export interface INewEmployeeRequestManagementData {
    myUser: IUserInfo;
    newEmployeeRequests: IList<INewEmployeeRequest>;
    subcars: IList<ISubcar>;
    centers: IList<ICenter>;
    filtersValue: any;
    tableFiltersConfig: ITableFiltersConfig
}
