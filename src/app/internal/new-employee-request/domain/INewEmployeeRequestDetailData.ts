import {ISubcar} from '../../subcar/domain/ISubcar';
import {INewEmployeeRequest} from './INewEmployeeRequest';
import {IList} from '../../../shared/generics/IList';
import {IWebCategory} from 'src/app/internal/category/domain/IWebCategory';
import {IWebCenter} from 'src/app/shared/entities/center/domain/IWebCenter';
import {IWebCompany} from 'src/app/internal/company/domain/IWebCompany';
import {IWebOffice} from '../../office/domain/IWebOffice';
import {IWebCar} from 'src/app/internal/car/domain/IWebCar';

export interface INewEmployeeRequestDetailData {
    //TODO:
    cars: IList<IWebCar>;
    subcars: IList<ISubcar>;
    categories: IList<IWebCategory>;
    centers: IList<IWebCenter>;
    companies: IList<IWebCompany>;
    offices: IList<IWebOffice>;
    workingDays: IList<any>;
    newEmployeeRequest?: INewEmployeeRequest;
}
