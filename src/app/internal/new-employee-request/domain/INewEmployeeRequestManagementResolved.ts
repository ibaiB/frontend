import {INewEmployeeRequestManagementData} from './INewEmployeeRequestManagementData';


export interface INewEmployeeRequestManagementResolved {
    tableData: INewEmployeeRequestManagementData;
    error?: any;
}
