import {IWebCarList} from '../../car/domain/IWebCarList';
import {IWebCategoryList} from '../../category/domain/IWebCategoryList';
import {IWebCenterList} from '../../../shared/entities/center/domain/IWebCenterList';
import {IWebCompanyList} from '../../company/domain/IWebCompanyList';
import {ISubcar} from '../../subcar/domain/ISubcar';
import {IWebOfficeList} from '../../office/domain/IWebOfficeList';
import {IWebWorkingDayList} from '../../working-day/domain/IWebWorkingDayList';
import {IList} from '../../../shared/generics/IList';

export interface INewEmployeeRequestCreateData {
    cars: IWebCarList;
    categories: IWebCategoryList;
    centers: IWebCenterList;
    companies: IWebCompanyList;
    offices: IWebOfficeList;
    workingDays: IWebWorkingDayList;
    subCars: IList<ISubcar>;
}
