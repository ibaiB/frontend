export interface INewEmployeeRequest {
    id?: string,
    idChat?: string,
    idFileManager?: string

    //Personal info
    address: string,
    ageChild1: string,
    ageChild2: string,
    ageChild3: string,
    ageChild4: string,
    bankAccountNumber: string,
    birthday: string,
    civilState: string,
    currentStudiesLevel: string,
    englishLevel: string,
    dni: string,
    city: string,
    name: string,
    nationality: string,
    ongoingStudies: string,
    personalEmail: string,
    personalPhone: string,
    postalCode: string,
    province: string,
    socialSecurityNumber: string,
    startDate: string,
    surname: string,
    hasChildren: boolean,
    linkedin: string,

    //Business info
    technologyGroup: string,
    annualSalary: number,
    bonusIncorporation: number,
    bonusPermanence: number,
    businessEmail: string,
    businessPhone: string,
    changeResidence: boolean,
    comments: string,
    canBeUpdated?: boolean,
    state: string;
    idCar: string,
    idSubcar: string;
    idCategory: string,
    idCenter: string,
    idCompany: string,
    idOffice: string,
    idWorkingDay: string,
    idCandidate?: string,
    monthlyPayments: number,
    monthBonusIncorporation: number,
    monthBonusPermanence: number,
    variableSalary: number,

    //files
    idLinkCVI: string,
    idLinkDni: string,
    idLinkEnglishCV: string,
    idLinkJustificationOfEmployment: string,
    idLinkOffer: string,
    idLinkSS: string,
    idLinkSocialSecurityCertificate: string,
    idLinkSpanishCV: string,

    typeOfContract: string,
    //booleans
    //TODO vamos a dejarlo pasar de momento
    lanzar?: boolean,
    incidenciasSolucionadas?: boolean,
    referenced: boolean,

    // TODO: quickfix
    category?: any;
    center?: any;
    company?: any;
    office?: any;
    subcar?: any;
    car?: any;
    candidate?: any;

    idEmployee?: string;
}
