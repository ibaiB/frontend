import {INewEmployeeRequestCreateData} from './INewEmployeeRequestCreateData';

export interface INewEmployeeRequestCreateResolved {
    data: INewEmployeeRequestCreateData;
    error?: any;
}
