import {IWebNewEmployeeRequest} from './IWebNewEmployeeRequest';

export interface IWebNewEmployeeRequestList {
    content: IWebNewEmployeeRequest[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
