import {IWebSubcar} from './IWebSubcar';

export interface IWebSubcarList {
    content: IWebSubcar[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
