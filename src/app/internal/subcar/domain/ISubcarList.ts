import {ISubcar} from './ISubcar';

export interface ISubcarList{
    content: ISubcar[],
    numberOfElements: number,
    totalElements: number,
    totalPages: number,
}
