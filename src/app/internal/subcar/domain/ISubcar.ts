import {IEmployeeRead} from '../../employee/domain/IEmployee';

export interface ISubcar {
    employees: IEmployeeRead[],
    id: string,
    name: string,
}
