export class SubcarMapper{
    mapTo(params: any){
        const {
            employees,
            id,
            name,
        } = params;

        return {
            employees: employees,
            id: id,
            name: name,
        }
    }
}