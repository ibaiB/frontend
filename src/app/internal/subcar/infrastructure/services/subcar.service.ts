import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {SubcarMapper} from '../SubcarMapper';
import {SubcarAbstractService} from '../SubcarAbstractService';
import {IWebSubcar} from '../../domain/IWebSubcar';
import {IList} from '../../../../shared/generics/IList';

@Injectable({
  providedIn: 'root'
})

export class SubcarService extends SubcarAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'subcars';

  constructor(private http: HttpClient, private mapper: SubcarMapper) { super(); }

  findById(id: string): Observable<IWebSubcar> {
    return this.http
      .get<IWebSubcar>(`${this.BASE_URL}${this.ENDPOINT}/${id}`)
      .pipe(map(this.mapper.mapTo));
  }

  search(params: any): Observable<IList<IWebSubcar>> {
    return this.http
      .get<IList<IWebSubcar>>(`${this.BASE_URL}${this.ENDPOINT}/search`, { params: new HttpParams({fromObject: params}) });
  }

}
