import {TestBed} from '@angular/core/testing';

import {SubcarService} from './subcar.service';

describe('SubcarService', () => {
  let service: SubcarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubcarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
