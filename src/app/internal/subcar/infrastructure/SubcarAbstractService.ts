import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';
import {IWebSubcar} from '../domain/IWebSubcar';

export abstract class SubcarAbstractService {
    abstract findById(id: string): Observable<IWebSubcar>;

    abstract search(params: any): Observable<IList<IWebSubcar>>;
}
