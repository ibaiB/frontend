import {SubcarAbstractService} from '../infrastructure/SubcarAbstractService';

export function getSingleSubCar(id: string, service: SubcarAbstractService) {
    return service.findById(id);
}
