import {SubcarService} from '../infrastructure/services/subcar.service';

export function getSubCars(params: any, service: SubcarService){
    return service.search(params);
}
