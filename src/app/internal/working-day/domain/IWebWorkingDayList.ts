import {IWebWorkingDay} from './IWebWorkingDay';

export interface IWebWorkingDayList {
    content: IWebWorkingDay[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
