export interface IWorkingDay {
    id: string;
    description: string;
    holidaysAmount: number;
    annualHours: number;
    weeklyHours: number;
}
