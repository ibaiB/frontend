export interface IWebWorkingDay {
    id: string;
    description: string;
}
