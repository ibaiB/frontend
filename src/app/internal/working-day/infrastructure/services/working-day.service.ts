import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment.prod';
import {WorkingDayAbstractService} from '../WokingDayAbstractService';
import {IWebWorkingDay} from '../../domain/IWebWorkingDay';
import {IWebWorkingDayList} from '../../domain/IWebWorkingDayList';

@Injectable({
  providedIn: 'any'
})
export class WorkingDayService extends WorkingDayAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private endpoint: string = 'workingdays';

  constructor(private _http: HttpClient) {
    super();
  }

  search(query: any): Observable<any> {
    return this._http.get<IWebWorkingDayList>(`${this.BASE_URL}${this.endpoint}/search`, {params: new HttpParams({fromObject: query})});
  }

  findById(id: any): Observable<any> {
    return this._http.get<IWebWorkingDay>(`${this.BASE_URL}${this.endpoint}/${id}`);
  }
}
