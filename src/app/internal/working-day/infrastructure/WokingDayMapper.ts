import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any'
})
export class WorkingDayMapper {
    mapTo(params: any) {
        const {
            id,
            description,
            numVacaciones,
            horasAnuales,
            horasSemanales
        } = params;

        return {
            id: id,
            description: description,
            holidaysAmount: numVacaciones,
            annualHours: horasAnuales,
            weeklyHours: horasSemanales
        };
    }

    mapFrom(params: any) {
        const {
            id,
            description
        } = params;

        return {
            id: id,
            description: description
        };
    }
}
