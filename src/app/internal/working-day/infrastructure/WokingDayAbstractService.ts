import {Observable} from 'rxjs';

import {IWebWorkingDay} from '../domain/IWebWorkingDay';
import {IWebWorkingDayList} from '../domain/IWebWorkingDayList';

export abstract class WorkingDayAbstractService {

  abstract search(query: any): Observable<IWebWorkingDayList>;

  abstract findById(id: any): Observable<IWebWorkingDay>;

}

