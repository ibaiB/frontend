export const types: string[] = ['CV_ESP', 'CV_ENG'];

export interface IDocument {
    id?: string,
    created: string,
    idEmployee: string,
    idLink: string,
    link?: string,
    type: string
}
