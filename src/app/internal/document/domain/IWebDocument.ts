export interface IWebDocument {
    id: string
    fechaCreacion: string,
    idEmployee: string,
    idLink: string,
    link?: string,
    employeeDocumentTypeEnum: string
}
