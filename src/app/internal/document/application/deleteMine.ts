import {DocumentAbstractService} from '../infrastructure/document-abstract-service';

export function removeMine(id: string, service: DocumentAbstractService) {
    return service.delete(id, true);
}
