import {IDocument} from '../domain/IDocument';
import {DocumentMapper} from '../infrastructure/document-mapper';
import {DocumentAbstractService} from '../infrastructure/document-abstract-service';
import {map} from 'rxjs/operators';

export function create(document: IDocument, service: DocumentAbstractService) {
    const mapper = new DocumentMapper();

    return service
        .create(mapper.mapFrom(document), false)
        .pipe(map(mapper.mapTo));
}
