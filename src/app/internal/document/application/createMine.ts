import {IDocument} from '../domain/IDocument';
import {DocumentMapper} from '../infrastructure/document-mapper';
import {DocumentAbstractService} from '../infrastructure/document-abstract-service';
import {map} from 'rxjs/operators';

export function createMine(document: IDocument, service: DocumentAbstractService) {
    const mapper = new DocumentMapper();

    return service
        .create(mapper.mapFrom(document), true)
        .pipe(map(mapper.mapTo));
}
