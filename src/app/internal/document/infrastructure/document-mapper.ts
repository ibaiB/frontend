import {IDocument} from '../domain/IDocument';
import {IWebDocument} from '../domain/IWebDocument';

export class DocumentMapper {
    mapTo(object: any): IDocument {
        const {
            fechaCreacion,
            idEmployee,
            idLink,
            employeeDocumentTypeEnum,
            id,
            link
        } = object;

        return {
            id,
            created: fechaCreacion,
            idEmployee,
            link,
            idLink,
            type: employeeDocumentTypeEnum
        };
    }

    mapFrom(object: any): IWebDocument {
        const {
            created,
            idEmployee,
            idLink,
            type,
            id,
        } = object;

        return {
            id,
            fechaCreacion: created,
            idEmployee,
            idLink,
            employeeDocumentTypeEnum: type
        };
    }
}
