import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {DocumentAbstractService} from '../document-abstract-service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentService extends DocumentAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'employees/documents';

  constructor(private _httpClient: HttpClient) {
    super();
  }

  create(document: any, mine: boolean, index?: string): Observable<any> {
    const base = `${this.BASE_URL}${this.ENDPOINT}`;
    const route = mine ? '/me' : '';
    const params: any = index ? {index: index} : index;

    return this._httpClient.post<any>(base + route, document, {params: new HttpParams({fromObject: params})});
  }

  delete(id: string, mine: boolean): Observable<any> {
    const base = `${this.BASE_URL}${this.ENDPOINT}`;
    const route = mine ? '/me/' : '/';

    return this._httpClient.delete<any>(base + route + id,);
  }
}
