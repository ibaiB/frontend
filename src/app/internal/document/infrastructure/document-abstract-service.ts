import {Observable} from 'rxjs';
import {IWebDocument} from '../domain/IWebDocument';

export abstract class DocumentAbstractService {
    abstract create(document: IWebDocument, mine: boolean, index?: string): Observable<IWebDocument>

    abstract delete(id: string, mine: boolean): Observable<any>
}
