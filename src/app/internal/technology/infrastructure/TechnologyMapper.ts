import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any'
})
export class TechnologyMapper {
    mapTo(params: any) {
        const {
            id,
            name,
            description
        } = params;

        return {
            id,
            name,
            description
        };
    }

    mapFrom(params: any) {
        const {
            id,
            name,
            description
        } = params;

        return {
            id,
            name,
            description
        };
    }
}
