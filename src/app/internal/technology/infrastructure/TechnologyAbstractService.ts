import {Observable} from 'rxjs';

import {IWebTechnologyList} from '../domain/IWebTechnologyList';

export abstract class TechnologyAbstractService {
  abstract search(query: any): Observable<IWebTechnologyList>;
}
