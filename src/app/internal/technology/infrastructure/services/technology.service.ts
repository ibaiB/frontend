import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {TechnologyAbstractService} from '../TechnologyAbstractService';
import {environment} from '../../../../../environments/environment.prod';
import {IWebTechnologyList} from '../../domain/IWebTechnologyList';

@Injectable({
  providedIn: 'any'
})
export class TechnologyService extends TechnologyAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private endpoint: string = 'technologies';

  constructor(private _http: HttpClient) {
    super();
  }

  search(query: any): Observable<any> {
    return this._http.get<IWebTechnologyList>(`${this.BASE_URL}${this.endpoint}/search`, { params: new HttpParams({ fromObject: query }) });
  }
}
