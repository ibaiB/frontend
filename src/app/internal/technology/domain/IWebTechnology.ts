type Technologies =
    | "JAVA"
    | "NET"
    | "DEVOPS"
    | "BIG_DATA"
    | "VISUALIZACION"
    | "IOT"
    | "MACHINE_LEARNING"
    | "SOFTWARE_ROBOTS"
    | "ID"
    | "NA"
    | "OTHERS"
    | "FULL_STACK"
    | "FRONT"
    | "PYTHON";
export interface IWebTechnology {
    id?: string;
    description: string;
    name: Technologies;
}
