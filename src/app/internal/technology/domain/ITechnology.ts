export interface ITechnology {
    id?: string;
    description: string;
    name: string;
}
