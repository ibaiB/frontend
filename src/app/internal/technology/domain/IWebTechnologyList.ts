import {IWebTechnology} from './IWebTechnology';

export interface IWebTechnologyList {
    content: IWebTechnology[];
    numberOfElements: number;
    totalElements: number;
    totalPages: number;
}
