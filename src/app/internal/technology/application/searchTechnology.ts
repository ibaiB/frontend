import {TechnologyAbstractService} from '../infrastructure/TechnologyAbstractService';

export function searchTechnology(query: any, service: TechnologyAbstractService) {
    return service.search(query);
}
