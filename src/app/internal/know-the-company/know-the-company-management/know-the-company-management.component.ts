import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-know-the-company-management',
  templateUrl: './know-the-company-management.component.html',
  styleUrls: ['./know-the-company-management.component.scss']
})
export class KnowTheCompanyManagementComponent implements OnInit {

  constructor(private _router: Router) {
  }

  ngOnInit(): void {
    this._router.navigate(['app/coming-soon']);
  }

}
