import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KnowTheCompanyManagementComponent} from './know-the-company-management.component';

describe('KnowTheCompanyManagementComponent', () => {
  let component: KnowTheCompanyManagementComponent;
  let fixture: ComponentFixture<KnowTheCompanyManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KnowTheCompanyManagementComponent]
    })
        .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnowTheCompanyManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
