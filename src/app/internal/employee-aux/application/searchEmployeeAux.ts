import {EmployeeAuxAbstractService} from '../infrastructure/employee-aux-abstract-service';

export function searchEmployeeAux(query: any, service: EmployeeAuxAbstractService){
    return service.search(query);
}
