import {map} from 'rxjs/operators';
import {EmployeeAuxAbstractService} from '../infrastructure/employee-aux-abstract-service';
import {EmployeeAuxMapper} from '../infrastructure/employee-aux-mapper';

export function updateEmployeeAux(employeeAux: any, id: string, service: EmployeeAuxAbstractService) {
    const mapper = new EmployeeAuxMapper();
    return service.update(mapper.mapFrom(employeeAux), id).pipe(map(mapper.mapTo));
}
