interface ICommon {
    id?: string,
    fechaFinFormacion: string,
    fechaPrimerCar: string,
    fechaPrimerCarProductivo: string,
    idCarProductivo: string,
    idPrimerCar: string,
    inversionTecnologia: string,
    tipoIncorporacion: string,
    dniEmployee: string,
}

export interface IWebEmployeeAuxWrite extends ICommon {
    idStudies: string[],
}

export interface IWebEmployeeAuxRead extends ICommon {
    technologyInvestment: string,
    typeIncorporation: string,
    employees: any[],
    studies: any[]
}
