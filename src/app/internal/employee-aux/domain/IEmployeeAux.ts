import {ICar} from '../../car/domain/ICar';
import {IStudy} from '../../../recruitment/study/domain/IStudy';
import {IEmployeeRead} from '../../employee/domain/IEmployee';

interface ICommon {
    id?: string,
    technologyInvestment: string,
    dniEmployee: string,
    typeIncorporation: string,
    internshipEndDate: string,
    firstCarDate: string,
    firstProductiveCarDate: string,
}

export interface IEmployeeAuxWrite extends ICommon {
    idFirstCar: string,
    idProductiveCar: string,
    idStudies: string[]
}

export interface IEmployeeAuxRead extends ICommon {
    firstCar: ICar,
    productiveCar: ICar,
    studies: IStudy[],
    employees: IEmployeeRead[]
}
