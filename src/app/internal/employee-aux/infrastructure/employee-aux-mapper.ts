import {Injectable} from '@angular/core';
import {IEmployeeAuxRead} from '../domain/IEmployeeAux';
import {IWebEmployeeAuxWrite} from '../domain/IWebEmployeeAux';
import {IList} from '../../../shared/generics/IList';
import {StudyMapper} from '../../../recruitment/study/infrastructure/StudyMapper';
import {CarMapper} from '../../car/infrastructure/CarMapper';
import {EmployeeMapper} from '../../employee/infrastructure/EmployeeMapper';

@Injectable({
    providedIn: 'any',
})
export class EmployeeAuxMapper {
    mapTo(params: any): IEmployeeAuxRead {
        const studyMapper = new StudyMapper();
        const carMapper = new CarMapper();
        const employeeMapper = new EmployeeMapper();

        const {
            id,
            technologyInvestment,
            typeIncorporation,
            dniEmployee,
            idCarProductivo,
            primerCarProductivo,
            idPrimerCar,
            primerCar,
            fechaFinFormacion,
            fechaPrimerCarProductivo,
            fechaPrimerCar,
            employees,
            studies
        } = params;

        return {
            id,
            dniEmployee,
            firstCarDate: fechaPrimerCar,
            firstProductiveCarDate: fechaPrimerCarProductivo,
            internshipEndDate: fechaFinFormacion,
            technologyInvestment,
            typeIncorporation,
            employees: employees ? employees.map(employeeMapper.mapTo) : [],
            firstCar: primerCar ? carMapper.mapTo(primerCar) : primerCar,
            productiveCar: primerCarProductivo ? carMapper.mapTo(primerCarProductivo) : primerCarProductivo,
            studies: studies ? studies.map(studyMapper.mapTo) : []
        };
    }

    mapFrom(params: any): IWebEmployeeAuxWrite {
        const {
            id,
            dniEmployee,
            internshipEndDate,
            firstCarDate,
            firstProductiveCarDate,
            idProductiveCar,
            idFirstCar,
            technologyInvestment,
            incorporationType,
            idStudies
        } = params;

        return {
            id,
            dniEmployee,
            fechaFinFormacion: internshipEndDate,
            fechaPrimerCar: firstCarDate,
            fechaPrimerCarProductivo: firstProductiveCarDate,
            idPrimerCar: idFirstCar,
            idCarProductivo: idProductiveCar,
            inversionTecnologia: technologyInvestment,
            tipoIncorporacion: incorporationType,
            idStudies
        };
    }

    mapList(list: IList<any>): IList<IEmployeeAuxRead> {
        return {
            content: list.content.map(this.mapTo),
            numberOfElements: list.numberOfElements,
            totalElements: list.totalElements,
            totalPages: list.totalPages
        };
    }
}
