import {TestBed} from '@angular/core/testing';

import {EmployeeAuxService} from './employee-aux.service';

describe('EmployeeAuxService', () => {
  let service: EmployeeAuxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployeeAuxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
