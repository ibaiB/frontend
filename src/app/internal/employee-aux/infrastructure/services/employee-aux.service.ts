import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';
import {environment} from 'src/environments/environment.prod';
import {IWebEmployeeAuxRead, IWebEmployeeAuxWrite} from '../../domain/IWebEmployeeAux';
import {EmployeeAuxAbstractService} from '../employee-aux-abstract-service';

@Injectable({
  providedIn: 'any'
})
export class EmployeeAuxService extends EmployeeAuxAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'employeesAux';

  constructor(private _http: HttpClient) {
    super();
  }

  update(auxEmployee: IWebEmployeeAuxWrite, id: string): Observable<IWebEmployeeAuxRead> {
    return this._http.put<IWebEmployeeAuxRead>(`${this.BASE_URL}${this.ENDPOINT}/${id}`, this._clean(auxEmployee));
  }

  search(query: any): Observable<IList<IWebEmployeeAuxRead>> {
    return this._http.get<IList<IWebEmployeeAuxRead>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: this._clean(query)})});
  }

  private _clean(object): any {
    const cleaned = {};
    const keys = Object.keys(object);
    keys.forEach(key => {
      if (object[key]) {
        cleaned[key] = object[key];
      }
    });
    return cleaned;
  }

}
