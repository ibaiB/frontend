import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ResolvedData} from 'src/app/shared/generics/IResolvedData';
import {searchEmployeeAux} from '../../application/searchEmployeeAux';
import {IEmployeeAuxTable} from '../../domain/IEmployeeAuxTable';
import {EmployeeAuxService} from '../services/employee-aux.service';

@Injectable({
    providedIn: 'any'
})
export class EmployeeAuxTableResolverService implements Resolve<ResolvedData<IEmployeeAuxTable>> {

    constructor(private _service: EmployeeAuxService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): ResolvedData<IEmployeeAuxTable> | Observable<ResolvedData<IEmployeeAuxTable>> | Promise<ResolvedData<IEmployeeAuxTable>> {
        return searchEmployeeAux({page: 0, size: 500}, this._service)
      .pipe(
        map(response => ({
          data: {
            employeeAux: response
          },
        })),
        catchError(error => {
          return of({
            data: null,
            error: error,
            message: 'Error while fetching employee aux table data'
          })
        }));
  }
}
