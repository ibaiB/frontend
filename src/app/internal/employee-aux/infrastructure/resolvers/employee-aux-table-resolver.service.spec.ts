import {TestBed} from '@angular/core/testing';

import {EmployeeAuxTableResolverService} from './employee-aux-table-resolver.service';

describe('EmployeeAuxTableResolverService', () => {
  let service: EmployeeAuxTableResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployeeAuxTableResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
