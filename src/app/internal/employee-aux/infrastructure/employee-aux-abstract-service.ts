import {Observable} from 'rxjs';
import {IList} from 'src/app/shared/generics/IList';
import {IWebEmployeeAuxRead, IWebEmployeeAuxWrite} from '../domain/IWebEmployeeAux';

export abstract class EmployeeAuxAbstractService {
    abstract update(auxEmployee: IWebEmployeeAuxWrite, id: string): Observable<IWebEmployeeAuxRead>;

    abstract search(query: any): Observable<IList<IWebEmployeeAuxRead>>;
}
