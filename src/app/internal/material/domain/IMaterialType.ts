export interface IMaterialType{
    id: number;
    description: string;
    nombre: string;
    tipo: string;
}
