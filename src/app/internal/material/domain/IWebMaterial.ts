import {IMaterialType} from './IMaterialType';

export interface IWebMaterial {
    id: string;
    name: string;
    description: string;
    materialType: IMaterialType;
}
