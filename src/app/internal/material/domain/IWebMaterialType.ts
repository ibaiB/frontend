export interface IWebMaterialType{
    id: number;
    description: string;
    nombre: string;
    tipo: string;
}
