import {IMaterialType} from './IMaterialType';

export interface IMaterial {
    id: string;
    name: string;
    description: string;
    materialType: IMaterialType;
}
