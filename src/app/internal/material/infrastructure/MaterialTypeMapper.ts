import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class MaterialTypeMapper{
    mapTo(params: any){
        const {
            id,
            description,
            nombre,
            tipo
        } = params;

        return {
            id: id,
            description: description,
            nombre: nombre,
            tipo: tipo
        };
    }

    mapFrom(params: any){
        const {
            id,
            description,
            nombre,
            tipo
        } = params;

        return {
            id: id,
            description: description,
            nombre: nombre,
            tipo: tipo
        };
    }
}
