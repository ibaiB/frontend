import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'any',
})
export class MaterialMapper {
    mapTo(params: any) {
        const {
            id,
            name,
            description,
            materialType,
        } = params;

        return {
            id: id,
            name: name,
            description: description,
            materialType: materialType,
        };
    }

    mapFrom(params: any) {
        const {
            id,
            name,
            description,
            materialType,
        } = params;

        return {
            id: id,
            name: name,
            description: description,
            materialType: materialType,
        };
    }
}
