import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {environment} from '../../../../../environments/environment.prod';
import {IWebMaterial} from '../../domain/IWebMaterial';
import {MaterialAbstractService} from '../MaterialAbstractService';
import {IList} from '../../../../shared/generics/IList';

@Injectable({
  providedIn: 'any'
})
export class MaterialService extends MaterialAbstractService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'materials';

  constructor(
      private _http: HttpClient
  ) {
    super();
  }

  findById(id: string): Observable<IWebMaterial> {
    return this._http.get<IWebMaterial>(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  search(query: any): Observable<IList<IWebMaterial>> {
    return this._http.get<IList<IWebMaterial>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: query})});
  }

}
