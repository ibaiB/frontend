import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {IWebMaterial} from '../domain/IWebMaterial';
import {IList} from '../../../shared/generics/IList';

@Injectable({
    providedIn: 'root'
})
export abstract class MaterialAbstractService {

    abstract findById(id: string): Observable<IWebMaterial>;

    abstract search(query: any): Observable<IList<IWebMaterial>>;

}
