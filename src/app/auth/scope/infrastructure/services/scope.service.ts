import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ScopeMapper} from '../scope-mapper';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AbstractScopeService} from '../abstract-scope-service';
import {IScope} from '../../domain/IScope';
import {map, mergeMap, toArray} from 'rxjs/operators';
import {IScopeList} from '../../domain/IScopeList';
import {environment} from '../../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ScopeService extends AbstractScopeService{

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'scopes';

  constructor(private httpClient: HttpClient, private mapper: ScopeMapper) {super();}

  createScope(scope: IScope): Observable<IScope> {
    return this.httpClient
        .post<IScope>(`${this.BASE_URL}${this.ENDPOINT}`, scope)
        .pipe(map(this.mapper.mapTo));
  }

  deleteScope(id: string) {
    return this.httpClient
        .delete(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  search(params: any): Observable<IScope[]> {
    let httpParams = new HttpParams();
    //@TODO separate in a function
    let paramsKeys = Object.keys(params);
    paramsKeys.forEach(key => httpParams = httpParams.append(key, params[key].toString()))

    return this.httpClient
        .get<IScopeList>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: httpParams})
        .pipe(mergeMap(list => list.content))
        .pipe(map(this.mapper.mapTo))
        .pipe(toArray());
  }


}
