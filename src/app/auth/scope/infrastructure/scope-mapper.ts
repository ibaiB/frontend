export class ScopeMapper {
    mapTo(params: any){
        const {id, name, value, description} = params;

        return {
            id: id,
            name: name,
            value: value,
            description: description
        }
    }
}
