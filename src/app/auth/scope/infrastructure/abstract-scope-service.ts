import {Observable} from 'rxjs';
import {IScope} from '../domain/IScope';

export abstract class AbstractScopeService {
    public abstract createScope(scope: IScope): Observable<IScope>;
    public abstract deleteScope(id: string);
    public abstract search(params: any): Observable<IScope[]>;
}
