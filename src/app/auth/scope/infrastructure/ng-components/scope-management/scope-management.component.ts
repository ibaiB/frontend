import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IScope} from '../../../domain/IScope';

@Component({
  selector: 'app-scope-management',
  templateUrl: './scope-management.component.html',
  styleUrls: ['./scope-management.component.scss']
})
export class ScopeManagementComponent implements OnInit {

  @Input() all: IScope[];
  @Input() itemScopes: string[];
  @Output() updated = new EventEmitter<IScope[]>();

  entities: {
    name: string,
    scopes: { id: string, name: string }[]
  }[] = [];


  constructor() {

  }

  ngOnInit() {
    this._setEntities(this.all);
  }

  private _setEntities(scopes: IScope[]) {
    let [current] = scopes[0].value.split('_');
    let entityScopes: { id: string, name: string }[] = [];

    scopes.forEach(scope => {
      const [entity, action] = scope.value.split('_');

      if (current === entity) {
        entityScopes.push({
          id: scope.id,
          name: this.parseEntity(action ?? entity)
        });
      } else {
        this.entities.push({
          name: this.parseEntity(current),
          scopes: entityScopes,
        });
        current = entity;
        entityScopes = [];
      }
    });
  }

  parseEntity(entity: string) {
    const value = entity.toLowerCase();
    return value.charAt(0).toUpperCase() + value.slice(1);
  }

  hasScope(id: string): boolean {
    return this.itemScopes.includes(id);
  }

  detectScopeChange(id: string) {

  }

  detectEntityChange(scopes) {

  }

}
