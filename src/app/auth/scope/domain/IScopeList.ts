import {IScope} from './IScope';

export interface IScopeList{
    content: IScope[];
    contentLength: number;
    totalElements: number;
    totalPages: number
}
