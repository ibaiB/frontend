export interface IScope{
    id?: string,
    name: string,
    value: string,
    description: string
}
