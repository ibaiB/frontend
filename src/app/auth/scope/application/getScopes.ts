import {AbstractScopeService} from '../infrastructure/abstract-scope-service';

export function getScopes(params: any, service: AbstractScopeService){
    return service.search(params);
}
