import {AbstractSessionService} from '../infrastructure/abstract-session-service';
import {tap} from 'rxjs/operators';
import {CookieFacadeService} from '../infrastructure/services/cookie.service';

export function getUserInfo(service: AbstractSessionService) {
    return service.userInfo()
        .pipe(tap(user => {
            const cookieFacade = new CookieFacadeService();
            cookieFacade.save('userInfo', JSON.stringify(user));
        }));
}
