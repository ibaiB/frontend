import {AbstractSessionService} from '../infrastructure/abstract-session-service';

export function exchangeToken(googleToken: string, service: AbstractSessionService){
    return service.exchangeToken(googleToken);
}
