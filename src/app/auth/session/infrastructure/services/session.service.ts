import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IUser} from '../../../user/domain/IUser';
import {map} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';
import {UserMapper} from '../../../user/infrastructure/user-mapper';
import {AbstractSessionService} from '../abstract-session-service';
import {environment} from '../../../../../environments/environment';
import {IBackToken} from '../../domain/IBackToken';
import {IUserInfo} from '../../domain/IUserInfo';
import {TokenMapper} from '../token-mapper';
import {UserInfoMapper} from '../user-info-mapper';

@Injectable({
  providedIn: 'root'
})
export class SessionService extends AbstractSessionService{

  BASE_URL: string = environment.baseUrl;
  ENDPOINT: string = 'oauth2';

  constructor(private httpClient: HttpClient,
              private userMapper: UserMapper,
              private tokenMapper: TokenMapper,
              private userInfoMapper: UserInfoMapper) { super();}

  //@TODO this endpoint is never called
  login(email: string, password: string): Observable<IUser>  {
    return this.httpClient
        .post<any>('login', {email: email, password: password})
        .pipe(map(this.userMapper.mapTo));
  }

  getUser(): Observable<IUser> {
    //@TODO this route should include an id
    return this.httpClient
        .get<any>('oauth2/userInfo')
        .pipe(map(this.userMapper.mapTo));
  }

  exchangeToken(googleToken: string): Observable<IBackToken> {
    let httpParams = new HttpParams()
        .set('token', googleToken);

    return this.httpClient
        .get<IBackToken>(`${this.BASE_URL}${this.ENDPOINT}/exchange`, {params: httpParams})
        .pipe(map(this.tokenMapper.mapTo));
  }

  //@TODO this endpoint is never called
  access(user: IUser): Observable<IBackToken> {


    return undefined;
  }

  //@TODO this endpoint is never called
  authorize(clientId: string, user: IUser): Observable<IBackToken> {
    return undefined;
  }

  //@TODO this endpoint is never called
  authorizeUserInfo(clientId: string): Observable<IUser> {
    return undefined;
  }

  //@TODO this endpoint is never called
  logout() {
  }

  //@TODO this endpoint is never called
  tokenInfo(clientId: string): Observable<any> {
    return undefined;
  }

  userInfo(): Observable<IUserInfo> {
    return this.httpClient
        .get(`${this.BASE_URL}${this.ENDPOINT}/userInfo`)
        .pipe(map(this.userInfoMapper.mapTo));
  }
}
