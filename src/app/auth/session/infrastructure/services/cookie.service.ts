import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CookieFacadeService {

    constructor() {
    }

    save(key: string, value: string) {
        localStorage.setItem(key, value);
    }

    contains(key: string) {
        return localStorage.getItem(key) !== null;
    }

    get(key: string) {
        return localStorage.getItem(key);
    }

    delete(key: string) {
        localStorage.removeItem(key);
    }
}
