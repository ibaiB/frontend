import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {CookieFacadeService} from "./cookie.service";
import {authConfig} from "../../../config/auth.config";

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private cookie: CookieFacadeService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const existsToken = this.cookie.contains(authConfig.sessionStorageKey);
    if (!existsToken) {
      return next.handle(req);
    }
    const token = this.cookie.get(authConfig.sessionStorageKey);
    const headers = req.clone({
      headers: req.headers.set('Authorization', `${token}`)
    });
    return next.handle(headers);
  }
}
