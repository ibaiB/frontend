export class TokenMapper {
    mapTo(params: any){
        const {expiration, access_id_token, issued_at, user_id} = params;

        return {
            expiration: expiration,
            idToken: access_id_token,
            issuedAt: issued_at,
            userId: user_id
        }
    }
}
