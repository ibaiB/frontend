export class UserInfoMapper {
    mapTo(params: any){
        const {email,
            emailVerified,
            id,
            loggedWithGoogle,
            roles,
            scopes,
            username} = params;

        return {
            email: email,
            emailVerified: emailVerified,
            id: id,
            loggedWithGoogle: loggedWithGoogle,
            roles: roles,
            scopes: scopes,
            username: username
        }
    }
}
