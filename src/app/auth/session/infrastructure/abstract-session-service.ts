import {Observable} from 'rxjs';
import {IUser} from '../../user/domain/IUser';
import {IBackToken} from '../domain/IBackToken';
import {IUserInfo} from '../domain/IUserInfo';

export abstract class AbstractSessionService {
    //@TODO returned value has more info than current model
    public abstract login(email: string, password: string): Observable<IUser>;
    public abstract getUser(id: number): Observable<IUser>;
    public abstract exchangeToken(googleToken: string): Observable<IBackToken>;
    public abstract access(user: IUser): Observable<IBackToken>;
    public abstract authorize(clientId: string, user: IUser): Observable<IBackToken>;
    public abstract authorizeUserInfo(clientId: string): Observable<IUser>;
    public abstract logout();
    //@TODO change when returned info has a model
    public abstract tokenInfo(clientId: string): Observable<any>;
    public abstract userInfo(): Observable<IUserInfo>
}
