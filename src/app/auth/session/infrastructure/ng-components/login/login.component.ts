import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {exchangeToken} from '../../../application/exchangeToken';
import {SessionService} from '../../services/session.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {CookieFacadeService} from '../../services/cookie.service';
import {authConfig} from '../../../../config/auth.config';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    //@TODO this ones should be config via .env
    private googleClientId = authConfig.googleClientId;
    private cookiePolicy = authConfig.cookiePolicy;
    private scope = authConfig.scope;
    private googleId = authConfig.googleId;
    private GAPI_URL = authConfig.GAPI_URL;
    private SESSION_STORAGE_KEY = authConfig.sessionStorageKey;
    private contador = 0;

    private auth2: any;

    @ViewChild('loginButton') private loginButton: ElementRef<HTMLButtonElement>;
    @ViewChild('loginButtonRefresh') private loginButtonRefresh: ElementRef<HTMLButtonElement>;
    @ViewChild('container') private container: ElementRef<HTMLButtonElement>;


    constructor(
        private sessionService: SessionService,
        private cookieFacade: CookieFacadeService,
        private ngZone: NgZone,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.googleSDK();
    }

    googleSDK() {
        (window as any)['googleSDKLoaded'] = () => {
            (window as any)['gapi'].load('auth2', () => {
                this.auth2 = (window as any)['gapi'].auth2.init({
                    client_id: this.googleClientId,
                    cookiepolicy: this.cookiePolicy,
                    scope: this.scope,
                });
                this.prepareLoginButton();
            });
        };

        const fjs = document.getElementsByTagName('script')[0];
        if (document.getElementById(this.googleId)) {
            return;
        }

        const js = document.createElement('script');
        js.id = this.googleId;
        js.src = this.GAPI_URL;
        fjs.parentNode.insertBefore(js, fjs);
    }

    prepareLoginButton() {
        this.container.nativeElement.addEventListener('click', () => {
            if (this.contador !== 5) {
                this.contador++;
                return;
            }
            this.loginButtonRefresh.nativeElement.style.display = 'inline';
            this.loginButton.nativeElement.style.display = 'none';
        });
        this.loginButtonRefresh.nativeElement.addEventListener('click', () => {
            this.auth2.grantOfflineAccess({redirect_uri: 'postmessage', approval_prompt: 'force'}).then((refreshToken) => {
                alert('dev refresh token, do not share: ' + refreshToken.code);
                this.loginButtonRefresh.nativeElement.style.display = 'none';
                this.loginButton.nativeElement.style.display = 'inline';
            });
        });
        this.auth2.attachClickHandler(this.loginButton.nativeElement, {},
            (googleUser: any) => {
                this.ngZone.run(() => {
                    this.loginComplete(googleUser.getAuthResponse().id_token);
                });
            },
            (error: any) => {
                console.error('error at GAPI', error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    confirmButtonColor: '#db5e5e'
                });
            }
        );
    }

    loginComplete(token: string) {
        exchangeToken(`Google ${token}`, this.sessionService)
            .subscribe(backToken => {
                this.cookieFacade.save(this.SESSION_STORAGE_KEY, backToken.idToken);
                this.router.navigate(['']);
            }, error => {
                console.error('error at auth API', error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    confirmButtonColor: '#db5e5e'
                });
            });
    }

}
