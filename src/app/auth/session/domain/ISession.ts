export interface ISession {
    id?: number,
    token: string,
    expeditionDate: string,
    expirationDate: string,
}
