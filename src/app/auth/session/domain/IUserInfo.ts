export interface IUserInfo {
    email: string,
    emailVerified: boolean,
    id: string,
    loggedWithGoogle: boolean,
    roles: string[],
    scopes: string[],
    username: string
}
