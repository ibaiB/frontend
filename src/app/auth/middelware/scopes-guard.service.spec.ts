import { TestBed } from '@angular/core/testing';

import { ScopesGuardService } from './scopes-guard.service';

describe('ScopesGuard', () => {
  let guard: ScopesGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ScopesGuardService);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
