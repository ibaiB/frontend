import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
  CanActivateChild,
} from "@angular/router";
import { forkJoin, Observable, of, pipe } from "rxjs";
import { map } from "rxjs/operators";
import { hasAccessToSubmoduleByPath } from "src/app/home/middleware/navbar-config";
import { getUserInfo } from "../session/application/getUserInfo";
import { IUserInfo } from "../session/domain/IUserInfo";
import { CookieFacadeService } from "../session/infrastructure/services/cookie.service";
import { SessionService } from "../session/infrastructure/services/session.service";

@Injectable({
  providedIn: "root",
})
export class ScopesGuardService implements CanActivateChild {
  private userInfo: IUserInfo;

  constructor(
    cookies: CookieFacadeService,
    private router: Router,
    private _sessionService: SessionService) {
      this.userInfo = JSON.parse(cookies.get("userInfo"));
      if (!this.userInfo) getUserInfo(this._sessionService).subscribe(
        (userInfo) => (this.userInfo = userInfo)
      );
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):| Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean| UrlTree {
    const moduleName = route.routeConfig.path;
    const $userInfo = this.userInfo
      ? of(this.userInfo)
      : getUserInfo(this._sessionService).pipe(
          map((userInfo) => (this.userInfo = userInfo))
        );

    return forkJoin($userInfo).pipe(
      map((res) => {
        if (hasAccessToSubmoduleByPath(moduleName, res[0].scopes)) {
          return true;
        }
        this.router.navigate(["forbidden"]);
        return false;
      })
    );
  }
}
