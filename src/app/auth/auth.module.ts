import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './session/infrastructure/ng-components/login/login.component';
import {UserMapper} from './user/infrastructure/user-mapper';
import {TokenMapper} from './session/infrastructure/token-mapper';
import {UserInfoMapper} from './session/infrastructure/user-info-mapper';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {RoleMapper} from './role/infrastructure/role-mapper';
import {ScopeMapper} from './scope/infrastructure/scope-mapper';
import {TokenInterceptorService} from "./session/infrastructure/services/token-interceptor.service";

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule
  ],
  exports: [
    LoginComponent
  ],
  providers: [
    UserMapper,
    TokenMapper,
    UserInfoMapper,
    RoleMapper,
    ScopeMapper,
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true}
  ]
})
export class AuthModule { }
