import {AbstractRoleService} from '../infrastructure/abstract-role-service';


export function getRoles(params: any, service: AbstractRoleService){
    return service.search(params);
}
