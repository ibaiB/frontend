import {AbstractRoleService} from '../infrastructure/abstract-role-service';

export function removeRole(id: string, service: AbstractRoleService) {
    return service.deleteRole(id);
}
