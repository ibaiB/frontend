import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { getRoles } from 'src/app/auth/role/application/getRoles';
import { getScopes } from 'src/app/auth/scope/application/getScopes';
import { RoleService } from 'src/app/auth/role/infrastructure/services/role.service';
import { ScopeService } from 'src/app/auth/scope/infrastructure/services/scope.service';
import {IRole} from '../../domain/IRole';
import {IScope} from '../../../scope/domain/IScope';


@Injectable({
  providedIn: 'root'
})
export class RoleDetailResolverService implements Resolve<any> {

  constructor(
    private roleService: RoleService,
    private scopeService: ScopeService,
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const $scopes = getScopes({page: 0, size: 1000}, this.scopeService);
    const $role = getRoles({id: route.params['id']}, this.roleService);

    const fork: Observable<any> = route.params['id'] ? forkJoin($scopes, $role) : forkJoin($scopes);

    return fork 
        .pipe(map(res => ({
          data: {
            scopes: res[0],
            role: res[1] ? this.join(res[1].content[0],res[0]) : null
          }
        })));
  }

  join(role: IRole, scopes: IScope[]): IRole {
    role.scopes = scopes
        .filter(s => role.scopes.includes(s.id))
        .map(s => s.name);

    return role;
  }
}
