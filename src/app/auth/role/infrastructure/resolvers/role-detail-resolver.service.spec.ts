import {TestBed} from '@angular/core/testing';

import {RoleDetailResolverService} from './role-detail-resolver.service';

describe('RoleDetailResolverService', () => {
  let service: RoleDetailResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoleDetailResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
