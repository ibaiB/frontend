import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { CookieFacadeService } from 'src/app/auth/session/infrastructure/services/cookie.service';
import { ITableFiltersConfig } from 'src/app/shared/custom-mat-elements/dynamic';
import {getRoles} from '../../application/getRoles';
import {RoleService} from '../services/role.service';

@Injectable({
  providedIn: 'root'
})
export class RoleTableResolverService implements Resolve<any> {

  constructor(
      private _service: RoleService,
      private _cookieService : CookieFacadeService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const filtersCache = this._cookieService.get('rolesFilterCache');
    const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('role-table-config'));
    const orderParams = {...tableFiltersConfig?.order} ?? {};
    const rolParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};

    return getRoles({...JSON.parse(filtersCache),...rolParams, ...orderParams}, this._service)
        .pipe(
            map(res => ({
              data: res,
              tableFiltersConfig: tableFiltersConfig,
              filtersValue:filtersCache
            })),
            catchError(err => {
              return of({
                data: null,
                error: err,
                message: 'Error on role table resolver, data couldn\'t be fetched'
              });
            })
        );
  }
}
