import {TestBed} from '@angular/core/testing';

import {RoleTableResolverService} from './role-table-resolver.service';

describe('RoleTableResolverService', () => {
  let service: RoleTableResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoleTableResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
