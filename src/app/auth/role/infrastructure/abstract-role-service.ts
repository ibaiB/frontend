import {Observable} from 'rxjs';
import {IRole} from '../domain/IRole';
import {IList} from '../../../shared/generics/IList';

export abstract class AbstractRoleService {
    public abstract createRole(role: IRole): Observable<IRole>;

    public abstract deleteRole(id: string): Observable<any>;

    public abstract updateScope(roleId: string, scopes: []): Observable<IRole>;

    public abstract search(params: any): Observable<IList<IRole>>;
}
