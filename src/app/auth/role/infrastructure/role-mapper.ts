export class RoleMapper {
    mapTo(params: any){
        const {id, name, value, description, scopes} = params;

        return {
            id: id,
            name: name,
            value: value,
            description: description,
            scopes
        }
    }
}
