import {Component, OnInit, ViewChild} from '@angular/core';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from '../../../../../shared/custom-mat-elements/dynamic';
import {filters, table} from './config';
import {IList} from '../../../../../shared/generics/IList';
import {DynamicTableMarkIiComponent} from '../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {ActivatedRoute, Router} from '@angular/router';
import {getRoles} from '../../../application/getRoles';
import {RoleService} from '../../services/role.service';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';

@Component({
    selector: 'app-roles-table',
    templateUrl: './roles-table.component.html',
    styleUrls: ['./roles-table.component.scss']
})
export class RolesTableComponent implements OnInit {
    tableConfig: ITableConfig = table;
    filters: IAuto[] = filters;
    filtersValue: any;
    tableData: IList<any>;
    queryPagination;
    queryOrder;
    queryFilters = {};

    loading: boolean = true;
    configVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        private _service: RoleService,
        private _cookieFacade: CookieFacadeService
    ) {
    }

    ngOnInit() {
        const {data, error, filtersValue, tableFiltersConfig} = this._route.snapshot.data['response'];

        if (data) {
            this.tableData = data;
            this.filtersValue = filtersValue;
            this.loading = false;
            parseTableFiltersConfig(tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = data.filtersValue ? this._clean((JSON.parse(data.filtersValue))) : {};
            this.tableConfig.pagination = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = tableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries({[this.tableConfig.order.orderField]: ''}).find(entry => entry[1] === '')[0] : '';
        }
        this.configFilterValue();
    }

    configFilterValue() {
        let queryFiltersAux = this.filtersValue ? this._clean(JSON.parse(this.filtersValue)) : {};
        this.filters.forEach(filter => {
            filter.defaultValue = queryFiltersAux[this.getFilterProp(filter)];
            filter.value = queryFiltersAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    filtersChanged($event: any) {
        this.queryFilters = $event && Object.keys($event).length !== 0
            ? $event
            : {};
        this._cookieFacade.save('rolesFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('role-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    paginationChanged($event: any) {
        this.queryPagination.page = $event.page;
        this.queryPagination.size = $event.size;
        this._cookieFacade.save('role-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged($event: any) {
        const aux = {[$event.orderField]: ''};

        this.queryOrder.orderField = Object
            .entries(aux)
            .find(entry => entry[1] === '')[0];

        this.queryOrder.order = $event.order;
        this.queryPagination.page = $event.page;
        this.queryPagination.size = $event.size;
        this._cookieFacade.save('role-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('role-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }

    backSearch() {
        getRoles({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._service)
            .subscribe(response => {
                this.tableData = {
                    content: response.content,
                    totalElements: response.totalElements,
                    numberOfElements: response.numberOfElements,
                    totalPages: response.totalPages
                };
            });
    }

    navigate() {
        this._router.navigate(['new'], {relativeTo: this._route});
    }
}
