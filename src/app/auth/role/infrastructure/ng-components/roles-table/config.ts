import {IAuto, ITableConfig} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
    multiSelect: false,
    columns: [
        {
            name: 'Id',
            prop: 'id',
            shown: true,
            route: './app/systems-administrator/roles/role-detail/',
            cellClass: 'id-table-column',
            routeId: 'id'
        },
        {
            name: 'Name',
            prop: 'name',
            shown: true,
        },
        {
            name: 'Description',
            prop: 'description',
            shown: true,
        },
        {
            name: 'Value',
            prop: 'value',
            shown: true,
        }
    ]
};

export const filters: IAuto[] = [
    {
        options: [],
        prop: 'id',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Id',
        placeholder: 'Id',
        shown: true,
    },
    {
        options: [],
        prop: 'name',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Name',
        placeholder: 'Name',
        shown: true,
    },
    {
        options: [],
        prop: 'value',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Value',
        placeholder: 'Value',
        shown: true,
    },
];
