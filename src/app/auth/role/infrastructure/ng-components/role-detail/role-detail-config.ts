export interface Module {
    name: string;
    path: string;
    children: Concept[];
}

export interface Concept {
    name: string;
    path?: string;
    icon: string;
    children?: Concept[];
    scopeCreate?: string[];
    scopeRead?: string[];
    isLeaf?: boolean;
}

export interface ExtendedConcept {
    name: string;
    scopeCreate: string[];
    scopeRead: string[];
    createChecked: boolean;
    readChecked: boolean;
    children: Concept[];
}

export const config: Module[] = [
    {
        name: 'Staffit',
        path: 'my-staffit',
        children: [
            {
                name: 'All employees',
                path: 'employees',
                icon: 'face',
                scopeCreate: [],
                scopeRead: [],
                isLeaf: true
            },
            {
                name: 'Who is who?',
                path: 'who-is-who',
                icon: 'quiz',
                scopeCreate: [],
                scopeRead: [],
                isLeaf: true
            },
            {
                name: 'Rooms',
                path: 'rooms',
                icon: 'weekend',
                scopeCreate: [],
                scopeRead: [],
                isLeaf: true
            },
            {
                name: 'Know the company',
                path: 'know-the-company',
                icon: 'help',
                scopeCreate: [],
                scopeRead: [],
                isLeaf: true
            },
        ]
    },
    {
        name: 'Systems Administrator',
        path: 'systems-administrator',
        children: [
            {
                name: 'Admin panels',
                path: 'admin-panels',
                icon: 'analytics',
                scopeCreate: [],
                scopeRead: [],
                isLeaf: true
            },
            {
                name: 'Users',
                path: 'users',
                icon: 'group',
                scopeCreate: [
                    'USUARIO_CREATE',
                    'USUARIO_UPDATE',
                    'USUARIO_DELETE',
                    'Update scope users',
                    'USUARIO_READ',
                    'Read scope users'
                ],
                scopeRead: ['USUARIO_READ', 'Read scope users'],
                isLeaf: true
            },
            {
                name: 'Roles',
                path: 'roles',
                icon: 'admin_panel_settings',
                scopeCreate: [
                    'Create scope admin',
                    'Update scope admin',
                    'Delete scope admin',
                    'Read scope admin'
                ],
                scopeRead: ['Read scope admin'],
                isLeaf: true
            },
        ]
    },
    {
        name: 'Internal',
        path: 'internal',
        children: [
            {
                name: 'Employee management',
                icon: 'supervisor_account',
                path: 'employee',
                scopeCreate: [],
                scopeRead: [],
                children: [
                    {
                        name: 'Reports',
                        path: 'reports',
                        icon: 'groups',
                        scopeCreate: ['EMPLOYEE_CREATE', 'EMPLOYEE_UPDATE', 'EMPLOYEE_DELETE', 'EMPLOYEE_READ', 'EMPLOYEEAUX_READ', 'COMPANY_READ', 'CATEGORY_READ', 'CAR_READ', 'SUBCAR_READ'],
                        scopeRead: ['EMPLOYEE_READ', 'EMPLOYEEAUX_READ', 'COMPANY_READ', 'CATEGORY_READ', 'CAR_READ', 'SUBCAR_READ'],
                        isLeaf: true
                    },
                    {
                        name: 'Requests',
                        path: 'new-requests',
                        icon: 'person_adds',
                        scopeCreate: ['NEWEMPLOYEE_CREATE', 'NEWEMPLOYEE_UPDATE', 'NEWEMPLOYEE_DELETE', 'NEWEMPLOYEE_READ', 'CAR_READ', 'SUBCAR_READ', 'CENTER_READ'],
                        scopeRead: ['NEWEMPLOYEE_READ', 'CAR_READ', 'SUBCAR_READ', 'CENTER_READ'],
                        isLeaf: true
                    }
                ]
            },
            {
                name: 'Billing',
                path: 'billing',
                icon: 'euro',
                scopeCreate: [],
                scopeRead: [],
                isLeaf: true
            },
            {
                name: 'Room management',
                path: 'room-management',
                icon: 'weekend',
                scopeCreate: [],
                scopeRead: [],
                isLeaf: true
            },
            {
                name: 'Admin know the company',
                path: 'admin-know-the-company',
                icon: 'help',
                scopeCreate: [],
                scopeRead: [],
                isLeaf: true
            }
        ]
    },
    {
        name: 'Recruitment',
        path: 'recruitment',
        children: [
            {
                name: 'Campaigns',
                path: 'campaigns',
                icon: 'campaign',
                scopeCreate: ['CAMPAIGN_CREATE', 'CAMPAIGN_UPDATE', 'CAMPAIGN_DELETE', 'CAMPAIGN_READ', 'COLLEGE_READ', 'STUDY_READ', 'CANDIDATE_READ'],
                scopeRead: ['CAMPAIGN_READ', 'COLLEGE_READ', 'STUDY_READ', 'CANDIDATE_READ'],
                isLeaf: true
            },
            {
                name: 'Candidates',
                path: 'candidates',
                icon: 'person_search',
                scopeCreate: ['CANDIDATE_CREATE', 'CANDIDATE_UPDATE', 'CANDIDATE_DELETE', 'CANDIDATE_READ', 'COLLEGE_READ', 'EMPLOYEE_READ', 'TECHNOLOGY_READ', 'STUDY_READ'],
                scopeRead: ['CANDIDATE_READ', 'STUDY_READ', 'COLLEGE_READ', 'TECHNOLOGY_READ', 'EMPLOYEE_READ'],
                isLeaf: true
            },
            {
                name: 'Colleges',
                path: 'colleges',
                icon: 'local_library',
                scopeCreate: ['COLLEGE_CREATE', 'COLLEGE_UPDATE', 'COLLEGE_DELETE', 'STUDY_READ', 'COLLEGE_READ'],
                scopeRead: ['COLLEGE_READ', 'STUDY_READ'],
                isLeaf: true
            },
            {
                name: 'Studies',
                path: 'studies',
                icon: 'school',
                scopeCreate: ['STUDY_CREATE', 'STUDY_UPDATE', 'STUDY_DELETE', 'STUDY_READ'],
                scopeRead: ['STUDY_READ'],
                isLeaf: true
            }
        ]
    },
    {
        name: 'Evaluations',
        path: 'evaluations',
        children: [
            {
                name: 'Tests',
                path: 'tests',
                icon: 'note_add',
                scopeCreate: ['TEST_CREATE', 'TEST_UPDATE', 'TEST_DELETE', 'TEST_READ', 'CORRECTION_READ'],
                scopeRead: ['TEST_READ', 'CORRECTION_READ'],
                isLeaf: true
            },
            {
                name: 'Exams',
                path: 'exams',
                icon: 'assignment',
                scopeCreate: ['EXAM_CREATE', 'EXAM_UPDATE', 'EXAM_DELETE', 'EXAMSENT_CREATE', 'EXAMSENT_UPDATE', 'EXAMSENT_DELETE', 'TEST_READ', 'CORRECTION_READ', 'QUESTION_CREATE', 'QUESTION_DELETE', 'EXAM_READ', 'EXAMSENT_READ'],
                scopeRead: ['EXAM_READ', 'CORRECTION_READ', 'EXAMSENT_READ', 'TEST_READ'],
                isLeaf: true
            }
        ]
    },
    {
        name: 'Crm',
        path: 'crm',
        children: [
            {
                name: 'Accounts',
                path: 'accounts',
                icon: 'business',
                scopeCreate: ['CUENTA_CREATE', 'CUENTA_UPDATE', 'CUENTA_DELETE', 'CUENTA_READ', 'CAR_READ', 'SUBCAR_READ', 'TERRITORIO_READ', 'USUARIO_READ'],
                scopeRead: ['CUENTA_READ', 'CAR_READ', 'SUBCAR_READ', 'TERRITORIO_READ', 'USUARIO_READ'],
                isLeaf: true
            },
            {
                name: 'Contacts',
                path: 'contacts',
                icon: 'contacts',
                scopeCreate: ['CONTACTO_CREATE', 'CONTACTO_UPDATE', 'CONTACTO_DELETE', 'CONTACTO_READ', 'CAR_READ', 'SUBCAR_READ', 'USUARIO_READ'],
                scopeRead: ['CONTACTO_READ', 'CAR_READ', 'SUBCAR_READ', 'USUARIO_READ'],
                isLeaf: true
            },
            {
                name: 'Activities',
                path: 'activities',
                icon: 'event',
                scopeCreate: ['ACTIVIDAD_CREATE', 'ACTIVIDAD_UPDATE', 'ACTIVIDAD_DELETE', 'ACTIVIDAD_READ', 'USUARIO_READ', 'CAR_READ', 'SUBCAR_READ'],
                scopeRead: ['ACTIVIDAD_READ', 'USUARIO_READ', 'CAR_READ', 'SUBCAR_READ'],
                isLeaf: true
            },
            {
                name: 'Opportunities',
                path: 'opportunities',
                icon: 'euro',
                scopeCreate: ['OPORTUNIDAD_CREATE', 'OPORTUNIDAD_UPDATE', 'OPORTUNIDAD_DELETE', 'TRACKING_OPORTUNIDAD_CREATE', 'TRACKING_OPORTUNIDAD_UPDATE', 'OPORTUNIDAD_READ', 'TRACKING_OPORTUNIDAD_DELETE', 'TRACKING_OPORTUNIDAD_READ', 'USUARIO_READ', 'CUENTA_READ', 'CONTACTO_READ', 'ESTADO_READ', 'CAR_READ', 'SUBCAR_READ'],
                scopeRead: ['OPORTUNIDAD_READ', 'TRACKING_OPORTUNIDAD_READ', 'USUARIO_READ', 'CUENTA_READ', 'CONTACTO_READ', 'ESTADO_READ', 'CAR_READ', 'SUBCAR_READ'],
                isLeaf: true
            }
        ]
    }
];