import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {ActivatedRoute, Router} from '@angular/router';
import {IRole} from 'src/app/auth/role/domain/IRole';
import {IScope} from 'src/app/auth/scope/domain/IScope';
import {RoleService} from 'src/app/auth/role/infrastructure/services/role.service';
import {questionAlert, successAlert} from 'src/app/shared/error/custom-alerts';
import {Concept, config, ExtendedConcept} from './role-detail-config';
import {removeRole} from '../../../application/removeRole';

@Component({
    selector: 'app-role-detail',
    templateUrl: './role-detail.component.html',
    styleUrls: ['./role-detail.component.scss'],
})
export class RoleDetailComponent implements OnInit {
    form: FormGroup;
    loading: boolean = true;
    cantBeChanged: boolean;
    role: IRole;
    allScopes: IScope[];
    checkedScopes: IScope[];
    allConcepts: ExtendedConcept[];
    changedConcepts: any[];
    modules = config;

    constructor(
        private _fb: FormBuilder,
        private _service: RoleService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
    }

    ngOnInit(): void {
        const {data} = this._route.snapshot.data['response'];

        if (data) {
            this.role = data.role;
            this.allScopes = data.scopes;
            this.checkedScopes = this.role?.fullScopes ?? [];
            this.loading = false;
            this.cantBeChanged = this.role ? ['ROLE_ADMIN', 'ROLE_USER'].includes(this.role.value) : false;
            this.initForm();
            this._loadExtendedConcepts();
        }
    }

    private _loadExtendedConcepts() {
        this.allConcepts = [];
        this.modules.forEach(module => {
            module.children.forEach(concept => {
                this._parseTreeNode(concept);
                if (!concept.children) {
                    return;
                }
                concept.children.forEach(conceptChild => {
                    this._parseTreeNode(conceptChild);

                    // Add scopes of child to its parent
                    concept.scopeCreate.push(...conceptChild.scopeCreate);
                    concept.scopeRead.push(...conceptChild.scopeRead);
                });

                // Remove duplicates
                concept.scopeCreate = [...new Set(concept.scopeCreate)];
                concept.scopeRead = [...new Set(concept.scopeRead)];
            });
        });
    }

    private _parseTreeNode(concept: Concept) {
        this.allConcepts.push({
            name: concept.name,
            scopeCreate: concept.scopeCreate,
            scopeRead: concept.scopeRead,
            createChecked: this.allAreChecked(concept.scopeCreate),
            readChecked: this.allAreChecked(concept.scopeRead),
            children: concept.children
        });
    }

    private _resetConceptsCheckedState() {
        this.changedConcepts = [];
        this.allConcepts.forEach(concept => {
            const oldCreateChecked = concept.createChecked;
            const oldReadChecked = concept.readChecked;

            concept.createChecked = this.allAreChecked(concept.scopeCreate);
            concept.readChecked = this.allAreChecked(concept.scopeRead);

            if (!concept.children) {
                if (oldCreateChecked !== concept.createChecked) {
                    this.changedConcepts.push({name: concept.name, mode: 'create'});
                }
                if (oldReadChecked !== concept.readChecked) {
                    this.changedConcepts.push({name: concept.name, mode: 'read'});
                }
            }
        });
    }

    /* FORM LOGIC */

    initForm() {
        if (this.role != null) {
            this.form = this._fb.group({
                name: [{value: this.role.name, disabled: true}],
                value: [{value: this.role.value, disabled: true}],
                description: [{value: this.role.description, disabled: true}],
            });
        } else {
            this.form = this._fb.group({
                name: ['', Validators.required],
                value: ['ROLE_', Validators.required],
                description: ['', Validators.required],
            });
        }
    }

    onSaveClick() {
        if (this.role) {
            this.update();
            return;
        }
        if (this.form.valid) {
            this.create();
        }
    }

    create() {
        const role: IRole = this.form.value;
        this._service.createRole(role).subscribe(
            (res) => {
                successAlert('The role was successfully saved');
                this.updateScope(res.id);
            }
        );
    }

    update() {
        this.updateScope(this.role.id);
    }

    updateScope(idRole: string) {
        const ids: string[] = this.checkedScopes.map(scope => scope.id);

        this._service.updateScope(idRole, ids).subscribe(
            () => {
                successAlert('The scopes were successfully added');
            }
        );
    }

    remove() {
        removeRole(this.role.id, this._service)
            .subscribe(() => {
                successAlert('The scopes were successfully added');
                this._router.navigate(['./app/systems-administrator/roles']);
            });
    }

    /* CHECKBOX LOGIC */

    isChecked(scope: IScope): boolean {
        return !!this.checkedScopes.find(cs => cs.id === scope.id);
    }

    allAreChecked(moduleScopeNames: string[]): boolean {
        const checkedModuleScopes = this.checkedScopes.filter(cs => moduleScopeNames.includes(cs.name));
        return checkedModuleScopes.length === moduleScopeNames.length;
    }

    onSelectAll(event: MatCheckboxChange) {
        this.checkedScopes = event.checked ? [...this.allScopes] : [];
    }

    onConceptChange(scopeNames: string[], event: MatCheckboxChange, mode: string, conceptName: string) {
        // Case 1: concept checked
        if (event.checked) {
            this._addScopes(scopeNames);
            this._resetConceptsCheckedState();
            return;
        }

        // Case 2: concept unchecked
        const scopes = this.allScopes.filter(scope => scopeNames.includes(scope.name));
        const concept = this.allConcepts.find(c => c.name === conceptName);

        this._resetConceptsCheckedState();
        const notSharedScopes = this._findNotSharedScopes(scopes, concept, mode);

        if (notSharedScopes.length === 0) {
            // Case 2.1: all scopes shared with selected concepts
            const oldCheckedScopes = this.checkedScopes;
            this.checkedScopes = this.checkedScopes.filter(cs => !scopeNames.includes(cs.name));
            this._resetConceptsCheckedState();

            const htmlMessage =
                `All scopes of concept <b>${conceptName} (${mode})</b> are shared with other selected concepts.<br><br>
        If you continue, the following concepts will be deselected:<br>
        <ul style="text-align: left">
          ${this.changedConcepts.map(c => `<li>${c.name} (${c.mode})</li>`).join('')}
        </ul>`;

            questionAlert(htmlMessage).then(result => {
                if (result.isConfirmed) {
                    successAlert('Concepts deselected');
                    this._cleanOrphanScopes();

                } else {
                    this.checkedScopes = oldCheckedScopes;
                    this._resetConceptsCheckedState();
                }
            });

        } else {
            // Case 2.2: some/no scopes shared with selected concepts
            this.checkedScopes = this.checkedScopes.filter(cs => !notSharedScopes.includes(cs.id));
            this._resetConceptsCheckedState();
            this._cleanOrphanScopes();
        }
    }

    onScopeChange(scope: IScope, event: MatCheckboxChange) {
        if (event.checked) {
            const checkedScope = this.checkedScopes.find(cs => cs.id === scope.id);
            if (!checkedScope) {
                this.checkedScopes.push(scope);
            }
        } else {
            const index = this.checkedScopes.findIndex(cs => cs.id === scope.id);
            if (index > -1) {
                this.checkedScopes.splice(index, 1);
            }
        }
    }

    private _findNotSharedScopes(scopes: IScope[], concept: any, mode: string) {
        const notSharedScopes = [];
        let isScopeInAnotherConcept: boolean;

        scopes.forEach((scope) => {
            isScopeInAnotherConcept = this.allConcepts.some(c => {
                // Same concept
                if (c.name === concept?.name) {
                    switch (mode) {
                        case 'create':
                            return c.readChecked && c.scopeRead.includes(scope.name);
                        case 'read':
                            return false;
                    }
                }

                // Ignore concept if related (child or parent)
                if (!!concept?.children?.find(child => child.name === c.name)) {
                    return false;
                }
                if (concept && !!c.children?.find(child => child.name === concept.name)) {
                    return false;
                }

                return (c.createChecked && c.scopeCreate.includes(scope.name)) ||
                    (c.readChecked && c.scopeRead.includes(scope.name));
            });

            if (!isScopeInAnotherConcept) {
                notSharedScopes.push(scope.id);
            }
        });

        return notSharedScopes;
    }

    private _addScopes(scopeNames: string[]) {
        const scopes = this.allScopes.filter(scope => scopeNames.includes(scope.name));
        scopes.forEach((scope) => {
            const checkedScope = this.checkedScopes.find(cs => cs.id === scope.id);
            if (!checkedScope) {
                this.checkedScopes.push(scope);
            }
        });
    }

    private _cleanOrphanScopes() {
        const orphanScopes = this._findNotSharedScopes(this.checkedScopes, null, null);
        this.checkedScopes = this.checkedScopes.filter(cs => !orphanScopes.includes(cs.id));
    }

}
