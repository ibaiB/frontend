import {Injectable} from '@angular/core';
import {AbstractRoleService} from '../abstract-role-service';
import {Observable} from 'rxjs';
import {IRole} from '../../domain/IRole';
import {HttpClient, HttpParams} from '@angular/common/http';
import {RoleMapper} from '../role-mapper';
import {environment} from '../../../../../environments/environment';
import {IList} from '../../../../shared/generics/IList';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends AbstractRoleService {

  private BASE_URL: string = environment.baseUrl;
  private ENDPOINT: string = 'roles';

  constructor(private httpClient: HttpClient, private mapper: RoleMapper) {
    super();
  }

  addScope(roleId: string, scopes: string[]): Observable<IRole> {
    return this.httpClient
      .post<IRole>(`${this.BASE_URL}${this.ENDPOINT}/${roleId}/scopes`, { scope_ids : scopes });
  }

  updateScope(roleId: string, scopes: string[]): Observable<IRole> {
    return this.httpClient
        .put<IRole>(`${this.BASE_URL}${this.ENDPOINT}/${roleId}/scopes`, { scope_ids : scopes });
  }

  createRole(role: IRole): Observable<IRole> {
    return this.httpClient
        .post<IRole>(`${this.BASE_URL}${this.ENDPOINT}`, role);
  }

  deleteRole(id: string): Observable<any> {
    return this.httpClient
        .delete(`${this.BASE_URL}${this.ENDPOINT}/${id}`);
  }

  search(params: any): Observable<IList<IRole>> {
    return this.httpClient.get<IList<IRole>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: params})});
  }
}
