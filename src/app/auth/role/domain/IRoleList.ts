import {IRole} from './IRole';

export interface IRoleList{
    content: IRole[];
    contentLength: number;
    totalElements: number;
    totalPages: number
}
