export interface IRole {
    id?: string;
    name: string;
    value: string;
    description: string;
    scopes: string[];
    fullScopes: any[];
}
