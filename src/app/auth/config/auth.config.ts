export const authConfig = {
    sessionStorageKey: 'staffit-auth',
    googleClientId: "680896564485-15ltf1tg733jk9qr7465rloqpkf0gri6.apps.googleusercontent.com",
    cookiePolicy: "single_host_origin",
    scope: "profile email",
    googleId: "google-jssdk",
    GAPI_URL: "https://apis.google.com/js/platform.js?onload=googleSDKLoaded",
    SESSION_STORAGE_KEY: 'staffit-auth'
}