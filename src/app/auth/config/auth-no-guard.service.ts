import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {authConfig} from "./auth.config";
import {CookieFacadeService} from "../session/infrastructure/services/cookie.service";

@Injectable({
  providedIn: 'root'
})
export class AuthNoGuardService implements CanActivate {
  constructor(private cookieFacade: CookieFacadeService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.cookieFacade.contains(authConfig.sessionStorageKey) ? this.router.parseUrl('/app/my-staffit/my-employee') : true;
  }
}
