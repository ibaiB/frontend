import { TestBed } from '@angular/core/testing';

import { AuthNoGuardService } from './auth-no-guard.service';

describe('AuthNoGuardService', () => {
  let service: AuthNoGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthNoGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
