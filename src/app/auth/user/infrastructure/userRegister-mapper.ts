export class UserRegisterMapper {
    mapTo(params: any) {
        const {userId, username, email, password} = params;

        return {
            userId: userId,
            username: username,
            email: email,
            password:password
        }
    }
}