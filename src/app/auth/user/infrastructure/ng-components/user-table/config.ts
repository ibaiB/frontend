import {IAuto, ITableConfig} from 'src/app/shared/custom-mat-elements/dynamic';

export const table: ITableConfig = {
    multiSelect: false,
    columns: [
        {
            name: 'Id',
            prop: 'id',
            shown: true,
            route: './app/systems-administrator/users/user-detail/',
            cellClass: 'id-table-column',
            routeId: 'id'
        },
        {
            name: 'Username',
            prop: 'username',
            shown: true,
        },
        {
            name: 'Email',
            prop: 'email',
            shown: true,
        },
        {
            name: 'Status',
            prop: 'lockedValue',
            shown: true
        }
    ]
};

export const filters: IAuto[] = [
    {
        options: [],
        prop: 'id',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Id',
        placeholder: 'Id',
        shown: true,
    },
    {
        options: [],
        prop: 'username',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Username',
        placeholder: 'Username',
        shown: true,
    },
    {
        options: [],
        prop: 'email',
        retProp: '',
        type: 'input',
        appearance: 'outline',
        class: 'input',
        label: 'Email',
        placeholder: 'Email',
        shown: true,
    },
];
