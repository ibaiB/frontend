import {Component, OnInit, ViewChild} from '@angular/core';
import {createTableFiltersConfig, IAuto, ITableConfig, parseTableFiltersConfig} from '../../../../../shared/custom-mat-elements/dynamic';
import {IList} from '../../../../../shared/generics/IList';
import {DynamicTableMarkIiComponent} from '../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component';
import {ActivatedRoute, Router} from '@angular/router';
import {filters, table} from './config';
import {getUsers} from '../../../application/getUsers';
import {UserService} from '../../services/user.service';
import {IUser} from '../../../domain/IUser';
import Swal from 'sweetalert2';
import {lock} from '../../../application/lock';
import {unlock} from '../../../application/unlock';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {CookieFacadeService} from 'src/app/auth/session/infrastructure/services/cookie.service';

@Component({
    selector: 'app-user-table',
    templateUrl: './user-table.component.html',
    styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {

    tableConfig: ITableConfig = table;
    filters: IAuto[] = filters;
    tableData: IList<any>;
    queryPagination = {page: 0, size: 10};
    queryOrder = {orderField: '', order: ''};
    queryFilters = {};
    filtersValue: any;

    loading: boolean = true;
    configVisibility: boolean = false;

    @ViewChild('table') dynamicTable: DynamicTableMarkIiComponent;

    constructor(
        private _router: Router,
        private _route: ActivatedRoute,
        private _service: UserService,
        private _cookieFacade: CookieFacadeService
    ) {

    }

    ngOnInit() {
        const {data, filtersValue, tableFiltersConfig} = this._route.snapshot.data['response'];

        if (data) {
            this.tableData = {
                content: data.content.map(i => {
                    return {
                        id: i.id,
                        username: i.username,
                        email: i.email,
                        lockedValue: i.locked ? 'Locked' : 'Unlocked'
                    };
                }),
                numberOfElements: data.numberOfElements,
                totalElements: data.totalElements,
                totalPages: data.totalPages
            };
            this.filtersValue = filtersValue;
            parseTableFiltersConfig(tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = data.filtersValue ? this._clean((JSON.parse(data.filtersValue))) : {};
            this.tableConfig.pagination = tableFiltersConfig?.pagination ?? {page: 0, size: 10};
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = tableFiltersConfig?.order ?? {orderField: '', order: ''};
            this.queryOrder = {...this.tableConfig.order};
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ?
                Object.entries({[this.tableConfig.order.orderField]: ''}).find(entry => entry[1] === '')[0] : '';
            this.loading = false;
        }
        this.configFilterValue();
    }

    configFilterValue() {
        let queryFiltersAux = this.filtersValue ? this._clean(JSON.parse(this.filtersValue)) : {};

        this.filters.forEach(filter => {
            filter.defaultValue = queryFiltersAux[this.getFilterProp(filter)];
            filter.value = queryFiltersAux[this.getFilterProp(filter)];
        });
    }

    getFilterProp(filter: any) {
        return filter.prop ? (filter.type === 'autoselect' ? filter.searchValue : filter.prop) : filter.searchValue;
    }

    private _clean(object): any {
        const cleaned = {};
        const keys = Object.keys(object);
        keys.forEach(key => {
            if (object[key]) {
                cleaned[key] = object[key];
            }
        });
        return cleaned;
    }

    filtersChanged($event: any) {
        this.queryFilters = $event && Object.keys($event).length !== 0
            ? $event
            : {};
        this._cookieFacade.save('userFilterCache', JSON.stringify(this.queryFilters));
        this.queryPagination = {page: 0, size: this.queryPagination.size};
        this._cookieFacade.save('user-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.dynamicTable.resetPageIndex();
        this.backSearch();
    }

    paginationChanged($event: any) {
        this.queryPagination.page = $event.page;
        this.queryPagination.size = $event.size;
        this._cookieFacade.save('user-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    orderChanged($event: any) {
        const aux = {[$event.orderField]: ''};

        this.queryOrder.orderField = Object
            .entries(aux)
            .find(entry => entry[1] === '')[0];

        this.queryOrder.order = $event.order;
        this.queryPagination.page = $event.page;
        this.queryPagination.size = $event.size;
        this._cookieFacade.save('user-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        this.backSearch();
    }

    doActions(event: { action: string, item: IUser }) {
        if (event.action === 'Lock') {
            event.item.locked ? this.unlock(event.item) : this.lock(event.item);
        }
    }

    lock(user: IUser) {
        Swal.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: `${user.email} will be locked`,
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080',
        }).then(result => {
            if (result.isConfirmed) {
                lock(user.id, this._service)
                    .subscribe(res => {
                        successAlert('Success');
                    });
            }
        });
    }

    unlock(user: IUser) {
        Swal.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: `${user.email} will be unlocked`,
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080',
        }).then(result => {
            if (result.isConfirmed) {
                unlock(user.id, this._service)
                    .subscribe(res => {
                        successAlert('Success');
                    });
            }
        });
    }

    backSearch() {
        getUsers({...this.queryPagination, ...this.queryOrder, ...this.queryFilters}, this._service)
            .subscribe(response => {
                this.tableData = {
                    content: response.content.map(i => {
                        return {
                            id: i.id,
                            username: i.username,
                            email: i.email,
                            lockedValue: i.locked ? 'Locked' : 'Unlocked'
                        };
                    }),
                    numberOfElements: response.numberOfElements,
                    totalElements: response.totalElements,
                    totalPages: response.totalPages
                };
            });
    }

    configChange(event: any) {
        this.tableConfig = {...event[0]};
        this.filters = event[1];
        this._cookieFacade.save('user-table-config', JSON.stringify(createTableFiltersConfig(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
    }

    setConfigVisibility(visible: boolean) {
        this.configVisibility = visible;
    }
}
