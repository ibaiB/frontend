import {Component, OnInit, ViewChild} from '@angular/core';
import {IScope} from '../../../../scope/domain/IScope';
import {IRole} from '../../../../role/domain/IRole';
import {IUser} from '../../../domain/IUser';
import {UserService} from '../../services/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import {updateUserRoles} from '../../../application/updateUserRoles';
import {ActivatedRoute, Router} from '@angular/router';
import {successAlert} from '../../../../../shared/error/custom-alerts';
import {lock} from '../../../application/lock';
import {unlock} from '../../../application/unlock';
import {MatSlideToggle} from '@angular/material/slide-toggle';
import {MatSelectChange} from '@angular/material/select';
import {addPersonalUserRol} from 'src/app/auth/user/application/addPersonalRol';
import {AutocompleteComponent} from 'src/app/shared/custom-mat-elements/autocomplete/autocomplete.component';
import {IRegisterUser} from 'src/app/auth/user/domain/IRegisterUser';
import {registerNewUser} from 'src/app/auth/user/application/registerNewUser';
import {UserRegisterMapper} from 'src/app/auth/user/infrastructure/userRegister-mapper';
import {createUserName} from 'src/app/auth/user/application/createUserName';

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
    public form: FormGroup;
    loading: boolean = true;
    scopes: IScope[];
    roles: IRole[];
    data: any;
    user: IUser;
    updatedRoles: string[];
    selected: string;
    userRolesId: any;
    rolesName: any;
    route: string;
    checkedPassword: boolean = false;
    hide = true;
    strongPassword = false;

    @ViewChild('toggle') toggle: MatSlideToggle;
    @ViewChild('filtro') filtro: AutocompleteComponent;

    constructor(
        private _router: Router,
        private _service: UserService,
        private _route: ActivatedRoute,
        private _fb: FormBuilder,
        private _userRegisterMapper: UserRegisterMapper
    ) {
    }

    ngOnInit(): void {
        const {data} = this._route.snapshot.data['response'];

        if (data) {
            this.data = data;
            this.user = data.user;
            this.roles = this.user ? data.user.fullRoles : data.roles;
            this.scopes = data.scopes;
            this.updatedRoles = this.user ? this.roles.map(rol => rol.value) : [];
            this.userRolesId = this.user ? this.roles.map(rol => rol.id) : [];
            this.rolesName = this.roles.map(rol => rol.name);
            this.selected = this.user ? 'user' : 'all';
            this.route = this.user ? '../../../roles/role-detail/' : '../../roles/role-detail/';
            this.initForm();

            this.loading = false;
        }
    }

    private initForm() {
        this.form = this._fb.group({
            id: [{value: this.user?.id, disabled: true}],
            email: [{value: this.user ? this.user?.email : '', disabled: this.user ? true : false},
                Validators.compose([Validators.email, Validators.required])],
            username: [{value: this.user?.username ?? '', disabled: this.user ? true : false}, Validators.required],
            password: [{value: '', disabled: this.user ? true : false}],
            cpassword: [{value: '', disabled: this.user ? true : false}]
        });
    }

    checkPassword() {
        return this.checkedPassword = this.form.get('password').value === this.form.get('cpassword').value ? true : false;
    }

    completeUserName() {
        this.form.get('email').value ? this.form.get('username').setValue(createUserName(this.form.get('email').value)) : this.form.get('username').setValue('');
    }

    checkUserName() {
        return this.form.get('username').value.includes(' ');
    }

    detectRoleChange(role: IRole) {
        if (this.updatedRoles.includes(role.value)) {
            for (let i = 0; i < this.updatedRoles.length; i++) {
                if (this.updatedRoles[i] === role.value) {
                    this.updatedRoles.splice(i, 1);
                }
            }

        } else {
            this.updatedRoles.push(role.value);
        }
    }

    onSaveClick() {
        if (!this.checkPassword() || this.checkUserName()) {
            return;
        }
        const valid = this.user ? true : (this.form.valid && (this.form.get('password').value && this.form.get('cpassword').value));

        if (valid) {
            this.user ? this.applyChanges() : this.create();
            return;
        }
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Some fields are required',
        });
    }

    applyChanges() {
        Swal.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: `${this.user.email} will now be able to do screw things up`,
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080',
        }).then(result => {
            if (result.isConfirmed) {
                this.switchToRoleKeys();
                updateUserRoles(this.user.id, this.updatedRoles, this._service)
                    .subscribe(user => {
                        successAlert('User roles were correctly updated');
                        this.user = user;
                        this.data.user.fullRoles = this.data.roles.filter(rol => user.roles.includes(rol.id));
                        this.updatedRoles = this.data.user.fullRoles.map(rol => rol.value);
                        this.roles = this.selected === 'all' ? this.roles : this.data.user.fullRoles;
                        this.userRolesId = this.user.roles;
                        if (this.selected === 'user') {
                            this.filtro.clear();
                        }
                    });
            }
        });
    }

    create() {
        let regEx = new RegExp('^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{8,16}$');
        this.strongPassword = !regEx.test(this.form.get('password').value);
        if (this.strongPassword) {
            return;
        }
        const registerUser: IRegisterUser = this._userRegisterMapper.mapTo(this.form.value);
        registerNewUser(registerUser, this._service)
            .subscribe(user => {
                successAlert('User roles were correctly updated');
                this.user = user;
                this.data.user = user;
                this.data.user.fullRoles = [this.data.roles.find(rol => rol.value === 'ROLE_USER')];
                this.updatedRoles = [this.data.roles.find(rol => rol.value === 'ROLE_USER').value];
                this.userRolesId = [this.data.roles.find(rol => rol.value === 'ROLE_USER').id];
                if (this.selected === 'user') {
                    this.filtro.clear();
                }
                this.initForm();
            });
    }

    switchToRoleKeys() {
        const roles = this.data.roles.filter(role => this.updatedRoles.includes(role.value));
        this.updatedRoles = roles.map(role => role.id);
    }

    toggleChange() {
        this.user.locked ? this.showUnlockDialog() : this.showLockDialog();
    }

    showLockDialog() {
        Swal.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: `${this.user.email} will be locked`,
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080',
        }).then(result => {
            result.isConfirmed ? this.lock() : this.toggle.checked = false;
        });
    }

    lock() {
        lock(this.user.id, this._service)
            .subscribe(() => {
                successAlert('Success')
                    .then(() => {
                        this.user.locked = true;
                        this.toggle.checked = true;
                    });
            });
    }

    showUnlockDialog() {
        Swal.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: `${this.user.email} will be unlocked`,
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080',
        }).then(result => {
            result.isConfirmed ? this.unlock() : this.toggle.checked = true;
        });
    }

    unlock() {
        unlock(this.user.id, this._service)
            .subscribe(() => {
                successAlert('Success')
                    .then(() => {
                        this.user.locked = false;
                        this.toggle.checked = false;
                    });
            });
    }

    change(event: MatSelectChange) {
        this.roles = event.value === 'all' ? this.roles = this.data.roles : this.data.user.fullRoles;
        this.selected = event.value;
        this.rolesName = this.roles.map(rol => rol.name);
        this.filtro.clear();
    }

    addPersonalRol() {
        addPersonalUserRol(this.user.id, this._service).subscribe(res => {
            let id = res.roles.filter(x => !this.userRolesId.includes(x));
            this._router.navigate([`${this.route}/${id}`], {relativeTo: this._route});
        });
    }

    autocompleteValue(value: string) {
        this.roles = value ? this.roles.filter(rol => rol.name === value) : (this.selected === 'all' ? this.roles = this.data.roles : this.data.user.fullRoles);
    }

    deleteRole(roleValue: string) {
        const index = this.updatedRoles.indexOf(roleValue);
        if (index >= 0) {
            this.updatedRoles.splice(index, 1);
        }
        this.applyChanges();
    }

    viewRole(role: IRole) {
        this._router.navigate([`${this.route}${role.id}`], {relativeTo: this._route});
    }
}
