import {IUser} from '../domain/IUser';
import {Observable} from 'rxjs';
import {IRegisterUser} from '../domain/IRegisterUser';
import {IList} from '../../../shared/generics/IList';

export abstract class AbstractUserService {
    //@TODO check what info is passed
    public abstract register(user: IRegisterUser): Observable<IUser>;

    public abstract search(params: any): Observable<IList<IUser>>;
    //public abstract updateUser(user: IUser): Observable<IUser>;
    public abstract updateUserPassword(userId: string, newPassword: string);
    public abstract updateMyPassword(oldPassword: string, newPassword: string);
    public abstract lockUser(id: string);
    public abstract unlockUser(id: string);
    public abstract updateRoles(userId: string, roles: string[]): Observable<IUser>;

    public abstract addRoles(userId: string, roles: string[]): Observable<IUser>;
    public abstract addPersonalrol(userId: string): Observable<IUser>;
}
