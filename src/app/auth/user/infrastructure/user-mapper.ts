export class UserMapper {
    mapTo(params: any) {
        const {id, username, email, roles, scopes, session, locked,userId} = params;

        return {
            id: id ?? userId,
            username: username,
            email: email,
            roles: roles,
            scopes: scopes,
            session: session,
            locked: locked
        }
    }

    mapFrom(params: any) {
        const {id, username, email, roles, scopes, session, locked} = params;

        return {
            id,
            username,
            email,
            roles,
            scopes,
            session,
            locked
        }
    }
}
