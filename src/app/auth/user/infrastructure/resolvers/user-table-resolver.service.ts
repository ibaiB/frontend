import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { CookieFacadeService } from 'src/app/auth/session/infrastructure/services/cookie.service';
import { ITableFiltersConfig } from 'src/app/shared/custom-mat-elements/dynamic';
import {getUsers} from '../../application/getUsers';
import {UserService} from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class UserTableResolverService implements Resolve<any> {

  constructor(
      private _service: UserService,
      private _cookieService : CookieFacadeService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const filtersCache = this._cookieService.get('userFilterCache');
    const tableFiltersConfig: ITableFiltersConfig = JSON.parse(this._cookieService.get('user-table-config'));
    const orderParams = {...tableFiltersConfig?.order} ?? {};
    const userParams = tableFiltersConfig?.pagination ?? {page: 0, size: 10};

    return getUsers({...JSON.parse(filtersCache),...orderParams,...userParams}, this._service)
        .pipe(
            map(res => ({
              data: res,
              tableFiltersConfig: tableFiltersConfig,
              filtersValue:filtersCache
            })),
            catchError(err => {
              return of({
                data: null,
                error: err,
                message: 'Error on user table resolver, data couldn\'t be fetched'
              });
            })
        );
  }
}
