import {TestBed} from '@angular/core/testing';

import {UserTableResolverService} from './user-table-resolver.service';

describe('UserTableResolverService', () => {
  let service: UserTableResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserTableResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
