import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';
import {RoleService} from '../../../role/infrastructure/services/role.service';
import {ScopeService} from '../../../scope/infrastructure/services/scope.service';
import {UserService} from '../services/user.service';
import {getRoles} from '../../../role/application/getRoles';
import {getScopes} from '../../../scope/application/getScopes';
import {getUsers} from '../../application/getUsers';
import {IUser} from '../../domain/IUser';
import {IRole} from '../../../role/domain/IRole';
import {IScope} from '../../../scope/domain/IScope';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserDetailResolverService implements Resolve<any> {

  constructor(
      private roleService: RoleService,
      private scopeService: ScopeService,
      private userService: UserService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    const $roles = getRoles({page: 0, size: 1000}, this.roleService);
    const $scopes = getScopes({page: 0, size: 1000}, this.scopeService);
    const $user = getUsers({id: route.params['id']}, this.userService);

    const fork: Observable<any> = route.params['id'] ? forkJoin($roles, $scopes, $user) : forkJoin($roles, $scopes);

    return fork
        .pipe(map(res => ({
          data: {
            roles: res[0].content,
            scopes: res[1],
            user: res[2] ? this.join(res[2].content[0], res[0].content, res[1]) : null
          }
        })));
  }

  join(user: IUser, roles: IRole[], scopes: IScope[]): IUser {
    user.roles =  roles
        .filter(r => user.roles.includes(r.id))
        .map(r => r.name);

    user.scopes = scopes
        .filter(s => user.scopes.includes(s.id))
        .map(s => s.name);

    return user;
  }
}
