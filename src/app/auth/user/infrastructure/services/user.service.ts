import {SessionService} from '../../../session/infrastructure/services/session.service';
import {Injectable} from '@angular/core';
import {AbstractUserService} from '../abstract-user-service';
import {IUser} from '../../domain/IUser';
import {HttpClient, HttpParams} from '@angular/common/http';
import {UserMapper} from '../user-mapper';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IRegisterUser} from '../../domain/IRegisterUser';
import {environment} from '../../../../../environments/environment';
import {IList} from '../../../../shared/generics/IList';


@Injectable({
  providedIn: 'root'
})
// @TODO missing root URL
export class UserService extends AbstractUserService {

  BASE_URL = environment.baseUrl;
  ENDPOINT = 'users';

  constructor(private httpClient: HttpClient, private mapper: UserMapper, private sessionService: SessionService) {
    super();
  }

  register(user: IRegisterUser): Observable<IUser> {
    return this.httpClient
        .post<any>(`${this.BASE_URL}${this.ENDPOINT}`, user)
        .pipe(map(this.mapper.mapTo));
  }

  search(params: any): Observable<IList<IUser>> {
    return this.httpClient.get<IList<IUser>>(`${this.BASE_URL}${this.ENDPOINT}/search`, {params: new HttpParams({fromObject: params})});
  }

  updateUserPassword(userId: string, newPassword: string) {
    return this.httpClient
        .put(`${this.BASE_URL}${this.ENDPOINT}/${userId}/password`, newPassword);
  }

  updateMyPassword(oldPassword: string, newPassword: string) {
    return this.httpClient
        .put(`${this.BASE_URL}${this.ENDPOINT}/password`,
        { new_password: newPassword, old_password: oldPassword });
  }

  lockUser(id: string) {
    return this.httpClient
      .put(`${this.BASE_URL}${this.ENDPOINT}/${id}/lock`, {});
  }

  unlockUser(id: string) {
    return this.httpClient
      .put(`${this.BASE_URL}${this.ENDPOINT}/${id}/unlock`, {});
  }

  updateRoles(userId: string, roles: []): Observable<IUser> {
    return this.httpClient
      .put<IUser>(`${this.BASE_URL}${this.ENDPOINT}/${userId}/roles`, { role_ids: roles })
      .pipe(map(this.mapper.mapTo));
  }

  findMe() {
    // const httpHeaders = new HttpHeaders().set('Authorization', this.sessionService.getAuth());
    // return this.httpClient.get<IUser>(`${this.BASE_URL}usuarios/me`, { headers: httpHeaders });
  }

  addRoles(userId: string, roles: string[]): Observable<IUser> {
    return this.httpClient
      .post<IUser>(`${this.BASE_URL}${this.ENDPOINT}/${userId}/roles`, { role_ids: roles })
      .pipe(map(this.mapper.mapTo));
  }

   addPersonalrol(userId: string): Observable<IUser> {
    return this.httpClient
    .post<IUser>(`${this.BASE_URL}${this.ENDPOINT}/${userId}/roles/personal`,{})
    .pipe(map(this.mapper.mapTo));
  }
}
