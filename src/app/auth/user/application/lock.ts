import {UserService} from '../infrastructure/services/user.service';

export function lock(id: string, service: UserService) {
    return service.lockUser(id);
}
