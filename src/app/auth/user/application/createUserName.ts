import {IUserInfo} from '../../session/domain/IUserInfo';

export function createUserName(email: string){
    let surname = '';
    let spliced = email.split('@');
    let separated = spliced[0].split('.');
    let name:string = capitalizeFirstLetter(separated[0]);
    if (separated[1] !== undefined){
        name = name.concat(".");
        surname = capitalizeFirstLetter(separated[1]);
    }
    //@TODO add uppercase to name and surname
   return `${name}${surname}`;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
