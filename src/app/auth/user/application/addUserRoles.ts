import {AbstractUserService} from "../infrastructure/abstract-user-service";

export function addUserRoles(id: string, roles: string[], service: AbstractUserService) {
    return service.addRoles(id, roles);
}