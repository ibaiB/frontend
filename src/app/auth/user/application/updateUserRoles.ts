import {AbstractUserService} from '../infrastructure/abstract-user-service';

export function updateUserRoles(id: string, roles: string[], service: AbstractUserService){
    return service.updateRoles(id, roles);
}
