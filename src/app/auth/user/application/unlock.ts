import {UserService} from '../infrastructure/services/user.service';

export function unlock(id: string, service: UserService) {
    return service.unlockUser(id);
}
