import { IRegisterUser } from "../domain/IRegisterUser";
import {AbstractUserService} from "../infrastructure/abstract-user-service";

export function registerNewUser(user: IRegisterUser, service: AbstractUserService) {
    return service.register(user);
}