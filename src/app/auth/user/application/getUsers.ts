import {AbstractUserService} from '../infrastructure/abstract-user-service';

export function getUsers(params: any, service: AbstractUserService){
    return service.search(params);
}
