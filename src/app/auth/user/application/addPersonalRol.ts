import {AbstractUserService} from "../infrastructure/abstract-user-service";

export function addPersonalUserRol(id: string, service: AbstractUserService) {
    return service.addPersonalrol(id);
}