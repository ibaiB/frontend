import {ISession} from '../../session/domain/ISession';

export interface IUser {
    id?: string;
    username: string;
    email: string;
    roles: string[];
    scopes: string[];
    session?: ISession;
    locked: boolean;
}
