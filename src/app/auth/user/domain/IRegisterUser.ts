export interface IRegisterUser{
    username: string,
    email: string
    //@TODO should be an int not string
    userId?: string,
    password:string;
}
