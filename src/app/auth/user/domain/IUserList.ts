import {IUser} from './IUser';

export interface IUserList {
    content: IUser[],
    contentLength: number;
    totalElements: number;
    totalPages: number
}
