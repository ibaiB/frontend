// Angular
import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ErrorHandlerService} from './shared/error/error-handler.service';
import {HttpErrorInterceptorService} from './shared/error/http-error-interceptor.service';
// Components
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
// Modules
import {AppRoutingModule} from './app-routing.module';
import {AuthModule} from './auth/auth.module';
import {MyStaffitModule} from './my-staffit/my-staffit.module';
import {AngularMaterialModule} from './shared/material/angular-material.module';
import {SharedModule} from './shared/shared.module';
import {BreadcrumbComponent} from './home/breadcrumb/breadcrumb.component';
import {EvaluationComponent} from './evaluations/exam/infrastructure/ng-components/evaluation/evaluation.component';
import {InternalModule} from './internal/internal.module';


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        BreadcrumbComponent,
        EvaluationComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        AngularMaterialModule,
        InternalModule,
        // SharedModule,
        AuthModule,
        MyStaffitModule,
        SharedModule,
        InternalModule
    ],
    providers: [
        {
            // processes all errors
            provide: ErrorHandler,
            useClass: ErrorHandlerService
        },
        {
            // interceptor for HTTP errors
            provide: HTTP_INTERCEPTORS,
            useClass: HttpErrorInterceptorService,
            multi: true // multiple interceptors are possible
        }
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
