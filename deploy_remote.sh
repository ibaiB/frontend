#!/usr/bin/env bash
env=$1

user=""
key=""
host=""
case ${env} in
     "dev")
        user="ubuntu"
        host=15.237.10.209
        key="staffit_dev.pem"
     ;;
     "pre")
        user="ubuntu"
        host=15.188.31.176
        key="staffit_pre.pem"
     ;;
     "pro")
        host=192.168.10.220
     ;;
  esac

npm install
rm -rf ./dist/out
npm run build -- --output-path=./dist/out --prod
tar -cvf front.tar --exclude=node_modules --exclude=.git --exclude=.idea --exclude=src --exclude=e2e .
ssh -i "${key}" ${user}@${host} "cd /var/dev/ && rm -rf front && mkdir front"
scp -i "${key}" front.tar ${user}@${host}:/var/dev/front
ssh -i "${key}" ${user}@${host} "cd /var/dev/front && tar -xvf front.tar && chmod +x deploy.sh && sh deploy.sh"
ssh -i "${key}" ${user}@${host} "docker restart nginx"
rm -rf front.tar

