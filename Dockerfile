# Stage 2: Copy compiled frontend to Nginx image
FROM nginx:stable-alpine
COPY ./dist/out/ /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
