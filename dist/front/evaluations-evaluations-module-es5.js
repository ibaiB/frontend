function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["evaluations-evaluations-module"], {
  /***/
  "./src/app/evaluations/correction/application/createCorrection.ts":
  /*!************************************************************************!*\
    !*** ./src/app/evaluations/correction/application/createCorrection.ts ***!
    \************************************************************************/

  /*! exports provided: createCorrection */

  /***/
  function srcAppEvaluationsCorrectionApplicationCreateCorrectionTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "createCorrection", function () {
      return createCorrection;
    });
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    function createCorrection(correction, service, mapper) {
      return service.create(mapper.mapFrom(correction)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(mapper.mapTo));
    }
    /***/

  },

  /***/
  "./src/app/evaluations/correction/application/deleteCorrection.ts":
  /*!************************************************************************!*\
    !*** ./src/app/evaluations/correction/application/deleteCorrection.ts ***!
    \************************************************************************/

  /*! exports provided: deleteCorrection */

  /***/
  function srcAppEvaluationsCorrectionApplicationDeleteCorrectionTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "deleteCorrection", function () {
      return deleteCorrection;
    });

    function deleteCorrection(correctionId, service) {
      return service["delete"](correctionId);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/correction/application/getCorrection.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/evaluations/correction/application/getCorrection.ts ***!
    \*********************************************************************/

  /*! exports provided: getCorrection */

  /***/
  function srcAppEvaluationsCorrectionApplicationGetCorrectionTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getCorrection", function () {
      return getCorrection;
    });

    function getCorrection(correctionId, service) {
      return service.find(correctionId);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/correction/application/searchCorrections.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/evaluations/correction/application/searchCorrections.ts ***!
    \*************************************************************************/

  /*! exports provided: searchCorrections */

  /***/
  function srcAppEvaluationsCorrectionApplicationSearchCorrectionsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "searchCorrections", function () {
      return searchCorrections;
    });
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    function searchCorrections(query, service, mapper) {
      return service.findAll(query).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(mapper.mapList));
    }
    /***/

  },

  /***/
  "./src/app/evaluations/correction/application/updateCorrection.ts":
  /*!************************************************************************!*\
    !*** ./src/app/evaluations/correction/application/updateCorrection.ts ***!
    \************************************************************************/

  /*! exports provided: updateCorrection */

  /***/
  function srcAppEvaluationsCorrectionApplicationUpdateCorrectionTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "updateCorrection", function () {
      return updateCorrection;
    });
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    function updateCorrection(correction, service, mapper) {
      return service.update(mapper.mapFrom(correction)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(mapper.mapTo));
    }
    /***/

  },

  /***/
  "./src/app/evaluations/correction/infrastructure/CorrectionAbstractService.ts":
  /*!************************************************************************************!*\
    !*** ./src/app/evaluations/correction/infrastructure/CorrectionAbstractService.ts ***!
    \************************************************************************************/

  /*! exports provided: CorrectionAbstractService */

  /***/
  function srcAppEvaluationsCorrectionInfrastructureCorrectionAbstractServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CorrectionAbstractService", function () {
      return CorrectionAbstractService;
    });

    var CorrectionAbstractService = function CorrectionAbstractService() {
      _classCallCheck(this, CorrectionAbstractService);
    };
    /***/

  },

  /***/
  "./src/app/evaluations/correction/infrastructure/CorrectionMapper.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/evaluations/correction/infrastructure/CorrectionMapper.ts ***!
    \***************************************************************************/

  /*! exports provided: CorrectionMapper */

  /***/
  function srcAppEvaluationsCorrectionInfrastructureCorrectionMapperTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CorrectionMapper", function () {
      return CorrectionMapper;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var CorrectionMapper = /*#__PURE__*/function () {
      function CorrectionMapper() {
        _classCallCheck(this, CorrectionMapper);
      }

      _createClass(CorrectionMapper, [{
        key: "mapTo",
        value: function mapTo(params) {
          var id = params.id,
              name = params.name,
              description = params.description,
              counters = params.counters,
              idTest = params.idTest;
          return {
            id: id,
            name: name,
            description: description,
            counters: counters,
            idTest: idTest
          };
        }
      }, {
        key: "mapFrom",
        value: function mapFrom(params) {
          var id = params.id,
              name = params.name,
              description = params.description,
              counters = params.counters,
              idTest = params.idTest;
          return {
            id: id,
            name: name,
            description: description,
            counters: counters,
            idTest: idTest
          };
        }
      }, {
        key: "mapList",
        value: function mapList(list) {
          return {
            content: list.content,
            // TODO content: list.content.map(c => this.mapTo(c)),
            numberOfElements: list.numberOfElements,
            totalElements: list.totalElements,
            totalPages: list.totalPages
          };
        }
      }]);

      return CorrectionMapper;
    }();

    CorrectionMapper.ɵfac = function CorrectionMapper_Factory(t) {
      return new (t || CorrectionMapper)();
    };

    CorrectionMapper.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: CorrectionMapper,
      factory: CorrectionMapper.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CorrectionMapper, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/correction/infrastructure/resolvers/create-correction-resolver.service.ts":
  /*!*******************************************************************************************************!*\
    !*** ./src/app/evaluations/correction/infrastructure/resolvers/create-correction-resolver.service.ts ***!
    \*******************************************************************************************************/

  /*! exports provided: CreateCorrectionResolverService */

  /***/
  function srcAppEvaluationsCorrectionInfrastructureResolversCreateCorrectionResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CreateCorrectionResolverService", function () {
      return CreateCorrectionResolverService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _test_application_getTests__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../test/application/getTests */
    "./src/app/evaluations/test/application/getTests.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../test/infrastructure/services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var _test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../test/infrastructure/TestMapper */
    "./src/app/evaluations/test/infrastructure/TestMapper.ts");

    var CreateCorrectionResolverService = /*#__PURE__*/function () {
      function CreateCorrectionResolverService(_testService, _testMapper) {
        _classCallCheck(this, CreateCorrectionResolverService);

        this._testService = _testService;
        this._testMapper = _testMapper;
      }

      _createClass(CreateCorrectionResolverService, [{
        key: "resolve",
        value: function resolve(route, state) {
          var _this = this;

          return Object(_test_application_getTests__WEBPACK_IMPORTED_MODULE_2__["getTest"])(route.params['id'], this._testService).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return {
              data: {
                test: _this._testMapper.mapTo(response)
              }
            };
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (error) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])({
              data: null,
              error: {
                message: 'Error on create correction resolver, data couldn\'t be fetched',
                error: error
              }
            });
          }));
        }
      }]);

      return CreateCorrectionResolverService;
    }();

    CreateCorrectionResolverService.ɵfac = function CreateCorrectionResolverService_Factory(t) {
      return new (t || CreateCorrectionResolverService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_4__["TestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_5__["TestMapper"]));
    };

    CreateCorrectionResolverService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: CreateCorrectionResolverService,
      factory: CreateCorrectionResolverService.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CreateCorrectionResolverService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], function () {
        return [{
          type: _test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_4__["TestService"]
        }, {
          type: _test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_5__["TestMapper"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/correction/infrastructure/services/correction.service.ts":
  /*!**************************************************************************************!*\
    !*** ./src/app/evaluations/correction/infrastructure/services/correction.service.ts ***!
    \**************************************************************************************/

  /*! exports provided: CorrectionService */

  /***/
  function srcAppEvaluationsCorrectionInfrastructureServicesCorrectionServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CorrectionService", function () {
      return CorrectionService;
    });
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _CorrectionAbstractService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../CorrectionAbstractService */
    "./src/app/evaluations/correction/infrastructure/CorrectionAbstractService.ts");

    var CorrectionService = /*#__PURE__*/function (_CorrectionAbstractSe) {
      _inherits(CorrectionService, _CorrectionAbstractSe);

      var _super = _createSuper(CorrectionService);

      function CorrectionService(_http) {
        var _this2;

        _classCallCheck(this, CorrectionService);

        _this2 = _super.call(this);
        _this2._http = _http;
        _this2.BASE_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl;
        _this2.ENDPOINT = 'corrections';
        return _this2;
      }

      _createClass(CorrectionService, [{
        key: "find",
        value: function find(id) {
          return this._http.get("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(id));
        }
      }, {
        key: "findAll",
        value: function findAll(params) {
          return this._http.get("".concat(this.BASE_URL).concat(this.ENDPOINT, "/search"), {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]({
              fromObject: params
            })
          });
        }
      }, {
        key: "create",
        value: function create(correction) {
          return this._http.post("".concat(this.BASE_URL).concat(this.ENDPOINT), correction);
        }
      }, {
        key: "delete",
        value: function _delete(id) {
          return this._http["delete"]("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(id));
        }
      }, {
        key: "update",
        value: function update(correction) {
          return this._http.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(correction.id), correction);
        }
      }]);

      return CorrectionService;
    }(_CorrectionAbstractService__WEBPACK_IMPORTED_MODULE_3__["CorrectionAbstractService"]);

    CorrectionService.ɵfac = function CorrectionService_Factory(t) {
      return new (t || CorrectionService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]));
    };

    CorrectionService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
      token: CorrectionService,
      factory: CorrectionService.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](CorrectionService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/counter/infrastructure/CounterAbstractService.ts":
  /*!******************************************************************************!*\
    !*** ./src/app/evaluations/counter/infrastructure/CounterAbstractService.ts ***!
    \******************************************************************************/

  /*! exports provided: CounterAbstractService */

  /***/
  function srcAppEvaluationsCounterInfrastructureCounterAbstractServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CounterAbstractService", function () {
      return CounterAbstractService;
    });

    var CounterAbstractService = function CounterAbstractService() {
      _classCallCheck(this, CounterAbstractService);
    };
    /***/

  },

  /***/
  "./src/app/evaluations/counter/infrastructure/services/counter.service.ts":
  /*!********************************************************************************!*\
    !*** ./src/app/evaluations/counter/infrastructure/services/counter.service.ts ***!
    \********************************************************************************/

  /*! exports provided: CounterService */

  /***/
  function srcAppEvaluationsCounterInfrastructureServicesCounterServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CounterService", function () {
      return CounterService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../../../environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var _CounterAbstractService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../CounterAbstractService */
    "./src/app/evaluations/counter/infrastructure/CounterAbstractService.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var CounterService = /*#__PURE__*/function (_CounterAbstractServi) {
      _inherits(CounterService, _CounterAbstractServi);

      var _super2 = _createSuper(CounterService);

      function CounterService(http) {
        var _this3;

        _classCallCheck(this, CounterService);

        _this3 = _super2.call(this);
        _this3.http = http;
        _this3.BASE_URL = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].baseUrl;
        _this3.ENDPOINT = 'counters';
        return _this3;
      }

      _createClass(CounterService, [{
        key: "getCounters",
        value: function getCounters(examSentId) {
          return this.http.get("".concat(this.BASE_URL).concat(this.ENDPOINT, "?exam_sent_id=").concat(examSentId));
        }
      }]);

      return CounterService;
    }(_CounterAbstractService__WEBPACK_IMPORTED_MODULE_2__["CounterAbstractService"]);

    CounterService.ɵfac = function CounterService_Factory(t) {
      return new (t || CounterService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]));
    };

    CounterService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: CounterService,
      factory: CounterService.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CounterService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/evaluations-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/evaluations/evaluations-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: routing */

  /***/
  function srcAppEvaluationsEvaluationsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "routing", function () {
      return routing;
    });
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _test_infrastructure_ng_components_new_test_new_test_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/new-test/new-test.component */
    "./src/app/evaluations/test/infrastructure/ng-components/new-test/new-test.component.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_test_management_test_management_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/test-management/test-management.component */
    "./src/app/evaluations/test/infrastructure/ng-components/test-management/test-management.component.ts");
    /* harmony import */


    var _test_infrastructure_resolvers_test_management_resolver_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./test/infrastructure/resolvers/test-management-resolver.service */
    "./src/app/evaluations/test/infrastructure/resolvers/test-management-resolver.service.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_test_detail_test_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/test-detail/test-detail.component */
    "./src/app/evaluations/test/infrastructure/ng-components/test-detail/test-detail.component.ts");
    /* harmony import */


    var _test_infrastructure_resolvers_test_detail_resolver_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./test/infrastructure/resolvers/test-detail-resolver.service */
    "./src/app/evaluations/test/infrastructure/resolvers/test-detail-resolver.service.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_edit_test_edit_test_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/edit-test/edit-test.component */
    "./src/app/evaluations/test/infrastructure/ng-components/edit-test/edit-test.component.ts");
    /* harmony import */


    var _test_infrastructure_resolvers_edit_test_resolver_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./test/infrastructure/resolvers/edit-test-resolver.service */
    "./src/app/evaluations/test/infrastructure/resolvers/edit-test-resolver.service.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_create_correction_create_correction_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/create-correction/create-correction.component */
    "./src/app/evaluations/test/infrastructure/ng-components/create-correction/create-correction.component.ts");
    /* harmony import */


    var _correction_infrastructure_resolvers_create_correction_resolver_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./correction/infrastructure/resolvers/create-correction-resolver.service */
    "./src/app/evaluations/correction/infrastructure/resolvers/create-correction-resolver.service.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_edit_correction_edit_correction_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/edit-correction/edit-correction.component */
    "./src/app/evaluations/test/infrastructure/ng-components/edit-correction/edit-correction.component.ts");
    /* harmony import */


    var _exam_infrastructure_ng_components_exams_managment_exams_managment_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./exam/infrastructure/ng-components/exams-managment/exams-managment.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/exams-managment/exams-managment.component.ts");
    /* harmony import */


    var _exam_infrastructure_resolvers_exam_management_resolver_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./exam/infrastructure/resolvers/exam-management.resolver.service */
    "./src/app/evaluations/exam/infrastructure/resolvers/exam-management.resolver.service.ts");

    var routes = [{
      path: 'tests',
      data: {
        breadcrumb: 'Tests'
      },
      children: [{
        path: 'new',
        data: {
          breadcrumb: 'New Test'
        },
        component: _test_infrastructure_ng_components_new_test_new_test_component__WEBPACK_IMPORTED_MODULE_1__["NewTestComponent"]
      }, {
        path: '',
        data: {
          breadcrumb: 'Management'
        },
        component: _test_infrastructure_ng_components_test_management_test_management_component__WEBPACK_IMPORTED_MODULE_2__["TestManagementComponent"],
        resolve: {
          response: _test_infrastructure_resolvers_test_management_resolver_service__WEBPACK_IMPORTED_MODULE_3__["TestManagementResolverService"]
        }
      }, {
        path: 'test-detail/:id',
        component: _test_infrastructure_ng_components_test_detail_test_detail_component__WEBPACK_IMPORTED_MODULE_4__["TestDetailComponent"],
        data: {
          breadcrumb: 'detail/:id'
        },
        resolve: {
          response: _test_infrastructure_resolvers_test_detail_resolver_service__WEBPACK_IMPORTED_MODULE_5__["TestDetailResolverService"]
        }
      }, {
        path: 'edit/:id',
        component: _test_infrastructure_ng_components_edit_test_edit_test_component__WEBPACK_IMPORTED_MODULE_6__["EditTestComponent"],
        data: {
          breadcrumb: 'edit/:id'
        },
        resolve: {
          response: _test_infrastructure_resolvers_edit_test_resolver_service__WEBPACK_IMPORTED_MODULE_7__["EditTestResolverService"]
        }
      }, {
        path: 'test-correction/:id',
        component: _test_infrastructure_ng_components_create_correction_create_correction_component__WEBPACK_IMPORTED_MODULE_8__["CreateCorrectionComponent"],
        data: {
          breadcrumb: 'Test Correction/:id'
        },
        resolve: {
          response: _correction_infrastructure_resolvers_create_correction_resolver_service__WEBPACK_IMPORTED_MODULE_9__["CreateCorrectionResolverService"]
        }
      }, {
        path: ':idTest/test-correction/edit/:id',
        component: _test_infrastructure_ng_components_edit_correction_edit_correction_component__WEBPACK_IMPORTED_MODULE_10__["EditCorrectionComponent"],
        data: {
          breadcrumb: ':idTest/Test Correction/:id'
        }
      }]
    }, {
      path: 'exams',
      data: {
        breadcrumb: 'Exams'
      },
      component: _exam_infrastructure_ng_components_exams_managment_exams_managment_component__WEBPACK_IMPORTED_MODULE_11__["ExamsManagmentComponent"],
      resolve: {
        response: _exam_infrastructure_resolvers_exam_management_resolver_service__WEBPACK_IMPORTED_MODULE_12__["ExamManagementResolverService"]
      }
    }];

    var routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes);
    /***/

  },

  /***/
  "./src/app/evaluations/evaluations.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/evaluations/evaluations.module.ts ***!
    \***************************************************/

  /*! exports provided: EvaluationsModule */

  /***/
  function srcAppEvaluationsEvaluationsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EvaluationsModule", function () {
      return EvaluationsModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _evaluations_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./evaluations-routing.module */
    "./src/app/evaluations/evaluations-routing.module.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_new_test_new_test_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/new-test/new-test.component */
    "./src/app/evaluations/test/infrastructure/ng-components/new-test/new-test.component.ts");
    /* harmony import */


    var _exam_infrastructure_ng_components_new_exam_new_exam_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./exam/infrastructure/ng-components/new-exam/new-exam.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/new-exam/new-exam.component.ts");
    /* harmony import */


    var _exam_infrastructure_ng_components_exam_table_exam_table_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./exam/infrastructure/ng-components/exam-table/exam-table.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/exam-table/exam-table.component.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_test_management_test_table_test_table_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/test-management/test-table/test-table.component */
    "./src/app/evaluations/test/infrastructure/ng-components/test-management/test-table/test-table.component.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_test_analytics_test_analytics_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/test-analytics/test-analytics.component */
    "./src/app/evaluations/test/infrastructure/ng-components/test-analytics/test-analytics.component.ts");
    /* harmony import */


    var _exam_infrastructure_ng_components_exams_managment_exams_managment_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./exam/infrastructure/ng-components/exams-managment/exams-managment.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/exams-managment/exams-managment.component.ts");
    /* harmony import */


    var _exam_infrastructure_ng_components_sent_exams_table_sent_exams_table_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./exam/infrastructure/ng-components/sent-exams-table/sent-exams-table.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/sent-exams-table/sent-exams-table.component.ts");
    /* harmony import */


    var _exam_infrastructure_ng_components_sent_exam_detail_sent_exam_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./exam/infrastructure/ng-components/sent-exam-detail/sent-exam-detail.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/sent-exam-detail/sent-exam-detail.component.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_test_management_test_management_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/test-management/test-management.component */
    "./src/app/evaluations/test/infrastructure/ng-components/test-management/test-management.component.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_create_correction_create_correction_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/create-correction/create-correction.component */
    "./src/app/evaluations/test/infrastructure/ng-components/create-correction/create-correction.component.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_test_detail_test_detail_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/test-detail/test-detail.component */
    "./src/app/evaluations/test/infrastructure/ng-components/test-detail/test-detail.component.ts");
    /* harmony import */


    var _exam_infrastructure_ng_components_send_exam_send_exam_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./exam/infrastructure/ng-components/send-exam/send-exam.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/send-exam/send-exam.component.ts");
    /* harmony import */


    var _exam_infrastructure_ng_components_edit_exam_edit_exam_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./exam/infrastructure/ng-components/edit-exam/edit-exam.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/edit-exam/edit-exam.component.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_edit_test_edit_test_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/edit-test/edit-test.component */
    "./src/app/evaluations/test/infrastructure/ng-components/edit-test/edit-test.component.ts");
    /* harmony import */


    var _test_infrastructure_ng_components_edit_correction_edit_correction_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ./test/infrastructure/ng-components/edit-correction/edit-correction.component */
    "./src/app/evaluations/test/infrastructure/ng-components/edit-correction/edit-correction.component.ts");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ../shared/shared.module */
    "./src/app/shared/shared.module.ts");
    /* harmony import */


    var _shared_material_angular_material_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ../shared/material/angular-material.module */
    "./src/app/shared/material/angular-material.module.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var EvaluationsModule = function EvaluationsModule() {
      _classCallCheck(this, EvaluationsModule);
    };

    EvaluationsModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: EvaluationsModule
    });
    EvaluationsModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function EvaluationsModule_Factory(t) {
        return new (t || EvaluationsModule)();
      },
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_18__["SharedModule"], _shared_material_angular_material_module__WEBPACK_IMPORTED_MODULE_19__["AngularMaterialModule"], _evaluations_routing_module__WEBPACK_IMPORTED_MODULE_2__["routing"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](EvaluationsModule, {
        declarations: [_test_infrastructure_ng_components_new_test_new_test_component__WEBPACK_IMPORTED_MODULE_3__["NewTestComponent"], _exam_infrastructure_ng_components_new_exam_new_exam_component__WEBPACK_IMPORTED_MODULE_4__["NewExamComponent"], _exam_infrastructure_ng_components_exam_table_exam_table_component__WEBPACK_IMPORTED_MODULE_5__["ExamTableComponent"], _test_infrastructure_ng_components_test_management_test_table_test_table_component__WEBPACK_IMPORTED_MODULE_6__["TestTableComponent"], _test_infrastructure_ng_components_test_analytics_test_analytics_component__WEBPACK_IMPORTED_MODULE_7__["TestAnalyticsComponent"], _exam_infrastructure_ng_components_exams_managment_exams_managment_component__WEBPACK_IMPORTED_MODULE_8__["ExamsManagmentComponent"], _exam_infrastructure_ng_components_sent_exams_table_sent_exams_table_component__WEBPACK_IMPORTED_MODULE_9__["SentExamsTableComponent"], _exam_infrastructure_ng_components_sent_exam_detail_sent_exam_detail_component__WEBPACK_IMPORTED_MODULE_10__["SentExamDetailComponent"], _test_infrastructure_ng_components_test_management_test_management_component__WEBPACK_IMPORTED_MODULE_11__["TestManagementComponent"], _test_infrastructure_ng_components_create_correction_create_correction_component__WEBPACK_IMPORTED_MODULE_12__["CreateCorrectionComponent"], _test_infrastructure_ng_components_test_detail_test_detail_component__WEBPACK_IMPORTED_MODULE_13__["TestDetailComponent"], _exam_infrastructure_ng_components_send_exam_send_exam_component__WEBPACK_IMPORTED_MODULE_14__["SendExamComponent"], _exam_infrastructure_ng_components_edit_exam_edit_exam_component__WEBPACK_IMPORTED_MODULE_15__["EditExamComponent"], _test_infrastructure_ng_components_edit_test_edit_test_component__WEBPACK_IMPORTED_MODULE_16__["EditTestComponent"], _test_infrastructure_ng_components_edit_correction_edit_correction_component__WEBPACK_IMPORTED_MODULE_17__["EditCorrectionComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_18__["SharedModule"], _shared_material_angular_material_module__WEBPACK_IMPORTED_MODULE_19__["AngularMaterialModule"], _angular_router__WEBPACK_IMPORTED_MODULE_20__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EvaluationsModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_test_infrastructure_ng_components_new_test_new_test_component__WEBPACK_IMPORTED_MODULE_3__["NewTestComponent"], _exam_infrastructure_ng_components_new_exam_new_exam_component__WEBPACK_IMPORTED_MODULE_4__["NewExamComponent"], _exam_infrastructure_ng_components_exam_table_exam_table_component__WEBPACK_IMPORTED_MODULE_5__["ExamTableComponent"], _test_infrastructure_ng_components_test_management_test_table_test_table_component__WEBPACK_IMPORTED_MODULE_6__["TestTableComponent"], _test_infrastructure_ng_components_test_analytics_test_analytics_component__WEBPACK_IMPORTED_MODULE_7__["TestAnalyticsComponent"], _exam_infrastructure_ng_components_exams_managment_exams_managment_component__WEBPACK_IMPORTED_MODULE_8__["ExamsManagmentComponent"], _exam_infrastructure_ng_components_sent_exams_table_sent_exams_table_component__WEBPACK_IMPORTED_MODULE_9__["SentExamsTableComponent"], _exam_infrastructure_ng_components_sent_exam_detail_sent_exam_detail_component__WEBPACK_IMPORTED_MODULE_10__["SentExamDetailComponent"], _test_infrastructure_ng_components_test_management_test_management_component__WEBPACK_IMPORTED_MODULE_11__["TestManagementComponent"], _test_infrastructure_ng_components_create_correction_create_correction_component__WEBPACK_IMPORTED_MODULE_12__["CreateCorrectionComponent"], _test_infrastructure_ng_components_test_detail_test_detail_component__WEBPACK_IMPORTED_MODULE_13__["TestDetailComponent"], _exam_infrastructure_ng_components_send_exam_send_exam_component__WEBPACK_IMPORTED_MODULE_14__["SendExamComponent"], _exam_infrastructure_ng_components_edit_exam_edit_exam_component__WEBPACK_IMPORTED_MODULE_15__["EditExamComponent"], _test_infrastructure_ng_components_edit_test_edit_test_component__WEBPACK_IMPORTED_MODULE_16__["EditTestComponent"], _test_infrastructure_ng_components_edit_correction_edit_correction_component__WEBPACK_IMPORTED_MODULE_17__["EditCorrectionComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_18__["SharedModule"], _shared_material_angular_material_module__WEBPACK_IMPORTED_MODULE_19__["AngularMaterialModule"], _evaluations_routing_module__WEBPACK_IMPORTED_MODULE_2__["routing"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/exam/application/getExams.ts":
  /*!**********************************************************!*\
    !*** ./src/app/evaluations/exam/application/getExams.ts ***!
    \**********************************************************/

  /*! exports provided: getExams */

  /***/
  function srcAppEvaluationsExamApplicationGetExamsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getExams", function () {
      return getExams;
    });

    function getExams(params, service) {
      return service.search(params);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/exam/application/resend.ts":
  /*!********************************************************!*\
    !*** ./src/app/evaluations/exam/application/resend.ts ***!
    \********************************************************/

  /*! exports provided: resend */

  /***/
  function srcAppEvaluationsExamApplicationResendTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "resend", function () {
      return resend;
    });

    function resend(id, service) {
      return service.resend(id);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/exam/application/restart.ts":
  /*!*********************************************************!*\
    !*** ./src/app/evaluations/exam/application/restart.ts ***!
    \*********************************************************/

  /*! exports provided: restart */

  /***/
  function srcAppEvaluationsExamApplicationRestartTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "restart", function () {
      return restart;
    });

    function restart(id, service) {
      return service.restart(id);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/exam/application/searchExams.ts":
  /*!*************************************************************!*\
    !*** ./src/app/evaluations/exam/application/searchExams.ts ***!
    \*************************************************************/

  /*! exports provided: searchExams */

  /***/
  function srcAppEvaluationsExamApplicationSearchExamsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "searchExams", function () {
      return searchExams;
    });

    function searchExams(params, service) {
      return service.search(params);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/exam/application/sendExam.ts":
  /*!**********************************************************!*\
    !*** ./src/app/evaluations/exam/application/sendExam.ts ***!
    \**********************************************************/

  /*! exports provided: sendExam */

  /***/
  function srcAppEvaluationsExamApplicationSendExamTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "sendExam", function () {
      return sendExam;
    }); // export function sendExam(idCandidate: string, email: string, idExam: string, service: AbstractExamService) {
    //     return service.sendExam(idCandidate, email, idExam);
    // }


    function sendExam(idCandidateEmail, idExam, service) {
      return service.sendExam(idCandidateEmail, idExam);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/exam/application/updateExam.ts":
  /*!************************************************************!*\
    !*** ./src/app/evaluations/exam/application/updateExam.ts ***!
    \************************************************************/

  /*! exports provided: updateExam */

  /***/
  function srcAppEvaluationsExamApplicationUpdateExamTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "updateExam", function () {
      return updateExam;
    });

    function updateExam(Exam, service) {
      return service.update(Exam.id, Exam);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/exam/application/updateExamCorrection.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/evaluations/exam/application/updateExamCorrection.ts ***!
    \**********************************************************************/

  /*! exports provided: updateExamCorrection */

  /***/
  function srcAppEvaluationsExamApplicationUpdateExamCorrectionTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "updateExamCorrection", function () {
      return updateExamCorrection;
    });

    function updateExamCorrection(examId, correctionId, service) {
      return service.updateCorrection(examId, correctionId);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/abstract-exam-service.ts":
  /*!**************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/abstract-exam-service.ts ***!
    \**************************************************************************/

  /*! exports provided: AbstractExamService */

  /***/
  function srcAppEvaluationsExamInfrastructureAbstractExamServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AbstractExamService", function () {
      return AbstractExamService;
    });

    var AbstractExamService = function AbstractExamService() {
      _classCallCheck(this, AbstractExamService);
    };
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/ng-components/edit-exam/edit-exam.component.ts":
  /*!************************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/ng-components/edit-exam/edit-exam.component.ts ***!
    \************************************************************************************************/

  /*! exports provided: EditExamComponent */

  /***/
  function srcAppEvaluationsExamInfrastructureNgComponentsEditExamEditExamComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditExamComponent", function () {
      return EditExamComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
    /* harmony import */


    var src_app_evaluations_correction_application_searchCorrections__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/evaluations/correction/application/searchCorrections */
    "./src/app/evaluations/correction/application/searchCorrections.ts");
    /* harmony import */


    var src_app_evaluations_exam_application_updateExamCorrection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/evaluations/exam/application/updateExamCorrection */
    "./src/app/evaluations/exam/application/updateExamCorrection.ts");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _application_updateExam__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../application/updateExam */
    "./src/app/evaluations/exam/application/updateExam.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _services_exam_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../services/exam.service */
    "./src/app/evaluations/exam/infrastructure/services/exam.service.ts");
    /* harmony import */


    var src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/evaluations/correction/infrastructure/services/correction.service */
    "./src/app/evaluations/correction/infrastructure/services/correction.service.ts");
    /* harmony import */


    var src_app_evaluations_correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/evaluations/correction/infrastructure/CorrectionMapper */
    "./src/app/evaluations/correction/infrastructure/CorrectionMapper.ts");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
    /* harmony import */


    var _shared_custom_mat_elements_autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/autocomplete/autocomplete.component */
    "./src/app/shared/custom-mat-elements/autocomplete/autocomplete.component.ts");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");

    var EditExamComponent = /*#__PURE__*/function () {
      function EditExamComponent(_fb, dialogRef, data, _examService, _correctionService, _correctionMapper) {
        _classCallCheck(this, EditExamComponent);

        this._fb = _fb;
        this.dialogRef = dialogRef;
        this.data = data;
        this._examService = _examService;
        this._correctionService = _correctionService;
        this._correctionMapper = _correctionMapper;
        this.corrections = [];
        this.selectFood = 'Que paso rey';
        this.width = '100%';
        this.carValue = '';
        this.carOptions = [];
      }

      _createClass(EditExamComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this4 = this;

          this.carValue = this.data.correctionName;
          Object(src_app_evaluations_correction_application_searchCorrections__WEBPACK_IMPORTED_MODULE_2__["searchCorrections"])({}, this._correctionService, this._correctionMapper).subscribe(function (corrections) {
            _this4.corrections = corrections.content;
            _this4.carOptions = _this4.corrections.map(function (corr) {
              return corr.name;
            });
          });
          this.formInit();
        }
      }, {
        key: "autocompleteSelected",
        value: function autocompleteSelected(event) {
          this.carValue = event.value;
        }
      }, {
        key: "formInit",
        value: function formInit() {
          this.form = this._fb.group({
            name: [this.data.exam.name],
            test: [this.data.exam.test.name],
            description: [this.data.exam.description],
            corr: ['']
          });
        }
      }, {
        key: "onNoClick",
        value: function onNoClick() {
          this.dialogRef.close();
        }
      }, {
        key: "onCancelClick",
        value: function onCancelClick() {
          this.dialogRef.close();
        }
      }, {
        key: "onSaveClick",
        value: function onSaveClick() {
          var _this5 = this;

          var correction = this.corrections.find(function (c) {
            return c.name === _this5.carValue;
          });
          console.log(correction);

          if (this.form.valid) {
            var editExam = {
              id: this.data.exam.id,
              name: this.form.get('name').value,
              description: this.form.get('description').value,
              idTest: this.data.exam.test.id,
              idCorrection: correction.id
            };
            Object(_application_updateExam__WEBPACK_IMPORTED_MODULE_5__["updateExam"])(editExam, this._examService).subscribe(function (response) {
              Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["successAlert"])('The exam was updated successfully');
            }, function (error) {
              console.error('Error at edit exam', error);
              var _error$error = error.error,
                  message = _error$error.message,
                  id = _error$error.id;
              Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["errorAlert"])("Couldn't update exam", message, id);
            });
            Object(src_app_evaluations_exam_application_updateExamCorrection__WEBPACK_IMPORTED_MODULE_3__["updateExamCorrection"])(this.data.exam.id, correction.id, this._examService).subscribe(function (data) {
              return console.log(data);
            });
            this.dialogRef.close();
          }
        }
      }]);

      return EditExamComponent;
    }();

    EditExamComponent.ɵfac = function EditExamComponent_Factory(t) {
      return new (t || EditExamComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_exam_service__WEBPACK_IMPORTED_MODULE_7__["ExamService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_8__["CorrectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_9__["CorrectionMapper"]));
    };

    EditExamComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: EditExamComponent,
      selectors: [["app-edit-exam"]],
      decls: 29,
      vars: 7,
      consts: [[1, "dialog", "form"], [1, "black-dialog"], ["src", "../../../../../../assets/CRM/icons/dialogs/exam.png", "alt", "exam image"], [1, "dialog-content", "dialog-content-form", 3, "formGroup"], [1, "title"], [1, "container"], ["appearance", "outline", 1, "full-width"], ["matInput", "", "formControlName", "name", "required", ""], ["matInput", "", "formControlName", "description", "rows", "4", "required", ""], [1, "selectors"], [2, "padding-right", "1rem"], ["appearance", "outline"], ["matInput", "", "placeholder", "Test name", "formControlName", "test", "readonly", ""], [2, "padding-left", "1rem"], [1, "autocomplete-editEmployee", 3, "width", "placeholder", "matLabel", "options", "appearance", "defaultValue", "selectedValue"], [1, "buttons"], ["mat-raised-button", "", 1, "exit-button-form", 3, "click"], ["mat-raised-button", "", "type", "submit", 1, "submit-button", 3, "click"]],
      template: function EditExamComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-dialog-content", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "form", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "EDIT EXAM");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-form-field", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-form-field", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Description");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "textarea", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "section", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-form-field", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Test");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "section", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "app-autocomplete", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selectedValue", function EditExamComponent_Template_app_autocomplete_selectedValue_23_listener($event) {
            return ctx.autocompleteSelected({
              formField: "corr",
              value: $event
            });
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "button", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditExamComponent_Template_button_click_25_listener() {
            return ctx.onCancelClick();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Exit");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditExamComponent_Template_button_click_27_listener() {
            return ctx.onSaveClick();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Update");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("width", ctx.width)("placeholder", "Correction")("matLabel", "Correction")("options", ctx.carOptions)("appearance", "outline")("defaultValue", ctx.carValue);
        }
      },
      directives: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_11__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["RequiredValidator"], _shared_custom_mat_elements_autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_12__["AutocompleteComponent"], _angular_material_button__WEBPACK_IMPORTED_MODULE_13__["MatButton"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n  .mat-dialog-container {\n  padding: 0;\n}\n  .mat-dialog-content {\n  margin: 0;\n  padding: 0;\n  min-height: 470px;\n}\n.black-dialog[_ngcontent-%COMP%] {\n  background-color: black;\n  width: 165px;\n  padding-left: 15px;\n  border-radius: 5px;\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 19px 3px rgba(0, 0, 0, 0.14), 0px 9px 36px 8px rgba(0, 0, 0, 0.12);\n}\n.black-dialog[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-top: 210px;\n}\n.readOnlyInput[_ngcontent-%COMP%] {\n  opacity: 0.3;\n  cursor: not-allowed;\n}\n.dialog[_ngcontent-%COMP%] {\n  width: 850px;\n  display: flex;\n}\n.detail[_ngcontent-%COMP%] {\n  height: 500px;\n}\n.form[_ngcontent-%COMP%] {\n  min-height: 550px;\n  height: 550px;\n  max-height: 550px;\n}\n.dialog-content[_ngcontent-%COMP%] {\n  width: 100%;\n  overflow-y: auto;\n  padding: 40px 40px 0 80px;\n}\n.dialog-content[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.dialog-content-detail[_ngcontent-%COMP%] {\n  padding: 55px;\n}\n.dialog-content-form[_ngcontent-%COMP%] {\n  padding: 30px 50px 30px 100px;\n}\n.title[_ngcontent-%COMP%] {\n  display: flex;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n  font-weight: 400 !important;\n}\n.columns[_ngcontent-%COMP%] {\n  display: flex;\n  margin-top: 15px;\n}\n.columns[_ngcontent-%COMP%]   section[_ngcontent-%COMP%] {\n  width: 50%;\n  padding-right: 10px;\n}\n.exit-button[_ngcontent-%COMP%] {\n  margin: 30px 0 0 500px;\n  background-color: #c2c2c2;\n  color: white;\n  width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n.circle-content[_ngcontent-%COMP%] {\n  position: relative;\n  margin: 20px 40px 0 50px;\n}\n.circle-content-opportunity[_ngcontent-%COMP%] {\n  position: relative;\n  margin: auto 40px auto auto;\n}\n\n.circle[_ngcontent-%COMP%] {\n  width: 30px;\n  height: 30px;\n  border: 0px solid #c2c2c2;\n  border-radius: 120px;\n  line-height: 200px;\n  text-align: center;\n  color: #ddd;\n  font-size: 25px;\n  font-weight: 600;\n  position: absolute;\n  overflow: hidden;\n  z-index: 1;\n  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;\n}\n.circle[_ngcontent-%COMP%]:before {\n  content: \"\";\n  position: absolute;\n  width: 85px;\n  height: 85px;\n  background-color: var(--tooltip-color);\n  left: 50%;\n  transform: translateX(-50%);\n  border-radius: 40%;\n  -webkit-animation: fill 3s ease-in-out;\n          animation: fill 3s ease-in-out;\n  z-index: -1;\n}\n@-webkit-keyframes fill {\n  from {\n    top: 30px;\n    transform: translateX(-50%) rotate(0deg);\n  }\n  to {\n    top: -50px;\n    transform: translateX(-50%) rotate(360deg);\n  }\n}\n@keyframes fill {\n  from {\n    top: 30px;\n    transform: translateX(-50%) rotate(0deg);\n  }\n  to {\n    top: -50px;\n    transform: translateX(-50%) rotate(360deg);\n  }\n}\n\n.buttons[_ngcontent-%COMP%] {\n  display: flex;\n  margin-bottom: 30px;\n}\n.exit-button-form[_ngcontent-%COMP%] {\n  margin: 30px 0 0 325px;\n  background-color: #c2c2c2;\n  color: white;\n  width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n.submit-button[_ngcontent-%COMP%] {\n  margin: 30px 0 0 25px;\n  background-color: #e46e75;\n  color: white;\n  min-width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n\n.activity-color[_ngcontent-%COMP%], .opportunity-color[_ngcontent-%COMP%] {\n  width: 18px;\n  height: 18px;\n  border-radius: 15px;\n  margin: auto;\n}\n.activity-priority[_ngcontent-%COMP%], .opportunity-priority[_ngcontent-%COMP%] {\n  margin: auto 0px auto auto;\n  text-align: center;\n}\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n\n.dialog-content[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 10px;\n}\n.dialog-content[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #c2c2c2;\n  border-radius: 4px;\n}\n.full-width[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.selectors[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n}\n.selectors[_ngcontent-%COMP%]   section[_ngcontent-%COMP%] {\n  width: 50%;\n}\n.selectors[_ngcontent-%COMP%]   section[_ngcontent-%COMP%]   .mat-select[_ngcontent-%COMP%] {\n  display: contents;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2VkaXQtZXhhbS9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXNzZXRzXFxzdHlsZXNcXHN0YWZmaXQtc3R5bGUtMS5zY3NzIiwic3JjL2FwcC9ldmFsdWF0aW9ucy9leGFtL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvZWRpdC1leGFtL2VkaXQtZXhhbS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2VkaXQtZXhhbS9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXNzZXRzXFxzdHlsZXNcXGRpYWxvZy11bmlmaWVkLnNjc3MiLCJzcmMvYXBwL2V2YWx1YXRpb25zL2V4YW0vaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9lZGl0LWV4YW0vQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFwcFxcZXZhbHVhdGlvbnNcXGV4YW1cXGluZnJhc3RydWN0dXJlXFxuZy1jb21wb25lbnRzXFxlZGl0LWV4YW1cXGVkaXQtZXhhbS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQ0FBQTtBQVFBLHdCQUFBO0FBUUEsdUJBQUE7QUFHQSwyQkFBQTtBQUVBO0VBQ0UsNkJBQUE7QUNoQkY7QURrQkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ2hCSjtBRGtCSTtFQUNFLG1CQTNCUTtFQTRCUixnQkFBQTtFQUNBLGNBQUE7QUNoQk47QURzQkE7RUFDRSxhQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNuQkY7QURxQkU7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ25CSjtBRHFCSTtFQUNFLFlBQUE7QUNuQk47QURzQkU7RUFDRSxtQkFBQTtFQUNBLGNBQUE7QUNwQko7QUR3QkE7RUFDRSw2QkFBQTtFQUNBLGVBQUE7QUNyQkY7QUR1QkU7RUFDRSxTQUFBO0FDckJKO0FEd0JFO0VBQ0UsZ0NBQUE7QUN0Qko7QUR5QkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ3ZCSjtBRHlCSTtFQUNFLG1CQTNFUTtFQTRFUixnQkFBQTtFQUNBLGNBQUE7QUN2Qk47QUQwQkk7RUFDRSx5QkE1RWlCO0VBNkVqQixtQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FDeEJOO0FENkJBO0VBQ0UsY0FsRm1CO0FDd0RyQjtBRDZCQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQzFCRjtBRDZCQTtFQUNFLFdBQUE7QUMxQkY7QUQ2QkE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDMUJGO0FENEJFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDMUJKO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUMzQkY7QUQ4QkE7RUFDRSxlQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0Usa0JBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7QUMzQkY7QURrQ0E7RUFDRSxjQUFBO0FDL0JGO0FEa0NBLHlCQUFBO0FBRUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1EQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLHlCQUFBO0FDaENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLGNBQUE7QUNsQ0Y7QURxQ0EsWUFBQTtBQUVBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSx1QkFBQTtBQ25DRjtBRHNDQTtFQUNFLGVBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLDBDQUFBO0FDbkNGO0FDak1JO0VBQ0ksVUFBQTtBRG9NUjtBQ2xNSTtFQUNJLFNBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7QURvTVI7QUNoTUE7RUFDSSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsOEhBQUE7QURtTUo7QUNoTUk7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7QURrTVI7QUMvTEE7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7QURrTUo7QUMvTEE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBRGtNSjtBQzlMQTtFQUNJLGFBQUE7QURpTUo7QUM5TEE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtBRGlNSjtBQzlMQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0FEaU1KO0FDOUxJO0VBQ0ksZ0JBQUE7QURnTVI7QUM1TEE7RUFFSSxhQUFBO0FEOExKO0FDM0xBO0VBQ0ksNkJBQUE7QUQ4TEo7QUMzTEE7RUFDSSxhQUFBO0FEOExKO0FDM0xBO0VBQ0ksY0FBQTtFQUNBLDJCQUFBO0FEOExKO0FDM0xBO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0FEOExKO0FDN0xJO0VBQ0ksVUFBQTtFQUNBLG1CQUFBO0FEK0xSO0FDM0xBO0VBQ0ksc0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FEOExKO0FDM0xBO0VBQ0ksa0JBQUE7RUFDQSx3QkFBQTtBRDhMSjtBQzNMQTtFQUNJLGtCQUFBO0VBQ0EsMkJBQUE7QUQ4TEo7QUMzTEE7Ozs7Q0FBQTtBQU1BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLHFEQUFBO0FENkxKO0FDMUxBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxzQ0FBQTtFQUNBLFNBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0NBQUE7VUFBQSw4QkFBQTtFQUNBLFdBQUE7QUQ2TEo7QUMxTEE7RUFDSTtJQUNJLFNBQUE7SUFDQSx3Q0FBQTtFRDZMTjtFQzNMRTtJQUNJLFVBQUE7SUFDQSwwQ0FBQTtFRDZMTjtBQUNGO0FDck1BO0VBQ0k7SUFDSSxTQUFBO0lBQ0Esd0NBQUE7RUQ2TE47RUMzTEU7SUFDSSxVQUFBO0lBQ0EsMENBQUE7RUQ2TE47QUFDRjtBQzFMQTs7Q0FBQTtBQUlBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FEMkxKO0FDeExBO0VBQ0ksc0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FEMkxKO0FDeExBO0VBQ0kscUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBRDJMSjtBQ3hMQTs7OztDQUFBO0FBTUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBRDBMSjtBQ3ZMQTtFQUNJLDBCQUFBO0VBQ0Esa0JBQUE7QUQwTEo7QUN2TEE7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1EQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtBRDBMSjtBQ3ZMQTtFQUFvQix5QkFBQTtBRDJMcEI7QUMxTEE7RUFBaUIseUJBQUE7QUQ4TGpCO0FDN0xBO0VBQW1CLHlCQUFBO0FEaU1uQjtBQ2hNQTtFQUFpQix5QkFBQTtBRG9NakI7QUNuTUE7RUFBb0IseUJBQUE7QUR1TXBCO0FDck1BOzs7O0NBQUE7QUFNQTtFQUNJLFdBQUE7QUR1TUo7QUNwTUE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0FEdU1KO0FFbmFBO0VBQ0ksV0FBQTtBRnNhSjtBRW5hQTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtBRnNhSjtBRXJhSTtFQUNJLFVBQUE7QUZ1YVI7QUVyYVE7RUFDSSxpQkFBQTtBRnVhWiIsImZpbGUiOiJzcmMvYXBwL2V2YWx1YXRpb25zL2V4YW0vaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9lZGl0LWV4YW0vZWRpdC1leGFtLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xyXG4kc21hbGw6IDAuOHJlbTtcclxuJG1lZGl1bTogMXJlbTtcclxuJGxhcmdlOiAxLjI1cmVtO1xyXG4kZXh0cmEtbGFyZ2U6IDEuNTYzcmVtO1xyXG4kaHVnZTogMS45NTNyZW07XHJcbiRleHRyYS1odWdlOiAyLjQ0MXJlbTtcclxuXHJcbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cclxuJGNvbG9yLXBhbGV0dGUtMS1tYWluOiAjREU3MzczO1xyXG4kY29sb3ItcGFsZXR0ZS0xLXNlY29uZDogI0FCQUJGRjtcclxuJGNvbG9yLXByaW1hcnktbGlnaHQ6ICNFRkVGRUY7XHJcbiRjb2xvci1wcmltYXJ5LWRhcms6ICMxOTE5MTk7XHJcbiRjb2xvci1zZWNvbmRhcnktZGFyazogIzMzMzMzMztcclxuJGNvbG9yLXNjcm9sbGJhci10aHVtYjogIzhDOEM4QztcclxuXHJcbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xyXG5cclxuXHJcbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cclxuXHJcbi5jLWNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuXHJcbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAuZHluLWZpbHRlcnMge1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG4gICAgb3ZlcmZsb3cteDogYXV0bztcclxuICAgIG1hcmdpbjogMCA0MHB4IDAgMDtcclxuXHJcbiAgICA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xyXG4gICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb25maWdCdXR0b24ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gIH1cclxufVxyXG5cclxuLmNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuICBtYXgtaGVpZ2h0OiA4OCU7XHJcblxyXG4gIC5jYXJkLWNvbnRlbnQsIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuXHJcbiAgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXBhbGV0dGUtMS1tYWluO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycmVtO1xyXG4gICAgICBoZWlnaHQ6IDIuM3JlbTtcclxuICAgICAgY29sb3I6ICNlZmVmZWY7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xyXG4gIGNvbG9yOiAkY29sb3ItcHJpbWFyeS1kYXJrO1xyXG59XHJcblxyXG4uZmlsdGVyIHtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcclxuICBtYXgtd2lkdGg6IDEwZW07XHJcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gIC8vIG1heC1oZWlnaHQ6IDgwMHB4O1xyXG4gIC5tYXQtdGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICB9XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItcm93IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XHJcbiAgY29sb3I6ICM4YjhiOGI7XHJcbn1cclxuXHJcbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xyXG4gIHBhZGRpbmctbGVmdDogMDtcclxufVxyXG5cclxuLmlkSGVhZGVyIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5pZENlbGwge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxubWF0LWNhcmQtY29udGVudCB7XHJcbiAgbWFyZ2luOiAwIDM0cHg7XHJcbn1cclxuXHJcbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXHJcblxyXG4ucHJpb3JpdHkge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLy8gQFRPRE86IGNoYW5nZSBjb2xvciB0byB2YXJpYWJsZXNcclxuXHJcbi5wcmlvcml0eS1tdXlhbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xyXG59XHJcblxyXG4ucHJpb3JpdHktYWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcclxufVxyXG5cclxuLnByaW9yaXR5LW5vcm1hbCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcclxufVxyXG5cclxuLnByaW9yaXR5LWJhamEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1uaW5ndW5hIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4ucmVkLWRhdGUge1xyXG4gIGNvbG9yOiAjZWI0MTQxO1xyXG59XHJcblxyXG4vKiBNQVQgVEFCICovXHJcblxyXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcclxuICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIC4xMik7XHJcbn1cclxuIiwiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xuLyogPT09PT0gV0VJR0hUID09PT09ICovXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXG4uYy1jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG5cbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyB7XG4gIGZsZXgtZ3JvdzogMTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbiAgbWFyZ2luOiAwIDQwcHggMCAwO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxNTBweDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5jb25maWdCdXR0b24ge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBmbGV4LXNocmluazogMDtcbn1cblxuLmNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG4gIG1heC1oZWlnaHQ6IDg4JTtcbn1cbi5jYXJkLWNvbXBvbmVudCAuY2FyZC1jb250ZW50LCAuY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMDtcbn1cbi5jYXJkLWNvbXBvbmVudCA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNERTczNzM7XG4gIGJvcmRlci1yYWRpdXM6IDJyZW07XG4gIGhlaWdodDogMi4zcmVtO1xuICBjb2xvcjogI2VmZWZlZjtcbn1cblxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcbiAgY29sb3I6ICMxOTE5MTk7XG59XG5cbi5maWx0ZXIge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMC41ZW07XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcbiAgbWF4LXdpZHRoOiAxMGVtO1xuICBtYXJnaW4tcmlnaHQ6IDVlbTtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93LXg6IGF1dG87XG59XG4udGFibGUtcmVzcG9uc2l2ZSAubWF0LXRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgZGlzcGxheTogdGFibGU7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4ubWF0LWhlYWRlci1yb3cge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xuICBjb2xvcjogIzhiOGI4Yjtcbn1cblxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cblxuLmlkSGVhZGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmlkQ2VsbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG5tYXQtY2FyZC1jb250ZW50IHtcbiAgbWFyZ2luOiAwIDM0cHg7XG59XG5cbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXG4ucHJpb3JpdHkge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5wcmlvcml0eS1tdXlhbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcbn1cblxuLnByaW9yaXR5LWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xufVxuXG4ucHJpb3JpdHktbm9ybWFsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcbn1cblxuLnByaW9yaXR5LWJhamEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xufVxuXG4ucHJpb3JpdHktbmluZ3VuYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5yZWQtZGF0ZSB7XG4gIGNvbG9yOiAjZWI0MTQxO1xufVxuXG4vKiBNQVQgVEFCICovXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG59XG5cbjo6bmctZGVlcCAubWF0LWRpYWxvZy1jb250YWluZXIge1xuICBwYWRkaW5nOiAwO1xufVxuOjpuZy1kZWVwIC5tYXQtZGlhbG9nLWNvbnRlbnQge1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG4gIG1pbi1oZWlnaHQ6IDQ3MHB4O1xufVxuXG4uYmxhY2stZGlhbG9nIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIHdpZHRoOiAxNjVweDtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJveC1zaGFkb3c6IDBweCAxMXB4IDE1cHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMiksIDBweCAyNHB4IDE5cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xNCksIDBweCA5cHggMzZweCA4cHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn1cbi5ibGFjay1kaWFsb2cgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDIxMHB4O1xufVxuXG4ucmVhZE9ubHlJbnB1dCB7XG4gIG9wYWNpdHk6IDAuMztcbiAgY3Vyc29yOiBub3QtYWxsb3dlZDtcbn1cblxuLmRpYWxvZyB7XG4gIHdpZHRoOiA4NTBweDtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLmRldGFpbCB7XG4gIGhlaWdodDogNTAwcHg7XG59XG5cbi5mb3JtIHtcbiAgbWluLWhlaWdodDogNTUwcHg7XG4gIGhlaWdodDogNTUwcHg7XG4gIG1heC1oZWlnaHQ6IDU1MHB4O1xufVxuXG4uZGlhbG9nLWNvbnRlbnQge1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgcGFkZGluZzogNDBweCA0MHB4IDAgODBweDtcbn1cbi5kaWFsb2ctY29udGVudCBzcGFuIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLmRpYWxvZy1jb250ZW50LWRldGFpbCB7XG4gIHBhZGRpbmc6IDU1cHg7XG59XG5cbi5kaWFsb2ctY29udGVudC1mb3JtIHtcbiAgcGFkZGluZzogMzBweCA1MHB4IDMwcHggMTAwcHg7XG59XG5cbi50aXRsZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5yZWQtZGF0ZSB7XG4gIGNvbG9yOiAjZWI0MTQxO1xuICBmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XG59XG5cbi5jb2x1bW5zIHtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5jb2x1bW5zIHNlY3Rpb24ge1xuICB3aWR0aDogNTAlO1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuXG4uZXhpdC1idXR0b24ge1xuICBtYXJnaW46IDMwcHggMCAwIDUwMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xuICBjb2xvcjogd2hpdGU7XG4gIHdpZHRoOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuXG4uY2lyY2xlLWNvbnRlbnQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbjogMjBweCA0MHB4IDAgNTBweDtcbn1cblxuLmNpcmNsZS1jb250ZW50LW9wcG9ydHVuaXR5IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW46IGF1dG8gNDBweCBhdXRvIGF1dG87XG59XG5cbi8qIFxuICAgIC0tLS0tLS0tLS0tLS0tXG4gICAgRFlOQU1JQyBDSVJDTEVcbiAgICAtLS0tLS0tLS0tLS0tLVxuKi9cbi5jaXJjbGUge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBib3JkZXI6IDBweCBzb2xpZCAjYzJjMmMyO1xuICBib3JkZXItcmFkaXVzOiAxMjBweDtcbiAgbGluZS1oZWlnaHQ6IDIwMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjZGRkO1xuICBmb250LXNpemU6IDI1cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgei1pbmRleDogMTtcbiAgYm94LXNoYWRvdzogcmdiYSgxMDAsIDEwMCwgMTExLCAwLjIpIDBweCA3cHggMjlweCAwcHg7XG59XG5cbi5jaXJjbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogODVweDtcbiAgaGVpZ2h0OiA4NXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS10b29sdGlwLWNvbG9yKTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7XG4gIGJvcmRlci1yYWRpdXM6IDQwJTtcbiAgYW5pbWF0aW9uOiBmaWxsIDNzIGVhc2UtaW4tb3V0O1xuICB6LWluZGV4OiAtMTtcbn1cblxuQGtleWZyYW1lcyBmaWxsIHtcbiAgZnJvbSB7XG4gICAgdG9wOiAzMHB4O1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoMGRlZyk7XG4gIH1cbiAgdG8ge1xuICAgIHRvcDogLTUwcHg7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgzNjBkZWcpO1xuICB9XG59XG4vKiBcbiAgICBGT1JNIFxuKi9cbi5idXR0b25zIHtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLmV4aXQtYnV0dG9uLWZvcm0ge1xuICBtYXJnaW46IDMwcHggMCAwIDMyNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xuICBjb2xvcjogd2hpdGU7XG4gIHdpZHRoOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuXG4uc3VibWl0LWJ1dHRvbiB7XG4gIG1hcmdpbjogMzBweCAwIDAgMjVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U0NmU3NTtcbiAgY29sb3I6IHdoaXRlO1xuICBtaW4td2lkdGg6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICBoZWlnaHQ6IDQwcHg7XG59XG5cbi8qIFxuICAgIENJUkNMRSBDT0xPUiBcbiAgICAgICAgJiZcbiAgICBQUklPUklUWVxuKi9cbi5hY3Rpdml0eS1jb2xvciwgLm9wcG9ydHVuaXR5LWNvbG9yIHtcbiAgd2lkdGg6IDE4cHg7XG4gIGhlaWdodDogMThweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4uYWN0aXZpdHktcHJpb3JpdHksIC5vcHBvcnR1bml0eS1wcmlvcml0eSB7XG4gIG1hcmdpbjogYXV0byAwcHggYXV0byBhdXRvO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5wcmlvcml0eSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA1cHggMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLnByaW9yaXR5LW11eWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xufVxuXG4ucHJpb3JpdHktYWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XG59XG5cbi5wcmlvcml0eS1ub3JtYWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4ucHJpb3JpdHktYmFqYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XG59XG5cbi5wcmlvcml0eS1uaW5ndW5hIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLyogXG4gICAgLS0tLS0tXG4gICAgU0NST0xMXG4gICAgLS0tLS0tXG4qL1xuLmRpYWxvZy1jb250ZW50Ojotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMHB4O1xufVxuXG4uZGlhbG9nLWNvbnRlbnQ6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2MyYzJjMjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuXG4uZnVsbC13aWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uc2VsZWN0b3JzIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLnNlbGVjdG9ycyBzZWN0aW9uIHtcbiAgd2lkdGg6IDUwJTtcbn1cbi5zZWxlY3RvcnMgc2VjdGlvbiAubWF0LXNlbGVjdCB7XG4gIGRpc3BsYXk6IGNvbnRlbnRzO1xufSIsIkBpbXBvcnQgXCIuL3N0YWZmaXQtc3R5bGUtMVwiO1xyXG5cclxuOjpuZy1kZWVwIHtcclxuICAgIC5tYXQtZGlhbG9nLWNvbnRhaW5lciB7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgIH1cclxuICAgIC5tYXQtZGlhbG9nLWNvbnRlbnQge1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDQ3MHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uYmxhY2stZGlhbG9nIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gICAgd2lkdGg6IDE2NXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDExcHggMTVweCAtN3B4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMHB4IDI0cHggMTlweCAzcHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMHB4IDlweCAzNnB4IDhweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG5cclxuXHJcbiAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIxMHB4O1xyXG4gICAgfVxyXG59XHJcbi5yZWFkT25seUlucHV0e1xyXG4gICAgb3BhY2l0eTogMC4zO1xyXG4gICAgY3Vyc29yOiBub3QtYWxsb3dlZDtcclxufVxyXG5cclxuLmRpYWxvZyB7XHJcbiAgICB3aWR0aDogODUwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxufVxyXG5cclxuLmRldGFpbCB7XHJcbiAgICBoZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4uZm9ybSB7XHJcbiAgICBtaW4taGVpZ2h0OiA1NTBweDtcclxuICAgIGhlaWdodDogNTUwcHg7XHJcbiAgICBtYXgtaGVpZ2h0OiA1NTBweDtcclxufVxyXG5cclxuLmRpYWxvZy1jb250ZW50IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgb3ZlcmZsb3cteTogYXV0bztcclxuICAgIHBhZGRpbmc6IDQwcHggNDBweCAwIDgwcHg7XHJcbiAgICAvLyBkaXNwbGF5OiBncmlkO1xyXG5cclxuICAgIHNwYW4ge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudC1kZXRhaWwge1xyXG4gICAgLy8gcGFkZGluZzogNDBweCA0MHB4IDAgODBweDtcclxuICAgIHBhZGRpbmc6IDU1cHg7XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudC1mb3JtIHtcclxuICAgIHBhZGRpbmc6IDMwcHggNTBweCAzMHB4IDEwMHB4O1xyXG59XHJcblxyXG4udGl0bGUgeyBcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5yZWQtZGF0ZSB7XHJcbiAgICBjb2xvcjogI2ViNDE0MTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNvbHVtbnMge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICBzZWN0aW9uIHtcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5leGl0LWJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDMwcHggMCAwIDUwMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHdpZHRoOiAxMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbn1cclxuXHJcbi5jaXJjbGUtY29udGVudHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbjogMjBweCA0MHB4IDAgNTBweDtcclxufVxyXG5cclxuLmNpcmNsZS1jb250ZW50LW9wcG9ydHVuaXR5e1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luOiBhdXRvIDQwcHggYXV0byBhdXRvO1xyXG59XHJcblxyXG4vKiBcclxuICAgIC0tLS0tLS0tLS0tLS0tXHJcbiAgICBEWU5BTUlDIENJUkNMRVxyXG4gICAgLS0tLS0tLS0tLS0tLS1cclxuKi9cclxuXHJcbi5jaXJjbGUge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBib3JkZXI6IDBweCBzb2xpZCAjYzJjMmMyO1xyXG4gICAgYm9yZGVyLXJhZGl1czoxMjBweDtcclxuICAgIGxpbmUtaGVpZ2h0OjIwMHB4O1xyXG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICBjb2xvcjojZGRkO1xyXG4gICAgZm9udC1zaXplOjI1cHg7XHJcbiAgICBmb250LXdlaWdodDo2MDA7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBvdmVyZmxvdzpoaWRkZW47XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgYm94LXNoYWRvdzogcmdiYSgxMDAsIDEwMCwgMTExLCAwLjIpIDBweCA3cHggMjlweCAwcHg7XHJcbn1cclxuXHJcbi5jaXJjbGU6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6XCJcIjtcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgd2lkdGg6ODVweDtcclxuICAgIGhlaWdodDo4NXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tdG9vbHRpcC1jb2xvcik7XHJcbiAgICBsZWZ0OjUwJTtcclxuICAgIHRyYW5zZm9ybTp0cmFuc2xhdGVYKC01MCUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czo0MCU7XHJcbiAgICBhbmltYXRpb246ZmlsbCAzcyBlYXNlLWluLW91dDtcclxuICAgIHotaW5kZXg6LTE7XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgZmlsbCB7XHJcbiAgICBmcm9tIHtcclxuICAgICAgICB0b3A6MzBweDtcclxuICAgICAgICB0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICB0byB7XHJcbiAgICAgICAgdG9wOi01MHB4O1xyXG4gICAgICAgIHRyYW5zZm9ybTp0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgzNjBkZWcpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBcclxuICAgIEZPUk0gXHJcbiovXHJcblxyXG4uYnV0dG9ucyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxufVxyXG5cclxuLmV4aXQtYnV0dG9uLWZvcm0ge1xyXG4gICAgbWFyZ2luOiAzMHB4IDAgMCAzMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG4uc3VibWl0LWJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDMwcHggMCAwIDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTQ2ZTc1O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgbWluLXdpZHRoOiAxMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbn1cclxuXHJcbi8qIFxyXG4gICAgQ0lSQ0xFIENPTE9SIFxyXG4gICAgICAgICYmXHJcbiAgICBQUklPUklUWVxyXG4qL1xyXG5cclxuLmFjdGl2aXR5LWNvbG9yLCAub3Bwb3J0dW5pdHktY29sb3Ige1xyXG4gICAgd2lkdGg6IDE4cHg7XHJcbiAgICBoZWlnaHQ6IDE4cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG4uYWN0aXZpdHktcHJpb3JpdHksIC5vcHBvcnR1bml0eS1wcmlvcml0eSB7XHJcbiAgICBtYXJnaW46IGF1dG8gMHB4IGF1dG8gYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnByaW9yaXR5IHtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7IGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTUgfVxyXG4ucHJpb3JpdHktYWx0YSB7IGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzEgfVxyXG4ucHJpb3JpdHktbm9ybWFsIHsgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZSB9XHJcbi5wcmlvcml0eS1iYWphIHsgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZCB9XHJcbi5wcmlvcml0eS1uaW5ndW5hIHsgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMiB9XHJcblxyXG4vKiBcclxuICAgIC0tLS0tLVxyXG4gICAgU0NST0xMXHJcbiAgICAtLS0tLS1cclxuKi9cclxuXHJcbi5kaWFsb2ctY29udGVudDo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gICAgd2lkdGg6IDEwcHg7XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudDo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gICAgYmFja2dyb3VuZDogI2MyYzJjMjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxufSIsIkBpbXBvcnQgJy4uLy4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvZGlhbG9nLXVuaWZpZWQnO1xyXG5cclxuLmZ1bGwtd2lkdGh7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnNlbGVjdG9yc3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBzZWN0aW9ue1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcblxyXG4gICAgICAgIC5tYXQtc2VsZWN0e1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBjb250ZW50cztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EditExamComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-edit-exam',
          templateUrl: './edit-exam.component.html',
          styleUrls: ['./edit-exam.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]
        }, {
          type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]
        }, {
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"]]
          }]
        }, {
          type: _services_exam_service__WEBPACK_IMPORTED_MODULE_7__["ExamService"]
        }, {
          type: src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_8__["CorrectionService"]
        }, {
          type: src_app_evaluations_correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_9__["CorrectionMapper"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/ng-components/exam-table/config.ts":
  /*!************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/ng-components/exam-table/config.ts ***!
    \************************************************************************************/

  /*! exports provided: table */

  /***/
  function srcAppEvaluationsExamInfrastructureNgComponentsExamTableConfigTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "table", function () {
      return table;
    });

    var table = {
      multiSelect: false,
      actions: [{
        name: 'Send Exam',
        icon: 'email'
      }, {
        name: 'Edit',
        icon: 'create'
      }],
      columns: [{
        name: 'Id',
        prop: 'id',
        shown: true,
        route: 'a',
        routeId: '',
        cellClass: 'id-table-column'
      }, {
        name: 'Name',
        prop: 'name',
        shown: true
      }, {
        name: 'Description',
        prop: 'description',
        shown: true
      }, {
        name: 'Test',
        prop: 'testName',
        shown: true
      }, {
        name: 'Correction',
        prop: 'correctionName',
        shown: true
      }]
    };
    /***/
  },

  /***/
  "./src/app/evaluations/exam/infrastructure/ng-components/exam-table/exam-table.component.ts":
  /*!**************************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/ng-components/exam-table/exam-table.component.ts ***!
    \**************************************************************************************************/

  /*! exports provided: ExamTableComponent */

  /***/
  function srcAppEvaluationsExamInfrastructureNgComponentsExamTableExamTableComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ExamTableComponent", function () {
      return ExamTableComponent;
    });
    /* harmony import */


    var src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! src/app/shared/custom-mat-elements/dynamic */
    "./src/app/shared/custom-mat-elements/dynamic.ts");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _application_searchExams__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../application/searchExams */
    "./src/app/evaluations/exam/application/searchExams.ts");
    /* harmony import */


    var _edit_exam_edit_exam_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../edit-exam/edit-exam.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/edit-exam/edit-exam.component.ts");
    /* harmony import */


    var _send_exam_send_exam_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../send-exam/send-exam.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/send-exam/send-exam.component.ts");
    /* harmony import */


    var _config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./config */
    "./src/app/evaluations/exam/infrastructure/ng-components/exam-table/config.ts");
    /* harmony import */


    var _services_exam_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../services/exam.service */
    "./src/app/evaluations/exam/infrastructure/services/exam.service.ts");
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
    /* harmony import */


    var src_app_evaluations_exam_infrastructure_ExamMapper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/evaluations/exam/infrastructure/ExamMapper */
    "./src/app/evaluations/exam/infrastructure/ExamMapper.ts");
    /* harmony import */


    var src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! src/app/auth/session/infrastructure/services/cookie.service */
    "./src/app/auth/session/infrastructure/services/cookie.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component */
    "./src/app/shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component.ts");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-filters/dynamic-filters.component */
    "./src/app/shared/custom-mat-elements/dynamic-filters/dynamic-filters.component.ts");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component */
    "./src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component.ts");

    var _c0 = ["table"];

    function ExamTableComponent_div_0_Template(rf, ctx) {
      if (rf & 1) {
        var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "app-dynamic-filters", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("query", function ExamTableComponent_div_0_Template_app_dynamic_filters_query_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r3);

          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r2.filtersChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "button", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ExamTableComponent_div_0_Template_button_click_3_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r3);

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r4.setConfigVisibility(!ctx_r4.configVisibility);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, " Settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "app-dynamic-table-mark-ii", 5, 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("action", function ExamTableComponent_div_0_Template_app_dynamic_table_mark_ii_action_7_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r3);

          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r5.doAction($event);
        })("pagination", function ExamTableComponent_div_0_Template_app_dynamic_table_mark_ii_pagination_7_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r3);

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r6.paginationChanged($event);
        })("order", function ExamTableComponent_div_0_Template_app_dynamic_table_mark_ii_order_7_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r3);

          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r7.orderChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("filters", ctx_r0.filters)("showFilters", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("config", ctx_r0.tableConfig)("data", ctx_r0.tableData);
      }
    }

    var ExamTableComponent = /*#__PURE__*/function () {
      function ExamTableComponent(_examService, _dialog, _examMapper, _cookieFacade) {
        _classCallCheck(this, ExamTableComponent);

        this._examService = _examService;
        this._dialog = _dialog;
        this._examMapper = _examMapper;
        this._cookieFacade = _cookieFacade;
        this.globalBackSearch = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.filters = [];
        this.columns = [];
        this.tableConfig = _config__WEBPACK_IMPORTED_MODULE_6__["table"];
        this.configVisibility = false;
        this.loading = true;
        this.scopeEmployeeInsert = false; // Auxiliary variable to check scope

        this.scopeEvaluationsInsert = false; // Auxiliary variable to check scope

        this.testOptions = [];
        this.correctionOptions = [];
      }

      _createClass(ExamTableComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _a, _b, _c, _d;

          this.testOptions = this.tests.content;
          this.correctionOptions = this.corrections.content.map(function (correction) {
            return correction.name;
          });

          this._buildFilters();

          var data = [];
          this.exams.content.forEach(function (exam) {
            return data.push({
              exam: exam,
              id: exam.id,
              name: exam.name,
              description: exam.description,
              // TODO: Fake properties
              correctionName: exam.correction.name,
              testName: exam.test.name
            });
          });
          this.exams.content = data;
          this.tableData = this.exams;
          Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["parseTableFiltersConfig"])(this.tableFiltersConfig, this.tableConfig, this.filters);
          this.queryFilters = this.filtersValues ? this._clean(JSON.parse(this.filtersValues)) : {};
          this.tableConfig.pagination = (_b = (_a = this.tableFiltersConfig) === null || _a === void 0 ? void 0 : _a.pagination) !== null && _b !== void 0 ? _b : {
            page: 0,
            size: 10
          };
          this.queryPagination = this.tableConfig.pagination;
          this.tableConfig.order = (_d = (_c = this.tableFiltersConfig) === null || _c === void 0 ? void 0 : _c.order) !== null && _d !== void 0 ? _d : {
            orderField: '',
            order: ''
          };
          this.queryOrder = Object.assign({}, this.tableConfig.order);
          this.tableConfig.order.orderField = this.tableConfig.order.orderField ? Object.entries(this._examMapper.mapTo(_defineProperty({}, this.tableConfig.order.orderField, ''))).find(function (entry) {
            return entry[1] === '';
          })[0] : '';
          this.loading = false;
          this.configFilterValue();
        }
      }, {
        key: "configFilterValue",
        value: function configFilterValue() {
          var _this6 = this;

          var filtersAux = this.filtersValues ? this._clean(this._examMapper.mapTo(JSON.parse(this.filtersValues))) : {};
          this.filters.forEach(function (filter) {
            filter.defaultValue = filtersAux[_this6.getFilterProp(filter)];
            filter.value = filtersAux[_this6.getFilterProp(filter)];
          });
        }
      }, {
        key: "getFilterProp",
        value: function getFilterProp(filter) {
          return filter.prop ? filter.type === 'autoselect' ? filter.searchValue : filter.prop : filter.searchValue;
        }
      }, {
        key: "_clean",
        value: function _clean(object) {
          var cleaned = {};
          var keys = Object.keys(object);
          keys.forEach(function (key) {
            if (object[key]) {
              cleaned[key] = object[key];
            }
          });
          return cleaned;
        }
      }, {
        key: "sendExam",
        value: function sendExam(exam) {
          var _this7 = this;

          var dialogRef = this._dialog.open(_send_exam_send_exam_component__WEBPACK_IMPORTED_MODULE_5__["SendExamComponent"], {
            data: {
              exam: exam,
              candidates: this.candidates.content
            }
          });

          dialogRef.afterClosed().subscribe(function () {
            return _this7.globalBackSearch.emit();
          });
        }
      }, {
        key: "editExam",
        value: function editExam(exam) {
          var _this8 = this;

          var dialogRef = this._dialog.open(_edit_exam_edit_exam_component__WEBPACK_IMPORTED_MODULE_4__["EditExamComponent"], {
            data: exam
          });

          dialogRef.afterClosed().subscribe(function () {
            return _this8.globalBackSearch.emit();
          });
        }
      }, {
        key: "doAction",
        value: function doAction(event) {
          if (event.action === 'Send Exam') {
            this.sendExam(event.item);
          }

          if (event.action === 'Edit') {
            this.editExam(event.item);
          }
        }
      }, {
        key: "configChange",
        value: function configChange(event) {
          this.tableConfig = Object.assign({}, event[0]);
          this.filters = event[1];

          this._cookieFacade.save('exam-table-config', JSON.stringify(Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        }
      }, {
        key: "setConfigVisibility",
        value: function setConfigVisibility(visible) {
          this.configVisibility = visible;
        }
      }, {
        key: "backSearch",
        value: function backSearch() {
          var _this9 = this;

          Object(_application_searchExams__WEBPACK_IMPORTED_MODULE_3__["searchExams"])(Object.assign(Object.assign(Object.assign({}, this.queryPagination), this.queryOrder), this.queryFilters), this._examService).subscribe(function (result) {
            var data = [];
            result.content.forEach(function (exam) {
              return data.push({
                exam: exam,
                id: exam.id,
                name: exam.name,
                description: exam.description,
                // TODO: Fake properties
                correctionName: exam.correction.name,
                testName: exam.test.name
              });
            });
            result.content = data;
            _this9.tableData = result;
          }, function (error) {
            console.error('error on back search', error);
            var _error$error2 = error.error,
                message = _error$error2.message,
                id = _error$error2.id;
            Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["errorAlert"])("Couldn't retrieve data", message, id);
          });
        }
      }, {
        key: "filtersChanged",
        value: function filtersChanged(params) {
          if (params && Object.keys(params).length !== 0) {
            this.queryFilters = params;
          } else {
            this.queryFilters = {};
          }

          this._cookieFacade.save('examFilterCache', JSON.stringify(this.queryFilters));

          this.queryPagination = {
            page: 0,
            size: this.queryPagination.size
          };

          this._cookieFacade.save('exam-table-config', JSON.stringify(Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.dynamicTable.resetPageIndex();
          this.backSearch();
        }
      }, {
        key: "paginationChanged",
        value: function paginationChanged(event) {
          this.queryPagination.page = event.page;
          this.queryPagination.size = event.size;

          this._cookieFacade.save('exam-table-config', JSON.stringify(Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "orderChanged",
        value: function orderChanged(event) {
          this.queryOrder.orderField = event.orderField;
          this.queryOrder.order = event.order;
          this.queryPagination.page = event.page;
          this.queryPagination.size = event.size;

          this._cookieFacade.save('exam-table-config', JSON.stringify(Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "_buildFilters",
        value: function _buildFilters() {
          this.filters = [{
            options: [],
            prop: 'id',
            retProp: '',
            type: 'input',
            appearance: 'outline',
            "class": 'input',
            label: 'Id',
            placeholder: 'Id',
            shown: true
          }, {
            options: [],
            prop: 'name',
            retProp: '',
            type: 'input',
            appearance: 'outline',
            "class": 'input',
            label: 'Name',
            placeholder: 'Name',
            shown: true
          }, {
            options: [],
            prop: 'description',
            retProp: '',
            type: 'input',
            appearance: 'outline',
            "class": 'input',
            label: 'Description',
            placeholder: 'Description',
            shown: true
          }];
        }
      }]);

      return ExamTableComponent;
    }();

    ExamTableComponent.ɵfac = function ExamTableComponent_Factory(t) {
      return new (t || ExamTableComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_services_exam_service__WEBPACK_IMPORTED_MODULE_7__["ExamService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_8__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_evaluations_exam_infrastructure_ExamMapper__WEBPACK_IMPORTED_MODULE_9__["ExamMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_10__["CookieFacadeService"]));
    };

    ExamTableComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: ExamTableComponent,
      selectors: [["app-exam-table"]],
      viewQuery: function ExamTableComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.dynamicTable = _t.first);
        }
      },
      inputs: {
        exams: "exams",
        filtersValues: "filtersValues",
        candidates: "candidates",
        tests: "tests",
        corrections: "corrections",
        tableFiltersConfig: "tableFiltersConfig"
      },
      outputs: {
        globalBackSearch: "globalBackSearch"
      },
      decls: 2,
      vars: 4,
      consts: [[4, "ngIf"], [3, "visibility", "columnsConfig", "filtersConfig", "configChanged", "closed"], [1, "filters-plus-config"], [1, "dyn-filters", 3, "filters", "showFilters", "query"], ["mat-button", "", 1, "configButton", 3, "click"], [3, "config", "data", "action", "pagination", "order"], ["table", ""]],
      template: function ExamTableComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, ExamTableComponent_div_0_Template, 9, 4, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "app-dynamic-table-config-panel", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("configChanged", function ExamTableComponent_Template_app_dynamic_table_config_panel_configChanged_1_listener($event) {
            return ctx.configChange($event);
          })("closed", function ExamTableComponent_Template_app_dynamic_table_config_panel_closed_1_listener() {
            return ctx.setConfigVisibility(false);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visibility", ctx.configVisibility)("columnsConfig", ctx.tableConfig)("filtersConfig", ctx.filters);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_11__["NgIf"], _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_12__["DynamicTableConfigPanelComponent"], _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_13__["DynamicFiltersComponent"], _angular_material_button__WEBPACK_IMPORTED_MODULE_14__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__["MatIcon"], _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_16__["DynamicTableMarkIiComponent"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2V4YW0tdGFibGUvQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFzc2V0c1xcc3R5bGVzXFxzdGFmZml0LXN0eWxlLTEuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2V4YW0tdGFibGUvZXhhbS10YWJsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQ0FBQTtBQVFBLHdCQUFBO0FBUUEsdUJBQUE7QUFHQSwyQkFBQTtBQUVBO0VBQ0UsNkJBQUE7QUNoQkY7QURrQkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ2hCSjtBRGtCSTtFQUNFLG1CQTNCUTtFQTRCUixnQkFBQTtFQUNBLGNBQUE7QUNoQk47QURzQkE7RUFDRSxhQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNuQkY7QURxQkU7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ25CSjtBRHFCSTtFQUNFLFlBQUE7QUNuQk47QURzQkU7RUFDRSxtQkFBQTtFQUNBLGNBQUE7QUNwQko7QUR3QkE7RUFDRSw2QkFBQTtFQUNBLGVBQUE7QUNyQkY7QUR1QkU7RUFDRSxTQUFBO0FDckJKO0FEd0JFO0VBQ0UsZ0NBQUE7QUN0Qko7QUR5QkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ3ZCSjtBRHlCSTtFQUNFLG1CQTNFUTtFQTRFUixnQkFBQTtFQUNBLGNBQUE7QUN2Qk47QUQwQkk7RUFDRSx5QkE1RWlCO0VBNkVqQixtQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FDeEJOO0FENkJBO0VBQ0UsY0FsRm1CO0FDd0RyQjtBRDZCQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQzFCRjtBRDZCQTtFQUNFLFdBQUE7QUMxQkY7QUQ2QkE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDMUJGO0FENEJFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDMUJKO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUMzQkY7QUQ4QkE7RUFDRSxlQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0Usa0JBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7QUMzQkY7QURrQ0E7RUFDRSxjQUFBO0FDL0JGO0FEa0NBLHlCQUFBO0FBRUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1EQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLHlCQUFBO0FDaENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLGNBQUE7QUNsQ0Y7QURxQ0EsWUFBQTtBQUVBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSx1QkFBQTtBQ25DRjtBRHNDQTtFQUNFLGVBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLDBDQUFBO0FDbkNGIiwiZmlsZSI6InNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2V4YW0tdGFibGUvZXhhbS10YWJsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cclxuJHNtYWxsOiAwLjhyZW07XHJcbiRtZWRpdW06IDFyZW07XHJcbiRsYXJnZTogMS4yNXJlbTtcclxuJGV4dHJhLWxhcmdlOiAxLjU2M3JlbTtcclxuJGh1Z2U6IDEuOTUzcmVtO1xyXG4kZXh0cmEtaHVnZTogMi40NDFyZW07XHJcblxyXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXHJcbiRjb2xvci1wYWxldHRlLTEtbWFpbjogI0RFNzM3MztcclxuJGNvbG9yLXBhbGV0dGUtMS1zZWNvbmQ6ICNBQkFCRkY7XHJcbiRjb2xvci1wcmltYXJ5LWxpZ2h0OiAjRUZFRkVGO1xyXG4kY29sb3ItcHJpbWFyeS1kYXJrOiAjMTkxOTE5O1xyXG4kY29sb3Itc2Vjb25kYXJ5LWRhcms6ICMzMzMzMzM7XHJcbiRjb2xvci1zY3JvbGxiYXItdGh1bWI6ICM4QzhDOEM7XHJcblxyXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cclxuXHJcblxyXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXHJcblxyXG4uYy1jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcblxyXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgLmR5bi1maWx0ZXJzIHtcclxuICAgIGZsZXgtZ3JvdzogMTtcclxuICAgIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgICBtYXJnaW46IDAgNDBweCAwIDA7XHJcblxyXG4gICAgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcclxuICAgICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29uZmlnQnV0dG9uIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcbiAgbWF4LWhlaWdodDogODglO1xyXG5cclxuICAuY2FyZC1jb250ZW50LCAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1wYWxldHRlLTEtbWFpbjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMnJlbTtcclxuICAgICAgaGVpZ2h0OiAyLjNyZW07XHJcbiAgICAgIGNvbG9yOiAjZWZlZmVmO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcclxuICBjb2xvcjogJGNvbG9yLXByaW1hcnktZGFyaztcclxufVxyXG5cclxuLmZpbHRlciB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMC41ZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XHJcbiAgbWF4LXdpZHRoOiAxMGVtO1xyXG4gIG1hcmdpbi1yaWdodDogNWVtO1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuICAvLyBtYXgtaGVpZ2h0OiA4MDBweDtcclxuICAubWF0LXRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgfVxyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xyXG4gIGNvbG9yOiAjOGI4YjhiO1xyXG59XHJcblxyXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbn1cclxuXHJcbi5pZEhlYWRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWRDZWxsIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbm1hdC1jYXJkLWNvbnRlbnQge1xyXG4gIG1hcmdpbjogMCAzNHB4O1xyXG59XHJcblxyXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xyXG5cclxuLnByaW9yaXR5IHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi8vIEBUT0RPOiBjaGFuZ2UgY29sb3IgdG8gdmFyaWFibGVzXHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcclxufVxyXG5cclxuLnByaW9yaXR5LWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1ub3JtYWwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1iYWphIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbmluZ3VuYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLnJlZC1kYXRlIHtcclxuICBjb2xvcjogI2ViNDE0MTtcclxufVxyXG5cclxuLyogTUFUIFRBQiAqL1xyXG5cclxuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xyXG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAuMTIpO1xyXG59XHJcbiIsIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xuLmMtY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMge1xuICBmbGV4LWdyb3c6IDE7XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTUwcHg7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuY29uZmlnQnV0dG9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZmxleC1zaHJpbms6IDA7XG59XG5cbi5jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xuICBtYXgtaGVpZ2h0OiA4OCU7XG59XG4uY2FyZC1jb21wb25lbnQgLmNhcmQtY29udGVudCwgLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBtYXJnaW46IDA7XG59XG4uY2FyZC1jb21wb25lbnQgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBidXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xuICBib3JkZXItcmFkaXVzOiAycmVtO1xuICBoZWlnaHQ6IDIuM3JlbTtcbiAgY29sb3I6ICNlZmVmZWY7XG59XG5cbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XG4gIGNvbG9yOiAjMTkxOTE5O1xufVxuXG4uZmlsdGVyIHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cblxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XG4gIG1heC13aWR0aDogMTBlbTtcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XG59XG5cbi50YWJsZS1yZXNwb25zaXZlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy14OiBhdXRvO1xufVxuLnRhYmxlLXJlc3BvbnNpdmUgLm1hdC10YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLm1hdC1oZWFkZXItcm93IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcbiAgY29sb3I6ICM4YjhiOGI7XG59XG5cbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG5cbi5pZEhlYWRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pZENlbGwge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxubWF0LWNhcmQtY29udGVudCB7XG4gIG1hcmdpbjogMCAzNHB4O1xufVxuXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xuLnByaW9yaXR5IHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucHJpb3JpdHktbXV5YWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XG59XG5cbi5wcmlvcml0eS1hbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcbn1cblxuLnByaW9yaXR5LW5vcm1hbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XG59XG5cbi5wcmlvcml0eS1iYWphIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcbn1cblxuLnByaW9yaXR5LW5pbmd1bmEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucmVkLWRhdGUge1xuICBjb2xvcjogI2ViNDE0MTtcbn1cblxuLyogTUFUIFRBQiAqL1xuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufSJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ExamTableComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-exam-table',
          templateUrl: './exam-table.component.html',
          styleUrls: ['./exam-table.component.scss']
        }]
      }], function () {
        return [{
          type: _services_exam_service__WEBPACK_IMPORTED_MODULE_7__["ExamService"]
        }, {
          type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_8__["MatDialog"]
        }, {
          type: src_app_evaluations_exam_infrastructure_ExamMapper__WEBPACK_IMPORTED_MODULE_9__["ExamMapper"]
        }, {
          type: src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_10__["CookieFacadeService"]
        }];
      }, {
        exams: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        filtersValues: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        candidates: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        tests: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        corrections: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        tableFiltersConfig: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        globalBackSearch: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        dynamicTable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['table']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/ng-components/exams-managment/exams-managment.component.ts":
  /*!************************************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/ng-components/exams-managment/exams-managment.component.ts ***!
    \************************************************************************************************************/

  /*! exports provided: ExamsManagmentComponent */

  /***/
  function srcAppEvaluationsExamInfrastructureNgComponentsExamsManagmentExamsManagmentComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ExamsManagmentComponent", function () {
      return ExamsManagmentComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _new_exam_new_exam_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../new-exam/new-exam.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/new-exam/new-exam.component.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/material/tabs */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tabs.js");
    /* harmony import */


    var _exam_table_exam_table_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../exam-table/exam-table.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/exam-table/exam-table.component.ts");
    /* harmony import */


    var _sent_exams_table_sent_exams_table_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../sent-exams-table/sent-exams-table.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/sent-exams-table/sent-exams-table.component.ts");

    var _c0 = ["examTable"];
    var _c1 = ["sentExamTable"];

    function ExamsManagmentComponent_mat_card_0_Template(rf, ctx) {
      if (rf & 1) {
        var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h2");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "EXAMS");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExamsManagmentComponent_mat_card_0_Template_button_click_4_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r3.createNewExamDialog();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " New ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-tab-group", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-tab", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "app-exam-table", 6, 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("globalBackSearch", function ExamsManagmentComponent_mat_card_0_Template_app_exam_table_globalBackSearch_10_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r5.backSearch();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-tab", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "app-sent-exams-table", 9, 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("exams", ctx_r0.exams)("filtersValues", ctx_r0.filtersValueExam)("candidates", ctx_r0.candidates)("tests", ctx_r0.tests)("corrections", ctx_r0.corrections)("tableFiltersConfig", ctx_r0.examTableFiltersConfig);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("filtersValues", ctx_r0.filtersValueSentExam)("sentExams", ctx_r0.sentExams)("tableFiltersConfig", ctx_r0.sentExamTableFiltersConfig);
      }
    }

    var ExamsManagmentComponent = /*#__PURE__*/function () {
      function ExamsManagmentComponent(_router, dialog, _route) {
        _classCallCheck(this, ExamsManagmentComponent);

        this._router = _router;
        this.dialog = dialog;
        this._route = _route;
        this.examsTable = true;
        this.loading = true;
      }

      _createClass(ExamsManagmentComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this._resolved = this._route.snapshot.data['response'];

          if (this._resolved.data) {
            this.exams = this._resolved.data.exams;
            this.sentExams = this._resolved.data.sentExams;
            this.candidates = this._resolved.data.candidates;
            this.tests = this._resolved.data.tests;
            this.corrections = this._resolved.data.corrections;
            this.filtersValueExam = this._resolved.data.filtersValuesExam;
            this.filtersValueSentExam = this._resolved.data.filtersValuesSentExam;
            this.loading = false;
            this.examTableFiltersConfig = this._resolved.data.examTableFiltersConfig;
            this.sentExamTableFiltersConfig = this._resolved.data.sentExamTableFiltersConfig;
          }
        }
      }, {
        key: "createNewExamDialog",
        value: function createNewExamDialog() {
          var _this10 = this;

          var dialogRef = this.dialog.open(_new_exam_new_exam_component__WEBPACK_IMPORTED_MODULE_1__["NewExamComponent"], {});
          dialogRef.afterClosed().subscribe(function (newExam) {
            _this10.backSearch();
          });
        }
      }, {
        key: "backSearch",
        value: function backSearch() {
          this.examTable.backSearch();
          this.sentExamTable.backSearch();
        }
      }]);

      return ExamsManagmentComponent;
    }();

    ExamsManagmentComponent.ɵfac = function ExamsManagmentComponent_Factory(t) {
      return new (t || ExamsManagmentComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]));
    };

    ExamsManagmentComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ExamsManagmentComponent,
      selectors: [["app-exams-managment"]],
      viewQuery: function ExamsManagmentComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.examTable = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.sentExamTable = _t.first);
        }
      },
      decls: 1,
      vars: 1,
      consts: [["class", "card-component", 4, "ngIf"], [1, "card-component"], [1, "title-component"], ["mat-flat-button", "", 3, "click"], [1, "card-content"], ["label", "Exams Table"], [3, "exams", "filtersValues", "candidates", "tests", "corrections", "tableFiltersConfig", "globalBackSearch"], ["examTable", ""], ["label", "Sent Exams"], [3, "filtersValues", "sentExams", "tableFiltersConfig"], ["sentExamTable", ""]],
      template: function ExamsManagmentComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, ExamsManagmentComponent_mat_card_0_Template, 15, 9, "mat-card", 0);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _angular_material_card__WEBPACK_IMPORTED_MODULE_5__["MatCard"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_7__["MatIcon"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_8__["MatTabGroup"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_8__["MatTab"], _exam_table_exam_table_component__WEBPACK_IMPORTED_MODULE_9__["ExamTableComponent"], _sent_exams_table_sent_exams_table_component__WEBPACK_IMPORTED_MODULE_10__["SentExamsTableComponent"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2V4YW1zLW1hbmFnbWVudC9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXNzZXRzXFxzdHlsZXNcXHN0YWZmaXQtc3R5bGUtMS5zY3NzIiwic3JjL2FwcC9ldmFsdWF0aW9ucy9leGFtL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvZXhhbXMtbWFuYWdtZW50L2V4YW1zLW1hbmFnbWVudC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQ0FBQTtBQVFBLHdCQUFBO0FBUUEsdUJBQUE7QUFHQSwyQkFBQTtBQUVBO0VBQ0UsNkJBQUE7QUNoQkY7QURrQkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ2hCSjtBRGtCSTtFQUNFLG1CQTNCUTtFQTRCUixnQkFBQTtFQUNBLGNBQUE7QUNoQk47QURzQkE7RUFDRSxhQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNuQkY7QURxQkU7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ25CSjtBRHFCSTtFQUNFLFlBQUE7QUNuQk47QURzQkU7RUFDRSxtQkFBQTtFQUNBLGNBQUE7QUNwQko7QUR3QkE7RUFDRSw2QkFBQTtFQUNBLGVBQUE7QUNyQkY7QUR1QkU7RUFDRSxTQUFBO0FDckJKO0FEd0JFO0VBQ0UsZ0NBQUE7QUN0Qko7QUR5QkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ3ZCSjtBRHlCSTtFQUNFLG1CQTNFUTtFQTRFUixnQkFBQTtFQUNBLGNBQUE7QUN2Qk47QUQwQkk7RUFDRSx5QkE1RWlCO0VBNkVqQixtQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FDeEJOO0FENkJBO0VBQ0UsY0FsRm1CO0FDd0RyQjtBRDZCQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQzFCRjtBRDZCQTtFQUNFLFdBQUE7QUMxQkY7QUQ2QkE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDMUJGO0FENEJFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDMUJKO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUMzQkY7QUQ4QkE7RUFDRSxlQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0Usa0JBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7QUMzQkY7QURrQ0E7RUFDRSxjQUFBO0FDL0JGO0FEa0NBLHlCQUFBO0FBRUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1EQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLHlCQUFBO0FDaENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLGNBQUE7QUNsQ0Y7QURxQ0EsWUFBQTtBQUVBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSx1QkFBQTtBQ25DRjtBRHNDQTtFQUNFLGVBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLDBDQUFBO0FDbkNGIiwiZmlsZSI6InNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2V4YW1zLW1hbmFnbWVudC9leGFtcy1tYW5hZ21lbnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiA9PT09PSBGT05UUyAoTWFqb3IgVGhpcmQpID09PT09ICovXHJcbiRzbWFsbDogMC44cmVtO1xyXG4kbWVkaXVtOiAxcmVtO1xyXG4kbGFyZ2U6IDEuMjVyZW07XHJcbiRleHRyYS1sYXJnZTogMS41NjNyZW07XHJcbiRodWdlOiAxLjk1M3JlbTtcclxuJGV4dHJhLWh1Z2U6IDIuNDQxcmVtO1xyXG5cclxuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xyXG4kY29sb3ItcGFsZXR0ZS0xLW1haW46ICNERTczNzM7XHJcbiRjb2xvci1wYWxldHRlLTEtc2Vjb25kOiAjQUJBQkZGO1xyXG4kY29sb3ItcHJpbWFyeS1saWdodDogI0VGRUZFRjtcclxuJGNvbG9yLXByaW1hcnktZGFyazogIzE5MTkxOTtcclxuJGNvbG9yLXNlY29uZGFyeS1kYXJrOiAjMzMzMzMzO1xyXG4kY29sb3Itc2Nyb2xsYmFyLXRodW1iOiAjOEM4QzhDO1xyXG5cclxuLyogPT09PT0gV0VJR0hUID09PT09ICovXHJcblxyXG5cclxuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xyXG5cclxuLmMtY2FyZC1jb21wb25lbnQge1xyXG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5cclxuLmZpbHRlcnMtcGx1cy1jb25maWcge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1mbG93OiByb3c7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gIC5keW4tZmlsdGVycyB7XHJcbiAgICBmbGV4LWdyb3c6IDE7XHJcbiAgICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gICAgbWFyZ2luOiAwIDQwcHggMCAwO1xyXG5cclxuICAgIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XHJcbiAgICAgIHdpZHRoOiAxNTBweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmNvbmZpZ0J1dHRvbiB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgfVxyXG59XHJcblxyXG4uY2FyZC1jb21wb25lbnQge1xyXG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xyXG4gIG1heC1oZWlnaHQ6IDg4JTtcclxuXHJcbiAgLmNhcmQtY29udGVudCwgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgfVxyXG5cclxuICA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24ge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItcGFsZXR0ZS0xLW1haW47XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJyZW07XHJcbiAgICAgIGhlaWdodDogMi4zcmVtO1xyXG4gICAgICBjb2xvcjogI2VmZWZlZjtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XHJcbiAgY29sb3I6ICRjb2xvci1wcmltYXJ5LWRhcms7XHJcbn1cclxuXHJcbi5maWx0ZXIge1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbm1hdC1mb3JtLWZpZWxkIG1hdC1zZWxlY3Qge1xyXG4gIG1heC13aWR0aDogMTBlbTtcclxuICBtYXJnaW4tcmlnaHQ6IDVlbTtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmUge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgLy8gbWF4LWhlaWdodDogODAwcHg7XHJcbiAgLm1hdC10YWJsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IHRhYmxlO1xyXG4gIH1cclxufVxyXG5cclxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1yb3cge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcclxuICBjb2xvcjogIzhiOGI4YjtcclxufVxyXG5cclxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XHJcbiAgcGFkZGluZy1sZWZ0OiAwO1xyXG59XHJcblxyXG4uaWRIZWFkZXIge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmlkQ2VsbCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5tYXQtY2FyZC1jb250ZW50IHtcclxuICBtYXJnaW46IDAgMzRweDtcclxufVxyXG5cclxuLyogPT09PT0gUFJJT1JJVFkgPT09PT0gKi9cclxuXHJcbi5wcmlvcml0eSB7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgcGFkZGluZzogNXB4IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4vLyBAVE9ETzogY2hhbmdlIGNvbG9yIHRvIHZhcmlhYmxlc1xyXG5cclxuLnByaW9yaXR5LW11eWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1hbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbm9ybWFsIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xyXG59XHJcblxyXG4ucHJpb3JpdHktYmFqYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcclxufVxyXG5cclxuLnByaW9yaXR5LW5pbmd1bmEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi5yZWQtZGF0ZSB7XHJcbiAgY29sb3I6ICNlYjQxNDE7XHJcbn1cclxuXHJcbi8qIE1BVCBUQUIgKi9cclxuXHJcbjo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIHtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHNwYW4ge1xyXG4gIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcclxuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgLjEyKTtcclxufVxyXG4iLCIvKiA9PT09PSBGT05UUyAoTWFqb3IgVGhpcmQpID09PT09ICovXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cbi5jLWNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cblxuLmZpbHRlcnMtcGx1cy1jb25maWcge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIHtcbiAgZmxleC1ncm93OiAxO1xuICBvdmVyZmxvdy14OiBhdXRvO1xuICBtYXJnaW46IDAgNDBweCAwIDA7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDE1MHB4O1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmNvbmZpZ0J1dHRvbiB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGZsZXgtc2hyaW5rOiAwO1xufVxuXG4uY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbiAgbWF4LWhlaWdodDogODglO1xufVxuLmNhcmQtY29tcG9uZW50IC5jYXJkLWNvbnRlbnQsIC5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAwO1xufVxuLmNhcmQtY29tcG9uZW50IDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RFNzM3MztcbiAgYm9yZGVyLXJhZGl1czogMnJlbTtcbiAgaGVpZ2h0OiAyLjNyZW07XG4gIGNvbG9yOiAjZWZlZmVmO1xufVxuXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xuICBjb2xvcjogIzE5MTkxOTtcbn1cblxuLmZpbHRlciB7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbm1hdC1mb3JtLWZpZWxkIG1hdC1zZWxlY3Qge1xuICBtYXgtd2lkdGg6IDEwZW07XG4gIG1hcmdpbi1yaWdodDogNWVtO1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbn1cbi50YWJsZS1yZXNwb25zaXZlIC5tYXQtdGFibGUge1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiB0YWJsZTtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XG59XG5cbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XG59XG5cbi5tYXQtaGVhZGVyLXJvdyB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XG4gIGNvbG9yOiAjOGI4YjhiO1xufVxuXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcbiAgcGFkZGluZy1sZWZ0OiAwO1xufVxuXG4uaWRIZWFkZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uaWRDZWxsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbm1hdC1jYXJkLWNvbnRlbnQge1xuICBtYXJnaW46IDAgMzRweDtcbn1cblxuLyogPT09PT0gUFJJT1JJVFkgPT09PT0gKi9cbi5wcmlvcml0eSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA1cHggMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLnByaW9yaXR5LW11eWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xufVxuXG4ucHJpb3JpdHktYWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XG59XG5cbi5wcmlvcml0eS1ub3JtYWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4ucHJpb3JpdHktYmFqYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XG59XG5cbi5wcmlvcml0eS1uaW5ndW5hIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLnJlZC1kYXRlIHtcbiAgY29sb3I6ICNlYjQxNDE7XG59XG5cbi8qIE1BVCBUQUIgKi9cbjo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIHtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xuICBmb250LXNpemU6IDEzcHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHNwYW4ge1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ExamsManagmentComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-exams-managment',
          templateUrl: './exams-managment.component.html',
          styleUrls: ['./exams-managment.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }];
      }, {
        examTable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['examTable']
        }],
        sentExamTable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['sentExamTable']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/ng-components/new-exam/new-exam.component.ts":
  /*!**********************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/ng-components/new-exam/new-exam.component.ts ***!
    \**********************************************************************************************/

  /*! exports provided: NewExamComponent */

  /***/
  function srcAppEvaluationsExamInfrastructureNgComponentsNewExamNewExamComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewExamComponent", function () {
      return NewExamComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../test/infrastructure/services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var _correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../correction/infrastructure/services/correction.service */
    "./src/app/evaluations/correction/infrastructure/services/correction.service.ts");
    /* harmony import */


    var _services_exam_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../services/exam.service */
    "./src/app/evaluations/exam/infrastructure/services/exam.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
    /* harmony import */


    var _angular_material_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material/select */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/material/core */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");

    function NewExamComponent_mat_option_21_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var test_r2 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", test_r2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", test_r2.name, " ");
      }
    }

    function NewExamComponent_mat_option_27_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var correction_r3 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", correction_r3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](correction_r3.name);
      }
    }

    var NewExamComponent = /*#__PURE__*/function () {
      function NewExamComponent(_testService, _correctionService, _examService, _fb, dialogRef) {
        _classCallCheck(this, NewExamComponent);

        this._testService = _testService;
        this._correctionService = _correctionService;
        this._examService = _examService;
        this._fb = _fb;
        this.dialogRef = dialogRef;
        this.tests = [];
        this.corrections = [];
        this.allCorrections = [];
      }

      _createClass(NewExamComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this11 = this;

          var base = {
            page: 0,
            size: 1000
          };

          var tests = this._testService.findAll(Object.assign({}, base));

          var corrections = this._correctionService.findAll(Object.assign({}, base));

          Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["forkJoin"])(tests, corrections).subscribe(function (data) {
            _this11.tests = data[0].content.filter(function (test) {
              return test.name;
            });
            _this11.corrections = data[1].content;
            _this11.allCorrections = data[1].content;
          });
          this.formInit();
        }
      }, {
        key: "formInit",
        value: function formInit() {
          this.form = this._fb.group({
            name: [''],
            test: [''],
            description: [''],
            correction: ['']
          });
        }
      }, {
        key: "selectedTest",
        value: function selectedTest(event) {
          this.corrections = this.allCorrections.filter(function (c) {
            return c.idTest === event.value.id;
          });
        }
      }, {
        key: "onNoClick",
        value: function onNoClick() {
          this.dialogRef.close();
        }
      }, {
        key: "onCancelClick",
        value: function onCancelClick() {
          this.dialogRef.close();
        }
      }, {
        key: "onSaveClick",
        value: function onSaveClick() {
          var _this12 = this;

          if (this.form.valid) {
            var name = this.form.get('name').value;
            var description = this.form.get('description').value;
            var test = this.form.get('test').value;
            var correction = this.form.get('correction').value;
            var result = {
              name: name,
              description: description,
              idTest: test.id,
              idCorrection: correction.id
            };

            this._examService.createExam(result).subscribe(function (result) {
              Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["successAlert"])('Exam created successfully').then(function () {
                return _this12.dialogRef.close();
              });
            });
          }
        }
      }]);

      return NewExamComponent;
    }();

    NewExamComponent.ɵfac = function NewExamComponent_Factory(t) {
      return new (t || NewExamComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_3__["TestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_4__["CorrectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_exam_service__WEBPACK_IMPORTED_MODULE_5__["ExamService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogRef"]));
    };

    NewExamComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: NewExamComponent,
      selectors: [["app-new-exam"]],
      decls: 31,
      vars: 3,
      consts: [[1, "dialog", "form"], [1, "black-dialog"], ["src", "../../../../assets/CRM/icons/dialogs/exam.png", "alt", "exam-image"], [1, "dialog-content", "dialog-content-form", 3, "formGroup"], [1, "container"], ["appearance", "outline", 1, "full-width"], ["matInput", "", "formControlName", "name", "required", ""], ["matInput", "", "formControlName", "description", "rows", "4", "required", ""], [1, "selectors"], [2, "padding-right", "1rem"], ["formControlName", "test", "placeholder", "Select Test", "required", "", 3, "selectionChange"], [3, "value", 4, "ngFor", "ngForOf"], [2, "padding-left", "1rem"], ["formControlName", "correction", "name", "correction", "required", ""], [1, "buttons"], ["mat-button", "", "type", "submit", 1, "submit-button", 3, "click"], [3, "value"]],
      template: function NewExamComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-dialog-content", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "form", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "New Exam");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-form-field", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "input", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-form-field", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Description");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "textarea", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "section", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "mat-form-field", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Select test");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "mat-select", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selectionChange", function NewExamComponent_Template_mat_select_selectionChange_20_listener($event) {
            return ctx.selectedTest($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, NewExamComponent_mat_option_21_Template, 2, 2, "mat-option", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "section", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "mat-form-field", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Select correction of test");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "mat-select", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, NewExamComponent_mat_option_27_Template, 2, 2, "mat-option", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "button", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewExamComponent_Template_button_click_29_listener() {
            return ctx.onSaveClick();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Create exam");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.tests);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.corrections);
        }
      },
      directives: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_8__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_9__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["RequiredValidator"], _angular_material_select__WEBPACK_IMPORTED_MODULE_10__["MatSelect"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgForOf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButton"], _angular_material_core__WEBPACK_IMPORTED_MODULE_13__["MatOption"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n  .mat-dialog-container {\n  padding: 0;\n}\n  .mat-dialog-content {\n  margin: 0;\n  padding: 0;\n  min-height: 470px;\n}\n.black-dialog[_ngcontent-%COMP%] {\n  background-color: black;\n  width: 165px;\n  padding-left: 15px;\n  border-radius: 5px;\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 19px 3px rgba(0, 0, 0, 0.14), 0px 9px 36px 8px rgba(0, 0, 0, 0.12);\n}\n.black-dialog[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-top: 210px;\n}\n.readOnlyInput[_ngcontent-%COMP%] {\n  opacity: 0.3;\n  cursor: not-allowed;\n}\n.dialog[_ngcontent-%COMP%] {\n  width: 850px;\n  display: flex;\n}\n.detail[_ngcontent-%COMP%] {\n  height: 500px;\n}\n.form[_ngcontent-%COMP%] {\n  min-height: 550px;\n  height: 550px;\n  max-height: 550px;\n}\n.dialog-content[_ngcontent-%COMP%] {\n  width: 100%;\n  overflow-y: auto;\n  padding: 40px 40px 0 80px;\n}\n.dialog-content[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.dialog-content-detail[_ngcontent-%COMP%] {\n  padding: 55px;\n}\n.dialog-content-form[_ngcontent-%COMP%] {\n  padding: 30px 50px 30px 100px;\n}\n.title[_ngcontent-%COMP%] {\n  display: flex;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n  font-weight: 400 !important;\n}\n.columns[_ngcontent-%COMP%] {\n  display: flex;\n  margin-top: 15px;\n}\n.columns[_ngcontent-%COMP%]   section[_ngcontent-%COMP%] {\n  width: 50%;\n  padding-right: 10px;\n}\n.exit-button[_ngcontent-%COMP%] {\n  margin: 30px 0 0 500px;\n  background-color: #c2c2c2;\n  color: white;\n  width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n.circle-content[_ngcontent-%COMP%] {\n  position: relative;\n  margin: 20px 40px 0 50px;\n}\n.circle-content-opportunity[_ngcontent-%COMP%] {\n  position: relative;\n  margin: auto 40px auto auto;\n}\n\n.circle[_ngcontent-%COMP%] {\n  width: 30px;\n  height: 30px;\n  border: 0px solid #c2c2c2;\n  border-radius: 120px;\n  line-height: 200px;\n  text-align: center;\n  color: #ddd;\n  font-size: 25px;\n  font-weight: 600;\n  position: absolute;\n  overflow: hidden;\n  z-index: 1;\n  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;\n}\n.circle[_ngcontent-%COMP%]:before {\n  content: \"\";\n  position: absolute;\n  width: 85px;\n  height: 85px;\n  background-color: var(--tooltip-color);\n  left: 50%;\n  transform: translateX(-50%);\n  border-radius: 40%;\n  -webkit-animation: fill 3s ease-in-out;\n          animation: fill 3s ease-in-out;\n  z-index: -1;\n}\n@-webkit-keyframes fill {\n  from {\n    top: 30px;\n    transform: translateX(-50%) rotate(0deg);\n  }\n  to {\n    top: -50px;\n    transform: translateX(-50%) rotate(360deg);\n  }\n}\n@keyframes fill {\n  from {\n    top: 30px;\n    transform: translateX(-50%) rotate(0deg);\n  }\n  to {\n    top: -50px;\n    transform: translateX(-50%) rotate(360deg);\n  }\n}\n\n.buttons[_ngcontent-%COMP%] {\n  display: flex;\n  margin-bottom: 30px;\n}\n.exit-button-form[_ngcontent-%COMP%] {\n  margin: 30px 0 0 325px;\n  background-color: #c2c2c2;\n  color: white;\n  width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n.submit-button[_ngcontent-%COMP%] {\n  margin: 30px 0 0 25px;\n  background-color: #e46e75;\n  color: white;\n  min-width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n\n.activity-color[_ngcontent-%COMP%], .opportunity-color[_ngcontent-%COMP%] {\n  width: 18px;\n  height: 18px;\n  border-radius: 15px;\n  margin: auto;\n}\n.activity-priority[_ngcontent-%COMP%], .opportunity-priority[_ngcontent-%COMP%] {\n  margin: auto 0px auto auto;\n  text-align: center;\n}\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n\n.dialog-content[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 10px;\n}\n.dialog-content[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #c2c2c2;\n  border-radius: 4px;\n}\n.full-width[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.selectors[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n}\n.selectors[_ngcontent-%COMP%]   section[_ngcontent-%COMP%] {\n  width: 50%;\n}\n.selectors[_ngcontent-%COMP%]   section[_ngcontent-%COMP%]   .mat-select[_ngcontent-%COMP%] {\n  display: contents;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL25ldy1leGFtL0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhc3NldHNcXHN0eWxlc1xcc3RhZmZpdC1zdHlsZS0xLnNjc3MiLCJzcmMvYXBwL2V2YWx1YXRpb25zL2V4YW0vaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9uZXctZXhhbS9uZXctZXhhbS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL25ldy1leGFtL0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhc3NldHNcXHN0eWxlc1xcZGlhbG9nLXVuaWZpZWQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL25ldy1leGFtL0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhcHBcXGV2YWx1YXRpb25zXFxleGFtXFxpbmZyYXN0cnVjdHVyZVxcbmctY29tcG9uZW50c1xcbmV3LWV4YW1cXG5ldy1leGFtLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9DQUFBO0FBUUEsd0JBQUE7QUFRQSx1QkFBQTtBQUdBLDJCQUFBO0FBRUE7RUFDRSw2QkFBQTtBQ2hCRjtBRGtCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDaEJKO0FEa0JJO0VBQ0UsbUJBM0JRO0VBNEJSLGdCQUFBO0VBQ0EsY0FBQTtBQ2hCTjtBRHNCQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQ25CRjtBRHFCRTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDbkJKO0FEcUJJO0VBQ0UsWUFBQTtBQ25CTjtBRHNCRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtBQ3BCSjtBRHdCQTtFQUNFLDZCQUFBO0VBQ0EsZUFBQTtBQ3JCRjtBRHVCRTtFQUNFLFNBQUE7QUNyQko7QUR3QkU7RUFDRSxnQ0FBQTtBQ3RCSjtBRHlCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDdkJKO0FEeUJJO0VBQ0UsbUJBM0VRO0VBNEVSLGdCQUFBO0VBQ0EsY0FBQTtBQ3ZCTjtBRDBCSTtFQUNFLHlCQTVFaUI7RUE2RWpCLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUN4Qk47QUQ2QkE7RUFDRSxjQWxGbUI7QUN3RHJCO0FENkJBO0VBQ0UsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDMUJGO0FENkJBO0VBQ0UsV0FBQTtBQzFCRjtBRDZCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQzFCRjtBRDZCQTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUMxQkY7QUQ0QkU7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUMxQko7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtBQzNCRjtBRDhCQTtFQUNFLGVBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQzNCRjtBRGtDQTtFQUNFLGNBQUE7QUMvQkY7QURrQ0EseUJBQUE7QUFFQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbURBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0VBQ0EseUJBQUE7QUNoQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UsY0FBQTtBQ2xDRjtBRHFDQSxZQUFBO0FBRUE7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLHVCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsZUFBQTtBQ25DRjtBRHNDQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsMENBQUE7QUNuQ0Y7QUNqTUk7RUFDSSxVQUFBO0FEb01SO0FDbE1JO0VBQ0ksU0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtBRG9NUjtBQ2hNQTtFQUNJLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSw4SEFBQTtBRG1NSjtBQ2hNSTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtBRGtNUjtBQy9MQTtFQUNJLFlBQUE7RUFDQSxtQkFBQTtBRGtNSjtBQy9MQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0FEa01KO0FDOUxBO0VBQ0ksYUFBQTtBRGlNSjtBQzlMQTtFQUNJLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0FEaU1KO0FDOUxBO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7QURpTUo7QUM5TEk7RUFDSSxnQkFBQTtBRGdNUjtBQzVMQTtFQUVJLGFBQUE7QUQ4TEo7QUMzTEE7RUFDSSw2QkFBQTtBRDhMSjtBQzNMQTtFQUNJLGFBQUE7QUQ4TEo7QUMzTEE7RUFDSSxjQUFBO0VBQ0EsMkJBQUE7QUQ4TEo7QUMzTEE7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7QUQ4TEo7QUM3TEk7RUFDSSxVQUFBO0VBQ0EsbUJBQUE7QUQrTFI7QUMzTEE7RUFDSSxzQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUQ4TEo7QUMzTEE7RUFDSSxrQkFBQTtFQUNBLHdCQUFBO0FEOExKO0FDM0xBO0VBQ0ksa0JBQUE7RUFDQSwyQkFBQTtBRDhMSjtBQzNMQTs7OztDQUFBO0FBTUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0VBQ0EscURBQUE7QUQ2TEo7QUMxTEE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHNDQUFBO0VBQ0EsU0FBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQ0FBQTtVQUFBLDhCQUFBO0VBQ0EsV0FBQTtBRDZMSjtBQzFMQTtFQUNJO0lBQ0ksU0FBQTtJQUNBLHdDQUFBO0VENkxOO0VDM0xFO0lBQ0ksVUFBQTtJQUNBLDBDQUFBO0VENkxOO0FBQ0Y7QUNyTUE7RUFDSTtJQUNJLFNBQUE7SUFDQSx3Q0FBQTtFRDZMTjtFQzNMRTtJQUNJLFVBQUE7SUFDQSwwQ0FBQTtFRDZMTjtBQUNGO0FDMUxBOztDQUFBO0FBSUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUQyTEo7QUN4TEE7RUFDSSxzQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUQyTEo7QUN4TEE7RUFDSSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FEMkxKO0FDeExBOzs7O0NBQUE7QUFNQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FEMExKO0FDdkxBO0VBQ0ksMEJBQUE7RUFDQSxrQkFBQTtBRDBMSjtBQ3ZMQTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbURBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0FEMExKO0FDdkxBO0VBQW9CLHlCQUFBO0FEMkxwQjtBQzFMQTtFQUFpQix5QkFBQTtBRDhMakI7QUM3TEE7RUFBbUIseUJBQUE7QURpTW5CO0FDaE1BO0VBQWlCLHlCQUFBO0FEb01qQjtBQ25NQTtFQUFvQix5QkFBQTtBRHVNcEI7QUNyTUE7Ozs7Q0FBQTtBQU1BO0VBQ0ksV0FBQTtBRHVNSjtBQ3BNQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7QUR1TUo7QUVsYUE7RUFDSSxXQUFBO0FGcWFKO0FFbGFBO0VBQ0ksYUFBQTtFQUNBLDhCQUFBO0FGcWFKO0FFcGFJO0VBQ0ksVUFBQTtBRnNhUjtBRXBhUTtFQUNJLGlCQUFBO0FGc2FaIiwiZmlsZSI6InNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL25ldy1leGFtL25ldy1leGFtLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xyXG4kc21hbGw6IDAuOHJlbTtcclxuJG1lZGl1bTogMXJlbTtcclxuJGxhcmdlOiAxLjI1cmVtO1xyXG4kZXh0cmEtbGFyZ2U6IDEuNTYzcmVtO1xyXG4kaHVnZTogMS45NTNyZW07XHJcbiRleHRyYS1odWdlOiAyLjQ0MXJlbTtcclxuXHJcbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cclxuJGNvbG9yLXBhbGV0dGUtMS1tYWluOiAjREU3MzczO1xyXG4kY29sb3ItcGFsZXR0ZS0xLXNlY29uZDogI0FCQUJGRjtcclxuJGNvbG9yLXByaW1hcnktbGlnaHQ6ICNFRkVGRUY7XHJcbiRjb2xvci1wcmltYXJ5LWRhcms6ICMxOTE5MTk7XHJcbiRjb2xvci1zZWNvbmRhcnktZGFyazogIzMzMzMzMztcclxuJGNvbG9yLXNjcm9sbGJhci10aHVtYjogIzhDOEM4QztcclxuXHJcbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xyXG5cclxuXHJcbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cclxuXHJcbi5jLWNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuXHJcbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAuZHluLWZpbHRlcnMge1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG4gICAgb3ZlcmZsb3cteDogYXV0bztcclxuICAgIG1hcmdpbjogMCA0MHB4IDAgMDtcclxuXHJcbiAgICA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xyXG4gICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb25maWdCdXR0b24ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gIH1cclxufVxyXG5cclxuLmNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuICBtYXgtaGVpZ2h0OiA4OCU7XHJcblxyXG4gIC5jYXJkLWNvbnRlbnQsIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuXHJcbiAgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXBhbGV0dGUtMS1tYWluO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycmVtO1xyXG4gICAgICBoZWlnaHQ6IDIuM3JlbTtcclxuICAgICAgY29sb3I6ICNlZmVmZWY7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xyXG4gIGNvbG9yOiAkY29sb3ItcHJpbWFyeS1kYXJrO1xyXG59XHJcblxyXG4uZmlsdGVyIHtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcclxuICBtYXgtd2lkdGg6IDEwZW07XHJcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gIC8vIG1heC1oZWlnaHQ6IDgwMHB4O1xyXG4gIC5tYXQtdGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICB9XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItcm93IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XHJcbiAgY29sb3I6ICM4YjhiOGI7XHJcbn1cclxuXHJcbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xyXG4gIHBhZGRpbmctbGVmdDogMDtcclxufVxyXG5cclxuLmlkSGVhZGVyIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5pZENlbGwge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxubWF0LWNhcmQtY29udGVudCB7XHJcbiAgbWFyZ2luOiAwIDM0cHg7XHJcbn1cclxuXHJcbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXHJcblxyXG4ucHJpb3JpdHkge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLy8gQFRPRE86IGNoYW5nZSBjb2xvciB0byB2YXJpYWJsZXNcclxuXHJcbi5wcmlvcml0eS1tdXlhbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xyXG59XHJcblxyXG4ucHJpb3JpdHktYWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcclxufVxyXG5cclxuLnByaW9yaXR5LW5vcm1hbCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcclxufVxyXG5cclxuLnByaW9yaXR5LWJhamEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1uaW5ndW5hIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4ucmVkLWRhdGUge1xyXG4gIGNvbG9yOiAjZWI0MTQxO1xyXG59XHJcblxyXG4vKiBNQVQgVEFCICovXHJcblxyXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcclxuICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIC4xMik7XHJcbn1cclxuIiwiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xuLyogPT09PT0gV0VJR0hUID09PT09ICovXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXG4uYy1jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG5cbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyB7XG4gIGZsZXgtZ3JvdzogMTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbiAgbWFyZ2luOiAwIDQwcHggMCAwO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxNTBweDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5jb25maWdCdXR0b24ge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBmbGV4LXNocmluazogMDtcbn1cblxuLmNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG4gIG1heC1oZWlnaHQ6IDg4JTtcbn1cbi5jYXJkLWNvbXBvbmVudCAuY2FyZC1jb250ZW50LCAuY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMDtcbn1cbi5jYXJkLWNvbXBvbmVudCA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNERTczNzM7XG4gIGJvcmRlci1yYWRpdXM6IDJyZW07XG4gIGhlaWdodDogMi4zcmVtO1xuICBjb2xvcjogI2VmZWZlZjtcbn1cblxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcbiAgY29sb3I6ICMxOTE5MTk7XG59XG5cbi5maWx0ZXIge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMC41ZW07XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcbiAgbWF4LXdpZHRoOiAxMGVtO1xuICBtYXJnaW4tcmlnaHQ6IDVlbTtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93LXg6IGF1dG87XG59XG4udGFibGUtcmVzcG9uc2l2ZSAubWF0LXRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgZGlzcGxheTogdGFibGU7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4ubWF0LWhlYWRlci1yb3cge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xuICBjb2xvcjogIzhiOGI4Yjtcbn1cblxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cblxuLmlkSGVhZGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmlkQ2VsbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG5tYXQtY2FyZC1jb250ZW50IHtcbiAgbWFyZ2luOiAwIDM0cHg7XG59XG5cbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXG4ucHJpb3JpdHkge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5wcmlvcml0eS1tdXlhbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcbn1cblxuLnByaW9yaXR5LWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xufVxuXG4ucHJpb3JpdHktbm9ybWFsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcbn1cblxuLnByaW9yaXR5LWJhamEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xufVxuXG4ucHJpb3JpdHktbmluZ3VuYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5yZWQtZGF0ZSB7XG4gIGNvbG9yOiAjZWI0MTQxO1xufVxuXG4vKiBNQVQgVEFCICovXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG59XG5cbjo6bmctZGVlcCAubWF0LWRpYWxvZy1jb250YWluZXIge1xuICBwYWRkaW5nOiAwO1xufVxuOjpuZy1kZWVwIC5tYXQtZGlhbG9nLWNvbnRlbnQge1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG4gIG1pbi1oZWlnaHQ6IDQ3MHB4O1xufVxuXG4uYmxhY2stZGlhbG9nIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIHdpZHRoOiAxNjVweDtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJveC1zaGFkb3c6IDBweCAxMXB4IDE1cHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMiksIDBweCAyNHB4IDE5cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xNCksIDBweCA5cHggMzZweCA4cHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn1cbi5ibGFjay1kaWFsb2cgaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDIxMHB4O1xufVxuXG4ucmVhZE9ubHlJbnB1dCB7XG4gIG9wYWNpdHk6IDAuMztcbiAgY3Vyc29yOiBub3QtYWxsb3dlZDtcbn1cblxuLmRpYWxvZyB7XG4gIHdpZHRoOiA4NTBweDtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLmRldGFpbCB7XG4gIGhlaWdodDogNTAwcHg7XG59XG5cbi5mb3JtIHtcbiAgbWluLWhlaWdodDogNTUwcHg7XG4gIGhlaWdodDogNTUwcHg7XG4gIG1heC1oZWlnaHQ6IDU1MHB4O1xufVxuXG4uZGlhbG9nLWNvbnRlbnQge1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgcGFkZGluZzogNDBweCA0MHB4IDAgODBweDtcbn1cbi5kaWFsb2ctY29udGVudCBzcGFuIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLmRpYWxvZy1jb250ZW50LWRldGFpbCB7XG4gIHBhZGRpbmc6IDU1cHg7XG59XG5cbi5kaWFsb2ctY29udGVudC1mb3JtIHtcbiAgcGFkZGluZzogMzBweCA1MHB4IDMwcHggMTAwcHg7XG59XG5cbi50aXRsZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5yZWQtZGF0ZSB7XG4gIGNvbG9yOiAjZWI0MTQxO1xuICBmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XG59XG5cbi5jb2x1bW5zIHtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5jb2x1bW5zIHNlY3Rpb24ge1xuICB3aWR0aDogNTAlO1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuXG4uZXhpdC1idXR0b24ge1xuICBtYXJnaW46IDMwcHggMCAwIDUwMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xuICBjb2xvcjogd2hpdGU7XG4gIHdpZHRoOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuXG4uY2lyY2xlLWNvbnRlbnQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbjogMjBweCA0MHB4IDAgNTBweDtcbn1cblxuLmNpcmNsZS1jb250ZW50LW9wcG9ydHVuaXR5IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW46IGF1dG8gNDBweCBhdXRvIGF1dG87XG59XG5cbi8qIFxuICAgIC0tLS0tLS0tLS0tLS0tXG4gICAgRFlOQU1JQyBDSVJDTEVcbiAgICAtLS0tLS0tLS0tLS0tLVxuKi9cbi5jaXJjbGUge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBib3JkZXI6IDBweCBzb2xpZCAjYzJjMmMyO1xuICBib3JkZXItcmFkaXVzOiAxMjBweDtcbiAgbGluZS1oZWlnaHQ6IDIwMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjZGRkO1xuICBmb250LXNpemU6IDI1cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgei1pbmRleDogMTtcbiAgYm94LXNoYWRvdzogcmdiYSgxMDAsIDEwMCwgMTExLCAwLjIpIDBweCA3cHggMjlweCAwcHg7XG59XG5cbi5jaXJjbGU6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogODVweDtcbiAgaGVpZ2h0OiA4NXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS10b29sdGlwLWNvbG9yKTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7XG4gIGJvcmRlci1yYWRpdXM6IDQwJTtcbiAgYW5pbWF0aW9uOiBmaWxsIDNzIGVhc2UtaW4tb3V0O1xuICB6LWluZGV4OiAtMTtcbn1cblxuQGtleWZyYW1lcyBmaWxsIHtcbiAgZnJvbSB7XG4gICAgdG9wOiAzMHB4O1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoMGRlZyk7XG4gIH1cbiAgdG8ge1xuICAgIHRvcDogLTUwcHg7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgzNjBkZWcpO1xuICB9XG59XG4vKiBcbiAgICBGT1JNIFxuKi9cbi5idXR0b25zIHtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLmV4aXQtYnV0dG9uLWZvcm0ge1xuICBtYXJnaW46IDMwcHggMCAwIDMyNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xuICBjb2xvcjogd2hpdGU7XG4gIHdpZHRoOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuXG4uc3VibWl0LWJ1dHRvbiB7XG4gIG1hcmdpbjogMzBweCAwIDAgMjVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U0NmU3NTtcbiAgY29sb3I6IHdoaXRlO1xuICBtaW4td2lkdGg6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICBoZWlnaHQ6IDQwcHg7XG59XG5cbi8qIFxuICAgIENJUkNMRSBDT0xPUiBcbiAgICAgICAgJiZcbiAgICBQUklPUklUWVxuKi9cbi5hY3Rpdml0eS1jb2xvciwgLm9wcG9ydHVuaXR5LWNvbG9yIHtcbiAgd2lkdGg6IDE4cHg7XG4gIGhlaWdodDogMThweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4uYWN0aXZpdHktcHJpb3JpdHksIC5vcHBvcnR1bml0eS1wcmlvcml0eSB7XG4gIG1hcmdpbjogYXV0byAwcHggYXV0byBhdXRvO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5wcmlvcml0eSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA1cHggMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLnByaW9yaXR5LW11eWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xufVxuXG4ucHJpb3JpdHktYWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XG59XG5cbi5wcmlvcml0eS1ub3JtYWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4ucHJpb3JpdHktYmFqYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XG59XG5cbi5wcmlvcml0eS1uaW5ndW5hIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLyogXG4gICAgLS0tLS0tXG4gICAgU0NST0xMXG4gICAgLS0tLS0tXG4qL1xuLmRpYWxvZy1jb250ZW50Ojotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMHB4O1xufVxuXG4uZGlhbG9nLWNvbnRlbnQ6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2MyYzJjMjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuXG4uZnVsbC13aWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uc2VsZWN0b3JzIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLnNlbGVjdG9ycyBzZWN0aW9uIHtcbiAgd2lkdGg6IDUwJTtcbn1cbi5zZWxlY3RvcnMgc2VjdGlvbiAubWF0LXNlbGVjdCB7XG4gIGRpc3BsYXk6IGNvbnRlbnRzO1xufSIsIkBpbXBvcnQgXCIuL3N0YWZmaXQtc3R5bGUtMVwiO1xyXG5cclxuOjpuZy1kZWVwIHtcclxuICAgIC5tYXQtZGlhbG9nLWNvbnRhaW5lciB7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgIH1cclxuICAgIC5tYXQtZGlhbG9nLWNvbnRlbnQge1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDQ3MHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uYmxhY2stZGlhbG9nIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gICAgd2lkdGg6IDE2NXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDExcHggMTVweCAtN3B4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMHB4IDI0cHggMTlweCAzcHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMHB4IDlweCAzNnB4IDhweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG5cclxuXHJcbiAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIxMHB4O1xyXG4gICAgfVxyXG59XHJcbi5yZWFkT25seUlucHV0e1xyXG4gICAgb3BhY2l0eTogMC4zO1xyXG4gICAgY3Vyc29yOiBub3QtYWxsb3dlZDtcclxufVxyXG5cclxuLmRpYWxvZyB7XHJcbiAgICB3aWR0aDogODUwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxufVxyXG5cclxuLmRldGFpbCB7XHJcbiAgICBoZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4uZm9ybSB7XHJcbiAgICBtaW4taGVpZ2h0OiA1NTBweDtcclxuICAgIGhlaWdodDogNTUwcHg7XHJcbiAgICBtYXgtaGVpZ2h0OiA1NTBweDtcclxufVxyXG5cclxuLmRpYWxvZy1jb250ZW50IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgb3ZlcmZsb3cteTogYXV0bztcclxuICAgIHBhZGRpbmc6IDQwcHggNDBweCAwIDgwcHg7XHJcbiAgICAvLyBkaXNwbGF5OiBncmlkO1xyXG5cclxuICAgIHNwYW4ge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudC1kZXRhaWwge1xyXG4gICAgLy8gcGFkZGluZzogNDBweCA0MHB4IDAgODBweDtcclxuICAgIHBhZGRpbmc6IDU1cHg7XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudC1mb3JtIHtcclxuICAgIHBhZGRpbmc6IDMwcHggNTBweCAzMHB4IDEwMHB4O1xyXG59XHJcblxyXG4udGl0bGUgeyBcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5yZWQtZGF0ZSB7XHJcbiAgICBjb2xvcjogI2ViNDE0MTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNvbHVtbnMge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICBzZWN0aW9uIHtcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5leGl0LWJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDMwcHggMCAwIDUwMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHdpZHRoOiAxMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbn1cclxuXHJcbi5jaXJjbGUtY29udGVudHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbjogMjBweCA0MHB4IDAgNTBweDtcclxufVxyXG5cclxuLmNpcmNsZS1jb250ZW50LW9wcG9ydHVuaXR5e1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luOiBhdXRvIDQwcHggYXV0byBhdXRvO1xyXG59XHJcblxyXG4vKiBcclxuICAgIC0tLS0tLS0tLS0tLS0tXHJcbiAgICBEWU5BTUlDIENJUkNMRVxyXG4gICAgLS0tLS0tLS0tLS0tLS1cclxuKi9cclxuXHJcbi5jaXJjbGUge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBib3JkZXI6IDBweCBzb2xpZCAjYzJjMmMyO1xyXG4gICAgYm9yZGVyLXJhZGl1czoxMjBweDtcclxuICAgIGxpbmUtaGVpZ2h0OjIwMHB4O1xyXG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICBjb2xvcjojZGRkO1xyXG4gICAgZm9udC1zaXplOjI1cHg7XHJcbiAgICBmb250LXdlaWdodDo2MDA7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBvdmVyZmxvdzpoaWRkZW47XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgYm94LXNoYWRvdzogcmdiYSgxMDAsIDEwMCwgMTExLCAwLjIpIDBweCA3cHggMjlweCAwcHg7XHJcbn1cclxuXHJcbi5jaXJjbGU6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6XCJcIjtcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgd2lkdGg6ODVweDtcclxuICAgIGhlaWdodDo4NXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tdG9vbHRpcC1jb2xvcik7XHJcbiAgICBsZWZ0OjUwJTtcclxuICAgIHRyYW5zZm9ybTp0cmFuc2xhdGVYKC01MCUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czo0MCU7XHJcbiAgICBhbmltYXRpb246ZmlsbCAzcyBlYXNlLWluLW91dDtcclxuICAgIHotaW5kZXg6LTE7XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgZmlsbCB7XHJcbiAgICBmcm9tIHtcclxuICAgICAgICB0b3A6MzBweDtcclxuICAgICAgICB0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICB0byB7XHJcbiAgICAgICAgdG9wOi01MHB4O1xyXG4gICAgICAgIHRyYW5zZm9ybTp0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgzNjBkZWcpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBcclxuICAgIEZPUk0gXHJcbiovXHJcblxyXG4uYnV0dG9ucyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxufVxyXG5cclxuLmV4aXQtYnV0dG9uLWZvcm0ge1xyXG4gICAgbWFyZ2luOiAzMHB4IDAgMCAzMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG4uc3VibWl0LWJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDMwcHggMCAwIDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTQ2ZTc1O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgbWluLXdpZHRoOiAxMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbn1cclxuXHJcbi8qIFxyXG4gICAgQ0lSQ0xFIENPTE9SIFxyXG4gICAgICAgICYmXHJcbiAgICBQUklPUklUWVxyXG4qL1xyXG5cclxuLmFjdGl2aXR5LWNvbG9yLCAub3Bwb3J0dW5pdHktY29sb3Ige1xyXG4gICAgd2lkdGg6IDE4cHg7XHJcbiAgICBoZWlnaHQ6IDE4cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG4uYWN0aXZpdHktcHJpb3JpdHksIC5vcHBvcnR1bml0eS1wcmlvcml0eSB7XHJcbiAgICBtYXJnaW46IGF1dG8gMHB4IGF1dG8gYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnByaW9yaXR5IHtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7IGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTUgfVxyXG4ucHJpb3JpdHktYWx0YSB7IGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzEgfVxyXG4ucHJpb3JpdHktbm9ybWFsIHsgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZSB9XHJcbi5wcmlvcml0eS1iYWphIHsgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZCB9XHJcbi5wcmlvcml0eS1uaW5ndW5hIHsgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMiB9XHJcblxyXG4vKiBcclxuICAgIC0tLS0tLVxyXG4gICAgU0NST0xMXHJcbiAgICAtLS0tLS1cclxuKi9cclxuXHJcbi5kaWFsb2ctY29udGVudDo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gICAgd2lkdGg6IDEwcHg7XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudDo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gICAgYmFja2dyb3VuZDogI2MyYzJjMjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxufSIsIlxyXG5AaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2RpYWxvZy11bmlmaWVkJztcclxuXHJcbi5mdWxsLXdpZHRoe1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5zZWxlY3RvcnN7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgc2VjdGlvbntcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG5cclxuICAgICAgICAubWF0LXNlbGVjdHtcclxuICAgICAgICAgICAgZGlzcGxheTogY29udGVudHM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NewExamComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-new-exam',
          templateUrl: './new-exam.component.html',
          styleUrls: ['./new-exam.component.scss']
        }]
      }], function () {
        return [{
          type: _test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_3__["TestService"]
        }, {
          type: _correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_4__["CorrectionService"]
        }, {
          type: _services_exam_service__WEBPACK_IMPORTED_MODULE_5__["ExamService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"]
        }, {
          type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__["MatDialogRef"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/ng-components/send-exam/send-exam.component.ts":
  /*!************************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/ng-components/send-exam/send-exam.component.ts ***!
    \************************************************************************************************/

  /*! exports provided: SendExamComponent */

  /***/
  function srcAppEvaluationsExamInfrastructureNgComponentsSendExamSendExamComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendExamComponent", function () {
      return SendExamComponent;
    });
    /* harmony import */


    var _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/cdk/keycodes */
    "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/keycodes.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _recruitment_candidate_application_searchCandidates__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../../../recruitment/candidate/application/searchCandidates */
    "./src/app/recruitment/candidate/application/searchCandidates.ts");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _application_sendExam__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../application/sendExam */
    "./src/app/evaluations/exam/application/sendExam.ts");
    /* harmony import */


    var _services_exam_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../services/exam.service */
    "./src/app/evaluations/exam/infrastructure/services/exam.service.ts");
    /* harmony import */


    var _recruitment_candidate_infrastructure_services_candidate_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../../../recruitment/candidate/infrastructure/services/candidate.service */
    "./src/app/recruitment/candidate/infrastructure/services/candidate.service.ts");
    /* harmony import */


    var _recruitment_candidate_infrastructure_CandidateMapper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../../../recruitment/candidate/infrastructure/CandidateMapper */
    "./src/app/recruitment/candidate/infrastructure/CandidateMapper.ts");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
    /* harmony import */


    var _shared_custom_mat_elements_autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/autocomplete/autocomplete.component */
    "./src/app/shared/custom-mat-elements/autocomplete/autocomplete.component.ts");
    /* harmony import */


    var _angular_material_chips__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/material/chips */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/chips.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");

    function SendExamComponent_mat_chip_19_Template(rf, ctx) {
      if (rf & 1) {
        var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "mat-chip", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("removed", function SendExamComponent_mat_chip_19_Template_mat_chip_removed_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4);

          var candidate_r2 = ctx.$implicit;

          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r3.remove(candidate_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "mat-icon", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "cancel");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var candidate_r2 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("selectable", false)("removable", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate3"](" ", candidate_r2.email, " (", candidate_r2.name, " ", candidate_r2.surname, ") ");
      }
    }

    var SendExamComponent = /*#__PURE__*/function () {
      function SendExamComponent(_fb, dialogRef, data, _examService, _candidateService, _candidateMapper) {
        _classCallCheck(this, SendExamComponent);

        this._fb = _fb;
        this.dialogRef = dialogRef;
        this.data = data;
        this._examService = _examService;
        this._candidateService = _candidateService;
        this._candidateMapper = _candidateMapper;
        this.separatorKeysCodes = [_angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_0__["ENTER"], _angular_cdk_keycodes__WEBPACK_IMPORTED_MODULE_0__["COMMA"]];
        this.selected = [];
        this.buffer = [];
        this.emails = [];
        this.candidateEmails = [];
      }

      _createClass(SendExamComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this13 = this;

          this.data.candidates.forEach(function (candidate) {
            candidate.candidateEmails.forEach(function (email) {
              _this13.emails.push("".concat(email.email, " (").concat(candidate.name, " ").concat(candidate.surname, ")"));

              _this13.candidateEmails.push(email);

              _this13.buffer.push({
                idCandidate: candidate.id,
                email: email.email,
                name: candidate.name,
                surname: candidate.surname
              });
            });
          });
          this.formInit();
        }
      }, {
        key: "formInit",
        value: function formInit() {
          this.form = this._fb.group({
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email],
            candidate: [''],
            exam: [this.data.exam.exam.name]
          });
        }
      }, {
        key: "autocompleteSelected",
        value: function autocompleteSelected(selectedRow) {
          if (selectedRow) {
            var splitedRow = selectedRow.split(' ');
            var candidateEmail = this.candidateEmails.find(function (e) {
              return e.email === splitedRow[0];
            });
            var candidate = this.buffer.find(function (c) {
              return c.email === splitedRow[0];
            });
            var selected = {
              idCandidate: candidate.idCandidate,
              idEmail: candidateEmail.id,
              email: candidateEmail.email,
              name: candidate.name,
              surname: candidate.surname
            };
            var contained = this.selected.find(function (c) {
              return c.idCandidate === candidate.idCandidate;
            });

            if (contained) {
              var index = this.selected.indexOf(contained);
              this.selected[index] = selected;
            } else {
              this.selected.push(selected);
            }
          }
        }
      }, {
        key: "remove",
        value: function remove(candidate) {
          var index = this.selected.indexOf(candidate);

          if (index > -1) {
            this.selected.splice(index, 1);
          }
        }
      }, {
        key: "onNoClick",
        value: function onNoClick() {
          this.dialogRef.close();
        }
      }, {
        key: "onCancelClick",
        value: function onCancelClick() {
          this.dialogRef.close();
        }
      }, {
        key: "onSaveClick",
        value: function onSaveClick() {
          var _this14 = this;

          var idExam = this.data.exam.exam.id;
          var sentExams = [];

          var _iterator = _createForOfIteratorHelper(this.selected),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var candidate = _step.value;
              var sentEmail = Object(_application_sendExam__WEBPACK_IMPORTED_MODULE_7__["sendExam"])(candidate.idEmail, idExam, this._examService);
              sentExams.push(sentEmail);
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["forkJoin"])(sentExams).subscribe(function () {
            Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_6__["successAlert"])('All users received the email correctly').then(function () {
              return _this14.dialogRef.close();
            });
          });
        }
      }, {
        key: "backSearch",
        value: function backSearch(event) {
          var _this15 = this;

          Object(_recruitment_candidate_application_searchCandidates__WEBPACK_IMPORTED_MODULE_5__["searchCandidates"])({
            email: event
          }, this._candidateService, this._candidateMapper).subscribe(function (candidates) {
            var candidate = candidates.content;
            _this15.emails = [];
            _this15.candidateEmails = [];
            _this15.buffer = [];
            candidate.forEach(function (candidate) {
              candidate.candidateEmails.forEach(function (email) {
                _this15.emails.push("".concat(email.email, " (").concat(candidate.name, " ").concat(candidate.surname, ")"));

                _this15.candidateEmails.push(email);

                _this15.buffer.push({
                  idCandidate: candidate.id,
                  email: email.email,
                  name: candidate.name,
                  surname: candidate.surname
                });
              });
            });
          });
        }
      }]);

      return SendExamComponent;
    }();

    SendExamComponent.ɵfac = function SendExamComponent_Factory(t) {
      return new (t || SendExamComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_services_exam_service__WEBPACK_IMPORTED_MODULE_8__["ExamService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_recruitment_candidate_infrastructure_services_candidate_service__WEBPACK_IMPORTED_MODULE_9__["CandidateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_recruitment_candidate_infrastructure_CandidateMapper__WEBPACK_IMPORTED_MODULE_10__["CandidateMapper"]));
    };

    SendExamComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: SendExamComponent,
      selectors: [["app-send-exam"]],
      decls: 26,
      vars: 10,
      consts: [[1, "dialog", "form"], [1, "black-dialog"], ["src", "../../../../../../assets/CRM/icons/dialogs/exam.png", "alt", "exam image"], [1, "dialog-content", "dialog-content-form", 3, "formGroup"], [1, "title"], [1, "columns"], ["appearance", "outline"], ["matInput", "", "placeholder", "Exam", "formControlName", "exam", "readonly", ""], [3, "placeholder", "matLabel", "options", "appearance", "defaultValue", "width", "selectedValue", "searchValue"], ["appearance", "outline", 1, "w100"], ["aria-label", "Fruit selection"], ["chipList", ""], [3, "selectable", "removable", "removed", 4, "ngFor", "ngForOf"], ["formControlName", "candidate", "readonly", "", 3, "matChipInputFor", "matChipInputSeparatorKeyCodes"], [1, "buttons"], ["mat-raised-button", "", 1, "exit-button-form", 3, "click"], ["mat-raised-button", "", "type", "submit", 1, "submit-button", 3, "click"], [3, "selectable", "removable", "removed"], ["matChipRemove", ""]],
      template: function SendExamComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "mat-dialog-content", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "img", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "form", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Send Exam");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "section");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "mat-form-field", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Exam to send");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "input", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "app-autocomplete", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("selectedValue", function SendExamComponent_Template_app_autocomplete_selectedValue_13_listener($event) {
            return ctx.autocompleteSelected($event);
          })("searchValue", function SendExamComponent_Template_app_autocomplete_searchValue_13_listener($event) {
            return ctx.backSearch($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "mat-form-field", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Selected Candidates");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "mat-chip-list", 10, 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, SendExamComponent_mat_chip_19_Template, 4, 5, "mat-chip", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](20, "input", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "button", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SendExamComponent_Template_button_click_22_listener() {
            return ctx.onCancelClick();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "Exit");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "button", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SendExamComponent_Template_button_click_24_listener() {
            return ctx.onSaveClick();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25, "Send");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](18);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.form);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("placeholder", "Search by email")("matLabel", "Candidate email")("options", ctx.emails)("appearance", "outline")("defaultValue", "")("width", "100%");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.selected);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("matChipInputFor", _r0)("matChipInputSeparatorKeyCodes", ctx.separatorKeysCodes);
        }
      },
      directives: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_12__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControlName"], _shared_custom_mat_elements_autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_13__["AutocompleteComponent"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_14__["MatChipList"], _angular_common__WEBPACK_IMPORTED_MODULE_15__["NgForOf"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_14__["MatChipInput"], _angular_material_button__WEBPACK_IMPORTED_MODULE_16__["MatButton"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_14__["MatChip"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__["MatIcon"], _angular_material_chips__WEBPACK_IMPORTED_MODULE_14__["MatChipRemove"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n  .mat-dialog-container {\n  padding: 0;\n}\n  .mat-dialog-content {\n  margin: 0;\n  padding: 0;\n  min-height: 470px;\n}\n.black-dialog[_ngcontent-%COMP%] {\n  background-color: black;\n  width: 165px;\n  padding-left: 15px;\n  border-radius: 5px;\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 19px 3px rgba(0, 0, 0, 0.14), 0px 9px 36px 8px rgba(0, 0, 0, 0.12);\n}\n.black-dialog[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-top: 210px;\n}\n.readOnlyInput[_ngcontent-%COMP%] {\n  opacity: 0.3;\n  cursor: not-allowed;\n}\n.dialog[_ngcontent-%COMP%] {\n  width: 850px;\n  display: flex;\n}\n.detail[_ngcontent-%COMP%] {\n  height: 500px;\n}\n.form[_ngcontent-%COMP%] {\n  min-height: 550px;\n  height: 550px;\n  max-height: 550px;\n}\n.dialog-content[_ngcontent-%COMP%] {\n  width: 100%;\n  overflow-y: auto;\n  padding: 40px 40px 0 80px;\n}\n.dialog-content[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.dialog-content-detail[_ngcontent-%COMP%] {\n  padding: 55px;\n}\n.dialog-content-form[_ngcontent-%COMP%] {\n  padding: 30px 50px 30px 100px;\n}\n.title[_ngcontent-%COMP%] {\n  display: flex;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n  font-weight: 400 !important;\n}\n.columns[_ngcontent-%COMP%] {\n  display: flex;\n  margin-top: 15px;\n}\n.columns[_ngcontent-%COMP%]   section[_ngcontent-%COMP%] {\n  width: 50%;\n  padding-right: 10px;\n}\n.exit-button[_ngcontent-%COMP%] {\n  margin: 30px 0 0 500px;\n  background-color: #c2c2c2;\n  color: white;\n  width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n.circle-content[_ngcontent-%COMP%] {\n  position: relative;\n  margin: 20px 40px 0 50px;\n}\n.circle-content-opportunity[_ngcontent-%COMP%] {\n  position: relative;\n  margin: auto 40px auto auto;\n}\n\n.circle[_ngcontent-%COMP%] {\n  width: 30px;\n  height: 30px;\n  border: 0px solid #c2c2c2;\n  border-radius: 120px;\n  line-height: 200px;\n  text-align: center;\n  color: #ddd;\n  font-size: 25px;\n  font-weight: 600;\n  position: absolute;\n  overflow: hidden;\n  z-index: 1;\n  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;\n}\n.circle[_ngcontent-%COMP%]:before {\n  content: \"\";\n  position: absolute;\n  width: 85px;\n  height: 85px;\n  background-color: var(--tooltip-color);\n  left: 50%;\n  transform: translateX(-50%);\n  border-radius: 40%;\n  -webkit-animation: fill 3s ease-in-out;\n          animation: fill 3s ease-in-out;\n  z-index: -1;\n}\n@-webkit-keyframes fill {\n  from {\n    top: 30px;\n    transform: translateX(-50%) rotate(0deg);\n  }\n  to {\n    top: -50px;\n    transform: translateX(-50%) rotate(360deg);\n  }\n}\n@keyframes fill {\n  from {\n    top: 30px;\n    transform: translateX(-50%) rotate(0deg);\n  }\n  to {\n    top: -50px;\n    transform: translateX(-50%) rotate(360deg);\n  }\n}\n\n.buttons[_ngcontent-%COMP%] {\n  display: flex;\n  margin-bottom: 30px;\n}\n.exit-button-form[_ngcontent-%COMP%] {\n  margin: 30px 0 0 325px;\n  background-color: #c2c2c2;\n  color: white;\n  width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n.submit-button[_ngcontent-%COMP%] {\n  margin: 30px 0 0 25px;\n  background-color: #e46e75;\n  color: white;\n  min-width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n\n.activity-color[_ngcontent-%COMP%], .opportunity-color[_ngcontent-%COMP%] {\n  width: 18px;\n  height: 18px;\n  border-radius: 15px;\n  margin: auto;\n}\n.activity-priority[_ngcontent-%COMP%], .opportunity-priority[_ngcontent-%COMP%] {\n  margin: auto 0px auto auto;\n  text-align: center;\n}\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n\n.dialog-content[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 10px;\n}\n.dialog-content[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #c2c2c2;\n  border-radius: 4px;\n}\n.columns[_ngcontent-%COMP%]   section[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.columns[_ngcontent-%COMP%]   section[_ngcontent-%COMP%]   .w100[_ngcontent-%COMP%] {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3NlbmQtZXhhbS9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXNzZXRzXFxzdHlsZXNcXHN0YWZmaXQtc3R5bGUtMS5zY3NzIiwic3JjL2FwcC9ldmFsdWF0aW9ucy9leGFtL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvc2VuZC1leGFtL3NlbmQtZXhhbS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3NlbmQtZXhhbS9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXNzZXRzXFxzdHlsZXNcXGRpYWxvZy11bmlmaWVkLnNjc3MiLCJzcmMvYXBwL2V2YWx1YXRpb25zL2V4YW0vaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9zZW5kLWV4YW0vQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFwcFxcZXZhbHVhdGlvbnNcXGV4YW1cXGluZnJhc3RydWN0dXJlXFxuZy1jb21wb25lbnRzXFxzZW5kLWV4YW1cXHNlbmQtZXhhbS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQ0FBQTtBQVFBLHdCQUFBO0FBUUEsdUJBQUE7QUFHQSwyQkFBQTtBQUVBO0VBQ0UsNkJBQUE7QUNoQkY7QURrQkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ2hCSjtBRGtCSTtFQUNFLG1CQTNCUTtFQTRCUixnQkFBQTtFQUNBLGNBQUE7QUNoQk47QURzQkE7RUFDRSxhQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNuQkY7QURxQkU7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ25CSjtBRHFCSTtFQUNFLFlBQUE7QUNuQk47QURzQkU7RUFDRSxtQkFBQTtFQUNBLGNBQUE7QUNwQko7QUR3QkE7RUFDRSw2QkFBQTtFQUNBLGVBQUE7QUNyQkY7QUR1QkU7RUFDRSxTQUFBO0FDckJKO0FEd0JFO0VBQ0UsZ0NBQUE7QUN0Qko7QUR5QkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ3ZCSjtBRHlCSTtFQUNFLG1CQTNFUTtFQTRFUixnQkFBQTtFQUNBLGNBQUE7QUN2Qk47QUQwQkk7RUFDRSx5QkE1RWlCO0VBNkVqQixtQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FDeEJOO0FENkJBO0VBQ0UsY0FsRm1CO0FDd0RyQjtBRDZCQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQzFCRjtBRDZCQTtFQUNFLFdBQUE7QUMxQkY7QUQ2QkE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDMUJGO0FENEJFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDMUJKO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUMzQkY7QUQ4QkE7RUFDRSxlQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0Usa0JBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7QUMzQkY7QURrQ0E7RUFDRSxjQUFBO0FDL0JGO0FEa0NBLHlCQUFBO0FBRUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1EQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLHlCQUFBO0FDaENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLGNBQUE7QUNsQ0Y7QURxQ0EsWUFBQTtBQUVBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSx1QkFBQTtBQ25DRjtBRHNDQTtFQUNFLGVBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLDBDQUFBO0FDbkNGO0FDak1JO0VBQ0ksVUFBQTtBRG9NUjtBQ2xNSTtFQUNJLFNBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7QURvTVI7QUNoTUE7RUFDSSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsOEhBQUE7QURtTUo7QUNoTUk7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7QURrTVI7QUMvTEE7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7QURrTUo7QUMvTEE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBRGtNSjtBQzlMQTtFQUNJLGFBQUE7QURpTUo7QUM5TEE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtBRGlNSjtBQzlMQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0FEaU1KO0FDOUxJO0VBQ0ksZ0JBQUE7QURnTVI7QUM1TEE7RUFFSSxhQUFBO0FEOExKO0FDM0xBO0VBQ0ksNkJBQUE7QUQ4TEo7QUMzTEE7RUFDSSxhQUFBO0FEOExKO0FDM0xBO0VBQ0ksY0FBQTtFQUNBLDJCQUFBO0FEOExKO0FDM0xBO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0FEOExKO0FDN0xJO0VBQ0ksVUFBQTtFQUNBLG1CQUFBO0FEK0xSO0FDM0xBO0VBQ0ksc0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FEOExKO0FDM0xBO0VBQ0ksa0JBQUE7RUFDQSx3QkFBQTtBRDhMSjtBQzNMQTtFQUNJLGtCQUFBO0VBQ0EsMkJBQUE7QUQ4TEo7QUMzTEE7Ozs7Q0FBQTtBQU1BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLHFEQUFBO0FENkxKO0FDMUxBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxzQ0FBQTtFQUNBLFNBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0NBQUE7VUFBQSw4QkFBQTtFQUNBLFdBQUE7QUQ2TEo7QUMxTEE7RUFDSTtJQUNJLFNBQUE7SUFDQSx3Q0FBQTtFRDZMTjtFQzNMRTtJQUNJLFVBQUE7SUFDQSwwQ0FBQTtFRDZMTjtBQUNGO0FDck1BO0VBQ0k7SUFDSSxTQUFBO0lBQ0Esd0NBQUE7RUQ2TE47RUMzTEU7SUFDSSxVQUFBO0lBQ0EsMENBQUE7RUQ2TE47QUFDRjtBQzFMQTs7Q0FBQTtBQUlBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0FEMkxKO0FDeExBO0VBQ0ksc0JBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FEMkxKO0FDeExBO0VBQ0kscUJBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBRDJMSjtBQ3hMQTs7OztDQUFBO0FBTUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBRDBMSjtBQ3ZMQTtFQUNJLDBCQUFBO0VBQ0Esa0JBQUE7QUQwTEo7QUN2TEE7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1EQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtBRDBMSjtBQ3ZMQTtFQUFvQix5QkFBQTtBRDJMcEI7QUMxTEE7RUFBaUIseUJBQUE7QUQ4TGpCO0FDN0xBO0VBQW1CLHlCQUFBO0FEaU1uQjtBQ2hNQTtFQUFpQix5QkFBQTtBRG9NakI7QUNuTUE7RUFBb0IseUJBQUE7QUR1TXBCO0FDck1BOzs7O0NBQUE7QUFNQTtFQUNJLFdBQUE7QUR1TUo7QUNwTUE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0FEdU1KO0FFbGFJO0VBQ0ksV0FBQTtBRnFhUjtBRXBhUTtFQUFPLFdBQUE7QUZ1YWYiLCJmaWxlIjoic3JjL2FwcC9ldmFsdWF0aW9ucy9leGFtL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvc2VuZC1leGFtL3NlbmQtZXhhbS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cclxuJHNtYWxsOiAwLjhyZW07XHJcbiRtZWRpdW06IDFyZW07XHJcbiRsYXJnZTogMS4yNXJlbTtcclxuJGV4dHJhLWxhcmdlOiAxLjU2M3JlbTtcclxuJGh1Z2U6IDEuOTUzcmVtO1xyXG4kZXh0cmEtaHVnZTogMi40NDFyZW07XHJcblxyXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXHJcbiRjb2xvci1wYWxldHRlLTEtbWFpbjogI0RFNzM3MztcclxuJGNvbG9yLXBhbGV0dGUtMS1zZWNvbmQ6ICNBQkFCRkY7XHJcbiRjb2xvci1wcmltYXJ5LWxpZ2h0OiAjRUZFRkVGO1xyXG4kY29sb3ItcHJpbWFyeS1kYXJrOiAjMTkxOTE5O1xyXG4kY29sb3Itc2Vjb25kYXJ5LWRhcms6ICMzMzMzMzM7XHJcbiRjb2xvci1zY3JvbGxiYXItdGh1bWI6ICM4QzhDOEM7XHJcblxyXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cclxuXHJcblxyXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXHJcblxyXG4uYy1jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcblxyXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgLmR5bi1maWx0ZXJzIHtcclxuICAgIGZsZXgtZ3JvdzogMTtcclxuICAgIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgICBtYXJnaW46IDAgNDBweCAwIDA7XHJcblxyXG4gICAgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcclxuICAgICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29uZmlnQnV0dG9uIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcbiAgbWF4LWhlaWdodDogODglO1xyXG5cclxuICAuY2FyZC1jb250ZW50LCAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1wYWxldHRlLTEtbWFpbjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMnJlbTtcclxuICAgICAgaGVpZ2h0OiAyLjNyZW07XHJcbiAgICAgIGNvbG9yOiAjZWZlZmVmO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcclxuICBjb2xvcjogJGNvbG9yLXByaW1hcnktZGFyaztcclxufVxyXG5cclxuLmZpbHRlciB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMC41ZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XHJcbiAgbWF4LXdpZHRoOiAxMGVtO1xyXG4gIG1hcmdpbi1yaWdodDogNWVtO1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuICAvLyBtYXgtaGVpZ2h0OiA4MDBweDtcclxuICAubWF0LXRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgfVxyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xyXG4gIGNvbG9yOiAjOGI4YjhiO1xyXG59XHJcblxyXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbn1cclxuXHJcbi5pZEhlYWRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWRDZWxsIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbm1hdC1jYXJkLWNvbnRlbnQge1xyXG4gIG1hcmdpbjogMCAzNHB4O1xyXG59XHJcblxyXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xyXG5cclxuLnByaW9yaXR5IHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi8vIEBUT0RPOiBjaGFuZ2UgY29sb3IgdG8gdmFyaWFibGVzXHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcclxufVxyXG5cclxuLnByaW9yaXR5LWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1ub3JtYWwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1iYWphIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbmluZ3VuYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLnJlZC1kYXRlIHtcclxuICBjb2xvcjogI2ViNDE0MTtcclxufVxyXG5cclxuLyogTUFUIFRBQiAqL1xyXG5cclxuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xyXG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAuMTIpO1xyXG59XHJcbiIsIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xuLmMtY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMge1xuICBmbGV4LWdyb3c6IDE7XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTUwcHg7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuY29uZmlnQnV0dG9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZmxleC1zaHJpbms6IDA7XG59XG5cbi5jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xuICBtYXgtaGVpZ2h0OiA4OCU7XG59XG4uY2FyZC1jb21wb25lbnQgLmNhcmQtY29udGVudCwgLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBtYXJnaW46IDA7XG59XG4uY2FyZC1jb21wb25lbnQgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBidXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xuICBib3JkZXItcmFkaXVzOiAycmVtO1xuICBoZWlnaHQ6IDIuM3JlbTtcbiAgY29sb3I6ICNlZmVmZWY7XG59XG5cbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XG4gIGNvbG9yOiAjMTkxOTE5O1xufVxuXG4uZmlsdGVyIHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cblxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XG4gIG1heC13aWR0aDogMTBlbTtcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XG59XG5cbi50YWJsZS1yZXNwb25zaXZlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy14OiBhdXRvO1xufVxuLnRhYmxlLXJlc3BvbnNpdmUgLm1hdC10YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLm1hdC1oZWFkZXItcm93IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcbiAgY29sb3I6ICM4YjhiOGI7XG59XG5cbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG5cbi5pZEhlYWRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pZENlbGwge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxubWF0LWNhcmQtY29udGVudCB7XG4gIG1hcmdpbjogMCAzNHB4O1xufVxuXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xuLnByaW9yaXR5IHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucHJpb3JpdHktbXV5YWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XG59XG5cbi5wcmlvcml0eS1hbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcbn1cblxuLnByaW9yaXR5LW5vcm1hbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XG59XG5cbi5wcmlvcml0eS1iYWphIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcbn1cblxuLnByaW9yaXR5LW5pbmd1bmEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucmVkLWRhdGUge1xuICBjb2xvcjogI2ViNDE0MTtcbn1cblxuLyogTUFUIFRBQiAqL1xuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufVxuXG46Om5nLWRlZXAgLm1hdC1kaWFsb2ctY29udGFpbmVyIHtcbiAgcGFkZGluZzogMDtcbn1cbjo6bmctZGVlcCAubWF0LWRpYWxvZy1jb250ZW50IHtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBtaW4taGVpZ2h0OiA0NzBweDtcbn1cblxuLmJsYWNrLWRpYWxvZyB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICB3aWR0aDogMTY1cHg7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBib3gtc2hhZG93OiAwcHggMTFweCAxNXB4IC03cHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwcHggMjRweCAxOXB4IDNweCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwcHggOXB4IDM2cHggOHB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG59XG4uYmxhY2stZGlhbG9nIGltZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tdG9wOiAyMTBweDtcbn1cblxuLnJlYWRPbmx5SW5wdXQge1xuICBvcGFjaXR5OiAwLjM7XG4gIGN1cnNvcjogbm90LWFsbG93ZWQ7XG59XG5cbi5kaWFsb2cge1xuICB3aWR0aDogODUwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5kZXRhaWwge1xuICBoZWlnaHQ6IDUwMHB4O1xufVxuXG4uZm9ybSB7XG4gIG1pbi1oZWlnaHQ6IDU1MHB4O1xuICBoZWlnaHQ6IDU1MHB4O1xuICBtYXgtaGVpZ2h0OiA1NTBweDtcbn1cblxuLmRpYWxvZy1jb250ZW50IHtcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93LXk6IGF1dG87XG4gIHBhZGRpbmc6IDQwcHggNDBweCAwIDgwcHg7XG59XG4uZGlhbG9nLWNvbnRlbnQgc3BhbiB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5kaWFsb2ctY29udGVudC1kZXRhaWwge1xuICBwYWRkaW5nOiA1NXB4O1xufVxuXG4uZGlhbG9nLWNvbnRlbnQtZm9ybSB7XG4gIHBhZGRpbmc6IDMwcHggNTBweCAzMHB4IDEwMHB4O1xufVxuXG4udGl0bGUge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuXG4ucmVkLWRhdGUge1xuICBjb2xvcjogI2ViNDE0MTtcbiAgZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xufVxuXG4uY29sdW1ucyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG4uY29sdW1ucyBzZWN0aW9uIHtcbiAgd2lkdGg6IDUwJTtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuLmV4aXQtYnV0dG9uIHtcbiAgbWFyZ2luOiAzMHB4IDAgMCA1MDBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbiAgY29sb3I6IHdoaXRlO1xuICB3aWR0aDogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDI0cHg7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLmNpcmNsZS1jb250ZW50IHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW46IDIwcHggNDBweCAwIDUwcHg7XG59XG5cbi5jaXJjbGUtY29udGVudC1vcHBvcnR1bml0eSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luOiBhdXRvIDQwcHggYXV0byBhdXRvO1xufVxuXG4vKiBcbiAgICAtLS0tLS0tLS0tLS0tLVxuICAgIERZTkFNSUMgQ0lSQ0xFXG4gICAgLS0tLS0tLS0tLS0tLS1cbiovXG4uY2lyY2xlIHtcbiAgd2lkdGg6IDMwcHg7XG4gIGhlaWdodDogMzBweDtcbiAgYm9yZGVyOiAwcHggc29saWQgI2MyYzJjMjtcbiAgYm9yZGVyLXJhZGl1czogMTIwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyMDBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogI2RkZDtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHotaW5kZXg6IDE7XG4gIGJveC1zaGFkb3c6IHJnYmEoMTAwLCAxMDAsIDExMSwgMC4yKSAwcHggN3B4IDI5cHggMHB4O1xufVxuXG4uY2lyY2xlOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDg1cHg7XG4gIGhlaWdodDogODVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tdG9vbHRpcC1jb2xvcik7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICBib3JkZXItcmFkaXVzOiA0MCU7XG4gIGFuaW1hdGlvbjogZmlsbCAzcyBlYXNlLWluLW91dDtcbiAgei1pbmRleDogLTE7XG59XG5cbkBrZXlmcmFtZXMgZmlsbCB7XG4gIGZyb20ge1xuICAgIHRvcDogMzBweDtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSkgcm90YXRlKDBkZWcpO1xuICB9XG4gIHRvIHtcbiAgICB0b3A6IC01MHB4O1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoMzYwZGVnKTtcbiAgfVxufVxuLyogXG4gICAgRk9STSBcbiovXG4uYnV0dG9ucyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG5cbi5leGl0LWJ1dHRvbi1mb3JtIHtcbiAgbWFyZ2luOiAzMHB4IDAgMCAzMjVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbiAgY29sb3I6IHdoaXRlO1xuICB3aWR0aDogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDI0cHg7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLnN1Ym1pdC1idXR0b24ge1xuICBtYXJnaW46IDMwcHggMCAwIDI1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlNDZlNzU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWluLXdpZHRoOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuXG4vKiBcbiAgICBDSVJDTEUgQ09MT1IgXG4gICAgICAgICYmXG4gICAgUFJJT1JJVFlcbiovXG4uYWN0aXZpdHktY29sb3IsIC5vcHBvcnR1bml0eS1jb2xvciB7XG4gIHdpZHRoOiAxOHB4O1xuICBoZWlnaHQ6IDE4cHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIG1hcmdpbjogYXV0bztcbn1cblxuLmFjdGl2aXR5LXByaW9yaXR5LCAub3Bwb3J0dW5pdHktcHJpb3JpdHkge1xuICBtYXJnaW46IGF1dG8gMHB4IGF1dG8gYXV0bztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucHJpb3JpdHkge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5wcmlvcml0eS1tdXlhbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcbn1cblxuLnByaW9yaXR5LWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xufVxuXG4ucHJpb3JpdHktbm9ybWFsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcbn1cblxuLnByaW9yaXR5LWJhamEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xufVxuXG4ucHJpb3JpdHktbmluZ3VuYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi8qIFxuICAgIC0tLS0tLVxuICAgIFNDUk9MTFxuICAgIC0tLS0tLVxuKi9cbi5kaWFsb2ctY29udGVudDo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTBweDtcbn1cblxuLmRpYWxvZy1jb250ZW50Ojotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjMmMyYzI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLmNvbHVtbnMgc2VjdGlvbiB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmNvbHVtbnMgc2VjdGlvbiAudzEwMCB7XG4gIHdpZHRoOiAxMDAlO1xufSIsIkBpbXBvcnQgXCIuL3N0YWZmaXQtc3R5bGUtMVwiO1xyXG5cclxuOjpuZy1kZWVwIHtcclxuICAgIC5tYXQtZGlhbG9nLWNvbnRhaW5lciB7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgIH1cclxuICAgIC5tYXQtZGlhbG9nLWNvbnRlbnQge1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgIG1pbi1oZWlnaHQ6IDQ3MHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uYmxhY2stZGlhbG9nIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gICAgd2lkdGg6IDE2NXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDExcHggMTVweCAtN3B4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMHB4IDI0cHggMTlweCAzcHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMHB4IDlweCAzNnB4IDhweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG5cclxuXHJcbiAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIxMHB4O1xyXG4gICAgfVxyXG59XHJcbi5yZWFkT25seUlucHV0e1xyXG4gICAgb3BhY2l0eTogMC4zO1xyXG4gICAgY3Vyc29yOiBub3QtYWxsb3dlZDtcclxufVxyXG5cclxuLmRpYWxvZyB7XHJcbiAgICB3aWR0aDogODUwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxufVxyXG5cclxuLmRldGFpbCB7XHJcbiAgICBoZWlnaHQ6IDUwMHB4O1xyXG59XHJcblxyXG4uZm9ybSB7XHJcbiAgICBtaW4taGVpZ2h0OiA1NTBweDtcclxuICAgIGhlaWdodDogNTUwcHg7XHJcbiAgICBtYXgtaGVpZ2h0OiA1NTBweDtcclxufVxyXG5cclxuLmRpYWxvZy1jb250ZW50IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgb3ZlcmZsb3cteTogYXV0bztcclxuICAgIHBhZGRpbmc6IDQwcHggNDBweCAwIDgwcHg7XHJcbiAgICAvLyBkaXNwbGF5OiBncmlkO1xyXG5cclxuICAgIHNwYW4ge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudC1kZXRhaWwge1xyXG4gICAgLy8gcGFkZGluZzogNDBweCA0MHB4IDAgODBweDtcclxuICAgIHBhZGRpbmc6IDU1cHg7XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudC1mb3JtIHtcclxuICAgIHBhZGRpbmc6IDMwcHggNTBweCAzMHB4IDEwMHB4O1xyXG59XHJcblxyXG4udGl0bGUgeyBcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5yZWQtZGF0ZSB7XHJcbiAgICBjb2xvcjogI2ViNDE0MTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmNvbHVtbnMge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICBzZWN0aW9uIHtcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5leGl0LWJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDMwcHggMCAwIDUwMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHdpZHRoOiAxMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbn1cclxuXHJcbi5jaXJjbGUtY29udGVudHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbjogMjBweCA0MHB4IDAgNTBweDtcclxufVxyXG5cclxuLmNpcmNsZS1jb250ZW50LW9wcG9ydHVuaXR5e1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luOiBhdXRvIDQwcHggYXV0byBhdXRvO1xyXG59XHJcblxyXG4vKiBcclxuICAgIC0tLS0tLS0tLS0tLS0tXHJcbiAgICBEWU5BTUlDIENJUkNMRVxyXG4gICAgLS0tLS0tLS0tLS0tLS1cclxuKi9cclxuXHJcbi5jaXJjbGUge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBib3JkZXI6IDBweCBzb2xpZCAjYzJjMmMyO1xyXG4gICAgYm9yZGVyLXJhZGl1czoxMjBweDtcclxuICAgIGxpbmUtaGVpZ2h0OjIwMHB4O1xyXG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICBjb2xvcjojZGRkO1xyXG4gICAgZm9udC1zaXplOjI1cHg7XHJcbiAgICBmb250LXdlaWdodDo2MDA7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBvdmVyZmxvdzpoaWRkZW47XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgYm94LXNoYWRvdzogcmdiYSgxMDAsIDEwMCwgMTExLCAwLjIpIDBweCA3cHggMjlweCAwcHg7XHJcbn1cclxuXHJcbi5jaXJjbGU6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6XCJcIjtcclxuICAgIHBvc2l0aW9uOmFic29sdXRlO1xyXG4gICAgd2lkdGg6ODVweDtcclxuICAgIGhlaWdodDo4NXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tdG9vbHRpcC1jb2xvcik7XHJcbiAgICBsZWZ0OjUwJTtcclxuICAgIHRyYW5zZm9ybTp0cmFuc2xhdGVYKC01MCUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czo0MCU7XHJcbiAgICBhbmltYXRpb246ZmlsbCAzcyBlYXNlLWluLW91dDtcclxuICAgIHotaW5kZXg6LTE7XHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgZmlsbCB7XHJcbiAgICBmcm9tIHtcclxuICAgICAgICB0b3A6MzBweDtcclxuICAgICAgICB0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoMGRlZyk7XHJcbiAgICB9XHJcbiAgICB0byB7XHJcbiAgICAgICAgdG9wOi01MHB4O1xyXG4gICAgICAgIHRyYW5zZm9ybTp0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgzNjBkZWcpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKiBcclxuICAgIEZPUk0gXHJcbiovXHJcblxyXG4uYnV0dG9ucyB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxufVxyXG5cclxuLmV4aXQtYnV0dG9uLWZvcm0ge1xyXG4gICAgbWFyZ2luOiAzMHB4IDAgMCAzMjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG4uc3VibWl0LWJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDMwcHggMCAwIDI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTQ2ZTc1O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgbWluLXdpZHRoOiAxMDBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbn1cclxuXHJcbi8qIFxyXG4gICAgQ0lSQ0xFIENPTE9SIFxyXG4gICAgICAgICYmXHJcbiAgICBQUklPUklUWVxyXG4qL1xyXG5cclxuLmFjdGl2aXR5LWNvbG9yLCAub3Bwb3J0dW5pdHktY29sb3Ige1xyXG4gICAgd2lkdGg6IDE4cHg7XHJcbiAgICBoZWlnaHQ6IDE4cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG4uYWN0aXZpdHktcHJpb3JpdHksIC5vcHBvcnR1bml0eS1wcmlvcml0eSB7XHJcbiAgICBtYXJnaW46IGF1dG8gMHB4IGF1dG8gYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnByaW9yaXR5IHtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZzogNXB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7IGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTUgfVxyXG4ucHJpb3JpdHktYWx0YSB7IGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzEgfVxyXG4ucHJpb3JpdHktbm9ybWFsIHsgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZSB9XHJcbi5wcmlvcml0eS1iYWphIHsgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZCB9XHJcbi5wcmlvcml0eS1uaW5ndW5hIHsgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMiB9XHJcblxyXG4vKiBcclxuICAgIC0tLS0tLVxyXG4gICAgU0NST0xMXHJcbiAgICAtLS0tLS1cclxuKi9cclxuXHJcbi5kaWFsb2ctY29udGVudDo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gICAgd2lkdGg6IDEwcHg7XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudDo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gICAgYmFja2dyb3VuZDogI2MyYzJjMjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxufSIsIkBpbXBvcnQgJy4uLy4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvZGlhbG9nLXVuaWZpZWQnO1xyXG5cclxuLmNvbHVtbnMge1xyXG4gICAgc2VjdGlvbiB7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgLncxMDAge3dpZHRoOiAxMDAlOyB9XHJcbiAgICB9XHJcbn1cclxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](SendExamComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-send-exam',
          templateUrl: './send-exam.component.html',
          styleUrls: ['./send-exam.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"]
        }, {
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
            args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"]]
          }]
        }, {
          type: _services_exam_service__WEBPACK_IMPORTED_MODULE_8__["ExamService"]
        }, {
          type: _recruitment_candidate_infrastructure_services_candidate_service__WEBPACK_IMPORTED_MODULE_9__["CandidateService"]
        }, {
          type: _recruitment_candidate_infrastructure_CandidateMapper__WEBPACK_IMPORTED_MODULE_10__["CandidateMapper"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/ng-components/sent-exam-detail/sent-exam-detail.component.ts":
  /*!**************************************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/ng-components/sent-exam-detail/sent-exam-detail.component.ts ***!
    \**************************************************************************************************************/

  /*! exports provided: SentExamDetailComponent */

  /***/
  function srcAppEvaluationsExamInfrastructureNgComponentsSentExamDetailSentExamDetailComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SentExamDetailComponent", function () {
      return SentExamDetailComponent;
    });
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _counter_infrastructure_services_counter_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../counter/infrastructure/services/counter.service */
    "./src/app/evaluations/counter/infrastructure/services/counter.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_divider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material/divider */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/divider.js");

    function SentExamDetailComponent_ng_container_10_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
      }

      if (rf & 2) {
        var result_r2 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"]("", result_r2.counter.name, ": ", result_r2.result, " ");
      }
    }

    function SentExamDetailComponent_ng_container_12_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Question: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "span", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "span", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, "Answer: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](8, "span", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "mat-divider");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
      }

      if (rf & 2) {
        var nonComputed_r3 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", nonComputed_r3.questionDescription, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("innerHTML", nonComputed_r3.responseText, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeHtml"]);
      }
    }

    var SentExamDetailComponent = /*#__PURE__*/function () {
      function SentExamDetailComponent(data, _service) {
        _classCallCheck(this, SentExamDetailComponent);

        this.data = data;
        this._service = _service;
      }

      _createClass(SentExamDetailComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this16 = this;

          this._service.getCounters(this.data.id).subscribe(function (res) {
            _this16.counterResults = res.results;
            _this16.nonComputedResponses = res.nonComputedResponses;
          });
        }
      }]);

      return SentExamDetailComponent;
    }();

    SentExamDetailComponent.ɵfac = function SentExamDetailComponent_Factory(t) {
      return new (t || SentExamDetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__["MAT_DIALOG_DATA"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_counter_infrastructure_services_counter_service__WEBPACK_IMPORTED_MODULE_2__["CounterService"]));
    };

    SentExamDetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: SentExamDetailComponent,
      selectors: [["app-sent-exam-detail"]],
      decls: 15,
      vars: 5,
      consts: [[1, "dialog", "detail"], [1, "black-dialog"], ["src", "../../../../../../assets/CRM/icons/dialogs/exam.png", "alt", ""], [1, "dialog-content", "dialog-content-detail"], [1, "title"], [1, "results"], [1, "left"], [4, "ngFor", "ngForOf"], [1, "right"], ["mat-raised-button", "", "mat-dialog-close", "", 1, "exit-button"], [3, "innerHTML"], [1, "answer"]],
      template: function SentExamDetailComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "mat-dialog-content", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "img", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](7, "titlecase");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, SentExamDetailComponent_ng_container_10_Template, 3, 2, "ng-container", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, SentExamDetailComponent_ng_container_12_Template, 10, 2, "ng-container", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, "Exit");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind1"](7, 3, ctx.data.examName));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.counterResults);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.nonComputedResponses);
        }
      },
      directives: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__["MatDialogContent"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_4__["MatButton"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__["MatDialogClose"], _angular_material_divider__WEBPACK_IMPORTED_MODULE_5__["MatDivider"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["TitleCasePipe"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n  .mat-dialog-container {\n  padding: 0;\n}\n  .mat-dialog-content {\n  margin: 0;\n  padding: 0;\n  min-height: 470px;\n}\n.black-dialog[_ngcontent-%COMP%] {\n  background-color: black;\n  width: 165px;\n  padding-left: 15px;\n  border-radius: 5px;\n  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2), 0px 24px 19px 3px rgba(0, 0, 0, 0.14), 0px 9px 36px 8px rgba(0, 0, 0, 0.12);\n}\n.black-dialog[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  margin-top: 210px;\n}\n.readOnlyInput[_ngcontent-%COMP%] {\n  opacity: 0.3;\n  cursor: not-allowed;\n}\n.dialog[_ngcontent-%COMP%] {\n  width: 850px;\n  display: flex;\n}\n.detail[_ngcontent-%COMP%] {\n  height: 500px;\n}\n.form[_ngcontent-%COMP%] {\n  min-height: 550px;\n  height: 550px;\n  max-height: 550px;\n}\n.dialog-content[_ngcontent-%COMP%] {\n  width: 100%;\n  overflow-y: auto;\n  padding: 40px 40px 0 80px;\n}\n.dialog-content[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.dialog-content-detail[_ngcontent-%COMP%] {\n  padding: 55px;\n}\n.dialog-content-form[_ngcontent-%COMP%] {\n  padding: 30px 50px 30px 100px;\n}\n.title[_ngcontent-%COMP%] {\n  display: flex;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n  font-weight: 400 !important;\n}\n.columns[_ngcontent-%COMP%] {\n  display: flex;\n  margin-top: 15px;\n}\n.columns[_ngcontent-%COMP%]   section[_ngcontent-%COMP%] {\n  width: 50%;\n  padding-right: 10px;\n}\n.exit-button[_ngcontent-%COMP%] {\n  margin: 30px 0 0 500px;\n  background-color: #c2c2c2;\n  color: white;\n  width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n.circle-content[_ngcontent-%COMP%] {\n  position: relative;\n  margin: 20px 40px 0 50px;\n}\n.circle-content-opportunity[_ngcontent-%COMP%] {\n  position: relative;\n  margin: auto 40px auto auto;\n}\n\n.circle[_ngcontent-%COMP%] {\n  width: 30px;\n  height: 30px;\n  border: 0px solid #c2c2c2;\n  border-radius: 120px;\n  line-height: 200px;\n  text-align: center;\n  color: #ddd;\n  font-size: 25px;\n  font-weight: 600;\n  position: absolute;\n  overflow: hidden;\n  z-index: 1;\n  box-shadow: rgba(100, 100, 111, 0.2) 0px 7px 29px 0px;\n}\n.circle[_ngcontent-%COMP%]:before {\n  content: \"\";\n  position: absolute;\n  width: 85px;\n  height: 85px;\n  background-color: var(--tooltip-color);\n  left: 50%;\n  transform: translateX(-50%);\n  border-radius: 40%;\n  -webkit-animation: fill 3s ease-in-out;\n          animation: fill 3s ease-in-out;\n  z-index: -1;\n}\n@-webkit-keyframes fill {\n  from {\n    top: 30px;\n    transform: translateX(-50%) rotate(0deg);\n  }\n  to {\n    top: -50px;\n    transform: translateX(-50%) rotate(360deg);\n  }\n}\n@keyframes fill {\n  from {\n    top: 30px;\n    transform: translateX(-50%) rotate(0deg);\n  }\n  to {\n    top: -50px;\n    transform: translateX(-50%) rotate(360deg);\n  }\n}\n\n.buttons[_ngcontent-%COMP%] {\n  display: flex;\n  margin-bottom: 30px;\n}\n.exit-button-form[_ngcontent-%COMP%] {\n  margin: 30px 0 0 325px;\n  background-color: #c2c2c2;\n  color: white;\n  width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n.submit-button[_ngcontent-%COMP%] {\n  margin: 30px 0 0 25px;\n  background-color: #e46e75;\n  color: white;\n  min-width: 100px;\n  border-radius: 24px;\n  height: 40px;\n}\n\n.activity-color[_ngcontent-%COMP%], .opportunity-color[_ngcontent-%COMP%] {\n  width: 18px;\n  height: 18px;\n  border-radius: 15px;\n  margin: auto;\n}\n.activity-priority[_ngcontent-%COMP%], .opportunity-priority[_ngcontent-%COMP%] {\n  margin: auto 0px auto auto;\n  text-align: center;\n}\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n\n.dialog-content[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 10px;\n}\n.dialog-content[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #c2c2c2;\n  border-radius: 4px;\n}\n.padding[_ngcontent-%COMP%] {\n  padding-left: 5px;\n}\n.inline-block[_ngcontent-%COMP%] {\n  display: inline-block;\n}\n.answer[_ngcontent-%COMP%] {\n  color: #de7373;\n}\n.results[_ngcontent-%COMP%] {\n  display: flex;\n}\n.results[_ngcontent-%COMP%]   .left[_ngcontent-%COMP%] {\n  width: 30%;\n  flex-direction: column;\n}\n.results[_ngcontent-%COMP%]   .right[_ngcontent-%COMP%] {\n  width: 70%;\n  flex-direction: column;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3NlbnQtZXhhbS1kZXRhaWwvQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFzc2V0c1xcc3R5bGVzXFxzdGFmZml0LXN0eWxlLTEuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3NlbnQtZXhhbS1kZXRhaWwvc2VudC1leGFtLWRldGFpbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3NlbnQtZXhhbS1kZXRhaWwvQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFzc2V0c1xcc3R5bGVzXFxkaWFsb2ctdW5pZmllZC5zY3NzIiwic3JjL2FwcC9ldmFsdWF0aW9ucy9leGFtL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvc2VudC1leGFtLWRldGFpbC9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXBwXFxldmFsdWF0aW9uc1xcZXhhbVxcaW5mcmFzdHJ1Y3R1cmVcXG5nLWNvbXBvbmVudHNcXHNlbnQtZXhhbS1kZXRhaWxcXHNlbnQtZXhhbS1kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0NBQUE7QUFRQSx3QkFBQTtBQVFBLHVCQUFBO0FBR0EsMkJBQUE7QUFFQTtFQUNFLDZCQUFBO0FDaEJGO0FEa0JFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUNoQko7QURrQkk7RUFDRSxtQkEzQlE7RUE0QlIsZ0JBQUE7RUFDQSxjQUFBO0FDaEJOO0FEc0JBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDbkJGO0FEcUJFO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNuQko7QURxQkk7RUFDRSxZQUFBO0FDbkJOO0FEc0JFO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0FDcEJKO0FEd0JBO0VBQ0UsNkJBQUE7RUFDQSxlQUFBO0FDckJGO0FEdUJFO0VBQ0UsU0FBQTtBQ3JCSjtBRHdCRTtFQUNFLGdDQUFBO0FDdEJKO0FEeUJFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUN2Qko7QUR5Qkk7RUFDRSxtQkEzRVE7RUE0RVIsZ0JBQUE7RUFDQSxjQUFBO0FDdkJOO0FEMEJJO0VBQ0UseUJBNUVpQjtFQTZFakIsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ3hCTjtBRDZCQTtFQUNFLGNBbEZtQjtBQ3dEckI7QUQ2QkE7RUFDRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxXQUFBO0FDMUJGO0FENkJBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDMUJGO0FENkJBO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQzFCRjtBRDRCRTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQzFCSjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZUFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0FDM0JGO0FEa0NBO0VBQ0UsY0FBQTtBQy9CRjtBRGtDQSx5QkFBQTtBQUVBO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtREFBQTtFQUNBLGdCQUFBO0VBQ0EsMEJBQUE7RUFDQSx5QkFBQTtBQ2hDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSxjQUFBO0FDbENGO0FEcUNBLFlBQUE7QUFFQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsdUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxlQUFBO0FDbkNGO0FEc0NBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSwwQ0FBQTtBQ25DRjtBQ2pNSTtFQUNJLFVBQUE7QURvTVI7QUNsTUk7RUFDSSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0FEb01SO0FDaE1BO0VBQ0ksdUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLDhIQUFBO0FEbU1KO0FDaE1JO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0FEa01SO0FDL0xBO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0FEa01KO0FDL0xBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7QURrTUo7QUM5TEE7RUFDSSxhQUFBO0FEaU1KO0FDOUxBO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7QURpTUo7QUM5TEE7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtBRGlNSjtBQzlMSTtFQUNJLGdCQUFBO0FEZ01SO0FDNUxBO0VBRUksYUFBQTtBRDhMSjtBQzNMQTtFQUNJLDZCQUFBO0FEOExKO0FDM0xBO0VBQ0ksYUFBQTtBRDhMSjtBQzNMQTtFQUNJLGNBQUE7RUFDQSwyQkFBQTtBRDhMSjtBQzNMQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtBRDhMSjtBQzdMSTtFQUNJLFVBQUE7RUFDQSxtQkFBQTtBRCtMUjtBQzNMQTtFQUNJLHNCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBRDhMSjtBQzNMQTtFQUNJLGtCQUFBO0VBQ0Esd0JBQUE7QUQ4TEo7QUMzTEE7RUFDSSxrQkFBQTtFQUNBLDJCQUFBO0FEOExKO0FDM0xBOzs7O0NBQUE7QUFNQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxxREFBQTtBRDZMSjtBQzFMQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esc0NBQUE7RUFDQSxTQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtFQUNBLHNDQUFBO1VBQUEsOEJBQUE7RUFDQSxXQUFBO0FENkxKO0FDMUxBO0VBQ0k7SUFDSSxTQUFBO0lBQ0Esd0NBQUE7RUQ2TE47RUMzTEU7SUFDSSxVQUFBO0lBQ0EsMENBQUE7RUQ2TE47QUFDRjtBQ3JNQTtFQUNJO0lBQ0ksU0FBQTtJQUNBLHdDQUFBO0VENkxOO0VDM0xFO0lBQ0ksVUFBQTtJQUNBLDBDQUFBO0VENkxOO0FBQ0Y7QUMxTEE7O0NBQUE7QUFJQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtBRDJMSjtBQ3hMQTtFQUNJLHNCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBRDJMSjtBQ3hMQTtFQUNJLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUQyTEo7QUN4TEE7Ozs7Q0FBQTtBQU1BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUQwTEo7QUN2TEE7RUFDSSwwQkFBQTtFQUNBLGtCQUFBO0FEMExKO0FDdkxBO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtREFBQTtFQUNBLGdCQUFBO0VBQ0EsMEJBQUE7QUQwTEo7QUN2TEE7RUFBb0IseUJBQUE7QUQyTHBCO0FDMUxBO0VBQWlCLHlCQUFBO0FEOExqQjtBQzdMQTtFQUFtQix5QkFBQTtBRGlNbkI7QUNoTUE7RUFBaUIseUJBQUE7QURvTWpCO0FDbk1BO0VBQW9CLHlCQUFBO0FEdU1wQjtBQ3JNQTs7OztDQUFBO0FBTUE7RUFDSSxXQUFBO0FEdU1KO0FDcE1BO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtBRHVNSjtBRW5hQTtFQUNFLGlCQUFBO0FGc2FGO0FFcGFBO0VBQ0UscUJBQUE7QUZ1YUY7QUVwYUE7RUFDRSxjQUFBO0FGdWFGO0FFcGFBO0VBQ0UsYUFBQTtBRnVhRjtBRXJhRTtFQUNFLFVBQUE7RUFDQSxzQkFBQTtBRnVhSjtBRXJhRTtFQUNFLFVBQUE7RUFDQSxzQkFBQTtBRnVhSiIsImZpbGUiOiJzcmMvYXBwL2V2YWx1YXRpb25zL2V4YW0vaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9zZW50LWV4YW0tZGV0YWlsL3NlbnQtZXhhbS1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiA9PT09PSBGT05UUyAoTWFqb3IgVGhpcmQpID09PT09ICovXHJcbiRzbWFsbDogMC44cmVtO1xyXG4kbWVkaXVtOiAxcmVtO1xyXG4kbGFyZ2U6IDEuMjVyZW07XHJcbiRleHRyYS1sYXJnZTogMS41NjNyZW07XHJcbiRodWdlOiAxLjk1M3JlbTtcclxuJGV4dHJhLWh1Z2U6IDIuNDQxcmVtO1xyXG5cclxuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xyXG4kY29sb3ItcGFsZXR0ZS0xLW1haW46ICNERTczNzM7XHJcbiRjb2xvci1wYWxldHRlLTEtc2Vjb25kOiAjQUJBQkZGO1xyXG4kY29sb3ItcHJpbWFyeS1saWdodDogI0VGRUZFRjtcclxuJGNvbG9yLXByaW1hcnktZGFyazogIzE5MTkxOTtcclxuJGNvbG9yLXNlY29uZGFyeS1kYXJrOiAjMzMzMzMzO1xyXG4kY29sb3Itc2Nyb2xsYmFyLXRodW1iOiAjOEM4QzhDO1xyXG5cclxuLyogPT09PT0gV0VJR0hUID09PT09ICovXHJcblxyXG5cclxuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xyXG5cclxuLmMtY2FyZC1jb21wb25lbnQge1xyXG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5cclxuLmZpbHRlcnMtcGx1cy1jb25maWcge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1mbG93OiByb3c7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gIC5keW4tZmlsdGVycyB7XHJcbiAgICBmbGV4LWdyb3c6IDE7XHJcbiAgICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gICAgbWFyZ2luOiAwIDQwcHggMCAwO1xyXG5cclxuICAgIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XHJcbiAgICAgIHdpZHRoOiAxNTBweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmNvbmZpZ0J1dHRvbiB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgfVxyXG59XHJcblxyXG4uY2FyZC1jb21wb25lbnQge1xyXG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xyXG4gIG1heC1oZWlnaHQ6IDg4JTtcclxuXHJcbiAgLmNhcmQtY29udGVudCwgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgfVxyXG5cclxuICA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24ge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItcGFsZXR0ZS0xLW1haW47XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJyZW07XHJcbiAgICAgIGhlaWdodDogMi4zcmVtO1xyXG4gICAgICBjb2xvcjogI2VmZWZlZjtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XHJcbiAgY29sb3I6ICRjb2xvci1wcmltYXJ5LWRhcms7XHJcbn1cclxuXHJcbi5maWx0ZXIge1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbm1hdC1mb3JtLWZpZWxkIG1hdC1zZWxlY3Qge1xyXG4gIG1heC13aWR0aDogMTBlbTtcclxuICBtYXJnaW4tcmlnaHQ6IDVlbTtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmUge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgLy8gbWF4LWhlaWdodDogODAwcHg7XHJcbiAgLm1hdC10YWJsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IHRhYmxlO1xyXG4gIH1cclxufVxyXG5cclxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1yb3cge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcclxuICBjb2xvcjogIzhiOGI4YjtcclxufVxyXG5cclxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XHJcbiAgcGFkZGluZy1sZWZ0OiAwO1xyXG59XHJcblxyXG4uaWRIZWFkZXIge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmlkQ2VsbCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5tYXQtY2FyZC1jb250ZW50IHtcclxuICBtYXJnaW46IDAgMzRweDtcclxufVxyXG5cclxuLyogPT09PT0gUFJJT1JJVFkgPT09PT0gKi9cclxuXHJcbi5wcmlvcml0eSB7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgcGFkZGluZzogNXB4IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4vLyBAVE9ETzogY2hhbmdlIGNvbG9yIHRvIHZhcmlhYmxlc1xyXG5cclxuLnByaW9yaXR5LW11eWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1hbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbm9ybWFsIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xyXG59XHJcblxyXG4ucHJpb3JpdHktYmFqYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcclxufVxyXG5cclxuLnByaW9yaXR5LW5pbmd1bmEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi5yZWQtZGF0ZSB7XHJcbiAgY29sb3I6ICNlYjQxNDE7XHJcbn1cclxuXHJcbi8qIE1BVCBUQUIgKi9cclxuXHJcbjo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIHtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHNwYW4ge1xyXG4gIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcclxuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgLjEyKTtcclxufVxyXG4iLCIvKiA9PT09PSBGT05UUyAoTWFqb3IgVGhpcmQpID09PT09ICovXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cbi5jLWNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cblxuLmZpbHRlcnMtcGx1cy1jb25maWcge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIHtcbiAgZmxleC1ncm93OiAxO1xuICBvdmVyZmxvdy14OiBhdXRvO1xuICBtYXJnaW46IDAgNDBweCAwIDA7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDE1MHB4O1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmNvbmZpZ0J1dHRvbiB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGZsZXgtc2hyaW5rOiAwO1xufVxuXG4uY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbiAgbWF4LWhlaWdodDogODglO1xufVxuLmNhcmQtY29tcG9uZW50IC5jYXJkLWNvbnRlbnQsIC5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAwO1xufVxuLmNhcmQtY29tcG9uZW50IDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RFNzM3MztcbiAgYm9yZGVyLXJhZGl1czogMnJlbTtcbiAgaGVpZ2h0OiAyLjNyZW07XG4gIGNvbG9yOiAjZWZlZmVmO1xufVxuXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xuICBjb2xvcjogIzE5MTkxOTtcbn1cblxuLmZpbHRlciB7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbm1hdC1mb3JtLWZpZWxkIG1hdC1zZWxlY3Qge1xuICBtYXgtd2lkdGg6IDEwZW07XG4gIG1hcmdpbi1yaWdodDogNWVtO1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbn1cbi50YWJsZS1yZXNwb25zaXZlIC5tYXQtdGFibGUge1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiB0YWJsZTtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XG59XG5cbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XG59XG5cbi5tYXQtaGVhZGVyLXJvdyB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XG4gIGNvbG9yOiAjOGI4YjhiO1xufVxuXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcbiAgcGFkZGluZy1sZWZ0OiAwO1xufVxuXG4uaWRIZWFkZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uaWRDZWxsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbm1hdC1jYXJkLWNvbnRlbnQge1xuICBtYXJnaW46IDAgMzRweDtcbn1cblxuLyogPT09PT0gUFJJT1JJVFkgPT09PT0gKi9cbi5wcmlvcml0eSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA1cHggMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLnByaW9yaXR5LW11eWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xufVxuXG4ucHJpb3JpdHktYWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XG59XG5cbi5wcmlvcml0eS1ub3JtYWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4ucHJpb3JpdHktYmFqYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XG59XG5cbi5wcmlvcml0eS1uaW5ndW5hIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLnJlZC1kYXRlIHtcbiAgY29sb3I6ICNlYjQxNDE7XG59XG5cbi8qIE1BVCBUQUIgKi9cbjo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIHtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xuICBmb250LXNpemU6IDEzcHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHNwYW4ge1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn1cblxuOjpuZy1kZWVwIC5tYXQtZGlhbG9nLWNvbnRhaW5lciB7XG4gIHBhZGRpbmc6IDA7XG59XG46Om5nLWRlZXAgLm1hdC1kaWFsb2ctY29udGVudCB7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgbWluLWhlaWdodDogNDcwcHg7XG59XG5cbi5ibGFjay1kaWFsb2cge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgd2lkdGg6IDE2NXB4O1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYm94LXNoYWRvdzogMHB4IDExcHggMTVweCAtN3B4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMHB4IDI0cHggMTlweCAzcHggcmdiYSgwLCAwLCAwLCAwLjE0KSwgMHB4IDlweCAzNnB4IDhweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufVxuLmJsYWNrLWRpYWxvZyBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogMjEwcHg7XG59XG5cbi5yZWFkT25seUlucHV0IHtcbiAgb3BhY2l0eTogMC4zO1xuICBjdXJzb3I6IG5vdC1hbGxvd2VkO1xufVxuXG4uZGlhbG9nIHtcbiAgd2lkdGg6IDg1MHB4O1xuICBkaXNwbGF5OiBmbGV4O1xufVxuXG4uZGV0YWlsIHtcbiAgaGVpZ2h0OiA1MDBweDtcbn1cblxuLmZvcm0ge1xuICBtaW4taGVpZ2h0OiA1NTBweDtcbiAgaGVpZ2h0OiA1NTBweDtcbiAgbWF4LWhlaWdodDogNTUwcHg7XG59XG5cbi5kaWFsb2ctY29udGVudCB7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy15OiBhdXRvO1xuICBwYWRkaW5nOiA0MHB4IDQwcHggMCA4MHB4O1xufVxuLmRpYWxvZy1jb250ZW50IHNwYW4ge1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uZGlhbG9nLWNvbnRlbnQtZGV0YWlsIHtcbiAgcGFkZGluZzogNTVweDtcbn1cblxuLmRpYWxvZy1jb250ZW50LWZvcm0ge1xuICBwYWRkaW5nOiAzMHB4IDUwcHggMzBweCAxMDBweDtcbn1cblxuLnRpdGxlIHtcbiAgZGlzcGxheTogZmxleDtcbn1cblxuLnJlZC1kYXRlIHtcbiAgY29sb3I6ICNlYjQxNDE7XG4gIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbn1cblxuLmNvbHVtbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuLmNvbHVtbnMgc2VjdGlvbiB7XG4gIHdpZHRoOiA1MCU7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG59XG5cbi5leGl0LWJ1dHRvbiB7XG4gIG1hcmdpbjogMzBweCAwIDAgNTAwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICBoZWlnaHQ6IDQwcHg7XG59XG5cbi5jaXJjbGUtY29udGVudCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luOiAyMHB4IDQwcHggMCA1MHB4O1xufVxuXG4uY2lyY2xlLWNvbnRlbnQtb3Bwb3J0dW5pdHkge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbjogYXV0byA0MHB4IGF1dG8gYXV0bztcbn1cblxuLyogXG4gICAgLS0tLS0tLS0tLS0tLS1cbiAgICBEWU5BTUlDIENJUkNMRVxuICAgIC0tLS0tLS0tLS0tLS0tXG4qL1xuLmNpcmNsZSB7XG4gIHdpZHRoOiAzMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIGJvcmRlcjogMHB4IHNvbGlkICNjMmMyYzI7XG4gIGJvcmRlci1yYWRpdXM6IDEyMHB4O1xuICBsaW5lLWhlaWdodDogMjAwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICNkZGQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB6LWluZGV4OiAxO1xuICBib3gtc2hhZG93OiByZ2JhKDEwMCwgMTAwLCAxMTEsIDAuMikgMHB4IDdweCAyOXB4IDBweDtcbn1cblxuLmNpcmNsZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiA4NXB4O1xuICBoZWlnaHQ6IDg1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXRvb2x0aXAtY29sb3IpO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbiAgYm9yZGVyLXJhZGl1czogNDAlO1xuICBhbmltYXRpb246IGZpbGwgM3MgZWFzZS1pbi1vdXQ7XG4gIHotaW5kZXg6IC0xO1xufVxuXG5Aa2V5ZnJhbWVzIGZpbGwge1xuICBmcm9tIHtcbiAgICB0b3A6IDMwcHg7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpIHJvdGF0ZSgwZGVnKTtcbiAgfVxuICB0byB7XG4gICAgdG9wOiAtNTBweDtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSkgcm90YXRlKDM2MGRlZyk7XG4gIH1cbn1cbi8qIFxuICAgIEZPUk0gXG4qL1xuLmJ1dHRvbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuXG4uZXhpdC1idXR0b24tZm9ybSB7XG4gIG1hcmdpbjogMzBweCAwIDAgMzI1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICBoZWlnaHQ6IDQwcHg7XG59XG5cbi5zdWJtaXQtYnV0dG9uIHtcbiAgbWFyZ2luOiAzMHB4IDAgMCAyNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTQ2ZTc1O1xuICBjb2xvcjogd2hpdGU7XG4gIG1pbi13aWR0aDogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDI0cHg7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLyogXG4gICAgQ0lSQ0xFIENPTE9SIFxuICAgICAgICAmJlxuICAgIFBSSU9SSVRZXG4qL1xuLmFjdGl2aXR5LWNvbG9yLCAub3Bwb3J0dW5pdHktY29sb3Ige1xuICB3aWR0aDogMThweDtcbiAgaGVpZ2h0OiAxOHB4O1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5hY3Rpdml0eS1wcmlvcml0eSwgLm9wcG9ydHVuaXR5LXByaW9yaXR5IHtcbiAgbWFyZ2luOiBhdXRvIDBweCBhdXRvIGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnByaW9yaXR5IHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4ucHJpb3JpdHktbXV5YWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XG59XG5cbi5wcmlvcml0eS1hbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcbn1cblxuLnByaW9yaXR5LW5vcm1hbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XG59XG5cbi5wcmlvcml0eS1iYWphIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcbn1cblxuLnByaW9yaXR5LW5pbmd1bmEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4vKiBcbiAgICAtLS0tLS1cbiAgICBTQ1JPTExcbiAgICAtLS0tLS1cbiovXG4uZGlhbG9nLWNvbnRlbnQ6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEwcHg7XG59XG5cbi5kaWFsb2ctY29udGVudDo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjYzJjMmMyO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbi5wYWRkaW5nIHtcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG5cbi5pbmxpbmUtYmxvY2sge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5hbnN3ZXIge1xuICBjb2xvcjogI2RlNzM3Mztcbn1cblxuLnJlc3VsdHMge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnJlc3VsdHMgLmxlZnQge1xuICB3aWR0aDogMzAlO1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuLnJlc3VsdHMgLnJpZ2h0IHtcbiAgd2lkdGg6IDcwJTtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn0iLCJAaW1wb3J0IFwiLi9zdGFmZml0LXN0eWxlLTFcIjtcclxuXHJcbjo6bmctZGVlcCB7XHJcbiAgICAubWF0LWRpYWxvZy1jb250YWluZXIge1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICB9XHJcbiAgICAubWF0LWRpYWxvZy1jb250ZW50IHtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICBtaW4taGVpZ2h0OiA0NzBweDtcclxuICAgIH1cclxufVxyXG5cclxuLmJsYWNrLWRpYWxvZyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICAgIHdpZHRoOiAxNjVweDtcclxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGJveC1zaGFkb3c6IDBweCAxMXB4IDE1cHggLTdweCByZ2JhKDAsIDAsIDAsIDAuMiksIDBweCAyNHB4IDE5cHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xNCksIDBweCA5cHggMzZweCA4cHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcclxuXHJcblxyXG4gICAgaW1nIHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMTBweDtcclxuICAgIH1cclxufVxyXG4ucmVhZE9ubHlJbnB1dHtcclxuICAgIG9wYWNpdHk6IDAuMztcclxuICAgIGN1cnNvcjogbm90LWFsbG93ZWQ7XHJcbn1cclxuXHJcbi5kaWFsb2cge1xyXG4gICAgd2lkdGg6IDg1MHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuXHJcbn1cclxuXHJcbi5kZXRhaWwge1xyXG4gICAgaGVpZ2h0OiA1MDBweDtcclxufVxyXG5cclxuLmZvcm0ge1xyXG4gICAgbWluLWhlaWdodDogNTUwcHg7XHJcbiAgICBoZWlnaHQ6IDU1MHB4O1xyXG4gICAgbWF4LWhlaWdodDogNTUwcHg7XHJcbn1cclxuXHJcbi5kaWFsb2ctY29udGVudCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgICBwYWRkaW5nOiA0MHB4IDQwcHggMCA4MHB4O1xyXG4gICAgLy8gZGlzcGxheTogZ3JpZDtcclxuXHJcbiAgICBzcGFuIHtcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgfVxyXG59XHJcblxyXG4uZGlhbG9nLWNvbnRlbnQtZGV0YWlsIHtcclxuICAgIC8vIHBhZGRpbmc6IDQwcHggNDBweCAwIDgwcHg7XHJcbiAgICBwYWRkaW5nOiA1NXB4O1xyXG59XHJcblxyXG4uZGlhbG9nLWNvbnRlbnQtZm9ybSB7XHJcbiAgICBwYWRkaW5nOiAzMHB4IDUwcHggMzBweCAxMDBweDtcclxufVxyXG5cclxuLnRpdGxlIHsgXHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4ucmVkLWRhdGUge1xyXG4gICAgY29sb3I6ICNlYjQxNDE7XHJcbiAgICBmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jb2x1bW5zIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgc2VjdGlvbiB7XHJcbiAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uZXhpdC1idXR0b24ge1xyXG4gICAgbWFyZ2luOiAzMHB4IDAgMCA1MDBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB3aWR0aDogMTAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG4uY2lyY2xlLWNvbnRlbnR7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW46IDIwcHggNDBweCAwIDUwcHg7XHJcbn1cclxuXHJcbi5jaXJjbGUtY29udGVudC1vcHBvcnR1bml0eXtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbjogYXV0byA0MHB4IGF1dG8gYXV0bztcclxufVxyXG5cclxuLyogXHJcbiAgICAtLS0tLS0tLS0tLS0tLVxyXG4gICAgRFlOQU1JQyBDSVJDTEVcclxuICAgIC0tLS0tLS0tLS0tLS0tXHJcbiovXHJcblxyXG4uY2lyY2xlIHtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgYm9yZGVyOiAwcHggc29saWQgI2MyYzJjMjtcclxuICAgIGJvcmRlci1yYWRpdXM6MTIwcHg7XHJcbiAgICBsaW5lLWhlaWdodDoyMDBweDtcclxuICAgIHRleHQtYWxpZ246Y2VudGVyO1xyXG4gICAgY29sb3I6I2RkZDtcclxuICAgIGZvbnQtc2l6ZToyNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6NjAwO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgb3ZlcmZsb3c6aGlkZGVuO1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGJveC1zaGFkb3c6IHJnYmEoMTAwLCAxMDAsIDExMSwgMC4yKSAwcHggN3B4IDI5cHggMHB4O1xyXG59XHJcblxyXG4uY2lyY2xlOmJlZm9yZSB7XHJcbiAgICBjb250ZW50OlwiXCI7XHJcbiAgICBwb3NpdGlvbjphYnNvbHV0ZTtcclxuICAgIHdpZHRoOjg1cHg7XHJcbiAgICBoZWlnaHQ6ODVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXRvb2x0aXAtY29sb3IpO1xyXG4gICAgbGVmdDo1MCU7XHJcbiAgICB0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNTAlKTtcclxuICAgIGJvcmRlci1yYWRpdXM6NDAlO1xyXG4gICAgYW5pbWF0aW9uOmZpbGwgM3MgZWFzZS1pbi1vdXQ7XHJcbiAgICB6LWluZGV4Oi0xO1xyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIGZpbGwge1xyXG4gICAgZnJvbSB7XHJcbiAgICAgICAgdG9wOjMwcHg7XHJcbiAgICAgICAgdHJhbnNmb3JtOnRyYW5zbGF0ZVgoLTUwJSkgcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgdG8ge1xyXG4gICAgICAgIHRvcDotNTBweDtcclxuICAgICAgICB0cmFuc2Zvcm06dHJhbnNsYXRlWCgtNTAlKSByb3RhdGUoMzYwZGVnKTtcclxuICAgIH1cclxufVxyXG5cclxuLyogXHJcbiAgICBGT1JNIFxyXG4qL1xyXG5cclxuLmJ1dHRvbnMge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbn1cclxuXHJcbi5leGl0LWJ1dHRvbi1mb3JtIHtcclxuICAgIG1hcmdpbjogMzBweCAwIDAgMzI1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcclxuICAgIGhlaWdodDogNDBweDtcclxufVxyXG5cclxuLnN1Ym1pdC1idXR0b24ge1xyXG4gICAgbWFyZ2luOiAzMHB4IDAgMCAyNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U0NmU3NTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIG1pbi13aWR0aDogMTAwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG4vKiBcclxuICAgIENJUkNMRSBDT0xPUiBcclxuICAgICAgICAmJlxyXG4gICAgUFJJT1JJVFlcclxuKi9cclxuXHJcbi5hY3Rpdml0eS1jb2xvciwgLm9wcG9ydHVuaXR5LWNvbG9yIHtcclxuICAgIHdpZHRoOiAxOHB4O1xyXG4gICAgaGVpZ2h0OiAxOHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgIG1hcmdpbjogYXV0bztcclxufVxyXG5cclxuLmFjdGl2aXR5LXByaW9yaXR5LCAub3Bwb3J0dW5pdHktcHJpb3JpdHkge1xyXG4gICAgbWFyZ2luOiBhdXRvIDBweCBhdXRvIGF1dG87XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5wcmlvcml0eSB7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxufVxyXG5cclxuLnByaW9yaXR5LW11eWFsdGEgeyBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1IH1cclxuLnByaW9yaXR5LWFsdGEgeyBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxIH1cclxuLnByaW9yaXR5LW5vcm1hbCB7IGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGUgfVxyXG4ucHJpb3JpdHktYmFqYSB7IGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQgfVxyXG4ucHJpb3JpdHktbmluZ3VuYSB7IGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzIgfVxyXG5cclxuLyogXHJcbiAgICAtLS0tLS1cclxuICAgIFNDUk9MTFxyXG4gICAgLS0tLS0tXHJcbiovXHJcblxyXG4uZGlhbG9nLWNvbnRlbnQ6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICAgIHdpZHRoOiAxMHB4O1xyXG59XHJcblxyXG4uZGlhbG9nLWNvbnRlbnQ6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICAgIGJhY2tncm91bmQ6ICNjMmMyYzI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbn0iLCJAaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL2RpYWxvZy11bmlmaWVkJztcclxuXHJcbi5wYWRkaW5nIHtcclxuICBwYWRkaW5nLWxlZnQ6IDVweDtcclxufVxyXG4uaW5saW5lLWJsb2NrIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5hbnN3ZXIge1xyXG4gIGNvbG9yOiAjZGU3MzczO1xyXG59XHJcblxyXG4ucmVzdWx0cyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuXHJcbiAgLmxlZnQge1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgfVxyXG4gIC5yaWdodCB7XHJcbiAgICB3aWR0aDogNzAlO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICB9XHJcbn1cclxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](SentExamDetailComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-sent-exam-detail',
          templateUrl: './sent-exam-detail.component.html',
          styleUrls: ['./sent-exam-detail.component.scss']
        }]
      }], function () {
        return [{
          type: undefined,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
            args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_0__["MAT_DIALOG_DATA"]]
          }]
        }, {
          type: _counter_infrastructure_services_counter_service__WEBPACK_IMPORTED_MODULE_2__["CounterService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/ng-components/sent-exams-table/config.ts":
  /*!******************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/ng-components/sent-exams-table/config.ts ***!
    \******************************************************************************************/

  /*! exports provided: table */

  /***/
  function srcAppEvaluationsExamInfrastructureNgComponentsSentExamsTableConfigTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "table", function () {
      return table;
    });

    var table = {
      multiSelect: false,
      actions: [{
        name: 'Show',
        icon: 'search'
      }, {
        name: 'Resend',
        icon: 'repeat'
      }, {
        name: 'Restart',
        icon: 'replay'
      }],
      columns: [{
        name: 'Id',
        prop: 'id',
        shown: true,
        route: '',
        routeId: '',
        cellClass: 'id-table-column'
      }, {
        name: 'Exam',
        prop: 'examName',
        shown: true
      }, {
        name: 'Test',
        prop: 'testName',
        shown: true
      }, {
        name: 'Email',
        prop: 'email',
        shown: true
      }, {
        name: 'Exam sent at',
        prop: 'examSentAt',
        type: 'date',
        shown: true
      }, {
        name: 'Exam started at',
        prop: 'examStartedAt',
        type: 'date',
        shown: true
      }, {
        name: 'Exam end at',
        prop: 'examEndAt',
        type: 'date',
        shown: true
      }]
    };
    /***/
  },

  /***/
  "./src/app/evaluations/exam/infrastructure/ng-components/sent-exams-table/sent-exams-table.component.ts":
  /*!**************************************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/ng-components/sent-exams-table/sent-exams-table.component.ts ***!
    \**************************************************************************************************************/

  /*! exports provided: SentExamsTableComponent */

  /***/
  function srcAppEvaluationsExamInfrastructureNgComponentsSentExamsTableSentExamsTableComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SentExamsTableComponent", function () {
      return SentExamsTableComponent;
    });
    /* harmony import */


    var src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! src/app/shared/custom-mat-elements/dynamic */
    "./src/app/shared/custom-mat-elements/dynamic.ts");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/material/paginator */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/paginator.js");
    /* harmony import */


    var _angular_material_sort__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material/sort */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/sort.js");
    /* harmony import */


    var _sent_exam_detail_sent_exam_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../sent-exam-detail/sent-exam-detail.component */
    "./src/app/evaluations/exam/infrastructure/ng-components/sent-exam-detail/sent-exam-detail.component.ts");
    /* harmony import */


    var _config__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./config */
    "./src/app/evaluations/exam/infrastructure/ng-components/sent-exams-table/config.ts");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
    /* harmony import */


    var _application_resend__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../application/resend */
    "./src/app/evaluations/exam/application/resend.ts");
    /* harmony import */


    var _application_restart__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../application/restart */
    "./src/app/evaluations/exam/application/restart.ts");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _services_exam_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../services/exam.service */
    "./src/app/evaluations/exam/infrastructure/services/exam.service.ts");
    /* harmony import */


    var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/dialog */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
    /* harmony import */


    var src_app_evaluations_sent_exam_infraestructure_SentExamMapper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! src/app/evaluations/sent-exam/infraestructure/SentExamMapper */
    "./src/app/evaluations/sent-exam/infraestructure/SentExamMapper.ts");
    /* harmony import */


    var src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! src/app/auth/session/infrastructure/services/cookie.service */
    "./src/app/auth/session/infrastructure/services/cookie.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component */
    "./src/app/shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component.ts");
    /* harmony import */


    var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/material/progress-bar */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-bar.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-filters/dynamic-filters.component */
    "./src/app/shared/custom-mat-elements/dynamic-filters/dynamic-filters.component.ts");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component */
    "./src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component.ts");

    var _c0 = ["table"];

    function SentExamsTableComponent_mat_progress_bar_0_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "mat-progress-bar", 3);
      }
    }

    function SentExamsTableComponent_div_1_Template(rf, ctx) {
      if (rf & 1) {
        var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "app-dynamic-filters", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("query", function SentExamsTableComponent_div_1_Template_app_dynamic_filters_query_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4);

          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r3.filtersChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "button", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SentExamsTableComponent_div_1_Template_button_click_3_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4);

          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r5.setConfigVisibility(!ctx_r5.configVisibility);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, " Settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "app-dynamic-table-mark-ii", 7, 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("action", function SentExamsTableComponent_div_1_Template_app_dynamic_table_mark_ii_action_7_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4);

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r6.doAction($event);
        })("pagination", function SentExamsTableComponent_div_1_Template_app_dynamic_table_mark_ii_pagination_7_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4);

          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r7.paginationChanged($event);
        })("order", function SentExamsTableComponent_div_1_Template_app_dynamic_table_mark_ii_order_7_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4);

          var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r8.orderChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("filters", ctx_r1.filters)("showFilters", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("config", ctx_r1.tableConfig)("data", ctx_r1.tableData);
      }
    }

    var SentExamsTableComponent = /*#__PURE__*/function () {
      function SentExamsTableComponent(examService, dialog, _sentExamMapper, _cookieFacade) {
        _classCallCheck(this, SentExamsTableComponent);

        this.examService = examService;
        this.dialog = dialog;
        this._sentExamMapper = _sentExamMapper;
        this._cookieFacade = _cookieFacade;
        this.examsTable = true;
        this.exams = [];
        this.idExam = null;
        this.user = undefined;
        this.showFilters = false;
        this.loading = true;
        this.scopeEmployeeInsert = false; // Auxiliary variable to check scope

        this.scopeEvaluationsInsert = false; // Auxiliary variable to check scope

        this.tableConfig = _config__WEBPACK_IMPORTED_MODULE_5__["table"];
        this.filters = [];
        this.columns = [];
        this.configVisibility = false;
      }

      _createClass(SentExamsTableComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _a, _b, _c, _d;

          this._buildFilters();

          this.tableData = this.sentExams;
          Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["parseTableFiltersConfig"])(this.tableFiltersConfig, this.tableConfig, this.filters);
          this.queryFilters = this.filtersValues ? this._clean(JSON.parse(this.filtersValues)) : {};
          this.tableConfig.pagination = (_b = (_a = this.tableFiltersConfig) === null || _a === void 0 ? void 0 : _a.pagination) !== null && _b !== void 0 ? _b : {
            page: 0,
            size: 10
          };
          this.queryPagination = this.tableConfig.pagination;
          this.tableConfig.order = (_d = (_c = this.tableFiltersConfig) === null || _c === void 0 ? void 0 : _c.order) !== null && _d !== void 0 ? _d : {
            orderField: '',
            order: ''
          };
          this.queryOrder = Object.assign({}, this.tableConfig.order);
          this.tableConfig.order.orderField = this.tableConfig.order.orderField ? Object.entries(this._sentExamMapper.mapTo(_defineProperty({}, this.tableConfig.order.orderField, ''))).find(function (entry) {
            return entry[1] === '';
          })[0] : '';
          this.loading = false;
          this.configFilterValue();
        }
      }, {
        key: "configFilterValue",
        value: function configFilterValue() {
          var _this17 = this;

          var queryFilterAux = this.filtersValues ? this._clean(this._sentExamMapper.mapTo(JSON.parse(this.filtersValues))) : {};
          this.filters.forEach(function (filter) {
            filter.defaultValue = queryFilterAux[_this17.getFilterProp(filter)];
            filter.value = queryFilterAux[_this17.getFilterProp(filter)];
          });
        }
      }, {
        key: "getFilterProp",
        value: function getFilterProp(filter) {
          return filter.prop ? filter.type === 'autoselect' ? filter.searchValue : filter.prop : filter.searchValue;
        }
      }, {
        key: "_clean",
        value: function _clean(object) {
          var cleaned = {};
          var keys = Object.keys(object);
          keys.forEach(function (key) {
            if (object[key]) {
              cleaned[key] = object[key];
            }
          });
          return cleaned;
        }
      }, {
        key: "configChange",
        value: function configChange(event) {
          this.tableConfig = Object.assign({}, event[0]);
          this.filters = event[1];

          this._cookieFacade.save('sentExam-table-config', JSON.stringify(Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        }
      }, {
        key: "setConfigVisibility",
        value: function setConfigVisibility(visible) {
          this.configVisibility = visible;
        }
      }, {
        key: "backSearch",
        value: function backSearch() {
          var _this18 = this;

          if (this.queryFilters.examStartedAt) {
            var date = new Date(this.queryFilters.examStartedAt);
            date = new Date(date.setDate(date.getDate() - 1));
            this.queryFilters.examStartedAtInitial = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
            date = new Date(date.setDate(date.getDate() + 2)); // Date adding one day

            this.queryFilters.examStartedAtFinal = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
          }

          this.examService.getSentExams(Object.assign(Object.assign(Object.assign({}, this.queryPagination), this.queryOrder), this.queryFilters)).subscribe(function (exams) {
            _this18.tableData = exams;
          });
        }
      }, {
        key: "doAction",
        value: function doAction(event) {
          if (event.action === 'Show') {
            this.openDetailSentExamDialog(event.item);
          }

          if (event.action === 'Resend') {
            this.openResendExamDialog(event.item);
          }

          if (event.action === 'Restart') {
            this.openRestartExamDialog(event.item);
          }
        }
      }, {
        key: "openDetailSentExamDialog",
        value: function openDetailSentExamDialog(sentExam) {
          this.dialog.open(_sent_exam_detail_sent_exam_detail_component__WEBPACK_IMPORTED_MODULE_4__["SentExamDetailComponent"], {
            data: sentExam
          });
        }
      }, {
        key: "openResendExamDialog",
        value: function openResendExamDialog(sentExam) {
          var _this19 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: 'You are about to resend this exam',
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080'
          }).then(function (result) {
            if (result.isConfirmed) {
              Object(_application_resend__WEBPACK_IMPORTED_MODULE_7__["resend"])(sentExam.id, _this19.examService).subscribe(function () {
                Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_9__["successAlert"])('The exam was successfully resent');
              });
            }
          });
        }
      }, {
        key: "openRestartExamDialog",
        value: function openRestartExamDialog(sentExam) {
          var _this20 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: 'You are about to restart this exam\'s time',
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080'
          }).then(function (result) {
            if (result.isConfirmed) {
              Object(_application_restart__WEBPACK_IMPORTED_MODULE_8__["restart"])(sentExam.id, _this20.examService).subscribe(function () {
                Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_9__["successAlert"])('The exam was successfully resent');
              });
            }
          });
        }
      }, {
        key: "filtersChanged",
        value: function filtersChanged(params) {
          if (params && Object.keys(params).length !== 0) {
            this.queryFilters = params;
          } else {
            this.queryFilters = {};
          }

          this._cookieFacade.save('sentExamFilterCache', JSON.stringify(this.queryFilters));

          this.queryPagination = {
            page: 0,
            size: this.queryPagination.size
          };

          this._cookieFacade.save('sentExam-table-config', JSON.stringify(Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.dynamicTable.resetPageIndex();
          this.backSearch();
        }
      }, {
        key: "paginationChanged",
        value: function paginationChanged(event) {
          this.queryPagination.page = event.page;
          this.queryPagination.size = event.size;

          this._cookieFacade.save('sentExam-table-config', JSON.stringify(Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "orderChanged",
        value: function orderChanged(event) {
          this.queryOrder.orderField = event.orderField;
          this.queryOrder.order = event.order;
          this.queryPagination.page = event.page;
          this.queryPagination.size = event.size;

          this._cookieFacade.save('sentExam-table-config', JSON.stringify(Object(src_app_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_0__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "_buildFilters",
        value: function _buildFilters() {
          this.filters = [{
            options: [],
            prop: 'id',
            retProp: '',
            type: 'input',
            appearance: 'outline',
            "class": 'input',
            label: 'Id',
            placeholder: 'Id',
            shown: true
          }, {
            options: [],
            prop: 'email',
            retProp: '',
            type: 'input',
            appearance: 'outline',
            "class": 'input',
            label: 'Email',
            placeholder: 'Email',
            shown: true
          }, {
            options: [],
            prop: 'examStartedAt',
            retProp: 'examStartedAt',
            type: 'date',
            appearance: 'outline',
            "class": 'date',
            label: 'Exam started at',
            placeholder: 'Exam started at',
            shown: true
          }];
        }
      }]);

      return SentExamsTableComponent;
    }();

    SentExamsTableComponent.ɵfac = function SentExamsTableComponent_Factory(t) {
      return new (t || SentExamsTableComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_services_exam_service__WEBPACK_IMPORTED_MODULE_10__["ExamService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__["MatDialog"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_evaluations_sent_exam_infraestructure_SentExamMapper__WEBPACK_IMPORTED_MODULE_12__["SentExamMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_13__["CookieFacadeService"]));
    };

    SentExamsTableComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: SentExamsTableComponent,
      selectors: [["app-sent-exams-table"]],
      viewQuery: function SentExamsTableComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"], true);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_angular_material_sort__WEBPACK_IMPORTED_MODULE_3__["MatSort"], true);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.paginator = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.sort = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.dynamicTable = _t.first);
        }
      },
      inputs: {
        sentExams: "sentExams",
        filtersValues: "filtersValues",
        tableFiltersConfig: "tableFiltersConfig"
      },
      decls: 3,
      vars: 5,
      consts: [["mode", "indeterminate", 4, "ngIf"], [4, "ngIf"], [3, "visibility", "columnsConfig", "filtersConfig", "configChanged", "closed"], ["mode", "indeterminate"], [1, "filters-plus-config"], [1, "dyn-filters", 3, "filters", "showFilters", "query"], ["mat-button", "", 1, "configButton", 3, "click"], [3, "config", "data", "action", "pagination", "order"], ["table", ""]],
      template: function SentExamsTableComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, SentExamsTableComponent_mat_progress_bar_0_Template, 1, 0, "mat-progress-bar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, SentExamsTableComponent_div_1_Template, 9, 4, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "app-dynamic-table-config-panel", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("configChanged", function SentExamsTableComponent_Template_app_dynamic_table_config_panel_configChanged_2_listener($event) {
            return ctx.configChange($event);
          })("closed", function SentExamsTableComponent_Template_app_dynamic_table_config_panel_closed_2_listener() {
            return ctx.setConfigVisibility(false);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visibility", ctx.configVisibility)("columnsConfig", ctx.tableConfig)("filtersConfig", ctx.filters);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_14__["NgIf"], _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_15__["DynamicTableConfigPanelComponent"], _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_16__["MatProgressBar"], _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_17__["DynamicFiltersComponent"], _angular_material_button__WEBPACK_IMPORTED_MODULE_18__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_19__["MatIcon"], _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_20__["DynamicTableMarkIiComponent"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3NlbnQtZXhhbXMtdGFibGUvQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFzc2V0c1xcc3R5bGVzXFxzdGFmZml0LXN0eWxlLTEuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3NlbnQtZXhhbXMtdGFibGUvc2VudC1leGFtcy10YWJsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQ0FBQTtBQVFBLHdCQUFBO0FBUUEsdUJBQUE7QUFHQSwyQkFBQTtBQUVBO0VBQ0UsNkJBQUE7QUNoQkY7QURrQkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ2hCSjtBRGtCSTtFQUNFLG1CQTNCUTtFQTRCUixnQkFBQTtFQUNBLGNBQUE7QUNoQk47QURzQkE7RUFDRSxhQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNuQkY7QURxQkU7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ25CSjtBRHFCSTtFQUNFLFlBQUE7QUNuQk47QURzQkU7RUFDRSxtQkFBQTtFQUNBLGNBQUE7QUNwQko7QUR3QkE7RUFDRSw2QkFBQTtFQUNBLGVBQUE7QUNyQkY7QUR1QkU7RUFDRSxTQUFBO0FDckJKO0FEd0JFO0VBQ0UsZ0NBQUE7QUN0Qko7QUR5QkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ3ZCSjtBRHlCSTtFQUNFLG1CQTNFUTtFQTRFUixnQkFBQTtFQUNBLGNBQUE7QUN2Qk47QUQwQkk7RUFDRSx5QkE1RWlCO0VBNkVqQixtQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FDeEJOO0FENkJBO0VBQ0UsY0FsRm1CO0FDd0RyQjtBRDZCQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQzFCRjtBRDZCQTtFQUNFLFdBQUE7QUMxQkY7QUQ2QkE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDMUJGO0FENEJFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDMUJKO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUMzQkY7QUQ4QkE7RUFDRSxlQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0Usa0JBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7QUMzQkY7QURrQ0E7RUFDRSxjQUFBO0FDL0JGO0FEa0NBLHlCQUFBO0FBRUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1EQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLHlCQUFBO0FDaENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLGNBQUE7QUNsQ0Y7QURxQ0EsWUFBQTtBQUVBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSx1QkFBQTtBQ25DRjtBRHNDQTtFQUNFLGVBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLDBDQUFBO0FDbkNGIiwiZmlsZSI6InNyYy9hcHAvZXZhbHVhdGlvbnMvZXhhbS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3NlbnQtZXhhbXMtdGFibGUvc2VudC1leGFtcy10YWJsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cclxuJHNtYWxsOiAwLjhyZW07XHJcbiRtZWRpdW06IDFyZW07XHJcbiRsYXJnZTogMS4yNXJlbTtcclxuJGV4dHJhLWxhcmdlOiAxLjU2M3JlbTtcclxuJGh1Z2U6IDEuOTUzcmVtO1xyXG4kZXh0cmEtaHVnZTogMi40NDFyZW07XHJcblxyXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXHJcbiRjb2xvci1wYWxldHRlLTEtbWFpbjogI0RFNzM3MztcclxuJGNvbG9yLXBhbGV0dGUtMS1zZWNvbmQ6ICNBQkFCRkY7XHJcbiRjb2xvci1wcmltYXJ5LWxpZ2h0OiAjRUZFRkVGO1xyXG4kY29sb3ItcHJpbWFyeS1kYXJrOiAjMTkxOTE5O1xyXG4kY29sb3Itc2Vjb25kYXJ5LWRhcms6ICMzMzMzMzM7XHJcbiRjb2xvci1zY3JvbGxiYXItdGh1bWI6ICM4QzhDOEM7XHJcblxyXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cclxuXHJcblxyXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXHJcblxyXG4uYy1jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcblxyXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgLmR5bi1maWx0ZXJzIHtcclxuICAgIGZsZXgtZ3JvdzogMTtcclxuICAgIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgICBtYXJnaW46IDAgNDBweCAwIDA7XHJcblxyXG4gICAgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcclxuICAgICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29uZmlnQnV0dG9uIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcbiAgbWF4LWhlaWdodDogODglO1xyXG5cclxuICAuY2FyZC1jb250ZW50LCAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1wYWxldHRlLTEtbWFpbjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMnJlbTtcclxuICAgICAgaGVpZ2h0OiAyLjNyZW07XHJcbiAgICAgIGNvbG9yOiAjZWZlZmVmO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcclxuICBjb2xvcjogJGNvbG9yLXByaW1hcnktZGFyaztcclxufVxyXG5cclxuLmZpbHRlciB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMC41ZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XHJcbiAgbWF4LXdpZHRoOiAxMGVtO1xyXG4gIG1hcmdpbi1yaWdodDogNWVtO1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuICAvLyBtYXgtaGVpZ2h0OiA4MDBweDtcclxuICAubWF0LXRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgfVxyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xyXG4gIGNvbG9yOiAjOGI4YjhiO1xyXG59XHJcblxyXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbn1cclxuXHJcbi5pZEhlYWRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWRDZWxsIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbm1hdC1jYXJkLWNvbnRlbnQge1xyXG4gIG1hcmdpbjogMCAzNHB4O1xyXG59XHJcblxyXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xyXG5cclxuLnByaW9yaXR5IHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi8vIEBUT0RPOiBjaGFuZ2UgY29sb3IgdG8gdmFyaWFibGVzXHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcclxufVxyXG5cclxuLnByaW9yaXR5LWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1ub3JtYWwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1iYWphIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbmluZ3VuYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLnJlZC1kYXRlIHtcclxuICBjb2xvcjogI2ViNDE0MTtcclxufVxyXG5cclxuLyogTUFUIFRBQiAqL1xyXG5cclxuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xyXG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAuMTIpO1xyXG59XHJcbiIsIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xuLmMtY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMge1xuICBmbGV4LWdyb3c6IDE7XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTUwcHg7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuY29uZmlnQnV0dG9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZmxleC1zaHJpbms6IDA7XG59XG5cbi5jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xuICBtYXgtaGVpZ2h0OiA4OCU7XG59XG4uY2FyZC1jb21wb25lbnQgLmNhcmQtY29udGVudCwgLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBtYXJnaW46IDA7XG59XG4uY2FyZC1jb21wb25lbnQgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBidXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xuICBib3JkZXItcmFkaXVzOiAycmVtO1xuICBoZWlnaHQ6IDIuM3JlbTtcbiAgY29sb3I6ICNlZmVmZWY7XG59XG5cbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XG4gIGNvbG9yOiAjMTkxOTE5O1xufVxuXG4uZmlsdGVyIHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cblxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XG4gIG1heC13aWR0aDogMTBlbTtcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XG59XG5cbi50YWJsZS1yZXNwb25zaXZlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy14OiBhdXRvO1xufVxuLnRhYmxlLXJlc3BvbnNpdmUgLm1hdC10YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLm1hdC1oZWFkZXItcm93IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcbiAgY29sb3I6ICM4YjhiOGI7XG59XG5cbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG5cbi5pZEhlYWRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pZENlbGwge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxubWF0LWNhcmQtY29udGVudCB7XG4gIG1hcmdpbjogMCAzNHB4O1xufVxuXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xuLnByaW9yaXR5IHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucHJpb3JpdHktbXV5YWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XG59XG5cbi5wcmlvcml0eS1hbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcbn1cblxuLnByaW9yaXR5LW5vcm1hbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XG59XG5cbi5wcmlvcml0eS1iYWphIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcbn1cblxuLnByaW9yaXR5LW5pbmd1bmEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucmVkLWRhdGUge1xuICBjb2xvcjogI2ViNDE0MTtcbn1cblxuLyogTUFUIFRBQiAqL1xuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufSJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](SentExamsTableComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-sent-exams-table',
          templateUrl: './sent-exams-table.component.html',
          styleUrls: ['./sent-exams-table.component.scss']
        }]
      }], function () {
        return [{
          type: _services_exam_service__WEBPACK_IMPORTED_MODULE_10__["ExamService"]
        }, {
          type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__["MatDialog"]
        }, {
          type: src_app_evaluations_sent_exam_infraestructure_SentExamMapper__WEBPACK_IMPORTED_MODULE_12__["SentExamMapper"]
        }, {
          type: src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_13__["CookieFacadeService"]
        }];
      }, {
        sentExams: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        filtersValues: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        tableFiltersConfig: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        paginator: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_angular_material_paginator__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]]
        }],
        sort: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_3__["MatSort"]]
        }],
        dynamicTable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['table']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/resolvers/exam-management.resolver.service.ts":
  /*!***********************************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/resolvers/exam-management.resolver.service.ts ***!
    \***********************************************************************************************/

  /*! exports provided: ExamManagementResolverService */

  /***/
  function srcAppEvaluationsExamInfrastructureResolversExamManagementResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ExamManagementResolverService", function () {
      return ExamManagementResolverService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _recruitment_candidate_application_searchCandidates__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../recruitment/candidate/application/searchCandidates */
    "./src/app/recruitment/candidate/application/searchCandidates.ts");
    /* harmony import */


    var _correction_application_searchCorrections__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../correction/application/searchCorrections */
    "./src/app/evaluations/correction/application/searchCorrections.ts");
    /* harmony import */


    var _application_getExams__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../application/getExams */
    "./src/app/evaluations/exam/application/getExams.ts");
    /* harmony import */


    var _sent_exam_application_getSentExams__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../sent-exam/application/getSentExams */
    "./src/app/evaluations/sent-exam/application/getSentExams.ts");
    /* harmony import */


    var _test_application_searchTests__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../test/application/searchTests */
    "./src/app/evaluations/test/application/searchTests.ts");
    /* harmony import */


    var _services_exam_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../services/exam.service */
    "./src/app/evaluations/exam/infrastructure/services/exam.service.ts");
    /* harmony import */


    var _recruitment_candidate_infrastructure_services_candidate_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../../recruitment/candidate/infrastructure/services/candidate.service */
    "./src/app/recruitment/candidate/infrastructure/services/candidate.service.ts");
    /* harmony import */


    var _test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../test/infrastructure/services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var _correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../correction/infrastructure/services/correction.service */
    "./src/app/evaluations/correction/infrastructure/services/correction.service.ts");
    /* harmony import */


    var _correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../correction/infrastructure/CorrectionMapper */
    "./src/app/evaluations/correction/infrastructure/CorrectionMapper.ts");
    /* harmony import */


    var _recruitment_candidate_infrastructure_CandidateMapper__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../../recruitment/candidate/infrastructure/CandidateMapper */
    "./src/app/recruitment/candidate/infrastructure/CandidateMapper.ts");
    /* harmony import */


    var src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! src/app/auth/session/infrastructure/services/cookie.service */
    "./src/app/auth/session/infrastructure/services/cookie.service.ts");

    var ExamManagementResolverService = /*#__PURE__*/function () {
      function ExamManagementResolverService(_examService, _candidateService, _testService, _correctionService, _correctionMapper, _candidateMapper, _cookieService) {
        _classCallCheck(this, ExamManagementResolverService);

        this._examService = _examService;
        this._candidateService = _candidateService;
        this._testService = _testService;
        this._correctionService = _correctionService;
        this._correctionMapper = _correctionMapper;
        this._candidateMapper = _candidateMapper;
        this._cookieService = _cookieService;
      }

      _createClass(ExamManagementResolverService, [{
        key: "resolve",
        value: function resolve() {
          var _a, _b;

          var params = {
            page: 0,
            size: 100
          };

          var filtersCacheExam = this._cookieService.get('examFilterCache');

          var filtersCacheSentExam = this._cookieService.get('sentExamFilterCache');

          var examTableFiltersConfig = JSON.parse(this._cookieService.get('exam-table-config'));
          var sentExamTableFiltersConfig = JSON.parse(this._cookieService.get('sentExam-table-config'));
          var examParms = (_a = examTableFiltersConfig === null || examTableFiltersConfig === void 0 ? void 0 : examTableFiltersConfig.pagination) !== null && _a !== void 0 ? _a : {
            page: 0,
            size: 10
          };
          var sentExamParams = (_b = sentExamTableFiltersConfig === null || sentExamTableFiltersConfig === void 0 ? void 0 : sentExamTableFiltersConfig.pagination) !== null && _b !== void 0 ? _b : {
            page: 0,
            size: 10
          };
          var exams = Object(_application_getExams__WEBPACK_IMPORTED_MODULE_5__["getExams"])(Object.assign(Object.assign({}, JSON.parse(filtersCacheExam)), examParms), this._examService);
          var sentExams = Object(_sent_exam_application_getSentExams__WEBPACK_IMPORTED_MODULE_6__["getSentExams"])(Object.assign(Object.assign({}, JSON.parse(filtersCacheSentExam)), sentExamParams), this._examService);
          var candidates = Object(_recruitment_candidate_application_searchCandidates__WEBPACK_IMPORTED_MODULE_3__["searchCandidates"])(params, this._candidateService, this._candidateMapper);
          var tests = Object(_test_application_searchTests__WEBPACK_IMPORTED_MODULE_7__["searchTests"])(params, this._testService);
          var corrections = Object(_correction_application_searchCorrections__WEBPACK_IMPORTED_MODULE_4__["searchCorrections"])(params, this._correctionService, this._correctionMapper);
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["forkJoin"])(exams, sentExams, candidates, tests, corrections).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            return {
              data: {
                exams: response[0],
                sentExams: response[1],
                candidates: response[2],
                tests: response[3],
                corrections: response[4],
                filtersValuesExam: filtersCacheExam,
                filtersValuesSentExam: filtersCacheSentExam,
                examTableFiltersConfig: examTableFiltersConfig,
                sentExamTableFiltersConfig: sentExamTableFiltersConfig
              }
            };
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])({
              data: null,
              message: 'Error on exam management resolver, data couldn\' be fetched',
              error: error
            });
          }));
        }
      }]);

      return ExamManagementResolverService;
    }();

    ExamManagementResolverService.ɵfac = function ExamManagementResolverService_Factory(t) {
      return new (t || ExamManagementResolverService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_exam_service__WEBPACK_IMPORTED_MODULE_8__["ExamService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_recruitment_candidate_infrastructure_services_candidate_service__WEBPACK_IMPORTED_MODULE_9__["CandidateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_10__["TestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_11__["CorrectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_12__["CorrectionMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_recruitment_candidate_infrastructure_CandidateMapper__WEBPACK_IMPORTED_MODULE_13__["CandidateMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_14__["CookieFacadeService"]));
    };

    ExamManagementResolverService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ExamManagementResolverService,
      factory: ExamManagementResolverService.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ExamManagementResolverService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], function () {
        return [{
          type: _services_exam_service__WEBPACK_IMPORTED_MODULE_8__["ExamService"]
        }, {
          type: _recruitment_candidate_infrastructure_services_candidate_service__WEBPACK_IMPORTED_MODULE_9__["CandidateService"]
        }, {
          type: _test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_10__["TestService"]
        }, {
          type: _correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_11__["CorrectionService"]
        }, {
          type: _correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_12__["CorrectionMapper"]
        }, {
          type: _recruitment_candidate_infrastructure_CandidateMapper__WEBPACK_IMPORTED_MODULE_13__["CandidateMapper"]
        }, {
          type: src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_14__["CookieFacadeService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/exam/infrastructure/services/exam.service.ts":
  /*!**************************************************************************!*\
    !*** ./src/app/evaluations/exam/infrastructure/services/exam.service.ts ***!
    \**************************************************************************/

  /*! exports provided: ExamService */

  /***/
  function srcAppEvaluationsExamInfrastructureServicesExamServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ExamService", function () {
      return ExamService;
    });
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../../environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var _abstract_exam_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../abstract-exam-service */
    "./src/app/evaluations/exam/infrastructure/abstract-exam-service.ts");

    var ExamService = /*#__PURE__*/function (_abstract_exam_servic) {
      _inherits(ExamService, _abstract_exam_servic);

      var _super3 = _createSuper(ExamService);

      function ExamService(_http) {
        var _this21;

        _classCallCheck(this, ExamService);

        _this21 = _super3.call(this);
        _this21._http = _http;
        _this21.BASE_URL = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl;
        _this21.ENDPOINT = 'exams';
        _this21.ENDPOINT2 = 'exams-sent';
        return _this21;
      }

      _createClass(ExamService, [{
        key: "createExam",
        value: function createExam(exam) {
          return this._http.post("".concat(this.BASE_URL).concat(this.ENDPOINT), exam);
        }
      }, {
        key: "get",
        value: function get(id) {
          throw new Error('Method not implemented.');
        }
      }, {
        key: "update",
        value: function update(idExam, exam) {
          return this._http.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(idExam), exam);
        }
      }, {
        key: "updateCorrection",
        value: function updateCorrection(idExam, idCorrection) {
          return this._http.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(idExam, "/correction/").concat(idCorrection), {});
        }
      }, {
        key: "search",
        value: function search(query) {
          return this._http.get("".concat(this.BASE_URL).concat(this.ENDPOINT, "/search"), {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]({
              fromObject: query
            })
          });
        }
      }, {
        key: "getSentExams",
        value: function getSentExams(query) {
          return this._http.get("".concat(this.BASE_URL).concat(this.ENDPOINT2, "/search"), {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]({
              fromObject: query
            })
          });
        }
      }, {
        key: "sendExam",
        value: function sendExam(idCandidateEmail, idExam) {
          return this._http.post("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(idExam, "/send?idCandidateEmail=").concat(idCandidateEmail), idCandidateEmail);
        }
      }, {
        key: "resend",
        value: function resend(idExamSent) {
          return this._http.post("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(idExamSent, "/send/existing"), idExamSent);
        }
      }, {
        key: "restart",
        value: function restart(idExamSent) {
          return this._http.put("".concat(this.BASE_URL).concat(this.ENDPOINT2, "/").concat(idExamSent, "/restart"), idExamSent);
        }
      }]);

      return ExamService;
    }(_abstract_exam_service__WEBPACK_IMPORTED_MODULE_3__["AbstractExamService"]);

    ExamService.ɵfac = function ExamService_Factory(t) {
      return new (t || ExamService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]));
    };

    ExamService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
      token: ExamService,
      factory: ExamService.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ExamService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/question/application/removeQuestion.ts":
  /*!********************************************************************!*\
    !*** ./src/app/evaluations/question/application/removeQuestion.ts ***!
    \********************************************************************/

  /*! exports provided: removeQuestion */

  /***/
  function srcAppEvaluationsQuestionApplicationRemoveQuestionTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "removeQuestion", function () {
      return removeQuestion;
    });

    function removeQuestion(questionId, testId, service) {
      return service.removeQuestion(questionId, testId);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/question/infrastructure/QuestionMapper.ts":
  /*!***********************************************************************!*\
    !*** ./src/app/evaluations/question/infrastructure/QuestionMapper.ts ***!
    \***********************************************************************/

  /*! exports provided: QuestionMapper */

  /***/
  function srcAppEvaluationsQuestionInfrastructureQuestionMapperTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "QuestionMapper", function () {
      return QuestionMapper;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var QuestionMapper = /*#__PURE__*/function () {
      function QuestionMapper() {
        _classCallCheck(this, QuestionMapper);
      }

      _createClass(QuestionMapper, [{
        key: "mapTo",
        value: function mapTo(params) {
          var id = params.id,
              name = params.name,
              description = params.description,
              step = params.step,
              questionType = params.questionType,
              answers = params.answers;
          return {
            id: id,
            name: name,
            description: description,
            step: step,
            questionType: questionType,
            answers: answers
          };
        }
      }, {
        key: "mapFrom",
        value: function mapFrom(params) {
          var id = params.id,
              name = params.name,
              description = params.description,
              step = params.step,
              questionType = params.questionType,
              answers = params.answers;
          return {
            id: id,
            name: name,
            description: description,
            step: step,
            questionType: questionType,
            answers: answers
          };
        }
      }]);

      return QuestionMapper;
    }();

    QuestionMapper.ɵfac = function QuestionMapper_Factory(t) {
      return new (t || QuestionMapper)();
    };

    QuestionMapper.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: QuestionMapper,
      factory: QuestionMapper.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](QuestionMapper, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/sent-exam/application/getSentExams.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/evaluations/sent-exam/application/getSentExams.ts ***!
    \*******************************************************************/

  /*! exports provided: getSentExams */

  /***/
  function srcAppEvaluationsSentExamApplicationGetSentExamsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getSentExams", function () {
      return getSentExams;
    });

    function getSentExams(params, service) {
      return service.getSentExams(params);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/sent-exam/infraestructure/SentExamMapper.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/evaluations/sent-exam/infraestructure/SentExamMapper.ts ***!
    \*************************************************************************/

  /*! exports provided: SentExamMapper */

  /***/
  function srcAppEvaluationsSentExamInfraestructureSentExamMapperTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SentExamMapper", function () {
      return SentExamMapper;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var SentExamMapper = /*#__PURE__*/function () {
      function SentExamMapper() {
        _classCallCheck(this, SentExamMapper);
      }

      _createClass(SentExamMapper, [{
        key: "mapTo",
        value: function mapTo(params) {
          var id = params.id,
              email = params.email,
              examSentAt = params.examSentAt,
              examStartedAt = params.examStartedAt,
              examEndAt = params.examEndAt,
              exam_id = params.exam_id,
              examName = params.examName,
              correction_id = params.correction_id,
              correction_name = params.correction_name,
              test_id = params.test_id,
              test_name = params.test_name;
          return {
            id: id,
            email: email,
            examSentAt: examSentAt,
            examStartedAt: examStartedAt,
            examEndAt: examEndAt,
            examId: exam_id,
            examName: examName,
            correctionId: correction_id,
            correctionName: correction_name,
            testId: test_id,
            testName: test_name
          };
        }
      }]);

      return SentExamMapper;
    }();

    SentExamMapper.ɵfac = function SentExamMapper_Factory(t) {
      return new (t || SentExamMapper)();
    };

    SentExamMapper.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: SentExamMapper,
      factory: SentExamMapper.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SentExamMapper, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/application/createTest.ts":
  /*!************************************************************!*\
    !*** ./src/app/evaluations/test/application/createTest.ts ***!
    \************************************************************/

  /*! exports provided: createTest */

  /***/
  function srcAppEvaluationsTestApplicationCreateTestTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "createTest", function () {
      return createTest;
    });
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    function createTest(test, service, mapper) {
      return service.create(mapper.mapFrom(test)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(mapper.mapTo));
    }
    /***/

  },

  /***/
  "./src/app/evaluations/test/application/getTests.ts":
  /*!**********************************************************!*\
    !*** ./src/app/evaluations/test/application/getTests.ts ***!
    \**********************************************************/

  /*! exports provided: getTest */

  /***/
  function srcAppEvaluationsTestApplicationGetTestsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getTest", function () {
      return getTest;
    });

    function getTest(id, service) {
      return service.finbById(id);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/test/application/searchTests.ts":
  /*!*************************************************************!*\
    !*** ./src/app/evaluations/test/application/searchTests.ts ***!
    \*************************************************************/

  /*! exports provided: searchTests */

  /***/
  function srcAppEvaluationsTestApplicationSearchTestsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "searchTests", function () {
      return searchTests;
    });

    function searchTests(query, service) {
      return service.findAll(query);
    }
    /***/

  },

  /***/
  "./src/app/evaluations/test/application/updateTest.ts":
  /*!************************************************************!*\
    !*** ./src/app/evaluations/test/application/updateTest.ts ***!
    \************************************************************/

  /*! exports provided: updateTest */

  /***/
  function srcAppEvaluationsTestApplicationUpdateTestTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "updateTest", function () {
      return updateTest;
    });
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    function updateTest(test, service, mapper) {
      return service.update(mapper.mapFrom(test)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(mapper.mapTo));
    }
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/TestAbstractService.ts":
  /*!************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/TestAbstractService.ts ***!
    \************************************************************************/

  /*! exports provided: TestAbstractService */

  /***/
  function srcAppEvaluationsTestInfrastructureTestAbstractServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestAbstractService", function () {
      return TestAbstractService;
    });

    var TestAbstractService = function TestAbstractService() {
      _classCallCheck(this, TestAbstractService);
    };
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/TestMapper.ts":
  /*!***************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/TestMapper.ts ***!
    \***************************************************************/

  /*! exports provided: TestMapper */

  /***/
  function srcAppEvaluationsTestInfrastructureTestMapperTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestMapper", function () {
      return TestMapper;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var TestMapper = /*#__PURE__*/function () {
      function TestMapper() {
        _classCallCheck(this, TestMapper);
      }

      _createClass(TestMapper, [{
        key: "mapTo",
        value: function mapTo(params) {
          var description = params.description,
              id = params.id,
              name = params.name,
              questions = params.questions;
          return {
            description: description,
            id: id,
            name: name,
            questions: questions
          };
        }
      }, {
        key: "mapFrom",
        value: function mapFrom(params) {
          var description = params.description,
              id = params.id,
              name = params.name,
              questions = params.questions;
          return {
            description: description,
            id: id,
            name: name,
            questions: questions
          };
        }
      }]);

      return TestMapper;
    }();

    TestMapper.ɵfac = function TestMapper_Factory(t) {
      return new (t || TestMapper)();
    };

    TestMapper.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: TestMapper,
      factory: TestMapper.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestMapper, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/ng-components/create-correction/create-correction.component.ts":
  /*!****************************************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/ng-components/create-correction/create-correction.component.ts ***!
    \****************************************************************************************************************/

  /*! exports provided: CreateCorrectionComponent */

  /***/
  function srcAppEvaluationsTestInfrastructureNgComponentsCreateCorrectionCreateCorrectionComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CreateCorrectionComponent", function () {
      return CreateCorrectionComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _correction_application_createCorrection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../correction/application/createCorrection */
    "./src/app/evaluations/correction/application/createCorrection.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../../correction/infrastructure/services/correction.service */
    "./src/app/evaluations/correction/infrastructure/services/correction.service.ts");
    /* harmony import */


    var _correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../../correction/infrastructure/CorrectionMapper */
    "./src/app/evaluations/correction/infrastructure/CorrectionMapper.ts");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_radio__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/material/radio */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
    /* harmony import */


    var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/material/checkbox */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");

    function CreateCorrectionComponent_div_19_div_15_div_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-radio-button", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-form-field", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "input", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r7 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", false);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", answer_r7.value, " ", answer_r7.message, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("formControlName", answer_r7.id);
      }
    }

    function CreateCorrectionComponent_div_19_div_15_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-radio-group");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, CreateCorrectionComponent_div_19_div_15_div_2_Template, 9, 4, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", question_r2.answers);
      }
    }

    function CreateCorrectionComponent_div_19_div_16_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-checkbox", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-form-field", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "input", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r10 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", answer_r10.value, " ", answer_r10.message, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("formControlName", answer_r10.id);
      }
    }

    function CreateCorrectionComponent_div_19_div_16_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, CreateCorrectionComponent_div_19_div_16_div_1_Template, 9, 3, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", question_r2.answers);
      }
    }

    function CreateCorrectionComponent_div_19_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Step: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Description: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, CreateCorrectionComponent_div_19_div_15_Template, 3, 1, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, CreateCorrectionComponent_div_19_div_16_Template, 2, 1, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = ctx.$implicit;
        var i_r3 = ctx.index;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", i_r3 + 1, ". ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](question_r2.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](question_r2.step);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](question_r2.description);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "RADIO");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "CHECKBOX");
      }
    }

    function CreateCorrectionComponent_div_57_button_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateCorrectionComponent_div_57_button_9_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r17);

          var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r16.confirmEditCounter();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "done");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateCorrectionComponent_div_57_button_10_Template(rf, ctx) {
      if (rf & 1) {
        var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateCorrectionComponent_div_57_button_10_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

          var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r18.cancelEditCounter();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "clear");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateCorrectionComponent_div_57_Template(rf, ctx) {
      if (rf & 1) {
        var _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateCorrectionComponent_div_57_Template_mat_card_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r21);

          var counter_r12 = ctx.$implicit;

          var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r20.editCounter(counter_r12);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, CreateCorrectionComponent_div_57_button_9_Template, 3, 0, "button", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, CreateCorrectionComponent_div_57_button_10_Template, 3, 0, "button", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateCorrectionComponent_div_57_Template_button_click_11_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r21);

          var counter_r12 = ctx.$implicit;

          var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r22.removeCounter(counter_r12);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "delete");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var counter_r12 = ctx.$implicit;
        var i_r13 = ctx.index;

        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", counter_r12.isClicked && ctx_r1.counterClicked);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](counter_r12.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](counter_r12.description);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", i_r13 === ctx_r1.counterIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", i_r13 === ctx_r1.counterIndex);
      }
    }

    var CreateCorrectionComponent = /*#__PURE__*/function () {
      function CreateCorrectionComponent(_fb, _route, _router, _service, _mapper) {
        _classCallCheck(this, CreateCorrectionComponent);

        this._fb = _fb;
        this._route = _route;
        this._router = _router;
        this._service = _service;
        this._mapper = _mapper;
        this.counters = [];
        this.loading = true;
        this.showCounterPanel = false;
        this.showAlerts = true;
        this.showCountersForms = false;
        this.counterClicked = {
          backgroundColor: 'rgb(238, 238, 238)',
          transform: 'scale(0.95)'
        };
        this.counterIndex = -1;
      }

      _createClass(CreateCorrectionComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this._resolved = this._route.snapshot.data['response'];

          if (this._resolved.data) {
            this.test = this._resolved.data.test;
            this.loading = false;

            this._initCorrectionForm();

            this._initCounterForm();
          }
        }
      }, {
        key: "_initCorrectionForm",
        value: function _initCorrectionForm() {
          this.correctionForm = this._fb.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: ['']
          });
        }
      }, {
        key: "_initCounterForm",
        value: function _initCounterForm(counter) {
          var config = {
            name: [counter ? counter.name : '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [counter ? counter.description : '']
          };

          if (!counter) {
            this.test.questions.forEach(function (question) {
              question.answers.forEach(function (answer) {
                config[answer.id] = ['0'];
              });
            });
          } else {
            counter.answerCounters.forEach(function (ac) {
              config[ac.idAnswer] = [ac.calcValue];
            });
          }

          this.counterForm = this._fb.group(config);
        }
      }, {
        key: "addCounter",
        value: function addCounter() {
          if (this.counterForm.valid) {
            var counter = this._formatCounter();

            this.counters.push(counter);

            this._initCounterForm();
          }
        }
      }, {
        key: "_formatCounter",
        value: function _formatCounter() {
          var _this22 = this;

          var _this$counterForm$val = this.counterForm.value,
              name = _this$counterForm$val.name,
              description = _this$counterForm$val.description;
          var keys = Object.keys(this.counterForm.value);
          var answersCounters = [];
          keys.forEach(function (key) {
            if (key !== 'name' && key !== 'description') {
              answersCounters.push({
                calcValue: Number.parseInt(_this22.counterForm.value[key]),
                idAnswer: key
              });
            }
          });
          return {
            name: name,
            description: description,
            answerCounters: answersCounters
          };
        }
      }, {
        key: "editCounter",
        value: function editCounter(counter) {
          this.counterIndex = this.counters.indexOf(counter);

          this._initCounterForm(counter);
        }
      }, {
        key: "confirmEditCounter",
        value: function confirmEditCounter() {
          if (this.counterForm.valid) {
            var counter = this._formatCounter();

            this.counters[this.counterIndex] = counter;

            this._initCounterForm();

            this.counterIndex = -1;
          }
        }
      }, {
        key: "cancelEditCounter",
        value: function cancelEditCounter() {
          this._initCounterForm();

          this.counterIndex = -1;
        }
      }, {
        key: "removeCounter",
        value: function removeCounter(counter) {
          var _this23 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["questionAlert"])('This counter will be deleted').then(function (result) {
            if (result.isConfirmed) {
              var index = _this23.counters.indexOf(counter);

              if (index > -1) {
                _this23.counters.splice(index, 1);
              }
            }
          });
        }
      }, {
        key: "onCorrectionReset",
        value: function onCorrectionReset() {
          var _this24 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["questionAlert"])('All your changes will be discarted!').then(function (result) {
            if (result.isConfirmed) {
              _this24._initCorrectionForm();

              _this24._initCounterForm();

              _this24.counters = [];
            }
          });
        }
      }, {
        key: "onCorrectCancel",
        value: function onCorrectCancel() {
          var _this25 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["questionAlert"])("Your correction won't be saved! Please consider saving first").then(function (result) {
            if (result.isConfirmed) {
              _this25._router.navigate(['../..'], {
                relativeTo: _this25._route
              });
            }
          });
        }
      }, {
        key: "onCorrectionSave",
        value: function onCorrectionSave() {
          var _this26 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["questionAlert"])('Once sent you can update it').then(function (result) {
            if (result.isConfirmed) {
              var correction = Object.assign(Object.assign({}, _this26.correctionForm.value), {
                counters: _this26.counters,
                idTest: _this26.test.id
              });
              Object(_correction_application_createCorrection__WEBPACK_IMPORTED_MODULE_3__["createCorrection"])(correction, _this26._service, _this26._mapper).subscribe(function () {
                Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["successAlert"])('The test was created successfully');

                _this26._initCorrectionForm();

                _this26._initCounterForm();

                _this26.counters = [];

                _this26._router.navigate(['../..'], {
                  relativeTo: _this26._route
                });
              });
            }
          });
        }
      }]);

      return CreateCorrectionComponent;
    }();

    CreateCorrectionComponent.ɵfac = function CreateCorrectionComponent_Factory(t) {
      return new (t || CreateCorrectionComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_5__["CorrectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_6__["CorrectionMapper"]));
    };

    CreateCorrectionComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: CreateCorrectionComponent,
      selectors: [["app-create-correction"]],
      decls: 58,
      vars: 8,
      consts: [[1, "new-correction-container"], [1, "new-correction-component"], [1, "title-component"], [1, "material-icons"], [1, "test-body"], [1, "test-description"], [1, "text-id"], [1, "text-description"], [3, "formGroup"], ["class", "test-questions", 4, "ngFor", "ngForOf"], [1, "counter-list-container"], [1, "title-counters"], [1, "correction-panel", 3, "formGroup"], ["aria-label", "name", "placeholder", "Correction name", "matInput", "", "formControlName", "name"], ["aria-label", "description", "placeholder", "Correction description", "matInput", "", "formControlName", "description"], [1, "counter-list-buttons"], ["mat-flat-button", "", 1, "button-counter", "cancel", 3, "click"], ["mat-flat-button", "", 1, "button-counter", "reset", 3, "click"], ["mat-flat-button", "", 1, "button-counter", "save", 3, "click"], [1, "divide-counters"], [1, "title-counter-container"], ["mat-stroked-button", "", 1, "add-counter-button", 3, "click"], [1, "form-counter", 3, "formGroup"], ["aria-label", "name", "placeholder", "Counter name", "matInput", "", "formControlName", "name"], ["aria-label", "description", "placeholder", "Counter description", "matInput", "", "formControlName", "description"], [1, "counter-list"], ["class", "counter-cards", 4, "ngFor", "ngForOf"], [1, "test-questions"], [1, "question"], [1, "question-statement"], [1, "question-step"], [1, "question-description"], [1, "question-response"], ["class", "radio-question", 4, "ngIf"], [1, "radio-question"], ["class", "one-answer", 4, "ngFor", "ngForOf"], [1, "one-answer"], [1, "answer-text"], ["disabled", "", 3, "value"], [1, "counter-inputs"], ["appearance", "outline", 1, "counter-input"], ["aria-label", "answer", "matInput", "", 3, "formControlName"], ["disabled", ""], [1, "counter-cards"], [1, "counter-card", 3, "ngStyle", "click"], [1, "counter-card-content"], [1, "counter-card-text"], [1, "counter-name"], [1, "counter-description"], [1, "counter-card-buttons"], ["mat-icon-button", "", 3, "click", 4, "ngIf"], ["mat-icon-button", "", 3, "click"]],
      template: function CreateCorrectionComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "note_add");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-card-content");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "ID: ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Description: ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "form", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, CreateCorrectionComponent_div_19_Template, 17, 6, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "mat-card", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "h3", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Correction ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "span", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "note_add");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "form", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "input", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "mat-error");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Enter a name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "textarea", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "button", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateCorrectionComponent_Template_button_click_33_listener() {
            return ctx.onCorrectCancel();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, " Cancel >> ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "button", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateCorrectionComponent_Template_button_click_35_listener() {
            return ctx.onCorrectionReset();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, " Reset>> ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "button", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateCorrectionComponent_Template_button_click_37_listener() {
            return ctx.onCorrectionSave();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, " Save>> ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "div", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h3", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Counters ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "span", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "timeline");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "button", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateCorrectionComponent_Template_button_click_45_listener() {
            return ctx.addCounter();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "span", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "add");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, " Add counter ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "form", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "input", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "mat-error");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Enter a name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "textarea", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](57, CreateCorrectionComponent_div_57_Template, 14, 5, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.test.name);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.test.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.test.description);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.counterForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.test.questions);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.correctionForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.counterForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.counters);
        }
      },
      directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_7__["MatCard"], _angular_material_card__WEBPACK_IMPORTED_MODULE_7__["MatCardContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatFormField"], _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_9__["MatError"], _angular_material_button__WEBPACK_IMPORTED_MODULE_11__["MatButton"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_12__["MatRadioGroup"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_12__["MatRadioButton"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_13__["MatCheckbox"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgStyle"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_14__["MatIcon"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n.new-correction-container[_ngcontent-%COMP%] {\n  display: flex;\n  width: 100%;\n}\n.new-correction-component[_ngcontent-%COMP%] {\n  margin-left: 3em;\n  margin-right: 3em;\n  margin-bottom: 3em;\n  width: 60%;\n}\n.counter-list-container[_ngcontent-%COMP%] {\n  margin-bottom: 3em;\n  width: 25%;\n}\n.add-counter-button[_ngcontent-%COMP%] {\n  border-radius: 24px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  margin: 1.6rem;\n}\n.title-counter-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n}\n@media (max-width: 1750px) {\n  .new-correction-component[_ngcontent-%COMP%] {\n    margin-left: 3em;\n    margin-right: 3em;\n  }\n}\n.test-description[_ngcontent-%COMP%] {\n  font-size: 15px;\n  margin-bottom: 20px;\n}\n.test-description[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.test-description[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  padding: 5px 0;\n}\n.question[_ngcontent-%COMP%] {\n  padding: 10px 0;\n}\n.question[_ngcontent-%COMP%]   .question-statement[_ngcontent-%COMP%]    > span[_ngcontent-%COMP%] {\n  font-size: 17px;\n  font-weight: 600;\n}\n.question[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  padding: 2px 0;\n}\n.question-step[_ngcontent-%COMP%] {\n  margin-top: 10px;\n  margin-left: 20px;\n}\n.question-step[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.question-description[_ngcontent-%COMP%] {\n  margin-top: 10px;\n  margin-left: 20px;\n}\n.question-description[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.question-response[_ngcontent-%COMP%] {\n  padding: 5px 0;\n  margin-top: 15px;\n}\n.question-response[_ngcontent-%COMP%]   .one-answer[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n}\n.question-response[_ngcontent-%COMP%]   .one-answer[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n  text-align: center;\n  max-width: 5em;\n  font-size: 0.8em;\n}\n.mat-form-field-infix[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.mat-form-field-infix[_ngcontent-%COMP%]   .ng-tns-c84-5[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.answer-text[_ngcontent-%COMP%] {\n  display: flex;\n  padding: 5px;\n  margin-left: 50px;\n  align-items: center;\n}\n.sirculo[_ngcontent-%COMP%] {\n  height: 15px;\n  width: 15px;\n  border: 1px solid;\n  border-radius: 20px;\n}\n\n.correction-panel[_ngcontent-%COMP%], .form-counter[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n}\n.correction-panel[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%], .form-counter[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n  width: 84%;\n  margin: auto;\n}\n@media (max-width: 1750px) {\n  .correction-panel[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%], .form-counter[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n    width: 79%;\n  }\n}\n.counter-list-buttons[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-around;\n  margin: 40px 0 20px 0;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .button-counter[_ngcontent-%COMP%] {\n  border-radius: 25px;\n  color: white;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .save[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .cancel[_ngcontent-%COMP%] {\n  background-color: #767676;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .reset[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.divide-counters[_ngcontent-%COMP%] {\n  height: 30px;\n  border-bottom: 1px solid lightgrey;\n  margin: auto;\n}\n.title-counters[_ngcontent-%COMP%] {\n  display: flex;\n  margin-left: 30px;\n  margin-top: 35px !important;\n}\n.title-counters[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%] {\n  margin-left: 10px;\n}\n.counter-card[_ngcontent-%COMP%] {\n  margin: 15px;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%] {\n  display: flex;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-text[_ngcontent-%COMP%] {\n  width: 70%;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-text[_ngcontent-%COMP%]   .counter-name[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-buttons[_ngcontent-%COMP%] {\n  width: 30%;\n  display: flex;\n  flex-wrap: wrap;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-buttons[_ngcontent-%COMP%]   .save-icon[_ngcontent-%COMP%] {\n  color: #DE7373;\n}\n.counter-inputs[_ngcontent-%COMP%] {\n  display: flex;\n}\n.counter-inputs[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%] {\n  font-size: 22px;\n  margin-top: 13px;\n  color: #989898;\n}\n.counter-list[_ngcontent-%COMP%]   mat-card[_ngcontent-%COMP%] {\n  transition: transform 0.5s, background-color 0.2s;\n  cursor: pointer;\n}\n.counter-list[_ngcontent-%COMP%]   mat-card[_ngcontent-%COMP%]:hover {\n  transform: scale(0.95);\n  background-color: #eeeeee;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2NyZWF0ZS1jb3JyZWN0aW9uL0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhc3NldHNcXHN0eWxlc1xcc3RhZmZpdC1zdHlsZS0xLnNjc3MiLCJzcmMvYXBwL2V2YWx1YXRpb25zL3Rlc3QvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9jcmVhdGUtY29ycmVjdGlvbi9jcmVhdGUtY29ycmVjdGlvbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2NyZWF0ZS1jb3JyZWN0aW9uL0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhcHBcXGV2YWx1YXRpb25zXFx0ZXN0XFxpbmZyYXN0cnVjdHVyZVxcbmctY29tcG9uZW50c1xcY3JlYXRlLWNvcnJlY3Rpb25cXGNyZWF0ZS1jb3JyZWN0aW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9DQUFBO0FBUUEsd0JBQUE7QUFRQSx1QkFBQTtBQUdBLDJCQUFBO0FBRUE7RUFDRSw2QkFBQTtBQ2hCRjtBRGtCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDaEJKO0FEa0JJO0VBQ0UsbUJBM0JRO0VBNEJSLGdCQUFBO0VBQ0EsY0FBQTtBQ2hCTjtBRHNCQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQ25CRjtBRHFCRTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDbkJKO0FEcUJJO0VBQ0UsWUFBQTtBQ25CTjtBRHNCRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtBQ3BCSjtBRHdCQTtFQUNFLDZCQUFBO0VBQ0EsZUFBQTtBQ3JCRjtBRHVCRTtFQUNFLFNBQUE7QUNyQko7QUR3QkU7RUFDRSxnQ0FBQTtBQ3RCSjtBRHlCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDdkJKO0FEeUJJO0VBQ0UsbUJBM0VRO0VBNEVSLGdCQUFBO0VBQ0EsY0FBQTtBQ3ZCTjtBRDBCSTtFQUNFLHlCQTVFaUI7RUE2RWpCLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUN4Qk47QUQ2QkE7RUFDRSxjQWxGbUI7QUN3RHJCO0FENkJBO0VBQ0UsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDMUJGO0FENkJBO0VBQ0UsV0FBQTtBQzFCRjtBRDZCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQzFCRjtBRDZCQTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUMxQkY7QUQ0QkU7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUMxQko7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtBQzNCRjtBRDhCQTtFQUNFLGVBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQzNCRjtBRGtDQTtFQUNFLGNBQUE7QUMvQkY7QURrQ0EseUJBQUE7QUFFQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbURBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0VBQ0EseUJBQUE7QUNoQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UsY0FBQTtBQ2xDRjtBRHFDQSxZQUFBO0FBRUE7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLHVCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsZUFBQTtBQ25DRjtBRHNDQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsMENBQUE7QUNuQ0Y7QUNqTUE7RUFDSSxhQUFBO0VBQ0EsV0FBQTtBRG9NSjtBQ2pNQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QURvTUo7QUNqTUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7QURvTUo7QUNqTUE7RUFDSSxtQkFBQTtFQUNBLDJCQUFBO0VBQUEsd0JBQUE7RUFBQSxtQkFBQTtFQUNBLGNBQUE7QURvTUo7QUNqTUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLDhCQUFBO0FEb01KO0FDak1BO0VBQ0k7SUFDSSxnQkFBQTtJQUNBLGlCQUFBO0VEb01OO0FBQ0Y7QUNqTUE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QURtTUo7QUNsTUk7RUFDSSxnQkFBQTtBRG9NUjtBQ2xNSTtFQUNJLGNBQUE7QURvTVI7QUNoTUE7RUFDSSxlQUFBO0FEbU1KO0FDak1RO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FEbU1aO0FDL0xJO0VBQ0ksY0FBQTtBRGlNUjtBQzdMQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7QURnTUo7QUM5TEk7RUFDSSxnQkFBQTtBRGdNUjtBQzVMQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7QUQrTEo7QUM5TEk7RUFDSSxnQkFBQTtBRGdNUjtBQzVMQTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtBRCtMSjtBQzlMSTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtBRGdNUjtBQzlMUTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FEZ01aO0FDM0xBO0VBQ0ksVUFBQTtBRDhMSjtBQzNMQTtFQUNJLFVBQUE7QUQ4TEo7QUMzTEE7RUFDSSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUQ4TEo7QUMzTEE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUQ4TEo7QUMxTEE7O0NBQUE7QUFJQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtBRDRMSjtBQzNMSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0FENkxSO0FDMUxJO0VBQ0k7SUFDSSxVQUFBO0VENExWO0FBQ0Y7QUN4TEE7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxxQkFBQTtBRDJMSjtBQzFMSTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtBRDRMUjtBQzFMSTtFQUNJLHlCQUFBO0FENExSO0FDMUxJO0VBQ0kseUJBQUE7QUQ0TFI7QUMxTEk7RUFDSSx5QkFBQTtBRDRMUjtBQ3hMQTtFQUNJLFlBQUE7RUFDQSxrQ0FBQTtFQUNBLFlBQUE7QUQyTEo7QUN4TEE7RUFDSSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSwyQkFBQTtBRDJMSjtBQzFMSTtFQUNJLGlCQUFBO0FENExSO0FDeExBO0VBQ0ksWUFBQTtBRDJMSjtBQ3pMSTtFQUNJLGFBQUE7QUQyTFI7QUN6TFE7RUFDSSxVQUFBO0FEMkxaO0FDMUxZO0VBQ0ksZ0JBQUE7QUQ0TGhCO0FDekxRO0VBQ0ksVUFBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0FEMkxaO0FDMUxZO0VBQ0ksY0FBQTtBRDRMaEI7QUN2TEE7RUFDSSxhQUFBO0FEMExKO0FDekxJO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBRDJMUjtBQ3RMSTtFQUNJLGlEQUFBO0VBRUEsZUFBQTtBRHdMUjtBQ3RMSTtFQUdJLHNCQUFBO0VBQ0EseUJBQUE7QUR3TFIiLCJmaWxlIjoic3JjL2FwcC9ldmFsdWF0aW9ucy90ZXN0L2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvY3JlYXRlLWNvcnJlY3Rpb24vY3JlYXRlLWNvcnJlY3Rpb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiA9PT09PSBGT05UUyAoTWFqb3IgVGhpcmQpID09PT09ICovXHJcbiRzbWFsbDogMC44cmVtO1xyXG4kbWVkaXVtOiAxcmVtO1xyXG4kbGFyZ2U6IDEuMjVyZW07XHJcbiRleHRyYS1sYXJnZTogMS41NjNyZW07XHJcbiRodWdlOiAxLjk1M3JlbTtcclxuJGV4dHJhLWh1Z2U6IDIuNDQxcmVtO1xyXG5cclxuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xyXG4kY29sb3ItcGFsZXR0ZS0xLW1haW46ICNERTczNzM7XHJcbiRjb2xvci1wYWxldHRlLTEtc2Vjb25kOiAjQUJBQkZGO1xyXG4kY29sb3ItcHJpbWFyeS1saWdodDogI0VGRUZFRjtcclxuJGNvbG9yLXByaW1hcnktZGFyazogIzE5MTkxOTtcclxuJGNvbG9yLXNlY29uZGFyeS1kYXJrOiAjMzMzMzMzO1xyXG4kY29sb3Itc2Nyb2xsYmFyLXRodW1iOiAjOEM4QzhDO1xyXG5cclxuLyogPT09PT0gV0VJR0hUID09PT09ICovXHJcblxyXG5cclxuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xyXG5cclxuLmMtY2FyZC1jb21wb25lbnQge1xyXG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5cclxuLmZpbHRlcnMtcGx1cy1jb25maWcge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1mbG93OiByb3c7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gIC5keW4tZmlsdGVycyB7XHJcbiAgICBmbGV4LWdyb3c6IDE7XHJcbiAgICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gICAgbWFyZ2luOiAwIDQwcHggMCAwO1xyXG5cclxuICAgIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XHJcbiAgICAgIHdpZHRoOiAxNTBweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmNvbmZpZ0J1dHRvbiB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgfVxyXG59XHJcblxyXG4uY2FyZC1jb21wb25lbnQge1xyXG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xyXG4gIG1heC1oZWlnaHQ6IDg4JTtcclxuXHJcbiAgLmNhcmQtY29udGVudCwgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgfVxyXG5cclxuICA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24ge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItcGFsZXR0ZS0xLW1haW47XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJyZW07XHJcbiAgICAgIGhlaWdodDogMi4zcmVtO1xyXG4gICAgICBjb2xvcjogI2VmZWZlZjtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XHJcbiAgY29sb3I6ICRjb2xvci1wcmltYXJ5LWRhcms7XHJcbn1cclxuXHJcbi5maWx0ZXIge1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbm1hdC1mb3JtLWZpZWxkIG1hdC1zZWxlY3Qge1xyXG4gIG1heC13aWR0aDogMTBlbTtcclxuICBtYXJnaW4tcmlnaHQ6IDVlbTtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmUge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgLy8gbWF4LWhlaWdodDogODAwcHg7XHJcbiAgLm1hdC10YWJsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IHRhYmxlO1xyXG4gIH1cclxufVxyXG5cclxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1yb3cge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcclxuICBjb2xvcjogIzhiOGI4YjtcclxufVxyXG5cclxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XHJcbiAgcGFkZGluZy1sZWZ0OiAwO1xyXG59XHJcblxyXG4uaWRIZWFkZXIge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmlkQ2VsbCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5tYXQtY2FyZC1jb250ZW50IHtcclxuICBtYXJnaW46IDAgMzRweDtcclxufVxyXG5cclxuLyogPT09PT0gUFJJT1JJVFkgPT09PT0gKi9cclxuXHJcbi5wcmlvcml0eSB7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgcGFkZGluZzogNXB4IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4vLyBAVE9ETzogY2hhbmdlIGNvbG9yIHRvIHZhcmlhYmxlc1xyXG5cclxuLnByaW9yaXR5LW11eWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1hbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbm9ybWFsIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xyXG59XHJcblxyXG4ucHJpb3JpdHktYmFqYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcclxufVxyXG5cclxuLnByaW9yaXR5LW5pbmd1bmEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi5yZWQtZGF0ZSB7XHJcbiAgY29sb3I6ICNlYjQxNDE7XHJcbn1cclxuXHJcbi8qIE1BVCBUQUIgKi9cclxuXHJcbjo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIHtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHNwYW4ge1xyXG4gIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcclxuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgLjEyKTtcclxufVxyXG4iLCIvKiA9PT09PSBGT05UUyAoTWFqb3IgVGhpcmQpID09PT09ICovXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cbi5jLWNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cblxuLmZpbHRlcnMtcGx1cy1jb25maWcge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIHtcbiAgZmxleC1ncm93OiAxO1xuICBvdmVyZmxvdy14OiBhdXRvO1xuICBtYXJnaW46IDAgNDBweCAwIDA7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDE1MHB4O1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmNvbmZpZ0J1dHRvbiB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGZsZXgtc2hyaW5rOiAwO1xufVxuXG4uY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbiAgbWF4LWhlaWdodDogODglO1xufVxuLmNhcmQtY29tcG9uZW50IC5jYXJkLWNvbnRlbnQsIC5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAwO1xufVxuLmNhcmQtY29tcG9uZW50IDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RFNzM3MztcbiAgYm9yZGVyLXJhZGl1czogMnJlbTtcbiAgaGVpZ2h0OiAyLjNyZW07XG4gIGNvbG9yOiAjZWZlZmVmO1xufVxuXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xuICBjb2xvcjogIzE5MTkxOTtcbn1cblxuLmZpbHRlciB7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbm1hdC1mb3JtLWZpZWxkIG1hdC1zZWxlY3Qge1xuICBtYXgtd2lkdGg6IDEwZW07XG4gIG1hcmdpbi1yaWdodDogNWVtO1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbn1cbi50YWJsZS1yZXNwb25zaXZlIC5tYXQtdGFibGUge1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiB0YWJsZTtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XG59XG5cbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XG59XG5cbi5tYXQtaGVhZGVyLXJvdyB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XG4gIGNvbG9yOiAjOGI4YjhiO1xufVxuXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcbiAgcGFkZGluZy1sZWZ0OiAwO1xufVxuXG4uaWRIZWFkZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uaWRDZWxsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbm1hdC1jYXJkLWNvbnRlbnQge1xuICBtYXJnaW46IDAgMzRweDtcbn1cblxuLyogPT09PT0gUFJJT1JJVFkgPT09PT0gKi9cbi5wcmlvcml0eSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA1cHggMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLnByaW9yaXR5LW11eWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xufVxuXG4ucHJpb3JpdHktYWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XG59XG5cbi5wcmlvcml0eS1ub3JtYWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4ucHJpb3JpdHktYmFqYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XG59XG5cbi5wcmlvcml0eS1uaW5ndW5hIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLnJlZC1kYXRlIHtcbiAgY29sb3I6ICNlYjQxNDE7XG59XG5cbi8qIE1BVCBUQUIgKi9cbjo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIHtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xuICBmb250LXNpemU6IDEzcHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHNwYW4ge1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn1cblxuLm5ldy1jb3JyZWN0aW9uLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubmV3LWNvcnJlY3Rpb24tY29tcG9uZW50IHtcbiAgbWFyZ2luLWxlZnQ6IDNlbTtcbiAgbWFyZ2luLXJpZ2h0OiAzZW07XG4gIG1hcmdpbi1ib3R0b206IDNlbTtcbiAgd2lkdGg6IDYwJTtcbn1cblxuLmNvdW50ZXItbGlzdC1jb250YWluZXIge1xuICBtYXJnaW4tYm90dG9tOiAzZW07XG4gIHdpZHRoOiAyNSU7XG59XG5cbi5hZGQtY291bnRlci1idXR0b24ge1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xuICBtYXJnaW46IDEuNnJlbTtcbn1cblxuLnRpdGxlLWNvdW50ZXItY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgZmxleC13cmFwOiBub3dyYXA7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDE3NTBweCkge1xuICAubmV3LWNvcnJlY3Rpb24tY29tcG9uZW50IHtcbiAgICBtYXJnaW4tbGVmdDogM2VtO1xuICAgIG1hcmdpbi1yaWdodDogM2VtO1xuICB9XG59XG4udGVzdC1kZXNjcmlwdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi50ZXN0LWRlc2NyaXB0aW9uIHNwYW4ge1xuICBmb250LXdlaWdodDogNTAwO1xufVxuLnRlc3QtZGVzY3JpcHRpb24gPiBkaXYge1xuICBwYWRkaW5nOiA1cHggMDtcbn1cblxuLnF1ZXN0aW9uIHtcbiAgcGFkZGluZzogMTBweCAwO1xufVxuLnF1ZXN0aW9uIC5xdWVzdGlvbi1zdGF0ZW1lbnQgPiBzcGFuIHtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLnF1ZXN0aW9uID4gZGl2IHtcbiAgcGFkZGluZzogMnB4IDA7XG59XG5cbi5xdWVzdGlvbi1zdGVwIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG4ucXVlc3Rpb24tc3RlcCBzcGFuIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLnF1ZXN0aW9uLWRlc2NyaXB0aW9uIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG4ucXVlc3Rpb24tZGVzY3JpcHRpb24gc3BhbiB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5xdWVzdGlvbi1yZXNwb25zZSB7XG4gIHBhZGRpbmc6IDVweCAwO1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuLnF1ZXN0aW9uLXJlc3BvbnNlIC5vbmUtYW5zd2VyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLnF1ZXN0aW9uLXJlc3BvbnNlIC5vbmUtYW5zd2VyIG1hdC1mb3JtLWZpZWxkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXgtd2lkdGg6IDVlbTtcbiAgZm9udC1zaXplOiAwLjhlbTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkLWluZml4IHtcbiAgcGFkZGluZzogMDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkLWluZml4IC5uZy10bnMtYzg0LTUge1xuICBwYWRkaW5nOiAwO1xufVxuXG4uYW5zd2VyLXRleHQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uc2lyY3VsbyB7XG4gIGhlaWdodDogMTVweDtcbiAgd2lkdGg6IDE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xufVxuXG4vKlxuICAgIENPVU5URVJTXG4qL1xuLmNvcnJlY3Rpb24tcGFuZWwsIC5mb3JtLWNvdW50ZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuLmNvcnJlY3Rpb24tcGFuZWwgbWF0LWZvcm0tZmllbGQsIC5mb3JtLWNvdW50ZXIgbWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogODQlO1xuICBtYXJnaW46IGF1dG87XG59XG5AbWVkaWEgKG1heC13aWR0aDogMTc1MHB4KSB7XG4gIC5jb3JyZWN0aW9uLXBhbmVsIG1hdC1mb3JtLWZpZWxkLCAuZm9ybS1jb3VudGVyIG1hdC1mb3JtLWZpZWxkIHtcbiAgICB3aWR0aDogNzklO1xuICB9XG59XG5cbi5jb3VudGVyLWxpc3QtYnV0dG9ucyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICBtYXJnaW46IDQwcHggMCAyMHB4IDA7XG59XG4uY291bnRlci1saXN0LWJ1dHRvbnMgLmJ1dHRvbi1jb3VudGVyIHtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmNvdW50ZXItbGlzdC1idXR0b25zIC5zYXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RFNzM3Mztcbn1cbi5jb3VudGVyLWxpc3QtYnV0dG9ucyAuY2FuY2VsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzc2NzY3Njtcbn1cbi5jb3VudGVyLWxpc3QtYnV0dG9ucyAucmVzZXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4uZGl2aWRlLWNvdW50ZXJzIHtcbiAgaGVpZ2h0OiAzMHB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmV5O1xuICBtYXJnaW46IGF1dG87XG59XG5cbi50aXRsZS1jb3VudGVycyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICBtYXJnaW4tdG9wOiAzNXB4ICFpbXBvcnRhbnQ7XG59XG4udGl0bGUtY291bnRlcnMgLm1hdGVyaWFsLWljb25zIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG5cbi5jb3VudGVyLWNhcmQge1xuICBtYXJnaW46IDE1cHg7XG59XG4uY291bnRlci1jYXJkIC5jb3VudGVyLWNhcmQtY29udGVudCB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uY291bnRlci1jYXJkIC5jb3VudGVyLWNhcmQtY29udGVudCAuY291bnRlci1jYXJkLXRleHQge1xuICB3aWR0aDogNzAlO1xufVxuLmNvdW50ZXItY2FyZCAuY291bnRlci1jYXJkLWNvbnRlbnQgLmNvdW50ZXItY2FyZC10ZXh0IC5jb3VudGVyLW5hbWUge1xuICBmb250LXdlaWdodDogNTAwO1xufVxuLmNvdW50ZXItY2FyZCAuY291bnRlci1jYXJkLWNvbnRlbnQgLmNvdW50ZXItY2FyZC1idXR0b25zIHtcbiAgd2lkdGg6IDMwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xufVxuLmNvdW50ZXItY2FyZCAuY291bnRlci1jYXJkLWNvbnRlbnQgLmNvdW50ZXItY2FyZC1idXR0b25zIC5zYXZlLWljb24ge1xuICBjb2xvcjogI0RFNzM3Mztcbn1cblxuLmNvdW50ZXItaW5wdXRzIHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5jb3VudGVyLWlucHV0cyAubWF0ZXJpYWwtaWNvbnMge1xuICBmb250LXNpemU6IDIycHg7XG4gIG1hcmdpbi10b3A6IDEzcHg7XG4gIGNvbG9yOiAjOTg5ODk4O1xufVxuXG4uY291bnRlci1saXN0IG1hdC1jYXJkIHtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMsIGJhY2tncm91bmQtY29sb3IgMC4ycztcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmNvdW50ZXItbGlzdCBtYXQtY2FyZDpob3ZlciB7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwLjk1KTtcbiAgLW1zLXRyYW5zZm9ybTogc2NhbGUoMC45NSk7XG4gIHRyYW5zZm9ybTogc2NhbGUoMC45NSk7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWVlZWU7XG59IiwiQGltcG9ydCAnLi4vLi4vLi4vLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9zdGFmZml0LXN0eWxlLTEnO1xyXG5cclxuXHJcbi5uZXctY29ycmVjdGlvbi1jb250YWluZXJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5uZXctY29ycmVjdGlvbi1jb21wb25lbnR7XHJcbiAgICBtYXJnaW4tbGVmdDozZW07XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDNlbTtcclxuICAgIG1hcmdpbi1ib3R0b206IDNlbTtcclxuICAgIHdpZHRoOiA2MCU7XHJcbn1cclxuXHJcbi5jb3VudGVyLWxpc3QtY29udGFpbmVye1xyXG4gICAgbWFyZ2luLWJvdHRvbTogM2VtO1xyXG4gICAgd2lkdGg6IDI1JTtcclxufVxyXG5cclxuLmFkZC1jb3VudGVyLWJ1dHRvbntcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBoZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gICAgbWFyZ2luOiAxLjZyZW07XHJcbn1cclxuXHJcbi50aXRsZS1jb3VudGVyLWNvbnRhaW5lcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuXHJcbkBtZWRpYSAobWF4LXdpZHRoOiAxNzUwcHgpIHtcclxuICAgIC5uZXctY29ycmVjdGlvbi1jb21wb25lbnR7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6M2VtO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogM2VtO1xyXG4gICAgfVxyXG59XHJcblxyXG4udGVzdC1kZXNjcmlwdGlvbntcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICBzcGFue1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbiAgICA+IGRpdntcclxuICAgICAgICBwYWRkaW5nOiA1cHggMDtcclxuICAgIH1cclxufVxyXG5cclxuLnF1ZXN0aW9ue1xyXG4gICAgcGFkZGluZzogMTBweCAwO1xyXG4gICAgLnF1ZXN0aW9uLXN0YXRlbWVudHtcclxuICAgICAgICA+c3BhbntcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICA+IGRpdntcclxuICAgICAgICBwYWRkaW5nOiAycHggMDtcclxuICAgIH1cclxufVxyXG5cclxuLnF1ZXN0aW9uLXN0ZXB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICBcclxuICAgIHNwYW57XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIH1cclxufVxyXG5cclxuLnF1ZXN0aW9uLWRlc2NyaXB0aW9ue1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgc3BhbntcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgfVxyXG59XHJcblxyXG4ucXVlc3Rpb24tcmVzcG9uc2V7XHJcbiAgICBwYWRkaW5nOiA1cHggMDtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAub25lLWFuc3dlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICBcclxuICAgICAgICBtYXQtZm9ybS1maWVsZHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXgtd2lkdGg6IDVlbTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAuOGVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4e1xyXG4gICAgcGFkZGluZzogMDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4IC5uZy10bnMtYzg0LTV7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG4uYW5zd2VyLXRleHR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uc2lyY3Vsb3tcclxuICAgIGhlaWdodDogMTVweDtcclxuICAgIHdpZHRoOiAxNXB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG59XHJcblxyXG5cclxuLypcclxuICAgIENPVU5URVJTXHJcbiovXHJcblxyXG4uY29ycmVjdGlvbi1wYW5lbCwgLmZvcm0tY291bnRlcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgbWF0LWZvcm0tZmllbGR7XHJcbiAgICAgICAgd2lkdGg6IDg0JTtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICB9XHJcblxyXG4gICAgQG1lZGlhKG1heC13aWR0aDogMTc1MHB4KXtcclxuICAgICAgICBtYXQtZm9ybS1maWVsZHtcclxuICAgICAgICAgICAgd2lkdGg6IDc5JTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb3VudGVyLWxpc3QtYnV0dG9uc3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIG1hcmdpbjogNDBweCAwIDIwcHggMDtcclxuICAgIC5idXR0b24tY291bnRlcntcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuICAgIC5zYXZle1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNERTczNzM7XHJcbiAgICB9XHJcbiAgICAuY2FuY2Vse1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM3Njc2NzY7XHJcbiAgICB9XHJcbiAgICAucmVzZXR7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcclxuICAgIH1cclxufVxyXG5cclxuLmRpdmlkZS1jb3VudGVyc3tcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyZXk7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbn1cclxuXHJcbi50aXRsZS1jb3VudGVyc3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXJnaW4tbGVmdDogMzBweDtcclxuICAgIG1hcmdpbi10b3A6IDM1cHggIWltcG9ydGFudDtcclxuICAgIC5tYXRlcmlhbC1pY29uc3tcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIH1cclxufVxyXG5cclxuLmNvdW50ZXItY2FyZHtcclxuICAgIG1hcmdpbjogMTVweDtcclxuXHJcbiAgICAuY291bnRlci1jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuXHJcbiAgICAgICAgLmNvdW50ZXItY2FyZC10ZXh0e1xyXG4gICAgICAgICAgICB3aWR0aDogNzAlO1xyXG4gICAgICAgICAgICAuY291bnRlci1uYW1le1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuY291bnRlci1jYXJkLWJ1dHRvbnN7XHJcbiAgICAgICAgICAgIHdpZHRoOiAzMCU7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDs7XHJcbiAgICAgICAgICAgIC5zYXZlLWljb257XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogI0RFNzM3MztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4uY291bnRlci1pbnB1dHN7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLm1hdGVyaWFsLWljb25ze1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxM3B4O1xyXG4gICAgICAgIGNvbG9yOiAjOTg5ODk4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uY291bnRlci1saXN0e1xyXG4gICAgbWF0LWNhcmR7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMsXHJcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvciAwLjJzO1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIH1cclxuICAgIG1hdC1jYXJkOmhvdmVye1xyXG4gICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwLjk1KTtcclxuICAgICAgICAtbXMtdHJhbnNmb3JtOiBzY2FsZSgwLjk1KTtcclxuICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyMzgsIDIzOCwgMjM4KTtcclxuICAgIH1cclxufVxyXG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CreateCorrectionComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-create-correction',
          templateUrl: './create-correction.component.html',
          styleUrls: ['./create-correction.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_5__["CorrectionService"]
        }, {
          type: _correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_6__["CorrectionMapper"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/ng-components/edit-correction/edit-correction.component.ts":
  /*!************************************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/ng-components/edit-correction/edit-correction.component.ts ***!
    \************************************************************************************************************/

  /*! exports provided: EditCorrectionComponent */

  /***/
  function srcAppEvaluationsTestInfrastructureNgComponentsEditCorrectionEditCorrectionComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditCorrectionComponent", function () {
      return EditCorrectionComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var src_app_evaluations_correction_application_getCorrection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/evaluations/correction/application/getCorrection */
    "./src/app/evaluations/correction/application/getCorrection.ts");
    /* harmony import */


    var src_app_evaluations_correction_application_updateCorrection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/evaluations/correction/application/updateCorrection */
    "./src/app/evaluations/correction/application/updateCorrection.ts");
    /* harmony import */


    var src_app_evaluations_test_application_getTests__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/evaluations/test/application/getTests */
    "./src/app/evaluations/test/application/getTests.ts");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/evaluations/correction/infrastructure/services/correction.service */
    "./src/app/evaluations/correction/infrastructure/services/correction.service.ts");
    /* harmony import */


    var src_app_evaluations_correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/evaluations/correction/infrastructure/CorrectionMapper */
    "./src/app/evaluations/correction/infrastructure/CorrectionMapper.ts");
    /* harmony import */


    var src_app_evaluations_test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/evaluations/test/infrastructure/services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/progress-bar */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-bar.js");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
    /* harmony import */


    var _angular_material_radio__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/material/radio */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
    /* harmony import */


    var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! @angular/material/checkbox */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");
    /* harmony import */


    var _shared_custom_mat_elements_ckeditor_ckeditor_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/ckeditor/ckeditor.component */
    "./src/app/shared/custom-mat-elements/ckeditor/ckeditor.component.ts");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");

    function EditCorrectionComponent_mat_progress_bar_0_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "mat-progress-bar", 2);
      }
    }

    function EditCorrectionComponent_div_1_div_18_div_15_div_1_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-radio-button", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", answer_r9.value, " ", answer_r9.message, "");
      }
    }

    function EditCorrectionComponent_div_1_div_18_div_15_div_1_div_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-checkbox", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", answer_r9.value, " ", answer_r9.message, "");
      }
    }

    function EditCorrectionComponent_div_1_div_18_div_15_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, EditCorrectionComponent_div_1_div_18_div_15_div_1_div_1_Template, 3, 2, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, EditCorrectionComponent_div_1_div_18_div_15_div_1_div_2_Template, 3, 2, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-form-field", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "input", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r9 = ctx.$implicit;

        var question_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r4.questionType === "RADIO");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r4.questionType === "CHECKBOX");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("formControlName", answer_r9.id);
      }
    }

    function EditCorrectionComponent_div_1_div_18_div_15_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, EditCorrectionComponent_div_1_div_18_div_15_div_1_Template, 8, 3, "div", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", question_r4.answers);
      }
    }

    function EditCorrectionComponent_div_1_div_18_div_16_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-ckeditor", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", true);
      }
    }

    function EditCorrectionComponent_div_1_div_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Step: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Description: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, EditCorrectionComponent_div_1_div_18_div_15_Template, 2, 1, "div", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, EditCorrectionComponent_div_1_div_18_div_16_Template, 2, 1, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r4 = ctx.$implicit;
        var i_r5 = ctx.index;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", i_r5 + 1, ". ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](question_r4.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](question_r4.step);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", question_r4.description, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r4.questionType === "RADIO" || question_r4.questionType === "CHECKBOX");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r4.questionType === "TEXT");
      }
    }

    function EditCorrectionComponent_div_1_div_58_button_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditCorrectionComponent_div_1_div_58_button_9_Template_button_click_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r22);

          var counter_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r21.confirmEditCounter($event, counter_r16);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "done");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditCorrectionComponent_div_1_div_58_button_10_Template(rf, ctx) {
      if (rf & 1) {
        var _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditCorrectionComponent_div_1_div_58_button_10_Template_button_click_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25);

          var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r24.cancelEditCounter($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "clear");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditCorrectionComponent_div_1_div_58_button_11_Template(rf, ctx) {
      if (rf & 1) {
        var _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditCorrectionComponent_div_1_div_58_button_11_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

          var counter_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r26.removeCounter(counter_r16, "current");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "delete");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditCorrectionComponent_div_1_div_58_Template(rf, ctx) {
      if (rf & 1) {
        var _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditCorrectionComponent_div_1_div_58_Template_div_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r30);

          var counter_r16 = ctx.$implicit;
          var i_r17 = ctx.index;

          var ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r29.editCounter(counter_r16, i_r17);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, EditCorrectionComponent_div_1_div_58_button_9_Template, 3, 0, "button", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, EditCorrectionComponent_div_1_div_58_button_10_Template, 3, 0, "button", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, EditCorrectionComponent_div_1_div_58_button_11_Template, 3, 0, "button", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var counter_r16 = ctx.$implicit;
        var i_r17 = ctx.index;

        var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](counter_r16.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](counter_r16.description);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", i_r17 === ctx_r3.counterIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", i_r17 === ctx_r3.counterIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", i_r17 === ctx_r3.counterIndex);
      }
    }

    function EditCorrectionComponent_div_1_Template(rf, ctx) {
      if (rf & 1) {
        var _r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h2");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](5, "uppercase");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-card-content");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "ID: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Description: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "form", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, EditCorrectionComponent_div_1_div_18_Template, 17, 6, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "mat-card", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "h3", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Correction ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "span", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "note_add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "strong");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Name: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "p", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "strong");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Description: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "button", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditCorrectionComponent_div_1_Template_button_click_34_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r32);

          var ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r31.onCorrectCancel();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, " Cancel ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "button", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditCorrectionComponent_div_1_Template_button_click_36_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r32);

          var ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r33.onCorrectionReset();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, " Reset ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "button", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditCorrectionComponent_div_1_Template_button_click_38_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r32);

          var ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r34.onCorrectionSave();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Save ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h3", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Counters ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "span", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "timeline");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "button", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditCorrectionComponent_div_1_Template_button_click_46_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r32);

          var ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r35.addCounter();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, " Add counter ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "span", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "form", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "mat-form-field");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "input", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Enter a name");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "mat-form-field");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "textarea", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](58, EditCorrectionComponent_div_1_div_58_Template, 12, 5, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](5, 9, ctx_r1.test.name));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r1.test.id);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r1.test.description);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r1.counterForm);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.test.questions);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r1.correction.name, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r1.correction.description, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r1.counterForm);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.correction.counters);
      }
    }

    var defaultTest = {
      name: '',
      description: '',
      questions: []
    };
    var defaultCorrection = {
      name: '',
      description: '',
      counters: [],
      idTest: ''
    };

    var EditCorrectionComponent = /*#__PURE__*/function () {
      function EditCorrectionComponent(_fb, _route, _router, _service, _mapper, _correctionService, _testService) {
        _classCallCheck(this, EditCorrectionComponent);

        this._fb = _fb;
        this._route = _route;
        this._router = _router;
        this._service = _service;
        this._mapper = _mapper;
        this._correctionService = _correctionService;
        this._testService = _testService;
        this.test = defaultTest;
        this.counters = [];
        this.counterIndex = -1;
        this.loading = true;
        this.disabledInput = true;

        var nav = this._router.getCurrentNavigation();

        if (nav) {
          var state = nav.extras.state;
          this.test = state ? state.test : defaultTest;
          this.correction = state ? state.correction : defaultCorrection;
        } else {
          this.test = defaultTest;
          this.correction = defaultCorrection;
        }
      }

      _createClass(EditCorrectionComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this27 = this;

          this._route.params.subscribe(function (p) {
            Object(src_app_evaluations_test_application_getTests__WEBPACK_IMPORTED_MODULE_4__["getTest"])(p.idTest, _this27._testService).subscribe(function (test) {
              _this27.test = test;
              Object(src_app_evaluations_correction_application_getCorrection__WEBPACK_IMPORTED_MODULE_2__["getCorrection"])(p.id, _this27._correctionService).subscribe(function (correction) {
                _this27.correction = correction;
                console.log('correction', _this27.correction);
                _this27.loading = false;

                _this27._initCounterForm();
              });
            });
          });
        }
      }, {
        key: "_initCounterForm",
        value: function _initCounterForm(counter) {
          var _this28 = this;

          var config = {
            name: [counter ? counter.name : '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [counter ? counter.description : '']
          };
          this.test.questions.forEach(function (question) {
            question.answers.forEach(function (answer) {
              config[answer.id] = [{
                value: '0',
                disabled: _this28.disabledInput
              }];
            });
          });
          this.counterForm = this._fb.group(config);

          if (counter) {
            counter.answerCounters.forEach(function (ac) {
              _this28.counterForm.get(ac.idAnswer).setValue(ac.calcValue);
            });
          }
        }
      }, {
        key: "addCounter",
        value: function addCounter() {
          if (this.counterForm.valid) {
            var counter = this._formatCounter();

            this.correction.counters.push(counter);

            this._initCounterForm();
          }
        }
      }, {
        key: "_formatCounter",
        value: function _formatCounter() {
          var _this29 = this;

          var _this$counterForm$val2 = this.counterForm.value,
              name = _this$counterForm$val2.name,
              description = _this$counterForm$val2.description;
          var keys = Object.keys(this.counterForm.value);
          var answersCounters = [];
          keys.forEach(function (key) {
            if (key !== 'name' && key !== 'description') {
              answersCounters.push({
                calcValue: Number.parseInt(_this29.counterForm.value[key]),
                idAnswer: key
              });
            }
          });
          return {
            name: name,
            description: description,
            answerCounters: answersCounters
          };
        }
      }, {
        key: "editCounter",
        value: function editCounter(counter, index) {
          this.disabledInput = false;
          this.counterIndex = index;

          this._initCounterForm(counter);
        }
      }, {
        key: "confirmEditCounter",
        value: function confirmEditCounter(event, counter) {
          event.stopPropagation();

          if (this.counterForm.valid) {
            var updatedCounter = this._formatCounter();

            if (counter.id) {
              updatedCounter.answerCounters.forEach(function (ac) {
                var c = counter.answerCounters.find(function (cac) {
                  return cac.idAnswer === ac.idAnswer;
                });

                if (c) {
                  c.calcValue = ac.calcValue;
                } else {
                  var answerCounter = {
                    idAnswer: ac.idAnswer,
                    calcValue: ac.calcValue
                  };
                  counter.answerCounters.push(answerCounter);
                }
              });
            } else {
              counter = updatedCounter;
            }

            this.counterIndex = -1;

            this._disableInputs();
          }
        }
      }, {
        key: "_disableInputs",
        value: function _disableInputs() {
          this.counterForm.disable();
          this.counterForm.get('name').enable();
          this.counterForm.get('description').enable();
        }
      }, {
        key: "cancelEditCounter",
        value: function cancelEditCounter(event) {
          event.stopPropagation();

          this._initCounterForm();

          this.counterIndex = -1;

          this._disableInputs();
        }
      }, {
        key: "removeCounter",
        value: function removeCounter(counter, type) {
          var _this30 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_5__["questionAlert"])('This counter will be deleted').then(function (result) {
            if (result.isConfirmed) {
              if (type === 'new') {
                _this30.correction.counters = _this30.correction.counters.filter(function (c) {
                  return c.id !== counter.id;
                });
                _this30.counterIndex = -1;
              } else {
                _this30.correction.counters = _this30.correction.counters.filter(function (c) {
                  return c.id !== counter.id;
                });
                _this30.counterIndex = -1;
              }

              _this30.disabledInput = true;

              _this30._disableInputs();
            }
          });
        }
      }, {
        key: "onCorrectionReset",
        value: function onCorrectionReset() {
          var _this31 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_5__["questionAlert"])('All your changes will be discarted!').then(function (result) {
            if (result.isConfirmed) {
              var counter = _this31.correction.counters[_this31.counterIndex];

              _this31._initCounterForm(counter);
            }
          });
        }
      }, {
        key: "onCorrectCancel",
        value: function onCorrectCancel() {
          var _this32 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_5__["questionAlert"])("Your correction won't be saved! Please consider saving first").then(function (result) {
            if (result.isConfirmed) {
              _this32._router.navigate(['../../../..'], {
                relativeTo: _this32._route
              });
            }
          });
        }
      }, {
        key: "onCorrectionSave",
        value: function onCorrectionSave() {
          var _this33 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_5__["questionAlert"])('Once sent you can update it').then(function (result) {
            if (result.isConfirmed) {
              var correction = {
                name: _this33.correction.name,
                description: _this33.correction.description,
                counters: _toConsumableArray(_this33.correction.counters),
                idTest: _this33.test.id,
                id: _this33.correction.id
              };
              Object(src_app_evaluations_correction_application_updateCorrection__WEBPACK_IMPORTED_MODULE_3__["updateCorrection"])(correction, _this33._service, _this33._mapper).subscribe(function (correction) {
                //TODO think about what to do after success
                Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_5__["successAlert"])('Correction was updated successfully');
              });
            }
          });
        }
      }]);

      return EditCorrectionComponent;
    }();

    EditCorrectionComponent.ɵfac = function EditCorrectionComponent_Factory(t) {
      return new (t || EditCorrectionComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_7__["CorrectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_8__["CorrectionMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_7__["CorrectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_9__["TestService"]));
    };

    EditCorrectionComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: EditCorrectionComponent,
      selectors: [["app-edit-correction"]],
      decls: 2,
      vars: 2,
      consts: [["mode", "indeterminate", 4, "ngIf"], ["class", "new-correction-container", 4, "ngIf"], ["mode", "indeterminate"], [1, "new-correction-container"], [1, "new-correction-component"], [1, "title-component"], [1, "test-body"], [1, "test-description"], [1, "text-id"], [1, "text-description"], [3, "formGroup"], ["class", "test-questions", 4, "ngFor", "ngForOf"], [1, "counter-list-container"], [1, "title-counters"], [1, "material-icons"], [1, "correction"], [1, "correction-name"], [1, "correction-description"], [1, "counter-list-buttons"], ["mat-flat-button", "", 1, "button-counter", "cancel", 3, "click"], ["mat-flat-button", "", 1, "button-counter", "reset", 3, "click"], ["mat-flat-button", "", 1, "button-counter", "save", 3, "click"], [1, "divide-counters"], [1, "title-counter-container"], ["mat-stroked-button", "", 1, "add-counter-button", 3, "click"], [1, "form-counter", 3, "formGroup"], ["aria-label", "name", "placeholder", "Counter name", "matInput", "", "formControlName", "name"], ["aria-label", "description", "placeholder", "Counter description", "matInput", "", "formControlName", "description"], [1, "counter-list"], ["class", "counter-cards", 3, "click", 4, "ngFor", "ngForOf"], [1, "test-questions"], [1, "question"], [1, "question-statement"], [1, "question-step"], [1, "question-description"], [3, "innerHTML"], [1, "question-response"], ["class", "radio-question", 4, "ngIf"], [4, "ngIf"], [1, "radio-question"], ["class", "one-answer", 4, "ngFor", "ngForOf"], [1, "one-answer"], ["class", "answer-text", 4, "ngIf"], [1, "counter-inputs"], ["appearance", "standard", 1, "counter-input"], ["matInput", "", "type", "number", "aria-label", "answer", 3, "formControlName"], [1, "answer-text"], ["value", "false", "disabled", ""], ["disabled", ""], [3, "disabled"], [1, "counter-cards", 3, "click"], [1, "counter-card"], [1, "counter-card-content"], [1, "counter-card-text"], [1, "counter-name"], [1, "counter-description"], [1, "counter-card-buttons"], ["mat-icon-button", "", 3, "click", 4, "ngIf"], ["mat-icon-button", "", 3, "click"]],
      template: function EditCorrectionComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, EditCorrectionComponent_mat_progress_bar_0_Template, 1, 0, "mat-progress-bar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, EditCorrectionComponent_div_1_Template, 59, 11, "div", 1);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_10__["NgIf"], _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_11__["MatProgressBar"], _angular_material_card__WEBPACK_IMPORTED_MODULE_12__["MatCard"], _angular_material_card__WEBPACK_IMPORTED_MODULE_12__["MatCardContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgForOf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_13__["MatButton"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__["MatFormField"], _angular_material_input__WEBPACK_IMPORTED_MODULE_15__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__["MatError"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NumberValueAccessor"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_16__["MatRadioButton"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_17__["MatCheckbox"], _shared_custom_mat_elements_ckeditor_ckeditor_component__WEBPACK_IMPORTED_MODULE_18__["CkeditorComponent"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_19__["MatIcon"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_10__["UpperCasePipe"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n.title-component[_ngcontent-%COMP%] {\n  margin-left: 2rem;\n}\n.new-correction-container[_ngcontent-%COMP%] {\n  display: flex;\n  width: 100%;\n}\n.new-correction-component[_ngcontent-%COMP%] {\n  margin-left: 3em;\n  margin-right: 3em;\n  margin-bottom: 3em;\n  width: 60%;\n}\n.counter-list-container[_ngcontent-%COMP%] {\n  margin-bottom: 3em;\n  width: 25%;\n}\n.title-counter-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n}\n@media (max-width: 1750px) {\n  .new-correction-component[_ngcontent-%COMP%] {\n    margin-left: 3em;\n    margin-right: 3em;\n  }\n}\n.test-description[_ngcontent-%COMP%] {\n  font-size: 15px;\n  margin-bottom: 20px;\n}\n.test-description[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.test-description[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  padding: 5px 0;\n}\n.add-counter-button[_ngcontent-%COMP%] {\n  border-radius: 24px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  margin: 1.6rem;\n}\n.question[_ngcontent-%COMP%] {\n  padding: 10px 0;\n}\n.question[_ngcontent-%COMP%]   .question-statement[_ngcontent-%COMP%]    > span[_ngcontent-%COMP%] {\n  font-size: 17px;\n  font-weight: 600;\n}\n.question[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  padding: 2px 0;\n}\n.question-step[_ngcontent-%COMP%] {\n  margin-top: 10px;\n  margin-left: 20px;\n}\n.question-step[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.question-description[_ngcontent-%COMP%] {\n  margin-top: 10px;\n  margin-left: 20px;\n}\n.question-description[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.question-response[_ngcontent-%COMP%] {\n  padding: 5px 0;\n  margin-top: 15px;\n}\n.question-response[_ngcontent-%COMP%]   .one-answer[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n}\n.question-response[_ngcontent-%COMP%]   .one-answer[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n  text-align: center;\n  max-width: 5em;\n  font-size: 0.8em;\n}\n.mat-form-field-infix[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.mat-form-field-infix[_ngcontent-%COMP%]   .ng-tns-c84-5[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.answer-text[_ngcontent-%COMP%] {\n  display: flex;\n  padding: 5px;\n  margin-left: 50px;\n  align-items: center;\n}\n.sirculo[_ngcontent-%COMP%] {\n  height: 15px;\n  width: 15px;\n  border: 1px solid;\n  border-radius: 20px;\n}\n\n.correction-panel[_ngcontent-%COMP%], .form-counter[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n}\n.correction-panel[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%], .form-counter[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n  width: 84%;\n  margin: auto;\n}\n@media (max-width: 1750px) {\n  .correction-panel[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%], .form-counter[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n    width: 79%;\n  }\n}\n.counter-list-buttons[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-around;\n  margin: 40px 0 20px 0;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .button-counter[_ngcontent-%COMP%] {\n  border-radius: 25px;\n  color: white;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .save[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .cancel[_ngcontent-%COMP%] {\n  background-color: #767676;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .reset[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.divide-counters[_ngcontent-%COMP%] {\n  height: 30px;\n  border-bottom: 1px solid lightgrey;\n  margin: auto;\n}\n.title-counters[_ngcontent-%COMP%] {\n  display: flex;\n  margin-left: 0px;\n  margin-top: 35px !important;\n}\n.title-counters[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%] {\n  margin-left: 10px;\n}\n.counter-card[_ngcontent-%COMP%] {\n  margin: 15px;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%] {\n  display: flex;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-text[_ngcontent-%COMP%] {\n  width: 70%;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-text[_ngcontent-%COMP%]   .counter-name[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-buttons[_ngcontent-%COMP%] {\n  width: 45%;\n  display: flex;\n  flex-wrap: wrap;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-buttons[_ngcontent-%COMP%]   .save-icon[_ngcontent-%COMP%] {\n  color: #DE7373;\n}\n.counter-inputs[_ngcontent-%COMP%] {\n  display: flex;\n}\n.counter-inputs[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%] {\n  font-size: 22px;\n  margin-top: 13px;\n  color: #989898;\n}\n.counter-list[_ngcontent-%COMP%]   mat-card[_ngcontent-%COMP%] {\n  transition: transform 0.5s, background-color 0.2s;\n  cursor: pointer;\n}\n.counter-list[_ngcontent-%COMP%]   mat-card[_ngcontent-%COMP%]:hover {\n  transform: scale(0.95);\n  background-color: #eeeeee;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2VkaXQtY29ycmVjdGlvbi9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXNzZXRzXFxzdHlsZXNcXHN0YWZmaXQtc3R5bGUtMS5zY3NzIiwic3JjL2FwcC9ldmFsdWF0aW9ucy90ZXN0L2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvZWRpdC1jb3JyZWN0aW9uL2VkaXQtY29ycmVjdGlvbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2VkaXQtY29ycmVjdGlvbi9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXBwXFxldmFsdWF0aW9uc1xcdGVzdFxcaW5mcmFzdHJ1Y3R1cmVcXG5nLWNvbXBvbmVudHNcXGVkaXQtY29ycmVjdGlvblxcZWRpdC1jb3JyZWN0aW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9DQUFBO0FBUUEsd0JBQUE7QUFRQSx1QkFBQTtBQUdBLDJCQUFBO0FBRUE7RUFDRSw2QkFBQTtBQ2hCRjtBRGtCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDaEJKO0FEa0JJO0VBQ0UsbUJBM0JRO0VBNEJSLGdCQUFBO0VBQ0EsY0FBQTtBQ2hCTjtBRHNCQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQ25CRjtBRHFCRTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDbkJKO0FEcUJJO0VBQ0UsWUFBQTtBQ25CTjtBRHNCRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtBQ3BCSjtBRHdCQTtFQUNFLDZCQUFBO0VBQ0EsZUFBQTtBQ3JCRjtBRHVCRTtFQUNFLFNBQUE7QUNyQko7QUR3QkU7RUFDRSxnQ0FBQTtBQ3RCSjtBRHlCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDdkJKO0FEeUJJO0VBQ0UsbUJBM0VRO0VBNEVSLGdCQUFBO0VBQ0EsY0FBQTtBQ3ZCTjtBRDBCSTtFQUNFLHlCQTVFaUI7RUE2RWpCLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUN4Qk47QUQ2QkE7RUFDRSxjQWxGbUI7QUN3RHJCO0FENkJBO0VBQ0UsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDMUJGO0FENkJBO0VBQ0UsV0FBQTtBQzFCRjtBRDZCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQzFCRjtBRDZCQTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUMxQkY7QUQ0QkU7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUMxQko7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtBQzNCRjtBRDhCQTtFQUNFLGVBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQzNCRjtBRGtDQTtFQUNFLGNBQUE7QUMvQkY7QURrQ0EseUJBQUE7QUFFQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbURBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0VBQ0EseUJBQUE7QUNoQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UsY0FBQTtBQ2xDRjtBRHFDQSxZQUFBO0FBRUE7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLHVCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsZUFBQTtBQ25DRjtBRHNDQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsMENBQUE7QUNuQ0Y7QUNsTUE7RUFDSSxpQkFBQTtBRHFNSjtBQ2xNQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0FEcU1KO0FDbE1BO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBRHFNSjtBQ2xNQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtBRHFNSjtBQ2xNQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsOEJBQUE7QURxTUo7QUNsTUE7RUFDSTtJQUNJLGdCQUFBO0lBQ0EsaUJBQUE7RURxTU47QUFDRjtBQ2xNQTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtBRG9NSjtBQ25NSTtFQUNJLGdCQUFBO0FEcU1SO0FDbk1JO0VBQ0ksY0FBQTtBRHFNUjtBQ2pNQTtFQUNJLG1CQUFBO0VBQ0EsMkJBQUE7RUFBQSx3QkFBQTtFQUFBLG1CQUFBO0VBQ0EsY0FBQTtBRG9NSjtBQ2xNQTtFQUNJLGVBQUE7QURxTUo7QUNuTVE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QURxTVo7QUNqTUk7RUFDSSxjQUFBO0FEbU1SO0FDL0xBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtBRGtNSjtBQ2hNSTtFQUNJLGdCQUFBO0FEa01SO0FDOUxBO0VBQ0ksZ0JBQUE7RUFDQSxpQkFBQTtBRGlNSjtBQ2hNSTtFQUNJLGdCQUFBO0FEa01SO0FDOUxBO0VBQ0ksY0FBQTtFQUNBLGdCQUFBO0FEaU1KO0FDaE1JO0VBQ0ksYUFBQTtFQUNBLDhCQUFBO0FEa01SO0FDaE1RO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QURrTVo7QUM3TEE7RUFDSSxVQUFBO0FEZ01KO0FDN0xBO0VBQ0ksVUFBQTtBRGdNSjtBQzdMQTtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBRGdNSjtBQzVMQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBRCtMSjtBQzNMQTs7Q0FBQTtBQUlBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0FENkxKO0FDNUxJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7QUQ4TFI7QUMzTEk7RUFDSTtJQUNJLFVBQUE7RUQ2TFY7QUFDRjtBQ3pMQTtFQUNJLGFBQUE7RUFDQSw2QkFBQTtFQUNBLHFCQUFBO0FENExKO0FDM0xJO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0FENkxSO0FDM0xJO0VBQ0kseUJBQUE7QUQ2TFI7QUMzTEk7RUFDSSx5QkFBQTtBRDZMUjtBQzNMSTtFQUNJLHlCQUFBO0FENkxSO0FDekxBO0VBQ0ksWUFBQTtFQUNBLGtDQUFBO0VBQ0EsWUFBQTtBRDRMSjtBQ3pMQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtFQUNBLDJCQUFBO0FENExKO0FDM0xJO0VBQ0ksaUJBQUE7QUQ2TFI7QUN6TEE7RUFDSSxZQUFBO0FENExKO0FDMUxJO0VBQ0ksYUFBQTtBRDRMUjtBQzFMUTtFQUNJLFVBQUE7QUQ0TFo7QUMzTFk7RUFDSSxnQkFBQTtBRDZMaEI7QUMxTFE7RUFDSSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7QUQ0TFo7QUMzTFk7RUFDSSxjQUFBO0FENkxoQjtBQ3hMQTtFQUNJLGFBQUE7QUQyTEo7QUMxTEk7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FENExSO0FDdkxJO0VBQ0ksaURBQUE7RUFFQSxlQUFBO0FEeUxSO0FDdkxJO0VBR0ksc0JBQUE7RUFDQSx5QkFBQTtBRHlMUiIsImZpbGUiOiJzcmMvYXBwL2V2YWx1YXRpb25zL3Rlc3QvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9lZGl0LWNvcnJlY3Rpb24vZWRpdC1jb3JyZWN0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xyXG4kc21hbGw6IDAuOHJlbTtcclxuJG1lZGl1bTogMXJlbTtcclxuJGxhcmdlOiAxLjI1cmVtO1xyXG4kZXh0cmEtbGFyZ2U6IDEuNTYzcmVtO1xyXG4kaHVnZTogMS45NTNyZW07XHJcbiRleHRyYS1odWdlOiAyLjQ0MXJlbTtcclxuXHJcbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cclxuJGNvbG9yLXBhbGV0dGUtMS1tYWluOiAjREU3MzczO1xyXG4kY29sb3ItcGFsZXR0ZS0xLXNlY29uZDogI0FCQUJGRjtcclxuJGNvbG9yLXByaW1hcnktbGlnaHQ6ICNFRkVGRUY7XHJcbiRjb2xvci1wcmltYXJ5LWRhcms6ICMxOTE5MTk7XHJcbiRjb2xvci1zZWNvbmRhcnktZGFyazogIzMzMzMzMztcclxuJGNvbG9yLXNjcm9sbGJhci10aHVtYjogIzhDOEM4QztcclxuXHJcbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xyXG5cclxuXHJcbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cclxuXHJcbi5jLWNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuXHJcbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAuZHluLWZpbHRlcnMge1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG4gICAgb3ZlcmZsb3cteDogYXV0bztcclxuICAgIG1hcmdpbjogMCA0MHB4IDAgMDtcclxuXHJcbiAgICA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xyXG4gICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb25maWdCdXR0b24ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gIH1cclxufVxyXG5cclxuLmNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuICBtYXgtaGVpZ2h0OiA4OCU7XHJcblxyXG4gIC5jYXJkLWNvbnRlbnQsIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuXHJcbiAgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXBhbGV0dGUtMS1tYWluO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycmVtO1xyXG4gICAgICBoZWlnaHQ6IDIuM3JlbTtcclxuICAgICAgY29sb3I6ICNlZmVmZWY7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xyXG4gIGNvbG9yOiAkY29sb3ItcHJpbWFyeS1kYXJrO1xyXG59XHJcblxyXG4uZmlsdGVyIHtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcclxuICBtYXgtd2lkdGg6IDEwZW07XHJcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gIC8vIG1heC1oZWlnaHQ6IDgwMHB4O1xyXG4gIC5tYXQtdGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICB9XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItcm93IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XHJcbiAgY29sb3I6ICM4YjhiOGI7XHJcbn1cclxuXHJcbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xyXG4gIHBhZGRpbmctbGVmdDogMDtcclxufVxyXG5cclxuLmlkSGVhZGVyIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5pZENlbGwge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxubWF0LWNhcmQtY29udGVudCB7XHJcbiAgbWFyZ2luOiAwIDM0cHg7XHJcbn1cclxuXHJcbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXHJcblxyXG4ucHJpb3JpdHkge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLy8gQFRPRE86IGNoYW5nZSBjb2xvciB0byB2YXJpYWJsZXNcclxuXHJcbi5wcmlvcml0eS1tdXlhbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xyXG59XHJcblxyXG4ucHJpb3JpdHktYWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcclxufVxyXG5cclxuLnByaW9yaXR5LW5vcm1hbCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcclxufVxyXG5cclxuLnByaW9yaXR5LWJhamEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1uaW5ndW5hIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4ucmVkLWRhdGUge1xyXG4gIGNvbG9yOiAjZWI0MTQxO1xyXG59XHJcblxyXG4vKiBNQVQgVEFCICovXHJcblxyXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcclxuICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIC4xMik7XHJcbn1cclxuIiwiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xuLyogPT09PT0gV0VJR0hUID09PT09ICovXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXG4uYy1jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG5cbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyB7XG4gIGZsZXgtZ3JvdzogMTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbiAgbWFyZ2luOiAwIDQwcHggMCAwO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxNTBweDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5jb25maWdCdXR0b24ge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBmbGV4LXNocmluazogMDtcbn1cblxuLmNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG4gIG1heC1oZWlnaHQ6IDg4JTtcbn1cbi5jYXJkLWNvbXBvbmVudCAuY2FyZC1jb250ZW50LCAuY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMDtcbn1cbi5jYXJkLWNvbXBvbmVudCA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNERTczNzM7XG4gIGJvcmRlci1yYWRpdXM6IDJyZW07XG4gIGhlaWdodDogMi4zcmVtO1xuICBjb2xvcjogI2VmZWZlZjtcbn1cblxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcbiAgY29sb3I6ICMxOTE5MTk7XG59XG5cbi5maWx0ZXIge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMC41ZW07XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcbiAgbWF4LXdpZHRoOiAxMGVtO1xuICBtYXJnaW4tcmlnaHQ6IDVlbTtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93LXg6IGF1dG87XG59XG4udGFibGUtcmVzcG9uc2l2ZSAubWF0LXRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgZGlzcGxheTogdGFibGU7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4ubWF0LWhlYWRlci1yb3cge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xuICBjb2xvcjogIzhiOGI4Yjtcbn1cblxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cblxuLmlkSGVhZGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmlkQ2VsbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG5tYXQtY2FyZC1jb250ZW50IHtcbiAgbWFyZ2luOiAwIDM0cHg7XG59XG5cbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXG4ucHJpb3JpdHkge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5wcmlvcml0eS1tdXlhbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcbn1cblxuLnByaW9yaXR5LWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xufVxuXG4ucHJpb3JpdHktbm9ybWFsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcbn1cblxuLnByaW9yaXR5LWJhamEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xufVxuXG4ucHJpb3JpdHktbmluZ3VuYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5yZWQtZGF0ZSB7XG4gIGNvbG9yOiAjZWI0MTQxO1xufVxuXG4vKiBNQVQgVEFCICovXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG59XG5cbi50aXRsZS1jb21wb25lbnQge1xuICBtYXJnaW4tbGVmdDogMnJlbTtcbn1cblxuLm5ldy1jb3JyZWN0aW9uLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubmV3LWNvcnJlY3Rpb24tY29tcG9uZW50IHtcbiAgbWFyZ2luLWxlZnQ6IDNlbTtcbiAgbWFyZ2luLXJpZ2h0OiAzZW07XG4gIG1hcmdpbi1ib3R0b206IDNlbTtcbiAgd2lkdGg6IDYwJTtcbn1cblxuLmNvdW50ZXItbGlzdC1jb250YWluZXIge1xuICBtYXJnaW4tYm90dG9tOiAzZW07XG4gIHdpZHRoOiAyNSU7XG59XG5cbi50aXRsZS1jb3VudGVyLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGZsZXgtd3JhcDogbm93cmFwO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiAxNzUwcHgpIHtcbiAgLm5ldy1jb3JyZWN0aW9uLWNvbXBvbmVudCB7XG4gICAgbWFyZ2luLWxlZnQ6IDNlbTtcbiAgICBtYXJnaW4tcmlnaHQ6IDNlbTtcbiAgfVxufVxuLnRlc3QtZGVzY3JpcHRpb24ge1xuICBmb250LXNpemU6IDE1cHg7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4udGVzdC1kZXNjcmlwdGlvbiBzcGFuIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbi50ZXN0LWRlc2NyaXB0aW9uID4gZGl2IHtcbiAgcGFkZGluZzogNXB4IDA7XG59XG5cbi5hZGQtY291bnRlci1idXR0b24ge1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xuICBtYXJnaW46IDEuNnJlbTtcbn1cblxuLnF1ZXN0aW9uIHtcbiAgcGFkZGluZzogMTBweCAwO1xufVxuLnF1ZXN0aW9uIC5xdWVzdGlvbi1zdGF0ZW1lbnQgPiBzcGFuIHtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuLnF1ZXN0aW9uID4gZGl2IHtcbiAgcGFkZGluZzogMnB4IDA7XG59XG5cbi5xdWVzdGlvbi1zdGVwIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG4ucXVlc3Rpb24tc3RlcCBzcGFuIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLnF1ZXN0aW9uLWRlc2NyaXB0aW9uIHtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG4ucXVlc3Rpb24tZGVzY3JpcHRpb24gc3BhbiB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5xdWVzdGlvbi1yZXNwb25zZSB7XG4gIHBhZGRpbmc6IDVweCAwO1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuLnF1ZXN0aW9uLXJlc3BvbnNlIC5vbmUtYW5zd2VyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLnF1ZXN0aW9uLXJlc3BvbnNlIC5vbmUtYW5zd2VyIG1hdC1mb3JtLWZpZWxkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXgtd2lkdGg6IDVlbTtcbiAgZm9udC1zaXplOiAwLjhlbTtcbn1cblxuLm1hdC1mb3JtLWZpZWxkLWluZml4IHtcbiAgcGFkZGluZzogMDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkLWluZml4IC5uZy10bnMtYzg0LTUge1xuICBwYWRkaW5nOiAwO1xufVxuXG4uYW5zd2VyLXRleHQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uc2lyY3VsbyB7XG4gIGhlaWdodDogMTVweDtcbiAgd2lkdGg6IDE1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xufVxuXG4vKlxuICAgIENPVU5URVJTXG4qL1xuLmNvcnJlY3Rpb24tcGFuZWwsIC5mb3JtLWNvdW50ZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuLmNvcnJlY3Rpb24tcGFuZWwgbWF0LWZvcm0tZmllbGQsIC5mb3JtLWNvdW50ZXIgbWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogODQlO1xuICBtYXJnaW46IGF1dG87XG59XG5AbWVkaWEgKG1heC13aWR0aDogMTc1MHB4KSB7XG4gIC5jb3JyZWN0aW9uLXBhbmVsIG1hdC1mb3JtLWZpZWxkLCAuZm9ybS1jb3VudGVyIG1hdC1mb3JtLWZpZWxkIHtcbiAgICB3aWR0aDogNzklO1xuICB9XG59XG5cbi5jb3VudGVyLWxpc3QtYnV0dG9ucyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICBtYXJnaW46IDQwcHggMCAyMHB4IDA7XG59XG4uY291bnRlci1saXN0LWJ1dHRvbnMgLmJ1dHRvbi1jb3VudGVyIHtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmNvdW50ZXItbGlzdC1idXR0b25zIC5zYXZlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RFNzM3Mztcbn1cbi5jb3VudGVyLWxpc3QtYnV0dG9ucyAuY2FuY2VsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzc2NzY3Njtcbn1cbi5jb3VudGVyLWxpc3QtYnV0dG9ucyAucmVzZXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4uZGl2aWRlLWNvdW50ZXJzIHtcbiAgaGVpZ2h0OiAzMHB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmV5O1xuICBtYXJnaW46IGF1dG87XG59XG5cbi50aXRsZS1jb3VudGVycyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDM1cHggIWltcG9ydGFudDtcbn1cbi50aXRsZS1jb3VudGVycyAubWF0ZXJpYWwtaWNvbnMge1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuLmNvdW50ZXItY2FyZCB7XG4gIG1hcmdpbjogMTVweDtcbn1cbi5jb3VudGVyLWNhcmQgLmNvdW50ZXItY2FyZC1jb250ZW50IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5jb3VudGVyLWNhcmQgLmNvdW50ZXItY2FyZC1jb250ZW50IC5jb3VudGVyLWNhcmQtdGV4dCB7XG4gIHdpZHRoOiA3MCU7XG59XG4uY291bnRlci1jYXJkIC5jb3VudGVyLWNhcmQtY29udGVudCAuY291bnRlci1jYXJkLXRleHQgLmNvdW50ZXItbmFtZSB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG4uY291bnRlci1jYXJkIC5jb3VudGVyLWNhcmQtY29udGVudCAuY291bnRlci1jYXJkLWJ1dHRvbnMge1xuICB3aWR0aDogNDUlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG59XG4uY291bnRlci1jYXJkIC5jb3VudGVyLWNhcmQtY29udGVudCAuY291bnRlci1jYXJkLWJ1dHRvbnMgLnNhdmUtaWNvbiB7XG4gIGNvbG9yOiAjREU3MzczO1xufVxuXG4uY291bnRlci1pbnB1dHMge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLmNvdW50ZXItaW5wdXRzIC5tYXRlcmlhbC1pY29ucyB7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgbWFyZ2luLXRvcDogMTNweDtcbiAgY29sb3I6ICM5ODk4OTg7XG59XG5cbi5jb3VudGVyLWxpc3QgbWF0LWNhcmQge1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cywgYmFja2dyb3VuZC1jb2xvciAwLjJzO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uY291bnRlci1saXN0IG1hdC1jYXJkOmhvdmVyIHtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xuICAtbXMtdHJhbnNmb3JtOiBzY2FsZSgwLjk1KTtcbiAgdHJhbnNmb3JtOiBzY2FsZSgwLjk1KTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VlZWVlZTtcbn0iLCJAaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL3N0YWZmaXQtc3R5bGUtMSc7XHJcblxyXG4udGl0bGUtY29tcG9uZW50IHtcclxuICAgIG1hcmdpbi1sZWZ0OiAycmVtO1xyXG59XHJcblxyXG4ubmV3LWNvcnJlY3Rpb24tY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm5ldy1jb3JyZWN0aW9uLWNvbXBvbmVudCB7XHJcbiAgICBtYXJnaW4tbGVmdDogM2VtO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAzZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzZW07XHJcbiAgICB3aWR0aDogNjAlO1xyXG59XHJcblxyXG4uY291bnRlci1saXN0LWNvbnRhaW5lcntcclxuICAgIG1hcmdpbi1ib3R0b206IDNlbTtcclxuICAgIHdpZHRoOiAyNSU7XHJcbn1cclxuXHJcbi50aXRsZS1jb3VudGVyLWNvbnRhaW5lcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgZmxleC13cmFwOiBub3dyYXA7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuXHJcbkBtZWRpYSAobWF4LXdpZHRoOiAxNzUwcHgpIHtcclxuICAgIC5uZXctY29ycmVjdGlvbi1jb21wb25lbnR7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6M2VtO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogM2VtO1xyXG4gICAgfVxyXG59XHJcblxyXG4udGVzdC1kZXNjcmlwdGlvbntcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICBzcGFue1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB9XHJcbiAgICA+IGRpdntcclxuICAgICAgICBwYWRkaW5nOiA1cHggMDtcclxuICAgIH1cclxufVxyXG5cclxuLmFkZC1jb3VudGVyLWJ1dHRvbntcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBoZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gICAgbWFyZ2luOiAxLjZyZW07XHJcbn1cclxuLnF1ZXN0aW9ue1xyXG4gICAgcGFkZGluZzogMTBweCAwO1xyXG4gICAgLnF1ZXN0aW9uLXN0YXRlbWVudHtcclxuICAgICAgICA+c3BhbntcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICA+IGRpdntcclxuICAgICAgICBwYWRkaW5nOiAycHggMDtcclxuICAgIH1cclxufVxyXG5cclxuLnF1ZXN0aW9uLXN0ZXB7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICBcclxuICAgIHNwYW57XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIH1cclxufVxyXG5cclxuLnF1ZXN0aW9uLWRlc2NyaXB0aW9ue1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgc3BhbntcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgfVxyXG59XHJcblxyXG4ucXVlc3Rpb24tcmVzcG9uc2V7XHJcbiAgICBwYWRkaW5nOiA1cHggMDtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAub25lLWFuc3dlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICBcclxuICAgICAgICBtYXQtZm9ybS1maWVsZHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXgtd2lkdGg6IDVlbTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAuOGVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4e1xyXG4gICAgcGFkZGluZzogMDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4IC5uZy10bnMtYzg0LTV7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG4uYW5zd2VyLXRleHR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG5cclxuLnNpcmN1bG97XHJcbiAgICBoZWlnaHQ6IDE1cHg7XHJcbiAgICB3aWR0aDogMTVweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxufVxyXG5cclxuXHJcbi8qXHJcbiAgICBDT1VOVEVSU1xyXG4qL1xyXG5cclxuLmNvcnJlY3Rpb24tcGFuZWwsIC5mb3JtLWNvdW50ZXJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIG1hdC1mb3JtLWZpZWxke1xyXG4gICAgICAgIHdpZHRoOiA4NCU7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgfVxyXG5cclxuICAgIEBtZWRpYShtYXgtd2lkdGg6IDE3NTBweCl7XHJcbiAgICAgICAgbWF0LWZvcm0tZmllbGR7XHJcbiAgICAgICAgICAgIHdpZHRoOiA3OSU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4uY291bnRlci1saXN0LWJ1dHRvbnN7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBtYXJnaW46IDQwcHggMCAyMHB4IDA7XHJcbiAgICAuYnV0dG9uLWNvdW50ZXJ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgICAuc2F2ZXtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xyXG4gICAgfVxyXG4gICAgLmNhbmNlbHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzY3Njc2O1xyXG4gICAgfVxyXG4gICAgLnJlc2V0e1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5kaXZpZGUtY291bnRlcnN7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgbGlnaHRncmV5O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG4udGl0bGUtY291bnRlcnN7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDM1cHggIWltcG9ydGFudDtcclxuICAgIC5tYXRlcmlhbC1pY29uc3tcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIH1cclxufVxyXG5cclxuLmNvdW50ZXItY2FyZHtcclxuICAgIG1hcmdpbjogMTVweDtcclxuXHJcbiAgICAuY291bnRlci1jYXJkLWNvbnRlbnR7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuXHJcbiAgICAgICAgLmNvdW50ZXItY2FyZC10ZXh0e1xyXG4gICAgICAgICAgICB3aWR0aDogNzAlO1xyXG4gICAgICAgICAgICAuY291bnRlci1uYW1le1xyXG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuY291bnRlci1jYXJkLWJ1dHRvbnN7XHJcbiAgICAgICAgICAgIHdpZHRoOiA0NSU7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICAgICAgLnNhdmUtaWNvbntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjREU3MzczO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbi5jb3VudGVyLWlucHV0c3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAubWF0ZXJpYWwtaWNvbnN7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEzcHg7XHJcbiAgICAgICAgY29sb3I6ICM5ODk4OTg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb3VudGVyLWxpc3R7XHJcbiAgICBtYXQtY2FyZHtcclxuICAgICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyxcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yIDAuMnM7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgfVxyXG4gICAgbWF0LWNhcmQ6aG92ZXJ7XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xyXG4gICAgICAgIC1tcy10cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xyXG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMC45NSk7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIzOCwgMjM4LCAyMzgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EditCorrectionComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-edit-correction',
          templateUrl: './edit-correction.component.html',
          styleUrls: ['./edit-correction.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }, {
          type: src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_7__["CorrectionService"]
        }, {
          type: src_app_evaluations_correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_8__["CorrectionMapper"]
        }, {
          type: src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_7__["CorrectionService"]
        }, {
          type: src_app_evaluations_test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_9__["TestService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/ng-components/edit-test/edit-test.component.ts":
  /*!************************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/ng-components/edit-test/edit-test.component.ts ***!
    \************************************************************************************************/

  /*! exports provided: EditTestComponent */

  /***/
  function srcAppEvaluationsTestInfrastructureNgComponentsEditTestEditTestComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditTestComponent", function () {
      return EditTestComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var src_app_recruitment_dictionaries__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/recruitment/dictionaries */
    "./src/app/recruitment/dictionaries.ts");
    /* harmony import */


    var src_app_evaluations_question_application_removeQuestion__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/evaluations/question/application/removeQuestion */
    "./src/app/evaluations/question/application/removeQuestion.ts");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _application_updateTest__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../application/updateTest */
    "./src/app/evaluations/test/application/updateTest.ts");
    /* harmony import */


    var src_app_evaluations_test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/evaluations/test/infrastructure/services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_evaluations_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/evaluations/test/infrastructure/TestMapper */
    "./src/app/evaluations/test/infrastructure/TestMapper.ts");
    /* harmony import */


    var src_app_evaluations_question_infrastructure_QuestionMapper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/evaluations/question/infrastructure/QuestionMapper */
    "./src/app/evaluations/question/infrastructure/QuestionMapper.ts");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/material/tooltip */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
    /* harmony import */


    var _shared_custom_mat_elements_ckeditor_ckeditor_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/ckeditor/ckeditor.component */
    "./src/app/shared/custom-mat-elements/ckeditor/ckeditor.component.ts");
    /* harmony import */


    var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! @angular/material/checkbox */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");
    /* harmony import */


    var _angular_material_radio__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! @angular/material/radio */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
    /* harmony import */


    var _angular_material_select__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! @angular/material/select */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
    /* harmony import */


    var _angular_material_core__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! @angular/material/core */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");

    function EditTestComponent_div_13_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Step: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Description: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        var i_r3 = ctx_r13.index;
        var question_r2 = ctx_r13.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", i_r3 + 1, ". ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", question_r2.name, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](question_r2.step);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", question_r2.description, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);
      }
    }

    function EditTestComponent_div_13_button_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_div_13_button_3_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r14.editQuestion(question_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "create");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditTestComponent_div_13_button_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_div_13_button_4_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r17.removeQuestion(question_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "delete");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditTestComponent_div_13_button_5_Template(rf, ctx) {
      if (rf & 1) {
        var _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_div_13_button_5_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r21);

          var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r20.confirmEditQuestion();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "done");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditTestComponent_div_13_button_6_Template(rf, ctx) {
      if (rf & 1) {
        var _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_div_13_button_6_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23);

          var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r22.cancelEditQuestion();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "clear");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditTestComponent_div_13_form_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-form-field", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "textarea", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "A statement for the question is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-form-field", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Step should be a number (ie: 7)");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "app-ckeditor", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var i_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

        var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r9.formQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", i_r3 + 1, ". ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("control", ctx_r9.formQuestion.get("description"));
      }
    }

    function EditTestComponent_div_13_div_10_app_ckeditor_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-ckeditor", 45);
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", true);
      }
    }

    function EditTestComponent_div_13_div_10_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, EditTestComponent_div_13_div_10_app_ckeditor_1_Template, 1, 1, "app-ckeditor", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r10.showEditQuestion);
      }
    }

    function EditTestComponent_div_13_div_11_div_1_mat_checkbox_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-checkbox", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", answer_r26.value, " ", answer_r26.message, "");
      }
    }

    function EditTestComponent_div_13_div_11_div_1_mat_radio_button_3_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-radio-button", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", answer_r26.value, " ", answer_r26.message, "");
      }
    }

    function EditTestComponent_div_13_div_11_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, EditTestComponent_div_13_div_11_div_1_mat_checkbox_2_Template, 2, 2, "mat-checkbox", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, EditTestComponent_div_13_div_11_div_1_mat_radio_button_3_Template, 2, 2, "mat-radio-button", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "CHECKBOX");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "RADIO");
      }
    }

    function EditTestComponent_div_13_div_11_form_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-form-field", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "A value for the answer is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-form-field", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "A statement for the answer is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r29.formAnswer);
      }
    }

    function EditTestComponent_div_13_div_11_button_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r41 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 65);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_div_13_div_11_button_4_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r41);

          var answer_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r39.editAnswer(question_r2, answer_r26);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "create");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditTestComponent_div_13_div_11_button_5_Template(rf, ctx) {
      if (rf & 1) {
        var _r45 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_div_13_div_11_button_5_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r45);

          var answer_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r43.removeAnswer(question_r2, answer_r26);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "delete");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditTestComponent_div_13_div_11_button_6_Template(rf, ctx) {
      if (rf & 1) {
        var _r49 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_div_13_div_11_button_6_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r49);

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;

          var ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r47.confirmEditAnswer(question_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "done");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditTestComponent_div_13_div_11_button_7_Template(rf, ctx) {
      if (rf & 1) {
        var _r51 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 68);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_div_13_div_11_button_7_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r51);

          var ctx_r50 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r50.cancelEditAnswer();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "clear");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditTestComponent_div_13_div_11_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, EditTestComponent_div_13_div_11_div_1_Template, 4, 2, "div", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, EditTestComponent_div_13_div_11_form_2_Template, 12, 1, "form", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, EditTestComponent_div_13_div_11_button_4_Template, 3, 0, "button", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, EditTestComponent_div_13_div_11_button_5_Template, 3, 0, "button", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, EditTestComponent_div_13_div_11_button_6_Template, 3, 0, "button", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, EditTestComponent_div_13_div_11_button_7_Template, 3, 0, "button", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var j_r27 = ctx.index;

        var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r27 !== ctx_r11.answerIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (question_r2.questionType === "RADIO" || question_r2.questionType === "CHECKBOX") && ctx_r11.showEditAnswer && j_r27 === ctx_r11.answerIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r11.showEditAnswer);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r11.showEditAnswer);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r11.showEditAnswer);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r11.showEditAnswer);
      }
    }

    function EditTestComponent_div_13_form_12_span_10_Template(rf, ctx) {
      if (rf & 1) {
        var _r56 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_div_13_form_12_span_10_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r56);

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;

          var ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r54.addAnswer(question_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Add Answer");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function EditTestComponent_div_13_form_12_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-card-content");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-form-field");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-form-field");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, EditTestComponent_div_13_form_12_span_10_Template, 5, 0, "span", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r12.formAnswer);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "RADIO" || question_r2.questionType === "CHECKBOX");
      }
    }

    function EditTestComponent_div_13_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, EditTestComponent_div_13_div_1_Template, 14, 4, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, EditTestComponent_div_13_button_3_Template, 3, 0, "button", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, EditTestComponent_div_13_button_4_Template, 3, 0, "button", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, EditTestComponent_div_13_button_5_Template, 3, 0, "button", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, EditTestComponent_div_13_button_6_Template, 3, 0, "button", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, EditTestComponent_div_13_form_7_Template, 14, 3, "form", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Answers:");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, EditTestComponent_div_13_div_10_Template, 2, 1, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, EditTestComponent_div_13_div_11_Template, 8, 6, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, EditTestComponent_div_13_form_12_Template, 11, 2, "form", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = ctx.$implicit;
        var i_r3 = ctx.index;

        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", i_r3 !== ctx_r0.questionIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.showEditQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.showEditQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.showEditQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.showEditQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.showEditQuestion && i_r3 === ctx_r0.questionIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "TEXT");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", question_r2.answers);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (question_r2.questionType === "RADIO" || question_r2.questionType === "CHECKBOX") && !ctx_r0.showEditAnswer);
      }
    }

    function EditTestComponent_form_18_mat_option_11_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 76);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var type_r59 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", type_r59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](type_r59);
      }
    }

    function EditTestComponent_form_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-form-field", 72);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "textarea", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "A statement for the question is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-form-field", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Step should be a number (ie: 7)");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-form-field", 73);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-select", 74);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, EditTestComponent_form_18_mat_option_11_Template, 2, 2, "mat-option", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "A statement for the question is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "app-ckeditor", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r1.formQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.questionTypes);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("control", ctx_r1.formQuestion.get("description"));
      }
    }

    var EditTestComponent = /*#__PURE__*/function () {
      function EditTestComponent(_fb, _service, _router, _route, _mapper, _questionMapper) {
        _classCallCheck(this, EditTestComponent);

        this._fb = _fb;
        this._service = _service;
        this._router = _router;
        this._route = _route;
        this._mapper = _mapper;
        this._questionMapper = _questionMapper; // dictionaries

        this.testTypes = src_app_recruitment_dictionaries__WEBPACK_IMPORTED_MODULE_2__["dictionaries"].tests;
        this.questionTypes = src_app_recruitment_dictionaries__WEBPACK_IMPORTED_MODULE_2__["dictionaries"].inputTypes; // aux arrays

        this.questions = []; // semaphores

        this.showEditQuestion = false;
        this.showEditAnswer = false; //state index

        this.questionIndex = -1;
        this.answerIndex = -1;
      }

      _createClass(EditTestComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this._resolved = this._route.snapshot.data['response'];

          if (this._resolved.error) {
            console.error(this._resolved.error.message, this._resolved.error.error);
            var _this$_resolved$error = this._resolved.error.error,
                message = _this$_resolved$error.message,
                id = _this$_resolved$error.id;
            Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["errorAlert"])("Couldn't retrieve data", message, id);
          }

          if (this._resolved.data !== null) {
            this.test = this._resolved.data.test;
            this.questions = this._resolved.data.test.questions;
          }

          this.formTestInit();
          this.formQuestionInit();
          this.formAnswerInit();
        }
      }, {
        key: "formTestInit",
        value: function formTestInit() {
          this.formTest = this._fb.group({
            name: [this.test ? this.test.name : '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [this.test ? this.test.description : '']
          });
        }
      }, {
        key: "formQuestionInit",
        value: function formQuestionInit(question) {
          this.formQuestion = this._fb.group({
            description: [question ? question.description : ""],
            name: [question ? question.name : "", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            questionType: [question ? question.questionType : "", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            step: [question ? question.step : "", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("([0-9])*")]]
          });
        }
      }, {
        key: "formAnswerInit",
        value: function formAnswerInit(answer) {
          this.formAnswer = this._fb.group({
            message: [answer ? answer.message : "", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            value: [answer ? answer.value : "", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
          });
        }
      }, {
        key: "addQuestion",
        value: function addQuestion() {
          if (this.formQuestion.valid) {
            this.questions.push(Object.assign(Object.assign({}, this.formQuestion.value), {
              answers: []
            }));
          }

          this.formQuestionInit();
        }
      }, {
        key: "confirmEditQuestion",
        value: function confirmEditQuestion() {
          if (this.formQuestion.valid) {
            var questionId = this.questions[this.questionIndex].id;
            var answers = this.questions[this.questionIndex].answers;
            this.questions[this.questionIndex] = this.formQuestion.value;
            this.questions[this.questionIndex].answers = answers;
            this.questions[this.questionIndex].id = questionId;
            this.formQuestionInit();
            this.showEditQuestion = false;
            this.questionIndex = -1;
          }
        }
      }, {
        key: "cancelEditQuestion",
        value: function cancelEditQuestion() {
          this.formQuestionInit();
          this.formAnswerInit();
          this.showEditQuestion = false;
          this.questionIndex = -1;
        }
      }, {
        key: "editQuestion",
        value: function editQuestion(question) {
          this.questionIndex = this.questions.indexOf(question);
          this.formQuestionInit(question);
          this.showEditQuestion = true;
        }
      }, {
        key: "removeQuestion",
        value: function removeQuestion(question) {
          var _this34 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["questionAlert"])('This question will be deleted').then(function (result) {
            if (result.isConfirmed) {
              Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["questionAlert"])('This action will delete all answers related to this question for all sent exams').then(function (result2) {
                if (result2.isConfirmed) {
                  var index = _this34.questions.indexOf(question);

                  if (index > -1) {
                    _this34.questions.splice(index, 1);
                  }

                  Object(src_app_evaluations_question_application_removeQuestion__WEBPACK_IMPORTED_MODULE_3__["removeQuestion"])(question.id, _this34.test.id, _this34._service).subscribe(function (test) {
                    return console.log(test);
                  });
                }
              });
            }
          });
        }
      }, {
        key: "addAnswer",
        value: function addAnswer(question) {
          if (this.formAnswer.valid) question.answers.push(this.formAnswer.value);
          this.formAnswerInit();
        }
      }, {
        key: "editAnswer",
        value: function editAnswer(question, answer) {
          this.answerIndex = question.answers.indexOf(answer);
          this.formAnswerInit(answer);
          this.showEditAnswer = true;
        }
      }, {
        key: "confirmEditAnswer",
        value: function confirmEditAnswer(question) {
          if (this.formAnswer.valid) {
            var answerId = question.answers[this.answerIndex].id;
            question.answers[this.answerIndex] = this.formAnswer.value;
            question.answers[this.answerIndex].id = answerId;
            this.formAnswerInit();
            this.showEditAnswer = false;
            this.answerIndex = -1;
          }
        }
      }, {
        key: "cancelEditAnswer",
        value: function cancelEditAnswer() {
          this.formAnswerInit();
          this.showEditAnswer = false;
          this.answerIndex = -1;
        }
      }, {
        key: "removeAnswer",
        value: function removeAnswer(question, answer) {
          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["questionAlert"])('This answer will be deleted').then(function (result) {
            if (result.isConfirmed) {
              Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["questionAlert"])('This action may cause changes in other parts of the application').then(function (result2) {
                if (result2.isConfirmed) {
                  var index = question.answers.indexOf(answer);

                  if (index > -1) {
                    question.answers.splice(index, 1);
                  }
                }
              });
            }
          });
        }
      }, {
        key: "onTestReset",
        value: function onTestReset() {
          var _this35 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["questionAlert"])('All your changes will be discarted').then(function (result) {
            if (result.isConfirmed) {
              _this35.formTestInit();

              _this35.formQuestionInit();

              _this35.formAnswerInit();

              _this35.questions = [];
            }
          });
        }
      }, {
        key: "onCancelClick",
        value: function onCancelClick() {
          var _this36 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["questionAlert"])("Your test won't be saved! Please consider saving first").then(function (result) {
            if (result.isConfirmed) {
              _this36._router.navigate(["../../"], {
                relativeTo: _this36._route
              });
            }
          });
        }
      }, {
        key: "onTestSave",
        value: function onTestSave() {
          var _this37 = this;

          console.log(this.test);
          this.test.name = this.formTest.get('name').value;
          this.test.description = this.formTest.get('description').value;
          Object(_application_updateTest__WEBPACK_IMPORTED_MODULE_5__["updateTest"])(this.test, this._service, this._mapper).subscribe(function (response) {
            Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["successAlert"])('Test was created succesfully');

            _this37.formTestInit();

            _this37.formQuestionInit();

            _this37.formAnswerInit();

            _this37.questions = response.questions;
          }, function (error) {
            console.error('Error on create test', error);
            var _error$error3 = error.error,
                message = _error$error3.message,
                id = _error$error3.id;
            Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["errorAlert"])("Couldn't update test", message, id);
          });
        }
      }]);

      return EditTestComponent;
    }();

    EditTestComponent.ɵfac = function EditTestComponent_Factory(t) {
      return new (t || EditTestComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_6__["TestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_8__["TestMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_question_infrastructure_QuestionMapper__WEBPACK_IMPORTED_MODULE_9__["QuestionMapper"]));
    };

    EditTestComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: EditTestComponent,
      selectors: [["app-edit-test"]],
      decls: 32,
      vars: 3,
      consts: [[1, "new-test-component"], [1, "form-newTest"], [3, "formGroup"], ["appearance", "standard", 1, "test-name"], ["matInput", "", "arial-label", "name", "placeholder", "Name", "formControlName", "name"], [1, "test-description"], ["matInput", "", "arial-label", "description", "placeholder", "Description", "formControlName", "description"], [1, "folio"], ["class", "question", 4, "ngFor", "ngForOf"], [1, "question-text"], ["class", "form-question", 3, "formGroup", 4, "ngIf"], [1, "add-question"], ["mat-stroked-button", "", 1, "btn", 3, "click"], [1, "buttons-test"], ["mat-flat-button", "", 1, "staffit-button", "reset-test-button", 3, "click"], ["mat-flat-button", "", 1, "staffit-button", "cancel-button", 3, "click"], ["mat-flat-button", "", 1, "staffit-button", "save-test-button", 3, "click"], [1, "question"], ["class", "question-hide", 4, "ngIf"], [1, "question-actions"], ["mat-icon-button", "", "matTooltip", "Edit question", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Delete question", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Comfirm edit question", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Cancel edit question", 3, "click", 4, "ngIf"], ["class", "text-answer", 4, "ngIf"], ["class", "question-answer", 4, "ngFor", "ngForOf"], ["class", "answer-form", 3, "formGroup", 4, "ngIf"], [1, "question-hide"], [1, "question-statement"], [1, "questions-margin"], [1, "question-step"], [1, "question-description"], [3, "innerHTML"], ["mat-icon-button", "", "matTooltip", "Edit question", 3, "click"], ["mat-icon-button", "", "matTooltip", "Delete question", 3, "click"], ["mat-icon-button", "", "matTooltip", "Comfirm edit question", 3, "click"], ["mat-icon-button", "", "matTooltip", "Cancel edit question", 3, "click"], [1, "form-question", 3, "formGroup"], [1, "form-statement-noType"], ["matInput", "", "arial-label", "name", "placeholder", "Statement", "formControlName", "name"], [1, "form-step"], ["matInput", "", "arial-label", "name", "placeholder", "Step", "formControlName", "step"], [1, "text-answer"], [3, "control"], [3, "disabled", 4, "ngIf"], [3, "disabled"], [1, "question-answer"], [4, "ngIf"], [1, "answer-actions"], ["mat-icon-button", "", "matTooltip", "Edit answer", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Delete answer", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Comfirm edit answer", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Cancel edit answer", 3, "click", 4, "ngIf"], [1, "answer-message"], ["disabled", "", 4, "ngIf"], ["value", "false", "disabled", "", 4, "ngIf"], ["disabled", ""], ["value", "false", "disabled", ""], [1, "answer-form", 3, "formGroup"], [1, "edit-answer"], [1, "answer-value-form"], [2, "width", "90%"], ["matInput", "", "arial-label", "name", "placeholder", "Value", "formControlName", "value"], [1, "answer-statement-form"], ["matInput", "", "arial-label", "name", "placeholder", "Statement", "formControlName", "message"], ["mat-icon-button", "", "matTooltip", "Edit answer", 3, "click"], ["mat-icon-button", "", "matTooltip", "Delete answer", 3, "click"], ["mat-icon-button", "", "matTooltip", "Comfirm edit answer", 3, "click"], ["mat-icon-button", "", "matTooltip", "Cancel edit answer", 3, "click"], [1, "answer-card"], [1, "add-answer"], ["class", "add-answer", 4, "ngIf"], [1, "form-statement"], [1, "form-type"], ["arial-label", "testType", "placeholder", "Type", "formControlName", "questionType"], [3, "value", 4, "ngFor", "ngForOf"], [3, "value"]],
      template: function EditTestComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-form-field", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Test name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-error");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "A name for the test is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-form-field", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "textarea", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-card-content");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, EditTestComponent_div_13_Template, 13, 9, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "New Question");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "mat-card");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "mat-card-content");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, EditTestComponent_form_18_Template, 16, 3, "form", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "span", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_Template_button_click_20_listener() {
            return ctx.addQuestion();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "mat-icon");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "add");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " Add Question");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "mat-card-actions");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_Template_button_click_26_listener() {
            return ctx.onTestReset();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Reset Test");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "button", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_Template_button_click_28_listener() {
            return ctx.onCancelClick();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Cancel");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "button", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function EditTestComponent_Template_button_click_30_listener() {
            return ctx.onTestSave();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Save Test");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.formTest);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.questions);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.showEditQuestion);
        }
      },
      directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_10__["MatCard"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_12__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__["MatError"], _angular_material_card__WEBPACK_IMPORTED_MODULE_10__["MatCardContent"], _angular_common__WEBPACK_IMPORTED_MODULE_13__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_13__["NgIf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_14__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__["MatIcon"], _angular_material_card__WEBPACK_IMPORTED_MODULE_10__["MatCardActions"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_16__["MatTooltip"], _shared_custom_mat_elements_ckeditor_ckeditor_component__WEBPACK_IMPORTED_MODULE_17__["CkeditorComponent"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__["MatCheckbox"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_19__["MatRadioButton"], _angular_material_select__WEBPACK_IMPORTED_MODULE_20__["MatSelect"], _angular_material_core__WEBPACK_IMPORTED_MODULE_21__["MatOption"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n.new-test-component[_ngcontent-%COMP%] {\n  margin: 3em 17em 3em 17em;\n}\n@media (max-width: 1750px) {\n  .new-test-component[_ngcontent-%COMP%] {\n    margin: 3em 10em 3em 10em;\n  }\n}\n\n.btn[_ngcontent-%COMP%] {\n  border-radius: 24px;\n  margin: 1rem 0;\n}\n.text-answer[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.form-newTest[_ngcontent-%COMP%] {\n  margin-left: 35px;\n  display: flex;\n}\n.form-newTest[_ngcontent-%COMP%]    > form[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.form-newTest[_ngcontent-%COMP%]   .test-name[_ngcontent-%COMP%] {\n  width: 30%;\n}\n.form-newTest[_ngcontent-%COMP%]   .test-description[_ngcontent-%COMP%] {\n  width: 63%;\n  margin-left: 4%;\n}\n\n.folio[_ngcontent-%COMP%] {\n  width: 90%;\n  background: white;\n  border-radius: 1px;\n  padding-top: 30px;\n}\n.folio[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n  color: #DE7373;\n}\n.folio[_ngcontent-%COMP%]   .question-answer[_ngcontent-%COMP%] {\n  padding: 5px;\n  margin-left: 50px;\n  position: relative;\n}\n.folio[_ngcontent-%COMP%]   .question-actions[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0px;\n  right: -80px;\n}\n.answer-actions[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0;\n  right: 0;\n  display: flex;\n}\n.answer-actions[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n  font-size: 17px;\n}\n.answer-message[_ngcontent-%COMP%] {\n  width: 95%;\n}\n\n.form-question[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  margin-top: 15px;\n}\n.form-question[_ngcontent-%COMP%]   .form-statement[_ngcontent-%COMP%], .form-question[_ngcontent-%COMP%]   .form-description[_ngcontent-%COMP%] {\n  width: 56%;\n}\n.form-question[_ngcontent-%COMP%]   .form-statement-noType[_ngcontent-%COMP%] {\n  width: 76%;\n}\n.form-question[_ngcontent-%COMP%]   .form-step[_ngcontent-%COMP%], .form-question[_ngcontent-%COMP%]   .form-type[_ngcontent-%COMP%] {\n  margin-left: auto;\n  margin-top: 15px;\n}\n.question[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  position: relative;\n}\n.question[_ngcontent-%COMP%]   .question-statement[_ngcontent-%COMP%]    > span[_ngcontent-%COMP%] {\n  font-size: 17px;\n  font-weight: 600;\n}\n.question[_ngcontent-%COMP%]   .questions-margin[_ngcontent-%COMP%] {\n  margin-left: 20px;\n}\n.question[_ngcontent-%COMP%]   .questions-margin[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.question[_ngcontent-%COMP%]   .question-step[_ngcontent-%COMP%], .question[_ngcontent-%COMP%]   .question-description[_ngcontent-%COMP%], .question[_ngcontent-%COMP%]   .question-statement[_ngcontent-%COMP%] {\n  padding: 5px;\n}\n.add-question[_ngcontent-%COMP%] {\n  margin: 10px 0px;\n}\n.answer-card[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.question-text[_ngcontent-%COMP%] {\n  margin: 2rem 0 1rem;\n  font-size: large;\n  font-weight: 500;\n}\n.edit-answer[_ngcontent-%COMP%] {\n  display: flex;\n}\n.edit-answer[_ngcontent-%COMP%]   .answer-value-form[_ngcontent-%COMP%] {\n  width: 25%;\n}\n.edit-answer[_ngcontent-%COMP%]   .answer-statement-form[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.edit-answer[_ngcontent-%COMP%]   .answer-statement-form[_ngcontent-%COMP%]   [_ngcontent-%COMP%]:first-child {\n  width: 90%;\n}\n.add-answer[_ngcontent-%COMP%] {\n  display: flex;\n}\n.add-answer[_ngcontent-%COMP%]   .answer-value-form[_ngcontent-%COMP%] {\n  margin: 0 2rem 0 0;\n}\n.add-answer[_ngcontent-%COMP%]   .answer-statement-form[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.add-answer[_ngcontent-%COMP%]   .answer-statement-form[_ngcontent-%COMP%]   [_ngcontent-%COMP%]:first-child {\n  width: 90%;\n}\n\n.buttons-test[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  margin: 80px 0 30px 0;\n}\n.staffit-button[_ngcontent-%COMP%] {\n  color: #fff;\n  border-radius: 24px;\n  width: 150px;\n  margin-right: 20px;\n}\n.save-test-button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n}\n.reset-test-button[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.cancel-button[_ngcontent-%COMP%] {\n  background-color: #767676;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2VkaXQtdGVzdC9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXNzZXRzXFxzdHlsZXNcXHN0YWZmaXQtc3R5bGUtMS5zY3NzIiwic3JjL2FwcC9ldmFsdWF0aW9ucy90ZXN0L2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvZWRpdC10ZXN0L2VkaXQtdGVzdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL2VkaXQtdGVzdC9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXBwXFxldmFsdWF0aW9uc1xcdGVzdFxcaW5mcmFzdHJ1Y3R1cmVcXG5nLWNvbXBvbmVudHNcXGVkaXQtdGVzdFxcZWRpdC10ZXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9DQUFBO0FBUUEsd0JBQUE7QUFRQSx1QkFBQTtBQUdBLDJCQUFBO0FBRUE7RUFDRSw2QkFBQTtBQ2hCRjtBRGtCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDaEJKO0FEa0JJO0VBQ0UsbUJBM0JRO0VBNEJSLGdCQUFBO0VBQ0EsY0FBQTtBQ2hCTjtBRHNCQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQ25CRjtBRHFCRTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDbkJKO0FEcUJJO0VBQ0UsWUFBQTtBQ25CTjtBRHNCRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtBQ3BCSjtBRHdCQTtFQUNFLDZCQUFBO0VBQ0EsZUFBQTtBQ3JCRjtBRHVCRTtFQUNFLFNBQUE7QUNyQko7QUR3QkU7RUFDRSxnQ0FBQTtBQ3RCSjtBRHlCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDdkJKO0FEeUJJO0VBQ0UsbUJBM0VRO0VBNEVSLGdCQUFBO0VBQ0EsY0FBQTtBQ3ZCTjtBRDBCSTtFQUNFLHlCQTVFaUI7RUE2RWpCLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUN4Qk47QUQ2QkE7RUFDRSxjQWxGbUI7QUN3RHJCO0FENkJBO0VBQ0UsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDMUJGO0FENkJBO0VBQ0UsV0FBQTtBQzFCRjtBRDZCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQzFCRjtBRDZCQTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUMxQkY7QUQ0QkU7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUMxQko7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtBQzNCRjtBRDhCQTtFQUNFLGVBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQzNCRjtBRGtDQTtFQUNFLGNBQUE7QUMvQkY7QURrQ0EseUJBQUE7QUFFQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbURBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0VBQ0EseUJBQUE7QUNoQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UsY0FBQTtBQ2xDRjtBRHFDQSxZQUFBO0FBRUE7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLHVCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsZUFBQTtBQ25DRjtBRHNDQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsMENBQUE7QUNuQ0Y7QUNqTUE7RUFDSSx5QkFBQTtBRG9NSjtBQ2pNQTtFQUNJO0lBQ0kseUJBQUE7RURvTU47QUFDRjtBQ2pNQTs7OztDQUFBO0FBTUE7RUFDSSxtQkFBQTtFQUNBLGNBQUE7QURrTUo7QUMvTEE7RUFDSSxXQUFBO0FEa01KO0FDL0xBO0VBQ0ksaUJBQUE7RUFDQSxhQUFBO0FEa01KO0FDaE1JO0VBQ0ksV0FBQTtBRGtNUjtBQ2hNSTtFQUNJLFVBQUE7QURrTVI7QUNoTUk7RUFDSSxVQUFBO0VBQ0EsZUFBQTtBRGtNUjtBQzNMQTs7OztDQUFBO0FBTUE7RUFDSSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FENkxKO0FDM0xJO0VBQ0ksY0FBQTtBRDZMUjtBQzFMSTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FENExSO0FDekxJO0VBQ0ksa0JBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtBRDJMUjtBQ3ZMQTtFQUNJLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFFBQUE7RUFDQSxhQUFBO0FEMExKO0FDekxJO0VBQ0ksZUFBQTtBRDJMUjtBQ3ZMQTtFQUNJLFVBQUE7QUQwTEo7QUN2TEE7Ozs7Q0FBQTtBQU1BO0VBQ0ksYUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBRHlMSjtBQ3ZMSTtFQUNJLFVBQUE7QUR5TFI7QUN2TEk7RUFDSSxVQUFBO0FEeUxSO0FDdkxJO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtBRHlMUjtBQ3JMQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7QUR3TEo7QUN0TFE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUR3TFo7QUNyTEk7RUFDSSxpQkFBQTtBRHVMUjtBQ3RMUTtFQUNJLGdCQUFBO0FEd0xaO0FDckxJO0VBQ0ksWUFBQTtBRHVMUjtBQ25MQTtFQUNJLGdCQUFBO0FEc0xKO0FDbkxBO0VBQ0ksVUFBQTtBRHNMSjtBQ25MQTtFQUNJLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBRHNMSjtBQ25MQTtFQUNJLGFBQUE7QURzTEo7QUNyTEk7RUFDSSxVQUFBO0FEdUxSO0FDckxJO0VBQ0ksV0FBQTtBRHVMUjtBQ3RMUTtFQUNJLFVBQUE7QUR3TFo7QUNuTEE7RUFDSSxhQUFBO0FEc0xKO0FDckxJO0VBQ0ksa0JBQUE7QUR1TFI7QUNyTEk7RUFDSSxXQUFBO0FEdUxSO0FDdExRO0VBQ0ksVUFBQTtBRHdMWjtBQy9LQTs7OztDQUFBO0FBTUE7RUFDSSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtBRGlMSjtBQzlLQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBRGlMSjtBQzlLQTtFQUNJLHlCQUFBO0FEaUxKO0FDOUtBO0VBQ0kseUJBQUE7QURpTEo7QUM5S0E7RUFDSSx5QkFBQTtBRGlMSiIsImZpbGUiOiJzcmMvYXBwL2V2YWx1YXRpb25zL3Rlc3QvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9lZGl0LXRlc3QvZWRpdC10ZXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xyXG4kc21hbGw6IDAuOHJlbTtcclxuJG1lZGl1bTogMXJlbTtcclxuJGxhcmdlOiAxLjI1cmVtO1xyXG4kZXh0cmEtbGFyZ2U6IDEuNTYzcmVtO1xyXG4kaHVnZTogMS45NTNyZW07XHJcbiRleHRyYS1odWdlOiAyLjQ0MXJlbTtcclxuXHJcbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cclxuJGNvbG9yLXBhbGV0dGUtMS1tYWluOiAjREU3MzczO1xyXG4kY29sb3ItcGFsZXR0ZS0xLXNlY29uZDogI0FCQUJGRjtcclxuJGNvbG9yLXByaW1hcnktbGlnaHQ6ICNFRkVGRUY7XHJcbiRjb2xvci1wcmltYXJ5LWRhcms6ICMxOTE5MTk7XHJcbiRjb2xvci1zZWNvbmRhcnktZGFyazogIzMzMzMzMztcclxuJGNvbG9yLXNjcm9sbGJhci10aHVtYjogIzhDOEM4QztcclxuXHJcbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xyXG5cclxuXHJcbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cclxuXHJcbi5jLWNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuXHJcbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAuZHluLWZpbHRlcnMge1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG4gICAgb3ZlcmZsb3cteDogYXV0bztcclxuICAgIG1hcmdpbjogMCA0MHB4IDAgMDtcclxuXHJcbiAgICA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xyXG4gICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb25maWdCdXR0b24ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gIH1cclxufVxyXG5cclxuLmNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuICBtYXgtaGVpZ2h0OiA4OCU7XHJcblxyXG4gIC5jYXJkLWNvbnRlbnQsIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuXHJcbiAgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXBhbGV0dGUtMS1tYWluO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycmVtO1xyXG4gICAgICBoZWlnaHQ6IDIuM3JlbTtcclxuICAgICAgY29sb3I6ICNlZmVmZWY7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xyXG4gIGNvbG9yOiAkY29sb3ItcHJpbWFyeS1kYXJrO1xyXG59XHJcblxyXG4uZmlsdGVyIHtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcclxuICBtYXgtd2lkdGg6IDEwZW07XHJcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gIC8vIG1heC1oZWlnaHQ6IDgwMHB4O1xyXG4gIC5tYXQtdGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICB9XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItcm93IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XHJcbiAgY29sb3I6ICM4YjhiOGI7XHJcbn1cclxuXHJcbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xyXG4gIHBhZGRpbmctbGVmdDogMDtcclxufVxyXG5cclxuLmlkSGVhZGVyIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5pZENlbGwge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxubWF0LWNhcmQtY29udGVudCB7XHJcbiAgbWFyZ2luOiAwIDM0cHg7XHJcbn1cclxuXHJcbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXHJcblxyXG4ucHJpb3JpdHkge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLy8gQFRPRE86IGNoYW5nZSBjb2xvciB0byB2YXJpYWJsZXNcclxuXHJcbi5wcmlvcml0eS1tdXlhbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xyXG59XHJcblxyXG4ucHJpb3JpdHktYWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcclxufVxyXG5cclxuLnByaW9yaXR5LW5vcm1hbCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcclxufVxyXG5cclxuLnByaW9yaXR5LWJhamEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1uaW5ndW5hIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4ucmVkLWRhdGUge1xyXG4gIGNvbG9yOiAjZWI0MTQxO1xyXG59XHJcblxyXG4vKiBNQVQgVEFCICovXHJcblxyXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcclxuICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIC4xMik7XHJcbn1cclxuIiwiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xuLyogPT09PT0gV0VJR0hUID09PT09ICovXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXG4uYy1jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG5cbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyB7XG4gIGZsZXgtZ3JvdzogMTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbiAgbWFyZ2luOiAwIDQwcHggMCAwO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxNTBweDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5jb25maWdCdXR0b24ge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBmbGV4LXNocmluazogMDtcbn1cblxuLmNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG4gIG1heC1oZWlnaHQ6IDg4JTtcbn1cbi5jYXJkLWNvbXBvbmVudCAuY2FyZC1jb250ZW50LCAuY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMDtcbn1cbi5jYXJkLWNvbXBvbmVudCA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNERTczNzM7XG4gIGJvcmRlci1yYWRpdXM6IDJyZW07XG4gIGhlaWdodDogMi4zcmVtO1xuICBjb2xvcjogI2VmZWZlZjtcbn1cblxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcbiAgY29sb3I6ICMxOTE5MTk7XG59XG5cbi5maWx0ZXIge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMC41ZW07XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcbiAgbWF4LXdpZHRoOiAxMGVtO1xuICBtYXJnaW4tcmlnaHQ6IDVlbTtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93LXg6IGF1dG87XG59XG4udGFibGUtcmVzcG9uc2l2ZSAubWF0LXRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgZGlzcGxheTogdGFibGU7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4ubWF0LWhlYWRlci1yb3cge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xuICBjb2xvcjogIzhiOGI4Yjtcbn1cblxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cblxuLmlkSGVhZGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmlkQ2VsbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG5tYXQtY2FyZC1jb250ZW50IHtcbiAgbWFyZ2luOiAwIDM0cHg7XG59XG5cbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXG4ucHJpb3JpdHkge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5wcmlvcml0eS1tdXlhbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcbn1cblxuLnByaW9yaXR5LWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xufVxuXG4ucHJpb3JpdHktbm9ybWFsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcbn1cblxuLnByaW9yaXR5LWJhamEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xufVxuXG4ucHJpb3JpdHktbmluZ3VuYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5yZWQtZGF0ZSB7XG4gIGNvbG9yOiAjZWI0MTQxO1xufVxuXG4vKiBNQVQgVEFCICovXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG59XG5cbi5uZXctdGVzdC1jb21wb25lbnQge1xuICBtYXJnaW46IDNlbSAxN2VtIDNlbSAxN2VtO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogMTc1MHB4KSB7XG4gIC5uZXctdGVzdC1jb21wb25lbnQge1xuICAgIG1hcmdpbjogM2VtIDEwZW0gM2VtIDEwZW07XG4gIH1cbn1cbi8qXG4gICAgLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgSEVBREVSXG4gICAgLS0tLS0tLS0tLS0tLS1cbiovXG4uYnRuIHtcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcbiAgbWFyZ2luOiAxcmVtIDA7XG59XG5cbi50ZXh0LWFuc3dlciB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZm9ybS1uZXdUZXN0IHtcbiAgbWFyZ2luLWxlZnQ6IDM1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uZm9ybS1uZXdUZXN0ID4gZm9ybSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuLmZvcm0tbmV3VGVzdCAudGVzdC1uYW1lIHtcbiAgd2lkdGg6IDMwJTtcbn1cbi5mb3JtLW5ld1Rlc3QgLnRlc3QtZGVzY3JpcHRpb24ge1xuICB3aWR0aDogNjMlO1xuICBtYXJnaW4tbGVmdDogNCU7XG59XG5cbi8qXG4gICAgLS0tLS0tLS0tLS0tLVxuICAgICAgICBQQVBFUiAgICBcbiAgICAtLS0tLS0tLS0tLS0tXG4qL1xuLmZvbGlvIHtcbiAgd2lkdGg6IDkwJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDFweDtcbiAgcGFkZGluZy10b3A6IDMwcHg7XG59XG4uZm9saW8gbWF0LWljb24ge1xuICBjb2xvcjogI0RFNzM3Mztcbn1cbi5mb2xpbyAucXVlc3Rpb24tYW5zd2VyIHtcbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW4tbGVmdDogNTBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmZvbGlvIC5xdWVzdGlvbi1hY3Rpb25zIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDBweDtcbiAgcmlnaHQ6IC04MHB4O1xufVxuXG4uYW5zd2VyLWFjdGlvbnMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgcmlnaHQ6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uYW5zd2VyLWFjdGlvbnMgbWF0LWljb24ge1xuICBmb250LXNpemU6IDE3cHg7XG59XG5cbi5hbnN3ZXItbWVzc2FnZSB7XG4gIHdpZHRoOiA5NSU7XG59XG5cbi8qXG4gICAgLS0tLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgUVVFU1RJT05TXG4gICAgLS0tLS0tLS0tLS0tLS0tLS1cbiovXG4uZm9ybS1xdWVzdGlvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5mb3JtLXF1ZXN0aW9uIC5mb3JtLXN0YXRlbWVudCwgLmZvcm0tcXVlc3Rpb24gLmZvcm0tZGVzY3JpcHRpb24ge1xuICB3aWR0aDogNTYlO1xufVxuLmZvcm0tcXVlc3Rpb24gLmZvcm0tc3RhdGVtZW50LW5vVHlwZSB7XG4gIHdpZHRoOiA3NiU7XG59XG4uZm9ybS1xdWVzdGlvbiAuZm9ybS1zdGVwLCAuZm9ybS1xdWVzdGlvbiAuZm9ybS10eXBlIHtcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5xdWVzdGlvbiB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5xdWVzdGlvbiAucXVlc3Rpb24tc3RhdGVtZW50ID4gc3BhbiB7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5xdWVzdGlvbiAucXVlc3Rpb25zLW1hcmdpbiB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuLnF1ZXN0aW9uIC5xdWVzdGlvbnMtbWFyZ2luIHNwYW4ge1xuICBmb250LXdlaWdodDogNTAwO1xufVxuLnF1ZXN0aW9uIC5xdWVzdGlvbi1zdGVwLCAucXVlc3Rpb24gLnF1ZXN0aW9uLWRlc2NyaXB0aW9uLCAucXVlc3Rpb24gLnF1ZXN0aW9uLXN0YXRlbWVudCB7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuLmFkZC1xdWVzdGlvbiB7XG4gIG1hcmdpbjogMTBweCAwcHg7XG59XG5cbi5hbnN3ZXItY2FyZCB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5xdWVzdGlvbi10ZXh0IHtcbiAgbWFyZ2luOiAycmVtIDAgMXJlbTtcbiAgZm9udC1zaXplOiBsYXJnZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLmVkaXQtYW5zd2VyIHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5lZGl0LWFuc3dlciAuYW5zd2VyLXZhbHVlLWZvcm0ge1xuICB3aWR0aDogMjUlO1xufVxuLmVkaXQtYW5zd2VyIC5hbnN3ZXItc3RhdGVtZW50LWZvcm0ge1xuICB3aWR0aDogMTAwJTtcbn1cbi5lZGl0LWFuc3dlciAuYW5zd2VyLXN0YXRlbWVudC1mb3JtIDpmaXJzdC1jaGlsZCB7XG4gIHdpZHRoOiA5MCU7XG59XG5cbi5hZGQtYW5zd2VyIHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5hZGQtYW5zd2VyIC5hbnN3ZXItdmFsdWUtZm9ybSB7XG4gIG1hcmdpbjogMCAycmVtIDAgMDtcbn1cbi5hZGQtYW5zd2VyIC5hbnN3ZXItc3RhdGVtZW50LWZvcm0ge1xuICB3aWR0aDogMTAwJTtcbn1cbi5hZGQtYW5zd2VyIC5hbnN3ZXItc3RhdGVtZW50LWZvcm0gOmZpcnN0LWNoaWxkIHtcbiAgd2lkdGg6IDkwJTtcbn1cblxuLyogXG4gICAgLS0tLS0tLS0tLS0tLS0tLVxuICAgICAgICBCVVRUT05TXG4gICAgLS0tLS0tLS0tLS0tLS0tLVxuKi9cbi5idXR0b25zLXRlc3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBtYXJnaW46IDgwcHggMCAzMHB4IDA7XG59XG5cbi5zdGFmZml0LWJ1dHRvbiB7XG4gIGNvbG9yOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICB3aWR0aDogMTUwcHg7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cblxuLnNhdmUtdGVzdC1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xufVxuXG4ucmVzZXQtdGVzdC1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4uY2FuY2VsLWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM3Njc2NzY7XG59IiwiQGltcG9ydCAnLi4vLi4vLi4vLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9zdGFmZml0LXN0eWxlLTEnO1xyXG5cclxuXHJcbi5uZXctdGVzdC1jb21wb25lbnR7XHJcbiAgICBtYXJnaW46IDNlbSAxN2VtIDNlbSAxN2VtO1xyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogMTc1MHB4KSB7XHJcbiAgICAubmV3LXRlc3QtY29tcG9uZW50e1xyXG4gICAgICAgIG1hcmdpbjogM2VtIDEwZW0gM2VtIDEwZW07XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qXHJcbiAgICAtLS0tLS0tLS0tLS0tLVxyXG4gICAgICAgIEhFQURFUlxyXG4gICAgLS0tLS0tLS0tLS0tLS1cclxuKi9cclxuXHJcbi5idG57XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNHB4O1xyXG4gICAgbWFyZ2luOiAxcmVtIDA7XHJcbn1cclxuXHJcbi50ZXh0LWFuc3dlcntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZm9ybS1uZXdUZXN0e1xyXG4gICAgbWFyZ2luLWxlZnQ6IDM1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAgID4gZm9ybXtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICAgIC50ZXN0LW5hbWV7XHJcbiAgICAgICAgd2lkdGg6IDMwJTtcclxuICAgIH1cclxuICAgIC50ZXN0LWRlc2NyaXB0aW9ue1xyXG4gICAgICAgIHdpZHRoOiA2MyU7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDQlO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuXHJcblxyXG4vKlxyXG4gICAgLS0tLS0tLS0tLS0tLVxyXG4gICAgICAgIFBBUEVSICAgIFxyXG4gICAgLS0tLS0tLS0tLS0tLVxyXG4qL1xyXG5cclxuLmZvbGlve1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMXB4O1xyXG4gICAgcGFkZGluZy10b3A6IDMwcHg7XHJcblxyXG4gICAgbWF0LWljb257XHJcbiAgICAgICAgY29sb3I6ICNERTczNzM7XHJcbiAgICB9XHJcblxyXG4gICAgLnF1ZXN0aW9uLWFuc3dlcntcclxuICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgfVxyXG5cclxuICAgIC5xdWVzdGlvbi1hY3Rpb25ze1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDBweDtcclxuICAgICAgICByaWdodDogLTgwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5hbnN3ZXItYWN0aW9uc3tcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIG1hdC1pY29ue1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIH1cclxufVxyXG5cclxuLmFuc3dlci1tZXNzYWdle1xyXG4gICAgd2lkdGg6IDk1JTtcclxufVxyXG5cclxuLypcclxuICAgIC0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgICAgUVVFU1RJT05TXHJcbiAgICAtLS0tLS0tLS0tLS0tLS0tLVxyXG4qL1xyXG5cclxuLmZvcm0tcXVlc3Rpb257XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgIFxyXG4gICAgLmZvcm0tc3RhdGVtZW50LCAuZm9ybS1kZXNjcmlwdGlvbntcclxuICAgICAgICB3aWR0aDogNTYlO1xyXG4gICAgfVxyXG4gICAgLmZvcm0tc3RhdGVtZW50LW5vVHlwZXtcclxuICAgICAgICB3aWR0aDogNzYlO1xyXG4gICAgfVxyXG4gICAgLmZvcm0tc3RlcCwgLmZvcm0tdHlwZXtcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4ucXVlc3Rpb257XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLnF1ZXN0aW9uLXN0YXRlbWVudHtcclxuICAgICAgICA+IHNwYW57XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAucXVlc3Rpb25zLW1hcmdpbntcclxuICAgICAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgICAgICBzcGFue1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5xdWVzdGlvbi1zdGVwLCAucXVlc3Rpb24tZGVzY3JpcHRpb24sIC5xdWVzdGlvbi1zdGF0ZW1lbnR7XHJcbiAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uYWRkLXF1ZXN0aW9ue1xyXG4gICAgbWFyZ2luOiAxMHB4IDBweDtcclxufVxyXG5cclxuLmFuc3dlci1jYXJke1xyXG4gICAgcGFkZGluZzogMDtcclxufVxyXG5cclxuLnF1ZXN0aW9uLXRleHR7XHJcbiAgICBtYXJnaW46IDJyZW0gMCAxcmVtO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZTtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuXHJcbi5lZGl0LWFuc3dlcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAuYW5zd2VyLXZhbHVlLWZvcm17XHJcbiAgICAgICAgd2lkdGg6IDI1JTtcclxuICAgIH1cclxuICAgIC5hbnN3ZXItc3RhdGVtZW50LWZvcm17XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgOmZpcnN0LWNoaWxke1xyXG4gICAgICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmFkZC1hbnN3ZXJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLmFuc3dlci12YWx1ZS1mb3Jte1xyXG4gICAgICAgIG1hcmdpbjogMCAycmVtIDAgMDtcclxuICAgIH1cclxuICAgIC5hbnN3ZXItc3RhdGVtZW50LWZvcm17XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgOmZpcnN0LWNoaWxke1xyXG4gICAgICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbi8qIFxyXG4gICAgLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICAgIEJVVFRPTlNcclxuICAgIC0tLS0tLS0tLS0tLS0tLS1cclxuKi9cclxuXHJcbi5idXR0b25zLXRlc3R7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIG1hcmdpbjogODBweCAwIDMwcHggMDtcclxufVxyXG5cclxuLnN0YWZmaXQtYnV0dG9uIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcclxuICAgIHdpZHRoOiAxNTBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxufVxyXG5cclxuLnNhdmUtdGVzdC1idXR0b257XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xyXG59XHJcblxyXG4ucmVzZXQtdGVzdC1idXR0b257XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xyXG59XHJcblxyXG4uY2FuY2VsLWJ1dHRvbntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM3Njc2NzY7XHJcbn1cclxuXHJcbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EditTestComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-edit-test',
          templateUrl: './edit-test.component.html',
          styleUrls: ['./edit-test.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: src_app_evaluations_test_infrastructure_services_test_service__WEBPACK_IMPORTED_MODULE_6__["TestService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]
        }, {
          type: src_app_evaluations_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_8__["TestMapper"]
        }, {
          type: src_app_evaluations_question_infrastructure_QuestionMapper__WEBPACK_IMPORTED_MODULE_9__["QuestionMapper"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/ng-components/new-test/new-test.component.ts":
  /*!**********************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/ng-components/new-test/new-test.component.ts ***!
    \**********************************************************************************************/

  /*! exports provided: NewTestComponent */

  /***/
  function srcAppEvaluationsTestInfrastructureNgComponentsNewTestNewTestComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewTestComponent", function () {
      return NewTestComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var src_app_evaluations_test_application_createTest__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/evaluations/test/application/createTest */
    "./src/app/evaluations/test/application/createTest.ts");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _recruitment_dictionaries__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../../recruitment/dictionaries */
    "./src/app/recruitment/dictionaries.ts");
    /* harmony import */


    var _services_test_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_evaluations_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/evaluations/test/infrastructure/TestMapper */
    "./src/app/evaluations/test/infrastructure/TestMapper.ts");
    /* harmony import */


    var src_app_evaluations_question_infrastructure_QuestionMapper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/evaluations/question/infrastructure/QuestionMapper */
    "./src/app/evaluations/question/infrastructure/QuestionMapper.ts");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/material/tooltip */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _angular_material_select__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/material/select */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
    /* harmony import */


    var _angular_material_core__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! @angular/material/core */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _shared_custom_mat_elements_ckeditor_ckeditor_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/ckeditor/ckeditor.component */
    "./src/app/shared/custom-mat-elements/ckeditor/ckeditor.component.ts");

    function NewTestComponent_div_18_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Step: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Description: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        var i_r3 = ctx_r13.index;
        var question_r2 = ctx_r13.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", i_r3 + 1, ". ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", question_r2.name, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](question_r2.step);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", question_r2.description, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);
      }
    }

    function NewTestComponent_div_18_button_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_div_18_button_3_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r14.editQuestion(question_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "create");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function NewTestComponent_div_18_button_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_div_18_button_4_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r17.removeQuestion(question_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "delete");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function NewTestComponent_div_18_button_5_Template(rf, ctx) {
      if (rf & 1) {
        var _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_div_18_button_5_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r21);

          var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r20.confirmEditQuestion();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "done");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function NewTestComponent_div_18_button_6_Template(rf, ctx) {
      if (rf & 1) {
        var _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_div_18_button_6_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23);

          var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r22.cancelEditQuestion();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "clear");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function NewTestComponent_div_18_form_7_mat_option_13_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var type_r25 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", type_r25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](type_r25);
      }
    }

    function NewTestComponent_div_18_form_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-form-field", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "textarea", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "A title for the question is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-form-field", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Step should be a number (ie: 7)");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-form-field", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "textarea", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-form-field", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-select", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, NewTestComponent_div_18_form_7_mat_option_13_Template, 2, 2, "mat-option", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "A type for the question is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r9.formQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r9.questionTypes);
      }
    }

    function NewTestComponent_div_18_div_10_app_ckeditor_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-ckeditor", 51);
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", true);
      }
    }

    function NewTestComponent_div_18_div_10_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NewTestComponent_div_18_div_10_app_ckeditor_1_Template, 1, 1, "app-ckeditor", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r10.showEditQuestion);
      }
    }

    function NewTestComponent_div_18_div_11_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", answer_r27.value, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", answer_r27.message, " ");
      }
    }

    function NewTestComponent_div_18_div_11_form_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-form-field", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "input", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "A title for the answer is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-form-field", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "A value for the answer is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r30.formAnswer);
      }
    }

    function NewTestComponent_div_18_div_11_button_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r38 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 65);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_div_18_div_11_button_4_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r38);

          var answer_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r36.editAnswer(question_r2, answer_r27);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "create");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function NewTestComponent_div_18_div_11_button_5_Template(rf, ctx) {
      if (rf & 1) {
        var _r42 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_div_18_div_11_button_5_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r42);

          var answer_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r40.removeAnswer(question_r2, answer_r27);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "delete");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function NewTestComponent_div_18_div_11_button_6_Template(rf, ctx) {
      if (rf & 1) {
        var _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_div_18_div_11_button_6_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46);

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;

          var ctx_r44 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r44.confirmEditAnswer(question_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "done");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function NewTestComponent_div_18_div_11_button_7_Template(rf, ctx) {
      if (rf & 1) {
        var _r48 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 68);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_div_18_div_11_button_7_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r48);

          var ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r47.cancelEditAnswer();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "clear");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function NewTestComponent_div_18_div_11_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NewTestComponent_div_18_div_11_div_1_Template, 5, 2, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, NewTestComponent_div_18_div_11_form_2_Template, 9, 1, "form", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, NewTestComponent_div_18_div_11_button_4_Template, 3, 0, "button", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, NewTestComponent_div_18_div_11_button_5_Template, 3, 0, "button", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, NewTestComponent_div_18_div_11_button_6_Template, 3, 0, "button", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, NewTestComponent_div_18_div_11_button_7_Template, 3, 0, "button", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var j_r28 = ctx.index;

        var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r28 !== ctx_r11.answerIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (question_r2.questionType === "RADIO" || question_r2.questionType === "CHECKBOX") && ctx_r11.showEditAnswer && j_r28 === ctx_r11.answerIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r11.showEditAnswer);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r11.showEditAnswer);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r11.showEditAnswer);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r11.showEditAnswer);
      }
    }

    function NewTestComponent_div_18_form_12_span_10_Template(rf, ctx) {
      if (rf & 1) {
        var _r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 72);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_div_18_form_12_span_10_Template_button_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r53);

          var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2).$implicit;

          var ctx_r51 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r51.addAnswer(question_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " Add Answer");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function NewTestComponent_div_18_form_12_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-card-content");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-form-field");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-form-field");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, NewTestComponent_div_18_form_12_span_10_Template, 5, 0, "span", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r12.formAnswer);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "RADIO" || question_r2.questionType === "CHECKBOX");
      }
    }

    function NewTestComponent_div_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NewTestComponent_div_18_div_1_Template, 14, 4, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, NewTestComponent_div_18_button_3_Template, 3, 0, "button", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, NewTestComponent_div_18_button_4_Template, 3, 0, "button", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, NewTestComponent_div_18_button_5_Template, 3, 0, "button", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, NewTestComponent_div_18_button_6_Template, 3, 0, "button", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, NewTestComponent_div_18_form_7_Template, 16, 2, "form", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Answers:");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, NewTestComponent_div_18_div_10_Template, 2, 1, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, NewTestComponent_div_18_div_11_Template, 8, 6, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, NewTestComponent_div_18_form_12_Template, 11, 2, "form", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = ctx.$implicit;
        var i_r3 = ctx.index;

        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", i_r3 !== ctx_r0.questionIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.showEditQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.showEditQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.showEditQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.showEditQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.showEditQuestion && i_r3 === ctx_r0.questionIndex);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "TEXT");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", question_r2.answers);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", (question_r2.questionType === "RADIO" || question_r2.questionType === "CHECKBOX") && !ctx_r0.showEditAnswer);
      }
    }

    function NewTestComponent_form_23_mat_option_11_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var type_r56 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", type_r56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](type_r56);
      }
    }

    function NewTestComponent_form_23_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-form-field", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "textarea", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "A title for the question is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-form-field", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Step should be a number (ie: 7)");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-form-field", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-select", 73);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, NewTestComponent_form_23_mat_option_11_Template, 2, 2, "mat-option", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "A title for the question is required");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 74);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "app-ckeditor", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r1.formQuestion);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.questionTypes);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("control", ctx_r1.formQuestion.get("description"));
      }
    }

    var NewTestComponent = /*#__PURE__*/function () {
      function NewTestComponent(_fb, _service, _router, _route, _mapper, _questionMapper) {
        _classCallCheck(this, NewTestComponent);

        this._fb = _fb;
        this._service = _service;
        this._router = _router;
        this._route = _route;
        this._mapper = _mapper;
        this._questionMapper = _questionMapper; // dictionaries

        this.testTypes = _recruitment_dictionaries__WEBPACK_IMPORTED_MODULE_4__["dictionaries"].tests;
        this.questionTypes = _recruitment_dictionaries__WEBPACK_IMPORTED_MODULE_4__["dictionaries"].inputTypes; // aux arrays

        this.questions = []; // semaphores

        this.showEditQuestion = false;
        this.showEditAnswer = false; //state index

        this.questionIndex = -1;
        this.answerIndex = -1;
      }

      _createClass(NewTestComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.formTestInit();
          this.formQuestionInit();
          this.formAnswerInit();
        }
      }, {
        key: "formTestInit",
        value: function formTestInit() {
          this.formTest = this._fb.group({
            name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            description: [""]
          });
        }
      }, {
        key: "formQuestionInit",
        value: function formQuestionInit(question) {
          this.formQuestion = this._fb.group({
            description: [question ? question.description : ""],
            name: [question ? question.name : "", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            questionType: [question ? question.questionType : "", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            step: [question ? question.step : "", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("([0-9])*")]]
          });
        }
      }, {
        key: "formAnswerInit",
        value: function formAnswerInit(answer) {
          this.formAnswer = this._fb.group({
            message: [answer ? answer.message : "", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            value: [answer ? answer.value : "", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
          });
        }
      }, {
        key: "addQuestion",
        value: function addQuestion() {
          if (this.formQuestion.valid) this.questions.push(Object.assign(Object.assign({}, this.formQuestion.value), {
            answers: []
          }));
          this.formQuestionInit();
        }
      }, {
        key: "confirmEditQuestion",
        value: function confirmEditQuestion() {
          if (this.formQuestion.valid) {
            var answers = this.questions[this.questionIndex].answers;
            this.questions[this.questionIndex] = this.formQuestion.value;
            this.questions[this.questionIndex].answers = answers;
            this.formQuestionInit();
            this.showEditQuestion = false;
            this.questionIndex = -1;
          }
        }
      }, {
        key: "cancelEditQuestion",
        value: function cancelEditQuestion() {
          this.formQuestionInit();
          this.formAnswerInit();
          this.showEditQuestion = false;
          this.questionIndex = -1;
        }
      }, {
        key: "editQuestion",
        value: function editQuestion(question) {
          this.questionIndex = this.questions.indexOf(question);
          this.formQuestionInit(question);
          this.showEditQuestion = true;
        }
      }, {
        key: "removeQuestion",
        value: function removeQuestion(question) {
          var _this38 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_3__["questionAlert"])('This question will be deleted').then(function (result) {
            if (result.isConfirmed) {
              var index = _this38.questions.indexOf(question);

              if (index > -1) {
                _this38.questions.splice(index, 1);
              }
            }
          });
        }
      }, {
        key: "addAnswer",
        value: function addAnswer(question) {
          if (this.formAnswer.valid) question.answers.push(this.formAnswer.value);
          this.formAnswerInit();
        }
      }, {
        key: "editAnswer",
        value: function editAnswer(question, answer) {
          this.answerIndex = question.answers.indexOf(answer);
          this.formAnswerInit(answer);
          this.showEditAnswer = true;
        }
      }, {
        key: "confirmEditAnswer",
        value: function confirmEditAnswer(question) {
          if (this.formAnswer.valid) {
            question.answers[this.answerIndex] = this.formAnswer.value;
            this.formAnswerInit();
            this.showEditAnswer = false;
            this.answerIndex = -1;
          }
        }
      }, {
        key: "cancelEditAnswer",
        value: function cancelEditAnswer() {
          this.formAnswerInit();
          this.showEditAnswer = false;
          this.answerIndex = -1;
        }
      }, {
        key: "removeAnswer",
        value: function removeAnswer(question, answer) {
          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_3__["questionAlert"])('This answer will be deleted').then(function (result) {
            if (result.isConfirmed) {
              var index = question.answers.indexOf(answer);

              if (index > -1) {
                question.answers.splice(index, 1);
              }
            }
          });
        }
      }, {
        key: "onTestReset",
        value: function onTestReset() {
          var _this39 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_3__["questionAlert"])('All your changes will be discarted!').then(function (result) {
            if (result.isConfirmed) {
              _this39.formTestInit();

              _this39.formQuestionInit();

              _this39.formAnswerInit();

              _this39.questions = [];
            }
          });
        }
      }, {
        key: "onCancelClick",
        value: function onCancelClick() {
          var _this40 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_3__["questionAlert"])("Your test won't be saved! Please consider saving first").then(function (result) {
            if (result.isConfirmed) {
              _this40._router.navigate(["../"], {
                relativeTo: _this40._route
              });
            }
          });
        }
      }, {
        key: "onTestSave",
        value: function onTestSave() {
          var _this41 = this;

          Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_3__["questionAlert"])('Once sent you can update it').then(function (result) {
            if (result.isConfirmed && _this41.formTest.valid) {}

            var test = {
              name: _this41.formTest.get('name').value,
              description: _this41.formTest.get('description').value,
              questions: []
            };

            _this41.questions.forEach(function (question) {
              test.questions.push(question);
            });

            console.log(test);
            Object(src_app_evaluations_test_application_createTest__WEBPACK_IMPORTED_MODULE_2__["createTest"])(test, _this41._service, _this41._mapper).subscribe(function () {
              Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_3__["successAlert"])('Test was created successfully');

              _this41.formTestInit();

              _this41.formQuestionInit();

              _this41.formAnswerInit();

              _this41.questions = [];
            }, function (error) {
              console.error('Error on create test', error);
              var _error$error4 = error.error,
                  message = _error$error4.message,
                  id = _error$error4.id;
              Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_3__["errorAlert"])("Error creating test", message, id);
            });
          });
        }
      }]);

      return NewTestComponent;
    }();

    NewTestComponent.ɵfac = function NewTestComponent_Factory(t) {
      return new (t || NewTestComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_test_service__WEBPACK_IMPORTED_MODULE_5__["TestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_7__["TestMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_question_infrastructure_QuestionMapper__WEBPACK_IMPORTED_MODULE_8__["QuestionMapper"]));
    };

    NewTestComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: NewTestComponent,
      selectors: [["app-new-test"]],
      decls: 37,
      vars: 3,
      consts: [[1, "new-test-component"], [1, "title-component"], [1, "material-icons"], [1, "form-newTest"], [3, "formGroup"], ["appearance", "standard", 1, "test-name"], ["matInput", "", "arial-label", "name", "placeholder", "Name", "formControlName", "name"], [1, "test-description"], ["matInput", "", "arial-label", "description", "placeholder", "Description", "formControlName", "description"], [1, "folio"], ["class", "question", 4, "ngFor", "ngForOf"], [1, "question-text"], ["class", "form-question", 3, "formGroup", 4, "ngIf"], [1, "add-question"], ["mat-stroked-button", "", "matTooltip", "Add question", 1, "btn", 3, "click"], [1, "buttons-test"], ["mat-flat-button", "", 1, "staffit-button", "reset-test-button", 3, "click"], ["mat-flat-button", "", 1, "staffit-button", "cancel-button", 3, "click"], ["mat-flat-button", "", 1, "staffit-button", "save-test-button", 3, "click"], [1, "question"], ["class", "question-hide", 4, "ngIf"], [1, "question-actions"], ["mat-icon-button", "", "matTooltip", "Edit question", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Delete question", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Comfirm edit question", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Cancel edit question", 3, "click", 4, "ngIf"], ["class", "text-answer", 4, "ngIf"], ["class", "question-answer", 4, "ngFor", "ngForOf"], ["class", "answer-form", 3, "formGroup", 4, "ngIf"], [1, "question-hide"], [1, "question-statement"], [1, "questions-margin"], [1, "question-step"], [1, "question-description"], [3, "innerHTML"], ["mat-icon-button", "", "matTooltip", "Edit question", 3, "click"], ["mat-icon-button", "", "matTooltip", "Delete question", 3, "click"], ["mat-icon-button", "", "matTooltip", "Comfirm edit question", 3, "click"], ["mat-icon-button", "", "matTooltip", "Cancel edit question", 3, "click"], [1, "form-question", 3, "formGroup"], [1, "form-statement"], ["matInput", "", "arial-label", "name", "placeholder", "Title", "formControlName", "name"], [1, "form-step"], ["matInput", "", "arial-label", "name", "placeholder", "Step", "formControlName", "step"], [1, "form-description"], [1, "form-type"], ["arial-label", "testType", "placeholder", "Type", "formControlName", "questionType", 2, "display", "contents"], [3, "value", 4, "ngFor", "ngForOf"], [3, "value"], [1, "text-answer"], [3, "disabled", 4, "ngIf"], [3, "disabled"], [1, "question-answer"], [4, "ngIf"], [1, "answer-actions"], ["mat-icon-button", "", "matTooltip", "Edit answer", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Delete answer", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Comfirm edit answer", 3, "click", 4, "ngIf"], ["mat-icon-button", "", "matTooltip", "Cancel edit answer", 3, "click", 4, "ngIf"], [1, "answer-message"], [1, "answer-form", 3, "formGroup"], [1, "answer-statement-form"], ["matInput", "", "arial-label", "name", "placeholder", "Title", "formControlName", "message"], [1, "answer-value-form"], ["matInput", "", "arial-label", "name", "placeholder", "Value", "formControlName", "value"], ["mat-icon-button", "", "matTooltip", "Edit answer", 3, "click"], ["mat-icon-button", "", "matTooltip", "Delete answer", 3, "click"], ["mat-icon-button", "", "matTooltip", "Comfirm edit answer", 3, "click"], ["mat-icon-button", "", "matTooltip", "Cancel edit answer", 3, "click"], [1, "answer-card"], [1, "add-answer"], ["class", "add-answer", 4, "ngIf"], ["mat-stroked-button", "", 1, "btn", 3, "click"], ["arial-label", "testType", "placeholder", "Type", "formControlName", "questionType"], [1, "editor"], [3, "control"]],
      template: function NewTestComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "note_add");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "New test");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "form", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-form-field", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Test name");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-error");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "A name for the test is required");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-form-field", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "textarea", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "mat-card-content");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, NewTestComponent_div_18_Template, 13, 9, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "p", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "New Question");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "mat-card");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "mat-card-content");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, NewTestComponent_form_23_Template, 16, 3, "form", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "span", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "button", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_Template_button_click_25_listener() {
            return ctx.addQuestion();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " Add Question");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-icon");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "add");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "mat-card-actions");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "button", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_Template_button_click_31_listener() {
            return ctx.onTestReset();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Reset Test >>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "button", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_Template_button_click_33_listener() {
            return ctx.onCancelClick();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Cancel >>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "button", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NewTestComponent_Template_button_click_35_listener() {
            return ctx.onTestSave();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Save Test >>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.formTest);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.questions);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.showEditQuestion);
        }
      },
      directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_9__["MatCard"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_11__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatError"], _angular_material_card__WEBPACK_IMPORTED_MODULE_9__["MatCardContent"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgIf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_13__["MatButton"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_14__["MatTooltip"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__["MatIcon"], _angular_material_card__WEBPACK_IMPORTED_MODULE_9__["MatCardActions"], _angular_material_select__WEBPACK_IMPORTED_MODULE_16__["MatSelect"], _angular_material_core__WEBPACK_IMPORTED_MODULE_17__["MatOption"], _shared_custom_mat_elements_ckeditor_ckeditor_component__WEBPACK_IMPORTED_MODULE_18__["CkeditorComponent"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n.new-test-component[_ngcontent-%COMP%] {\n  margin: 3em 17em 3em 17em;\n}\n@media (max-width: 1750px) {\n  .new-test-component[_ngcontent-%COMP%] {\n    margin: 3em 10em 3em 10em;\n  }\n}\n\n.form-newTest[_ngcontent-%COMP%] {\n  margin-left: 35px;\n  display: flex;\n}\n.form-newTest[_ngcontent-%COMP%]    > form[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.form-newTest[_ngcontent-%COMP%]   .test-name[_ngcontent-%COMP%] {\n  width: 30%;\n}\n.form-newTest[_ngcontent-%COMP%]   .test-description[_ngcontent-%COMP%] {\n  width: 63%;\n  margin-left: 4%;\n}\n\n.folio[_ngcontent-%COMP%] {\n  width: 100%;\n  background: white;\n  border-radius: 1px;\n  padding-top: 30px;\n}\n.folio[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n  color: #DE7373;\n}\n.folio[_ngcontent-%COMP%]   .question-answer[_ngcontent-%COMP%] {\n  padding: 5px;\n  margin-left: 50px;\n  position: relative;\n}\n.folio[_ngcontent-%COMP%]   .question-actions[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0px;\n  right: -80px;\n}\n.answer-actions[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0;\n  right: 0;\n  display: flex;\n}\n.answer-actions[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n  font-size: 17px;\n}\n.answer-message[_ngcontent-%COMP%] {\n  width: 95%;\n}\n.editor[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.form-question[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  margin-top: 15px;\n}\n.form-question[_ngcontent-%COMP%]   .form-statement[_ngcontent-%COMP%], .form-question[_ngcontent-%COMP%]   .form-description[_ngcontent-%COMP%] {\n  width: 56%;\n}\n.form-question[_ngcontent-%COMP%]   .form-step[_ngcontent-%COMP%], .form-question[_ngcontent-%COMP%]   .form-type[_ngcontent-%COMP%] {\n  margin-left: auto;\n  margin-top: 15px;\n}\n.answer-card[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.question-text[_ngcontent-%COMP%] {\n  margin: 2rem 0 1rem;\n  font-size: large;\n  font-weight: 500;\n}\n.add-answer[_ngcontent-%COMP%] {\n  display: flex;\n}\n.add-answer[_ngcontent-%COMP%]   .answer-value-form[_ngcontent-%COMP%] {\n  margin: 0 2rem 0 0;\n}\n.add-answer[_ngcontent-%COMP%]   .answer-statement-form[_ngcontent-%COMP%] {\n  width: 100%;\n}\n.add-answer[_ngcontent-%COMP%]   .answer-statement-form[_ngcontent-%COMP%]   [_ngcontent-%COMP%]:first-child {\n  width: 90%;\n}\n.question[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  position: relative;\n  width: 92%;\n}\n.question[_ngcontent-%COMP%]   .question-statement[_ngcontent-%COMP%]    > span[_ngcontent-%COMP%] {\n  font-size: 17px;\n  font-weight: 600;\n}\n.question[_ngcontent-%COMP%]   .questions-margin[_ngcontent-%COMP%] {\n  margin-left: 20px;\n}\n.question[_ngcontent-%COMP%]   .questions-margin[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.question[_ngcontent-%COMP%]   .question-step[_ngcontent-%COMP%], .question[_ngcontent-%COMP%]   .question-description[_ngcontent-%COMP%], .question[_ngcontent-%COMP%]   .question-statement[_ngcontent-%COMP%] {\n  padding: 5px;\n}\n.add-question[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n}\n.btn[_ngcontent-%COMP%] {\n  border-radius: 24px;\n  margin: 1rem 0;\n}\n\n.buttons-test[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  margin: 80px 0 30px 0;\n}\n.staffit-button[_ngcontent-%COMP%] {\n  color: #fff;\n  border-radius: 24px;\n  width: 150px;\n  margin-right: 20px;\n}\n.save-test-button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n}\n.reset-test-button[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.cancel-button[_ngcontent-%COMP%] {\n  background-color: #767676;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL25ldy10ZXN0L0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhc3NldHNcXHN0eWxlc1xcc3RhZmZpdC1zdHlsZS0xLnNjc3MiLCJzcmMvYXBwL2V2YWx1YXRpb25zL3Rlc3QvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9uZXctdGVzdC9uZXctdGVzdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL25ldy10ZXN0L0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhcHBcXGV2YWx1YXRpb25zXFx0ZXN0XFxpbmZyYXN0cnVjdHVyZVxcbmctY29tcG9uZW50c1xcbmV3LXRlc3RcXG5ldy10ZXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9DQUFBO0FBUUEsd0JBQUE7QUFRQSx1QkFBQTtBQUdBLDJCQUFBO0FBRUE7RUFDRSw2QkFBQTtBQ2hCRjtBRGtCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDaEJKO0FEa0JJO0VBQ0UsbUJBM0JRO0VBNEJSLGdCQUFBO0VBQ0EsY0FBQTtBQ2hCTjtBRHNCQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQ25CRjtBRHFCRTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDbkJKO0FEcUJJO0VBQ0UsWUFBQTtBQ25CTjtBRHNCRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtBQ3BCSjtBRHdCQTtFQUNFLDZCQUFBO0VBQ0EsZUFBQTtBQ3JCRjtBRHVCRTtFQUNFLFNBQUE7QUNyQko7QUR3QkU7RUFDRSxnQ0FBQTtBQ3RCSjtBRHlCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDdkJKO0FEeUJJO0VBQ0UsbUJBM0VRO0VBNEVSLGdCQUFBO0VBQ0EsY0FBQTtBQ3ZCTjtBRDBCSTtFQUNFLHlCQTVFaUI7RUE2RWpCLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUN4Qk47QUQ2QkE7RUFDRSxjQWxGbUI7QUN3RHJCO0FENkJBO0VBQ0UsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDMUJGO0FENkJBO0VBQ0UsV0FBQTtBQzFCRjtBRDZCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQzFCRjtBRDZCQTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUMxQkY7QUQ0QkU7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUMxQko7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtBQzNCRjtBRDhCQTtFQUNFLGVBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQzNCRjtBRGtDQTtFQUNFLGNBQUE7QUMvQkY7QURrQ0EseUJBQUE7QUFFQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbURBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0VBQ0EseUJBQUE7QUNoQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UsY0FBQTtBQ2xDRjtBRHFDQSxZQUFBO0FBRUE7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLHVCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsZUFBQTtBQ25DRjtBRHNDQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsMENBQUE7QUNuQ0Y7QUNoTUE7RUFDSSx5QkFBQTtBRG1NSjtBQ2hNQTtFQUNJO0lBQ0kseUJBQUE7RURtTU47QUFDRjtBQ2hNQTs7OztDQUFBO0FBTUE7RUFDSSxpQkFBQTtFQUNBLGFBQUE7QURpTUo7QUMvTEk7RUFDSSxXQUFBO0FEaU1SO0FDL0xJO0VBQ0ksVUFBQTtBRGlNUjtBQy9MSTtFQUNJLFVBQUE7RUFDQSxlQUFBO0FEaU1SO0FDMUxBOzs7O0NBQUE7QUFNQTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUQ0TEo7QUMxTEk7RUFDSSxjQUFBO0FENExSO0FDekxJO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUQyTFI7QUN4TEk7RUFDSSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0FEMExSO0FDdExBO0VBQ0ksa0JBQUE7RUFDQSxNQUFBO0VBQ0EsUUFBQTtFQUNBLGFBQUE7QUR5TEo7QUN4TEk7RUFDSSxlQUFBO0FEMExSO0FDdExBO0VBQ0ksVUFBQTtBRHlMSjtBQ3RMQTtFQUNJLFdBQUE7QUR5TEo7QUN0TEE7Ozs7Q0FBQTtBQU1BO0VBQ0ksYUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBRHdMSjtBQ3RMSTtFQUNJLFVBQUE7QUR3TFI7QUN0TEk7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FEd0xSO0FDcExBO0VBQ0ksVUFBQTtBRHVMSjtBQ3BMQTtFQUNJLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBRHVMSjtBQ3BMQTtFQUNJLGFBQUE7QUR1TEo7QUN0TEk7RUFDSSxrQkFBQTtBRHdMUjtBQ3RMSTtFQUNJLFdBQUE7QUR3TFI7QUN2TFE7RUFDSSxVQUFBO0FEeUxaO0FDcExBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUR1TEo7QUNyTFE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUR1TFo7QUNwTEk7RUFDSSxpQkFBQTtBRHNMUjtBQ3JMUTtFQUNJLGdCQUFBO0FEdUxaO0FDcExJO0VBQ0ksWUFBQTtBRHNMUjtBQ2xMQTtFQUNJLGFBQUE7RUFDQSx5QkFBQTtBRHFMSjtBQ2xMQTtFQUNJLG1CQUFBO0VBQ0EsY0FBQTtBRHFMSjtBQ2pMQTs7OztDQUFBO0FBTUE7RUFDSSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtBRG1MSjtBQ2hMQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBRG1MSjtBQ2hMQTtFQUNJLHlCQUFBO0FEbUxKO0FDaExBO0VBQ0kseUJBQUE7QURtTEo7QUNoTEE7RUFDSSx5QkFBQTtBRG1MSiIsImZpbGUiOiJzcmMvYXBwL2V2YWx1YXRpb25zL3Rlc3QvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy9uZXctdGVzdC9uZXctdGVzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cclxuJHNtYWxsOiAwLjhyZW07XHJcbiRtZWRpdW06IDFyZW07XHJcbiRsYXJnZTogMS4yNXJlbTtcclxuJGV4dHJhLWxhcmdlOiAxLjU2M3JlbTtcclxuJGh1Z2U6IDEuOTUzcmVtO1xyXG4kZXh0cmEtaHVnZTogMi40NDFyZW07XHJcblxyXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXHJcbiRjb2xvci1wYWxldHRlLTEtbWFpbjogI0RFNzM3MztcclxuJGNvbG9yLXBhbGV0dGUtMS1zZWNvbmQ6ICNBQkFCRkY7XHJcbiRjb2xvci1wcmltYXJ5LWxpZ2h0OiAjRUZFRkVGO1xyXG4kY29sb3ItcHJpbWFyeS1kYXJrOiAjMTkxOTE5O1xyXG4kY29sb3Itc2Vjb25kYXJ5LWRhcms6ICMzMzMzMzM7XHJcbiRjb2xvci1zY3JvbGxiYXItdGh1bWI6ICM4QzhDOEM7XHJcblxyXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cclxuXHJcblxyXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXHJcblxyXG4uYy1jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcblxyXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgLmR5bi1maWx0ZXJzIHtcclxuICAgIGZsZXgtZ3JvdzogMTtcclxuICAgIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgICBtYXJnaW46IDAgNDBweCAwIDA7XHJcblxyXG4gICAgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcclxuICAgICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29uZmlnQnV0dG9uIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcbiAgbWF4LWhlaWdodDogODglO1xyXG5cclxuICAuY2FyZC1jb250ZW50LCAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1wYWxldHRlLTEtbWFpbjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMnJlbTtcclxuICAgICAgaGVpZ2h0OiAyLjNyZW07XHJcbiAgICAgIGNvbG9yOiAjZWZlZmVmO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcclxuICBjb2xvcjogJGNvbG9yLXByaW1hcnktZGFyaztcclxufVxyXG5cclxuLmZpbHRlciB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMC41ZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XHJcbiAgbWF4LXdpZHRoOiAxMGVtO1xyXG4gIG1hcmdpbi1yaWdodDogNWVtO1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuICAvLyBtYXgtaGVpZ2h0OiA4MDBweDtcclxuICAubWF0LXRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgfVxyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xyXG4gIGNvbG9yOiAjOGI4YjhiO1xyXG59XHJcblxyXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbn1cclxuXHJcbi5pZEhlYWRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWRDZWxsIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbm1hdC1jYXJkLWNvbnRlbnQge1xyXG4gIG1hcmdpbjogMCAzNHB4O1xyXG59XHJcblxyXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xyXG5cclxuLnByaW9yaXR5IHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi8vIEBUT0RPOiBjaGFuZ2UgY29sb3IgdG8gdmFyaWFibGVzXHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcclxufVxyXG5cclxuLnByaW9yaXR5LWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1ub3JtYWwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1iYWphIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbmluZ3VuYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLnJlZC1kYXRlIHtcclxuICBjb2xvcjogI2ViNDE0MTtcclxufVxyXG5cclxuLyogTUFUIFRBQiAqL1xyXG5cclxuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xyXG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAuMTIpO1xyXG59XHJcbiIsIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xuLmMtY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMge1xuICBmbGV4LWdyb3c6IDE7XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTUwcHg7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuY29uZmlnQnV0dG9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZmxleC1zaHJpbms6IDA7XG59XG5cbi5jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xuICBtYXgtaGVpZ2h0OiA4OCU7XG59XG4uY2FyZC1jb21wb25lbnQgLmNhcmQtY29udGVudCwgLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBtYXJnaW46IDA7XG59XG4uY2FyZC1jb21wb25lbnQgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBidXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xuICBib3JkZXItcmFkaXVzOiAycmVtO1xuICBoZWlnaHQ6IDIuM3JlbTtcbiAgY29sb3I6ICNlZmVmZWY7XG59XG5cbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XG4gIGNvbG9yOiAjMTkxOTE5O1xufVxuXG4uZmlsdGVyIHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cblxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XG4gIG1heC13aWR0aDogMTBlbTtcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XG59XG5cbi50YWJsZS1yZXNwb25zaXZlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy14OiBhdXRvO1xufVxuLnRhYmxlLXJlc3BvbnNpdmUgLm1hdC10YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLm1hdC1oZWFkZXItcm93IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcbiAgY29sb3I6ICM4YjhiOGI7XG59XG5cbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG5cbi5pZEhlYWRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pZENlbGwge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxubWF0LWNhcmQtY29udGVudCB7XG4gIG1hcmdpbjogMCAzNHB4O1xufVxuXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xuLnByaW9yaXR5IHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucHJpb3JpdHktbXV5YWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XG59XG5cbi5wcmlvcml0eS1hbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcbn1cblxuLnByaW9yaXR5LW5vcm1hbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XG59XG5cbi5wcmlvcml0eS1iYWphIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcbn1cblxuLnByaW9yaXR5LW5pbmd1bmEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucmVkLWRhdGUge1xuICBjb2xvcjogI2ViNDE0MTtcbn1cblxuLyogTUFUIFRBQiAqL1xuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufVxuXG4ubmV3LXRlc3QtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzZW0gMTdlbSAzZW0gMTdlbTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDE3NTBweCkge1xuICAubmV3LXRlc3QtY29tcG9uZW50IHtcbiAgICBtYXJnaW46IDNlbSAxMGVtIDNlbSAxMGVtO1xuICB9XG59XG4vKlxuICAgIC0tLS0tLS0tLS0tLS0tXG4gICAgICAgIEhFQURFUlxuICAgIC0tLS0tLS0tLS0tLS0tXG4qL1xuLmZvcm0tbmV3VGVzdCB7XG4gIG1hcmdpbi1sZWZ0OiAzNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLmZvcm0tbmV3VGVzdCA+IGZvcm0ge1xuICB3aWR0aDogMTAwJTtcbn1cbi5mb3JtLW5ld1Rlc3QgLnRlc3QtbmFtZSB7XG4gIHdpZHRoOiAzMCU7XG59XG4uZm9ybS1uZXdUZXN0IC50ZXN0LWRlc2NyaXB0aW9uIHtcbiAgd2lkdGg6IDYzJTtcbiAgbWFyZ2luLWxlZnQ6IDQlO1xufVxuXG4vKlxuICAgIC0tLS0tLS0tLS0tLS1cbiAgICAgICAgUEFQRVIgICAgXG4gICAgLS0tLS0tLS0tLS0tLVxuKi9cbi5mb2xpbyB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMXB4O1xuICBwYWRkaW5nLXRvcDogMzBweDtcbn1cbi5mb2xpbyBtYXQtaWNvbiB7XG4gIGNvbG9yOiAjREU3MzczO1xufVxuLmZvbGlvIC5xdWVzdGlvbi1hbnN3ZXIge1xuICBwYWRkaW5nOiA1cHg7XG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG4uZm9saW8gLnF1ZXN0aW9uLWFjdGlvbnMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMHB4O1xuICByaWdodDogLTgwcHg7XG59XG5cbi5hbnN3ZXItYWN0aW9ucyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5hbnN3ZXItYWN0aW9ucyBtYXQtaWNvbiB7XG4gIGZvbnQtc2l6ZTogMTdweDtcbn1cblxuLmFuc3dlci1tZXNzYWdlIHtcbiAgd2lkdGg6IDk1JTtcbn1cblxuLmVkaXRvciB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4vKlxuICAgIC0tLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgIFFVRVNUSU9OU1xuICAgIC0tLS0tLS0tLS0tLS0tLS0tXG4qL1xuLmZvcm0tcXVlc3Rpb24ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG4uZm9ybS1xdWVzdGlvbiAuZm9ybS1zdGF0ZW1lbnQsIC5mb3JtLXF1ZXN0aW9uIC5mb3JtLWRlc2NyaXB0aW9uIHtcbiAgd2lkdGg6IDU2JTtcbn1cbi5mb3JtLXF1ZXN0aW9uIC5mb3JtLXN0ZXAsIC5mb3JtLXF1ZXN0aW9uIC5mb3JtLXR5cGUge1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cblxuLmFuc3dlci1jYXJkIHtcbiAgcGFkZGluZzogMDtcbn1cblxuLnF1ZXN0aW9uLXRleHQge1xuICBtYXJnaW46IDJyZW0gMCAxcmVtO1xuICBmb250LXNpemU6IGxhcmdlO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uYWRkLWFuc3dlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uYWRkLWFuc3dlciAuYW5zd2VyLXZhbHVlLWZvcm0ge1xuICBtYXJnaW46IDAgMnJlbSAwIDA7XG59XG4uYWRkLWFuc3dlciAuYW5zd2VyLXN0YXRlbWVudC1mb3JtIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4uYWRkLWFuc3dlciAuYW5zd2VyLXN0YXRlbWVudC1mb3JtIDpmaXJzdC1jaGlsZCB7XG4gIHdpZHRoOiA5MCU7XG59XG5cbi5xdWVzdGlvbiB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDkyJTtcbn1cbi5xdWVzdGlvbiAucXVlc3Rpb24tc3RhdGVtZW50ID4gc3BhbiB7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5xdWVzdGlvbiAucXVlc3Rpb25zLW1hcmdpbiB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuLnF1ZXN0aW9uIC5xdWVzdGlvbnMtbWFyZ2luIHNwYW4ge1xuICBmb250LXdlaWdodDogNTAwO1xufVxuLnF1ZXN0aW9uIC5xdWVzdGlvbi1zdGVwLCAucXVlc3Rpb24gLnF1ZXN0aW9uLWRlc2NyaXB0aW9uLCAucXVlc3Rpb24gLnF1ZXN0aW9uLXN0YXRlbWVudCB7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuLmFkZC1xdWVzdGlvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG59XG5cbi5idG4ge1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICBtYXJnaW46IDFyZW0gMDtcbn1cblxuLyogXG4gICAgLS0tLS0tLS0tLS0tLS0tLVxuICAgICAgICBCVVRUT05TXG4gICAgLS0tLS0tLS0tLS0tLS0tLVxuKi9cbi5idXR0b25zLXRlc3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBtYXJnaW46IDgwcHggMCAzMHB4IDA7XG59XG5cbi5zdGFmZml0LWJ1dHRvbiB7XG4gIGNvbG9yOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICB3aWR0aDogMTUwcHg7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cblxuLnNhdmUtdGVzdC1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xufVxuXG4ucmVzZXQtdGVzdC1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4uY2FuY2VsLWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM3Njc2NzY7XG59IiwiXHJcbkBpbXBvcnQgJy4uLy4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvc3RhZmZpdC1zdHlsZS0xJztcclxuXHJcblxyXG4ubmV3LXRlc3QtY29tcG9uZW50e1xyXG4gICAgbWFyZ2luOiAzZW0gMTdlbSAzZW0gMTdlbTtcclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDE3NTBweCkge1xyXG4gICAgLm5ldy10ZXN0LWNvbXBvbmVudHtcclxuICAgICAgICBtYXJnaW46IDNlbSAxMGVtIDNlbSAxMGVtO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKlxyXG4gICAgLS0tLS0tLS0tLS0tLS1cclxuICAgICAgICBIRUFERVJcclxuICAgIC0tLS0tLS0tLS0tLS0tXHJcbiovXHJcblxyXG4uZm9ybS1uZXdUZXN0e1xyXG4gICAgbWFyZ2luLWxlZnQ6IDM1cHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAgID4gZm9ybXtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICAgIC50ZXN0LW5hbWV7XHJcbiAgICAgICAgd2lkdGg6IDMwJTtcclxuICAgIH1cclxuICAgIC50ZXN0LWRlc2NyaXB0aW9ue1xyXG4gICAgICAgIHdpZHRoOiA2MyU7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDQlO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuXHJcblxyXG4vKlxyXG4gICAgLS0tLS0tLS0tLS0tLVxyXG4gICAgICAgIFBBUEVSICAgIFxyXG4gICAgLS0tLS0tLS0tLS0tLVxyXG4qL1xyXG5cclxuLmZvbGlve1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDFweDtcclxuICAgIHBhZGRpbmctdG9wOiAzMHB4O1xyXG5cclxuICAgIG1hdC1pY29ue1xyXG4gICAgICAgIGNvbG9yOiAjREU3MzczO1xyXG4gICAgfVxyXG5cclxuICAgIC5xdWVzdGlvbi1hbnN3ZXJ7XHJcbiAgICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIH1cclxuXHJcbiAgICAucXVlc3Rpb24tYWN0aW9uc3tcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAwcHg7XHJcbiAgICAgICAgcmlnaHQ6IC04MHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uYW5zd2VyLWFjdGlvbnN7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXQtaWNvbntcclxuICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5hbnN3ZXItbWVzc2FnZXtcclxuICAgIHdpZHRoOiA5NSU7XHJcbn1cclxuXHJcbi5lZGl0b3J7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLypcclxuICAgIC0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAgICAgUVVFU1RJT05TXHJcbiAgICAtLS0tLS0tLS0tLS0tLS0tLVxyXG4qL1xyXG5cclxuLmZvcm0tcXVlc3Rpb257XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgIFxyXG4gICAgLmZvcm0tc3RhdGVtZW50LCAuZm9ybS1kZXNjcmlwdGlvbntcclxuICAgICAgICB3aWR0aDogNTYlO1xyXG4gICAgfVxyXG4gICAgLmZvcm0tc3RlcCwgLmZvcm0tdHlwZXtcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uYW5zd2VyLWNhcmR7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG4ucXVlc3Rpb24tdGV4dHtcclxuICAgIG1hcmdpbjogMnJlbSAwIDFyZW07XHJcbiAgICBmb250LXNpemU6IGxhcmdlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5cclxuLmFkZC1hbnN3ZXJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLmFuc3dlci12YWx1ZS1mb3Jte1xyXG4gICAgICAgIG1hcmdpbjogMCAycmVtIDAgMDtcclxuICAgIH1cclxuICAgIC5hbnN3ZXItc3RhdGVtZW50LWZvcm17XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgOmZpcnN0LWNoaWxke1xyXG4gICAgICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnF1ZXN0aW9ue1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiA5MiU7XHJcbiAgICAucXVlc3Rpb24tc3RhdGVtZW50e1xyXG4gICAgICAgID4gc3BhbntcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC5xdWVzdGlvbnMtbWFyZ2lue1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgICAgIHNwYW57XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgLnF1ZXN0aW9uLXN0ZXAsIC5xdWVzdGlvbi1kZXNjcmlwdGlvbiwgLnF1ZXN0aW9uLXN0YXRlbWVudHtcclxuICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5hZGQtcXVlc3Rpb257XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxufVxyXG5cclxuLmJ0bntcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICBtYXJnaW46IDFyZW0gMDtcclxufVxyXG5cclxuXHJcbi8qIFxyXG4gICAgLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgICAgIEJVVFRPTlNcclxuICAgIC0tLS0tLS0tLS0tLS0tLS1cclxuKi9cclxuXHJcbi5idXR0b25zLXRlc3R7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIG1hcmdpbjogODBweCAwIDMwcHggMDtcclxufVxyXG5cclxuLnN0YWZmaXQtYnV0dG9uIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcclxuICAgIHdpZHRoOiAxNTBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxufVxyXG5cclxuLnNhdmUtdGVzdC1idXR0b257XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xyXG59XHJcblxyXG4ucmVzZXQtdGVzdC1idXR0b257XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xyXG59XHJcblxyXG4uY2FuY2VsLWJ1dHRvbntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM3Njc2NzY7XHJcbn1cclxuXHJcbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NewTestComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-new-test',
          templateUrl: './new-test.component.html',
          styleUrls: ['./new-test.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: _services_test_service__WEBPACK_IMPORTED_MODULE_5__["TestService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]
        }, {
          type: src_app_evaluations_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_7__["TestMapper"]
        }, {
          type: src_app_evaluations_question_infrastructure_QuestionMapper__WEBPACK_IMPORTED_MODULE_8__["QuestionMapper"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/ng-components/test-analytics/test-analytics.component.ts":
  /*!**********************************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/ng-components/test-analytics/test-analytics.component.ts ***!
    \**********************************************************************************************************/

  /*! exports provided: TestAnalyticsComponent */

  /***/
  function srcAppEvaluationsTestInfrastructureNgComponentsTestAnalyticsTestAnalyticsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestAnalyticsComponent", function () {
      return TestAnalyticsComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

    var TestAnalyticsComponent = /*#__PURE__*/function () {
      function TestAnalyticsComponent() {
        _classCallCheck(this, TestAnalyticsComponent);
      }

      _createClass(TestAnalyticsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return TestAnalyticsComponent;
    }();

    TestAnalyticsComponent.ɵfac = function TestAnalyticsComponent_Factory(t) {
      return new (t || TestAnalyticsComponent)();
    };

    TestAnalyticsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TestAnalyticsComponent,
      selectors: [["app-test-analytics"]],
      decls: 2,
      vars: 0,
      template: function TestAnalyticsComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "test-analytics works!");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V2YWx1YXRpb25zL3Rlc3QvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy90ZXN0LWFuYWx5dGljcy90ZXN0LWFuYWx5dGljcy5jb21wb25lbnQuc2NzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestAnalyticsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-test-analytics',
          templateUrl: './test-analytics.component.html',
          styleUrls: ['./test-analytics.component.scss']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/ng-components/test-detail/test-detail.component.ts":
  /*!****************************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/ng-components/test-detail/test-detail.component.ts ***!
    \****************************************************************************************************/

  /*! exports provided: TestDetailComponent */

  /***/
  function srcAppEvaluationsTestInfrastructureNgComponentsTestDetailTestDetailComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestDetailComponent", function () {
      return TestDetailComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_app_evaluations_correction_application_deleteCorrection__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! src/app/evaluations/correction/application/deleteCorrection */
    "./src/app/evaluations/correction/application/deleteCorrection.ts");
    /* harmony import */


    var src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/evaluations/correction/infrastructure/services/correction.service */
    "./src/app/evaluations/correction/infrastructure/services/correction.service.ts");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/material/tooltip */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_radio__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material/radio */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
    /* harmony import */


    var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/checkbox */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");
    /* harmony import */


    var _shared_custom_mat_elements_ckeditor_ckeditor_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/ckeditor/ckeditor.component */
    "./src/app/shared/custom-mat-elements/ckeditor/ckeditor.component.ts");

    function TestDetailComponent_div_20_div_15_div_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-radio-button", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r8 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", false);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", answer_r8.value, " ", answer_r8.message, "");
      }
    }

    function TestDetailComponent_div_20_div_15_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-radio-group");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestDetailComponent_div_20_div_15_div_2_Template, 4, 3, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", question_r2.answers);
      }
    }

    function TestDetailComponent_div_20_div_16_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-checkbox", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var answer_r11 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", answer_r11.value, " ", answer_r11.message, "");
      }
    }

    function TestDetailComponent_div_20_div_16_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestDetailComponent_div_20_div_16_div_1_Template, 4, 2, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", question_r2.answers);
      }
    }

    function TestDetailComponent_div_20_div_17_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-ckeditor", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", true);
      }
    }

    function TestDetailComponent_div_20_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Step: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Description: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, TestDetailComponent_div_20_div_15_Template, 3, 1, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, TestDetailComponent_div_20_div_16_Template, 2, 1, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, TestDetailComponent_div_20_div_17_Template, 2, 1, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var question_r2 = ctx.$implicit;
        var i_r3 = ctx.index;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", i_r3 + 1, ". ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](question_r2.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](question_r2.step);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", question_r2.description, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "RADIO");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "CHECKBOX");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", question_r2.questionType === "TEXT");
      }
    }

    function TestDetailComponent_div_31_Template(rf, ctx) {
      if (rf & 1) {
        var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestDetailComponent_div_31_Template_div_click_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var correction_r13 = ctx.$implicit;

          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r15.editCorrection(correction_r13);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestDetailComponent_div_31_Template_button_click_9_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var correction_r13 = ctx.$implicit;

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r17.deleteCorrection($event, correction_r13);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "delete");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var correction_r13 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](correction_r13.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](correction_r13.description);
      }
    }

    var TestDetailComponent = /*#__PURE__*/function () {
      function TestDetailComponent(_route, _router, _correctionService) {
        _classCallCheck(this, TestDetailComponent);

        this._route = _route;
        this._router = _router;
        this._correctionService = _correctionService;
      }

      _createClass(TestDetailComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.test = this._route.snapshot.data["response"].data.test;
          this.corrections = this._route.snapshot.data["response"].data.corrections;
        }
      }, {
        key: "editTest",
        value: function editTest() {
          this._router.navigate(['../../edit/' + this.test.id], {
            relativeTo: this._route
          });
        }
      }, {
        key: "editCorrection",
        value: function editCorrection(correction) {
          this._router.navigate(["../../".concat(this.test.id, "/test-correction/edit/"), correction.id], {
            relativeTo: this._route,
            state: {
              test: this.test,
              correction: correction
            }
          });
        }
      }, {
        key: "addCorrection",
        value: function addCorrection() {
          this._router.navigate(['../../test-correction/' + this.test.id], {
            relativeTo: this._route
          });
        }
      }, {
        key: "deleteCorrection",
        value: function deleteCorrection(event, correction) {
          var _this42 = this;

          event.stopPropagation();
          Object(src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["questionAlert"])('This question will be deleted').then(function (result) {
            if (result.isConfirmed) {
              Object(src_app_evaluations_correction_application_deleteCorrection__WEBPACK_IMPORTED_MODULE_1__["deleteCorrection"])(correction.id, _this42._correctionService).subscribe(function (data) {
                Object(src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["successAlert"])('Correction deleted');
                _this42.corrections.content = _this42.corrections.content.filter(function (c) {
                  return c.id !== correction.id;
                });
              }, function (error) {
                console.error('Error on delete correction', error);
                var _error$error5 = error.error,
                    message = _error$error5.message,
                    id = _error$error5.id;
                Object(src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["errorAlert"])("Couldn't delete correction", message, id);
              });
            }
          });
        }
      }]);

      return TestDetailComponent;
    }();

    TestDetailComponent.ɵfac = function TestDetailComponent_Factory(t) {
      return new (t || TestDetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_4__["CorrectionService"]));
    };

    TestDetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TestDetailComponent,
      selectors: [["app-test-detail"]],
      decls: 32,
      vars: 8,
      consts: [[1, "test-detail-container"], [1, "test-detail-component"], [1, "title-component"], ["mat-icon-button", "", "matTooltip", "Edit question", 1, "edit-button", 3, "click"], [1, "test-body"], [1, "test-description"], [1, "text-id"], [1, "text-description"], ["class", "test-questions", 4, "ngFor", "ngForOf"], [1, "counter-list-container"], [1, "title-counters"], [1, "actions-bar"], [1, "corrections-number"], ["mat-icon-button", "", "matTooltip", "Add correction", 1, "add-correction-button", 3, "click"], [1, "counter-list"], ["class", "counter-cards", 4, "ngFor", "ngForOf"], [1, "test-questions"], [1, "question"], [1, "question-statement"], [1, "question-step"], [1, "question-description"], [3, "innerHTML"], [1, "question-response"], ["class", "radio-question", 4, "ngIf"], [4, "ngIf"], [1, "radio-question"], ["class", "one-answer", 4, "ngFor", "ngForOf"], [1, "one-answer"], [1, "answer-text"], ["disabled", "", 3, "value"], ["disabled", ""], [3, "disabled"], [1, "counter-cards"], [1, "counter-card"], [1, "counter-card-content", 3, "click"], [1, "counter-card-text"], [1, "counter-name"], [1, "counter-description"], [1, "counter-card-buttons"], ["mat-icon-button", "", 3, "click"]],
      template: function TestDetailComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](5, "uppercase");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestDetailComponent_Template_button_click_6_listener() {
            return ctx.editTest();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-icon");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "create");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-card-content");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "ID: ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Description: ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, TestDetailComponent_div_20_Template, 18, 7, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "mat-card", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "h3", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "CORRECTIONS");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestDetailComponent_Template_button_click_27_listener() {
            return ctx.addCorrection();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "mat-icon");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "control_point");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, TestDetailComponent_div_31_Template, 12, 2, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](5, 6, ctx.test.name));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.test.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.test.description);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.test.questions);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.corrections.content.length, " Corrections");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.corrections.content);
        }
      },
      directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_5__["MatCard"], _angular_material_button__WEBPACK_IMPORTED_MODULE_6__["MatButton"], _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_7__["MatTooltip"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIcon"], _angular_material_card__WEBPACK_IMPORTED_MODULE_5__["MatCardContent"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_10__["MatRadioGroup"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_10__["MatRadioButton"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_11__["MatCheckbox"], _shared_custom_mat_elements_ckeditor_ckeditor_component__WEBPACK_IMPORTED_MODULE_12__["CkeditorComponent"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["UpperCasePipe"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n.title-component[_ngcontent-%COMP%] {\n  margin-left: 2rem;\n}\n.test-detail-container[_ngcontent-%COMP%] {\n  display: flex;\n  width: 100%;\n}\n.edit-button[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 45px;\n  right: 45px;\n}\n.test-detail-component[_ngcontent-%COMP%] {\n  margin-left: 3em;\n  margin-right: 3em;\n  margin-bottom: 3em;\n  width: 60%;\n}\n.counter-list-container[_ngcontent-%COMP%] {\n  margin-bottom: 3em;\n  width: 25%;\n}\n.title-counter-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n}\n@media (max-width: 1750px) {\n  .new-correction-component[_ngcontent-%COMP%] {\n    margin-left: 3em;\n    margin-right: 3em;\n  }\n}\n.test-description[_ngcontent-%COMP%] {\n  font-size: 15px;\n  margin-bottom: 20px;\n}\n.test-description[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.test-description[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  padding: 5px 0;\n}\n.question[_ngcontent-%COMP%] {\n  padding: 10px;\n}\n.question[_ngcontent-%COMP%]   .question-statement[_ngcontent-%COMP%]    > span[_ngcontent-%COMP%] {\n  font-size: 17px;\n  font-weight: 600;\n}\n.question[_ngcontent-%COMP%]    > div[_ngcontent-%COMP%] {\n  padding: 2px 0;\n}\n.question-description[_ngcontent-%COMP%] {\n  margin-top: 10px;\n  margin-left: 20px;\n}\n.question-description[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.question-step[_ngcontent-%COMP%] {\n  margin-top: 10px;\n  margin-left: 20px;\n}\n.question-step[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.question-response[_ngcontent-%COMP%] {\n  padding: 5px 0;\n  margin-top: 15px;\n}\n.question-response[_ngcontent-%COMP%]   .one-answer[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n}\n.question-response[_ngcontent-%COMP%]   .one-answer[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n  text-align: center;\n  max-width: 5em;\n  font-size: 0.8em;\n}\n.mat-form-field-infix[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.mat-form-field-infix[_ngcontent-%COMP%]   .ng-tns-c84-5[_ngcontent-%COMP%] {\n  padding: 0;\n}\n.answer-text[_ngcontent-%COMP%] {\n  display: flex;\n  padding: 5px;\n  margin-left: 50px;\n}\n.sirculo[_ngcontent-%COMP%] {\n  height: 15px;\n  width: 15px;\n  border: 1px solid;\n  border-radius: 20px;\n}\n\n.correction-panel[_ngcontent-%COMP%], .form-counter[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n}\n.correction-panel[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%], .form-counter[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n  width: 84%;\n  margin: auto;\n}\n@media (max-width: 1750px) {\n  .correction-panel[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%], .form-counter[_ngcontent-%COMP%]   mat-form-field[_ngcontent-%COMP%] {\n    width: 79%;\n  }\n}\n.counter-list-buttons[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-around;\n  margin: 40px 0 20px 0;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .button-counter[_ngcontent-%COMP%] {\n  border-radius: 25px;\n  color: white;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .save[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .cancel[_ngcontent-%COMP%] {\n  background-color: #767676;\n}\n.counter-list-buttons[_ngcontent-%COMP%]   .reset[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.divide-counters[_ngcontent-%COMP%] {\n  height: 30px;\n  border-bottom: 1px solid lightgrey;\n  margin: auto;\n}\n.title-counters[_ngcontent-%COMP%] {\n  display: flex;\n  margin-left: 30px;\n  margin-top: 35px !important;\n}\n.title-counters[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%] {\n  margin-left: 10px;\n}\n.counter-card[_ngcontent-%COMP%] {\n  margin: 15px;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%] {\n  display: flex;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-text[_ngcontent-%COMP%] {\n  width: 70%;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-text[_ngcontent-%COMP%]   .counter-name[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-buttons[_ngcontent-%COMP%] {\n  width: 30%;\n  display: flex;\n  flex-wrap: wrap;\n}\n.counter-card[_ngcontent-%COMP%]   .counter-card-content[_ngcontent-%COMP%]   .counter-card-buttons[_ngcontent-%COMP%]   .save-icon[_ngcontent-%COMP%] {\n  color: #DE7373;\n}\n.counter-inputs[_ngcontent-%COMP%] {\n  display: flex;\n}\n.counter-inputs[_ngcontent-%COMP%]   .material-icons[_ngcontent-%COMP%] {\n  font-size: 22px;\n  margin-top: 13px;\n  color: #989898;\n}\n.counter-list[_ngcontent-%COMP%]   mat-card[_ngcontent-%COMP%] {\n  transition: transform 0.5s, background-color 0.2s;\n  cursor: pointer;\n}\n.counter-list[_ngcontent-%COMP%]   mat-card[_ngcontent-%COMP%]:hover {\n  transform: scale(0.95);\n  background-color: #eeeeee;\n}\n.actions-bar[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n}\n.actions-bar[_ngcontent-%COMP%]   .corrections-number[_ngcontent-%COMP%] {\n  margin-left: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3Rlc3QtZGV0YWlsL0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhc3NldHNcXHN0eWxlc1xcc3RhZmZpdC1zdHlsZS0xLnNjc3MiLCJzcmMvYXBwL2V2YWx1YXRpb25zL3Rlc3QvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy90ZXN0LWRldGFpbC90ZXN0LWRldGFpbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3Rlc3QtZGV0YWlsL0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhcHBcXGV2YWx1YXRpb25zXFx0ZXN0XFxpbmZyYXN0cnVjdHVyZVxcbmctY29tcG9uZW50c1xcdGVzdC1kZXRhaWxcXHRlc3QtZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLG9DQUFBO0FBUUEsd0JBQUE7QUFRQSx1QkFBQTtBQUdBLDJCQUFBO0FBRUE7RUFDRSw2QkFBQTtBQ2hCRjtBRGtCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDaEJKO0FEa0JJO0VBQ0UsbUJBM0JRO0VBNEJSLGdCQUFBO0VBQ0EsY0FBQTtBQ2hCTjtBRHNCQTtFQUNFLGFBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtBQ25CRjtBRHFCRTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDbkJKO0FEcUJJO0VBQ0UsWUFBQTtBQ25CTjtBRHNCRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtBQ3BCSjtBRHdCQTtFQUNFLDZCQUFBO0VBQ0EsZUFBQTtBQ3JCRjtBRHVCRTtFQUNFLFNBQUE7QUNyQko7QUR3QkU7RUFDRSxnQ0FBQTtBQ3RCSjtBRHlCRTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDhCQUFBO0FDdkJKO0FEeUJJO0VBQ0UsbUJBM0VRO0VBNEVSLGdCQUFBO0VBQ0EsY0FBQTtBQ3ZCTjtBRDBCSTtFQUNFLHlCQTVFaUI7RUE2RWpCLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUN4Qk47QUQ2QkE7RUFDRSxjQWxGbUI7QUN3RHJCO0FENkJBO0VBQ0UsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FDMUJGO0FENkJBO0VBQ0UsV0FBQTtBQzFCRjtBRDZCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQzFCRjtBRDZCQTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUMxQkY7QUQ0QkU7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUMxQko7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtBQzNCRjtBRDhCQTtFQUNFLGVBQUE7QUMzQkY7QUQ4QkE7RUFDRSx1QkFBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQzNCRjtBRGtDQTtFQUNFLGNBQUE7QUMvQkY7QURrQ0EseUJBQUE7QUFFQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbURBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0VBQ0EseUJBQUE7QUNoQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UsY0FBQTtBQ2xDRjtBRHFDQSxZQUFBO0FBRUE7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLHVCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsZUFBQTtBQ25DRjtBRHNDQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsMENBQUE7QUNuQ0Y7QUNsTUE7RUFDSSxpQkFBQTtBRHFNSjtBQ2xNQTtFQUNJLGFBQUE7RUFDQSxXQUFBO0FEcU1KO0FDbE1BO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBRHFNSjtBQ2xNQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QURxTUo7QUNsTUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7QURxTUo7QUNsTUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLDhCQUFBO0FEcU1KO0FDbE1BO0VBQ0k7SUFDSSxnQkFBQTtJQUNBLGlCQUFBO0VEcU1OO0FBQ0Y7QUNsTUE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QURvTUo7QUNuTUk7RUFDSSxnQkFBQTtBRHFNUjtBQ25NSTtFQUNJLGNBQUE7QURxTVI7QUNqTUE7RUFDSSxhQUFBO0FEb01KO0FDbE1RO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FEb01aO0FDaE1JO0VBQ0ksY0FBQTtBRGtNUjtBQy9MQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7QURrTUo7QUNoTUk7RUFDSSxnQkFBQTtBRGtNUjtBQzlMQTtFQUNJLGdCQUFBO0VBQ0EsaUJBQUE7QURpTUo7QUMvTEk7RUFDSSxnQkFBQTtBRGlNUjtBQzdMQTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtBRGdNSjtBQy9MSTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtBRGlNUjtBQy9MUTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FEaU1aO0FDNUxBO0VBQ0ksVUFBQTtBRCtMSjtBQzVMQTtFQUNJLFVBQUE7QUQrTEo7QUM1TEE7RUFDSSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FEK0xKO0FDNUxBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FEK0xKO0FDM0xBOztDQUFBO0FBSUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7QUQ2TEo7QUM1TEk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtBRDhMUjtBQzNMSTtFQUNJO0lBQ0ksVUFBQTtFRDZMVjtBQUNGO0FDekxBO0VBQ0ksYUFBQTtFQUNBLDZCQUFBO0VBQ0EscUJBQUE7QUQ0TEo7QUMzTEk7RUFDSSxtQkFBQTtFQUNBLFlBQUE7QUQ2TFI7QUMzTEk7RUFDSSx5QkFBQTtBRDZMUjtBQzNMSTtFQUNJLHlCQUFBO0FENkxSO0FDM0xJO0VBQ0kseUJBQUE7QUQ2TFI7QUN6TEE7RUFDSSxZQUFBO0VBQ0Esa0NBQUE7RUFDQSxZQUFBO0FENExKO0FDekxBO0VBQ0ksYUFBQTtFQUNBLGlCQUFBO0VBQ0EsMkJBQUE7QUQ0TEo7QUMzTEk7RUFDSSxpQkFBQTtBRDZMUjtBQ3pMQTtFQUNJLFlBQUE7QUQ0TEo7QUMxTEk7RUFDSSxhQUFBO0FENExSO0FDMUxRO0VBQ0ksVUFBQTtBRDRMWjtBQzNMWTtFQUNJLGdCQUFBO0FENkxoQjtBQzFMUTtFQUNJLFVBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtBRDRMWjtBQzNMWTtFQUNJLGNBQUE7QUQ2TGhCO0FDeExBO0VBQ0ksYUFBQTtBRDJMSjtBQzFMSTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUQ0TFI7QUN2TEk7RUFDSSxpREFBQTtFQUVBLGVBQUE7QUR5TFI7QUN2TEk7RUFHSSxzQkFBQTtFQUNBLHlCQUFBO0FEeUxSO0FDckxBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSw4QkFBQTtBRHdMSjtBQ3RMSTtFQUNJLGlCQUFBO0FEd0xSIiwiZmlsZSI6InNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3Rlc3QtZGV0YWlsL3Rlc3QtZGV0YWlsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xyXG4kc21hbGw6IDAuOHJlbTtcclxuJG1lZGl1bTogMXJlbTtcclxuJGxhcmdlOiAxLjI1cmVtO1xyXG4kZXh0cmEtbGFyZ2U6IDEuNTYzcmVtO1xyXG4kaHVnZTogMS45NTNyZW07XHJcbiRleHRyYS1odWdlOiAyLjQ0MXJlbTtcclxuXHJcbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cclxuJGNvbG9yLXBhbGV0dGUtMS1tYWluOiAjREU3MzczO1xyXG4kY29sb3ItcGFsZXR0ZS0xLXNlY29uZDogI0FCQUJGRjtcclxuJGNvbG9yLXByaW1hcnktbGlnaHQ6ICNFRkVGRUY7XHJcbiRjb2xvci1wcmltYXJ5LWRhcms6ICMxOTE5MTk7XHJcbiRjb2xvci1zZWNvbmRhcnktZGFyazogIzMzMzMzMztcclxuJGNvbG9yLXNjcm9sbGJhci10aHVtYjogIzhDOEM4QztcclxuXHJcbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xyXG5cclxuXHJcbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cclxuXHJcbi5jLWNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuXHJcbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAuZHluLWZpbHRlcnMge1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG4gICAgb3ZlcmZsb3cteDogYXV0bztcclxuICAgIG1hcmdpbjogMCA0MHB4IDAgMDtcclxuXHJcbiAgICA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xyXG4gICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb25maWdCdXR0b24ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gIH1cclxufVxyXG5cclxuLmNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuICBtYXgtaGVpZ2h0OiA4OCU7XHJcblxyXG4gIC5jYXJkLWNvbnRlbnQsIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuXHJcbiAgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXBhbGV0dGUtMS1tYWluO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycmVtO1xyXG4gICAgICBoZWlnaHQ6IDIuM3JlbTtcclxuICAgICAgY29sb3I6ICNlZmVmZWY7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xyXG4gIGNvbG9yOiAkY29sb3ItcHJpbWFyeS1kYXJrO1xyXG59XHJcblxyXG4uZmlsdGVyIHtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcclxuICBtYXgtd2lkdGg6IDEwZW07XHJcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gIC8vIG1heC1oZWlnaHQ6IDgwMHB4O1xyXG4gIC5tYXQtdGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICB9XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItcm93IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XHJcbiAgY29sb3I6ICM4YjhiOGI7XHJcbn1cclxuXHJcbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xyXG4gIHBhZGRpbmctbGVmdDogMDtcclxufVxyXG5cclxuLmlkSGVhZGVyIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5pZENlbGwge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxubWF0LWNhcmQtY29udGVudCB7XHJcbiAgbWFyZ2luOiAwIDM0cHg7XHJcbn1cclxuXHJcbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXHJcblxyXG4ucHJpb3JpdHkge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLy8gQFRPRE86IGNoYW5nZSBjb2xvciB0byB2YXJpYWJsZXNcclxuXHJcbi5wcmlvcml0eS1tdXlhbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xyXG59XHJcblxyXG4ucHJpb3JpdHktYWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcclxufVxyXG5cclxuLnByaW9yaXR5LW5vcm1hbCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcclxufVxyXG5cclxuLnByaW9yaXR5LWJhamEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1uaW5ndW5hIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4ucmVkLWRhdGUge1xyXG4gIGNvbG9yOiAjZWI0MTQxO1xyXG59XHJcblxyXG4vKiBNQVQgVEFCICovXHJcblxyXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcclxuICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIC4xMik7XHJcbn1cclxuIiwiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xuLyogPT09PT0gV0VJR0hUID09PT09ICovXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXG4uYy1jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG5cbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyB7XG4gIGZsZXgtZ3JvdzogMTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbiAgbWFyZ2luOiAwIDQwcHggMCAwO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxNTBweDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5jb25maWdCdXR0b24ge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBmbGV4LXNocmluazogMDtcbn1cblxuLmNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG4gIG1heC1oZWlnaHQ6IDg4JTtcbn1cbi5jYXJkLWNvbXBvbmVudCAuY2FyZC1jb250ZW50LCAuY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMDtcbn1cbi5jYXJkLWNvbXBvbmVudCA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNERTczNzM7XG4gIGJvcmRlci1yYWRpdXM6IDJyZW07XG4gIGhlaWdodDogMi4zcmVtO1xuICBjb2xvcjogI2VmZWZlZjtcbn1cblxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcbiAgY29sb3I6ICMxOTE5MTk7XG59XG5cbi5maWx0ZXIge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMC41ZW07XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcbiAgbWF4LXdpZHRoOiAxMGVtO1xuICBtYXJnaW4tcmlnaHQ6IDVlbTtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93LXg6IGF1dG87XG59XG4udGFibGUtcmVzcG9uc2l2ZSAubWF0LXRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgZGlzcGxheTogdGFibGU7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4ubWF0LWhlYWRlci1yb3cge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xuICBjb2xvcjogIzhiOGI4Yjtcbn1cblxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cblxuLmlkSGVhZGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmlkQ2VsbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG5tYXQtY2FyZC1jb250ZW50IHtcbiAgbWFyZ2luOiAwIDM0cHg7XG59XG5cbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXG4ucHJpb3JpdHkge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5wcmlvcml0eS1tdXlhbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcbn1cblxuLnByaW9yaXR5LWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xufVxuXG4ucHJpb3JpdHktbm9ybWFsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcbn1cblxuLnByaW9yaXR5LWJhamEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xufVxuXG4ucHJpb3JpdHktbmluZ3VuYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5yZWQtZGF0ZSB7XG4gIGNvbG9yOiAjZWI0MTQxO1xufVxuXG4vKiBNQVQgVEFCICovXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG59XG5cbi50aXRsZS1jb21wb25lbnQge1xuICBtYXJnaW4tbGVmdDogMnJlbTtcbn1cblxuLnRlc3QtZGV0YWlsLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZWRpdC1idXR0b24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNDVweDtcbiAgcmlnaHQ6IDQ1cHg7XG59XG5cbi50ZXN0LWRldGFpbC1jb21wb25lbnQge1xuICBtYXJnaW4tbGVmdDogM2VtO1xuICBtYXJnaW4tcmlnaHQ6IDNlbTtcbiAgbWFyZ2luLWJvdHRvbTogM2VtO1xuICB3aWR0aDogNjAlO1xufVxuXG4uY291bnRlci1saXN0LWNvbnRhaW5lciB7XG4gIG1hcmdpbi1ib3R0b206IDNlbTtcbiAgd2lkdGg6IDI1JTtcbn1cblxuLnRpdGxlLWNvdW50ZXItY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgZmxleC13cmFwOiBub3dyYXA7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDE3NTBweCkge1xuICAubmV3LWNvcnJlY3Rpb24tY29tcG9uZW50IHtcbiAgICBtYXJnaW4tbGVmdDogM2VtO1xuICAgIG1hcmdpbi1yaWdodDogM2VtO1xuICB9XG59XG4udGVzdC1kZXNjcmlwdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi50ZXN0LWRlc2NyaXB0aW9uIHNwYW4ge1xuICBmb250LXdlaWdodDogNTAwO1xufVxuLnRlc3QtZGVzY3JpcHRpb24gPiBkaXYge1xuICBwYWRkaW5nOiA1cHggMDtcbn1cblxuLnF1ZXN0aW9uIHtcbiAgcGFkZGluZzogMTBweDtcbn1cbi5xdWVzdGlvbiAucXVlc3Rpb24tc3RhdGVtZW50ID4gc3BhbiB7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5xdWVzdGlvbiA+IGRpdiB7XG4gIHBhZGRpbmc6IDJweCAwO1xufVxuXG4ucXVlc3Rpb24tZGVzY3JpcHRpb24ge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cbi5xdWVzdGlvbi1kZXNjcmlwdGlvbiBzcGFuIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLnF1ZXN0aW9uLXN0ZXAge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cbi5xdWVzdGlvbi1zdGVwIHNwYW4ge1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4ucXVlc3Rpb24tcmVzcG9uc2Uge1xuICBwYWRkaW5nOiA1cHggMDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5xdWVzdGlvbi1yZXNwb25zZSAub25lLWFuc3dlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5xdWVzdGlvbi1yZXNwb25zZSAub25lLWFuc3dlciBtYXQtZm9ybS1maWVsZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWF4LXdpZHRoOiA1ZW07XG4gIGZvbnQtc2l6ZTogMC44ZW07XG59XG5cbi5tYXQtZm9ybS1maWVsZC1pbmZpeCB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXQtZm9ybS1maWVsZC1pbmZpeCAubmctdG5zLWM4NC01IHtcbiAgcGFkZGluZzogMDtcbn1cblxuLmFuc3dlci10ZXh0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW4tbGVmdDogNTBweDtcbn1cblxuLnNpcmN1bG8ge1xuICBoZWlnaHQ6IDE1cHg7XG4gIHdpZHRoOiAxNXB4O1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbn1cblxuLypcbiAgICBDT1VOVEVSU1xuKi9cbi5jb3JyZWN0aW9uLXBhbmVsLCAuZm9ybS1jb3VudGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbn1cbi5jb3JyZWN0aW9uLXBhbmVsIG1hdC1mb3JtLWZpZWxkLCAuZm9ybS1jb3VudGVyIG1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDg0JTtcbiAgbWFyZ2luOiBhdXRvO1xufVxuQG1lZGlhIChtYXgtd2lkdGg6IDE3NTBweCkge1xuICAuY29ycmVjdGlvbi1wYW5lbCBtYXQtZm9ybS1maWVsZCwgLmZvcm0tY291bnRlciBtYXQtZm9ybS1maWVsZCB7XG4gICAgd2lkdGg6IDc5JTtcbiAgfVxufVxuXG4uY291bnRlci1saXN0LWJ1dHRvbnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbiAgbWFyZ2luOiA0MHB4IDAgMjBweCAwO1xufVxuLmNvdW50ZXItbGlzdC1idXR0b25zIC5idXR0b24tY291bnRlciB7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5jb3VudGVyLWxpc3QtYnV0dG9ucyAuc2F2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNERTczNzM7XG59XG4uY291bnRlci1saXN0LWJ1dHRvbnMgLmNhbmNlbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM3Njc2NzY7XG59XG4uY291bnRlci1saXN0LWJ1dHRvbnMgLnJlc2V0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcbn1cblxuLmRpdmlkZS1jb3VudGVycyB7XG4gIGhlaWdodDogMzBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JleTtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4udGl0bGUtY291bnRlcnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW4tbGVmdDogMzBweDtcbiAgbWFyZ2luLXRvcDogMzVweCAhaW1wb3J0YW50O1xufVxuLnRpdGxlLWNvdW50ZXJzIC5tYXRlcmlhbC1pY29ucyB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4uY291bnRlci1jYXJkIHtcbiAgbWFyZ2luOiAxNXB4O1xufVxuLmNvdW50ZXItY2FyZCAuY291bnRlci1jYXJkLWNvbnRlbnQge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLmNvdW50ZXItY2FyZCAuY291bnRlci1jYXJkLWNvbnRlbnQgLmNvdW50ZXItY2FyZC10ZXh0IHtcbiAgd2lkdGg6IDcwJTtcbn1cbi5jb3VudGVyLWNhcmQgLmNvdW50ZXItY2FyZC1jb250ZW50IC5jb3VudGVyLWNhcmQtdGV4dCAuY291bnRlci1uYW1lIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbi5jb3VudGVyLWNhcmQgLmNvdW50ZXItY2FyZC1jb250ZW50IC5jb3VudGVyLWNhcmQtYnV0dG9ucyB7XG4gIHdpZHRoOiAzMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cbi5jb3VudGVyLWNhcmQgLmNvdW50ZXItY2FyZC1jb250ZW50IC5jb3VudGVyLWNhcmQtYnV0dG9ucyAuc2F2ZS1pY29uIHtcbiAgY29sb3I6ICNERTczNzM7XG59XG5cbi5jb3VudGVyLWlucHV0cyB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uY291bnRlci1pbnB1dHMgLm1hdGVyaWFsLWljb25zIHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBtYXJnaW4tdG9wOiAxM3B4O1xuICBjb2xvcjogIzk4OTg5ODtcbn1cblxuLmNvdW50ZXItbGlzdCBtYXQtY2FyZCB7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjVzLCBiYWNrZ3JvdW5kLWNvbG9yIDAuMnM7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5jb3VudGVyLWxpc3QgbWF0LWNhcmQ6aG92ZXIge1xuICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMC45NSk7XG4gIC1tcy10cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xuICB0cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlZWVlO1xufVxuXG4uYWN0aW9ucy1iYXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBmbGV4LXdyYXA6IG5vd3JhcDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmFjdGlvbnMtYmFyIC5jb3JyZWN0aW9ucy1udW1iZXIge1xuICBtYXJnaW4tbGVmdDogMTVweDtcbn0iLCJAaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL3N0YWZmaXQtc3R5bGUtMSc7XHJcblxyXG4udGl0bGUtY29tcG9uZW50IHtcclxuICAgIG1hcmdpbi1sZWZ0OiAycmVtO1xyXG59XHJcblxyXG4udGVzdC1kZXRhaWwtY29udGFpbmVye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZWRpdC1idXR0b257XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDQ1cHg7XHJcbiAgICByaWdodDogNDVweDtcclxufVxyXG5cclxuLnRlc3QtZGV0YWlsLWNvbXBvbmVudHtcclxuICAgIG1hcmdpbi1sZWZ0OjNlbTtcclxuICAgIG1hcmdpbi1yaWdodDogM2VtO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogM2VtO1xyXG4gICAgd2lkdGg6IDYwJTtcclxufVxyXG5cclxuLmNvdW50ZXItbGlzdC1jb250YWluZXJ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzZW07XHJcbiAgICB3aWR0aDogMjUlO1xyXG59XHJcblxyXG4udGl0bGUtY291bnRlci1jb250YWluZXJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogMTc1MHB4KSB7XHJcbiAgICAubmV3LWNvcnJlY3Rpb24tY29tcG9uZW50e1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OjNlbTtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDNlbTtcclxuICAgIH1cclxufVxyXG5cclxuLnRlc3QtZGVzY3JpcHRpb257XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgc3BhbntcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgfVxyXG4gICAgPiBkaXZ7XHJcbiAgICAgICAgcGFkZGluZzogNXB4IDA7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5xdWVzdGlvbntcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAucXVlc3Rpb24tc3RhdGVtZW50e1xyXG4gICAgICAgID5zcGFue1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgID4gZGl2e1xyXG4gICAgICAgIHBhZGRpbmc6IDJweCAwO1xyXG4gICAgfVxyXG59XHJcbi5xdWVzdGlvbi1kZXNjcmlwdGlvbntcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIFxyXG4gICAgc3BhbntcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgfVxyXG59XHJcblxyXG4ucXVlc3Rpb24tc3RlcHtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIFxyXG4gICAgc3BhbntcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgfVxyXG59XHJcblxyXG4ucXVlc3Rpb24tcmVzcG9uc2V7XHJcbiAgICBwYWRkaW5nOiA1cHggMDtcclxuICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAub25lLWFuc3dlcntcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICBcclxuICAgICAgICBtYXQtZm9ybS1maWVsZHtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXgtd2lkdGg6IDVlbTtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAuOGVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4e1xyXG4gICAgcGFkZGluZzogMDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkLWluZml4IC5uZy10bnMtYzg0LTV7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG4uYW5zd2VyLXRleHR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbn1cclxuXHJcbi5zaXJjdWxve1xyXG4gICAgaGVpZ2h0OiAxNXB4O1xyXG4gICAgd2lkdGg6IDE1cHg7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuXHJcblxyXG4vKlxyXG4gICAgQ09VTlRFUlNcclxuKi9cclxuXHJcbi5jb3JyZWN0aW9uLXBhbmVsLCAuZm9ybS1jb3VudGVye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBtYXQtZm9ybS1maWVsZHtcclxuICAgICAgICB3aWR0aDogODQlO1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgIH1cclxuXHJcbiAgICBAbWVkaWEobWF4LXdpZHRoOiAxNzUwcHgpe1xyXG4gICAgICAgIG1hdC1mb3JtLWZpZWxke1xyXG4gICAgICAgICAgICB3aWR0aDogNzklO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmNvdW50ZXItbGlzdC1idXR0b25ze1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgbWFyZ2luOiA0MHB4IDAgMjBweCAwO1xyXG4gICAgLmJ1dHRvbi1jb3VudGVye1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG4gICAgLnNhdmV7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI0RFNzM3MztcclxuICAgIH1cclxuICAgIC5jYW5jZWx7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzc2NzY3NjtcclxuICAgIH1cclxuICAgIC5yZXNldHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xyXG4gICAgfVxyXG59XHJcblxyXG4uZGl2aWRlLWNvdW50ZXJze1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JleTtcclxuICAgIG1hcmdpbjogYXV0bztcclxufVxyXG5cclxuLnRpdGxlLWNvdW50ZXJze1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMzVweCAhaW1wb3J0YW50O1xyXG4gICAgLm1hdGVyaWFsLWljb25ze1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uY291bnRlci1jYXJke1xyXG4gICAgbWFyZ2luOiAxNXB4O1xyXG5cclxuICAgIC5jb3VudGVyLWNhcmQtY29udGVudHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG5cclxuICAgICAgICAuY291bnRlci1jYXJkLXRleHR7XHJcbiAgICAgICAgICAgIHdpZHRoOiA3MCU7XHJcbiAgICAgICAgICAgIC5jb3VudGVyLW5hbWV7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5jb3VudGVyLWNhcmQtYnV0dG9uc3tcclxuICAgICAgICAgICAgd2lkdGg6IDMwJTtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgZmxleC13cmFwOiB3cmFwOztcclxuICAgICAgICAgICAgLnNhdmUtaWNvbntcclxuICAgICAgICAgICAgICAgIGNvbG9yOiAjREU3MzczO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbi5jb3VudGVyLWlucHV0c3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAubWF0ZXJpYWwtaWNvbnN7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEzcHg7XHJcbiAgICAgICAgY29sb3I6ICM5ODk4OTg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jb3VudGVyLWxpc3R7XHJcbiAgICBtYXQtY2FyZHtcclxuICAgICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyxcclxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yIDAuMnM7XHJcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgfVxyXG4gICAgbWF0LWNhcmQ6aG92ZXJ7XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xyXG4gICAgICAgIC1tcy10cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xyXG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMC45NSk7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIzOCwgMjM4LCAyMzgpO1xyXG4gICAgfVxyXG59XHJcblxyXG4uYWN0aW9ucy1iYXJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIC5jb3JyZWN0aW9ucy1udW1iZXJ7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestDetailComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-test-detail',
          templateUrl: './test-detail.component.html',
          styleUrls: ['./test-detail.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: src_app_evaluations_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_4__["CorrectionService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/ng-components/test-management/test-management.component.ts":
  /*!************************************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/ng-components/test-management/test-management.component.ts ***!
    \************************************************************************************************************/

  /*! exports provided: TestManagementComponent */

  /***/
  function srcAppEvaluationsTestInfrastructureNgComponentsTestManagementTestManagementComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestManagementComponent", function () {
      return TestManagementComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _application_searchTests__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../application/searchTests */
    "./src/app/evaluations/test/application/searchTests.ts");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic */
    "./src/app/shared/custom-mat-elements/dynamic.ts");
    /* harmony import */


    var _test_table_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./test-table/config */
    "./src/app/evaluations/test/infrastructure/ng-components/test-management/test-table/config.ts");
    /* harmony import */


    var _services_test_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _auth_session_infrastructure_services_session_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../../../auth/session/infrastructure/services/session.service */
    "./src/app/auth/session/infrastructure/services/session.service.ts");
    /* harmony import */


    var _TestMapper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../TestMapper */
    "./src/app/evaluations/test/infrastructure/TestMapper.ts");
    /* harmony import */


    var src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/auth/session/infrastructure/services/cookie.service */
    "./src/app/auth/session/infrastructure/services/cookie.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component */
    "./src/app/shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component.ts");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-filters/dynamic-filters.component */
    "./src/app/shared/custom-mat-elements/dynamic-filters/dynamic-filters.component.ts");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component */
    "./src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component.ts");

    var _c0 = ["table"];

    function TestManagementComponent_mat_card_0_Template(rf, ctx) {
      if (rf & 1) {
        var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h2");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "TESTS");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " New ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "app-dynamic-filters", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("query", function TestManagementComponent_mat_card_0_Template_app_dynamic_filters_query_9_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r2.filtersChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestManagementComponent_mat_card_0_Template_button_click_10_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r4.setConfigVisibility(!ctx_r4.configVisibility);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, " Settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "app-dynamic-table-mark-ii", 8, 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("action", function TestManagementComponent_mat_card_0_Template_app_dynamic_table_mark_ii_action_14_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r5.doAction($event);
        })("pagination", function TestManagementComponent_mat_card_0_Template_app_dynamic_table_mark_ii_pagination_14_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r6.paginationChanged($event);
        })("order", function TestManagementComponent_mat_card_0_Template_app_dynamic_table_mark_ii_order_14_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r7.orderChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("filters", ctx_r0.filters)("showFilters", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("config", ctx_r0.tableConfig)("data", ctx_r0.tableData);
      }
    }

    var TestManagementComponent = /*#__PURE__*/function () {
      function TestManagementComponent(_testService, _router, _sessionService, _route, _testMapper, _cookieFacade) {
        _classCallCheck(this, TestManagementComponent);

        this._testService = _testService;
        this._router = _router;
        this._sessionService = _sessionService;
        this._route = _route;
        this._testMapper = _testMapper;
        this._cookieFacade = _cookieFacade;
        this.filters = [];
        this.tableConfig = _test_table_config__WEBPACK_IMPORTED_MODULE_3__["table"];
        this.configVisibility = false;
        this.loading = true;
        this.query = {};
      }

      _createClass(TestManagementComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _a, _b, _c, _d;

          this._resolved = this._route.snapshot.data.response;

          if (this._resolved.data) {
            this._buildFilters();

            Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_2__["parseTableFiltersConfig"])(this._resolved.data.tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = this._resolved.data.filtersValue ? this._clean(JSON.parse(this._resolved.data.filtersValue)) : {};
            this.tableConfig.pagination = (_b = (_a = this._resolved.data.tableFiltersConfig) === null || _a === void 0 ? void 0 : _a.pagination) !== null && _b !== void 0 ? _b : {
              page: 0,
              size: 10
            };
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = (_d = (_c = this._resolved.data.tableFiltersConfig) === null || _c === void 0 ? void 0 : _c.order) !== null && _d !== void 0 ? _d : {
              orderField: '',
              order: ''
            };
            this.queryOrder = Object.assign({}, this.tableConfig.order);
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ? Object.entries(this._testMapper.mapTo(_defineProperty({}, this.tableConfig.order.orderField, ''))).find(function (entry) {
              return entry[1] === '';
            })[0] : '';
            this.tableData = this._resolved.data.tests;
            this.loading = false;
          }

          this.configFilterValue();
        }
      }, {
        key: "configFilterValue",
        value: function configFilterValue() {
          var _this43 = this;

          var queryFiltersAux = this._resolved.data.filtersValue ? this._clean(this._testMapper.mapTo(JSON.parse(this._resolved.data.filtersValue))) : {};
          this.filters.forEach(function (filter) {
            filter.defaultValue = queryFiltersAux[_this43.getFilterProp(filter)];
            filter.value = queryFiltersAux[_this43.getFilterProp(filter)];
          });
        }
      }, {
        key: "getFilterProp",
        value: function getFilterProp(filter) {
          return filter.prop ? filter.type === 'autoselect' ? filter.searchValue : filter.prop : filter.searchValue;
        }
      }, {
        key: "_clean",
        value: function _clean(object) {
          var cleaned = {};
          var keys = Object.keys(object);
          keys.forEach(function (key) {
            if (object[key]) {
              cleaned[key] = object[key];
            }
          });
          return cleaned;
        }
      }, {
        key: "doAction",
        value: function doAction(event) {
          if (event.action === 'Add Correction') {
            this._router.navigate(["test-correction/".concat(event.item.id)], {
              relativeTo: this._route
            });
          }
        }
      }, {
        key: "navigate",
        value: function navigate(event) {
          if (event.property === 'id') {
            this._router.navigate(["test-detail/".concat(event.id)], {
              relativeTo: this._route
            });
          }
        }
      }, {
        key: "setSize",
        value: function setSize(event) {
          this.backSearch();
        }
      }, {
        key: "backSearch",
        value: function backSearch() {
          var _this44 = this;

          Object(_application_searchTests__WEBPACK_IMPORTED_MODULE_1__["searchTests"])(Object.assign(Object.assign(Object.assign({}, this.queryPagination), this.queryOrder), this.queryFilters), this._testService).subscribe(function (result) {
            _this44.tableData = {
              content: result.content,
              totalElements: result.totalElements,
              numberOfElements: result.numberOfElements,
              totalPages: result.totalPages
            };
          });
        }
      }, {
        key: "filtersChanged",
        value: function filtersChanged(params) {
          if (params && Object.keys(params).length !== 0) {
            var mapped = this._testMapper.mapFrom(params);

            this.queryFilters = mapped;
          } else {
            this.queryFilters = {};
          }

          this._cookieFacade.save('testFilterCache', JSON.stringify(this.queryFilters));

          this.queryPagination = {
            page: 0,
            size: this.queryPagination.size
          };
          this.dynamicTable.resetPageIndex();

          this._cookieFacade.save('test-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_2__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "paginationChanged",
        value: function paginationChanged(event) {
          this.queryPagination.page = event.page;
          this.queryPagination.size = event.size;

          this._cookieFacade.save('test-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_2__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "orderChanged",
        value: function orderChanged(event) {
          var aux = _defineProperty({}, event.orderField, '');

          aux = this._testMapper.mapFrom(aux);
          this.queryOrder.orderField = Object.entries(aux).find(function (entry) {
            return entry[1] === '';
          })[0];
          this.queryOrder.order = event.order;
          this.queryPagination.page = event.page;
          this.queryPagination.size = event.size;

          this._cookieFacade.save('test-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_2__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "configChange",
        value: function configChange(event) {
          this.tableConfig = Object.assign({}, event[0]);
          this.filters = event[1];

          this._cookieFacade.save('test-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_2__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        }
      }, {
        key: "setConfigVisibility",
        value: function setConfigVisibility(visible) {
          this.configVisibility = visible;
        }
      }, {
        key: "_buildFilters",
        value: function _buildFilters() {
          this.filters = [{
            options: [],
            prop: 'id',
            retProp: '',
            type: 'input',
            appearance: 'outline',
            "class": 'input',
            label: 'Id',
            placeholder: 'Id',
            shown: true
          }, {
            options: [],
            prop: 'name',
            retProp: '',
            type: 'input',
            appearance: 'outline',
            "class": 'input',
            label: 'Name',
            placeholder: 'Name',
            shown: true
          }, {
            options: [],
            prop: 'description',
            retProp: '',
            type: 'input',
            appearance: 'outline',
            "class": 'input',
            label: 'Description',
            placeholder: 'Description',
            shown: true
          }];
        }
      }]);

      return TestManagementComponent;
    }();

    TestManagementComponent.ɵfac = function TestManagementComponent_Factory(t) {
      return new (t || TestManagementComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_test_service__WEBPACK_IMPORTED_MODULE_4__["TestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_session_infrastructure_services_session_service__WEBPACK_IMPORTED_MODULE_6__["SessionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_TestMapper__WEBPACK_IMPORTED_MODULE_7__["TestMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_8__["CookieFacadeService"]));
    };

    TestManagementComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TestManagementComponent,
      selectors: [["app-test-management"]],
      viewQuery: function TestManagementComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dynamicTable = _t.first);
        }
      },
      decls: 2,
      vars: 4,
      consts: [["class", "card-component", 4, "ngIf"], [3, "visibility", "columnsConfig", "filtersConfig", "configChanged", "closed"], [1, "card-component"], [1, "title-component"], ["mat-flat-button", "", "routerLink", "../new"], [1, "filters-plus-config"], [1, "dyn-filters", 3, "filters", "showFilters", "query"], ["mat-button", "", 1, "configButton", 3, "click"], [1, "card-content", 3, "config", "data", "action", "pagination", "order"], ["table", ""]],
      template: function TestManagementComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, TestManagementComponent_mat_card_0_Template, 16, 4, "mat-card", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-dynamic-table-config-panel", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("configChanged", function TestManagementComponent_Template_app_dynamic_table_config_panel_configChanged_1_listener($event) {
            return ctx.configChange($event);
          })("closed", function TestManagementComponent_Template_app_dynamic_table_config_panel_closed_1_listener() {
            return ctx.setConfigVisibility(false);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visibility", ctx.configVisibility)("columnsConfig", ctx.tableConfig)("filtersConfig", ctx.filters);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_10__["DynamicTableConfigPanelComponent"], _angular_material_card__WEBPACK_IMPORTED_MODULE_11__["MatCard"], _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLink"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_13__["MatIcon"], _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_14__["DynamicFiltersComponent"], _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_15__["DynamicTableMarkIiComponent"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3Rlc3QtbWFuYWdlbWVudC9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXNzZXRzXFxzdHlsZXNcXHN0YWZmaXQtc3R5bGUtMS5zY3NzIiwic3JjL2FwcC9ldmFsdWF0aW9ucy90ZXN0L2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvdGVzdC1tYW5hZ2VtZW50L3Rlc3QtbWFuYWdlbWVudC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQ0FBQTtBQVFBLHdCQUFBO0FBUUEsdUJBQUE7QUFHQSwyQkFBQTtBQUVBO0VBQ0UsNkJBQUE7QUNoQkY7QURrQkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ2hCSjtBRGtCSTtFQUNFLG1CQTNCUTtFQTRCUixnQkFBQTtFQUNBLGNBQUE7QUNoQk47QURzQkE7RUFDRSxhQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNuQkY7QURxQkU7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ25CSjtBRHFCSTtFQUNFLFlBQUE7QUNuQk47QURzQkU7RUFDRSxtQkFBQTtFQUNBLGNBQUE7QUNwQko7QUR3QkE7RUFDRSw2QkFBQTtFQUNBLGVBQUE7QUNyQkY7QUR1QkU7RUFDRSxTQUFBO0FDckJKO0FEd0JFO0VBQ0UsZ0NBQUE7QUN0Qko7QUR5QkU7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSw4QkFBQTtBQ3ZCSjtBRHlCSTtFQUNFLG1CQTNFUTtFQTRFUixnQkFBQTtFQUNBLGNBQUE7QUN2Qk47QUQwQkk7RUFDRSx5QkE1RWlCO0VBNkVqQixtQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FDeEJOO0FENkJBO0VBQ0UsY0FsRm1CO0FDd0RyQjtBRDZCQTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQzFCRjtBRDZCQTtFQUNFLFdBQUE7QUMxQkY7QUQ2QkE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDMUJGO0FENEJFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDMUJKO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUMzQkY7QUQ4QkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUMzQkY7QUQ4QkE7RUFDRSxlQUFBO0FDM0JGO0FEOEJBO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0Usa0JBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7QUMzQkY7QURrQ0E7RUFDRSxjQUFBO0FDL0JGO0FEa0NBLHlCQUFBO0FBRUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1EQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLHlCQUFBO0FDaENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLGNBQUE7QUNsQ0Y7QURxQ0EsWUFBQTtBQUVBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSx1QkFBQTtBQ25DRjtBRHNDQTtFQUNFLGVBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxpQkFBQTtBQ25DRjtBRHNDQTtFQUNFLDBDQUFBO0FDbkNGIiwiZmlsZSI6InNyYy9hcHAvZXZhbHVhdGlvbnMvdGVzdC9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3Rlc3QtbWFuYWdlbWVudC90ZXN0LW1hbmFnZW1lbnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiA9PT09PSBGT05UUyAoTWFqb3IgVGhpcmQpID09PT09ICovXHJcbiRzbWFsbDogMC44cmVtO1xyXG4kbWVkaXVtOiAxcmVtO1xyXG4kbGFyZ2U6IDEuMjVyZW07XHJcbiRleHRyYS1sYXJnZTogMS41NjNyZW07XHJcbiRodWdlOiAxLjk1M3JlbTtcclxuJGV4dHJhLWh1Z2U6IDIuNDQxcmVtO1xyXG5cclxuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xyXG4kY29sb3ItcGFsZXR0ZS0xLW1haW46ICNERTczNzM7XHJcbiRjb2xvci1wYWxldHRlLTEtc2Vjb25kOiAjQUJBQkZGO1xyXG4kY29sb3ItcHJpbWFyeS1saWdodDogI0VGRUZFRjtcclxuJGNvbG9yLXByaW1hcnktZGFyazogIzE5MTkxOTtcclxuJGNvbG9yLXNlY29uZGFyeS1kYXJrOiAjMzMzMzMzO1xyXG4kY29sb3Itc2Nyb2xsYmFyLXRodW1iOiAjOEM4QzhDO1xyXG5cclxuLyogPT09PT0gV0VJR0hUID09PT09ICovXHJcblxyXG5cclxuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xyXG5cclxuLmMtY2FyZC1jb21wb25lbnQge1xyXG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5cclxuLmZpbHRlcnMtcGx1cy1jb25maWcge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1mbG93OiByb3c7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gIC5keW4tZmlsdGVycyB7XHJcbiAgICBmbGV4LWdyb3c6IDE7XHJcbiAgICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gICAgbWFyZ2luOiAwIDQwcHggMCAwO1xyXG5cclxuICAgIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XHJcbiAgICAgIHdpZHRoOiAxNTBweDtcclxuICAgIH1cclxuICB9XHJcbiAgLmNvbmZpZ0J1dHRvbiB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgZmxleC1zaHJpbms6IDA7XHJcbiAgfVxyXG59XHJcblxyXG4uY2FyZC1jb21wb25lbnQge1xyXG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xyXG4gIG1heC1oZWlnaHQ6IDg4JTtcclxuXHJcbiAgLmNhcmQtY29udGVudCwgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgfVxyXG5cclxuICA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcclxuICB9XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24ge1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkY29sb3ItcGFsZXR0ZS0xLW1haW47XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDJyZW07XHJcbiAgICAgIGhlaWdodDogMi4zcmVtO1xyXG4gICAgICBjb2xvcjogI2VmZWZlZjtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XHJcbiAgY29sb3I6ICRjb2xvci1wcmltYXJ5LWRhcms7XHJcbn1cclxuXHJcbi5maWx0ZXIge1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbm1hdC1mb3JtLWZpZWxkIG1hdC1zZWxlY3Qge1xyXG4gIG1heC13aWR0aDogMTBlbTtcclxuICBtYXJnaW4tcmlnaHQ6IDVlbTtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmUge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgLy8gbWF4LWhlaWdodDogODAwcHg7XHJcbiAgLm1hdC10YWJsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IHRhYmxlO1xyXG4gIH1cclxufVxyXG5cclxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4ubWF0LWhlYWRlci1yb3cge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcclxuICBjb2xvcjogIzhiOGI4YjtcclxufVxyXG5cclxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XHJcbiAgcGFkZGluZy1sZWZ0OiAwO1xyXG59XHJcblxyXG4uaWRIZWFkZXIge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmlkQ2VsbCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5tYXQtY2FyZC1jb250ZW50IHtcclxuICBtYXJnaW46IDAgMzRweDtcclxufVxyXG5cclxuLyogPT09PT0gUFJJT1JJVFkgPT09PT0gKi9cclxuXHJcbi5wcmlvcml0eSB7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgcGFkZGluZzogNXB4IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4vLyBAVE9ETzogY2hhbmdlIGNvbG9yIHRvIHZhcmlhYmxlc1xyXG5cclxuLnByaW9yaXR5LW11eWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1hbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbm9ybWFsIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xyXG59XHJcblxyXG4ucHJpb3JpdHktYmFqYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcclxufVxyXG5cclxuLnByaW9yaXR5LW5pbmd1bmEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi5yZWQtZGF0ZSB7XHJcbiAgY29sb3I6ICNlYjQxNDE7XHJcbn1cclxuXHJcbi8qIE1BVCBUQUIgKi9cclxuXHJcbjo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIHtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHNwYW4ge1xyXG4gIG1hcmdpbi1yaWdodDogNXB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcclxuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgLjEyKTtcclxufVxyXG4iLCIvKiA9PT09PSBGT05UUyAoTWFqb3IgVGhpcmQpID09PT09ICovXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cbi5jLWNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cblxuLmZpbHRlcnMtcGx1cy1jb25maWcge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIHtcbiAgZmxleC1ncm93OiAxO1xuICBvdmVyZmxvdy14OiBhdXRvO1xuICBtYXJnaW46IDAgNDBweCAwIDA7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDE1MHB4O1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmNvbmZpZ0J1dHRvbiB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGZsZXgtc2hyaW5rOiAwO1xufVxuXG4uY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbiAgbWF4LWhlaWdodDogODglO1xufVxuLmNhcmQtY29tcG9uZW50IC5jYXJkLWNvbnRlbnQsIC5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAwO1xufVxuLmNhcmQtY29tcG9uZW50IDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcbiAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RFNzM3MztcbiAgYm9yZGVyLXJhZGl1czogMnJlbTtcbiAgaGVpZ2h0OiAyLjNyZW07XG4gIGNvbG9yOiAjZWZlZmVmO1xufVxuXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xuICBjb2xvcjogIzE5MTkxOTtcbn1cblxuLmZpbHRlciB7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cblxuLm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbm1hdC1mb3JtLWZpZWxkIG1hdC1zZWxlY3Qge1xuICBtYXgtd2lkdGg6IDEwZW07XG4gIG1hcmdpbi1yaWdodDogNWVtO1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbn1cbi50YWJsZS1yZXNwb25zaXZlIC5tYXQtdGFibGUge1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiB0YWJsZTtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XG59XG5cbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XG59XG5cbi5tYXQtaGVhZGVyLXJvdyB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XG4gIGNvbG9yOiAjOGI4YjhiO1xufVxuXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcbiAgcGFkZGluZy1sZWZ0OiAwO1xufVxuXG4uaWRIZWFkZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uaWRDZWxsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbm1hdC1jYXJkLWNvbnRlbnQge1xuICBtYXJnaW46IDAgMzRweDtcbn1cblxuLyogPT09PT0gUFJJT1JJVFkgPT09PT0gKi9cbi5wcmlvcml0eSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBwYWRkaW5nOiA1cHggMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLnByaW9yaXR5LW11eWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xufVxuXG4ucHJpb3JpdHktYWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XG59XG5cbi5wcmlvcml0eS1ub3JtYWwge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjYjhlO1xufVxuXG4ucHJpb3JpdHktYmFqYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XG59XG5cbi5wcmlvcml0eS1uaW5ndW5hIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcbn1cblxuLnJlZC1kYXRlIHtcbiAgY29sb3I6ICNlYjQxNDE7XG59XG5cbi8qIE1BVCBUQUIgKi9cbjo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIHtcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xuICBmb250LXNpemU6IDEzcHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHNwYW4ge1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestManagementComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-test-management',
          templateUrl: './test-management.component.html',
          styleUrls: ['./test-management.component.scss']
        }]
      }], function () {
        return [{
          type: _services_test_service__WEBPACK_IMPORTED_MODULE_4__["TestService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: _auth_session_infrastructure_services_session_service__WEBPACK_IMPORTED_MODULE_6__["SessionService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]
        }, {
          type: _TestMapper__WEBPACK_IMPORTED_MODULE_7__["TestMapper"]
        }, {
          type: src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_8__["CookieFacadeService"]
        }];
      }, {
        dynamicTable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['table']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/ng-components/test-management/test-table/config.ts":
  /*!****************************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/ng-components/test-management/test-table/config.ts ***!
    \****************************************************************************************************/

  /*! exports provided: table */

  /***/
  function srcAppEvaluationsTestInfrastructureNgComponentsTestManagementTestTableConfigTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "table", function () {
      return table;
    });

    var table = {
      multiSelect: false,
      columns: [{
        name: 'Id',
        prop: 'id',
        shown: true,
        route: './app/evaluations/tests/test-detail/',
        cellClass: 'id-table-column',
        routeId: 'id'
      }, {
        name: 'Name',
        prop: 'name',
        shown: true
      }, {
        name: 'Description',
        prop: 'description',
        shown: true
      }],
      actions: [{
        name: 'Add Correction',
        icon: 'control_point'
      }]
    };
    /***/
  },

  /***/
  "./src/app/evaluations/test/infrastructure/ng-components/test-management/test-table/test-table.component.ts":
  /*!******************************************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/ng-components/test-management/test-table/test-table.component.ts ***!
    \******************************************************************************************************************/

  /*! exports provided: TestTableComponent */

  /***/
  function srcAppEvaluationsTestInfrastructureNgComponentsTestManagementTestTableTestTableComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestTableComponent", function () {
      return TestTableComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _application_searchTests__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../../application/searchTests */
    "./src/app/evaluations/test/application/searchTests.ts");
    /* harmony import */


    var _config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./config */
    "./src/app/evaluations/test/infrastructure/ng-components/test-management/test-table/config.ts");
    /* harmony import */


    var _services_test_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _auth_session_infrastructure_services_session_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../../../../auth/session/infrastructure/services/session.service */
    "./src/app/auth/session/infrastructure/services/session.service.ts");
    /* harmony import */


    var src_app_evaluations_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/evaluations/test/infrastructure/TestMapper */
    "./src/app/evaluations/test/infrastructure/TestMapper.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../../../../shared/custom-mat-elements/dynamic-filters/dynamic-filters.component */
    "./src/app/shared/custom-mat-elements/dynamic-filters/dynamic-filters.component.ts");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component */
    "./src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component.ts");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../../../shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component */
    "./src/app/shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component.ts");

    var _c0 = ["table"];

    function TestTableComponent_div_0_Template(rf, ctx) {
      if (rf & 1) {
        var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-dynamic-filters", 1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("query", function TestTableComponent_div_0_Template_app_dynamic_filters_query_1_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r2.filtersChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestTableComponent_div_0_Template_button_click_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r4.setConfigVisibility(!ctx_r4.configVisibility);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " Settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "app-dynamic-table-mark-ii", 3, 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("action", function TestTableComponent_div_0_Template_app_dynamic_table_mark_ii_action_6_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r5.doAction($event);
        })("pagination", function TestTableComponent_div_0_Template_app_dynamic_table_mark_ii_pagination_6_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r6.paginationChanged($event);
        })("order", function TestTableComponent_div_0_Template_app_dynamic_table_mark_ii_order_6_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r7.orderChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "app-dynamic-table-config-panel", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("configChanged", function TestTableComponent_div_0_Template_app_dynamic_table_config_panel_configChanged_8_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r8.configChange($event);
        })("closed", function TestTableComponent_div_0_Template_app_dynamic_table_config_panel_closed_8_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r9.setConfigVisibility(false);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("filters", ctx_r0.filters)("showFilters", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("config", ctx_r0.tableConfig)("data", ctx_r0.tableData);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visibility", ctx_r0.configVisibility)("columnsConfig", ctx_r0.tableConfig)("filtersConfig", ctx_r0.filters);
      }
    }

    var TestTableComponent = /*#__PURE__*/function () {
      function TestTableComponent(_testService, _router, _sessionService, _route, _testMapper) {
        _classCallCheck(this, TestTableComponent);

        this._testService = _testService;
        this._router = _router;
        this._sessionService = _sessionService;
        this._route = _route;
        this._testMapper = _testMapper;
        this.filters = [];
        this.tableConfig = _config__WEBPACK_IMPORTED_MODULE_2__["table"];
        this.queryPagination = {
          page: 0,
          size: 10
        };
        this.queryOrder = {
          orderField: '',
          order: ''
        };
        this.queryFilters = {};
        this.loading = true;
        this.query = {};
        this.configVisibility = false;
      }

      _createClass(TestTableComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this._buildFilters();

          this.tableData = this.tests;
          this.loading = false;
        }
      }, {
        key: "doAction",
        value: function doAction(event) {
          if (event.action === 'Add Correction') {
            this._router.navigate(["test-correction/".concat(event.item.id)], {
              relativeTo: this._route
            });
          }
        }
      }, {
        key: "navigate",
        value: function navigate(event) {
          if (event.property === 'id') {
            this._router.navigate(["test-detail/".concat(event.id)], {
              relativeTo: this._route
            });
          }
        }
      }, {
        key: "setSize",
        value: function setSize(event) {
          this.backSearch();
        }
      }, {
        key: "backSearch",
        value: function backSearch() {
          var _this45 = this;

          Object(_application_searchTests__WEBPACK_IMPORTED_MODULE_1__["searchTests"])(Object.assign(Object.assign(Object.assign({}, this.queryPagination), this.queryOrder), this.queryFilters), this._testService).subscribe(function (result) {
            _this45.tableData = {
              content: result.content,
              totalElements: result.totalElements,
              numberOfElements: result.numberOfElements,
              totalPages: result.totalPages
            };
          });
        }
      }, {
        key: "filtersChanged",
        value: function filtersChanged(params) {
          if (params && Object.keys(params).length !== 0) {
            var mapped = this._testMapper.mapFrom(params);

            this.queryFilters = mapped;
          } else {
            this.queryFilters = {};
          }

          this.queryPagination = {
            page: 0,
            size: this.queryPagination.size
          };
          this.dynamicTable.resetPageIndex();
          this.backSearch();
        }
      }, {
        key: "paginationChanged",
        value: function paginationChanged(event) {
          this.queryPagination.page = event.page;
          this.queryPagination.size = event.size;
          this.backSearch();
        }
      }, {
        key: "orderChanged",
        value: function orderChanged(event) {
          var aux = _defineProperty({}, event.orderField, '');

          aux = this._testMapper.mapFrom(aux);
          this.queryOrder.orderField = Object.entries(aux).find(function (entry) {
            return entry[1] === '';
          })[0];
          this.queryOrder.order = event.order;
          this.queryPagination.page = event.page;
          this.queryPagination.size = event.size;
          this.backSearch();
        }
      }, {
        key: "configChange",
        value: function configChange(event) {
          this.tableConfig = Object.assign({}, event[0]);
          this.filters = event[1];
        }
      }, {
        key: "setConfigVisibility",
        value: function setConfigVisibility(visible) {
          this.configVisibility = visible;
        }
      }, {
        key: "_buildFilters",
        value: function _buildFilters() {
          this.filters = [{
            options: [],
            prop: 'id',
            retProp: '',
            type: 'input',
            appearance: 'standard',
            "class": 'input',
            label: 'Id',
            placeholder: 'Id',
            shown: true
          }, {
            options: [],
            prop: 'name',
            retProp: '',
            type: 'input',
            appearance: 'standard',
            "class": 'input',
            label: 'Name',
            placeholder: 'Name',
            shown: true
          }, {
            options: [],
            prop: 'description',
            retProp: '',
            type: 'input',
            appearance: 'standard',
            "class": 'input',
            label: 'Description',
            placeholder: 'Description',
            shown: true
          }];
        }
      }]);

      return TestTableComponent;
    }();

    TestTableComponent.ɵfac = function TestTableComponent_Factory(t) {
      return new (t || TestTableComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_test_service__WEBPACK_IMPORTED_MODULE_3__["TestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_session_infrastructure_services_session_service__WEBPACK_IMPORTED_MODULE_5__["SessionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_evaluations_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_6__["TestMapper"]));
    };

    TestTableComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TestTableComponent,
      selectors: [["app-test-table"]],
      viewQuery: function TestTableComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dynamicTable = _t.first);
        }
      },
      inputs: {
        tests: "tests"
      },
      decls: 1,
      vars: 1,
      consts: [[4, "ngIf"], [3, "filters", "showFilters", "query"], ["mat-button", "", 1, "configButton", 3, "click"], [3, "config", "data", "action", "pagination", "order"], ["table", ""], [3, "visibility", "columnsConfig", "filtersConfig", "configChanged", "closed"]],
      template: function TestTableComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, TestTableComponent_div_0_Template, 9, 7, "div", 0);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_8__["DynamicFiltersComponent"], _angular_material_button__WEBPACK_IMPORTED_MODULE_9__["MatButton"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_10__["MatIcon"], _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_11__["DynamicTableMarkIiComponent"], _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_12__["DynamicTableConfigPanelComponent"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V2YWx1YXRpb25zL3Rlc3QvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy90ZXN0LW1hbmFnZW1lbnQvdGVzdC10YWJsZS90ZXN0LXRhYmxlLmNvbXBvbmVudC5zY3NzIn0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestTableComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-test-table',
          templateUrl: './test-table.component.html',
          styleUrls: ['./test-table.component.scss']
        }]
      }], function () {
        return [{
          type: _services_test_service__WEBPACK_IMPORTED_MODULE_3__["TestService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _auth_session_infrastructure_services_session_service__WEBPACK_IMPORTED_MODULE_5__["SessionService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: src_app_evaluations_test_infrastructure_TestMapper__WEBPACK_IMPORTED_MODULE_6__["TestMapper"]
        }];
      }, {
        tests: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        dynamicTable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['table']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/resolvers/edit-test-resolver.service.ts":
  /*!*****************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/resolvers/edit-test-resolver.service.ts ***!
    \*****************************************************************************************/

  /*! exports provided: EditTestResolverService */

  /***/
  function srcAppEvaluationsTestInfrastructureResolversEditTestResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditTestResolverService", function () {
      return EditTestResolverService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _application_getTests__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../application/getTests */
    "./src/app/evaluations/test/application/getTests.ts");
    /* harmony import */


    var _services_test_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");

    var EditTestResolverService = /*#__PURE__*/function () {
      function EditTestResolverService(_testService) {
        _classCallCheck(this, EditTestResolverService);

        this._testService = _testService;
      }

      _createClass(EditTestResolverService, [{
        key: "resolve",
        value: function resolve(route, state) {
          return Object(_application_getTests__WEBPACK_IMPORTED_MODULE_3__["getTest"])(route.params['id'], this._testService).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            return {
              data: {
                test: response
              }
            };
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])({
              data: null,
              message: 'Error on edit resolver',
              error: error
            });
          }));
        }
      }]);

      return EditTestResolverService;
    }();

    EditTestResolverService.ɵfac = function EditTestResolverService_Factory(t) {
      return new (t || EditTestResolverService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_test_service__WEBPACK_IMPORTED_MODULE_4__["TestService"]));
    };

    EditTestResolverService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: EditTestResolverService,
      factory: EditTestResolverService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EditTestResolverService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _services_test_service__WEBPACK_IMPORTED_MODULE_4__["TestService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/resolvers/test-detail-resolver.service.ts":
  /*!*******************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/resolvers/test-detail-resolver.service.ts ***!
    \*******************************************************************************************/

  /*! exports provided: TestDetailResolverService */

  /***/
  function srcAppEvaluationsTestInfrastructureResolversTestDetailResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestDetailResolverService", function () {
      return TestDetailResolverService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _correction_application_searchCorrections__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../correction/application/searchCorrections */
    "./src/app/evaluations/correction/application/searchCorrections.ts");
    /* harmony import */


    var _application_getTests__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../application/getTests */
    "./src/app/evaluations/test/application/getTests.ts");
    /* harmony import */


    var _services_test_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var _TestMapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../TestMapper */
    "./src/app/evaluations/test/infrastructure/TestMapper.ts");
    /* harmony import */


    var _correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../correction/infrastructure/services/correction.service */
    "./src/app/evaluations/correction/infrastructure/services/correction.service.ts");
    /* harmony import */


    var _correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../correction/infrastructure/CorrectionMapper */
    "./src/app/evaluations/correction/infrastructure/CorrectionMapper.ts");

    var TestDetailResolverService = /*#__PURE__*/function () {
      function TestDetailResolverService(_testService, _testMapper, _correctionService, _correctionMapper) {
        _classCallCheck(this, TestDetailResolverService);

        this._testService = _testService;
        this._testMapper = _testMapper;
        this._correctionService = _correctionService;
        this._correctionMapper = _correctionMapper;
      }

      _createClass(TestDetailResolverService, [{
        key: "resolve",
        value: function resolve(route, state) {
          var _this46 = this;

          var test = Object(_application_getTests__WEBPACK_IMPORTED_MODULE_4__["getTest"])(route.params["id"], this._testService);
          var corrections = Object(_correction_application_searchCorrections__WEBPACK_IMPORTED_MODULE_3__["searchCorrections"])({
            size: 100,
            page: 0,
            idTest: route.params["id"]
          }, this._correctionService, this._correctionMapper);
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["forkJoin"])(test, corrections).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return {
              data: {
                test: _this46._testMapper.mapTo(res[0]),
                corrections: res[1]
              }
            };
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])({
              data: null,
              message: 'Error on test detail resolver, data couldn\'t be fetched',
              error: err
            });
          }));
        }
      }]);

      return TestDetailResolverService;
    }();

    TestDetailResolverService.ɵfac = function TestDetailResolverService_Factory(t) {
      return new (t || TestDetailResolverService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_test_service__WEBPACK_IMPORTED_MODULE_5__["TestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_TestMapper__WEBPACK_IMPORTED_MODULE_6__["TestMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_7__["CorrectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_8__["CorrectionMapper"]));
    };

    TestDetailResolverService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: TestDetailResolverService,
      factory: TestDetailResolverService.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestDetailResolverService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], function () {
        return [{
          type: _services_test_service__WEBPACK_IMPORTED_MODULE_5__["TestService"]
        }, {
          type: _TestMapper__WEBPACK_IMPORTED_MODULE_6__["TestMapper"]
        }, {
          type: _correction_infrastructure_services_correction_service__WEBPACK_IMPORTED_MODULE_7__["CorrectionService"]
        }, {
          type: _correction_infrastructure_CorrectionMapper__WEBPACK_IMPORTED_MODULE_8__["CorrectionMapper"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/resolvers/test-management-resolver.service.ts":
  /*!***********************************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/resolvers/test-management-resolver.service.ts ***!
    \***********************************************************************************************/

  /*! exports provided: TestManagementResolverService */

  /***/
  function srcAppEvaluationsTestInfrastructureResolversTestManagementResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestManagementResolverService", function () {
      return TestManagementResolverService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _application_searchTests__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../application/searchTests */
    "./src/app/evaluations/test/application/searchTests.ts");
    /* harmony import */


    var _services_test_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/test.service */
    "./src/app/evaluations/test/infrastructure/services/test.service.ts");
    /* harmony import */


    var src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/auth/session/infrastructure/services/cookie.service */
    "./src/app/auth/session/infrastructure/services/cookie.service.ts");

    var TestManagementResolverService = /*#__PURE__*/function () {
      function TestManagementResolverService(_testService, _cookieService) {
        _classCallCheck(this, TestManagementResolverService);

        this._testService = _testService;
        this._cookieService = _cookieService;
      }

      _createClass(TestManagementResolverService, [{
        key: "resolve",
        value: function resolve() {
          var _a, _b;

          var filtersCache = this._cookieService.get('testFilterCache');

          var tableFiltersConfig = JSON.parse(this._cookieService.get('test-table-config'));
          var testParams = (_a = tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.pagination) !== null && _a !== void 0 ? _a : {
            page: 0,
            size: 10
          };
          var orderParams = (_b = Object.assign({}, tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.order)) !== null && _b !== void 0 ? _b : {};
          return Object(_application_searchTests__WEBPACK_IMPORTED_MODULE_3__["searchTests"])(Object.assign(Object.assign(Object.assign({}, JSON.parse(filtersCache)), testParams), orderParams), this._testService).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (response) {
            return {
              data: {
                tests: response,
                filtersValue: filtersCache,
                tableFiltersConfig: tableFiltersConfig
              }
            };
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (error) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])({
              data: null,
              message: 'Error on test managment resolver, data couldn\'t be fetched',
              error: error
            });
          }));
        }
      }]);

      return TestManagementResolverService;
    }();

    TestManagementResolverService.ɵfac = function TestManagementResolverService_Factory(t) {
      return new (t || TestManagementResolverService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_test_service__WEBPACK_IMPORTED_MODULE_4__["TestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieFacadeService"]));
    };

    TestManagementResolverService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: TestManagementResolverService,
      factory: TestManagementResolverService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestManagementResolverService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _services_test_service__WEBPACK_IMPORTED_MODULE_4__["TestService"]
        }, {
          type: src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieFacadeService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/evaluations/test/infrastructure/services/test.service.ts":
  /*!**************************************************************************!*\
    !*** ./src/app/evaluations/test/infrastructure/services/test.service.ts ***!
    \**************************************************************************/

  /*! exports provided: TestService */

  /***/
  function srcAppEvaluationsTestInfrastructureServicesTestServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TestService", function () {
      return TestService;
    });
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _TestAbstractService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../TestAbstractService */
    "./src/app/evaluations/test/infrastructure/TestAbstractService.ts");

    var TestService = /*#__PURE__*/function (_TestAbstractService_) {
      _inherits(TestService, _TestAbstractService_);

      var _super4 = _createSuper(TestService);

      function TestService(_http) {
        var _this47;

        _classCallCheck(this, TestService);

        _this47 = _super4.call(this);
        _this47._http = _http;
        _this47.BASE_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl;
        _this47.ENDPOINT = 'tests';
        return _this47;
      }

      _createClass(TestService, [{
        key: "findAll",
        value: function findAll(params) {
          return this._http.get("".concat(this.BASE_URL).concat(this.ENDPOINT, "/search"), {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpParams"]({
              fromObject: this._clean(params)
            })
          });
        }
      }, {
        key: "create",
        value: function create(test) {
          return this._http.post("".concat(this.BASE_URL).concat(this.ENDPOINT), test);
        }
      }, {
        key: "delete",
        value: function _delete(idTest) {
          return this._http["delete"]("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(idTest));
        }
      }, {
        key: "update",
        value: function update(test) {
          return this._http.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(test.id), test);
        }
      }, {
        key: "addQuestion",
        value: function addQuestion(question, idTest) {
          return this._http.post("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(idTest, "/questions"), question);
        }
      }, {
        key: "removeQuestion",
        value: function removeQuestion(idQuestion, idTest) {
          return this._http["delete"]("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(idTest, "/questions/").concat(idQuestion));
        }
      }, {
        key: "finbById",
        value: function finbById(id) {
          return this._http.get("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(id));
        }
      }, {
        key: "_clean",
        value: function _clean(object) {
          var cleaned = {};
          var keys = Object.keys(object);
          keys.forEach(function (key) {
            if (object[key]) {
              cleaned[key] = object[key];
            }
          });
          return cleaned;
        }
      }]);

      return TestService;
    }(_TestAbstractService__WEBPACK_IMPORTED_MODULE_3__["TestAbstractService"]);

    TestService.ɵfac = function TestService_Factory(t) {
      return new (t || TestService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]));
    };

    TestService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
      token: TestService,
      factory: TestService.ɵfac,
      providedIn: 'any'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](TestService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
          providedIn: 'any'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=evaluations-evaluations-module-es5.js.map