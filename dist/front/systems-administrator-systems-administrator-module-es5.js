function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["systems-administrator-systems-administrator-module"], {
  /***/
  "./src/app/auth/role/application/getRoles.ts":
  /*!***************************************************!*\
    !*** ./src/app/auth/role/application/getRoles.ts ***!
    \***************************************************/

  /*! exports provided: getRoles */

  /***/
  function srcAppAuthRoleApplicationGetRolesTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getRoles", function () {
      return getRoles;
    });

    function getRoles(params, service) {
      return service.search(params);
    }
    /***/

  },

  /***/
  "./src/app/auth/role/application/removeRole.ts":
  /*!*****************************************************!*\
    !*** ./src/app/auth/role/application/removeRole.ts ***!
    \*****************************************************/

  /*! exports provided: removeRole */

  /***/
  function srcAppAuthRoleApplicationRemoveRoleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "removeRole", function () {
      return removeRole;
    });

    function removeRole(id, service) {
      return service.deleteRole(id);
    }
    /***/

  },

  /***/
  "./src/app/auth/role/infrastructure/abstract-role-service.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/auth/role/infrastructure/abstract-role-service.ts ***!
    \*******************************************************************/

  /*! exports provided: AbstractRoleService */

  /***/
  function srcAppAuthRoleInfrastructureAbstractRoleServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AbstractRoleService", function () {
      return AbstractRoleService;
    });

    var AbstractRoleService = function AbstractRoleService() {
      _classCallCheck(this, AbstractRoleService);
    };
    /***/

  },

  /***/
  "./src/app/auth/role/infrastructure/ng-components/role-detail/role-detail-config.ts":
  /*!******************************************************************************************!*\
    !*** ./src/app/auth/role/infrastructure/ng-components/role-detail/role-detail-config.ts ***!
    \******************************************************************************************/

  /*! exports provided: config */

  /***/
  function srcAppAuthRoleInfrastructureNgComponentsRoleDetailRoleDetailConfigTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "config", function () {
      return config;
    });

    var config = [{
      name: 'Staffit',
      path: 'my-staffit',
      children: [{
        name: 'All employees',
        path: 'employees',
        icon: 'face',
        scopeCreate: [],
        scopeRead: [],
        isLeaf: true
      }, {
        name: 'Who is who?',
        path: 'who-is-who',
        icon: 'quiz',
        scopeCreate: [],
        scopeRead: [],
        isLeaf: true
      }, {
        name: 'Rooms',
        path: 'rooms',
        icon: 'weekend',
        scopeCreate: [],
        scopeRead: [],
        isLeaf: true
      }, {
        name: 'Know the company',
        path: 'know-the-company',
        icon: 'help',
        scopeCreate: [],
        scopeRead: [],
        isLeaf: true
      }]
    }, {
      name: 'Systems Administrator',
      path: 'systems-administrator',
      children: [{
        name: 'Admin panels',
        path: 'admin-panels',
        icon: 'analytics',
        scopeCreate: [],
        scopeRead: [],
        isLeaf: true
      }, {
        name: 'Users',
        path: 'users',
        icon: 'group',
        scopeCreate: ['USUARIO_CREATE', 'USUARIO_UPDATE', 'USUARIO_DELETE', 'Update scope users', 'USUARIO_READ', 'Read scope users'],
        scopeRead: ['USUARIO_READ', 'Read scope users'],
        isLeaf: true
      }, {
        name: 'Roles',
        path: 'roles',
        icon: 'admin_panel_settings',
        scopeCreate: ['Create scope admin', 'Update scope admin', 'Delete scope admin', 'Read scope admin'],
        scopeRead: ['Read scope admin'],
        isLeaf: true
      }]
    }, {
      name: 'Internal',
      path: 'internal',
      children: [{
        name: 'Employee management',
        icon: 'supervisor_account',
        path: 'employee',
        scopeCreate: [],
        scopeRead: [],
        children: [{
          name: 'Reports',
          path: 'reports',
          icon: 'groups',
          scopeCreate: ['EMPLOYEE_CREATE', 'EMPLOYEE_UPDATE', 'EMPLOYEE_DELETE', 'EMPLOYEE_READ', 'EMPLOYEEAUX_READ', 'COMPANY_READ', 'CATEGORY_READ', 'CAR_READ', 'SUBCAR_READ'],
          scopeRead: ['EMPLOYEE_READ', 'EMPLOYEEAUX_READ', 'COMPANY_READ', 'CATEGORY_READ', 'CAR_READ', 'SUBCAR_READ'],
          isLeaf: true
        }, {
          name: 'Requests',
          path: 'new-requests',
          icon: 'person_adds',
          scopeCreate: ['NEWEMPLOYEE_CREATE', 'NEWEMPLOYEE_UPDATE', 'NEWEMPLOYEE_DELETE', 'NEWEMPLOYEE_READ', 'CAR_READ', 'SUBCAR_READ', 'CENTER_READ'],
          scopeRead: ['NEWEMPLOYEE_READ', 'CAR_READ', 'SUBCAR_READ', 'CENTER_READ'],
          isLeaf: true
        }]
      }, {
        name: 'Billing',
        path: 'billing',
        icon: 'euro',
        scopeCreate: [],
        scopeRead: [],
        isLeaf: true
      }, {
        name: 'Room management',
        path: 'room-management',
        icon: 'weekend',
        scopeCreate: [],
        scopeRead: [],
        isLeaf: true
      }, {
        name: 'Admin know the company',
        path: 'admin-know-the-company',
        icon: 'help',
        scopeCreate: [],
        scopeRead: [],
        isLeaf: true
      }]
    }, {
      name: 'Recruitment',
      path: 'recruitment',
      children: [{
        name: 'Campaigns',
        path: 'campaigns',
        icon: 'campaign',
        scopeCreate: ['CAMPAIGN_CREATE', 'CAMPAIGN_UPDATE', 'CAMPAIGN_DELETE', 'CAMPAIGN_READ', 'COLLEGE_READ', 'STUDY_READ', 'CANDIDATE_READ'],
        scopeRead: ['CAMPAIGN_READ', 'COLLEGE_READ', 'STUDY_READ', 'CANDIDATE_READ'],
        isLeaf: true
      }, {
        name: 'Candidates',
        path: 'candidates',
        icon: 'person_search',
        scopeCreate: ['CANDIDATE_CREATE', 'CANDIDATE_UPDATE', 'CANDIDATE_DELETE', 'CANDIDATE_READ', 'COLLEGE_READ', 'EMPLOYEE_READ', 'TECHNOLOGY_READ', 'STUDY_READ'],
        scopeRead: ['CANDIDATE_READ', 'STUDY_READ', 'COLLEGE_READ', 'TECHNOLOGY_READ', 'EMPLOYEE_READ'],
        isLeaf: true
      }, {
        name: 'Colleges',
        path: 'colleges',
        icon: 'local_library',
        scopeCreate: ['COLLEGE_CREATE', 'COLLEGE_UPDATE', 'COLLEGE_DELETE', 'STUDY_READ', 'COLLEGE_READ'],
        scopeRead: ['COLLEGE_READ', 'STUDY_READ'],
        isLeaf: true
      }, {
        name: 'Studies',
        path: 'studies',
        icon: 'school',
        scopeCreate: ['STUDY_CREATE', 'STUDY_UPDATE', 'STUDY_DELETE', 'STUDY_READ'],
        scopeRead: ['STUDY_READ'],
        isLeaf: true
      }]
    }, {
      name: 'Evaluations',
      path: 'evaluations',
      children: [{
        name: 'Tests',
        path: 'tests',
        icon: 'note_add',
        scopeCreate: ['TEST_CREATE', 'TEST_UPDATE', 'TEST_DELETE', 'TEST_READ', 'CORRECTION_READ'],
        scopeRead: ['TEST_READ', 'CORRECTION_READ'],
        isLeaf: true
      }, {
        name: 'Exams',
        path: 'exams',
        icon: 'assignment',
        scopeCreate: ['EXAM_CREATE', 'EXAM_UPDATE', 'EXAM_DELETE', 'EXAMSENT_CREATE', 'EXAMSENT_UPDATE', 'EXAMSENT_DELETE', 'TEST_READ', 'CORRECTION_READ', 'QUESTION_CREATE', 'QUESTION_DELETE', 'EXAM_READ', 'EXAMSENT_READ'],
        scopeRead: ['EXAM_READ', 'CORRECTION_READ', 'EXAMSENT_READ', 'TEST_READ'],
        isLeaf: true
      }]
    }, {
      name: 'Crm',
      path: 'crm',
      children: [{
        name: 'Accounts',
        path: 'accounts',
        icon: 'business',
        scopeCreate: ['CUENTA_CREATE', 'CUENTA_UPDATE', 'CUENTA_DELETE', 'CUENTA_READ', 'CAR_READ', 'SUBCAR_READ', 'TERRITORIO_READ', 'USUARIO_READ'],
        scopeRead: ['CUENTA_READ', 'CAR_READ', 'SUBCAR_READ', 'TERRITORIO_READ', 'USUARIO_READ'],
        isLeaf: true
      }, {
        name: 'Contacts',
        path: 'contacts',
        icon: 'contacts',
        scopeCreate: ['CONTACTO_CREATE', 'CONTACTO_UPDATE', 'CONTACTO_DELETE', 'CONTACTO_READ', 'CAR_READ', 'SUBCAR_READ', 'USUARIO_READ'],
        scopeRead: ['CONTACTO_READ', 'CAR_READ', 'SUBCAR_READ', 'USUARIO_READ'],
        isLeaf: true
      }, {
        name: 'Activities',
        path: 'activities',
        icon: 'event',
        scopeCreate: ['ACTIVIDAD_CREATE', 'ACTIVIDAD_UPDATE', 'ACTIVIDAD_DELETE', 'ACTIVIDAD_READ', 'USUARIO_READ', 'CAR_READ', 'SUBCAR_READ'],
        scopeRead: ['ACTIVIDAD_READ', 'USUARIO_READ', 'CAR_READ', 'SUBCAR_READ'],
        isLeaf: true
      }, {
        name: 'Opportunities',
        path: 'opportunities',
        icon: 'euro',
        scopeCreate: ['OPORTUNIDAD_CREATE', 'OPORTUNIDAD_UPDATE', 'OPORTUNIDAD_DELETE', 'TRACKING_OPORTUNIDAD_CREATE', 'TRACKING_OPORTUNIDAD_UPDATE', 'OPORTUNIDAD_READ', 'TRACKING_OPORTUNIDAD_DELETE', 'TRACKING_OPORTUNIDAD_READ', 'USUARIO_READ', 'CUENTA_READ', 'CONTACTO_READ', 'ESTADO_READ', 'CAR_READ', 'SUBCAR_READ'],
        scopeRead: ['OPORTUNIDAD_READ', 'TRACKING_OPORTUNIDAD_READ', 'USUARIO_READ', 'CUENTA_READ', 'CONTACTO_READ', 'ESTADO_READ', 'CAR_READ', 'SUBCAR_READ'],
        isLeaf: true
      }]
    }];
    /***/
  },

  /***/
  "./src/app/auth/role/infrastructure/ng-components/role-detail/role-detail.component.ts":
  /*!*********************************************************************************************!*\
    !*** ./src/app/auth/role/infrastructure/ng-components/role-detail/role-detail.component.ts ***!
    \*********************************************************************************************/

  /*! exports provided: RoleDetailComponent */

  /***/
  function srcAppAuthRoleInfrastructureNgComponentsRoleDetailRoleDetailComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoleDetailComponent", function () {
      return RoleDetailComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _role_detail_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./role-detail-config */
    "./src/app/auth/role/infrastructure/ng-components/role-detail/role-detail-config.ts");
    /* harmony import */


    var _application_removeRole__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../application/removeRole */
    "./src/app/auth/role/application/removeRole.ts");
    /* harmony import */


    var src_app_auth_role_infrastructure_services_role_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/auth/role/infrastructure/services/role.service */
    "./src/app/auth/role/infrastructure/services/role.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/material/tabs */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tabs.js");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _angular_material_list__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/material/list */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/list.js");
    /* harmony import */


    var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/material/checkbox */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");

    function RoleDetailComponent_mat_card_content_2_button_22_Template(rf, ctx) {
      if (rf & 1) {
        var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RoleDetailComponent_mat_card_content_2_button_22_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5);

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r4.remove();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delete");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function RoleDetailComponent_mat_card_content_2_Template(rf, ctx) {
      if (rf & 1) {
        var _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card-content", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h2");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Role Info");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "form", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "section");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-form-field", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Name");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "input", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-form-field", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Value");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "input", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "mat-form-field", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "mat-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Description");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "textarea", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RoleDetailComponent_mat_card_content_2_Template_button_click_20_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7);

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r6.onSaveClick();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Save");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, RoleDetailComponent_mat_card_content_2_button_22_Template, 2, 0, "button", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r0.form);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.cantBeChanged);
      }
    }

    function RoleDetailComponent_div_6_div_1_div_3_hr_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "hr");
      }
    }

    function RoleDetailComponent_div_6_div_1_div_3_div_12_Template(rf, ctx) {
      if (rf & 1) {
        var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-icon", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-checkbox", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function RoleDetailComponent_div_6_div_1_div_3_div_12_Template_mat_checkbox_change_7_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var subchild_r14 = ctx.$implicit;

          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](4);

          return ctx_r15.onConceptChange(subchild_r14.scopeCreate, $event, "create", subchild_r14.name);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "CREATE ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-checkbox", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function RoleDetailComponent_div_6_div_1_div_3_div_12_Template_mat_checkbox_change_9_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var subchild_r14 = ctx.$implicit;

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](4);

          return ctx_r17.onConceptChange(subchild_r14.scopeRead, $event, "read", subchild_r14.name);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "READ ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var subchild_r14 = ctx.$implicit;

        var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](subchild_r14.icon);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](subchild_r14.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx_r13.allAreChecked(subchild_r14.scopeCreate))("disabled", subchild_r14.scopeCreate.length === 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx_r13.allAreChecked(subchild_r14.scopeRead))("disabled", subchild_r14.scopeRead.length === 0);
      }
    }

    function RoleDetailComponent_div_6_div_1_div_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RoleDetailComponent_div_6_div_1_div_3_hr_1_Template, 1, 0, "hr", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-icon", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-checkbox", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function RoleDetailComponent_div_6_div_1_div_3_Template_mat_checkbox_change_8_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

          var child_r11 = ctx.$implicit;

          var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r18.onConceptChange(child_r11.scopeCreate, $event, "create", child_r11.name);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "CREATE ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-checkbox", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function RoleDetailComponent_div_6_div_1_div_3_Template_mat_checkbox_change_10_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

          var child_r11 = ctx.$implicit;

          var ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r20.onConceptChange(child_r11.scopeRead, $event, "read", child_r11.name);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "READ ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, RoleDetailComponent_div_6_div_1_div_3_div_12_Template, 11, 6, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var child_r11 = ctx.$implicit;

        var module_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", child_r11 !== module_r9.children[0]);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](child_r11.icon);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](child_r11.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx_r10.allAreChecked(child_r11.scopeCreate))("disabled", child_r11.scopeCreate.length === 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx_r10.allAreChecked(child_r11.scopeRead))("disabled", child_r11.scopeRead.length === 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", child_r11.children);
      }
    }

    function RoleDetailComponent_div_6_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, RoleDetailComponent_div_6_div_1_div_3_Template, 13, 8, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var module_r9 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](module_r9.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", module_r9.children);
      }
    }

    function RoleDetailComponent_div_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, RoleDetailComponent_div_6_div_1_Template, 5, 2, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.modules);
      }
    }

    function RoleDetailComponent_div_8_ng_container_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-checkbox", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function RoleDetailComponent_div_8_ng_container_4_Template_mat_checkbox_change_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25);

          var scope_r23 = ctx.$implicit;

          var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r24.onScopeChange(scope_r23, $event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
      }

      if (rf & 2) {
        var scope_r23 = ctx.$implicit;

        var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx_r22.isChecked(scope_r23));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](scope_r23.name);
      }
    }

    function RoleDetailComponent_div_8_Template(rf, ctx) {
      if (rf & 1) {
        var _r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-checkbox", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function RoleDetailComponent_div_8_Template_mat_checkbox_change_2_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27);

          var ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r26.onSelectAll($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Select all");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, RoleDetailComponent_div_8_ng_container_4_Template, 4, 2, "ng-container", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx_r2.allScopes.length === ctx_r2.checkedScopes.length);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.allScopes);
      }
    }

    var RoleDetailComponent = /*#__PURE__*/function () {
      function RoleDetailComponent(_fb, _service, _route, _router) {
        _classCallCheck(this, RoleDetailComponent);

        this._fb = _fb;
        this._service = _service;
        this._route = _route;
        this._router = _router;
        this.loading = true;
        this.modules = _role_detail_config__WEBPACK_IMPORTED_MODULE_3__["config"];
      }

      _createClass(RoleDetailComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _a, _b;

          var data = this._route.snapshot.data['response'].data;

          if (data) {
            this.role = data.role;
            this.allScopes = data.scopes;
            this.checkedScopes = (_b = (_a = this.role) === null || _a === void 0 ? void 0 : _a.fullScopes) !== null && _b !== void 0 ? _b : [];
            this.loading = false;
            this.cantBeChanged = this.role ? ['ROLE_ADMIN', 'ROLE_USER'].includes(this.role.value) : false;
            this.initForm();

            this._loadExtendedConcepts();
          }
        }
      }, {
        key: "_loadExtendedConcepts",
        value: function _loadExtendedConcepts() {
          var _this = this;

          this.allConcepts = [];
          this.modules.forEach(function (module) {
            module.children.forEach(function (concept) {
              _this._parseTreeNode(concept);

              if (!concept.children) {
                return;
              }

              concept.children.forEach(function (conceptChild) {
                var _concept$scopeCreate, _concept$scopeRead;

                _this._parseTreeNode(conceptChild); // Add scopes of child to its parent


                (_concept$scopeCreate = concept.scopeCreate).push.apply(_concept$scopeCreate, _toConsumableArray(conceptChild.scopeCreate));

                (_concept$scopeRead = concept.scopeRead).push.apply(_concept$scopeRead, _toConsumableArray(conceptChild.scopeRead));
              }); // Remove duplicates

              concept.scopeCreate = _toConsumableArray(new Set(concept.scopeCreate));
              concept.scopeRead = _toConsumableArray(new Set(concept.scopeRead));
            });
          });
        }
      }, {
        key: "_parseTreeNode",
        value: function _parseTreeNode(concept) {
          this.allConcepts.push({
            name: concept.name,
            scopeCreate: concept.scopeCreate,
            scopeRead: concept.scopeRead,
            createChecked: this.allAreChecked(concept.scopeCreate),
            readChecked: this.allAreChecked(concept.scopeRead),
            children: concept.children
          });
        }
      }, {
        key: "_resetConceptsCheckedState",
        value: function _resetConceptsCheckedState() {
          var _this2 = this;

          this.changedConcepts = [];
          this.allConcepts.forEach(function (concept) {
            var oldCreateChecked = concept.createChecked;
            var oldReadChecked = concept.readChecked;
            concept.createChecked = _this2.allAreChecked(concept.scopeCreate);
            concept.readChecked = _this2.allAreChecked(concept.scopeRead);

            if (!concept.children) {
              if (oldCreateChecked !== concept.createChecked) {
                _this2.changedConcepts.push({
                  name: concept.name,
                  mode: 'create'
                });
              }

              if (oldReadChecked !== concept.readChecked) {
                _this2.changedConcepts.push({
                  name: concept.name,
                  mode: 'read'
                });
              }
            }
          });
        }
        /* FORM LOGIC */

      }, {
        key: "initForm",
        value: function initForm() {
          if (this.role != null) {
            this.form = this._fb.group({
              name: [{
                value: this.role.name,
                disabled: true
              }],
              value: [{
                value: this.role.value,
                disabled: true
              }],
              description: [{
                value: this.role.description,
                disabled: true
              }]
            });
          } else {
            this.form = this._fb.group({
              name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              value: ['ROLE_', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
            });
          }
        }
      }, {
        key: "onSaveClick",
        value: function onSaveClick() {
          if (this.role) {
            this.update();
            return;
          }

          if (this.form.valid) {
            this.create();
          }
        }
      }, {
        key: "create",
        value: function create() {
          var _this3 = this;

          var role = this.form.value;

          this._service.createRole(role).subscribe(function (res) {
            Object(src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["successAlert"])('The role was successfully saved');

            _this3.updateScope(res.id);
          });
        }
      }, {
        key: "update",
        value: function update() {
          this.updateScope(this.role.id);
        }
      }, {
        key: "updateScope",
        value: function updateScope(idRole) {
          var ids = this.checkedScopes.map(function (scope) {
            return scope.id;
          });

          this._service.updateScope(idRole, ids).subscribe(function () {
            Object(src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["successAlert"])('The scopes were successfully added');
          });
        }
      }, {
        key: "remove",
        value: function remove() {
          var _this4 = this;

          Object(_application_removeRole__WEBPACK_IMPORTED_MODULE_4__["removeRole"])(this.role.id, this._service).subscribe(function () {
            Object(src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["successAlert"])('The scopes were successfully added');

            _this4._router.navigate(['./app/systems-administrator/roles']);
          });
        }
        /* CHECKBOX LOGIC */

      }, {
        key: "isChecked",
        value: function isChecked(scope) {
          return !!this.checkedScopes.find(function (cs) {
            return cs.id === scope.id;
          });
        }
      }, {
        key: "allAreChecked",
        value: function allAreChecked(moduleScopeNames) {
          var checkedModuleScopes = this.checkedScopes.filter(function (cs) {
            return moduleScopeNames.includes(cs.name);
          });
          return checkedModuleScopes.length === moduleScopeNames.length;
        }
      }, {
        key: "onSelectAll",
        value: function onSelectAll(event) {
          this.checkedScopes = event.checked ? _toConsumableArray(this.allScopes) : [];
        }
      }, {
        key: "onConceptChange",
        value: function onConceptChange(scopeNames, event, mode, conceptName) {
          var _this5 = this;

          // Case 1: concept checked
          if (event.checked) {
            this._addScopes(scopeNames);

            this._resetConceptsCheckedState();

            return;
          } // Case 2: concept unchecked


          var scopes = this.allScopes.filter(function (scope) {
            return scopeNames.includes(scope.name);
          });
          var concept = this.allConcepts.find(function (c) {
            return c.name === conceptName;
          });

          this._resetConceptsCheckedState();

          var notSharedScopes = this._findNotSharedScopes(scopes, concept, mode);

          if (notSharedScopes.length === 0) {
            // Case 2.1: all scopes shared with selected concepts
            var oldCheckedScopes = this.checkedScopes;
            this.checkedScopes = this.checkedScopes.filter(function (cs) {
              return !scopeNames.includes(cs.name);
            });

            this._resetConceptsCheckedState();

            var htmlMessage = "All scopes of concept <b>".concat(conceptName, " (").concat(mode, ")</b> are shared with other selected concepts.<br><br>\n        If you continue, the following concepts will be deselected:<br>\n        <ul style=\"text-align: left\">\n          ").concat(this.changedConcepts.map(function (c) {
              return "<li>".concat(c.name, " (").concat(c.mode, ")</li>");
            }).join(''), "\n        </ul>");
            Object(src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["questionAlert"])(htmlMessage).then(function (result) {
              if (result.isConfirmed) {
                Object(src_app_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_2__["successAlert"])('Concepts deselected');

                _this5._cleanOrphanScopes();
              } else {
                _this5.checkedScopes = oldCheckedScopes;

                _this5._resetConceptsCheckedState();
              }
            });
          } else {
            // Case 2.2: some/no scopes shared with selected concepts
            this.checkedScopes = this.checkedScopes.filter(function (cs) {
              return !notSharedScopes.includes(cs.id);
            });

            this._resetConceptsCheckedState();

            this._cleanOrphanScopes();
          }
        }
      }, {
        key: "onScopeChange",
        value: function onScopeChange(scope, event) {
          if (event.checked) {
            var checkedScope = this.checkedScopes.find(function (cs) {
              return cs.id === scope.id;
            });

            if (!checkedScope) {
              this.checkedScopes.push(scope);
            }
          } else {
            var index = this.checkedScopes.findIndex(function (cs) {
              return cs.id === scope.id;
            });

            if (index > -1) {
              this.checkedScopes.splice(index, 1);
            }
          }
        }
      }, {
        key: "_findNotSharedScopes",
        value: function _findNotSharedScopes(scopes, concept, mode) {
          var _this6 = this;

          var notSharedScopes = [];
          var isScopeInAnotherConcept;
          scopes.forEach(function (scope) {
            isScopeInAnotherConcept = _this6.allConcepts.some(function (c) {
              var _a, _b; // Same concept


              if (c.name === (concept === null || concept === void 0 ? void 0 : concept.name)) {
                switch (mode) {
                  case 'create':
                    return c.readChecked && c.scopeRead.includes(scope.name);

                  case 'read':
                    return false;
                }
              } // Ignore concept if related (child or parent)


              if (!!((_a = concept === null || concept === void 0 ? void 0 : concept.children) === null || _a === void 0 ? void 0 : _a.find(function (child) {
                return child.name === c.name;
              }))) {
                return false;
              }

              if (concept && !!((_b = c.children) === null || _b === void 0 ? void 0 : _b.find(function (child) {
                return child.name === concept.name;
              }))) {
                return false;
              }

              return c.createChecked && c.scopeCreate.includes(scope.name) || c.readChecked && c.scopeRead.includes(scope.name);
            });

            if (!isScopeInAnotherConcept) {
              notSharedScopes.push(scope.id);
            }
          });
          return notSharedScopes;
        }
      }, {
        key: "_addScopes",
        value: function _addScopes(scopeNames) {
          var _this7 = this;

          var scopes = this.allScopes.filter(function (scope) {
            return scopeNames.includes(scope.name);
          });
          scopes.forEach(function (scope) {
            var checkedScope = _this7.checkedScopes.find(function (cs) {
              return cs.id === scope.id;
            });

            if (!checkedScope) {
              _this7.checkedScopes.push(scope);
            }
          });
        }
      }, {
        key: "_cleanOrphanScopes",
        value: function _cleanOrphanScopes() {
          var orphanScopes = this._findNotSharedScopes(this.checkedScopes, null, null);

          this.checkedScopes = this.checkedScopes.filter(function (cs) {
            return !orphanScopes.includes(cs.id);
          });
        }
      }]);

      return RoleDetailComponent;
    }();

    RoleDetailComponent.ɵfac = function RoleDetailComponent_Factory(t) {
      return new (t || RoleDetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_auth_role_infrastructure_services_role_service__WEBPACK_IMPORTED_MODULE_5__["RoleService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]));
    };

    RoleDetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: RoleDetailComponent,
      selectors: [["app-role-detail"]],
      decls: 9,
      vars: 3,
      consts: [[1, "cards"], [1, "roleInfo"], ["class", "container", 4, "ngIf"], [1, "roleCheck"], ["label", "Basic"], ["class", "scopes", 4, "ngIf"], ["label", "Advanced"], ["class", "allScopes", 4, "ngIf"], [1, "container"], [1, "content"], [3, "formGroup"], [1, "columns"], ["appearance", "outline"], ["aria-label", "name", "matInput", "", "placeholder", "Name", "formControlName", "name"], ["aria-label", "value", "matInput", "", "placeholder", "Value", "formControlName", "value"], ["aria-label", "description", "matInput", "", "placeholder", "Description", "formControlName", "description"], [1, "buttons"], ["mat-button", "", 1, "apply-button", 3, "click"], ["mat-button", "", "class", "apply-button", 3, "click", 4, "ngIf"], [1, "scopes"], [4, "ngFor", "ngForOf"], [4, "ngIf"], [1, "categorias"], [1, "iconos"], ["mat-list-icon", ""], [3, "checked", "disabled", "change"], [1, "checkboxChild", 3, "checked", "disabled", "change"], [1, "subCategorias"], [1, "subIconos"], [1, "mr", 3, "checked", "disabled", "change"], [1, "allScopes"], [1, "selectAll"], [3, "checked", "change"], [1, "oneScope"]],
      template: function RoleDetailComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, RoleDetailComponent_mat_card_content_2_Template, 23, 2, "mat-card-content", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-card", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-tab-group");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-tab", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, RoleDetailComponent_div_6_Template, 2, 1, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-tab", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, RoleDetailComponent_div_8_Template, 5, 2, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);
        }
      },
      directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_7__["MatCard"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_9__["MatTabGroup"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_9__["MatTab"], _angular_material_card__WEBPACK_IMPORTED_MODULE_7__["MatCardContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_11__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButton"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_13__["MatIcon"], _angular_material_list__WEBPACK_IMPORTED_MODULE_14__["MatListIconCssMatStyler"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckbox"]],
      styles: ["mat-card[_ngcontent-%COMP%] {\n  margin: 1rem;\n}\n\n.roleInfo[_ngcontent-%COMP%] {\n  width: 30%;\n}\n\n.roleCheck[_ngcontent-%COMP%] {\n  width: 70%;\n}\n\n.cards[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  margin: 3em;\n}\n\n.container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  justify-content: space-around;\n}\n\n.content[_ngcontent-%COMP%] {\n  margin: 2em;\n}\n\n.checkboxChild[_ngcontent-%COMP%] {\n  margin-right: 5em;\n}\n\n.mat-icon[_ngcontent-%COMP%] {\n  margin-right: 0.5em;\n}\n\n.scopes[_ngcontent-%COMP%] {\n  padding-right: 1em;\n  align-items: baseline;\n  margin-left: 2em;\n  height: 33vw;\n  overflow-y: scroll;\n}\n\n.allScopes[_ngcontent-%COMP%] {\n  display: flex;\n  padding-right: 1em;\n  align-items: baseline;\n  height: 33vw;\n  overflow-y: scroll;\n  flex-flow: wrap;\n  justify-content: space-around;\n  -moz-column-gap: 0.5rem;\n       column-gap: 0.5rem;\n}\n\n.allScopes[_ngcontent-%COMP%]   .oneScope[_ngcontent-%COMP%] {\n  width: 33%;\n}\n\n.allScopes[_ngcontent-%COMP%]   .oneScope[_ngcontent-%COMP%]:hover {\n  background: #ccc;\n  color: white;\n}\n\n.hr[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.scopes[_ngcontent-%COMP%]::-webkit-scrollbar, .allScopes[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 6px;\n  height: 6px;\n}\n\n.scopes[_ngcontent-%COMP%]::-webkit-scrollbar-thumb, .allScopes[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 4px;\n}\n\n.categorias[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: space-between;\n}\n\n.categorias[_ngcontent-%COMP%]:hover {\n  background: #ccc;\n  color: white;\n}\n\n.subCategorias[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  justify-content: space-between;\n}\n\n.subCategorias[_ngcontent-%COMP%]:hover {\n  background: #ccc;\n  color: white;\n}\n\n.iconos[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n  width: 40%;\n  padding-left: 0.2em;\n}\n\n.subIconos[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n  width: 40%;\n  margin-left: 2em;\n  padding-left: 0.2em;\n}\n\n.columns[_ngcontent-%COMP%] {\n  justify-content: center;\n  height: 65%;\n}\n\n.columns[_ngcontent-%COMP%]   section[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.buttons[_ngcontent-%COMP%] {\n  margin-top: 20px;\n  text-align: center;\n}\n\n.buttons[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  margin-bottom: 1rem;\n}\n\n.buttons[_ngcontent-%COMP%]   .apply-button[_ngcontent-%COMP%] {\n  color: #fff;\n  background-color: #e46e75;\n  border-radius: 24px;\n  width: 200px;\n}\n\n.buttons[_ngcontent-%COMP%]   .apply-button[_ngcontent-%COMP%]:hover {\n  background-color: #eb4e4e;\n  cursor: pointer;\n}\n\n.mr[_ngcontent-%COMP%] {\n  margin-right: 2em;\n}\n\n[_nghost-%COMP%]     .mat-checkbox-checked.mat-accent .mat-checkbox-background, .mat-checkbox-indeterminate.mat-accent[_ngcontent-%COMP%]   .mat-checkbox-background[_ngcontent-%COMP%] {\n  background-color: #eb4e4e;\n}\n\n[_nghost-%COMP%]     .mat-checkbox-layout {\n  padding: 0.5em;\n}\n\n.selectAll[_ngcontent-%COMP%] {\n  display: flex;\n  width: 100%;\n  justify-content: center;\n  align-items: center;\n  margin: 10px;\n  border-bottom: 1px solid rgba(128, 128, 128, 0.445);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9yb2xlL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvcm9sZS1kZXRhaWwvQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFwcFxcYXV0aFxccm9sZVxcaW5mcmFzdHJ1Y3R1cmVcXG5nLWNvbXBvbmVudHNcXHJvbGUtZGV0YWlsXFxyb2xlLWRldGFpbC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXV0aC9yb2xlL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvcm9sZS1kZXRhaWwvcm9sZS1kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FDQ0Y7O0FERUE7RUFDRSxVQUFBO0FDQ0Y7O0FERUE7RUFDRSxVQUFBO0FDQ0Y7O0FERUE7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0FDQ0Y7O0FERUE7RUFDRSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDZCQUFBO0FDQ0Y7O0FERUE7RUFDRSxXQUFBO0FDQ0Y7O0FERUE7RUFDRSxpQkFBQTtBQ0NGOztBREVBO0VBQ0UsbUJBQUE7QUNDRjs7QURDQTtFQUNJLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ0VKOztBREFBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsNkJBQUE7RUFDQSx1QkFBQTtPQUFBLGtCQUFBO0FDR0Y7O0FEREU7RUFDRSxVQUFBO0FDR0o7O0FEQUU7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUNFSjs7QURFQTtFQUNFLFdBQUE7QUNDRjs7QURFQTtFQUNFLFVBQUE7RUFDQSxXQUFBO0FDQ0Y7O0FERUE7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0FDQ0Y7O0FERUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDQ0Y7O0FERUE7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUNDRjs7QURFQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUNDRjs7QURFQTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtBQ0NGOztBREVBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0FDQ0Y7O0FERUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ0NGOztBREVBO0VBQ0UsdUJBQUE7RUFDQSxXQUFBO0FDQ0Y7O0FEQUU7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtBQ0VOOztBREVBO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtBQ0NGOztBRENFO0VBQ0UsbUJBQUE7QUNDSjs7QURDRTtFQUNFLFdBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0NKOztBRENJO0VBQ0UseUJBQUE7RUFDQSxlQUFBO0FDQ047O0FESUE7RUFDRSxpQkFBQTtBQ0RGOztBRElBO0VBQ0UseUJBQUE7QUNERjs7QURJQTtFQUNFLGNBQUE7QUNERjs7QURJQTtFQUNFLGFBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxtREFBQTtBQ0RGIiwiZmlsZSI6InNyYy9hcHAvYXV0aC9yb2xlL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvcm9sZS1kZXRhaWwvcm9sZS1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtY2FyZCB7XHJcbiAgbWFyZ2luOiAxcmVtO1xyXG59XHJcblxyXG4ucm9sZUluZm97XHJcbiAgd2lkdGg6IDMwJTtcclxufVxyXG5cclxuLnJvbGVDaGVja3tcclxuICB3aWR0aDogNzAlO1xyXG59XHJcblxyXG4uY2FyZHN7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW46IDNlbTtcclxufVxyXG5cclxuLmNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxufVxyXG4gIFxyXG4uY29udGVudCB7XHJcbiAgbWFyZ2luOiAyZW07XHJcbn1cclxuXHJcbi5jaGVja2JveENoaWxke1xyXG4gIG1hcmdpbi1yaWdodDogNWVtO1xyXG59XHJcblxyXG4ubWF0LWljb257XHJcbiAgbWFyZ2luLXJpZ2h0OiAwLjVlbTtcclxufVxyXG4uc2NvcGVze1xyXG4gICAgcGFkZGluZy1yaWdodDogMWVtO1xyXG4gICAgYWxpZ24taXRlbXM6IGJhc2VsaW5lO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDJlbTtcclxuICAgIGhlaWdodDogMzN2dztcclxuICAgIG92ZXJmbG93LXk6IHNjcm9sbDtcclxufVxyXG4uYWxsU2NvcGVze1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgcGFkZGluZy1yaWdodDogMWVtO1xyXG4gIGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcclxuICBoZWlnaHQ6IDMzdnc7XHJcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gIGZsZXgtZmxvdzogd3JhcDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICBjb2x1bW4tZ2FwOiAwLjVyZW07XHJcblxyXG4gIC5vbmVTY29wZXtcclxuICAgIHdpZHRoOiAzMyU7XHJcbiAgfVxyXG5cclxuICAub25lU2NvcGU6aG92ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxufVxyXG5cclxuLmhye1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uc2NvcGVzOjotd2Via2l0LXNjcm9sbGJhciwgLmFsbFNjb3Blczo6LXdlYmtpdC1zY3JvbGxiYXJ7XHJcbiAgd2lkdGg6IDZweDtcclxuICBoZWlnaHQ6IDZweDtcclxufVxyXG5cclxuLnNjb3Blczo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIsIC5hbGxTY29wZXM6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcclxufVxyXG5cclxuLmNhdGVnb3JpYXMge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG5cclxuLmNhdGVnb3JpYXM6aG92ZXJ7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5zdWJDYXRlZ29yaWFzIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuXHJcbi5zdWJDYXRlZ29yaWFzOmhvdmVye1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWNvbm9ze1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICB3aWR0aDogNDAlO1xyXG4gIHBhZGRpbmctbGVmdDogMC4yZW07XHJcbn1cclxuXHJcbi5zdWJJY29ub3N7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIHdpZHRoOiA0MCU7XHJcbiAgbWFyZ2luLWxlZnQ6IDJlbTtcclxuICBwYWRkaW5nLWxlZnQ6IDAuMmVtO1xyXG59XHJcblxyXG4uY29sdW1uc3tcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBoZWlnaHQ6IDY1JTtcclxuICBzZWN0aW9ue1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB9XHJcbn1cclxuXHJcbi5idXR0b25zIHtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgYnV0dG9uIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XHJcbiAgfVxyXG4gIC5hcHBseS1idXR0b24ge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTQ2ZTc1O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcclxuICAgIHdpZHRoOiAyMDBweDtcclxuXHJcbiAgICAmOmhvdmVyIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ViNGU0ZTtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLm1yIHtcclxuICBtYXJnaW4tcmlnaHQ6IDJlbTtcclxufVxyXG5cclxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtY2hlY2tib3gtY2hlY2tlZC5tYXQtYWNjZW50IC5tYXQtY2hlY2tib3gtYmFja2dyb3VuZCwubWF0LWNoZWNrYm94LWluZGV0ZXJtaW5hdGUubWF0LWFjY2VudCAubWF0LWNoZWNrYm94LWJhY2tncm91bmQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYjRlNGU7XHJcbn1cclxuXHJcbjpob3N0IDo6bmctZGVlcCAubWF0LWNoZWNrYm94LWxheW91dHtcclxuICBwYWRkaW5nOiAwLjVlbTtcclxufVxyXG5cclxuLnNlbGVjdEFsbCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICB3aWR0aDogMTAwJTtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1hcmdpbjogMTBweDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgxMjgsIDEyOCwgMTI4LCAwLjQ0NSk7XHJcbn1cclxuIiwibWF0LWNhcmQge1xuICBtYXJnaW46IDFyZW07XG59XG5cbi5yb2xlSW5mbyB7XG4gIHdpZHRoOiAzMCU7XG59XG5cbi5yb2xlQ2hlY2sge1xuICB3aWR0aDogNzAlO1xufVxuXG4uY2FyZHMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luOiAzZW07XG59XG5cbi5jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG59XG5cbi5jb250ZW50IHtcbiAgbWFyZ2luOiAyZW07XG59XG5cbi5jaGVja2JveENoaWxkIHtcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XG59XG5cbi5tYXQtaWNvbiB7XG4gIG1hcmdpbi1yaWdodDogMC41ZW07XG59XG5cbi5zY29wZXMge1xuICBwYWRkaW5nLXJpZ2h0OiAxZW07XG4gIGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcbiAgbWFyZ2luLWxlZnQ6IDJlbTtcbiAgaGVpZ2h0OiAzM3Z3O1xuICBvdmVyZmxvdy15OiBzY3JvbGw7XG59XG5cbi5hbGxTY29wZXMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nLXJpZ2h0OiAxZW07XG4gIGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcbiAgaGVpZ2h0OiAzM3Z3O1xuICBvdmVyZmxvdy15OiBzY3JvbGw7XG4gIGZsZXgtZmxvdzogd3JhcDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gIGNvbHVtbi1nYXA6IDAuNXJlbTtcbn1cbi5hbGxTY29wZXMgLm9uZVNjb3BlIHtcbiAgd2lkdGg6IDMzJTtcbn1cbi5hbGxTY29wZXMgLm9uZVNjb3BlOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uaHIge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLnNjb3Blczo6LXdlYmtpdC1zY3JvbGxiYXIsIC5hbGxTY29wZXM6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDZweDtcbiAgaGVpZ2h0OiA2cHg7XG59XG5cbi5zY29wZXM6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iLCAuYWxsU2NvcGVzOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLmNhdGVnb3JpYXMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG5cbi5jYXRlZ29yaWFzOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uc3ViQ2F0ZWdvcmlhcyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuLnN1YkNhdGVnb3JpYXM6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pY29ub3Mge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aWR0aDogNDAlO1xuICBwYWRkaW5nLWxlZnQ6IDAuMmVtO1xufVxuXG4uc3ViSWNvbm9zIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2lkdGg6IDQwJTtcbiAgbWFyZ2luLWxlZnQ6IDJlbTtcbiAgcGFkZGluZy1sZWZ0OiAwLjJlbTtcbn1cblxuLmNvbHVtbnMge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgaGVpZ2h0OiA2NSU7XG59XG4uY29sdW1ucyBzZWN0aW9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5idXR0b25zIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJ1dHRvbnMgYnV0dG9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbn1cbi5idXR0b25zIC5hcHBseS1idXR0b24ge1xuICBjb2xvcjogI2ZmZjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U0NmU3NTtcbiAgYm9yZGVyLXJhZGl1czogMjRweDtcbiAgd2lkdGg6IDIwMHB4O1xufVxuLmJ1dHRvbnMgLmFwcGx5LWJ1dHRvbjpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlYjRlNGU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLm1yIHtcbiAgbWFyZ2luLXJpZ2h0OiAyZW07XG59XG5cbjpob3N0IDo6bmctZGVlcCAubWF0LWNoZWNrYm94LWNoZWNrZWQubWF0LWFjY2VudCAubWF0LWNoZWNrYm94LWJhY2tncm91bmQsIC5tYXQtY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS5tYXQtYWNjZW50IC5tYXQtY2hlY2tib3gtYmFja2dyb3VuZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlYjRlNGU7XG59XG5cbjpob3N0IDo6bmctZGVlcCAubWF0LWNoZWNrYm94LWxheW91dCB7XG4gIHBhZGRpbmc6IDAuNWVtO1xufVxuXG4uc2VsZWN0QWxsIHtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDEwMCU7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW46IDEwcHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDEyOCwgMTI4LCAxMjgsIDAuNDQ1KTtcbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RoleDetailComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-role-detail',
          templateUrl: './role-detail.component.html',
          styleUrls: ['./role-detail.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: src_app_auth_role_infrastructure_services_role_service__WEBPACK_IMPORTED_MODULE_5__["RoleService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/auth/role/infrastructure/ng-components/roles-table/config.ts":
  /*!******************************************************************************!*\
    !*** ./src/app/auth/role/infrastructure/ng-components/roles-table/config.ts ***!
    \******************************************************************************/

  /*! exports provided: table, filters */

  /***/
  function srcAppAuthRoleInfrastructureNgComponentsRolesTableConfigTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "table", function () {
      return table;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "filters", function () {
      return filters;
    });

    var table = {
      multiSelect: false,
      columns: [{
        name: 'Id',
        prop: 'id',
        shown: true,
        route: './app/systems-administrator/roles/role-detail/',
        cellClass: 'id-table-column',
        routeId: 'id'
      }, {
        name: 'Name',
        prop: 'name',
        shown: true
      }, {
        name: 'Description',
        prop: 'description',
        shown: true
      }, {
        name: 'Value',
        prop: 'value',
        shown: true
      }]
    };
    var filters = [{
      options: [],
      prop: 'id',
      retProp: '',
      type: 'input',
      appearance: 'outline',
      "class": 'input',
      label: 'Id',
      placeholder: 'Id',
      shown: true
    }, {
      options: [],
      prop: 'name',
      retProp: '',
      type: 'input',
      appearance: 'outline',
      "class": 'input',
      label: 'Name',
      placeholder: 'Name',
      shown: true
    }, {
      options: [],
      prop: 'value',
      retProp: '',
      type: 'input',
      appearance: 'outline',
      "class": 'input',
      label: 'Value',
      placeholder: 'Value',
      shown: true
    }];
    /***/
  },

  /***/
  "./src/app/auth/role/infrastructure/ng-components/roles-table/roles-table.component.ts":
  /*!*********************************************************************************************!*\
    !*** ./src/app/auth/role/infrastructure/ng-components/roles-table/roles-table.component.ts ***!
    \*********************************************************************************************/

  /*! exports provided: RolesTableComponent */

  /***/
  function srcAppAuthRoleInfrastructureNgComponentsRolesTableRolesTableComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RolesTableComponent", function () {
      return RolesTableComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic */
    "./src/app/shared/custom-mat-elements/dynamic.ts");
    /* harmony import */


    var _config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./config */
    "./src/app/auth/role/infrastructure/ng-components/roles-table/config.ts");
    /* harmony import */


    var _application_getRoles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../application/getRoles */
    "./src/app/auth/role/application/getRoles.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _services_role_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../services/role.service */
    "./src/app/auth/role/infrastructure/services/role.service.ts");
    /* harmony import */


    var src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/auth/session/infrastructure/services/cookie.service */
    "./src/app/auth/session/infrastructure/services/cookie.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component */
    "./src/app/shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component.ts");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-filters/dynamic-filters.component */
    "./src/app/shared/custom-mat-elements/dynamic-filters/dynamic-filters.component.ts");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component */
    "./src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component.ts");

    var _c0 = ["table"];

    function RolesTableComponent_mat_card_0_Template(rf, ctx) {
      if (rf & 1) {
        var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h2");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "ROLES");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " New ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "app-dynamic-filters", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("query", function RolesTableComponent_mat_card_0_Template_app_dynamic_filters_query_9_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r2.filtersChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function RolesTableComponent_mat_card_0_Template_button_click_10_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r4.setConfigVisibility(!ctx_r4.configVisibility);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, " Settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "app-dynamic-table-mark-ii", 8, 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("pagination", function RolesTableComponent_mat_card_0_Template_app_dynamic_table_mark_ii_pagination_14_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r5.paginationChanged($event);
        })("order", function RolesTableComponent_mat_card_0_Template_app_dynamic_table_mark_ii_order_14_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r6.orderChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("filters", ctx_r0.filters)("showFilters", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("config", ctx_r0.tableConfig)("data", ctx_r0.tableData);
      }
    }

    var RolesTableComponent = /*#__PURE__*/function () {
      function RolesTableComponent(_router, _route, _service, _cookieFacade) {
        _classCallCheck(this, RolesTableComponent);

        this._router = _router;
        this._route = _route;
        this._service = _service;
        this._cookieFacade = _cookieFacade;
        this.tableConfig = _config__WEBPACK_IMPORTED_MODULE_2__["table"];
        this.filters = _config__WEBPACK_IMPORTED_MODULE_2__["filters"];
        this.queryFilters = {};
        this.loading = true;
        this.configVisibility = false;
      }

      _createClass(RolesTableComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _a, _b;

          var _this$_route$snapshot = this._route.snapshot.data['response'],
              data = _this$_route$snapshot.data,
              error = _this$_route$snapshot.error,
              filtersValue = _this$_route$snapshot.filtersValue,
              tableFiltersConfig = _this$_route$snapshot.tableFiltersConfig;

          if (data) {
            this.tableData = data;
            this.filtersValue = filtersValue;
            this.loading = false;
            Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["parseTableFiltersConfig"])(tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = data.filtersValue ? this._clean(JSON.parse(data.filtersValue)) : {};
            this.tableConfig.pagination = (_a = tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.pagination) !== null && _a !== void 0 ? _a : {
              page: 0,
              size: 10
            };
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = (_b = tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.order) !== null && _b !== void 0 ? _b : {
              orderField: '',
              order: ''
            };
            this.queryOrder = Object.assign({}, this.tableConfig.order);
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ? Object.entries(_defineProperty({}, this.tableConfig.order.orderField, '')).find(function (entry) {
              return entry[1] === '';
            })[0] : '';
          }

          this.configFilterValue();
        }
      }, {
        key: "configFilterValue",
        value: function configFilterValue() {
          var _this8 = this;

          var queryFiltersAux = this.filtersValue ? this._clean(JSON.parse(this.filtersValue)) : {};
          this.filters.forEach(function (filter) {
            filter.defaultValue = queryFiltersAux[_this8.getFilterProp(filter)];
            filter.value = queryFiltersAux[_this8.getFilterProp(filter)];
          });
        }
      }, {
        key: "getFilterProp",
        value: function getFilterProp(filter) {
          return filter.prop ? filter.type === 'autoselect' ? filter.searchValue : filter.prop : filter.searchValue;
        }
      }, {
        key: "_clean",
        value: function _clean(object) {
          var cleaned = {};
          var keys = Object.keys(object);
          keys.forEach(function (key) {
            if (object[key]) {
              cleaned[key] = object[key];
            }
          });
          return cleaned;
        }
      }, {
        key: "filtersChanged",
        value: function filtersChanged($event) {
          this.queryFilters = $event && Object.keys($event).length !== 0 ? $event : {};

          this._cookieFacade.save('rolesFilterCache', JSON.stringify(this.queryFilters));

          this.queryPagination = {
            page: 0,
            size: this.queryPagination.size
          };

          this._cookieFacade.save('role-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.dynamicTable.resetPageIndex();
          this.backSearch();
        }
      }, {
        key: "paginationChanged",
        value: function paginationChanged($event) {
          this.queryPagination.page = $event.page;
          this.queryPagination.size = $event.size;

          this._cookieFacade.save('role-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "orderChanged",
        value: function orderChanged($event) {
          var aux = _defineProperty({}, $event.orderField, '');

          this.queryOrder.orderField = Object.entries(aux).find(function (entry) {
            return entry[1] === '';
          })[0];
          this.queryOrder.order = $event.order;
          this.queryPagination.page = $event.page;
          this.queryPagination.size = $event.size;

          this._cookieFacade.save('role-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "configChange",
        value: function configChange(event) {
          this.tableConfig = Object.assign({}, event[0]);
          this.filters = event[1];

          this._cookieFacade.save('role-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        }
      }, {
        key: "setConfigVisibility",
        value: function setConfigVisibility(visible) {
          this.configVisibility = visible;
        }
      }, {
        key: "backSearch",
        value: function backSearch() {
          var _this9 = this;

          Object(_application_getRoles__WEBPACK_IMPORTED_MODULE_3__["getRoles"])(Object.assign(Object.assign(Object.assign({}, this.queryPagination), this.queryOrder), this.queryFilters), this._service).subscribe(function (response) {
            _this9.tableData = {
              content: response.content,
              totalElements: response.totalElements,
              numberOfElements: response.numberOfElements,
              totalPages: response.totalPages
            };
          });
        }
      }, {
        key: "navigate",
        value: function navigate() {
          this._router.navigate(['new'], {
            relativeTo: this._route
          });
        }
      }]);

      return RolesTableComponent;
    }();

    RolesTableComponent.ɵfac = function RolesTableComponent_Factory(t) {
      return new (t || RolesTableComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_role_service__WEBPACK_IMPORTED_MODULE_5__["RoleService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_6__["CookieFacadeService"]));
    };

    RolesTableComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: RolesTableComponent,
      selectors: [["app-roles-table"]],
      viewQuery: function RolesTableComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dynamicTable = _t.first);
        }
      },
      decls: 2,
      vars: 4,
      consts: [["class", "card-component", 4, "ngIf"], [3, "visibility", "columnsConfig", "filtersConfig", "configChanged", "closed"], [1, "card-component"], [1, "title-component"], ["mat-flat-button", "", "routerLink", "./new"], [1, "filters-plus-config"], [1, "dyn-filters", 3, "filters", "showFilters", "query"], ["mat-button", "", 1, "configButton", 3, "click"], [1, "card-content", 3, "config", "data", "pagination", "order"], ["table", ""]],
      template: function RolesTableComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, RolesTableComponent_mat_card_0_Template, 16, 4, "mat-card", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-dynamic-table-config-panel", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("configChanged", function RolesTableComponent_Template_app_dynamic_table_config_panel_configChanged_1_listener($event) {
            return ctx.configChange($event);
          })("closed", function RolesTableComponent_Template_app_dynamic_table_config_panel_closed_1_listener() {
            return ctx.setConfigVisibility(false);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visibility", ctx.configVisibility)("columnsConfig", ctx.tableConfig)("filtersConfig", ctx.filters);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_8__["DynamicTableConfigPanelComponent"], _angular_material_card__WEBPACK_IMPORTED_MODULE_9__["MatCard"], _angular_material_button__WEBPACK_IMPORTED_MODULE_10__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLink"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIcon"], _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_12__["DynamicFiltersComponent"], _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_13__["DynamicTableMarkIiComponent"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9yb2xlL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvcm9sZXMtdGFibGUvQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFzc2V0c1xcc3R5bGVzXFxzdGFmZml0LXN0eWxlLTEuc2NzcyIsInNyYy9hcHAvYXV0aC9yb2xlL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvcm9sZXMtdGFibGUvcm9sZXMtdGFibGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0NBQUE7QUFRQSx3QkFBQTtBQVFBLHVCQUFBO0FBR0EsMkJBQUE7QUFFQTtFQUNFLDZCQUFBO0FDaEJGO0FEa0JFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUNoQko7QURrQkk7RUFDRSxtQkEzQlE7RUE0QlIsZ0JBQUE7RUFDQSxjQUFBO0FDaEJOO0FEc0JBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDbkJGO0FEcUJFO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNuQko7QURxQkk7RUFDRSxZQUFBO0FDbkJOO0FEc0JFO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0FDcEJKO0FEd0JBO0VBQ0UsNkJBQUE7RUFDQSxlQUFBO0FDckJGO0FEdUJFO0VBQ0UsU0FBQTtBQ3JCSjtBRHdCRTtFQUNFLGdDQUFBO0FDdEJKO0FEeUJFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUN2Qko7QUR5Qkk7RUFDRSxtQkEzRVE7RUE0RVIsZ0JBQUE7RUFDQSxjQUFBO0FDdkJOO0FEMEJJO0VBQ0UseUJBNUVpQjtFQTZFakIsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ3hCTjtBRDZCQTtFQUNFLGNBbEZtQjtBQ3dEckI7QUQ2QkE7RUFDRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxXQUFBO0FDMUJGO0FENkJBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDMUJGO0FENkJBO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQzFCRjtBRDRCRTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQzFCSjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZUFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0FDM0JGO0FEa0NBO0VBQ0UsY0FBQTtBQy9CRjtBRGtDQSx5QkFBQTtBQUVBO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtREFBQTtFQUNBLGdCQUFBO0VBQ0EsMEJBQUE7RUFDQSx5QkFBQTtBQ2hDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSxjQUFBO0FDbENGO0FEcUNBLFlBQUE7QUFFQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsdUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxlQUFBO0FDbkNGO0FEc0NBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSwwQ0FBQTtBQ25DRiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvcm9sZS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3JvbGVzLXRhYmxlL3JvbGVzLXRhYmxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xyXG4kc21hbGw6IDAuOHJlbTtcclxuJG1lZGl1bTogMXJlbTtcclxuJGxhcmdlOiAxLjI1cmVtO1xyXG4kZXh0cmEtbGFyZ2U6IDEuNTYzcmVtO1xyXG4kaHVnZTogMS45NTNyZW07XHJcbiRleHRyYS1odWdlOiAyLjQ0MXJlbTtcclxuXHJcbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cclxuJGNvbG9yLXBhbGV0dGUtMS1tYWluOiAjREU3MzczO1xyXG4kY29sb3ItcGFsZXR0ZS0xLXNlY29uZDogI0FCQUJGRjtcclxuJGNvbG9yLXByaW1hcnktbGlnaHQ6ICNFRkVGRUY7XHJcbiRjb2xvci1wcmltYXJ5LWRhcms6ICMxOTE5MTk7XHJcbiRjb2xvci1zZWNvbmRhcnktZGFyazogIzMzMzMzMztcclxuJGNvbG9yLXNjcm9sbGJhci10aHVtYjogIzhDOEM4QztcclxuXHJcbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xyXG5cclxuXHJcbi8qID09PT09IFRFTVBMQVRFIDEgPT09PT0gKi9cclxuXHJcbi5jLWNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuXHJcbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAuZHluLWZpbHRlcnMge1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG4gICAgb3ZlcmZsb3cteDogYXV0bztcclxuICAgIG1hcmdpbjogMCA0MHB4IDAgMDtcclxuXHJcbiAgICA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xyXG4gICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb25maWdCdXR0b24ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGZsZXgtc2hyaW5rOiAwO1xyXG4gIH1cclxufVxyXG5cclxuLmNhcmQtY29tcG9uZW50IHtcclxuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcclxuICBtYXgtaGVpZ2h0OiA4OCU7XHJcblxyXG4gIC5jYXJkLWNvbnRlbnQsIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuXHJcbiAgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcblxyXG4gICAgaDIge1xyXG4gICAgICBmb250LXNpemU6ICRleHRyYS1sYXJnZTtcclxuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgbWFyZ2luOiAwLjVyZW07XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXBhbGV0dGUtMS1tYWluO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAycmVtO1xyXG4gICAgICBoZWlnaHQ6IDIuM3JlbTtcclxuICAgICAgY29sb3I6ICNlZmVmZWY7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1zZWxlY3QtdmFsdWUge1xyXG4gIGNvbG9yOiAkY29sb3ItcHJpbWFyeS1kYXJrO1xyXG59XHJcblxyXG4uZmlsdGVyIHtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4ubWF0LWZvcm0tZmllbGQge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcclxuICBtYXgtd2lkdGg6IDEwZW07XHJcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gIC8vIG1heC1oZWlnaHQ6IDgwMHB4O1xyXG4gIC5tYXQtdGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiB0YWJsZTtcclxuICB9XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi50YWJsZS1yZXNwb25zaXZlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG5cclxuLm1hdC1oZWFkZXItcm93IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxuICBib3JkZXItcmFkaXVzOiAwLjVyZW07XHJcbiAgY29sb3I6ICM4YjhiOGI7XHJcbn1cclxuXHJcbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xyXG4gIHBhZGRpbmctbGVmdDogMDtcclxufVxyXG5cclxuLmlkSGVhZGVyIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5pZENlbGwge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxubWF0LWNhcmQtY29udGVudCB7XHJcbiAgbWFyZ2luOiAwIDM0cHg7XHJcbn1cclxuXHJcbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXHJcblxyXG4ucHJpb3JpdHkge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDVweCAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjE1KSAxLjk1cHggMS45NXB4IDIuNnB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLy8gQFRPRE86IGNoYW5nZSBjb2xvciB0byB2YXJpYWJsZXNcclxuXHJcbi5wcmlvcml0eS1tdXlhbHRhIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDg0ODU1O1xyXG59XHJcblxyXG4ucHJpb3JpdHktYWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcclxufVxyXG5cclxuLnByaW9yaXR5LW5vcm1hbCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcclxufVxyXG5cclxuLnByaW9yaXR5LWJhamEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNhM2Q2OWQ7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1uaW5ndW5hIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xyXG59XHJcblxyXG4ucmVkLWRhdGUge1xyXG4gIGNvbG9yOiAjZWI0MTQxO1xyXG59XHJcblxyXG4vKiBNQVQgVEFCICovXHJcblxyXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcclxuICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWhlYWRlciB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIC4xMik7XHJcbn1cclxuIiwiLyogPT09PT0gRk9OVFMgKE1ham9yIFRoaXJkKSA9PT09PSAqL1xuLyogPT09PT0gUEFMRVRURSA9PT09PSAqL1xuLyogPT09PT0gV0VJR0hUID09PT09ICovXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXG4uYy1jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIHBhZGRpbmc6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uYy1jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG5cbi5maWx0ZXJzLXBsdXMtY29uZmlnIHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyB7XG4gIGZsZXgtZ3JvdzogMTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbiAgbWFyZ2luOiAwIDQwcHggMCAwO1xufVxuLmZpbHRlcnMtcGx1cy1jb25maWcgLmR5bi1maWx0ZXJzIDo6bmctZGVlcCBtYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxNTBweDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5jb25maWdCdXR0b24ge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBmbGV4LXNocmluazogMDtcbn1cblxuLmNhcmQtY29tcG9uZW50IHtcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XG4gIG1heC1oZWlnaHQ6IDg4JTtcbn1cbi5jYXJkLWNvbXBvbmVudCAuY2FyZC1jb250ZW50LCAuY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMDtcbn1cbi5jYXJkLWNvbXBvbmVudCA6Om5nLWRlZXAgbWF0LXRhYi1ncm91cCBtYXQtdGFiLWhlYWRlciB7XG4gIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQgaDIge1xuICBmb250LXNpemU6IDEuNTYzcmVtO1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDAuNXJlbTtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNERTczNzM7XG4gIGJvcmRlci1yYWRpdXM6IDJyZW07XG4gIGhlaWdodDogMi4zcmVtO1xuICBjb2xvcjogI2VmZWZlZjtcbn1cblxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcbiAgY29sb3I6ICMxOTE5MTk7XG59XG5cbi5maWx0ZXIge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMC41ZW07XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5tYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5tYXQtZm9ybS1maWVsZCBtYXQtc2VsZWN0IHtcbiAgbWF4LXdpZHRoOiAxMGVtO1xuICBtYXJnaW4tcmlnaHQ6IDVlbTtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmUge1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93LXg6IGF1dG87XG59XG4udGFibGUtcmVzcG9uc2l2ZSAubWF0LXRhYmxlIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgZGlzcGxheTogdGFibGU7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnNjcm9sbGJhci1zdHlsZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICB3aWR0aDogMTJweDtcbiAgaGVpZ2h0OiAxMnB4O1xufVxuXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufVxuXG4ubWF0LWhlYWRlci1yb3cge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xuICBjb2xvcjogIzhiOGI4Yjtcbn1cblxuLmlkQ2VsbDpmaXJzdC1vZi10eXBlLCAuaWRIZWFkZXI6Zmlyc3Qtb2YtdHlwZSB7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cblxuLmlkSGVhZGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmlkQ2VsbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdvcmQtYnJlYWs6IGJyZWFrLWFsbDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG5tYXQtY2FyZC1jb250ZW50IHtcbiAgbWFyZ2luOiAwIDM0cHg7XG59XG5cbi8qID09PT09IFBSSU9SSVRZID09PT09ICovXG4ucHJpb3JpdHkge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5wcmlvcml0eS1tdXlhbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcbn1cblxuLnByaW9yaXR5LWFsdGEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTk3MTcxO1xufVxuXG4ucHJpb3JpdHktbm9ybWFsIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmY2I4ZTtcbn1cblxuLnByaW9yaXR5LWJhamEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xufVxuXG4ucHJpb3JpdHktbmluZ3VuYSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XG59XG5cbi5yZWQtZGF0ZSB7XG4gIGNvbG9yOiAjZWI0MTQxO1xufVxuXG4vKiBNQVQgVEFCICovXG46Om5nLWRlZXAgbWF0LXRhYi1ncm91cCB7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbCAubWF0LXRhYi1sYWJlbC1jb250ZW50IHtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwtY29udGVudCBzcGFuIHtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xuICBib3JkZXItYm90dG9tOiAwIHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG59Il19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RolesTableComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-roles-table',
          templateUrl: './roles-table.component.html',
          styleUrls: ['./roles-table.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _services_role_service__WEBPACK_IMPORTED_MODULE_5__["RoleService"]
        }, {
          type: src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_6__["CookieFacadeService"]
        }];
      }, {
        dynamicTable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['table']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/auth/role/infrastructure/resolvers/role-detail-resolver.service.ts":
  /*!************************************************************************************!*\
    !*** ./src/app/auth/role/infrastructure/resolvers/role-detail-resolver.service.ts ***!
    \************************************************************************************/

  /*! exports provided: RoleDetailResolverService */

  /***/
  function srcAppAuthRoleInfrastructureResolversRoleDetailResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoleDetailResolverService", function () {
      return RoleDetailResolverService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var src_app_auth_role_application_getRoles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/auth/role/application/getRoles */
    "./src/app/auth/role/application/getRoles.ts");
    /* harmony import */


    var src_app_auth_scope_application_getScopes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/auth/scope/application/getScopes */
    "./src/app/auth/scope/application/getScopes.ts");
    /* harmony import */


    var src_app_auth_role_infrastructure_services_role_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/auth/role/infrastructure/services/role.service */
    "./src/app/auth/role/infrastructure/services/role.service.ts");
    /* harmony import */


    var src_app_auth_scope_infrastructure_services_scope_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/auth/scope/infrastructure/services/scope.service */
    "./src/app/auth/scope/infrastructure/services/scope.service.ts");

    var RoleDetailResolverService = /*#__PURE__*/function () {
      function RoleDetailResolverService(roleService, scopeService) {
        _classCallCheck(this, RoleDetailResolverService);

        this.roleService = roleService;
        this.scopeService = scopeService;
      }

      _createClass(RoleDetailResolverService, [{
        key: "resolve",
        value: function resolve(route, state) {
          var _this10 = this;

          var $scopes = Object(src_app_auth_scope_application_getScopes__WEBPACK_IMPORTED_MODULE_4__["getScopes"])({
            page: 0,
            size: 1000
          }, this.scopeService);
          var $role = Object(src_app_auth_role_application_getRoles__WEBPACK_IMPORTED_MODULE_3__["getRoles"])({
            id: route.params['id']
          }, this.roleService);
          var fork = route.params['id'] ? Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["forkJoin"])($scopes, $role) : Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["forkJoin"])($scopes);
          return fork.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return {
              data: {
                scopes: res[0],
                role: res[1] ? _this10.join(res[1].content[0], res[0]) : null
              }
            };
          }));
        }
      }, {
        key: "join",
        value: function join(role, scopes) {
          role.scopes = scopes.filter(function (s) {
            return role.scopes.includes(s.id);
          }).map(function (s) {
            return s.name;
          });
          return role;
        }
      }]);

      return RoleDetailResolverService;
    }();

    RoleDetailResolverService.ɵfac = function RoleDetailResolverService_Factory(t) {
      return new (t || RoleDetailResolverService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_auth_role_infrastructure_services_role_service__WEBPACK_IMPORTED_MODULE_5__["RoleService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_auth_scope_infrastructure_services_scope_service__WEBPACK_IMPORTED_MODULE_6__["ScopeService"]));
    };

    RoleDetailResolverService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: RoleDetailResolverService,
      factory: RoleDetailResolverService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RoleDetailResolverService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: src_app_auth_role_infrastructure_services_role_service__WEBPACK_IMPORTED_MODULE_5__["RoleService"]
        }, {
          type: src_app_auth_scope_infrastructure_services_scope_service__WEBPACK_IMPORTED_MODULE_6__["ScopeService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/auth/role/infrastructure/resolvers/role-table-resolver.service.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/auth/role/infrastructure/resolvers/role-table-resolver.service.ts ***!
    \***********************************************************************************/

  /*! exports provided: RoleTableResolverService */

  /***/
  function srcAppAuthRoleInfrastructureResolversRoleTableResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoleTableResolverService", function () {
      return RoleTableResolverService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _application_getRoles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../application/getRoles */
    "./src/app/auth/role/application/getRoles.ts");
    /* harmony import */


    var _services_role_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/role.service */
    "./src/app/auth/role/infrastructure/services/role.service.ts");
    /* harmony import */


    var src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/auth/session/infrastructure/services/cookie.service */
    "./src/app/auth/session/infrastructure/services/cookie.service.ts");

    var RoleTableResolverService = /*#__PURE__*/function () {
      function RoleTableResolverService(_service, _cookieService) {
        _classCallCheck(this, RoleTableResolverService);

        this._service = _service;
        this._cookieService = _cookieService;
      }

      _createClass(RoleTableResolverService, [{
        key: "resolve",
        value: function resolve(route, state) {
          var _a, _b;

          var filtersCache = this._cookieService.get('rolesFilterCache');

          var tableFiltersConfig = JSON.parse(this._cookieService.get('role-table-config'));
          var orderParams = (_a = Object.assign({}, tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.order)) !== null && _a !== void 0 ? _a : {};
          var rolParams = (_b = tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.pagination) !== null && _b !== void 0 ? _b : {
            page: 0,
            size: 10
          };
          return Object(_application_getRoles__WEBPACK_IMPORTED_MODULE_3__["getRoles"])(Object.assign(Object.assign(Object.assign({}, JSON.parse(filtersCache)), rolParams), orderParams), this._service).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return {
              data: res,
              tableFiltersConfig: tableFiltersConfig,
              filtersValue: filtersCache
            };
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])({
              data: null,
              error: err,
              message: 'Error on role table resolver, data couldn\'t be fetched'
            });
          }));
        }
      }]);

      return RoleTableResolverService;
    }();

    RoleTableResolverService.ɵfac = function RoleTableResolverService_Factory(t) {
      return new (t || RoleTableResolverService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_role_service__WEBPACK_IMPORTED_MODULE_4__["RoleService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieFacadeService"]));
    };

    RoleTableResolverService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: RoleTableResolverService,
      factory: RoleTableResolverService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RoleTableResolverService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _services_role_service__WEBPACK_IMPORTED_MODULE_4__["RoleService"]
        }, {
          type: src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieFacadeService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/auth/role/infrastructure/services/role.service.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/auth/role/infrastructure/services/role.service.ts ***!
    \*******************************************************************/

  /*! exports provided: RoleService */

  /***/
  function srcAppAuthRoleInfrastructureServicesRoleServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RoleService", function () {
      return RoleService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _abstract_role_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../abstract-role-service */
    "./src/app/auth/role/infrastructure/abstract-role-service.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _role_mapper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../role-mapper */
    "./src/app/auth/role/infrastructure/role-mapper.ts");

    var RoleService = /*#__PURE__*/function (_abstract_role_servic) {
      _inherits(RoleService, _abstract_role_servic);

      var _super = _createSuper(RoleService);

      function RoleService(httpClient, mapper) {
        var _this11;

        _classCallCheck(this, RoleService);

        _this11 = _super.call(this);
        _this11.httpClient = httpClient;
        _this11.mapper = mapper;
        _this11.BASE_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].baseUrl;
        _this11.ENDPOINT = 'roles';
        return _this11;
      }

      _createClass(RoleService, [{
        key: "addScope",
        value: function addScope(roleId, scopes) {
          return this.httpClient.post("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(roleId, "/scopes"), {
            scope_ids: scopes
          });
        }
      }, {
        key: "updateScope",
        value: function updateScope(roleId, scopes) {
          return this.httpClient.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(roleId, "/scopes"), {
            scope_ids: scopes
          });
        }
      }, {
        key: "createRole",
        value: function createRole(role) {
          return this.httpClient.post("".concat(this.BASE_URL).concat(this.ENDPOINT), role);
        }
      }, {
        key: "deleteRole",
        value: function deleteRole(id) {
          return this.httpClient["delete"]("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(id));
        }
      }, {
        key: "search",
        value: function search(params) {
          return this.httpClient.get("".concat(this.BASE_URL).concat(this.ENDPOINT, "/search"), {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]({
              fromObject: params
            })
          });
        }
      }]);

      return RoleService;
    }(_abstract_role_service__WEBPACK_IMPORTED_MODULE_1__["AbstractRoleService"]);

    RoleService.ɵfac = function RoleService_Factory(t) {
      return new (t || RoleService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_role_mapper__WEBPACK_IMPORTED_MODULE_4__["RoleMapper"]));
    };

    RoleService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: RoleService,
      factory: RoleService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](RoleService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }, {
          type: _role_mapper__WEBPACK_IMPORTED_MODULE_4__["RoleMapper"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/auth/scope/application/getScopes.ts":
  /*!*****************************************************!*\
    !*** ./src/app/auth/scope/application/getScopes.ts ***!
    \*****************************************************/

  /*! exports provided: getScopes */

  /***/
  function srcAppAuthScopeApplicationGetScopesTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getScopes", function () {
      return getScopes;
    });

    function getScopes(params, service) {
      return service.search(params);
    }
    /***/

  },

  /***/
  "./src/app/auth/scope/infrastructure/abstract-scope-service.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/auth/scope/infrastructure/abstract-scope-service.ts ***!
    \*********************************************************************/

  /*! exports provided: AbstractScopeService */

  /***/
  function srcAppAuthScopeInfrastructureAbstractScopeServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AbstractScopeService", function () {
      return AbstractScopeService;
    });

    var AbstractScopeService = function AbstractScopeService() {
      _classCallCheck(this, AbstractScopeService);
    };
    /***/

  },

  /***/
  "./src/app/auth/scope/infrastructure/ng-components/scope-management/scope-management.component.ts":
  /*!********************************************************************************************************!*\
    !*** ./src/app/auth/scope/infrastructure/ng-components/scope-management/scope-management.component.ts ***!
    \********************************************************************************************************/

  /*! exports provided: ScopeManagementComponent */

  /***/
  function srcAppAuthScopeInfrastructureNgComponentsScopeManagementScopeManagementComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ScopeManagementComponent", function () {
      return ScopeManagementComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/material/checkbox */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");

    function ScopeManagementComponent_div_1_mat_checkbox_5_Template(rf, ctx) {
      if (rf & 1) {
        var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-checkbox", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function ScopeManagementComponent_div_1_mat_checkbox_5_Template_mat_checkbox_change_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5);

          var scope_r3 = ctx.$implicit;

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r4.detectScopeChange(scope_r3.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var scope_r3 = ctx.$implicit;

        var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx_r2.hasScope(scope_r3.name));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", scope_r3.name, " ");
      }
    }

    function ScopeManagementComponent_div_1_Template(rf, ctx) {
      if (rf & 1) {
        var _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-checkbox", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function ScopeManagementComponent_div_1_Template_mat_checkbox_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7);

          var entity_r1 = ctx.$implicit;

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r6.detectEntityChange(entity_r1.scopes);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, ScopeManagementComponent_div_1_mat_checkbox_5_Template, 2, 2, "mat-checkbox", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var entity_r1 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", entity_r1.name, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", entity_r1.scopes);
      }
    }

    var ScopeManagementComponent = /*#__PURE__*/function () {
      function ScopeManagementComponent() {
        _classCallCheck(this, ScopeManagementComponent);

        this.updated = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.entities = [];
      }

      _createClass(ScopeManagementComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this._setEntities(this.all);
        }
      }, {
        key: "_setEntities",
        value: function _setEntities(scopes) {
          var _this12 = this;

          var _scopes$0$value$split = scopes[0].value.split('_'),
              _scopes$0$value$split2 = _slicedToArray(_scopes$0$value$split, 1),
              current = _scopes$0$value$split2[0];

          var entityScopes = [];
          scopes.forEach(function (scope) {
            var _scope$value$split = scope.value.split('_'),
                _scope$value$split2 = _slicedToArray(_scope$value$split, 2),
                entity = _scope$value$split2[0],
                action = _scope$value$split2[1];

            if (current === entity) {
              entityScopes.push({
                id: scope.id,
                name: _this12.parseEntity(action !== null && action !== void 0 ? action : entity)
              });
            } else {
              _this12.entities.push({
                name: _this12.parseEntity(current),
                scopes: entityScopes
              });

              current = entity;
              entityScopes = [];
            }
          });
        }
      }, {
        key: "parseEntity",
        value: function parseEntity(entity) {
          var value = entity.toLowerCase();
          return value.charAt(0).toUpperCase() + value.slice(1);
        }
      }, {
        key: "hasScope",
        value: function hasScope(id) {
          return this.itemScopes.includes(id);
        }
      }, {
        key: "detectScopeChange",
        value: function detectScopeChange(id) {}
      }, {
        key: "detectEntityChange",
        value: function detectEntityChange(scopes) {}
      }]);

      return ScopeManagementComponent;
    }();

    ScopeManagementComponent.ɵfac = function ScopeManagementComponent_Factory(t) {
      return new (t || ScopeManagementComponent)();
    };

    ScopeManagementComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ScopeManagementComponent,
      selectors: [["app-scope-management"]],
      inputs: {
        all: "all",
        itemScopes: "itemScopes"
      },
      outputs: {
        updated: "updated"
      },
      decls: 2,
      vars: 1,
      consts: [[1, "entity-container"], ["class", "entity-item", 4, "ngFor", "ngForOf"], [1, "entity-item"], [1, "entity-header"], [3, "change"], [1, "entity-body"], ["class", "scope-checkbox", 3, "checked", "change", 4, "ngFor", "ngForOf"], [1, "scope-checkbox", 3, "checked", "change"]],
      template: function ScopeManagementComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ScopeManagementComponent_div_1_Template, 6, 2, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.entities);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgForOf"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_2__["MatCheckbox"]],
      styles: [".entity-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  flex-wrap: wrap;\n}\n\n.entity-item[_ngcontent-%COMP%] {\n  margin: 0.2rem;\n  width: 12rem;\n}\n\n.entity-item[_ngcontent-%COMP%]   .entity-header[_ngcontent-%COMP%] {\n  background: black;\n  border-radius: 5px;\n  color: white;\n}\n\n.entity-item[_ngcontent-%COMP%]   .entity-header[_ngcontent-%COMP%]   mat-checkbox[_ngcontent-%COMP%] {\n  color: white;\n}\n\n.entity-item[_ngcontent-%COMP%]   .entity-body[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: column;\n}\n\nmat-checkbox[_ngcontent-%COMP%] {\n  margin: 3px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9zY29wZS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3Njb3BlLW1hbmFnZW1lbnQvQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFwcFxcYXV0aFxcc2NvcGVcXGluZnJhc3RydWN0dXJlXFxuZy1jb21wb25lbnRzXFxzY29wZS1tYW5hZ2VtZW50XFxzY29wZS1tYW5hZ2VtZW50LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9hdXRoL3Njb3BlL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvc2NvcGUtbWFuYWdlbWVudC9zY29wZS1tYW5hZ2VtZW50LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDQ0Y7O0FERUE7RUFDRSxjQUFBO0VBQ0EsWUFBQTtBQ0NGOztBRENFO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNDSjs7QURDSTtFQUNFLFlBQUE7QUNDTjs7QURHRTtFQUNFLGFBQUE7RUFDQSxpQkFBQTtBQ0RKOztBREtBO0VBQ0UsV0FBQTtBQ0ZGIiwiZmlsZSI6InNyYy9hcHAvYXV0aC9zY29wZS9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3Njb3BlLW1hbmFnZW1lbnQvc2NvcGUtbWFuYWdlbWVudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lbnRpdHktY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogcm93O1xyXG4gIGZsZXgtd3JhcDogd3JhcDtcclxufVxyXG5cclxuLmVudGl0eS1pdGVtIHtcclxuICBtYXJnaW46IDAuMnJlbTtcclxuICB3aWR0aDogMTJyZW07XHJcblxyXG4gIC5lbnRpdHktaGVhZGVyIHtcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG5cclxuICAgIG1hdC1jaGVja2JveCB7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5lbnRpdHktYm9keSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1mbG93OiBjb2x1bW47XHJcbiAgfVxyXG59XHJcblxyXG5tYXQtY2hlY2tib3gge1xyXG4gIG1hcmdpbjogM3B4O1xyXG59XHJcblxyXG5cclxuIiwiLmVudGl0eS1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAgZmxleC13cmFwOiB3cmFwO1xufVxuXG4uZW50aXR5LWl0ZW0ge1xuICBtYXJnaW46IDAuMnJlbTtcbiAgd2lkdGg6IDEycmVtO1xufVxuLmVudGl0eS1pdGVtIC5lbnRpdHktaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgY29sb3I6IHdoaXRlO1xufVxuLmVudGl0eS1pdGVtIC5lbnRpdHktaGVhZGVyIG1hdC1jaGVja2JveCB7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbi5lbnRpdHktaXRlbSAuZW50aXR5LWJvZHkge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IGNvbHVtbjtcbn1cblxubWF0LWNoZWNrYm94IHtcbiAgbWFyZ2luOiAzcHg7XG59Il19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ScopeManagementComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-scope-management',
          templateUrl: './scope-management.component.html',
          styleUrls: ['./scope-management.component.scss']
        }]
      }], function () {
        return [];
      }, {
        all: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        itemScopes: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        updated: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/auth/scope/infrastructure/services/scope.service.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/auth/scope/infrastructure/services/scope.service.ts ***!
    \*********************************************************************/

  /*! exports provided: ScopeService */

  /***/
  function srcAppAuthScopeInfrastructureServicesScopeServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ScopeService", function () {
      return ScopeService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _abstract_scope_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../abstract-scope-service */
    "./src/app/auth/scope/infrastructure/abstract-scope-service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _scope_mapper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../scope-mapper */
    "./src/app/auth/scope/infrastructure/scope-mapper.ts");

    var ScopeService = /*#__PURE__*/function (_abstract_scope_servi) {
      _inherits(ScopeService, _abstract_scope_servi);

      var _super2 = _createSuper(ScopeService);

      function ScopeService(httpClient, mapper) {
        var _this13;

        _classCallCheck(this, ScopeService);

        _this13 = _super2.call(this);
        _this13.httpClient = httpClient;
        _this13.mapper = mapper;
        _this13.BASE_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].baseUrl;
        _this13.ENDPOINT = 'scopes';
        return _this13;
      }

      _createClass(ScopeService, [{
        key: "createScope",
        value: function createScope(scope) {
          return this.httpClient.post("".concat(this.BASE_URL).concat(this.ENDPOINT), scope).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.mapper.mapTo));
        }
      }, {
        key: "deleteScope",
        value: function deleteScope(id) {
          return this.httpClient["delete"]("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(id));
        }
      }, {
        key: "search",
        value: function search(params) {
          var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"](); //@TODO separate in a function

          var paramsKeys = Object.keys(params);
          paramsKeys.forEach(function (key) {
            return httpParams = httpParams.append(key, params[key].toString());
          });
          return this.httpClient.get("".concat(this.BASE_URL).concat(this.ENDPOINT, "/search"), {
            params: httpParams
          }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function (list) {
            return list.content;
          })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.mapper.mapTo)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["toArray"])());
        }
      }]);

      return ScopeService;
    }(_abstract_scope_service__WEBPACK_IMPORTED_MODULE_2__["AbstractScopeService"]);

    ScopeService.ɵfac = function ScopeService_Factory(t) {
      return new (t || ScopeService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_scope_mapper__WEBPACK_IMPORTED_MODULE_5__["ScopeMapper"]));
    };

    ScopeService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ScopeService,
      factory: ScopeService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ScopeService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }, {
          type: _scope_mapper__WEBPACK_IMPORTED_MODULE_5__["ScopeMapper"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/auth/user/application/addPersonalRol.ts":
  /*!*********************************************************!*\
    !*** ./src/app/auth/user/application/addPersonalRol.ts ***!
    \*********************************************************/

  /*! exports provided: addPersonalUserRol */

  /***/
  function srcAppAuthUserApplicationAddPersonalRolTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "addPersonalUserRol", function () {
      return addPersonalUserRol;
    });

    function addPersonalUserRol(id, service) {
      return service.addPersonalrol(id);
    }
    /***/

  },

  /***/
  "./src/app/auth/user/application/createUserName.ts":
  /*!*********************************************************!*\
    !*** ./src/app/auth/user/application/createUserName.ts ***!
    \*********************************************************/

  /*! exports provided: createUserName */

  /***/
  function srcAppAuthUserApplicationCreateUserNameTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "createUserName", function () {
      return createUserName;
    });

    function createUserName(email) {
      var surname = '';
      var spliced = email.split('@');
      var separated = spliced[0].split('.');
      var name = capitalizeFirstLetter(separated[0]);

      if (separated[1] !== undefined) {
        name = name.concat(".");
        surname = capitalizeFirstLetter(separated[1]);
      } //@TODO add uppercase to name and surname


      return "".concat(name).concat(surname);
    }

    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    /***/

  },

  /***/
  "./src/app/auth/user/application/getUsers.ts":
  /*!***************************************************!*\
    !*** ./src/app/auth/user/application/getUsers.ts ***!
    \***************************************************/

  /*! exports provided: getUsers */

  /***/
  function srcAppAuthUserApplicationGetUsersTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "getUsers", function () {
      return getUsers;
    });

    function getUsers(params, service) {
      return service.search(params);
    }
    /***/

  },

  /***/
  "./src/app/auth/user/application/lock.ts":
  /*!***********************************************!*\
    !*** ./src/app/auth/user/application/lock.ts ***!
    \***********************************************/

  /*! exports provided: lock */

  /***/
  function srcAppAuthUserApplicationLockTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "lock", function () {
      return lock;
    });

    function lock(id, service) {
      return service.lockUser(id);
    }
    /***/

  },

  /***/
  "./src/app/auth/user/application/registerNewUser.ts":
  /*!**********************************************************!*\
    !*** ./src/app/auth/user/application/registerNewUser.ts ***!
    \**********************************************************/

  /*! exports provided: registerNewUser */

  /***/
  function srcAppAuthUserApplicationRegisterNewUserTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "registerNewUser", function () {
      return registerNewUser;
    });

    function registerNewUser(user, service) {
      return service.register(user);
    }
    /***/

  },

  /***/
  "./src/app/auth/user/application/unlock.ts":
  /*!*************************************************!*\
    !*** ./src/app/auth/user/application/unlock.ts ***!
    \*************************************************/

  /*! exports provided: unlock */

  /***/
  function srcAppAuthUserApplicationUnlockTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "unlock", function () {
      return unlock;
    });

    function unlock(id, service) {
      return service.unlockUser(id);
    }
    /***/

  },

  /***/
  "./src/app/auth/user/application/updateUserRoles.ts":
  /*!**********************************************************!*\
    !*** ./src/app/auth/user/application/updateUserRoles.ts ***!
    \**********************************************************/

  /*! exports provided: updateUserRoles */

  /***/
  function srcAppAuthUserApplicationUpdateUserRolesTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "updateUserRoles", function () {
      return updateUserRoles;
    });

    function updateUserRoles(id, roles, service) {
      return service.updateRoles(id, roles);
    }
    /***/

  },

  /***/
  "./src/app/auth/user/infrastructure/abstract-user-service.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/auth/user/infrastructure/abstract-user-service.ts ***!
    \*******************************************************************/

  /*! exports provided: AbstractUserService */

  /***/
  function srcAppAuthUserInfrastructureAbstractUserServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AbstractUserService", function () {
      return AbstractUserService;
    });

    var AbstractUserService = function AbstractUserService() {
      _classCallCheck(this, AbstractUserService);
    };
    /***/

  },

  /***/
  "./src/app/auth/user/infrastructure/ng-components/user-detail/user-detail.component.ts":
  /*!*********************************************************************************************!*\
    !*** ./src/app/auth/user/infrastructure/ng-components/user-detail/user-detail.component.ts ***!
    \*********************************************************************************************/

  /*! exports provided: UserDetailComponent */

  /***/
  function srcAppAuthUserInfrastructureNgComponentsUserDetailUserDetailComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserDetailComponent", function () {
      return UserDetailComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
    /* harmony import */


    var _application_updateUserRoles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../application/updateUserRoles */
    "./src/app/auth/user/application/updateUserRoles.ts");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _application_lock__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../application/lock */
    "./src/app/auth/user/application/lock.ts");
    /* harmony import */


    var _application_unlock__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../application/unlock */
    "./src/app/auth/user/application/unlock.ts");
    /* harmony import */


    var src_app_auth_user_application_addPersonalRol__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/auth/user/application/addPersonalRol */
    "./src/app/auth/user/application/addPersonalRol.ts");
    /* harmony import */


    var src_app_auth_user_application_registerNewUser__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/auth/user/application/registerNewUser */
    "./src/app/auth/user/application/registerNewUser.ts");
    /* harmony import */


    var src_app_auth_user_application_createUserName__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! src/app/auth/user/application/createUserName */
    "./src/app/auth/user/application/createUserName.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _services_user_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../services/user.service */
    "./src/app/auth/user/infrastructure/services/user.service.ts");
    /* harmony import */


    var src_app_auth_user_infrastructure_userRegister_mapper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! src/app/auth/user/infrastructure/userRegister-mapper */
    "./src/app/auth/user/infrastructure/userRegister-mapper.ts");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/material/form-field */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
    /* harmony import */


    var _angular_material_select__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @angular/material/select */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
    /* harmony import */


    var _angular_material_core__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! @angular/material/core */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _shared_custom_mat_elements_autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/autocomplete/autocomplete.component */
    "./src/app/shared/custom-mat-elements/autocomplete/autocomplete.component.ts");
    /* harmony import */


    var _angular_material_list__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! @angular/material/list */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/list.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_input__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! @angular/material/input */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! @angular/material/slide-toggle */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/slide-toggle.js");
    /* harmony import */


    var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
    /*! @angular/material/checkbox */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");

    var _c0 = ["toggle"];
    var _c1 = ["filtro"];

    function UserDetailComponent_mat_card_content_2_mat_form_field_8_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-form-field", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Id");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "input", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function UserDetailComponent_mat_card_content_2_div_17_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Do not put white characters");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function UserDetailComponent_mat_card_content_2_mat_form_field_18_Template(rf, ctx) {
      if (rf & 1) {
        var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-form-field", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Enter your password");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "input", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UserDetailComponent_mat_card_content_2_mat_form_field_18_Template_button_click_4_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13);

          var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r12.hide = !ctx_r12.hide;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("type", ctx_r7.hide ? "password" : "text");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("aria-label", "Hide password")("aria-pressed", ctx_r7.hide);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r7.hide ? "visibility_off" : "visibility");
      }
    }

    function UserDetailComponent_mat_card_content_2_div_19_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Password should consist 8-16 letters including alphabets and numbers");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function UserDetailComponent_mat_card_content_2_mat_form_field_20_Template(rf, ctx) {
      if (rf & 1) {
        var _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-form-field", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Confirm your password");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "input", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UserDetailComponent_mat_card_content_2_mat_form_field_20_Template_button_click_4_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15);

          var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r14.hide = !ctx_r14.hide;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("type", ctx_r9.hide ? "password" : "text");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("aria-label", "Hide password")("aria-pressed", ctx_r9.hide);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r9.hide ? "visibility_off" : "visibility");
      }
    }

    function UserDetailComponent_mat_card_content_2_div_21_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "error");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Passwords do not match");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function UserDetailComponent_mat_card_content_2_p_23_Template(rf, ctx) {
      if (rf & 1) {
        var _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "strong");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Status: ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-slide-toggle", 32, 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function UserDetailComponent_mat_card_content_2_p_23_Template_mat_slide_toggle_change_3_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r18);

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r17.toggleChange();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx_r11.user.locked);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r11.user.locked ? "Locked" : "Unlocked", "");
      }
    }

    function UserDetailComponent_mat_card_content_2_Template(rf, ctx) {
      if (rf & 1) {
        var _r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card-content", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h2");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "USER INFO");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "form", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, UserDetailComponent_mat_card_content_2_mat_form_field_8_Template, 4, 0, "mat-form-field", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-form-field", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Email");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "input", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup", function UserDetailComponent_mat_card_content_2_Template_input_keyup_12_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r20);

          var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r19.completeUserName();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "mat-form-field", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "User Name");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, UserDetailComponent_mat_card_content_2_div_17_Template, 5, 0, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, UserDetailComponent_mat_card_content_2_mat_form_field_18_Template, 7, 4, "mat-form-field", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, UserDetailComponent_mat_card_content_2_div_19_Template, 5, 0, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, UserDetailComponent_mat_card_content_2_mat_form_field_20_Template, 7, 4, "mat-form-field", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, UserDetailComponent_mat_card_content_2_div_21_Template, 5, 0, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, UserDetailComponent_mat_card_content_2_p_23_Template, 6, 2, "p", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r0.form);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.user);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.checkUserName());

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.user);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.strongPassword);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.user);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.checkPassword() && ctx_r0.form.get("cpassword").value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.user);
      }
    }

    function UserDetailComponent_mat_option_13_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "USER ROLES");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function UserDetailComponent_button_18_Template(rf, ctx) {
      if (rf & 1) {
        var _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UserDetailComponent_button_18_Template_button_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r22);

          var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r21.addPersonalRol();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " New");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function UserDetailComponent_div_21_mat_icon_7_Template(rf, ctx) {
      if (rf & 1) {
        var _r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-icon", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UserDetailComponent_div_21_mat_icon_7_Template_mat_icon_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27);

          var role_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r25.deleteRole(role_r23.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "delete_forever");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function UserDetailComponent_div_21_Template(rf, ctx) {
      if (rf & 1) {
        var _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-list-item", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-checkbox", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function UserDetailComponent_div_21_Template_mat_checkbox_change_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29);

          var role_r23 = ctx.$implicit;

          var ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r28.detectRoleChange(role_r23);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-icon", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UserDetailComponent_div_21_Template_mat_icon_click_5_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29);

          var role_r23 = ctx.$implicit;

          var ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r30.viewRole(role_r23);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "send");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, UserDetailComponent_div_21_mat_icon_7_Template, 2, 0, "mat-icon", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var role_r23 = ctx.$implicit;

        var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx_r4.user === null)("checked", ctx_r4.updatedRoles.includes(role_r23.value));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", role_r23.name, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", role_r23.value !== "ROLE_ADMIN" && role_r23.value !== "ROLE_USER");
      }
    }

    var UserDetailComponent = /*#__PURE__*/function () {
      function UserDetailComponent(_router, _service, _route, _fb, _userRegisterMapper) {
        _classCallCheck(this, UserDetailComponent);

        this._router = _router;
        this._service = _service;
        this._route = _route;
        this._fb = _fb;
        this._userRegisterMapper = _userRegisterMapper;
        this.loading = true;
        this.checkedPassword = false;
        this.hide = true;
        this.strongPassword = false;
      }

      _createClass(UserDetailComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var data = this._route.snapshot.data['response'].data;

          if (data) {
            this.data = data;
            this.user = data.user;
            this.roles = this.user ? data.user.fullRoles : data.roles;
            this.scopes = data.scopes;
            this.updatedRoles = this.user ? this.roles.map(function (rol) {
              return rol.value;
            }) : [];
            this.userRolesId = this.user ? this.roles.map(function (rol) {
              return rol.id;
            }) : [];
            this.rolesName = this.roles.map(function (rol) {
              return rol.name;
            });
            this.selected = this.user ? 'user' : 'all';
            this.route = this.user ? '../../../roles/role-detail/' : '../../roles/role-detail/';
            this.initForm();
            this.loading = false;
          }
        }
      }, {
        key: "initForm",
        value: function initForm() {
          var _a, _b, _c, _d;

          this.form = this._fb.group({
            id: [{
              value: (_a = this.user) === null || _a === void 0 ? void 0 : _a.id,
              disabled: true
            }],
            email: [{
              value: this.user ? (_b = this.user) === null || _b === void 0 ? void 0 : _b.email : '',
              disabled: this.user ? true : false
            }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])],
            username: [{
              value: (_d = (_c = this.user) === null || _c === void 0 ? void 0 : _c.username) !== null && _d !== void 0 ? _d : '',
              disabled: this.user ? true : false
            }, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            password: [{
              value: '',
              disabled: this.user ? true : false
            }],
            cpassword: [{
              value: '',
              disabled: this.user ? true : false
            }]
          });
        }
      }, {
        key: "checkPassword",
        value: function checkPassword() {
          return this.checkedPassword = this.form.get('password').value === this.form.get('cpassword').value ? true : false;
        }
      }, {
        key: "completeUserName",
        value: function completeUserName() {
          this.form.get('email').value ? this.form.get('username').setValue(Object(src_app_auth_user_application_createUserName__WEBPACK_IMPORTED_MODULE_9__["createUserName"])(this.form.get('email').value)) : this.form.get('username').setValue('');
        }
      }, {
        key: "checkUserName",
        value: function checkUserName() {
          return this.form.get('username').value.includes(' ');
        }
      }, {
        key: "detectRoleChange",
        value: function detectRoleChange(role) {
          if (this.updatedRoles.includes(role.value)) {
            for (var i = 0; i < this.updatedRoles.length; i++) {
              if (this.updatedRoles[i] === role.value) {
                this.updatedRoles.splice(i, 1);
              }
            }
          } else {
            this.updatedRoles.push(role.value);
          }
        }
      }, {
        key: "onSaveClick",
        value: function onSaveClick() {
          if (!this.checkPassword() || this.checkUserName()) {
            return;
          }

          var valid = this.user ? true : this.form.valid && this.form.get('password').value && this.form.get('cpassword').value;

          if (valid) {
            this.user ? this.applyChanges() : this.create();
            return;
          }

          sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Some fields are required'
          });
        }
      }, {
        key: "applyChanges",
        value: function applyChanges() {
          var _this14 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: "".concat(this.user.email, " will now be able to do screw things up"),
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080'
          }).then(function (result) {
            if (result.isConfirmed) {
              _this14.switchToRoleKeys();

              Object(_application_updateUserRoles__WEBPACK_IMPORTED_MODULE_3__["updateUserRoles"])(_this14.user.id, _this14.updatedRoles, _this14._service).subscribe(function (user) {
                Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["successAlert"])('User roles were correctly updated');
                _this14.user = user;
                _this14.data.user.fullRoles = _this14.data.roles.filter(function (rol) {
                  return user.roles.includes(rol.id);
                });
                _this14.updatedRoles = _this14.data.user.fullRoles.map(function (rol) {
                  return rol.value;
                });
                _this14.roles = _this14.selected === 'all' ? _this14.roles : _this14.data.user.fullRoles;
                _this14.userRolesId = _this14.user.roles;

                if (_this14.selected === 'user') {
                  _this14.filtro.clear();
                }
              });
            }
          });
        }
      }, {
        key: "create",
        value: function create() {
          var _this15 = this;

          var regEx = new RegExp('^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{8,16}$');
          this.strongPassword = !regEx.test(this.form.get('password').value);

          if (this.strongPassword) {
            return;
          }

          var registerUser = this._userRegisterMapper.mapTo(this.form.value);

          Object(src_app_auth_user_application_registerNewUser__WEBPACK_IMPORTED_MODULE_8__["registerNewUser"])(registerUser, this._service).subscribe(function (user) {
            Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["successAlert"])('User roles were correctly updated');
            _this15.user = user;
            _this15.data.user = user;
            _this15.data.user.fullRoles = [_this15.data.roles.find(function (rol) {
              return rol.value === 'ROLE_USER';
            })];
            _this15.updatedRoles = [_this15.data.roles.find(function (rol) {
              return rol.value === 'ROLE_USER';
            }).value];
            _this15.userRolesId = [_this15.data.roles.find(function (rol) {
              return rol.value === 'ROLE_USER';
            }).id];

            if (_this15.selected === 'user') {
              _this15.filtro.clear();
            }

            _this15.initForm();
          });
        }
      }, {
        key: "switchToRoleKeys",
        value: function switchToRoleKeys() {
          var _this16 = this;

          var roles = this.data.roles.filter(function (role) {
            return _this16.updatedRoles.includes(role.value);
          });
          this.updatedRoles = roles.map(function (role) {
            return role.id;
          });
        }
      }, {
        key: "toggleChange",
        value: function toggleChange() {
          this.user.locked ? this.showUnlockDialog() : this.showLockDialog();
        }
      }, {
        key: "showLockDialog",
        value: function showLockDialog() {
          var _this17 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: "".concat(this.user.email, " will be locked"),
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080'
          }).then(function (result) {
            result.isConfirmed ? _this17.lock() : _this17.toggle.checked = false;
          });
        }
      }, {
        key: "lock",
        value: function lock() {
          var _this18 = this;

          Object(_application_lock__WEBPACK_IMPORTED_MODULE_5__["lock"])(this.user.id, this._service).subscribe(function () {
            Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["successAlert"])('Success').then(function () {
              _this18.user.locked = true;
              _this18.toggle.checked = true;
            });
          });
        }
      }, {
        key: "showUnlockDialog",
        value: function showUnlockDialog() {
          var _this19 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: "".concat(this.user.email, " will be unlocked"),
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080'
          }).then(function (result) {
            result.isConfirmed ? _this19.unlock() : _this19.toggle.checked = true;
          });
        }
      }, {
        key: "unlock",
        value: function unlock() {
          var _this20 = this;

          Object(_application_unlock__WEBPACK_IMPORTED_MODULE_6__["unlock"])(this.user.id, this._service).subscribe(function () {
            Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_4__["successAlert"])('Success').then(function () {
              _this20.user.locked = false;
              _this20.toggle.checked = false;
            });
          });
        }
      }, {
        key: "change",
        value: function change(event) {
          this.roles = event.value === 'all' ? this.roles = this.data.roles : this.data.user.fullRoles;
          this.selected = event.value;
          this.rolesName = this.roles.map(function (rol) {
            return rol.name;
          });
          this.filtro.clear();
        }
      }, {
        key: "addPersonalRol",
        value: function addPersonalRol() {
          var _this21 = this;

          Object(src_app_auth_user_application_addPersonalRol__WEBPACK_IMPORTED_MODULE_7__["addPersonalUserRol"])(this.user.id, this._service).subscribe(function (res) {
            var id = res.roles.filter(function (x) {
              return !_this21.userRolesId.includes(x);
            });

            _this21._router.navigate(["".concat(_this21.route, "/").concat(id)], {
              relativeTo: _this21._route
            });
          });
        }
      }, {
        key: "autocompleteValue",
        value: function autocompleteValue(value) {
          this.roles = value ? this.roles.filter(function (rol) {
            return rol.name === value;
          }) : this.selected === 'all' ? this.roles = this.data.roles : this.data.user.fullRoles;
        }
      }, {
        key: "deleteRole",
        value: function deleteRole(roleValue) {
          var index = this.updatedRoles.indexOf(roleValue);

          if (index >= 0) {
            this.updatedRoles.splice(index, 1);
          }

          this.applyChanges();
        }
      }, {
        key: "viewRole",
        value: function viewRole(role) {
          this._router.navigate(["".concat(this.route).concat(role.id)], {
            relativeTo: this._route
          });
        }
      }]);

      return UserDetailComponent;
    }();

    UserDetailComponent.ɵfac = function UserDetailComponent_Factory(t) {
      return new (t || UserDetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_user_service__WEBPACK_IMPORTED_MODULE_11__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_10__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_auth_user_infrastructure_userRegister_mapper__WEBPACK_IMPORTED_MODULE_12__["UserRegisterMapper"]));
    };

    UserDetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: UserDetailComponent,
      selectors: [["app-user-detail"]],
      viewQuery: function UserDetailComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.toggle = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.filtro = _t.first);
        }
      },
      decls: 25,
      vars: 9,
      consts: [[1, "cards"], ["class", "container", 4, "ngIf"], [1, "role-list"], [1, "list-header"], [3, "value", "valueChange", "selectionChange"], ["value", "user", 4, "ngIf"], ["value", "all"], [1, "filter", 3, "placeholder", "matLabel", "appearance", "options", "selectedValue"], ["filtro", ""], ["mat-flat-button", "", 3, "click", 4, "ngIf"], [1, "scroll"], [1, "list-content"], [4, "ngFor", "ngForOf"], [1, "buttons"], ["mat-button", "", 1, "apply-button", 3, "click"], [1, "container"], [1, "content"], [1, "user-info"], [3, "formGroup"], [1, "columns"], ["appearance", "outline", 4, "ngIf"], ["appearance", "outline"], ["aria-label", "email", "matInput", "", "placeholder", "Email", "formControlName", "email", 3, "keyup"], ["aria-label", "username", "matInput", "", "placeholder", "UserName", "formControlName", "username"], ["class", "check", 4, "ngIf"], ["class", "username", 4, "ngIf"], ["aria-label", "id", "matInput", "", "placeholder", "Id", "formControlName", "id"], [1, "check"], ["matInput", "", "aria-label", "password", "matInput", "", "placeholder", "password", "formControlName", "password", 3, "type"], ["mat-icon-button", "", "matSuffix", "", 3, "click"], ["matInput", "", "aria-label", "cpassword", "matInput", "", "placeholder", "confirm your password", "formControlName", "cpassword", 3, "type"], [1, "username"], [3, "checked", "change"], ["toggle", ""], ["value", "user"], ["mat-flat-button", "", 3, "click"], [1, "role-list-item"], [3, "disabled", "checked", "change"], [1, "role_detail"], [3, "click"], [3, "click", 4, "ngIf"]],
      template: function UserDetailComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, UserDetailComponent_mat_card_content_2_Template, 24, 8, "mat-card-content", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-card");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "ASSIGNED ROLES");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-form-field");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "ROLES OPTION");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-select", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("valueChange", function UserDetailComponent_Template_mat_select_valueChange_12_listener($event) {
            return ctx.selected = $event;
          })("selectionChange", function UserDetailComponent_Template_mat_select_selectionChange_12_listener($event) {
            return ctx.change($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, UserDetailComponent_mat_option_13_Template, 2, 0, "mat-option", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-option", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "ALL ROLES");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "app-autocomplete", 7, 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("selectedValue", function UserDetailComponent_Template_app_autocomplete_selectedValue_16_listener($event) {
            return ctx.autocompleteValue($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, UserDetailComponent_button_18_Template, 4, 0, "button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "mat-list", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, UserDetailComponent_div_21_Template, 8, 4, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UserDetailComponent_Template_button_click_23_listener() {
            return ctx.onSaveClick();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Apply");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.selected);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.user);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", "Name")("matLabel", "ROL NAME")("appearance", "standard")("options", ctx.rolesName);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.user);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.roles);
        }
      },
      directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_13__["MatCard"], _angular_common__WEBPACK_IMPORTED_MODULE_14__["NgIf"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_15__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_15__["MatLabel"], _angular_material_select__WEBPACK_IMPORTED_MODULE_16__["MatSelect"], _angular_material_core__WEBPACK_IMPORTED_MODULE_17__["MatOption"], _shared_custom_mat_elements_autocomplete_autocomplete_component__WEBPACK_IMPORTED_MODULE_18__["AutocompleteComponent"], _angular_material_list__WEBPACK_IMPORTED_MODULE_19__["MatList"], _angular_common__WEBPACK_IMPORTED_MODULE_14__["NgForOf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_20__["MatButton"], _angular_material_card__WEBPACK_IMPORTED_MODULE_13__["MatCardContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_input__WEBPACK_IMPORTED_MODULE_21__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_22__["MatIcon"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_15__["MatSuffix"], _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_23__["MatSlideToggle"], _angular_material_list__WEBPACK_IMPORTED_MODULE_19__["MatListItem"], _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_24__["MatCheckbox"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n[_nghost-%COMP%]     mat-card {\n  max-height: 588px;\n  margin: 3rem;\n  width: 50%;\n}\n.cards[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  margin: 3em 3em 1em;\n}\n.container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: column;\n  justify-content: space-around;\n}\n.content[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  justify-content: space-evenly;\n  align-items: center;\n}\n.content[_ngcontent-%COMP%]   .user-info[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: column;\n  align-items: flex-start;\n  margin: 7.5rem;\n}\n.content[_ngcontent-%COMP%]   .user-info[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  text-align: center;\n}\np[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.role-list[_ngcontent-%COMP%] {\n  margin: 0.5em 0.5em 1rem;\n}\n.role-list[_ngcontent-%COMP%]   mat-list[_ngcontent-%COMP%] {\n  height: 25vw;\n  margin: 0.5em;\n}\n.role-list[_ngcontent-%COMP%]   mat-list[_ngcontent-%COMP%]   mat-list-item[_ngcontent-%COMP%]:hover {\n  cursor: pointer;\n  background: grey;\n  color: white;\n}\n.role-list[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n  color: #DE7373;\n}\n.scroll[_ngcontent-%COMP%] {\n  overflow-y: auto;\n}\n.scroll[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 6px;\n  height: 6px;\n}\n.scroll[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 4px;\n}\n[_nghost-%COMP%]     .mat-list-item-content {\n  justify-content: space-between;\n}\n.role_detail[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n  min-width: 3em;\n}\n.list-header[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  font-size: 0.8rem;\n  align-items: baseline;\n  margin-left: 1em;\n}\n.list-header[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%] {\n  max-width: 11em;\n  margin-right: 1em;\n}\n.list-header[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  color: #efefef;\n  margin-left: 1em;\n}\n.list-header[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n  color: #efefef;\n}\n.list-header[_ngcontent-%COMP%]   app-autocomplete[_ngcontent-%COMP%] {\n  max-width: 11em;\n}\n.buttons[_ngcontent-%COMP%] {\n  text-align: center;\n}\n.buttons[_ngcontent-%COMP%]   .apply-button[_ngcontent-%COMP%] {\n  color: #fff;\n  background-color: #e46e75;\n  border-radius: 24px;\n  width: 200px;\n}\n.buttons[_ngcontent-%COMP%]   .apply-button[_ngcontent-%COMP%]:hover {\n  background-color: #eb4e4e;\n  cursor: pointer;\n}\n.check[_ngcontent-%COMP%] {\n  font-size: 0.8rem;\n  color: red;\n  display: flex;\n  align-items: center;\n  margin: -1.5em 0 0.5em;\n}\n[_nghost-%COMP%]     .mat-checkbox-checked.mat-accent .mat-checkbox-background, .mat-checkbox-indeterminate.mat-accent[_ngcontent-%COMP%]   .mat-checkbox-background[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC91c2VyL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvdXNlci1kZXRhaWwvQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFzc2V0c1xcc3R5bGVzXFxzdGFmZml0LXN0eWxlLTEuc2NzcyIsInNyYy9hcHAvYXV0aC91c2VyL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvdXNlci1kZXRhaWwvdXNlci1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2F1dGgvdXNlci9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3VzZXItZGV0YWlsL0M6XFx1bmFpd29ya3NwYWNlXFxzdGFmZml0LWZyb250ZW5kLWFuZ3VsYXIvc3JjXFxhcHBcXGF1dGhcXHVzZXJcXGluZnJhc3RydWN0dXJlXFxuZy1jb21wb25lbnRzXFx1c2VyLWRldGFpbFxcdXNlci1kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0NBQUE7QUFRQSx3QkFBQTtBQVFBLHVCQUFBO0FBR0EsMkJBQUE7QUFFQTtFQUNFLDZCQUFBO0FDaEJGO0FEa0JFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUNoQko7QURrQkk7RUFDRSxtQkEzQlE7RUE0QlIsZ0JBQUE7RUFDQSxjQUFBO0FDaEJOO0FEc0JBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDbkJGO0FEcUJFO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNuQko7QURxQkk7RUFDRSxZQUFBO0FDbkJOO0FEc0JFO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0FDcEJKO0FEd0JBO0VBQ0UsNkJBQUE7RUFDQSxlQUFBO0FDckJGO0FEdUJFO0VBQ0UsU0FBQTtBQ3JCSjtBRHdCRTtFQUNFLGdDQUFBO0FDdEJKO0FEeUJFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUN2Qko7QUR5Qkk7RUFDRSxtQkEzRVE7RUE0RVIsZ0JBQUE7RUFDQSxjQUFBO0FDdkJOO0FEMEJJO0VBQ0UseUJBNUVpQjtFQTZFakIsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ3hCTjtBRDZCQTtFQUNFLGNBbEZtQjtBQ3dEckI7QUQ2QkE7RUFDRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxXQUFBO0FDMUJGO0FENkJBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDMUJGO0FENkJBO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQzFCRjtBRDRCRTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQzFCSjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZUFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0FDM0JGO0FEa0NBO0VBQ0UsY0FBQTtBQy9CRjtBRGtDQSx5QkFBQTtBQUVBO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtREFBQTtFQUNBLGdCQUFBO0VBQ0EsMEJBQUE7RUFDQSx5QkFBQTtBQ2hDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSxjQUFBO0FDbENGO0FEcUNBLFlBQUE7QUFFQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsdUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxlQUFBO0FDbkNGO0FEc0NBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSwwQ0FBQTtBQ25DRjtBQ2xNQTtFQUNFLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QURxTUY7QUNsTUE7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBRHFNRjtBQ2xNQTtFQUNFLGFBQUE7RUFDQSxpQkFBQTtFQUNBLDZCQUFBO0FEcU1GO0FDbE1BO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0FEcU1GO0FDbE1BO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxjQUFBO0FEcU1GO0FDcE1JO0VBQ0Usa0JBQUE7QURzTU47QUNsTUE7RUFDRSxnQkFBQTtBRHFNRjtBQ2xNQTtFQUNFLHdCQUFBO0FEcU1GO0FDbk1FO0VBQ0UsWUFBQTtFQUNBLGFBQUE7QURxTUo7QUNuTUk7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FEcU1OO0FDak1FO0VBQ0UsY0YvQ21CO0FDa1B2QjtBQy9MQTtFQUNFLGdCQUFBO0FEa01GO0FDL0xBO0VBQ0UsVUFBQTtFQUNBLFdBQUE7QURrTUY7QUMvTEE7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0FEa01GO0FDL0xBO0VBQ0UsOEJBQUE7QURrTUY7QUMvTEE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FEa01GO0FDL0xBO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsaUJGdEZNO0VFdUZOLHFCQUFBO0VBQ0EsZ0JBQUE7QURrTUY7QUNoTUU7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURrTUo7QUMvTEU7RUFDRSx5QkZ4Rm1CO0VFeUZuQixtQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBRGlNSjtBQy9MRTtFQUNFLGNBQUE7QURpTUo7QUMvTEU7RUFDRSxlQUFBO0FEaU1KO0FDN0xBO0VBQ0Usa0JBQUE7QURnTUY7QUM5TEU7RUFDRSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QURnTUo7QUM5TEk7RUFDRSx5QkFBQTtFQUNBLGVBQUE7QURnTU47QUMzTEE7RUFDRSxpQkY5SE07RUUrSE4sVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0FEOExGO0FDM0xBO0VBQ0UseUJGOUhxQjtBQzRUdkIiLCJmaWxlIjoic3JjL2FwcC9hdXRoL3VzZXIvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy91c2VyLWRldGFpbC91c2VyLWRldGFpbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cclxuJHNtYWxsOiAwLjhyZW07XHJcbiRtZWRpdW06IDFyZW07XHJcbiRsYXJnZTogMS4yNXJlbTtcclxuJGV4dHJhLWxhcmdlOiAxLjU2M3JlbTtcclxuJGh1Z2U6IDEuOTUzcmVtO1xyXG4kZXh0cmEtaHVnZTogMi40NDFyZW07XHJcblxyXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXHJcbiRjb2xvci1wYWxldHRlLTEtbWFpbjogI0RFNzM3MztcclxuJGNvbG9yLXBhbGV0dGUtMS1zZWNvbmQ6ICNBQkFCRkY7XHJcbiRjb2xvci1wcmltYXJ5LWxpZ2h0OiAjRUZFRkVGO1xyXG4kY29sb3ItcHJpbWFyeS1kYXJrOiAjMTkxOTE5O1xyXG4kY29sb3Itc2Vjb25kYXJ5LWRhcms6ICMzMzMzMzM7XHJcbiRjb2xvci1zY3JvbGxiYXItdGh1bWI6ICM4QzhDOEM7XHJcblxyXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cclxuXHJcblxyXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXHJcblxyXG4uYy1jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcblxyXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgLmR5bi1maWx0ZXJzIHtcclxuICAgIGZsZXgtZ3JvdzogMTtcclxuICAgIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgICBtYXJnaW46IDAgNDBweCAwIDA7XHJcblxyXG4gICAgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcclxuICAgICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29uZmlnQnV0dG9uIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcbiAgbWF4LWhlaWdodDogODglO1xyXG5cclxuICAuY2FyZC1jb250ZW50LCAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1wYWxldHRlLTEtbWFpbjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMnJlbTtcclxuICAgICAgaGVpZ2h0OiAyLjNyZW07XHJcbiAgICAgIGNvbG9yOiAjZWZlZmVmO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcclxuICBjb2xvcjogJGNvbG9yLXByaW1hcnktZGFyaztcclxufVxyXG5cclxuLmZpbHRlciB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMC41ZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XHJcbiAgbWF4LXdpZHRoOiAxMGVtO1xyXG4gIG1hcmdpbi1yaWdodDogNWVtO1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuICAvLyBtYXgtaGVpZ2h0OiA4MDBweDtcclxuICAubWF0LXRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgfVxyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xyXG4gIGNvbG9yOiAjOGI4YjhiO1xyXG59XHJcblxyXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbn1cclxuXHJcbi5pZEhlYWRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWRDZWxsIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbm1hdC1jYXJkLWNvbnRlbnQge1xyXG4gIG1hcmdpbjogMCAzNHB4O1xyXG59XHJcblxyXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xyXG5cclxuLnByaW9yaXR5IHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi8vIEBUT0RPOiBjaGFuZ2UgY29sb3IgdG8gdmFyaWFibGVzXHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcclxufVxyXG5cclxuLnByaW9yaXR5LWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1ub3JtYWwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1iYWphIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbmluZ3VuYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLnJlZC1kYXRlIHtcclxuICBjb2xvcjogI2ViNDE0MTtcclxufVxyXG5cclxuLyogTUFUIFRBQiAqL1xyXG5cclxuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xyXG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAuMTIpO1xyXG59XHJcbiIsIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xuLmMtY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMge1xuICBmbGV4LWdyb3c6IDE7XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTUwcHg7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuY29uZmlnQnV0dG9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZmxleC1zaHJpbms6IDA7XG59XG5cbi5jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xuICBtYXgtaGVpZ2h0OiA4OCU7XG59XG4uY2FyZC1jb21wb25lbnQgLmNhcmQtY29udGVudCwgLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBtYXJnaW46IDA7XG59XG4uY2FyZC1jb21wb25lbnQgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBidXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xuICBib3JkZXItcmFkaXVzOiAycmVtO1xuICBoZWlnaHQ6IDIuM3JlbTtcbiAgY29sb3I6ICNlZmVmZWY7XG59XG5cbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XG4gIGNvbG9yOiAjMTkxOTE5O1xufVxuXG4uZmlsdGVyIHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cblxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XG4gIG1heC13aWR0aDogMTBlbTtcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XG59XG5cbi50YWJsZS1yZXNwb25zaXZlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy14OiBhdXRvO1xufVxuLnRhYmxlLXJlc3BvbnNpdmUgLm1hdC10YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLm1hdC1oZWFkZXItcm93IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcbiAgY29sb3I6ICM4YjhiOGI7XG59XG5cbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG5cbi5pZEhlYWRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pZENlbGwge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxubWF0LWNhcmQtY29udGVudCB7XG4gIG1hcmdpbjogMCAzNHB4O1xufVxuXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xuLnByaW9yaXR5IHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucHJpb3JpdHktbXV5YWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XG59XG5cbi5wcmlvcml0eS1hbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcbn1cblxuLnByaW9yaXR5LW5vcm1hbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XG59XG5cbi5wcmlvcml0eS1iYWphIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcbn1cblxuLnByaW9yaXR5LW5pbmd1bmEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucmVkLWRhdGUge1xuICBjb2xvcjogI2ViNDE0MTtcbn1cblxuLyogTUFUIFRBQiAqL1xuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufVxuXG46aG9zdCA6Om5nLWRlZXAgbWF0LWNhcmQge1xuICBtYXgtaGVpZ2h0OiA1ODhweDtcbiAgbWFyZ2luOiAzcmVtO1xuICB3aWR0aDogNTAlO1xufVxuXG4uY2FyZHMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luOiAzZW0gM2VtIDFlbTtcbn1cblxuLmNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcbn1cblxuLmNvbnRlbnQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5jb250ZW50IC51c2VyLWluZm8ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IGNvbHVtbjtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gIG1hcmdpbjogNy41cmVtO1xufVxuLmNvbnRlbnQgLnVzZXItaW5mbyBoMiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxucCB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5yb2xlLWxpc3Qge1xuICBtYXJnaW46IDAuNWVtIDAuNWVtIDFyZW07XG59XG4ucm9sZS1saXN0IG1hdC1saXN0IHtcbiAgaGVpZ2h0OiAyNXZ3O1xuICBtYXJnaW46IDAuNWVtO1xufVxuLnJvbGUtbGlzdCBtYXQtbGlzdCBtYXQtbGlzdC1pdGVtOmhvdmVyIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBiYWNrZ3JvdW5kOiBncmV5O1xuICBjb2xvcjogd2hpdGU7XG59XG4ucm9sZS1saXN0IG1hdC1pY29uIHtcbiAgY29sb3I6ICNERTczNzM7XG59XG5cbi5zY3JvbGwge1xuICBvdmVyZmxvdy15OiBhdXRvO1xufVxuXG4uc2Nyb2xsOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiA2cHg7XG4gIGhlaWdodDogNnB4O1xufVxuXG4uc2Nyb2xsOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtbGlzdC1pdGVtLWNvbnRlbnQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG5cbi5yb2xlX2RldGFpbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1pbi13aWR0aDogM2VtO1xufVxuXG4ubGlzdC1oZWFkZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGZvbnQtc2l6ZTogMC44cmVtO1xuICBhbGlnbi1pdGVtczogYmFzZWxpbmU7XG4gIG1hcmdpbi1sZWZ0OiAxZW07XG59XG4ubGlzdC1oZWFkZXIgLm1hdC1mb3JtLWZpZWxkIHtcbiAgbWF4LXdpZHRoOiAxMWVtO1xuICBtYXJnaW4tcmlnaHQ6IDFlbTtcbn1cbi5saXN0LWhlYWRlciBidXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xuICBib3JkZXItcmFkaXVzOiAycmVtO1xuICBjb2xvcjogI2VmZWZlZjtcbiAgbWFyZ2luLWxlZnQ6IDFlbTtcbn1cbi5saXN0LWhlYWRlciBtYXQtaWNvbiB7XG4gIGNvbG9yOiAjZWZlZmVmO1xufVxuLmxpc3QtaGVhZGVyIGFwcC1hdXRvY29tcGxldGUge1xuICBtYXgtd2lkdGg6IDExZW07XG59XG5cbi5idXR0b25zIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJ1dHRvbnMgLmFwcGx5LWJ1dHRvbiB7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTQ2ZTc1O1xuICBib3JkZXItcmFkaXVzOiAyNHB4O1xuICB3aWR0aDogMjAwcHg7XG59XG4uYnV0dG9ucyAuYXBwbHktYnV0dG9uOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ViNGU0ZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uY2hlY2sge1xuICBmb250LXNpemU6IDAuOHJlbTtcbiAgY29sb3I6IHJlZDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luOiAtMS41ZW0gMCAwLjVlbTtcbn1cblxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtY2hlY2tib3gtY2hlY2tlZC5tYXQtYWNjZW50IC5tYXQtY2hlY2tib3gtYmFja2dyb3VuZCwgLm1hdC1jaGVja2JveC1pbmRldGVybWluYXRlLm1hdC1hY2NlbnQgLm1hdC1jaGVja2JveC1iYWNrZ3JvdW5kIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0RFNzM3Mztcbn0iLCJAaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi8uLi9hc3NldHMvc3R5bGVzL3N0YWZmaXQtc3R5bGUtMSc7XHJcblxyXG46aG9zdCA6Om5nLWRlZXAgbWF0LWNhcmR7XHJcbiAgbWF4LWhlaWdodDogNTg4cHg7XHJcbiAgbWFyZ2luOiAzcmVtO1xyXG4gIHdpZHRoOiA1MCU7XHJcbn1cclxuXHJcbi5jYXJkcyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW46IDNlbSAzZW0gMWVtO1xyXG59XHJcblxyXG4uY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG59XHJcblxyXG4uY29udGVudCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uY29udGVudCAudXNlci1pbmZvIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZmxvdzogY29sdW1uO1xyXG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gIG1hcmdpbjogNy41cmVtO1xyXG4gICAgaDIge1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbn1cclxuXHJcbnAge1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuXHJcbi5yb2xlLWxpc3Qge1xyXG4gIG1hcmdpbjogMC41ZW0gMC41ZW0gMXJlbTtcclxuXHJcbiAgbWF0LWxpc3Qge1xyXG4gICAgaGVpZ2h0OiAyNXZ3O1xyXG4gICAgbWFyZ2luOiAwLjVlbTtcclxuXHJcbiAgICBtYXQtbGlzdC1pdGVtOmhvdmVyIHtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICBiYWNrZ3JvdW5kOiBncmV5O1xyXG4gICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBtYXQtaWNvbiB7XHJcbiAgICBjb2xvcjogJGNvbG9yLXBhbGV0dGUtMS1tYWluO1xyXG4gIH1cclxufVxyXG5cclxuLnNjcm9sbCB7XHJcbiAgb3ZlcmZsb3cteTogYXV0bztcclxufVxyXG5cclxuLnNjcm9sbDo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiA2cHg7XHJcbiAgaGVpZ2h0OiA2cHg7XHJcbn1cclxuXHJcbi5zY3JvbGw6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDRweDtcclxufVxyXG5cclxuOmhvc3QgOjpuZy1kZWVwIC5tYXQtbGlzdC1pdGVtLWNvbnRlbnR7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcblxyXG4ucm9sZV9kZXRhaWx7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIG1pbi13aWR0aDogM2VtO1xyXG59XHJcblxyXG4ubGlzdC1oZWFkZXJ7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgZm9udC1zaXplOiAkc21hbGw7XHJcbiAgYWxpZ24taXRlbXM6IGJhc2VsaW5lO1xyXG4gIG1hcmdpbi1sZWZ0OiAxZW07XHJcblxyXG4gIC5tYXQtZm9ybS1maWVsZHtcclxuICAgIG1heC13aWR0aDogMTFlbTtcclxuICAgIG1hcmdpbi1yaWdodDogMWVtO1xyXG4gIH1cclxuXHJcbiAgYnV0dG9uIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1wYWxldHRlLTEtbWFpbjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDJyZW07XHJcbiAgICBjb2xvcjogI2VmZWZlZjtcclxuICAgIG1hcmdpbi1sZWZ0OiAxZW07XHJcbiAgfVxyXG4gIG1hdC1pY29ue1xyXG4gICAgY29sb3I6ICNlZmVmZWY7XHJcbiAgfVxyXG4gIGFwcC1hdXRvY29tcGxldGV7XHJcbiAgICBtYXgtd2lkdGg6IDExZW07XHJcbiAgfVxyXG59XHJcblxyXG4uYnV0dG9ucyB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAuYXBwbHktYnV0dG9uIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2U0NmU3NTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICB3aWR0aDogMjAwcHg7XHJcblxyXG4gICAgJjpob3ZlciB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlYjRlNGU7XHJcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5jaGVja3tcclxuICBmb250LXNpemU6JHNtYWxsO1xyXG4gIGNvbG9yOnJlZDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgbWFyZ2luOi0xLjVlbSAwIDAuNWVtO1xyXG59XHJcblxyXG46aG9zdCA6Om5nLWRlZXAgLm1hdC1jaGVja2JveC1jaGVja2VkLm1hdC1hY2NlbnQgLm1hdC1jaGVja2JveC1iYWNrZ3JvdW5kLC5tYXQtY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS5tYXQtYWNjZW50IC5tYXQtY2hlY2tib3gtYmFja2dyb3VuZCB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJGNvbG9yLXBhbGV0dGUtMS1tYWluO1xyXG59XHJcbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UserDetailComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-user-detail',
          templateUrl: './user-detail.component.html',
          styleUrls: ['./user-detail.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"]
        }, {
          type: _services_user_service__WEBPACK_IMPORTED_MODULE_11__["UserService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_10__["ActivatedRoute"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
        }, {
          type: src_app_auth_user_infrastructure_userRegister_mapper__WEBPACK_IMPORTED_MODULE_12__["UserRegisterMapper"]
        }];
      }, {
        toggle: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['toggle']
        }],
        filtro: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['filtro']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/auth/user/infrastructure/ng-components/user-table/config.ts":
  /*!*****************************************************************************!*\
    !*** ./src/app/auth/user/infrastructure/ng-components/user-table/config.ts ***!
    \*****************************************************************************/

  /*! exports provided: table, filters */

  /***/
  function srcAppAuthUserInfrastructureNgComponentsUserTableConfigTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "table", function () {
      return table;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "filters", function () {
      return filters;
    });

    var table = {
      multiSelect: false,
      columns: [{
        name: 'Id',
        prop: 'id',
        shown: true,
        route: './app/systems-administrator/users/user-detail/',
        cellClass: 'id-table-column',
        routeId: 'id'
      }, {
        name: 'Username',
        prop: 'username',
        shown: true
      }, {
        name: 'Email',
        prop: 'email',
        shown: true
      }, {
        name: 'Status',
        prop: 'lockedValue',
        shown: true
      }]
    };
    var filters = [{
      options: [],
      prop: 'id',
      retProp: '',
      type: 'input',
      appearance: 'outline',
      "class": 'input',
      label: 'Id',
      placeholder: 'Id',
      shown: true
    }, {
      options: [],
      prop: 'username',
      retProp: '',
      type: 'input',
      appearance: 'outline',
      "class": 'input',
      label: 'Username',
      placeholder: 'Username',
      shown: true
    }, {
      options: [],
      prop: 'email',
      retProp: '',
      type: 'input',
      appearance: 'outline',
      "class": 'input',
      label: 'Email',
      placeholder: 'Email',
      shown: true
    }];
    /***/
  },

  /***/
  "./src/app/auth/user/infrastructure/ng-components/user-table/user-table.component.ts":
  /*!*******************************************************************************************!*\
    !*** ./src/app/auth/user/infrastructure/ng-components/user-table/user-table.component.ts ***!
    \*******************************************************************************************/

  /*! exports provided: UserTableComponent */

  /***/
  function srcAppAuthUserInfrastructureNgComponentsUserTableUserTableComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserTableComponent", function () {
      return UserTableComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic */
    "./src/app/shared/custom-mat-elements/dynamic.ts");
    /* harmony import */


    var _config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./config */
    "./src/app/auth/user/infrastructure/ng-components/user-table/config.ts");
    /* harmony import */


    var _application_getUsers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../application/getUsers */
    "./src/app/auth/user/application/getUsers.ts");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */


    var _application_lock__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../application/lock */
    "./src/app/auth/user/application/lock.ts");
    /* harmony import */


    var _application_unlock__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../application/unlock */
    "./src/app/auth/user/application/unlock.ts");
    /* harmony import */


    var _shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../../../shared/error/custom-alerts */
    "./src/app/shared/error/custom-alerts.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _services_user_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../services/user.service */
    "./src/app/auth/user/infrastructure/services/user.service.ts");
    /* harmony import */


    var src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! src/app/auth/session/infrastructure/services/cookie.service */
    "./src/app/auth/session/infrastructure/services/cookie.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component */
    "./src/app/shared/custom-mat-elements/dynamic-table/dynamic-table-config-panel/dynamic-table-config-panel.component.ts");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_material_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/material/button */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
    /* harmony import */


    var _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/material/icon */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-filters/dynamic-filters.component */
    "./src/app/shared/custom-mat-elements/dynamic-filters/dynamic-filters.component.ts");
    /* harmony import */


    var _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ../../../../../shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component */
    "./src/app/shared/custom-mat-elements/dynamic-table-mark-ii/dynamic-table-mark-ii.component.ts");

    var _c0 = ["table"];

    function UserTableComponent_div_0_Template(rf, ctx) {
      if (rf & 1) {
        var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h2");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "USERS");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "button", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "add");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " New ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "app-dynamic-filters", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("query", function UserTableComponent_div_0_Template_app_dynamic_filters_query_10_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r2.filtersChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UserTableComponent_div_0_Template_button_click_11_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r4.setConfigVisibility(!ctx_r4.configVisibility);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-icon");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " Settings");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "app-dynamic-table-mark-ii", 8, 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("pagination", function UserTableComponent_div_0_Template_app_dynamic_table_mark_ii_pagination_15_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r5.paginationChanged($event);
        })("order", function UserTableComponent_div_0_Template_app_dynamic_table_mark_ii_order_15_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r6.orderChanged($event);
        })("action", function UserTableComponent_div_0_Template_app_dynamic_table_mark_ii_action_15_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r7.doActions($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("filters", ctx_r0.filters)("showFilters", true);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("config", ctx_r0.tableConfig)("data", ctx_r0.tableData);
      }
    }

    var UserTableComponent = /*#__PURE__*/function () {
      function UserTableComponent(_router, _route, _service, _cookieFacade) {
        _classCallCheck(this, UserTableComponent);

        this._router = _router;
        this._route = _route;
        this._service = _service;
        this._cookieFacade = _cookieFacade;
        this.tableConfig = _config__WEBPACK_IMPORTED_MODULE_2__["table"];
        this.filters = _config__WEBPACK_IMPORTED_MODULE_2__["filters"];
        this.queryPagination = {
          page: 0,
          size: 10
        };
        this.queryOrder = {
          orderField: '',
          order: ''
        };
        this.queryFilters = {};
        this.loading = true;
        this.configVisibility = false;
      }

      _createClass(UserTableComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _a, _b;

          var _this$_route$snapshot2 = this._route.snapshot.data['response'],
              data = _this$_route$snapshot2.data,
              filtersValue = _this$_route$snapshot2.filtersValue,
              tableFiltersConfig = _this$_route$snapshot2.tableFiltersConfig;

          if (data) {
            this.tableData = {
              content: data.content.map(function (i) {
                return {
                  id: i.id,
                  username: i.username,
                  email: i.email,
                  lockedValue: i.locked ? 'Locked' : 'Unlocked'
                };
              }),
              numberOfElements: data.numberOfElements,
              totalElements: data.totalElements,
              totalPages: data.totalPages
            };
            this.filtersValue = filtersValue;
            Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["parseTableFiltersConfig"])(tableFiltersConfig, this.tableConfig, this.filters);
            this.queryFilters = data.filtersValue ? this._clean(JSON.parse(data.filtersValue)) : {};
            this.tableConfig.pagination = (_a = tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.pagination) !== null && _a !== void 0 ? _a : {
              page: 0,
              size: 10
            };
            this.queryPagination = this.tableConfig.pagination;
            this.tableConfig.order = (_b = tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.order) !== null && _b !== void 0 ? _b : {
              orderField: '',
              order: ''
            };
            this.queryOrder = Object.assign({}, this.tableConfig.order);
            this.tableConfig.order.orderField = this.tableConfig.order.orderField ? Object.entries(_defineProperty({}, this.tableConfig.order.orderField, '')).find(function (entry) {
              return entry[1] === '';
            })[0] : '';
            this.loading = false;
          }

          this.configFilterValue();
        }
      }, {
        key: "configFilterValue",
        value: function configFilterValue() {
          var _this22 = this;

          var queryFiltersAux = this.filtersValue ? this._clean(JSON.parse(this.filtersValue)) : {};
          this.filters.forEach(function (filter) {
            filter.defaultValue = queryFiltersAux[_this22.getFilterProp(filter)];
            filter.value = queryFiltersAux[_this22.getFilterProp(filter)];
          });
        }
      }, {
        key: "getFilterProp",
        value: function getFilterProp(filter) {
          return filter.prop ? filter.type === 'autoselect' ? filter.searchValue : filter.prop : filter.searchValue;
        }
      }, {
        key: "_clean",
        value: function _clean(object) {
          var cleaned = {};
          var keys = Object.keys(object);
          keys.forEach(function (key) {
            if (object[key]) {
              cleaned[key] = object[key];
            }
          });
          return cleaned;
        }
      }, {
        key: "filtersChanged",
        value: function filtersChanged($event) {
          this.queryFilters = $event && Object.keys($event).length !== 0 ? $event : {};

          this._cookieFacade.save('userFilterCache', JSON.stringify(this.queryFilters));

          this.queryPagination = {
            page: 0,
            size: this.queryPagination.size
          };

          this._cookieFacade.save('user-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.dynamicTable.resetPageIndex();
          this.backSearch();
        }
      }, {
        key: "paginationChanged",
        value: function paginationChanged($event) {
          this.queryPagination.page = $event.page;
          this.queryPagination.size = $event.size;

          this._cookieFacade.save('user-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "orderChanged",
        value: function orderChanged($event) {
          var aux = _defineProperty({}, $event.orderField, '');

          this.queryOrder.orderField = Object.entries(aux).find(function (entry) {
            return entry[1] === '';
          })[0];
          this.queryOrder.order = $event.order;
          this.queryPagination.page = $event.page;
          this.queryPagination.size = $event.size;

          this._cookieFacade.save('user-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));

          this.backSearch();
        }
      }, {
        key: "doActions",
        value: function doActions(event) {
          if (event.action === 'Lock') {
            event.item.locked ? this.unlock(event.item) : this.lock(event.item);
          }
        }
      }, {
        key: "lock",
        value: function lock(user) {
          var _this23 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: "".concat(user.email, " will be locked"),
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080'
          }).then(function (result) {
            if (result.isConfirmed) {
              Object(_application_lock__WEBPACK_IMPORTED_MODULE_5__["lock"])(user.id, _this23._service).subscribe(function (res) {
                Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_7__["successAlert"])('Success');
              });
            }
          });
        }
      }, {
        key: "unlock",
        value: function unlock(user) {
          var _this24 = this;

          sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: "".concat(user.email, " will be unlocked"),
            showCancelButton: true,
            confirmButtonColor: '#db5e5e',
            cancelButtonColor: '#808080'
          }).then(function (result) {
            if (result.isConfirmed) {
              Object(_application_unlock__WEBPACK_IMPORTED_MODULE_6__["unlock"])(user.id, _this24._service).subscribe(function (res) {
                Object(_shared_error_custom_alerts__WEBPACK_IMPORTED_MODULE_7__["successAlert"])('Success');
              });
            }
          });
        }
      }, {
        key: "backSearch",
        value: function backSearch() {
          var _this25 = this;

          Object(_application_getUsers__WEBPACK_IMPORTED_MODULE_3__["getUsers"])(Object.assign(Object.assign(Object.assign({}, this.queryPagination), this.queryOrder), this.queryFilters), this._service).subscribe(function (response) {
            _this25.tableData = {
              content: response.content.map(function (i) {
                return {
                  id: i.id,
                  username: i.username,
                  email: i.email,
                  lockedValue: i.locked ? 'Locked' : 'Unlocked'
                };
              }),
              numberOfElements: response.numberOfElements,
              totalElements: response.totalElements,
              totalPages: response.totalPages
            };
          });
        }
      }, {
        key: "configChange",
        value: function configChange(event) {
          this.tableConfig = Object.assign({}, event[0]);
          this.filters = event[1];

          this._cookieFacade.save('user-table-config', JSON.stringify(Object(_shared_custom_mat_elements_dynamic__WEBPACK_IMPORTED_MODULE_1__["createTableFiltersConfig"])(this.tableConfig, this.filters, this.queryPagination, this.queryOrder)));
        }
      }, {
        key: "setConfigVisibility",
        value: function setConfigVisibility(visible) {
          this.configVisibility = visible;
        }
      }]);

      return UserTableComponent;
    }();

    UserTableComponent.ɵfac = function UserTableComponent_Factory(t) {
      return new (t || UserTableComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_user_service__WEBPACK_IMPORTED_MODULE_9__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_10__["CookieFacadeService"]));
    };

    UserTableComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: UserTableComponent,
      selectors: [["app-user-table"]],
      viewQuery: function UserTableComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dynamicTable = _t.first);
        }
      },
      decls: 2,
      vars: 4,
      consts: [[4, "ngIf"], [3, "visibility", "columnsConfig", "filtersConfig", "configChanged", "closed"], [1, "card-component"], [1, "title-component"], ["mat-flat-button", "", "mat-flat-button", "", "routerLink", "./new"], [1, "filters-plus-config"], [1, "dyn-filters", 3, "filters", "showFilters", "query"], ["mat-button", "", 1, "configButton", 3, "click"], [1, "card-content", 3, "config", "data", "pagination", "order", "action"], ["table", ""]],
      template: function UserTableComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, UserTableComponent_div_0_Template, 17, 4, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-dynamic-table-config-panel", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("configChanged", function UserTableComponent_Template_app_dynamic_table_config_panel_configChanged_1_listener($event) {
            return ctx.configChange($event);
          })("closed", function UserTableComponent_Template_app_dynamic_table_config_panel_closed_1_listener() {
            return ctx.setConfigVisibility(false);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loading);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visibility", ctx.configVisibility)("columnsConfig", ctx.tableConfig)("filtersConfig", ctx.filters);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_11__["NgIf"], _shared_custom_mat_elements_dynamic_table_dynamic_table_config_panel_dynamic_table_config_panel_component__WEBPACK_IMPORTED_MODULE_12__["DynamicTableConfigPanelComponent"], _angular_material_card__WEBPACK_IMPORTED_MODULE_13__["MatCard"], _angular_material_button__WEBPACK_IMPORTED_MODULE_14__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterLink"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_15__["MatIcon"], _shared_custom_mat_elements_dynamic_filters_dynamic_filters_component__WEBPACK_IMPORTED_MODULE_16__["DynamicFiltersComponent"], _shared_custom_mat_elements_dynamic_table_mark_ii_dynamic_table_mark_ii_component__WEBPACK_IMPORTED_MODULE_17__["DynamicTableMarkIiComponent"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC91c2VyL2luZnJhc3RydWN0dXJlL25nLWNvbXBvbmVudHMvdXNlci10YWJsZS9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXNzZXRzXFxzdHlsZXNcXHN0YWZmaXQtc3R5bGUtMS5zY3NzIiwic3JjL2FwcC9hdXRoL3VzZXIvaW5mcmFzdHJ1Y3R1cmUvbmctY29tcG9uZW50cy91c2VyLXRhYmxlL3VzZXItdGFibGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0NBQUE7QUFRQSx3QkFBQTtBQVFBLHVCQUFBO0FBR0EsMkJBQUE7QUFFQTtFQUNFLDZCQUFBO0FDaEJGO0FEa0JFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUNoQko7QURrQkk7RUFDRSxtQkEzQlE7RUE0QlIsZ0JBQUE7RUFDQSxjQUFBO0FDaEJOO0FEc0JBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDbkJGO0FEcUJFO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNuQko7QURxQkk7RUFDRSxZQUFBO0FDbkJOO0FEc0JFO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0FDcEJKO0FEd0JBO0VBQ0UsNkJBQUE7RUFDQSxlQUFBO0FDckJGO0FEdUJFO0VBQ0UsU0FBQTtBQ3JCSjtBRHdCRTtFQUNFLGdDQUFBO0FDdEJKO0FEeUJFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUN2Qko7QUR5Qkk7RUFDRSxtQkEzRVE7RUE0RVIsZ0JBQUE7RUFDQSxjQUFBO0FDdkJOO0FEMEJJO0VBQ0UseUJBNUVpQjtFQTZFakIsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ3hCTjtBRDZCQTtFQUNFLGNBbEZtQjtBQ3dEckI7QUQ2QkE7RUFDRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxXQUFBO0FDMUJGO0FENkJBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDMUJGO0FENkJBO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQzFCRjtBRDRCRTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQzFCSjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZUFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0FDM0JGO0FEa0NBO0VBQ0UsY0FBQTtBQy9CRjtBRGtDQSx5QkFBQTtBQUVBO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtREFBQTtFQUNBLGdCQUFBO0VBQ0EsMEJBQUE7RUFDQSx5QkFBQTtBQ2hDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSxjQUFBO0FDbENGO0FEcUNBLFlBQUE7QUFFQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsdUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxlQUFBO0FDbkNGO0FEc0NBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSwwQ0FBQTtBQ25DRiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvdXNlci9pbmZyYXN0cnVjdHVyZS9uZy1jb21wb25lbnRzL3VzZXItdGFibGUvdXNlci10YWJsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cclxuJHNtYWxsOiAwLjhyZW07XHJcbiRtZWRpdW06IDFyZW07XHJcbiRsYXJnZTogMS4yNXJlbTtcclxuJGV4dHJhLWxhcmdlOiAxLjU2M3JlbTtcclxuJGh1Z2U6IDEuOTUzcmVtO1xyXG4kZXh0cmEtaHVnZTogMi40NDFyZW07XHJcblxyXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXHJcbiRjb2xvci1wYWxldHRlLTEtbWFpbjogI0RFNzM3MztcclxuJGNvbG9yLXBhbGV0dGUtMS1zZWNvbmQ6ICNBQkFCRkY7XHJcbiRjb2xvci1wcmltYXJ5LWxpZ2h0OiAjRUZFRkVGO1xyXG4kY29sb3ItcHJpbWFyeS1kYXJrOiAjMTkxOTE5O1xyXG4kY29sb3Itc2Vjb25kYXJ5LWRhcms6ICMzMzMzMzM7XHJcbiRjb2xvci1zY3JvbGxiYXItdGh1bWI6ICM4QzhDOEM7XHJcblxyXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cclxuXHJcblxyXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXHJcblxyXG4uYy1jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcblxyXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgLmR5bi1maWx0ZXJzIHtcclxuICAgIGZsZXgtZ3JvdzogMTtcclxuICAgIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgICBtYXJnaW46IDAgNDBweCAwIDA7XHJcblxyXG4gICAgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcclxuICAgICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29uZmlnQnV0dG9uIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcbiAgbWF4LWhlaWdodDogODglO1xyXG5cclxuICAuY2FyZC1jb250ZW50LCAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1wYWxldHRlLTEtbWFpbjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMnJlbTtcclxuICAgICAgaGVpZ2h0OiAyLjNyZW07XHJcbiAgICAgIGNvbG9yOiAjZWZlZmVmO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcclxuICBjb2xvcjogJGNvbG9yLXByaW1hcnktZGFyaztcclxufVxyXG5cclxuLmZpbHRlciB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMC41ZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XHJcbiAgbWF4LXdpZHRoOiAxMGVtO1xyXG4gIG1hcmdpbi1yaWdodDogNWVtO1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuICAvLyBtYXgtaGVpZ2h0OiA4MDBweDtcclxuICAubWF0LXRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgfVxyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xyXG4gIGNvbG9yOiAjOGI4YjhiO1xyXG59XHJcblxyXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbn1cclxuXHJcbi5pZEhlYWRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWRDZWxsIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbm1hdC1jYXJkLWNvbnRlbnQge1xyXG4gIG1hcmdpbjogMCAzNHB4O1xyXG59XHJcblxyXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xyXG5cclxuLnByaW9yaXR5IHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi8vIEBUT0RPOiBjaGFuZ2UgY29sb3IgdG8gdmFyaWFibGVzXHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcclxufVxyXG5cclxuLnByaW9yaXR5LWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1ub3JtYWwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1iYWphIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbmluZ3VuYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLnJlZC1kYXRlIHtcclxuICBjb2xvcjogI2ViNDE0MTtcclxufVxyXG5cclxuLyogTUFUIFRBQiAqL1xyXG5cclxuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xyXG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAuMTIpO1xyXG59XHJcbiIsIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xuLmMtY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMge1xuICBmbGV4LWdyb3c6IDE7XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTUwcHg7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuY29uZmlnQnV0dG9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZmxleC1zaHJpbms6IDA7XG59XG5cbi5jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xuICBtYXgtaGVpZ2h0OiA4OCU7XG59XG4uY2FyZC1jb21wb25lbnQgLmNhcmQtY29udGVudCwgLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBtYXJnaW46IDA7XG59XG4uY2FyZC1jb21wb25lbnQgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBidXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xuICBib3JkZXItcmFkaXVzOiAycmVtO1xuICBoZWlnaHQ6IDIuM3JlbTtcbiAgY29sb3I6ICNlZmVmZWY7XG59XG5cbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XG4gIGNvbG9yOiAjMTkxOTE5O1xufVxuXG4uZmlsdGVyIHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cblxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XG4gIG1heC13aWR0aDogMTBlbTtcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XG59XG5cbi50YWJsZS1yZXNwb25zaXZlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy14OiBhdXRvO1xufVxuLnRhYmxlLXJlc3BvbnNpdmUgLm1hdC10YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLm1hdC1oZWFkZXItcm93IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcbiAgY29sb3I6ICM4YjhiOGI7XG59XG5cbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG5cbi5pZEhlYWRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pZENlbGwge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxubWF0LWNhcmQtY29udGVudCB7XG4gIG1hcmdpbjogMCAzNHB4O1xufVxuXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xuLnByaW9yaXR5IHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucHJpb3JpdHktbXV5YWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XG59XG5cbi5wcmlvcml0eS1hbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcbn1cblxuLnByaW9yaXR5LW5vcm1hbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XG59XG5cbi5wcmlvcml0eS1iYWphIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcbn1cblxuLnByaW9yaXR5LW5pbmd1bmEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucmVkLWRhdGUge1xuICBjb2xvcjogI2ViNDE0MTtcbn1cblxuLyogTUFUIFRBQiAqL1xuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufSJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UserTableComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-user-table',
          templateUrl: './user-table.component.html',
          styleUrls: ['./user-table.component.scss']
        }]
      }], function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"]
        }, {
          type: _services_user_service__WEBPACK_IMPORTED_MODULE_9__["UserService"]
        }, {
          type: src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_10__["CookieFacadeService"]
        }];
      }, {
        dynamicTable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['table']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/auth/user/infrastructure/resolvers/user-detail-resolver.service.ts":
  /*!************************************************************************************!*\
    !*** ./src/app/auth/user/infrastructure/resolvers/user-detail-resolver.service.ts ***!
    \************************************************************************************/

  /*! exports provided: UserDetailResolverService */

  /***/
  function srcAppAuthUserInfrastructureResolversUserDetailResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserDetailResolverService", function () {
      return UserDetailResolverService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _role_application_getRoles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../role/application/getRoles */
    "./src/app/auth/role/application/getRoles.ts");
    /* harmony import */


    var _scope_application_getScopes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../scope/application/getScopes */
    "./src/app/auth/scope/application/getScopes.ts");
    /* harmony import */


    var _application_getUsers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../application/getUsers */
    "./src/app/auth/user/application/getUsers.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _role_infrastructure_services_role_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../role/infrastructure/services/role.service */
    "./src/app/auth/role/infrastructure/services/role.service.ts");
    /* harmony import */


    var _scope_infrastructure_services_scope_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../scope/infrastructure/services/scope.service */
    "./src/app/auth/scope/infrastructure/services/scope.service.ts");
    /* harmony import */


    var _services_user_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../services/user.service */
    "./src/app/auth/user/infrastructure/services/user.service.ts");

    var UserDetailResolverService = /*#__PURE__*/function () {
      function UserDetailResolverService(roleService, scopeService, userService) {
        _classCallCheck(this, UserDetailResolverService);

        this.roleService = roleService;
        this.scopeService = scopeService;
        this.userService = userService;
      }

      _createClass(UserDetailResolverService, [{
        key: "resolve",
        value: function resolve(route, state) {
          var _this26 = this;

          var $roles = Object(_role_application_getRoles__WEBPACK_IMPORTED_MODULE_2__["getRoles"])({
            page: 0,
            size: 1000
          }, this.roleService);
          var $scopes = Object(_scope_application_getScopes__WEBPACK_IMPORTED_MODULE_3__["getScopes"])({
            page: 0,
            size: 1000
          }, this.scopeService);
          var $user = Object(_application_getUsers__WEBPACK_IMPORTED_MODULE_4__["getUsers"])({
            id: route.params['id']
          }, this.userService);
          var fork = route.params['id'] ? Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["forkJoin"])($roles, $scopes, $user) : Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["forkJoin"])($roles, $scopes);
          return fork.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (res) {
            return {
              data: {
                roles: res[0].content,
                scopes: res[1],
                user: res[2] ? _this26.join(res[2].content[0], res[0].content, res[1]) : null
              }
            };
          }));
        }
      }, {
        key: "join",
        value: function join(user, roles, scopes) {
          user.roles = roles.filter(function (r) {
            return user.roles.includes(r.id);
          }).map(function (r) {
            return r.name;
          });
          user.scopes = scopes.filter(function (s) {
            return user.scopes.includes(s.id);
          }).map(function (s) {
            return s.name;
          });
          return user;
        }
      }]);

      return UserDetailResolverService;
    }();

    UserDetailResolverService.ɵfac = function UserDetailResolverService_Factory(t) {
      return new (t || UserDetailResolverService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_role_infrastructure_services_role_service__WEBPACK_IMPORTED_MODULE_6__["RoleService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_scope_infrastructure_services_scope_service__WEBPACK_IMPORTED_MODULE_7__["ScopeService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_user_service__WEBPACK_IMPORTED_MODULE_8__["UserService"]));
    };

    UserDetailResolverService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: UserDetailResolverService,
      factory: UserDetailResolverService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UserDetailResolverService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _role_infrastructure_services_role_service__WEBPACK_IMPORTED_MODULE_6__["RoleService"]
        }, {
          type: _scope_infrastructure_services_scope_service__WEBPACK_IMPORTED_MODULE_7__["ScopeService"]
        }, {
          type: _services_user_service__WEBPACK_IMPORTED_MODULE_8__["UserService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/auth/user/infrastructure/resolvers/user-table-resolver.service.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/auth/user/infrastructure/resolvers/user-table-resolver.service.ts ***!
    \***********************************************************************************/

  /*! exports provided: UserTableResolverService */

  /***/
  function srcAppAuthUserInfrastructureResolversUserTableResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserTableResolverService", function () {
      return UserTableResolverService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _application_getUsers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../application/getUsers */
    "./src/app/auth/user/application/getUsers.ts");
    /* harmony import */


    var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/user.service */
    "./src/app/auth/user/infrastructure/services/user.service.ts");
    /* harmony import */


    var src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/auth/session/infrastructure/services/cookie.service */
    "./src/app/auth/session/infrastructure/services/cookie.service.ts");

    var UserTableResolverService = /*#__PURE__*/function () {
      function UserTableResolverService(_service, _cookieService) {
        _classCallCheck(this, UserTableResolverService);

        this._service = _service;
        this._cookieService = _cookieService;
      }

      _createClass(UserTableResolverService, [{
        key: "resolve",
        value: function resolve(route, state) {
          var _a, _b;

          var filtersCache = this._cookieService.get('userFilterCache');

          var tableFiltersConfig = JSON.parse(this._cookieService.get('user-table-config'));
          var orderParams = (_a = Object.assign({}, tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.order)) !== null && _a !== void 0 ? _a : {};
          var userParams = (_b = tableFiltersConfig === null || tableFiltersConfig === void 0 ? void 0 : tableFiltersConfig.pagination) !== null && _b !== void 0 ? _b : {
            page: 0,
            size: 10
          };
          return Object(_application_getUsers__WEBPACK_IMPORTED_MODULE_3__["getUsers"])(Object.assign(Object.assign(Object.assign({}, JSON.parse(filtersCache)), orderParams), userParams), this._service).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            return {
              data: res,
              tableFiltersConfig: tableFiltersConfig,
              filtersValue: filtersCache
            };
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(function (err) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])({
              data: null,
              error: err,
              message: 'Error on user table resolver, data couldn\'t be fetched'
            });
          }));
        }
      }]);

      return UserTableResolverService;
    }();

    UserTableResolverService.ɵfac = function UserTableResolverService_Factory(t) {
      return new (t || UserTableResolverService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieFacadeService"]));
    };

    UserTableResolverService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: UserTableResolverService,
      factory: UserTableResolverService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UserTableResolverService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]
        }, {
          type: src_app_auth_session_infrastructure_services_cookie_service__WEBPACK_IMPORTED_MODULE_5__["CookieFacadeService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/auth/user/infrastructure/services/user.service.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/auth/user/infrastructure/services/user.service.ts ***!
    \*******************************************************************/

  /*! exports provided: UserService */

  /***/
  function srcAppAuthUserInfrastructureServicesUserServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserService", function () {
      return UserService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _abstract_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../abstract-user-service */
    "./src/app/auth/user/infrastructure/abstract-user-service.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _user_mapper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../user-mapper */
    "./src/app/auth/user/infrastructure/user-mapper.ts");
    /* harmony import */


    var _session_infrastructure_services_session_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../session/infrastructure/services/session.service */
    "./src/app/auth/session/infrastructure/services/session.service.ts"); // @TODO missing root URL


    var UserService = /*#__PURE__*/function (_abstract_user_servic) {
      _inherits(UserService, _abstract_user_servic);

      var _super3 = _createSuper(UserService);

      function UserService(httpClient, mapper, sessionService) {
        var _this27;

        _classCallCheck(this, UserService);

        _this27 = _super3.call(this);
        _this27.httpClient = httpClient;
        _this27.mapper = mapper;
        _this27.sessionService = sessionService;
        _this27.BASE_URL = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].baseUrl;
        _this27.ENDPOINT = 'users';
        return _this27;
      }

      _createClass(UserService, [{
        key: "register",
        value: function register(user) {
          return this.httpClient.post("".concat(this.BASE_URL).concat(this.ENDPOINT), user).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.mapper.mapTo));
        }
      }, {
        key: "search",
        value: function search(params) {
          return this.httpClient.get("".concat(this.BASE_URL).concat(this.ENDPOINT, "/search"), {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]({
              fromObject: params
            })
          });
        }
      }, {
        key: "updateUserPassword",
        value: function updateUserPassword(userId, newPassword) {
          return this.httpClient.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(userId, "/password"), newPassword);
        }
      }, {
        key: "updateMyPassword",
        value: function updateMyPassword(oldPassword, newPassword) {
          return this.httpClient.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/password"), {
            new_password: newPassword,
            old_password: oldPassword
          });
        }
      }, {
        key: "lockUser",
        value: function lockUser(id) {
          return this.httpClient.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(id, "/lock"), {});
        }
      }, {
        key: "unlockUser",
        value: function unlockUser(id) {
          return this.httpClient.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(id, "/unlock"), {});
        }
      }, {
        key: "updateRoles",
        value: function updateRoles(userId, roles) {
          return this.httpClient.put("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(userId, "/roles"), {
            role_ids: roles
          }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.mapper.mapTo));
        }
      }, {
        key: "findMe",
        value: function findMe() {// const httpHeaders = new HttpHeaders().set('Authorization', this.sessionService.getAuth());
          // return this.httpClient.get<IUser>(`${this.BASE_URL}usuarios/me`, { headers: httpHeaders });
        }
      }, {
        key: "addRoles",
        value: function addRoles(userId, roles) {
          return this.httpClient.post("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(userId, "/roles"), {
            role_ids: roles
          }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.mapper.mapTo));
        }
      }, {
        key: "addPersonalrol",
        value: function addPersonalrol(userId) {
          return this.httpClient.post("".concat(this.BASE_URL).concat(this.ENDPOINT, "/").concat(userId, "/roles/personal"), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(this.mapper.mapTo));
        }
      }]);

      return UserService;
    }(_abstract_user_service__WEBPACK_IMPORTED_MODULE_1__["AbstractUserService"]);

    UserService.ɵfac = function UserService_Factory(t) {
      return new (t || UserService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_user_mapper__WEBPACK_IMPORTED_MODULE_5__["UserMapper"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_session_infrastructure_services_session_service__WEBPACK_IMPORTED_MODULE_6__["SessionService"]));
    };

    UserService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: UserService,
      factory: UserService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UserService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }, {
          type: _user_mapper__WEBPACK_IMPORTED_MODULE_5__["UserMapper"]
        }, {
          type: _session_infrastructure_services_session_service__WEBPACK_IMPORTED_MODULE_6__["SessionService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/auth/user/infrastructure/userRegister-mapper.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/auth/user/infrastructure/userRegister-mapper.ts ***!
    \*****************************************************************/

  /*! exports provided: UserRegisterMapper */

  /***/
  function srcAppAuthUserInfrastructureUserRegisterMapperTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserRegisterMapper", function () {
      return UserRegisterMapper;
    });

    var UserRegisterMapper = /*#__PURE__*/function () {
      function UserRegisterMapper() {
        _classCallCheck(this, UserRegisterMapper);
      }

      _createClass(UserRegisterMapper, [{
        key: "mapTo",
        value: function mapTo(params) {
          var userId = params.userId,
              username = params.username,
              email = params.email,
              password = params.password;
          return {
            userId: userId,
            username: username,
            email: email,
            password: password
          };
        }
      }]);

      return UserRegisterMapper;
    }();
    /***/

  },

  /***/
  "./src/app/systems-administrator/presentation/admin-panels/admin-panels.component.ts":
  /*!*******************************************************************************************!*\
    !*** ./src/app/systems-administrator/presentation/admin-panels/admin-panels.component.ts ***!
    \*******************************************************************************************/

  /*! exports provided: AdminPanelsComponent */

  /***/
  function srcAppSystemsAdministratorPresentationAdminPanelsAdminPanelsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AdminPanelsComponent", function () {
      return AdminPanelsComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../../../environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var _angular_material_card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/material/card */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
    /* harmony import */


    var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/material/tabs */
    "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tabs.js");
    /* harmony import */


    var _shared_pipes_SafeUrl__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../shared/pipes/SafeUrl */
    "./src/app/shared/pipes/SafeUrl.ts");

    var AdminPanelsComponent = /*#__PURE__*/function () {
      function AdminPanelsComponent() {
        _classCallCheck(this, AdminPanelsComponent);

        this.eureka = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].eureka;
        this.wallboard = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].wallboard;
        this.hytrix = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].hytrix;
        this.zipkin = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].zipkin;
        this.rabbitMq = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].rabbitMq;
        this.kibana = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].kibana;
      }

      _createClass(AdminPanelsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AdminPanelsComponent;
    }();

    AdminPanelsComponent.ɵfac = function AdminPanelsComponent_Factory(t) {
      return new (t || AdminPanelsComponent)();
    };

    AdminPanelsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AdminPanelsComponent,
      selectors: [["app-admin-panels"]],
      decls: 21,
      vars: 18,
      consts: [[1, "card-component"], ["label", "Eureka"], ["title", "panel", 3, "src"], ["label", "Wallboard"], ["label", "Hytrix"], ["label", "Zipkin"], ["label", "RabbitMQ"], ["label", "Kibana"]],
      template: function AdminPanelsComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card-content");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-tab-group");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "mat-tab", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "iframe", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](5, "safe");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-tab", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "iframe", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](8, "safe");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-tab", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "iframe", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "safe");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-tab", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "iframe", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](14, "safe");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "mat-tab", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "iframe", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](17, "safe");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-tab", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "iframe", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](20, "safe");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](5, 6, ctx.eureka), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeResourceUrl"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](8, 8, ctx.wallboard), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeResourceUrl"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 10, ctx.hytrix), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeResourceUrl"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](14, 12, ctx.zipkin), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeResourceUrl"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](17, 14, ctx.rabbitMq), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeResourceUrl"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](20, 16, ctx.kibana), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeResourceUrl"]);
        }
      },
      directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCard"], _angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCardContent"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_3__["MatTabGroup"], _angular_material_tabs__WEBPACK_IMPORTED_MODULE_3__["MatTab"]],
      pipes: [_shared_pipes_SafeUrl__WEBPACK_IMPORTED_MODULE_4__["SafeUrl"]],
      styles: [".c-card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.c-card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.filters-plus-config[_ngcontent-%COMP%] {\n  display: flex;\n  flex-flow: row;\n  align-items: center;\n  justify-content: space-between;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%] {\n  flex-grow: 1;\n  overflow-x: auto;\n  margin: 0 40px 0 0;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .dyn-filters[_ngcontent-%COMP%]     mat-form-field {\n  width: 150px;\n}\n.filters-plus-config[_ngcontent-%COMP%]   .configButton[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n  flex-shrink: 0;\n}\n.card-component[_ngcontent-%COMP%] {\n  margin: 3.5rem 1.5em 0 1.5rem;\n  max-height: 88%;\n}\n.card-component[_ngcontent-%COMP%]   .card-content[_ngcontent-%COMP%], .card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  margin: 0;\n}\n.card-component[_ngcontent-%COMP%]     mat-tab-group mat-tab-header {\n  margin-bottom: 0.5rem !important;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%] {\n  padding: 0;\n  display: flex;\n  flex-flow: row;\n  justify-content: space-between;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.563rem;\n  font-weight: 500;\n  margin: 0.5rem;\n}\n.card-component[_ngcontent-%COMP%]   .title-component[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\n  background-color: #DE7373;\n  border-radius: 2rem;\n  height: 2.3rem;\n  color: #efefef;\n}\n  .mat-select-value {\n  color: #191919;\n}\n.filter[_ngcontent-%COMP%] {\n  align-items: center;\n  margin-left: 0.5em;\n  margin-bottom: 20px;\n}\n.mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\nmat-form-field[_ngcontent-%COMP%]   mat-select[_ngcontent-%COMP%] {\n  max-width: 10em;\n  margin-right: 5em;\n}\n.table-responsive[_ngcontent-%COMP%] {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n}\n.table-responsive[_ngcontent-%COMP%]   .mat-table[_ngcontent-%COMP%] {\n  width: 100%;\n  max-width: 100%;\n  display: table;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.scrollbar-style[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\n.table-responsive[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n.mat-header-row[_ngcontent-%COMP%] {\n  background-color: black;\n  border-radius: 0.5rem;\n  color: #8b8b8b;\n}\n.idCell[_ngcontent-%COMP%]:first-of-type, .idHeader[_ngcontent-%COMP%]:first-of-type {\n  padding-left: 0;\n}\n.idHeader[_ngcontent-%COMP%] {\n  justify-content: center;\n  color: white;\n}\n.idCell[_ngcontent-%COMP%] {\n  text-align: center;\n  justify-content: center;\n  word-break: break-all;\n  margin-right: 10px;\n}\nmat-card-content[_ngcontent-%COMP%] {\n  margin: 0 34px;\n}\n\n.priority[_ngcontent-%COMP%] {\n  color: #ffffff;\n  padding: 5px 10px;\n  border-radius: 20px;\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\n  font-weight: 500;\n  text-transform: capitalize;\n  background-color: #c2c2c2;\n}\n.priority-muyalta[_ngcontent-%COMP%] {\n  background-color: #d84855;\n}\n.priority-alta[_ngcontent-%COMP%] {\n  background-color: #e97171;\n}\n.priority-normal[_ngcontent-%COMP%] {\n  background-color: #ffcb8e;\n}\n.priority-baja[_ngcontent-%COMP%] {\n  background-color: #a3d69d;\n}\n.priority-ninguna[_ngcontent-%COMP%] {\n  background-color: #c2c2c2;\n}\n.red-date[_ngcontent-%COMP%] {\n  color: #eb4141;\n}\n\n  mat-tab-group {\n  margin-left: 20px;\n}\n  .mat-tab-group.mat-primary .mat-ink-bar {\n  background-color: black;\n}\n  .mat-tab-label .mat-tab-label-content {\n  font-size: 13px;\n}\n  .mat-tab-label-content span {\n  margin-right: 5px;\n}\n  .mat-tab-header {\n  border-bottom: 0 solid rgba(0, 0, 0, 0.12);\n}\n.card-component[_ngcontent-%COMP%] {\n  height: 89%;\n}\n.card-component[_ngcontent-%COMP%]   mat-card-content[_ngcontent-%COMP%] {\n  height: 100%;\n}\n.card-component[_ngcontent-%COMP%]   mat-card-content[_ngcontent-%COMP%]   mat-tab-group[_ngcontent-%COMP%] {\n  height: 100% !important;\n}\niframe[_ngcontent-%COMP%] {\n  \n  height: 700px;\n  width: 100%;\n  margin-top: 0.5rem;\n  overflow-y: auto;\n}\niframe[_ngcontent-%COMP%]::-webkit-scrollbar {\n  width: 12px;\n  height: 12px;\n}\niframe[_ngcontent-%COMP%]::-webkit-scrollbar-thumb {\n  background: #ccc;\n  border-radius: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3lzdGVtcy1hZG1pbmlzdHJhdG9yL3ByZXNlbnRhdGlvbi9hZG1pbi1wYW5lbHMvQzpcXHVuYWl3b3Jrc3BhY2VcXHN0YWZmaXQtZnJvbnRlbmQtYW5ndWxhci9zcmNcXGFzc2V0c1xcc3R5bGVzXFxzdGFmZml0LXN0eWxlLTEuc2NzcyIsInNyYy9hcHAvc3lzdGVtcy1hZG1pbmlzdHJhdG9yL3ByZXNlbnRhdGlvbi9hZG1pbi1wYW5lbHMvYWRtaW4tcGFuZWxzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zeXN0ZW1zLWFkbWluaXN0cmF0b3IvcHJlc2VudGF0aW9uL2FkbWluLXBhbmVscy9DOlxcdW5haXdvcmtzcGFjZVxcc3RhZmZpdC1mcm9udGVuZC1hbmd1bGFyL3NyY1xcYXBwXFxzeXN0ZW1zLWFkbWluaXN0cmF0b3JcXHByZXNlbnRhdGlvblxcYWRtaW4tcGFuZWxzXFxhZG1pbi1wYW5lbHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0NBQUE7QUFRQSx3QkFBQTtBQVFBLHVCQUFBO0FBR0EsMkJBQUE7QUFFQTtFQUNFLDZCQUFBO0FDaEJGO0FEa0JFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUNoQko7QURrQkk7RUFDRSxtQkEzQlE7RUE0QlIsZ0JBQUE7RUFDQSxjQUFBO0FDaEJOO0FEc0JBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FDbkJGO0FEcUJFO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNuQko7QURxQkk7RUFDRSxZQUFBO0FDbkJOO0FEc0JFO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0FDcEJKO0FEd0JBO0VBQ0UsNkJBQUE7RUFDQSxlQUFBO0FDckJGO0FEdUJFO0VBQ0UsU0FBQTtBQ3JCSjtBRHdCRTtFQUNFLGdDQUFBO0FDdEJKO0FEeUJFO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUN2Qko7QUR5Qkk7RUFDRSxtQkEzRVE7RUE0RVIsZ0JBQUE7RUFDQSxjQUFBO0FDdkJOO0FEMEJJO0VBQ0UseUJBNUVpQjtFQTZFakIsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQ3hCTjtBRDZCQTtFQUNFLGNBbEZtQjtBQ3dEckI7QUQ2QkE7RUFDRSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUMxQkY7QUQ2QkE7RUFDRSxXQUFBO0FDMUJGO0FENkJBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDMUJGO0FENkJBO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQzFCRjtBRDRCRTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQzFCSjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0FDM0JGO0FEOEJBO0VBQ0UsZUFBQTtBQzNCRjtBRDhCQTtFQUNFLHVCQUFBO0VBQ0EsWUFBQTtBQzNCRjtBRDhCQTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0FDM0JGO0FEa0NBO0VBQ0UsY0FBQTtBQy9CRjtBRGtDQSx5QkFBQTtBQUVBO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtREFBQTtFQUNBLGdCQUFBO0VBQ0EsMEJBQUE7RUFDQSx5QkFBQTtBQ2hDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSx5QkFBQTtBQ2xDRjtBRHFDQTtFQUNFLHlCQUFBO0FDbENGO0FEcUNBO0VBQ0UseUJBQUE7QUNsQ0Y7QURxQ0E7RUFDRSxjQUFBO0FDbENGO0FEcUNBLFlBQUE7QUFFQTtFQUNFLGlCQUFBO0FDbkNGO0FEc0NBO0VBQ0UsdUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSxlQUFBO0FDbkNGO0FEc0NBO0VBQ0UsaUJBQUE7QUNuQ0Y7QURzQ0E7RUFDRSwwQ0FBQTtBQ25DRjtBQ2xNQTtFQUNFLFdBQUE7QURxTUY7QUNuTUU7RUFDRSxZQUFBO0FEcU1KO0FDbk1JO0VBQ0UsdUJBQUE7QURxTU47QUNoTUE7RUFDRSxnQ0FBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBRG1NRjtBQ2hNQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FEbU1GO0FDaE1BO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBRG1NRiIsImZpbGUiOiJzcmMvYXBwL3N5c3RlbXMtYWRtaW5pc3RyYXRvci9wcmVzZW50YXRpb24vYWRtaW4tcGFuZWxzL2FkbWluLXBhbmVscy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cclxuJHNtYWxsOiAwLjhyZW07XHJcbiRtZWRpdW06IDFyZW07XHJcbiRsYXJnZTogMS4yNXJlbTtcclxuJGV4dHJhLWxhcmdlOiAxLjU2M3JlbTtcclxuJGh1Z2U6IDEuOTUzcmVtO1xyXG4kZXh0cmEtaHVnZTogMi40NDFyZW07XHJcblxyXG4vKiA9PT09PSBQQUxFVFRFID09PT09ICovXHJcbiRjb2xvci1wYWxldHRlLTEtbWFpbjogI0RFNzM3MztcclxuJGNvbG9yLXBhbGV0dGUtMS1zZWNvbmQ6ICNBQkFCRkY7XHJcbiRjb2xvci1wcmltYXJ5LWxpZ2h0OiAjRUZFRkVGO1xyXG4kY29sb3ItcHJpbWFyeS1kYXJrOiAjMTkxOTE5O1xyXG4kY29sb3Itc2Vjb25kYXJ5LWRhcms6ICMzMzMzMzM7XHJcbiRjb2xvci1zY3JvbGxiYXItdGh1bWI6ICM4QzhDOEM7XHJcblxyXG4vKiA9PT09PSBXRUlHSFQgPT09PT0gKi9cclxuXHJcblxyXG4vKiA9PT09PSBURU1QTEFURSAxID09PT09ICovXHJcblxyXG4uYy1jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcblxyXG4gIC50aXRsZS1jb21wb25lbnQge1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWZsb3c6IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogJGV4dHJhLWxhcmdlO1xyXG4gICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICBtYXJnaW46IDAuNXJlbTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcblxyXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWZsb3c6IHJvdztcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHJcbiAgLmR5bi1maWx0ZXJzIHtcclxuICAgIGZsZXgtZ3JvdzogMTtcclxuICAgIG92ZXJmbG93LXg6IGF1dG87XHJcbiAgICBtYXJnaW46IDAgNDBweCAwIDA7XHJcblxyXG4gICAgOjpuZy1kZWVwIG1hdC1mb3JtLWZpZWxkIHtcclxuICAgICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29uZmlnQnV0dG9uIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICBmbGV4LXNocmluazogMDtcclxuICB9XHJcbn1cclxuXHJcbi5jYXJkLWNvbXBvbmVudCB7XHJcbiAgbWFyZ2luOiAzLjVyZW0gMS41ZW0gMCAxLjVyZW07XHJcbiAgbWF4LWhlaWdodDogODglO1xyXG5cclxuICAuY2FyZC1jb250ZW50LCAudGl0bGUtY29tcG9uZW50IHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCBtYXQtdGFiLWdyb3VwIG1hdC10YWItaGVhZGVyIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiAgLnRpdGxlLWNvbXBvbmVudCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZmxvdzogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgZm9udC1zaXplOiAkZXh0cmEtbGFyZ2U7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICRjb2xvci1wYWxldHRlLTEtbWFpbjtcclxuICAgICAgYm9yZGVyLXJhZGl1czogMnJlbTtcclxuICAgICAgaGVpZ2h0OiAyLjNyZW07XHJcbiAgICAgIGNvbG9yOiAjZWZlZmVmO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtc2VsZWN0LXZhbHVlIHtcclxuICBjb2xvcjogJGNvbG9yLXByaW1hcnktZGFyaztcclxufVxyXG5cclxuLmZpbHRlciB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogMC41ZW07XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XHJcbiAgbWF4LXdpZHRoOiAxMGVtO1xyXG4gIG1hcmdpbi1yaWdodDogNWVtO1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuICAvLyBtYXgtaGVpZ2h0OiA4MDBweDtcclxuICAubWF0LXRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogdGFibGU7XHJcbiAgfVxyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDEycHg7XHJcbiAgaGVpZ2h0OiAxMnB4O1xyXG59XHJcblxyXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhci10aHVtYiB7XHJcbiAgYmFja2dyb3VuZDogI2NjYztcclxuICBib3JkZXItcmFkaXVzOiAxNHB4O1xyXG59XHJcblxyXG4udGFibGUtcmVzcG9uc2l2ZTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gIHdpZHRoOiAxMnB4O1xyXG4gIGhlaWdodDogMTJweDtcclxufVxyXG5cclxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICBiYWNrZ3JvdW5kOiAjY2NjO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE0cHg7XHJcbn1cclxuXHJcbi5tYXQtaGVhZGVyLXJvdyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogMC41cmVtO1xyXG4gIGNvbG9yOiAjOGI4YjhiO1xyXG59XHJcblxyXG4uaWRDZWxsOmZpcnN0LW9mLXR5cGUsIC5pZEhlYWRlcjpmaXJzdC1vZi10eXBlIHtcclxuICBwYWRkaW5nLWxlZnQ6IDA7XHJcbn1cclxuXHJcbi5pZEhlYWRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWRDZWxsIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgd29yZC1icmVhazogYnJlYWstYWxsO1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuXHJcbm1hdC1jYXJkLWNvbnRlbnQge1xyXG4gIG1hcmdpbjogMCAzNHB4O1xyXG59XHJcblxyXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xyXG5cclxuLnByaW9yaXR5IHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA1cHggMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIGJveC1zaGFkb3c6IHJnYmEoMCwgMCwgMCwgMC4xNSkgMS45NXB4IDEuOTVweCAyLjZweDtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNjMmMyYzI7XHJcbn1cclxuXHJcbi8vIEBUT0RPOiBjaGFuZ2UgY29sb3IgdG8gdmFyaWFibGVzXHJcblxyXG4ucHJpb3JpdHktbXV5YWx0YSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Q4NDg1NTtcclxufVxyXG5cclxuLnByaW9yaXR5LWFsdGEge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlOTcxNzE7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1ub3JtYWwge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XHJcbn1cclxuXHJcbi5wcmlvcml0eS1iYWphIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTNkNjlkO1xyXG59XHJcblxyXG4ucHJpb3JpdHktbmluZ3VuYSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyYzJjMjtcclxufVxyXG5cclxuLnJlZC1kYXRlIHtcclxuICBjb2xvcjogI2ViNDE0MTtcclxufVxyXG5cclxuLyogTUFUIFRBQiAqL1xyXG5cclxuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC10YWItZ3JvdXAubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQge1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1oZWFkZXIge1xyXG4gIGJvcmRlci1ib3R0b206IDAgc29saWQgcmdiYSgwLCAwLCAwLCAuMTIpO1xyXG59XHJcbiIsIi8qID09PT09IEZPTlRTIChNYWpvciBUaGlyZCkgPT09PT0gKi9cbi8qID09PT09IFBBTEVUVEUgPT09PT0gKi9cbi8qID09PT09IFdFSUdIVCA9PT09PSAqL1xuLyogPT09PT0gVEVNUExBVEUgMSA9PT09PSAqL1xuLmMtY2FyZC1jb21wb25lbnQge1xuICBtYXJnaW46IDMuNXJlbSAxLjVlbSAwIDEuNXJlbTtcbn1cbi5jLWNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWZsb3c6IHJvdztcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuLmMtY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBoMiB7XG4gIGZvbnQtc2l6ZTogMS41NjNyZW07XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMC41cmVtO1xufVxuXG4uZmlsdGVycy1wbHVzLWNvbmZpZyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZmxvdzogcm93O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuZHluLWZpbHRlcnMge1xuICBmbGV4LWdyb3c6IDE7XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIG1hcmdpbjogMCA0MHB4IDAgMDtcbn1cbi5maWx0ZXJzLXBsdXMtY29uZmlnIC5keW4tZmlsdGVycyA6Om5nLWRlZXAgbWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTUwcHg7XG59XG4uZmlsdGVycy1wbHVzLWNvbmZpZyAuY29uZmlnQnV0dG9uIHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgZmxleC1zaHJpbms6IDA7XG59XG5cbi5jYXJkLWNvbXBvbmVudCB7XG4gIG1hcmdpbjogMy41cmVtIDEuNWVtIDAgMS41cmVtO1xuICBtYXgtaGVpZ2h0OiA4OCU7XG59XG4uY2FyZC1jb21wb25lbnQgLmNhcmQtY29udGVudCwgLmNhcmQtY29tcG9uZW50IC50aXRsZS1jb21wb25lbnQge1xuICBtYXJnaW46IDA7XG59XG4uY2FyZC1jb21wb25lbnQgOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAgbWF0LXRhYi1oZWFkZXIge1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW0gIWltcG9ydGFudDtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IHtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1mbG93OiByb3c7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cbi5jYXJkLWNvbXBvbmVudCAudGl0bGUtY29tcG9uZW50IGgyIHtcbiAgZm9udC1zaXplOiAxLjU2M3JlbTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwLjVyZW07XG59XG4uY2FyZC1jb21wb25lbnQgLnRpdGxlLWNvbXBvbmVudCBidXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjREU3MzczO1xuICBib3JkZXItcmFkaXVzOiAycmVtO1xuICBoZWlnaHQ6IDIuM3JlbTtcbiAgY29sb3I6ICNlZmVmZWY7XG59XG5cbjo6bmctZGVlcCAubWF0LXNlbGVjdC12YWx1ZSB7XG4gIGNvbG9yOiAjMTkxOTE5O1xufVxuXG4uZmlsdGVyIHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4ubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cblxubWF0LWZvcm0tZmllbGQgbWF0LXNlbGVjdCB7XG4gIG1heC13aWR0aDogMTBlbTtcbiAgbWFyZ2luLXJpZ2h0OiA1ZW07XG59XG5cbi50YWJsZS1yZXNwb25zaXZlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy14OiBhdXRvO1xufVxuLnRhYmxlLXJlc3BvbnNpdmUgLm1hdC10YWJsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IHRhYmxlO1xufVxuXG4uc2Nyb2xsYmFyLXN0eWxlOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbi5zY3JvbGxiYXItc3R5bGU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDEycHg7XG4gIGhlaWdodDogMTJweDtcbn1cblxuLnRhYmxlLXJlc3BvbnNpdmU6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZDogI2NjYztcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcbn1cblxuLm1hdC1oZWFkZXItcm93IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDAuNXJlbTtcbiAgY29sb3I6ICM4YjhiOGI7XG59XG5cbi5pZENlbGw6Zmlyc3Qtb2YtdHlwZSwgLmlkSGVhZGVyOmZpcnN0LW9mLXR5cGUge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG5cbi5pZEhlYWRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5pZENlbGwge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxubWF0LWNhcmQtY29udGVudCB7XG4gIG1hcmdpbjogMCAzNHB4O1xufVxuXG4vKiA9PT09PSBQUklPUklUWSA9PT09PSAqL1xuLnByaW9yaXR5IHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucHJpb3JpdHktbXV5YWx0YSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkODQ4NTU7XG59XG5cbi5wcmlvcml0eS1hbHRhIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U5NzE3MTtcbn1cblxuLnByaW9yaXR5LW5vcm1hbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmNiOGU7XG59XG5cbi5wcmlvcml0eS1iYWphIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2EzZDY5ZDtcbn1cblxuLnByaW9yaXR5LW5pbmd1bmEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzJjMmMyO1xufVxuXG4ucmVkLWRhdGUge1xuICBjb2xvcjogI2ViNDE0MTtcbn1cblxuLyogTUFUIFRBQiAqL1xuOjpuZy1kZWVwIG1hdC10YWItZ3JvdXAge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItbGFiZWwgLm1hdC10YWItbGFiZWwtY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWNvbnRlbnQgc3BhbiB7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC10YWItaGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufVxuXG4uY2FyZC1jb21wb25lbnQge1xuICBoZWlnaHQ6IDg5JTtcbn1cbi5jYXJkLWNvbXBvbmVudCBtYXQtY2FyZC1jb250ZW50IHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuLmNhcmQtY29tcG9uZW50IG1hdC1jYXJkLWNvbnRlbnQgbWF0LXRhYi1ncm91cCB7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xufVxuXG5pZnJhbWUge1xuICAvKlRPRE86IGhheSBxdWUgY2FtYmlhciBlc3RvIFhEKi9cbiAgaGVpZ2h0OiA3MDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDAuNXJlbTtcbiAgb3ZlcmZsb3cteTogYXV0bztcbn1cblxuaWZyYW1lOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIHdpZHRoOiAxMnB4O1xuICBoZWlnaHQ6IDEycHg7XG59XG5cbmlmcmFtZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xuICBiYWNrZ3JvdW5kOiAjY2NjO1xuICBib3JkZXItcmFkaXVzOiAxNHB4O1xufSIsIkBpbXBvcnQgJy4uLy4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvc3RhZmZpdC1zdHlsZS0xLnNjc3MnO1xyXG5cclxuLmNhcmQtY29tcG9uZW50IHtcclxuICBoZWlnaHQ6IDg5JTtcclxuXHJcbiAgbWF0LWNhcmQtY29udGVudCB7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcblxyXG4gICAgbWF0LXRhYi1ncm91cCB7XHJcbiAgICAgIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuaWZyYW1lIHtcclxuICAvKlRPRE86IGhheSBxdWUgY2FtYmlhciBlc3RvIFhEKi9cclxuICBoZWlnaHQ6IDcwMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1hcmdpbi10b3A6IDAuNXJlbTtcclxuICBvdmVyZmxvdy15OiBhdXRvO1xyXG59XHJcblxyXG5pZnJhbWU6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICB3aWR0aDogMTJweDtcclxuICBoZWlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbmlmcmFtZTo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICNjY2M7XHJcbiAgYm9yZGVyLXJhZGl1czogMTRweDtcclxufVxyXG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AdminPanelsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-admin-panels',
          templateUrl: './admin-panels.component.html',
          styleUrls: ['./admin-panels.component.scss']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/systems-administrator/systems-administrator-routing.module.ts":
  /*!*******************************************************************************!*\
    !*** ./src/app/systems-administrator/systems-administrator-routing.module.ts ***!
    \*******************************************************************************/

  /*! exports provided: routing */

  /***/
  function srcAppSystemsAdministratorSystemsAdministratorRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "routing", function () {
      return routing;
    });
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _presentation_admin_panels_admin_panels_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./presentation/admin-panels/admin-panels.component */
    "./src/app/systems-administrator/presentation/admin-panels/admin-panels.component.ts");
    /* harmony import */


    var _auth_user_infrastructure_ng_components_user_table_user_table_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../auth/user/infrastructure/ng-components/user-table/user-table.component */
    "./src/app/auth/user/infrastructure/ng-components/user-table/user-table.component.ts");
    /* harmony import */


    var _auth_user_infrastructure_ng_components_user_detail_user_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../auth/user/infrastructure/ng-components/user-detail/user-detail.component */
    "./src/app/auth/user/infrastructure/ng-components/user-detail/user-detail.component.ts");
    /* harmony import */


    var _auth_role_infrastructure_ng_components_roles_table_roles_table_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../auth/role/infrastructure/ng-components/roles-table/roles-table.component */
    "./src/app/auth/role/infrastructure/ng-components/roles-table/roles-table.component.ts");
    /* harmony import */


    var _auth_role_infrastructure_ng_components_role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../auth/role/infrastructure/ng-components/role-detail/role-detail.component */
    "./src/app/auth/role/infrastructure/ng-components/role-detail/role-detail.component.ts");
    /* harmony import */


    var _auth_user_infrastructure_resolvers_user_table_resolver_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../auth/user/infrastructure/resolvers/user-table-resolver.service */
    "./src/app/auth/user/infrastructure/resolvers/user-table-resolver.service.ts");
    /* harmony import */


    var _auth_user_infrastructure_resolvers_user_detail_resolver_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../auth/user/infrastructure/resolvers/user-detail-resolver.service */
    "./src/app/auth/user/infrastructure/resolvers/user-detail-resolver.service.ts");
    /* harmony import */


    var _auth_role_infrastructure_resolvers_role_table_resolver_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../auth/role/infrastructure/resolvers/role-table-resolver.service */
    "./src/app/auth/role/infrastructure/resolvers/role-table-resolver.service.ts");
    /* harmony import */


    var _auth_role_infrastructure_resolvers_role_detail_resolver_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../auth/role/infrastructure/resolvers/role-detail-resolver.service */
    "./src/app/auth/role/infrastructure/resolvers/role-detail-resolver.service.ts");

    var routes = [{
      path: 'admin-panels',
      component: _presentation_admin_panels_admin_panels_component__WEBPACK_IMPORTED_MODULE_1__["AdminPanelsComponent"],
      data: {
        breadcrumb: 'Admin Panels'
      }
    }, {
      path: 'users',
      data: {
        breadcrumb: 'Users'
      },
      children: [{
        path: '',
        component: _auth_user_infrastructure_ng_components_user_table_user_table_component__WEBPACK_IMPORTED_MODULE_2__["UserTableComponent"],
        data: {
          breadcrumb: ''
        },
        resolve: {
          response: _auth_user_infrastructure_resolvers_user_table_resolver_service__WEBPACK_IMPORTED_MODULE_6__["UserTableResolverService"]
        }
      }, {
        path: 'user-detail/:id',
        component: _auth_user_infrastructure_ng_components_user_detail_user_detail_component__WEBPACK_IMPORTED_MODULE_3__["UserDetailComponent"],
        data: {
          breadcrumb: 'detail/:id'
        },
        resolve: {
          response: _auth_user_infrastructure_resolvers_user_detail_resolver_service__WEBPACK_IMPORTED_MODULE_7__["UserDetailResolverService"]
        }
      }, {
        path: 'new',
        component: _auth_user_infrastructure_ng_components_user_detail_user_detail_component__WEBPACK_IMPORTED_MODULE_3__["UserDetailComponent"],
        data: {
          breadcrumb: 'new'
        },
        resolve: {
          response: _auth_user_infrastructure_resolvers_user_detail_resolver_service__WEBPACK_IMPORTED_MODULE_7__["UserDetailResolverService"]
        }
      }]
    }, {
      path: 'roles',
      data: {
        breadcrumb: 'Roles'
      },
      children: [{
        path: '',
        component: _auth_role_infrastructure_ng_components_roles_table_roles_table_component__WEBPACK_IMPORTED_MODULE_4__["RolesTableComponent"],
        data: {
          breadcrumb: ''
        },
        resolve: {
          response: _auth_role_infrastructure_resolvers_role_table_resolver_service__WEBPACK_IMPORTED_MODULE_8__["RoleTableResolverService"]
        }
      }, {
        path: 'role-detail/:id',
        component: _auth_role_infrastructure_ng_components_role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_5__["RoleDetailComponent"],
        data: {
          breadcrumb: 'detail/:id'
        },
        resolve: {
          response: _auth_role_infrastructure_resolvers_role_detail_resolver_service__WEBPACK_IMPORTED_MODULE_9__["RoleDetailResolverService"]
        }
      }, {
        path: 'new',
        component: _auth_role_infrastructure_ng_components_role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_5__["RoleDetailComponent"],
        data: {
          breadcrumb: 'new'
        },
        resolve: {
          response: _auth_role_infrastructure_resolvers_role_detail_resolver_service__WEBPACK_IMPORTED_MODULE_9__["RoleDetailResolverService"]
        }
      }]
    }];

    var routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes);
    /***/

  },

  /***/
  "./src/app/systems-administrator/systems-administrator.module.ts":
  /*!***********************************************************************!*\
    !*** ./src/app/systems-administrator/systems-administrator.module.ts ***!
    \***********************************************************************/

  /*! exports provided: SystemsAdministratorModule */

  /***/
  function srcAppSystemsAdministratorSystemsAdministratorModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SystemsAdministratorModule", function () {
      return SystemsAdministratorModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _systems_administrator_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./systems-administrator-routing.module */
    "./src/app/systems-administrator/systems-administrator-routing.module.ts");
    /* harmony import */


    var _shared_material_angular_material_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../shared/material/angular-material.module */
    "./src/app/shared/material/angular-material.module.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _presentation_admin_panels_admin_panels_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./presentation/admin-panels/admin-panels.component */
    "./src/app/systems-administrator/presentation/admin-panels/admin-panels.component.ts");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../shared/shared.module */
    "./src/app/shared/shared.module.ts");
    /* harmony import */


    var _auth_user_infrastructure_ng_components_user_table_user_table_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../auth/user/infrastructure/ng-components/user-table/user-table.component */
    "./src/app/auth/user/infrastructure/ng-components/user-table/user-table.component.ts");
    /* harmony import */


    var _auth_user_infrastructure_ng_components_user_detail_user_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../auth/user/infrastructure/ng-components/user-detail/user-detail.component */
    "./src/app/auth/user/infrastructure/ng-components/user-detail/user-detail.component.ts");
    /* harmony import */


    var _auth_role_infrastructure_ng_components_roles_table_roles_table_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../auth/role/infrastructure/ng-components/roles-table/roles-table.component */
    "./src/app/auth/role/infrastructure/ng-components/roles-table/roles-table.component.ts");
    /* harmony import */


    var _auth_role_infrastructure_ng_components_role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../auth/role/infrastructure/ng-components/role-detail/role-detail.component */
    "./src/app/auth/role/infrastructure/ng-components/role-detail/role-detail.component.ts");
    /* harmony import */


    var _auth_scope_infrastructure_ng_components_scope_management_scope_management_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../auth/scope/infrastructure/ng-components/scope-management/scope-management.component */
    "./src/app/auth/scope/infrastructure/ng-components/scope-management/scope-management.component.ts");
    /* harmony import */


    var _auth_user_infrastructure_userRegister_mapper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../auth/user/infrastructure/userRegister-mapper */
    "./src/app/auth/user/infrastructure/userRegister-mapper.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

    var SystemsAdministratorModule = function SystemsAdministratorModule() {
      _classCallCheck(this, SystemsAdministratorModule);
    };

    SystemsAdministratorModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: SystemsAdministratorModule
    });
    SystemsAdministratorModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function SystemsAdministratorModule_Factory(t) {
        return new (t || SystemsAdministratorModule)();
      },
      providers: [_auth_user_infrastructure_userRegister_mapper__WEBPACK_IMPORTED_MODULE_12__["UserRegisterMapper"]],
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _systems_administrator_routing_module__WEBPACK_IMPORTED_MODULE_2__["routing"], _shared_material_angular_material_module__WEBPACK_IMPORTED_MODULE_3__["AngularMaterialModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SystemsAdministratorModule, {
        declarations: [_presentation_admin_panels_admin_panels_component__WEBPACK_IMPORTED_MODULE_5__["AdminPanelsComponent"], _auth_user_infrastructure_ng_components_user_table_user_table_component__WEBPACK_IMPORTED_MODULE_7__["UserTableComponent"], _auth_user_infrastructure_ng_components_user_detail_user_detail_component__WEBPACK_IMPORTED_MODULE_8__["UserDetailComponent"], _auth_role_infrastructure_ng_components_roles_table_roles_table_component__WEBPACK_IMPORTED_MODULE_9__["RolesTableComponent"], _auth_role_infrastructure_ng_components_role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_10__["RoleDetailComponent"], _auth_scope_infrastructure_ng_components_scope_management_scope_management_component__WEBPACK_IMPORTED_MODULE_11__["ScopeManagementComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_13__["RouterModule"], _shared_material_angular_material_module__WEBPACK_IMPORTED_MODULE_3__["AngularMaterialModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SystemsAdministratorModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_presentation_admin_panels_admin_panels_component__WEBPACK_IMPORTED_MODULE_5__["AdminPanelsComponent"], _auth_user_infrastructure_ng_components_user_table_user_table_component__WEBPACK_IMPORTED_MODULE_7__["UserTableComponent"], _auth_user_infrastructure_ng_components_user_detail_user_detail_component__WEBPACK_IMPORTED_MODULE_8__["UserDetailComponent"], _auth_role_infrastructure_ng_components_roles_table_roles_table_component__WEBPACK_IMPORTED_MODULE_9__["RolesTableComponent"], _auth_role_infrastructure_ng_components_role_detail_role_detail_component__WEBPACK_IMPORTED_MODULE_10__["RoleDetailComponent"], _auth_scope_infrastructure_ng_components_scope_management_scope_management_component__WEBPACK_IMPORTED_MODULE_11__["ScopeManagementComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _systems_administrator_routing_module__WEBPACK_IMPORTED_MODULE_2__["routing"], _shared_material_angular_material_module__WEBPACK_IMPORTED_MODULE_3__["AngularMaterialModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"]],
          providers: [_auth_user_infrastructure_userRegister_mapper__WEBPACK_IMPORTED_MODULE_12__["UserRegisterMapper"]]
        }]
      }], null, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=systems-administrator-systems-administrator-module-es5.js.map